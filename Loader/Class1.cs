﻿using System;
using NLog;
using Torch;
using Torch.API;
using Torch.API.Session;
using Torch.API.Managers;
using Torch.Managers.PatchManager;
using System.Reflection;
using System.IO;

namespace Loader
{
	public class CommonPlugin : TorchPluginBase
	{
		private string[] libs = new string[] { "SharedLib.dll" , "SharedLibTorch.dll" };

		public override void Init(ITorchBase torch)
		{
			base.Init(torch);

			var log = LogManager.GetCurrentClassLogger();
			

			var absolute = Path.GetFullPath("./Plugins");
			log.Error("Current dir:" + absolute);

			foreach (var x in libs)
			{
				var path = Path.Combine(absolute, x);
				log.Error ("Loading DLL:" + path);
				var dependency1 = Assembly.LoadFile(path);
			}

			var DLL = Assembly.LoadFile(Path.Combine(absolute, "APIExtender.dll"));
			foreach (Type type in DLL.GetExportedTypes())
			{
				if (type.IsAssignableFrom(typeof(TorchPluginBase)))
				{

				}
				var c = Activator.CreateInstance(type);
				type.InvokeMember("Init", BindingFlags.InvokeMethod, null, c, new object[] { torch });
			}
		}

		public virtual void SessionChanged(ITorchSession session, TorchSessionState state)
		{
			switch (state)
			{
				case TorchSessionState.Loaded:
					var pm = Torch.Managers.GetManager<PatchManager>();
					try
					{
						var context = pm.AcquireContext();
						Patch(context);
						pm.Commit();
					}
					catch (Exception e)
					{
						//Log.Fatal("SafeZonePatch::Patch: " + e);
					}
					break;
			}
		}

		public virtual void Patch(PatchContext context)
		{

		}
	}
}
