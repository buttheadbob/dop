﻿using Sandbox.Game.World;
using VRage.Game.ModAPI;

namespace NAPI
{
    static class Relations2
    {
        public static bool IsAllyGrid(this IMyCubeGrid cubeGrid, long player)
        {
            IMyFaction myFaction = MySession.Static.Factions.TryGetPlayerFaction(player);
            foreach (var key in cubeGrid.BigOwners)
            {
                if (key == player) return true;

                if (myFaction != null)
                {
                    IMyFaction myFaction2 = MySession.Static.Factions.TryGetPlayerFaction(key);
                    if (myFaction2 == myFaction) { return true; }
                }
            }

            return false;
        }
    }
}