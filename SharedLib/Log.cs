﻿using Sandbox.Game;
using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRage.Utils;

namespace NAPI
{
    public interface ILogger
    {
        void Write(String s);
        void Info(String s);
        void Warn(String s);
    }

    public class DefaultLogger : ILogger
    {
        public void Info(string s)
        {
            Write(s);
        }

        public void Warn(string s)
        {
            Write(s);
        }

        public void Write(string s)
        {
            if (MyAPIGateway.Session != null)
            {
                Common.SendChatMessage(s);
            }
            else
            {
                MyLog.Default.WriteLine(s);
            }
        }

        
    }

    public static class Log
    {
        private static ILogger logger = new DefaultLogger();

        public static void Init(ILogger logger)
        {
            Log.logger = logger;
        }

        public static void Fatal(string s)
        {
            logger.Write(s);
        }

        public static void Fatal(Exception e, string s)
        {
            logger.Write(s + " " + e.ToString());
        }

        public static void Info(string s)
        {
            logger.Info(s);
        }

        public static void Warn(string s)
        {
            logger.Warn(s);
        }

        public static void Error(string s)
        {
            logger.Write(s);
        }

        public static void Error(Exception e, string s)
        {
            logger.Write(s + " " + e.ToString());
        }

        public static void Error(Exception e)
        {
            logger.Write(e.ToString());
        }

        public static StringBuilder sb = new StringBuilder();
        static Dictionary<long, DateTime> spamming = new Dictionary<long, DateTime>();
        static Dictionary<long, int> spammingAmounts = new Dictionary<long, int>();

        public static void LogSpamming(int key, Action<StringBuilder, int> action, long intervalMs = 1000, bool ingame = false)
        {
            try
            {
                if (!spamming.ContainsKey(key)) { spamming.Add(key, DateTime.Now.AddMilliseconds(-intervalMs * 2)); }

                if (!spammingAmounts.ContainsKey(key)) { spammingAmounts.Add(key, 0); }

                var lastTime = spamming[key];
                if ((DateTime.Now - lastTime).Duration().TotalMilliseconds > intervalMs)
                {
                    action(sb, spammingAmounts[key]);
                    spammingAmounts[key] = 0;
                    var what = sb.ToString();
                    sb.Clear();

                    spamming[key] = DateTime.Now;
                    if (ingame) { MyVisualScriptLogicProvider.SendChatMessage(what, "Log"); } else { Log.Error(what); }
                }
                else { spammingAmounts.Increment(key); }
            }
            catch (Exception e) { }
        }


    }
}
