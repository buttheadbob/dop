﻿using System;
using System.Collections.Generic;
using Sandbox.Definitions;
using VRage.Game;
using VRage.Game.ModAPI;
using VRage;
using VRage.ObjectBuilders;
using Sandbox.ModAPI;
using VRageMath;
using VRage.Game.Entity;
using Sandbox.Game.Entities;

namespace NAPI
{
    public static class InventoryUtils
    {
       
        public static void minus(this Dictionary<MyDefinitionId, MyFixedPoint> x, Dictionary<MyDefinitionId, MyFixedPoint> other)
        {
            foreach (var i in other)
            {
                if (x.ContainsKey(i.Key)) { x[i.Key] -= other[i.Key]; }
            }
        }

        public static void AddItem(this IMyInventory inv, MyDefinitionId id, double amount) { inv.AddItems((MyFixedPoint)amount, (MyObjectBuilder_PhysicalObject)MyObjectBuilderSerializer.CreateNewObject(id)); }
        public static void AddItem(this IMyInventory inv, MyDefinitionId id, MyFixedPoint amount) { inv.AddItems(amount, (MyObjectBuilder_PhysicalObject)MyObjectBuilderSerializer.CreateNewObject(id)); }

        public static void GetAllCargosInRange(ref BoundingSphereD sphere2, List<MyEntity> cargos, string blockName, double minimalLeftLiters = -1d, bool allowAssemblers = false)
        {
            var enterPoint = new List<MyEntity>();
            var data = MyEntities.GetEntitiesInSphere(ref sphere2);

            enterPoint.AddList(data); //any block with inventory
            enterPoint.RemoveAll((x) =>
            {
                var term = (x as IMyTerminalBlock);
                if (term == null) return true;
                if (!term.IsFunctional) return true;
                if (!term.HasLocalPlayerAccess()) return true;
                if (term.CustomName.Contains(blockName)) return true;
                if (!term.HasInventory) return true;
                return false;
            });


            var grids = new HashSet<IMyCubeGrid>();
            foreach (var x in enterPoint) { grids.Add((x as IMyCubeBlock).CubeGrid); }

            var allVariants = new HashSet<IMyTerminalBlock>();

            foreach (var g in grids)
            {
                var blocks = g.FindBlocks((x) =>
                {
                    var term = x.FatBlock as IMyTerminalBlock;
                    if (term == null) return false;
                    if (!term.IsFunctional) return false;
                    if (!term.HasLocalPlayerAccess()) return false;
                    if (term.CustomName.Contains(blockName)) return false;
                    if (!term.HasInventory) return false;
                    if (minimalLeftLiters >= 0)
                    {
                        if (term.GetInventory(0).GetLeftVolumeInLiters() < minimalLeftLiters)
                        {
                            if (allowAssemblers)
                            {
                                if (term is IMyAssembler && term.GetInventory(1).GetLeftVolumeInLiters() < minimalLeftLiters) { return false; }
                            }
                            else { return false; }
                        }
                    }

                    return (term is IMyCargoContainer || term is IMyShipConnector || (allowAssemblers && term is IMyAssembler));
                });

                foreach (var x in blocks)
                {
                    var fat = x.FatBlock as IMyTerminalBlock;
                    foreach (var y in enterPoint)
                    {
                        if (areConnected(fat, y as IMyTerminalBlock))
                        {
                            cargos.Add(fat as MyEntity);
                            break;
                        }
                    }
                }
            }

            data.Clear();
        }

        private static bool areConnected(IMyTerminalBlock a, IMyTerminalBlock b)
        {
            if (a.CubeGrid != b.CubeGrid) return false;
            if (a == b) return true;

            for (var x = 0; x < a.InventoryCount; x++)
            {
                for (var y = 0; y < b.InventoryCount; y++)
                {
                    if (a.GetInventory(x).IsConnectedTo(b.GetInventory(y))) { return true; }
                }
            }

            return false;
        }


        public static string CustomName(this IMyInventory x)
        {
            var term = (x.Owner as IMyTerminalBlock);
            if (term == null) return x.Owner.DisplayName;
            else return term.CustomName;
        }

        public static MyFixedPoint calculateCargoMass(this IMyCubeGrid grid)
        {
            MyFixedPoint mass = 0;
            grid.FindBlocks(x =>
            {
                var fat = x.FatBlock;
                if (fat == null || !fat.HasInventory) return false;
                var i = fat.GetInventory();
                if (i == null) return false;

                mass += i.CurrentMass;
                return false;
            });

            return mass;
        }

        public static MyFixedPoint RemoveAmount(this IMyInventory inv, MyDefinitionId id, double amount)
        {
            if (amount <= 0) return (MyFixedPoint)amount;

            var items = inv.GetItems();
            var l = items.Count;
            var k = 0;
            var am = (MyFixedPoint)amount;
            for (var i = 0; i < l; i++)
            {
                var itm = items[i];
                if (itm.Content.GetId() == id)
                {
                    if (itm.Amount <= am)
                    {
                        am -= itm.Amount;
                        inv.RemoveItemsAt(i - k);

                        //Log.Info("RemoveAmount:1:"+id + "/"+amount + " am:" + am);
                        k++;
                    }
                    else
                    {
                        inv.RemoveItemAmount(itm, am);
                        //Log.Info("RemoveAmount:2:"+id + "/"+amount + " am:" + am);
                        return 0;
                    }
                }
            }

            //Log.Info("RemoveAmount:END");

            return am;
        }

        public static Dictionary<MyDefinitionId, MyFixedPoint> CountItems(this IMyInventory inventory, Dictionary<MyDefinitionId, MyFixedPoint> d = null)
        {
            var items = inventory.GetItems();
            if (d == null) { d = new Dictionary<MyDefinitionId, MyFixedPoint>(); }

            foreach (var x in items)
            {
                var id = x.Content.GetId();
                if (!d.ContainsKey(id)) { d.Add(x.Content.GetId(), x.Amount); } else { d[id] += x.Amount; }
            }

            return d;
        }

        public static MyFixedPoint CountItemType(this IMyInventory inventory, String type, String subtype)
        {
            var total = MyFixedPoint.Zero;

            var items = inventory.GetItems();
            foreach (var x in items)
            {
                var id = x.Content.GetId();
                if (id.TypeId.ToString() == type)
                {
                    if (subtype == null || subtype == id.SubtypeName) { total += x.Amount; }
                }
            }

            return total;
        }


        public static int GetItemIndexByID(this IMyInventory inv, uint itemId)
        {
            for (var index = 0; index < inv.ItemCount; index++)
            {
                if (inv.GetItemAt(index).Value.ItemId == itemId) { return index; }
            }

            return -1;
        }

        public static void MoveAllItemsFrom(this IMyInventory inventory, IMyInventory from, Func<VRage.Game.ModAPI.Ingame.MyInventoryItem, MyFixedPoint?> p = null, bool alwaysAtEnd = false)
        {
            for (var x = from.ItemCount - 1; x >= 0; x--)
            {
                var t = from.GetItemAt(x);
                MyFixedPoint? amount = p != null ? p.Invoke(t.Value) : null;
                if (amount == null || amount > 0) { from.TransferItemTo(inventory, x, checkConnection: false, amount: amount, targetItemIndex: (alwaysAtEnd ? (int?)inventory.ItemCount : null)); }
            }
        }

        public static void PullRequest(this IMyInventory target, List<IMyInventory> from, Dictionary<MyDefinitionId, MyFixedPoint> what, bool alwaysAtEnd = false)
        {
            foreach (var x in from)
            {
                if (what.Count == 0) break;

                MoveAllItemsFrom(target, x, (i) =>
                {
                    if (what.ContainsKey(i.Type))
                    {
                        var need = what[i.Type];
                        var have = i.Amount;


                        if (need > have)
                        {
                            what[i.Type] = need - have;
                            return null;
                        }
                        else
                        {
                            what.Remove(i.Type);
                            return need;
                        }
                    }

                    return -1;
                }, alwaysAtEnd: alwaysAtEnd);
            }
        }

        public static void PushRequest(this IMyInventory target, List<IMyInventory> from, Dictionary<MyDefinitionId, MyFixedPoint> what, bool alwaysAtEnd = false)
        {
            foreach (var x in from)
            {
                if (what.Count == 0) break;

                MoveAllItemsFrom(x, target, (i) =>
                {
                    if (what.ContainsKey(i.Type))
                    {
                        var need = what[i.Type];
                        var have = i.Amount;


                        if (need > have)
                        {
                            what[i.Type] = need - have;
                            return null;
                        }
                        else
                        {
                            what.Remove(i.Type);
                            return need;
                        }
                    }

                    return -1;
                }, alwaysAtEnd: alwaysAtEnd);
            }
        }

        public static double CanProduce(this MyBlueprintDefinitionBase bp, Dictionary<MyDefinitionId, MyFixedPoint> items)
        {
            double can_produce_times = Double.MaxValue;
            foreach (var pre in bp.Prerequisites)
            {
                var have = items.GetOr(pre.Id, MyFixedPoint.Zero);
                var times = (double)have / (double)pre.Amount;
                if (times < can_produce_times)
                {
                    can_produce_times = times;
                    if (can_produce_times == 0) { return 0; }
                }
            }

            return can_produce_times;
        }

        public static double InputVolume(this MyBlueprintDefinitionBase bp)
        { //TODO
            return 1;
        }

        public static MyFixedPoint GetLeftVolume(this IMyInventory inventory) { return inventory.MaxVolume - inventory.CurrentVolume; }

        public static double GetLeftVolumeInLiters(this IMyInventory inventory) { return ((double)inventory.GetLeftVolume()) * 1000d; }

        public static double GetFilledRatio(this IMyInventory inventory) { return (double)inventory.CurrentVolume / (double)inventory.MaxVolume; }

        public static void SetItems(this IMyInventory inventory, Dictionary<MyDefinitionId, MyFixedPoint> set)
        { //OLD???
            var items = inventory.GetItems();
            var have = inventory.CountItems();
            var l = items.Count;


            for (var i = 0; i < l; i++)
            {
                var x = items[i];
                var id = x.Content.GetId();

                if (set.ContainsKey(id))
                {
                    var dx = set[id] - (have.ContainsKey(id) ? have[id] : 0);
                    if (dx < 0)
                    {
                        if (x.Amount + dx > 0)
                        {
                            inventory.RemoveItemAmount(x, -dx);
                            set.Remove(id);
                        }
                        else
                        {
                            items.RemoveAt(i);
                            i--;
                            l--;
                            set[id] += dx;
                        }
                    }
                    else
                    {
                        inventory.AddItems(dx, (MyObjectBuilder_PhysicalObject)MyObjectBuilderSerializer.CreateNewObject(id));
                        set.Remove(id);
                    }
                }
                else
                {
                    //DOP.Log.Info("SetItems : Other " + id + " " + x.Amount);
                }
            }

            foreach (var y in set)
            {
                //DOP.Log.Info("SetItems : AddItem Left" + (y.Value) + " " + (MyObjectBuilder_PhysicalObject)MyObjectBuilderSerializer.CreateNewObject(y.Key));
                inventory.AddItems(y.Value, (MyObjectBuilder_PhysicalObject)MyObjectBuilderSerializer.CreateNewObject(y.Key));
            }
        }
    }
}