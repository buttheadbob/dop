using Sandbox.Game.World;
using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using VRage.Game.ModAPI;

namespace NAPI
{
    public static class Common2
    {
        public static bool DelayAction(this Torch.Commands.CommandModule module, Dictionary<ulong, long> lastActivated, int minDelay)
        {
            var steamId = module.Context.Player.SteamUserId;
            var lastTime = lastActivated.GetValueOrDefault(steamId, 0);
            var now = SharpUtils.timeStamp();
            if (now - lastTime < minDelay)
            {
                module.Context.Respond("You cant use command so often");
                return false;
            }

            if (lastActivated.ContainsKey(steamId)) { lastActivated[steamId] = now; } else { lastActivated.Add(steamId, now); }

            return true;
        }


        public static MyPlayer getPlayer(long id)
        {
            var ind = new List<IMyPlayer>();

            MyAPIGateway.Players.GetPlayers(ind, (x) => { return x.IdentityId == id; });
            return ind.Count > 0 ? ind[0] as MyPlayer : null;
        }

        public static String getPlayerName(long id)
        {
            var p = getPlayer(id);
            return p == null ? "UnknownP" : p.DisplayName;
        }

        public static bool isBot(long id) { return MySession.Static.Players.IdentityIsNpc(id); }

        public static String allIdentity()
        {
            var ind = new List<IMyIdentity>();
            MyAPIGateway.Players.GetAllIdentites(ind, (x) => { return true; });
            var s = "";
            foreach (var x in ind) { s += x.DisplayName + "|" + x.IdentityId + " " + "\n"; }

            return s;
        }

        public static String allIdentity2()
        {
            var ind = new List<IMyPlayer>();
            MyAPIGateway.Multiplayer.Players.GetPlayers(ind, (x) => { return true; });
            var s = "";
            foreach (var x in ind) { s += x.DisplayName + "|" + x.IdentityId + " " + x.IsAdmin + " " + x.IsBot + " " + "\n"; }

            return s;
        }

        public static String allIdentity3()
        {
            var ind = new List<IMyPlayer>();
            MyAPIGateway.Players.GetPlayers(ind, (x) => { return true; });
            var s = "";
            foreach (var x in ind) { s += x.DisplayName + "|" + x.IdentityId + " " + x.IsAdmin + " " + x.IsBot + " " + "\n"; }

            return s;
        }
    }
}