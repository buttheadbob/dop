﻿using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRage.Game;
using VRage.Game.ModAPI;
using VRageMath;

namespace NAPI
{
    public static class Ext
    {
        public static void FindAndMove<T>(this List<T> list, int newPos, Func<T, bool> x)
        {
            var ind = list.FindIndex((y) => x.Invoke(y));
            if (ind != -1) { list.Move(ind, newPos); }
        }


        public static Dictionary<MyDefinitionId, int> GetBlockPrice(this IMySlimBlock slim, Dictionary<MyDefinitionId, int> dict = null)
        {
            if (dict == null) dict = new Dictionary<MyDefinitionId, int>();

            var cmps = (slim.BlockDefinition as MyCubeBlockDefinition).Components;
            foreach (var xx in cmps)
            {
                var id = xx.Definition.Id;
                var c = xx.Count;
                if (dict.ContainsKey(id)) { dict[id] += c; } else { dict.Add(id, c); }
            }

            return dict;
        }

        public static Dictionary<MyDefinitionId, int> GetBlockLeftNeededComponents(this IMySlimBlock slim, Dictionary<MyDefinitionId, int> dict = null, Dictionary<MyDefinitionId, int> temp = null)
        {
            if (dict == null) dict = new Dictionary<MyDefinitionId, int>();

            var cmps = (slim.BlockDefinition as MyCubeBlockDefinition).Components;

            temp.Clear();
            foreach (var xx in cmps)
            {
                var id = xx.Definition.Id;
                var c = xx.Count;
                if (temp.ContainsKey(id)) { temp[id] += c; } else { temp.Add(id, c); }
            }

            foreach (var x in temp)
            {
                var id = x.Key;
                var has = slim.GetConstructionStockpileItemAmount(id);
                var need = x.Value;
                var left = need - has;
                if (left > 0)
                {
                    if (dict.ContainsKey(id)) { dict[id] += left; } else { dict.Add(id, left); }
                }
            }

            return dict;
        }

        public static Vector3D GetWorldPosition(this IMySlimBlock block)
        {
            BoundingBoxD box;
            block.GetWorldBoundingBox(out box);
            return box.Center;
        }


        public static bool Contains(this MySafeZone __instance, Vector3 point)
        {
            if (__instance.Shape == MySafeZoneShape.Sphere)
            {
                BoundingSphereD boundingSphereD = new BoundingSphereD(__instance.PositionComp.GetPosition(), (double)__instance.Radius);
                return (boundingSphereD.Contains(point) != ContainmentType.Contains);
            }
            else
            {
                MyOrientedBoundingBoxD myOrientedBoundingBoxD = new MyOrientedBoundingBoxD(__instance.PositionComp.LocalAABB, __instance.PositionComp.WorldMatrix);
                return !myOrientedBoundingBoxD.Contains(ref point);
            }
        }
    }
}