﻿using System;
using System.Collections.Generic;

namespace NAPI
{
    public class ObjectPool<T>
    {
        private int max = 10;
        private List<T> data;
        private Func<T> creator;
        private Action<T> cleaner;

        public ObjectPool(Func<T> creator, Action<T> cleaner, int max = 10)
        {
            this.cleaner = cleaner;
            this.creator = creator;
            this.max = max;
            this.data = new List<T>(max);
        }

        public T get()
        {
            if (data.Count > 0) { return data.Pop(); } else { return creator.Invoke(); }
        }

        public void put(T item)
        {
            if (data.Count < max) { data.Add(item); cleaner.Invoke(item); }
            else
            {
                if (cleaner != null) cleaner.Invoke(item);
            }
        }
    }
}