using System;
using System.Collections.Generic;
using Sandbox.Definitions;
using VRageMath;
using VRage.Game.ModAPI;
using Sandbox.Game.Entities;
using VRage.Game.ObjectBuilders.Definitions.SessionComponents;
using NAPI;
using Sandbox.Game.Entities.Cube;
using VRage.Game.Entity;

namespace Scripts.Shared
{
    public static class Voxels
    {

        public static bool IsInsideVoxel(this MySlimBlock block, MyGridPlacementSettings settings)
        {            
            var def = block.BlockDefinition;
            if (!(def is MyCubeBlockDefinition)) return false;
            var CurrentBlockDefinition = def as MyCubeBlockDefinition;
            var cellSize = MyDefinitionManager.Static.GetCubeSize(CurrentBlockDefinition.CubeSize);
            var localBB = new BoundingBoxD(-CurrentBlockDefinition.Size * cellSize * 0.5f, CurrentBlockDefinition.Size * cellSize * 0.5f);
            block.GetLocalMatrix(out Matrix locmatr);            
            var isAllowed = MyCubeGrid.IsAabbInsideVoxel(MatrixD.Multiply(locmatr, block.CubeGrid.WorldMatrix), localBB, settings);
            return !isAllowed;
        }

        public static bool IsInsideVoxels(this IMyPlayer Me_Player)
        {
            try
            {
                var Me = Me_Player.Character;
                if (Me == null) return false;
                var vsettings = new VoxelPlacementSettings { PlacementMode = VoxelPlacementMode.Volumetric, MaxAllowed = 0.95f, MinAllowed = 0 };
                var settings = new MyGridPlacementSettings
                {
                    CanAnchorToStaticGrid = true,
                    EnablePreciseRotationWhenSnapped = true,
                    SearchHalfExtentsDeltaAbsolute = 0,
                    SearchHalfExtentsDeltaRatio = 0,
                    SnapMode = SnapMode.Base6Directions,
                    VoxelPlacement = vsettings
                };

                var worldMatrix = Me.WorldMatrix;
                var localAabb = Me.LocalAABB;

                return MyCubeGrid.IsAabbInsideVoxel(worldMatrix, localAabb, settings);
            }
            catch (Exception e)
            {
                Log.Error(e, "[Player_SOS_Rescue]");
                return false;
            }
        }
        /// <summary>
        /// True - grid in voxels
        /// </summary>
        /// <param name="cubeGrid"></param>
        /// <returns></returns>
        public static bool VoxelDetectorForGrids(IMyCubeGrid cubeGrid)
        {
            try
            {
                MyGridPlacementSettings grid_settings = new MyGridPlacementSettings();//WTF HERE?? grid_settings это настройка проверки с заполнением вокселями блока в 20% для быстрой проверки
                MyGridPlacementSettings settings = new MyGridPlacementSettings();//WTF HERE?? settings это настройка проверки с заполнением вокселями блока в 95% для медленной проверки потому что после создания свойство VoxelPlacementSettings.MaxAllowed не поменять

                settings.CanAnchorToStaticGrid = true;
                settings.EnablePreciseRotationWhenSnapped = true;
                settings.SearchHalfExtentsDeltaAbsolute = 0;
                settings.SearchHalfExtentsDeltaRatio = 0;
                settings.SnapMode = SnapMode.Base6Directions;

                grid_settings = settings;//WTF HERE??

                var vsettings = new VoxelPlacementSettings();
                vsettings.PlacementMode = VoxelPlacementMode.Volumetric;
                vsettings.MaxAllowed = 0.95f;
                vsettings.MinAllowed = 0;

                var grid_vsettings = new VoxelPlacementSettings();
                grid_vsettings.PlacementMode = VoxelPlacementMode.Volumetric;
                grid_vsettings.MaxAllowed = 0.20f;
                grid_vsettings.MinAllowed = 0;

                grid_settings.VoxelPlacement = grid_vsettings;
                settings.VoxelPlacement = vsettings;

                MatrixD grid_worldMatrix = cubeGrid.WorldMatrix;
                BoundingBoxD cubeGrid_localAABB = cubeGrid.LocalAABB;
                Log.Warn("(VoxelDetectorForGrids) check start " + cubeGrid.DisplayName + " WorldMatrix " + cubeGrid.WorldMatrix.ToString() + " LocalAABB " +  cubeGrid.LocalAABB.ToString());
                if (MyCubeGrid.IsAabbInsideVoxel(grid_worldMatrix, cubeGrid_localAABB, grid_settings)) 
                {           
                    Log.Warn("CheckEachGridBlock ="+ CheckEachGridBlock(cubeGrid, settings));
                    return true;
                    //return CheckEachGridBlock(cubeGrid, settings);
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "[Player_SOS_Rescue] ");
                //return;
            }
            return false;
        }


        public static bool CheckEachGridBlock(IMyCubeGrid cubeGrid, MyGridPlacementSettings settings)
        { // TODO: ready for slow mode optimization
            var blocks = new List<IMySlimBlock>();
            cubeGrid.GetBlocks(blocks, block => block.FatBlock != null);
            foreach (MySlimBlock block in blocks)
            {
                if (!block.IsInsideVoxel(settings)) continue;
                var Block_Type = block.BlockDefinition.Id.SubtypeName;
                Log.Error("Grid in Voxels " + Block_Type);
                return true;
            }

            return false;
        }
    }
}