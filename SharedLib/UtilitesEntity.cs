using System;
using System.Collections.Generic;
using System.Linq;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Slime;
using VRage.Game.ModAPI;
using VRage.ObjectBuilders;
using VRageMath;

namespace NAPI
{
    public static class UtilitesEntity
    {
        public static Dictionary<Vector3D, List<KeyValuePair<T, Vector3D>>> Cluster<T>(Dictionary<T, Vector3D> dict, float range)
        {
            range *= range;
            var cluster = new Dictionary<Vector3D, List<KeyValuePair<T, Vector3D>>>();

            foreach (var x in dict)
            {
                var found = false; 
                foreach (var y in cluster)
                {
                    var d = x.Value - y.Key;
                    if (d.LengthSquared() < range)
                    {
                        cluster[y.Key].Add(x);
                        found = true;
                    }
                }

                if (!found) { cluster.GetOrCreate(x.Value).Add(x); }
            }

            return cluster;
        }

        public static List<MySlimBlock> GetBlocksFromAllGrids(string subtypeId) { return GetBlocksFromAllGrids((b) => b.isExactSubtype(subtypeId)); }
        public static List<MySlimBlock> GetBlocksFromAllGrids(MyObjectBuilderType typeId, string subtypeId) { return GetBlocksFromAllGrids((b) => b.isExactId(typeId, subtypeId)); }

        public static List<MySlimBlock> GetBlocksFromAllGrids(Func<MySlimBlock, bool> filter) { return MyEntities.GetEntities().OfType<MyCubeGrid>().SelectMany(grid => grid.GetBlocks().Where(filter)).ToList(); }
        public static List<MySlimBlock> GetFatBlocksFromAllGrids(Func<MySlimBlock, bool> filter) { return MyEntities.GetEntities().OfType<MyCubeGrid>().SelectMany(grid => grid.GetFatBlocks().Select((x) => x.SlimBlock).Where(filter)).ToList(); }

        public static List<List<IMyCubeGrid>> GetAllGridGroups ()
        {
            var all = MyEntities.GetEntities().OfType<IMyCubeGrid>().ToHashSet();
            var ll = new List<List<IMyCubeGrid>>();

            while (all.Count > 0)
            {
                var connected = all.First().GetConnectedGrids(GridLinkTypeEnum.Physical);
                foreach(var x in connected)
                {
                    all.Remove((IMyCubeGrid)x);
                }
                ll.Add (connected);
            }
            return ll;
        }

        public static bool IsAdminGrid(MyCubeGrid x)
        {
            if (x.BigOwners != null && x.BigOwners.FirstOrDefault() > 0)
            {
                var Steamid = MySession.Static.Players.TryGetSteamId(x.BigOwners.FirstOrDefault());
                if (Steamid != 0 && MySession.Static.IsUserAdmin(Steamid))
                    return true;
            }

            if (x.BigOwners != null && x.BigOwners.FirstOrDefault() == 0)
            {
                if (x.DisplayName.Contains("[NPC-IGNORE]_[Wormhole-Gate]"))
                    return true;
            }

            return true;
        }
        
        public static List<MySlimBlock> GetBlocksFromAllGridsParalell(Func<MySlimBlock, bool> filter, Func<MyCubeGrid, bool> gridFilter = null)
        {
            var grids = MyEntities.GetEntities().OfType<MyCubeGrid>().ToList();
            var arrayList = new List<MySlimBlock>();

            MyAPIGateway.Parallel.ForEach(grids, (x) =>
            {
                if (x.Closed || x.MarkedForClose) { return; }

                if (gridFilter != null && !gridFilter(x))
                {
                    return;
                }
                
                var blocks = x.GetBlocks();

                var selected = new List<MySlimBlock>();
                foreach (var y in blocks)
                {
                    if (y == null) { continue; }
                    if (filter.Invoke(y)) { selected.Add(y); }
                }

                lock (arrayList) { arrayList.AddList(selected); }
            });

            lock (arrayList) { return arrayList; }
        }

        public static MyCubeGrid GetGrid (Func<MyCubeGrid, bool> filter)
        {
            return MyEntities.GetEntities().OfType<MyCubeGrid>().First(filter);
        }

        public static List<MyCubeGrid> GetAllGrids(Func<MyCubeGrid, bool> filter) { return MyEntities.GetEntities().OfType<MyCubeGrid>().Where(filter).ToList(); }

        public static List<MyCubeGrid> GetAllGrids() { return MyEntities.GetEntities().OfType<MyCubeGrid>().ToList(); }

        public static bool isEnabled(this MySlimBlock b)
        {
            var f = b.FatBlock as MyFunctionalBlock;
            return f != null && f.Enabled;
        }

        public static bool isEnabledAndFunctional(this MySlimBlock b)
        {
            var f = b.FatBlock as MyFunctionalBlock;
            return f != null && f.Enabled && f.IsFunctional;
        }

        public static bool isExactId(this MySlimBlock b, MyObjectBuilderType type, String subtypeId)
        {
            var id = b.BlockDefinition.Id;
            return id.TypeId == type && id.SubtypeName == subtypeId;
        }

        public static bool isExactSubtype(this MySlimBlock b, string subtypeId)
        {
            var id = b.BlockDefinition.Id;
            return id.SubtypeName == subtypeId;
        }

        public static bool idIsLike(this MySlimBlock b, string like)
        {
            var id = b.BlockDefinition.Id.TypeId + "/" + b.BlockDefinition.Id.SubtypeName;
            return id.Contains(like);
        }

        public static bool isNearPlayer(this MySlimBlock b, float distance)
        {
            var p = b.WorldPosition;
            distance *= distance;

            var players = new List<IMyPlayer>();
            MyAPIGateway.Multiplayer.Players.GetPlayers(players);
            foreach (var x in players)
            {
                if (x.Character != null)
                {
                    var d = (p - x.Character.GetPosition()).LengthSquared();
                    if (d < distance) return true;
                }
            }

            return false;
        }


        public static bool hasFat(this MySlimBlock b) { return b.FatBlock != null; }
    }
}