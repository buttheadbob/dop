namespace NAPI
{
    interface Getter<T>
    {
        T Get();
    }

    class FixedGetter<T> : Getter<T>
    {
        T t;
        public FixedGetter(T t) { this.t = t; }
        public T Get() { return t; }
    }
}