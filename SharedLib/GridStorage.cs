﻿using System;
using System.Collections.Generic;
using NAPI;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using Slime;
using VRage.Game.ModAPI;

/*
namespace SharedLib
{

    public class MyCubeGrid
    {
        public BlocksCache cache;
    }
    
    public class BlocksCache
    {
        public Holder<IMyTerminalBlock> TerminalBlocks;
        public Holder<IMyFunctionalBlock> FunctionalBlocks;
        public Holder<IMyShipConnector> ControllerBlocks;
        
        public Holder<IMyMotorSuspension> WheelSuspension;
        public Holder<IMyWheel> Wheels;
        
        public Holder<MyInventory> Inventories;
    }
    


    public static class EXT3
    {
        public static BlocksCache Blocks (this MyCubeGrid grid)
        {
            return new BlocksCache();
        }
    }

    public class Example
    {
        
        
        static void Do()
        {
            var holder = new HolderStaticInfo(new Type[] {typeof(IMyTerminalBlock) }, null, (o, grid, h) =>
            {
                var hh = new Holder<IMyTerminalBlock>();
                grid.Blocks().TerminalBlocks = hh;
                o.Add(h, hh);
            });
        }
    }
    
        public class HolderStaticInfo
        {
            internal String Name;
            internal Type[] Types;
                
            private Action<GridIndex, MyCubeGrid, HolderStaticInfo> m_creator;
            public Action<GridIndex, MyCubeGrid, HolderStaticInfo> Creator
            {
                get => m_creator;
                private set => m_creator = value;
            }
                
            public HolderStaticInfo(Type[] types, String Name, Action<GridIndex, MyCubeGrid, HolderStaticInfo> Creator)
            {
                this.Types = (Type[]) types.Clone();
                this.m_creator = Creator;
                this.Name = Name;
            }
        }

        public class Holder<T>
        {
            public void Match (object block, bool add, bool callEvent)
            {
                if (block is T tb)
                {
                    if (add)
                    {
                        Add(tb, callEvent);
                    }
                    else
                    {
                        Remove(tb, callEvent);
                    }
                }
            }
            
            
            /// <summary>
            /// Owner can have full access to set
            /// </summary>
            protected HashSet<T> Set = new HashSet<T>(); 
            
            
            /// <summary>
            /// Called only in main thread
            /// </summary>
            public Action<T> Added;
            /// <summary>
            /// Called only in main thread
            /// </summary>
            public Action<T> Removed;
            

            public void Contains(T t)
            {
                Set.Contains(t);
            }

            internal void Add(T t, bool callEvent)
            {
                Set.Add(t);
            }

            internal void Remove(T t, bool callEvent)
            {
                Set.Remove(t);
            }
        }
        
        

        
        
        public class GridIndex
        {
            private static List<HolderStaticInfo> Creators = new List<HolderStaticInfo>();
            private static Dictionary<Type, List<Type>> TypeToTypes = new Dictionary<Type, List<Type>>();

            public static void Register(HolderStaticInfo checker)
            {
                Creators.Add(checker);
            }
            
            public static void Init()
            {
                MyEntities.OnEntityCreate += entity =>
                {
                    if (entity is IMyCubeGrid g)
                    {
                        var h = new GridIndex();
                        h.AttachCreators();
                        g.OverFatBlocks((x) =>
                        {
                            h.AddOrRemove(x, true, true);
                        });
                        g.OnBlockAdded += block =>
                        {
                            if (block.FatBlock == null) return;
                            h.AddOrRemove(block, true, true);
                        };
                        g.OnBlockRemoved += block =>
                        {
                            if (block.FatBlock == null) return;
                            h.AddOrRemove(block, false, true);
                        };
                        
                    }
                };
            }
            
            
            private Dictionary<String, object> HoldersByName = new Dictionary<String, object>();
            private Dictionary<Type, List<Action<object, bool, bool>>> Checkers = new Dictionary<Type, List<Action<object, bool, bool>>>();

            public bool Add<T>(HolderStaticInfo staticInfo, Holder<T> h)
            {
                foreach (var x in staticInfo.Types)
                {
                    Checkers.GetOrCreate(x).Add(h.Match);
                }
                
                if (HoldersByName.ContainsKey(staticInfo.Name))
                {
                    return false;
                }
                
                HoldersByName[staticInfo.Name] = h;
                return true;
            }

            private void AttachCreators()
            {
                foreach (var creator in Creators)
                {
                    creator.Creator.Invoke(this, creator);
                }
            }
            
            internal void AddOrRemove(object obj, bool addedOrRemoved, bool triggerEvent)
            {
                var t = obj.GetType();
                foreach (var v in TypeToTypes[t])
                {
                    foreach (var action in Checkers[v])
                    {
                        action.Invoke(obj, addedOrRemoved, triggerEvent);
                    }
                }
            }
            
            public void RegisterTypes(Type t)
            {
                var list = new List<Type>();
                //var types = t..GetTypes();
                //foreach (var x in types)
                //{
                //    list.Add(x);
                //}
                TypeToTypes[t] = list;
            }
        }
}*/