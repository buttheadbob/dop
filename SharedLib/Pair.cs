﻿namespace NAPI
{
    public class Pair<K, V>
    {
        public K k;
        public V v;

        public Pair(K k, V v)
        {
            this.k = k;
            this.v = v;
        }
    }

    public class Tripple<K, V, T>
    {
        public K k;
        public V v;
        public T t;

        public Tripple(K k, V v, T t)
        {
            this.k = k;
            this.v = v;
            this.t = t;
        }
    }
}