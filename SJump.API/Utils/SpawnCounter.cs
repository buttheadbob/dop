﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using VRage.Game;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRageMath;

namespace SJump.API
{
    public class SpawnCounter
    {
        private int _counter;
        private List<IMyEntity> _entlist;
        private readonly int _maxCount;
        public Vector3D _targetSpawnPosition;
        private readonly ulong _steamid;
        private readonly MyObjectBuilder_CubeGrid[] _obs;
        private GateInfo _gateinfo;

        public SpawnCounter(MyObjectBuilder_CubeGrid[] obs, Vector3D TargetGatePosition, ulong steamid, int count, GateInfo gateinfo)
        {
            _obs = obs;
            _counter = 0;
            _entlist = new List<IMyEntity>();
            _maxCount = count;
            _targetSpawnPosition = TargetGatePosition;
            _steamid = steamid;
            _gateinfo = gateinfo;
        }

        public void Increment(IMyEntity ent)
        {
            _counter++;
            _entlist.Add(ent);


            if (_counter < _maxCount)
            {
                return;
            }
            var grids = SpawnCallback(_obs, _entlist, _targetSpawnPosition, _steamid, _gateinfo);

        }

        private static List<IMyEntity> SpawnCallback(MyObjectBuilder_CubeGrid[] obs, List<IMyEntity> grids, Vector3D TargetGatePosition, ulong steamid, GateInfo gateinfo)
        {
            MyAPIGateway.Multiplayer.SendEntitiesCreated(obs.ToList<MyObjectBuilder_EntityBase>());
            foreach (var ent in grids)
            {
                (ent as MyCubeGrid).DetectDisconnectsAfterFrame();
                MyAPIGateway.Entities.AddEntity(ent, true);
            }
            PlayerUtils.FixPilotsAgain(grids);
            MainLogic.ToLog("(SpawnSomeGrids_Callback) Incoming ship spawn fin,playersteamid = " + steamid + " Grid count = " + grids.Count());
            if (!String.IsNullOrEmpty(gateinfo.jump_in_msg))
                MyAPIGateway.Utilities.SendMessage(gateinfo.jump_in_msg);           
            //Util.SendOnNewPlayerSuccessToClient(steamid);
            //Thread.Sleep(0);
            //  Util.EntityRefresh.Refresh(steamid);
            return grids;
        }
    }
}