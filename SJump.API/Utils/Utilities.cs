﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ICSharpCode.SharpZipLib.BZip2;
using Newtonsoft.Json;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Multiplayer;
using Sandbox.ModAPI;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.Game.ObjectBuilders.Components;
using VRage.Game.ObjectBuilders.ComponentSystem;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage.Utils;
using VRageMath;

namespace SJump.API
{
    public class Util
    {
        private static readonly Random Random = new Random();

        public static bool RecievePayLoad(string req_body)
        {
            MainLogic.ToLog("RecievePayLoad");
            if (PayloadUnPack(Convert.FromBase64String(req_body), out ShipTransferObjectData clientData))
            {
                //if game pool if full this will spawn a new thread. =)
                MyAPIGateway.Parallel.StartBackground(() => RecievePayloadParallel(clientData));
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void RecievePayloadParallel(ShipTransferObjectData clientData)
        {
            var a = SerializeFromByteArrayToBP(clientData.Grids);
            SpawnUtils.SpawnSomeGrids(a.ShipBlueprints[0].CubeGrids, clientData.jumpInfo);
        }

        public static void AdressAndPortFromString(string orig_adressstr, out ushort port, out string ip)
        {
            var adressstr = orig_adressstr.Replace("http://", "");
            adressstr = adressstr.Replace("https://", "");
            string[] array = adressstr.Trim().Split(':');
            if (array.Length < 2)
            {
                port = 27016;
            }
            else
            {
                port = ushort.Parse(array[1]);
            }
            ip = System.Net.Dns.GetHostAddresses(array[0])[0].ToString();
        }

        public static bool TryGetEntityByNameOrId(string nameOrId, out IMyEntity entity)
        {
            if (long.TryParse(nameOrId, out long id))
            {
                return MyAPIGateway.Entities.TryGetEntityById(id, out entity);
            }

            foreach (var ent in MyEntities.GetEntities())
            {
                if (ent.DisplayName == nameOrId)
                {
                    entity = ent;
                    return true;
                }
            }

            entity = null;
            return false;
        }

        /// <summary>
        /// true = passed
        /// </summary>
        /// <param name="grids_objectbuilders"></param>
        /// <param name="blacklist">list with blacklisted SubtypeName items</param>
        /// <returns></returns>
        public static bool CheckBlackList_items(MyObjectBuilder_CubeGrid[] grids_objectbuilders, List<string> blacklist, out List<string> reason)
        {
            //var blacklist = ParseBlacklist(blacklisted_subtypes);
            reason = new List<string>();
            foreach (var grid in grids_objectbuilders)
            {
                var blocks = grid.CubeBlocks;
                foreach (var block in blocks)
                {
                    if (block.ComponentContainer != null)
                    {
                        MyObjectBuilder_ComponentContainer.ComponentData componentData = block.ComponentContainer.Components.Find((MyObjectBuilder_ComponentContainer.ComponentData s) => s.Component.TypeId == typeof(MyObjectBuilder_Inventory));
                        if (componentData != null)
                        {

                            var itemsininv = (componentData.Component as MyObjectBuilder_Inventory).Items;
                            if (itemsininv == null || itemsininv.Count == 0) continue;

                            foreach (var blackitem in blacklist)
                            {

                                foreach (var item in itemsininv)
                                {
                                    if (item.PhysicalContent.SubtypeName.Equals(blackitem, StringComparison.InvariantCultureIgnoreCase))
                                        reason.Add(item.SubtypeName);
                                }
                            }
                        }
                    }
                }
            }

            if (reason.Any())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// true = passed
        /// </summary>
        /// <param name="grids_objectbuilders">where find</param>
        /// <param name="blacklist">list with blacklisted SubtypeName items</param>
        /// <returns></returns>
        public static bool CheckBlackList_blocks(MyObjectBuilder_CubeGrid[] grids_objectbuilders, List<string> blacklist, out List<string> reason)
        {
            reason = new List<string>();
            //TODO
            //var blacklist = ParseBlacklist(blacklisted_subtypes);
            foreach (var grid in grids_objectbuilders)
            {
                var result = grid.CubeBlocks.Where(x => blacklist.Contains(x.SubtypeId.String));
                foreach (var block in result)
                {
                    reason.Add(block.SubtypeName);
                }
            }

            if (reason.Any()) return false;

            return true;
        }

        /// <summary>
        /// weird shit but only this works thx keen
        /// </summary>
        /// <param name="gridstotpob"></param>
        /// <returns></returns>
        private static byte[] SerializeBPToByteArray(MyObjectBuilder_Definitions grids_bp)
        {            
            var memsteam = new MemoryStream();
            MyObjectBuilderSerializer.SerializeXML(memsteam, grids_bp);
            return memsteam.ToArray();
        }

        private static MyObjectBuilder_Definitions SerializeFromByteArrayToBP(byte[] barray)
        {            
            MemoryStream mymemstream = new MemoryStream(barray);
            MyObjectBuilder_Base obs;
            MyObjectBuilderSerializer.DeserializeXML(mymemstream, out obs, typeof(MyObjectBuilder_Definitions));
            return obs as MyObjectBuilder_Definitions;
        }

        public static bool SendPayload(MyObjectBuilder_Definitions bp, JumpInfo jumpInfo, string API_Adress)
        {
            ShipTransferObjectData data = new ShipTransferObjectData(SerializeBPToByteArray(bp), jumpInfo);
            string totalStr = JsonConvert.SerializeObject(data, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full
            });

            bool result;
            using (MemoryStream source = new MemoryStream(Encoding.UTF8.GetBytes(totalStr)))
            {
                MemoryStream target = new MemoryStream();
                BZip2.Compress(source, target, false, 1024);
                totalStr = null;
                data = null;
                var url = "http://" + API_Adress + "/SJump/ShipDock/" + jumpInfo.PilotSteamId;
                result = API_requester.SendPayloadTo(target.ToArray(), url);
            }
            return result;
        }

        private static bool PayloadUnPack(byte[] rawdata, out ShipTransferObjectData clientData)
        {
            try
            {
                string obString;
                using (MemoryStream source = new MemoryStream(rawdata))
                {
                    using (MemoryStream target = new MemoryStream())
                    {
                        BZip2.Decompress(source, target, true);
                        obString = Encoding.UTF8.GetString(target.ToArray());
                    }
                }

                clientData = JsonConvert.DeserializeObject<ShipTransferObjectData>(obString, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto,
                    TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full
                });
                obString = null;
                return true;
            }
            catch (Exception ex)
            {
                MainLogic.ToLog("Error deserializing grid!");
                MainLogic.ToLog(ex.ToString());
                clientData = null;
                return false;
            }
        }

        /// <summary>
        /// Randomizes a vector by the given amount
        /// </summary>
        /// <param name="start"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static Vector3D RandomPositionFromPoint(Vector3D start, double distance)
        {
            double z = Random.NextDouble() * 2 - 1;
            double piVal = Random.NextDouble() * 2 * Math.PI;
            double zSqrt = Math.Sqrt(1 - z * z);
            var direction = new Vector3D(zSqrt * Math.Cos(piVal), zSqrt * Math.Sin(piVal), z);

            //var mat = MatrixD.CreateFromYawPitchRoll(RandomRadian, RandomRadian, RandomRadian);
            //Vector3D direction = Vector3D.Transform(start, mat);
            direction.Normalize();
            start += direction * -2;
            return start + direction * distance;
        }
    }
}
