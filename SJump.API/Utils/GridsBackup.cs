﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Sandbox.Definitions;
using VRage.Game;
using VRage.ObjectBuilders;
using VRage.Utils;

namespace SJump.API
{
    public class GridsBackup
    {
        private static readonly Random Random = new Random();
        public static bool SaveShipsToFile(ulong steamId, int totalpcu, int totalblocks, ref MyObjectBuilder_CubeGrid[] grids_objectbuilders, string folder, out string filename, out MyObjectBuilder_Definitions bp)
        {
            string gridname = "";
            if (grids_objectbuilders[0].DisplayName.Length > 30)
            {
                gridname = grids_objectbuilders[0].DisplayName.Substring(0, 30);
            }
            else
            {
                gridname = grids_objectbuilders[0].DisplayName;
            }
            string filenameexported = DateTime.Now.ToShortDateString() + "_" + DateTime.Now.ToLongTimeString() + "_" + totalpcu + "_" + totalblocks + "_" + gridname + "_" + Random.Next();
            var a1 = Path.GetInvalidPathChars();
            var b1 = Path.GetInvalidFileNameChars();

            var arraychar = a1.Concat(b1);
            foreach (char c in arraychar)
            {
                filenameexported = filenameexported.Replace(c.ToString(), ".");
            }
            filename = filenameexported;
                       
            return SaveToFile(ref grids_objectbuilders, steamId, folder, filenameexported, out bp);
        }

        //SJump.API.MainLogic.PluginConfig.BackupPath
        private static bool SaveToFile(ref MyObjectBuilder_CubeGrid[] grids_objectbuilders, ulong steamid, string folder, string filenameexported, out MyObjectBuilder_Definitions bp)
        {
            MyObjectBuilder_ShipBlueprintDefinition myObjectBuilder_ShipBlueprintDefinition = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_ShipBlueprintDefinition>();
            myObjectBuilder_ShipBlueprintDefinition.Id = new MyDefinitionId(new MyObjectBuilderType(typeof(MyObjectBuilder_ShipBlueprintDefinition)), MyUtils.StripInvalidChars(filenameexported));
            myObjectBuilder_ShipBlueprintDefinition.DLCs = GetNecessaryDLCs(myObjectBuilder_ShipBlueprintDefinition.CubeGrids);
            myObjectBuilder_ShipBlueprintDefinition.CubeGrids = grids_objectbuilders;
            myObjectBuilder_ShipBlueprintDefinition.RespawnShip = false;
            myObjectBuilder_ShipBlueprintDefinition.DisplayName = filenameexported;
            myObjectBuilder_ShipBlueprintDefinition.OwnerSteamId = steamid;
            //myObjectBuilder_ShipBlueprintDefinition.CubeGrids[0].DisplayName = blueprintDisplayName;
            MyObjectBuilder_Definitions myObjectBuilder_Definitions = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_Definitions>();
            myObjectBuilder_Definitions.ShipBlueprints = new MyObjectBuilder_ShipBlueprintDefinition[1];
            myObjectBuilder_Definitions.ShipBlueprints[0] = myObjectBuilder_ShipBlueprintDefinition;
            string mypathdir = Path.Combine(folder, steamid.ToString());
            if (!Directory.Exists(mypathdir))
            {
                Directory.CreateDirectory(mypathdir);
            }

            string finalpath = Path.Combine(mypathdir, filenameexported + ".sbc");
            bp = myObjectBuilder_Definitions;
            var flag = MyObjectBuilderSerializer.SerializeXML(finalpath, false, myObjectBuilder_Definitions, null);
            if (flag)
                MyObjectBuilderSerializer.SerializePB(finalpath + MyObjectBuilderSerializer.ProtobufferExtension, true, myObjectBuilder_Definitions);

            return flag;
        }

        private static string[] GetNecessaryDLCs(MyObjectBuilder_CubeGrid[] cubeGrids)
        {
            if (cubeGrids.IsNullOrEmpty<MyObjectBuilder_CubeGrid>())
            {
                return null;
            }

            HashSet<string> hashSet = new HashSet<string>();
            for (int i = 0; i < cubeGrids.Length; i++)
            {
                foreach (MyObjectBuilder_CubeBlock builder in cubeGrids[i].CubeBlocks)
                {
                    MyCubeBlockDefinition cubeBlockDefinition = MyDefinitionManager.Static.GetCubeBlockDefinition(builder);
                    if (cubeBlockDefinition != null && cubeBlockDefinition.DLCs != null && cubeBlockDefinition.DLCs.Length != 0)
                    {
                        for (int j = 0; j < cubeBlockDefinition.DLCs.Length; j++)
                        {
                            hashSet.Add(cubeBlockDefinition.DLCs[j]);
                        }
                    }
                }
            }
            return hashSet.ToArray<string>();
        }
    }
}
