﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using Sandbox.Definitions;
using Sandbox.Engine.Multiplayer;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.GameSystems.BankingAndCurrency;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using VRage;
using VRage.Game;
using VRage.Game.Entity;
using VRage.ModAPI;
using VRage.Network;
using VRage.ObjectBuilders;
using VRage.Utils;
using VRageMath;

namespace SJump.API
{
    public class PlayerUtils
    {
        /// <summary>
        /// in game thread pls, will remove all characters in world
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public static void ClearCharacters(ulong steamid)
        {
            try
            {
                MainLogic.ToLog("Find characters and delete. start");               
                MyIdentity current_Identity = Sync.Players.TryGetPlayerIdentity( new MyPlayer.PlayerId(steamid, 0));
                if (current_Identity == null)
                {
                     MainLogic.ToLog("Identity == null)");
                    return;
                }
                var temp = current_Identity.SavedCharacters.ToList();
                MainLogic.ToLog("Total found SavedCharacters: " + temp.Count);
                foreach (long entityId in temp)
                {
                    MyCharacter myCharacter = MyEntities.GetEntityById(entityId, false) as MyCharacter;
                    if (myCharacter == null)
                    {
                        continue;
                    }

                    if (myCharacter.IsUsing is MyCryoChamber)
                    {
                        MainLogic.ToLog("Found body in Cryo [");
                        (myCharacter.IsUsing as MyCryoChamber).RemovePilot();
                        MainLogic.ToLog("Found body in Cryo [" + (myCharacter.IsUsing as MyCryoChamber).Name + "], erase start");
                    }

                    if (myCharacter.IsUsing is MyCockpit)
                    {
                         MainLogic.ToLog("Found body in MyCockpit");
                        (myCharacter.IsUsing as MyCockpit).RemovePilot();
                        MainLogic.ToLog("Found body in MyCockpit end");
                    }

                    /*
                    if (player.Identity.SavedCharacters.Contains(myCharacter.EntityId))
                    {
                        player.Identity.SavedCharacters.Remove(myCharacter.EntityId);
                        MainLogic.ToLog(" body deleted [" + myCharacter.EntityId + "]");
                    }
                    */
                 
                    myCharacter.EnableBag(false);
                    MyVisualScriptLogicProvider.SetPlayersHealth(myCharacter.GetPlayerIdentityId(), 0);
                    myCharacter.Close();
                   // myCharacter.Kill(true, new VRage.Game.ModAPI.MyDamageInformation(true, 999999f, MyDamageType.Deformation, 0));
                   // myCharacter.Close();
                    MainLogic.ToLog("Found body erased.");

                }
                MainLogic.ToLog("SavedCharacters ...");
                current_Identity.SavedCharacters?.Clear();
                // MainLogic.ToLog("ChangeCharacter ..to null");
                current_Identity.ChangeCharacter(null);
                MainLogic.ToLog("Find characters and delete end. SavedCharacters count = " + current_Identity.SavedCharacters.Count());
                return;
            }
            catch (Exception ex)
            {
                MainLogic.ToLog("ClearCharacters catcvh!" + ex.ToString());
            }
        }


        private static MyIdentity AllocateIdentityForRemotePlayer(ulong clientSteamId, string displayName, string characterModel, long oldIdentityId)
        {
            long identity_by_steamid = Sync.Players.TryGetIdentityId(clientSteamId);
            if (Sync.Players.TryGetIdentityId(clientSteamId) != 0)
            {
                MainLogic.ToLog("Identity exists.Using it.");
                return Sync.Players.TryGetIdentity(identity_by_steamid);
            }

            if (Sync.Players.HasIdentity(oldIdentityId))
            {
                MyIdentity old_identity = Sync.Players.TryGetIdentity(oldIdentityId);
                if (old_identity != null)
                {
                    var serversteamid = Sync.Players.TryGetSteamId(old_identity.IdentityId);
                    if (serversteamid == clientSteamId)
                    {
                        MainLogic.ToLog("Identity exists.Using it.2.");
                        return old_identity;
                    }
                    else
                    {
                        MainLogic.ToLog("Identity in USE? creating new! remote steam id:" + clientSteamId);
                        return AllocateNewIdentityForNewPlayer(clientSteamId, displayName, characterModel);

                    }
                }
            }

            MainLogic.ToLog("Identity not exists and not in use,creating new using old identityID.");
            MyEntityIdentifier.MarkIdUsed(oldIdentityId);
            return Sync.Players.CreateNewIdentity(displayName, oldIdentityId, characterModel, new Vector3(255, 255, 255));
        }

        public static MyIdentity CreateRemotePlayerLocaly(ulong steamid, long identityID, string playerName, string characterModel)
        {
            MyPlayer.PlayerId playerId = new MyPlayer.PlayerId(steamid, 0);
            MyIdentity myIdentity = Sync.Players.TryGetPlayerIdentity(playerId);
            if (myIdentity == null)
            {
                myIdentity = AllocateIdentityForRemotePlayer(steamid, playerName, characterModel, identityID);
                if (myIdentity.IdentityId != 0)
                {
                    myIdentity.SetDisplayName(playerName);
                    var steamfakeclient = new MyNetworkClient(steamid, playerName);
                    var temp_player = Sync.Players.CreateNewPlayer(myIdentity, steamfakeclient, myIdentity.DisplayName, true);
                    Sync.Players.RemovePlayer(temp_player, true);
                    MySession.Static.Factions.CompatDefaultFactions(playerId);
                    MyAccountInfo myAccountInfo;
                    if (!MyBankingSystem.Static.TryGetAccountInfo(myIdentity.IdentityId, out myAccountInfo))
                    {
                        MyBankingSystem.Static.CreateAccount(myIdentity.IdentityId);
                    }
                    //TODO new thing MyMultiplayer.RaiseStaticEvent<ulong, int, long, string, List<Vector3>, bool>((IMyEventOwner s) => new Action<ulong, int, long, string, List<Vector3>, bool>(MyPlayerCollection.OnPlayerCreated), id.SteamId, id.SerialId, identity.IdentityId, playerName, list, realPlayer, default(EndpointId), null);
                    return myIdentity;
                }
                else
                {
                    throw new Exception("(NewPlayerRequest) [RARE][FATAL] Fuckup, bye. should be never throw...gg ");//should be never throw
                }
            }
            else
            {
                return myIdentity;
            }
        }

        private static void TryFindAndRemoveExistingCharacter2(MyPlayer player)
        {
            if (player == null)
            {
                return;
            }
            foreach (long entityId in player.Identity.SavedCharacters)
            {
                MyCharacter myCharacter = MyEntities.GetEntityById(entityId, false) as MyCharacter;
                if (myCharacter != null && !myCharacter.IsDead)
                {
                    if (myCharacter.Parent == null)
                    {
                        myCharacter.Kill(true, new VRage.Game.ModAPI.MyDamageInformation(true, 9999, MyStringHash.GetOrCompute("Deformation"), 0));
                        myCharacter.Close();
                        myCharacter = null;
                        return;
                    }
                    if (myCharacter.Parent is MyCockpit)
                    {
                        MyCockpit myCockpit = myCharacter.Parent as MyCockpit;
                        myCockpit.RemovePilot();
                        myCharacter.Kill(true, new VRage.Game.ModAPI.MyDamageInformation(true, 9999, MyStringHash.GetOrCompute("Deformation"), 0));
                        myCharacter.Close();
                        myCharacter = null;
                        return;
                    }
                }
            }
            return;
        }
        public static void FixPilotsAgain(List<IMyEntity> Grids)
        {
            foreach (MyCubeGrid CubeGird in Grids)
            {
                foreach (MySlimBlock block in CubeGird.GetBlocks())
                {                    
                    if (block?.FatBlock is MyCockpit c)
                    {
                        if (c.Pilot == null) continue;
                        try
                        {
                            MyIdentity Identity = c.Pilot.GetIdentity();
                            MainLogic.ToLog("Found " + Identity.DisplayName + " cockpit! IdentityID: "+ Identity.IdentityId);
                            Identity.LastLogoutTime = DateTime.Now;
                            Identity.LastLoginTime = DateTime.Now;
                            Identity.ChangeCharacter(c.Pilot);
                            c.RemovePilot();
                            MyPlayer.PlayerId ID2;
                            MySession.Static.Players.TryGetPlayerId(Identity.IdentityId, out ID2);
                            MainLogic.ToLog("Found PlayerId:" + ID2.SteamId);
                            MySession.Static.Players.SetControlledEntity(ID2, Identity.Character);
                           // MySession.Static.SetCameraController(MyCameraControllerEnum.Entity, c, null);                            
                            if(Identity.SavedCharacters.Count() == 0)
                            {
                                Identity.SavedCharacters.Add(c.Pilot.EntityId);
                            }                            
                            c.AttachPilot(Identity.Character);
                        }
                        catch (Exception ex)
                        {
                            MainLogic.ToLog("Unable to get pilot Identity!" + ex.ToString());
                        }
                    }
                }
            }
        }
        private static MyIdentity AllocateNewIdentityForNewPlayer(ulong clientSteamId, string displayName, string characterModel)
        {
            MyPlayer.PlayerId playerId = new MyPlayer.PlayerId(clientSteamId, 0);
            long identityId = MyEntityIdentifier.AllocateId(MyEntityIdentifier.ID_OBJECT_TYPE.IDENTITY, MyEntityIdentifier.ID_ALLOCATION_METHOD.SERIAL_START_WITH_1);

            if (Sync.Players.HasIdentity(identityId))
            {
                throw new Exception("Identity exists!");
            }
            //TODO check this with threads

            /*
            while (Sync.Players.HasIdentity(identityId))
            {
                identityId = MyEntityIdentifier.AllocateId(MyEntityIdentifier.ID_OBJECT_TYPE.IDENTITY, MyEntityIdentifier.ID_ALLOCATION_METHOD.SERIAL_START_WITH_1);
            }          
            */
            MyEntityIdentifier.MarkIdUsed(identityId);
            return Sync.Players.CreateNewIdentity(displayName, identityId, characterModel, new Vector3(255, 255, 255));
        }
    }
}
