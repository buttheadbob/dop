﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Newtonsoft.Json;
using VRage;
using VRage.Game;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.Network;
using VRage.Serialization;
using VRageMath;

namespace SJump.API
{
        public enum Colors
        {
            Red,
            Green,
            Blue,
            Yellow,
            Black,
            White,
            Grey,
            Brown,
            Lime,
            Orange,
            Navy,
            Purple,
            Magenta,
            Tan,
            Cyan,
            Olive,
            Maroon,
            Aquamarine,
            Turquoise,
            Silver,
            Teal,
            Indigo,
            Violet,
            Pink
        };
        public enum GridLinkType
        {
            Single,
            Logical,
            Physical,
            Mechanical,
        }        

        [Serializable]
        [Obfuscation(Exclude = true, ApplyToMembers = true)]
        public struct GateInfo
        {
            public Colors gate_color;
            public Colors to_gate_color;
            public string to_ServerAddress;
            public string to_APIAdress;
            public MyPromoteLevel promote_lvl;
            public bool copy_only;
            public string jump_out_msg;
            public string jump_in_msg;
            public int depart_dist;
            public int minarrival_dist;
            public int maxarrival_dist;
            public bool remote_limit_check;
            public List<string> blocks_subtype_blacklist;
            public List<string> items_subtype_blacklist;
            public bool reset_speed;
        }
        
     
        [Serializable]       
        public struct ShipInfo
        {
            public byte[] data;
            public ulong steamid;
            public string from_server;
            public string to_server;
            public DateTime warp_time;
            public uint id;
            public Colors GateColor;
            public uint spawned;
            //removed something
        }


        [Obfuscation(Exclude = true, ApplyToMembers = true)]
        [JsonObject]
        public class ShipTransferObjectData
        {
            public JumpInfo jumpInfo;

            public byte[] Grids;
            public ShipTransferObjectData()
            {
            }
            public ShipTransferObjectData(byte[] grids, JumpInfo jumpinfo)
            {
                Grids = grids;
                jumpInfo = jumpinfo;
            }
        }

        [Obfuscation(Exclude = true, ApplyToMembers = true)]
        public class GridsWrapper : IXmlSerializable
        {
            public MyObjectBuilder_CubeGrid[] Data { get; set; }

            public XmlSchema GetSchema()
            {
                return null;
            }

            public void ReadXml(XmlReader reader)
            {
                var ser = MyXmlSerializerManager.GetOrCreateSerializer(typeof(MyObjectBuilder_CubeGrid[]));
                var o = ser.Deserialize(reader);
                Data = (MyObjectBuilder_CubeGrid[])o;
            }

            public void WriteXml(XmlWriter writer)
            {

                var ser = MyXmlSerializerManager.GetOrCreateSerializer(typeof(MyObjectBuilder_CubeGrid[]));
                ser.Serialize(writer, Data);
            }

            public static implicit operator MyObjectBuilder_CubeGrid[](GridsWrapper o)
            {
                return o.Data;
            }

            public static implicit operator GridsWrapper(MyObjectBuilder_CubeGrid[] o)
            {
                return new GridsWrapper() { Data = o };
            }
        }

        [Obfuscation(Exclude = true, ApplyToMembers = true)]
        [JsonObject]
        public class JumpInfo
        {
            public ulong PilotSteamId;
            public Colors TargetGate;
            public List<string> CustomData;
            public Dictionary<ulong, Tuple<long, long, string>> OwnersData;
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    [Serializable]
    public class SerialPair<K, V>
    {
        #region Properties
        /// <summary>
        /// Key
        /// </summary>
        public K Key { get; set; }
        /// <summary>
        /// Value
        /// </summary>
        public V Value { get; set; }
        #endregion
        #region Constructors
        /// <summary>
        /// Default constructor
        /// Required, or XmlSerializer complains.
        /// </summary>
        public SerialPair()
        {
        }
        /// <summary>
        /// Construct a SerialPair from a KeyValuePair of the same types.
        /// </summary>
        /// <param name="kvp"></param>
        public SerialPair(KeyValuePair<K, V> kvp)
        {
            Key = kvp.Key;
            Value = kvp.Value;
        }
        /// <summary>
        /// Construct a SerialPair from a Key and Value of the same types.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public SerialPair(K key, V value)
        {
            Key = key;
            Value = value;
        }
        /// <summary>
        /// Static constructor - allows as quick as possible detection of 
        /// non-serializable parameters.
        /// </summary>
        static SerialPair()
        {
            if (!typeof(K).IsSerializable && !(typeof(K) is ISerializable))
            {
                throw new InvalidOperationException("A serializable Type is required");
            }
            if (!typeof(V).IsSerializable && !(typeof(V) is ISerializable))
            {
                throw new InvalidOperationException("A serializable Type is required");
            }
        }
        #endregion
        #region Public Methods
        /// <summary>
        /// Regenerate a Dictionary from a serialised string
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Dictionary<K, V> DeserializeDictionary(string s)
        {
            Dictionary<K, V> regeneratedTemplates;
            List<SerialPair<K, V>> list;
            byte[] combinedBytes = System.Text.Encoding.ASCII.GetBytes(s);
            using (MemoryStream sr = new MemoryStream(combinedBytes))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(List<SerialPair<K, V>>));
                list = (List<SerialPair<K, V>>)deserializer.Deserialize(sr);
                regeneratedTemplates = new Dictionary<K, V>();
                foreach (SerialPair<K, V> pair in list)
                {
                    regeneratedTemplates.Add(pair.Key, pair.Value);
                }
            }
            return regeneratedTemplates;
        }
        /// <summary>
        /// Generate a serialized string from a Dictionary.
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        public static string SerializeDictionary(Dictionary<K, V> dict)
        {
            string s;
            List<SerialPair<K, V>> list = new List<SerialPair<K, V>>(dict.Count);
            foreach (KeyValuePair<K, V> kvp in dict.ToArray())
            {
                SerialPair<K, V> sp = new SerialPair<K, V>(kvp);
                list.Add(sp);
            }
            using (MemoryStream xml = new MemoryStream())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<SerialPair<K, V>>));
                serializer.Serialize(xml, list);
                xml.Seek(0, 0);
                byte[] bytes = xml.GetBuffer();
                s = System.Text.Encoding.ASCII.GetString(bytes);
            }
            return s;
        }
        #endregion
    }

