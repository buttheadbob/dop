﻿using Havok;
using Sandbox;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using SpaceEngineers.Game.EntityComponents.DebugRenders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Torch;
using VRage;
using VRage.Game;
using VRage.ObjectBuilders;
using VRage.Utils;
using VRageMath;

namespace SJump.API
{
    public static class SpawnUtils
    {
        private static readonly Random Random = new Random();

        /// <summary>
        /// Remap, and spawn given grids group. And set correct position/rotation
        /// </summary>
        public static void SpawnSomeGrids(MyObjectBuilder_CubeGrid[] cubeGrids, JumpInfo jinfo)
        {
            try
            {
                var steamid = jinfo.PilotSteamId;
                GateInfo gateinfo = MainLogic.PluginConfig.Gates.Find(x => x.gate_color == jinfo.TargetGate);
                MainLogic.ToLog("SpawnSomeGridsFromThread start");
         
                var  targetposition = Vector3D.Zero; //TODO
                BoundingSphereD sphere = new BoundingSphereD(Vector3D.Zero, 0);
                double randomdistance = Random.Next(gateinfo.minarrival_dist, gateinfo.maxarrival_dist);
                targetposition = Util.RandomPositionFromPoint(targetposition, randomdistance);
                MyAPIGateway.Entities.RemapObjectBuilderCollection(cubeGrids);
                CreatePlayers(ref jinfo.OwnersData);
                for (int i = 0; i < cubeGrids.Length; i++)
                {
                    var ob = cubeGrids[i];
                    foreach (MyObjectBuilder_CubeBlock block in ob.CubeBlocks)
                    {
                        if (!(block is MyObjectBuilder_Cockpit c))
                        {
                            continue;
                        }

                        if (c.Pilot != null)
                        {
                            /*
                            MainLogic.ToLog("c.Pilot != null");
                            var pilotSteamId = c.Pilot.PlayerSteamId;
                            //var current_IdentityId = MyAPIGateway.Multiplayer.Players.TryGetIdentityId(pilotSteamId);
                            MyPlayer.PlayerId playerId = new MyPlayer.PlayerId(pilotSteamId, 0);
                            MyIdentity current_pilotIdentity = Sync.Players.TryGetPlayerIdentity(playerId);
                            if (current_pilotIdentity == null)
                            {
                                throw new Exception("!!!!!!!Cant found identity on this server!!!!, steamid:" + pilotSteamId);
                            }

                            c.Pilot.OwningPlayerIdentityId = current_pilotIdentity.IdentityId;
                            // var current_pilotIdentity = MySession.Static.Players.TryGetIdentity(current_IdentityId);
                            var work = TorchBase.Instance.InvokeAsync(() => PlayerUtils.ClearCharacters(pilotSteamId));
                            work.Wait();
                            var work2 = TorchBase.Instance.InvokeAsync(() =>
                            current_pilotIdentity.ChangeCharacter(null));
                            work2.Wait();
                            //current_pilotIdentity.SavedCharacters.Add(c.Pilot.EntityId);                            
                            //TODO more logs
                            //TODO c.Pilot.EntityId can be used bu someguy already?
                             //MyAPIGateway.Multiplayer.Players.SetControlledEntity(pilotSteamId, c.Pilot as VRage.ModAPI.IMyEntity);//HMM maybe this is not work
                            MainLogic.ToLog("SetControlledEntity successful");
                            */
                        }
                    }
                    ob.IsStatic = false;
                    if (gateinfo.reset_speed)
                    {
                        cubeGrids[i].LinearVelocity = new SerializableVector3(0, 0, 0);
                        cubeGrids[i].AngularVelocity = new SerializableVector3(0, 0, 0);
                    }

                    if (i == 0)
                    {
                        sphere = cubeGrids[i].CalculateBoundingSphere();
                    }
                    else
                    {
                        sphere = BoundingSphereD.CreateMerged(sphere, (cubeGrids[i].CalculateBoundingSphere()));
                    }
                }
                RemapOwnership(ref cubeGrids, jinfo.OwnersData);
                MyAPIGateway.Entities.RemapObjectBuilderCollection(cubeGrids);
                MainLogic.ToLog("(SpawnSomeGrids) Sphere radius = " + sphere.Radius.ToString());
                MainLogic.ToLog("(SpawnSomeGrids) Spawn target pos = " + targetposition.ToString());
                Vector3D newcleanpos = targetposition;
                //TODO Torch method.
                var a = TorchBase.Instance.InvokeAsync(() => MyAPIGateway.Entities.FindFreePlace(targetposition, (float)sphere.Radius + 100));
                a.Wait();
                Vector3D? newcleanpostemp = a.Result;
                if (newcleanpostemp != null)
                {
                    newcleanpos = (Vector3D)newcleanpostemp;
                }
                else
                {
                    MainLogic.ToLog("(SpawnSomeGrids) Cant find free place,chosen new random(2000)point");
                    newcleanpos = Util.RandomPositionFromPoint(newcleanpos, 2000);
                }

                MainLogic.ToLog("(SpawnSomeGrids) Final pos for spawn = " + newcleanpos.ToString());
                Vector3D oldpos = cubeGrids[0].PositionAndOrientation.GetValueOrDefault().Position + Vector3D.Zero;
                var counter = new SpawnCounter(cubeGrids, targetposition, steamid, cubeGrids.Count(), gateinfo);
                counter._targetSpawnPosition = newcleanpos;
                for (int i = 0; i < cubeGrids.Length; i++) // set position
                {
                    var ob = cubeGrids[i];

                    if (i == 0)
                    {

                        if (ob.PositionAndOrientation.HasValue)
                        {
                            var posiyto = ob.PositionAndOrientation.GetValueOrDefault();
                            posiyto.Position = newcleanpos;
                            ob.PositionAndOrientation = posiyto;
                            MainLogic.ToLog("(SpawnSomeGrids) new pos main grid = " + (ob.PositionAndOrientation.GetValueOrDefault().Position - Vector3D.Zero).ToString());
                        }
                    }
                    else
                    {
                        var o = ob.PositionAndOrientation.GetValueOrDefault();
                        o.Position = newcleanpos + o.Position - oldpos;
                        ob.PositionAndOrientation = o;
                    }
                    MyAPIGateway.Entities.CreateFromObjectBuilderParallel(ob, false, counter.Increment);
                    MainLogic.ToLog("SpawnSomeGridsFromThread end");
                }
            }
            catch (Exception ex)
            {
                MainLogic.ToLog("SpawnSomeGridsFromThread exception = " + ex.ToString());
            }
        }

        private static void CreatePlayers(ref Dictionary<ulong, Tuple<long, long, string>> ownersData)
        {
             MainLogic.ToLog("CreatePlayers start.");
            foreach (var steamId in ownersData.Keys.ToList())
            {               
                MyPlayer.PlayerId playerId = new MyPlayer.PlayerId(steamId, 0);
                MyIdentity current_Identity = Sync.Players.TryGetPlayerIdentity(playerId);
                if (current_Identity == null)
                {
                    MainLogic.ToLog("TryGetIdentityId(pilotSteamId) = 0 cant found any player identity on this server, trying to create, steamid: " + steamId);
                    current_Identity = PlayerUtils.CreateRemotePlayerLocaly(steamId, ownersData[steamId].Item1, ownersData[steamId].Item3, "Default_Astronaut");
                    MainLogic.ToLog($"Player {ownersData[steamId].Item3} created, old id:[{ownersData[steamId].Item1}], new id: [{current_Identity.IdentityId}]");
                }
                
                if(current_Identity == null)
                {
                    throw new Exception("Identity creation fail. Report to foogs!");                    
                }
                ownersData[steamId] = new Tuple<long, long, string>(ownersData[steamId].Item1, current_Identity.IdentityId, ownersData[steamId].Item3);
            }           
            MainLogic.ToLog("CreatePlayers end.");
        }

        public static void RemapOwnership(ref MyObjectBuilder_CubeGrid[] cubeGrids, Dictionary<ulong, Tuple<long, long, string>> OwnersData)
        {
            foreach (var grid in cubeGrids)
            {
                foreach (var block in grid.CubeBlocks)
                {
                    block.ShareMode = MyOwnershipShareModeEnum.Faction;
                    bool ownFlag = false;
                    bool buildFlag = false;
                    foreach (var owner in OwnersData)
                    {
                        if(block.Owner == owner.Value.Item1)
                        {
                            block.Owner = owner.Value.Item2;
                            ownFlag = true;
                        }
                        if (block.BuiltBy == owner.Value.Item1)
                        {
                            block.BuiltBy = owner.Value.Item2;
                            buildFlag = true;
                        }                       
                    }
                    if (!ownFlag)
                    {
                        block.Owner = 0;
                    }
                    if (!buildFlag)
                    {
                        block.BuiltBy = 0;
                    }
                }
            }
        }
    }
}
