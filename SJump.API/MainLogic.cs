﻿using Sandbox.Game.World;
using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using Torch;
using Torch.API.Managers;
using Torch.Managers.PatchManager;
using VRage.Game;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.Utils;
using VRageMath;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Torch.Mod;
using Torch.Mod.Messages;
using NLog;
using Sandbox.Game;
using Sandbox.Game.Multiplayer;
using Sandbox.Common.ObjectBuilders;
using System.Threading;

namespace SJump.API
{
    public class MainLogic
    {
        private static List<long> m_lockentities = new List<long>();
        private static readonly Logger m_log = LogManager.GetLogger("SJump");
        public static PluginSettings PluginConfig;
        public static MainLogic Instanse;
        public string Storagepath;
        bool IsTorch = true;
        MyRemoteServer myRemoteServer = null;

        public void InitLogic(PluginSettings pluginconfig)
        {
            PluginConfig = pluginconfig;
            Instanse = this;
              SetupGates();
            if (IsTorch)
            {
                var torch = TorchBase.Instance;
                Storagepath = torch.Config.InstancePath;
                myRemoteServer = new MyRemoteServer(PluginConfig.RemoteAPIPort, PluginConfig.RemoteApiKey);
            }
          
            m_log.Warn("Init SJump.API end.");
        }

        public void DisposeLogic()
        {
            myRemoteServer.Stop();
        }

        public void SendMsgToClient(string msg, ulong steamid = 0)
        {
            //TODO
            if (this.IsTorch)
            {
                // m_chatManager.SendMessageAsOther("HyperGate", msg, Color.Green, steamid, "White");
            }
            var playerId = Sync.Players.TryGetPlayerBySteamId(steamid).Identity.IdentityId;
            MyVisualScriptLogicProvider.SendChatMessage(msg, "HyperGate", playerId, "White");
        }

        public void StartJump(ulong steamId)
        {
            SendMsgToClient("Initializing jump...", steamId);
            MyAPIGateway.Parallel.StartBackground(
            () => DoJump(steamId));
        }

        private void DoJump(ulong steamId)
        {
            GateInfo targetGate = new GateInfo();
            ToLog("Jump request received by: " + steamId);
            if (!PluginConfig.Enabled)
            {
                ToLog("ServerJump is disabled in config!");
                SendMsgToClient("ServerJump disabled by admin!", steamId);
                return;
            }

            /*
            if (Patch.IsRestartSoon(10))
            {
            ToLog("Restart soon jump aborted for: " + steamId);
            SendMsgToClient("Restart will happens soon, try again later!", steamId);
            return;
            }*/

            IMyPlayer requestedplayer = Sync.Players.TryGetPlayerBySteamId(steamId);
            if (!(requestedplayer?.Controller?.ControlledEntity?.Entity is IMyCubeBlock block))
            {
                SendMsgToClient("Can't find your ship. Make sure you're seated in the ship you want to take with you.", steamId);
                return;
            }

            MyCubeGrid pilotedgGrid = (MyCubeGrid)(block?.CubeGrid);
            if (pilotedgGrid == null)
            {
                SendMsgToClient("Can't find your ship. Make sure you're seated in the ship you want to take with you.", steamId);
                return;
            }

            var pilotedgGridId = pilotedgGrid.EntityId;
            try
            {
                lock (m_lockentities)
                {
                    if (m_lockentities.Contains(pilotedgGridId))
                    {
                        ToLog("EntityId is already locked aborted! = " + pilotedgGridId);
                        SendMsgToClient("Failed to initialize jump because it is already in progress. Try later.", steamId);
                        return;
                    }
                    else
                    {
                        m_lockentities.Add(pilotedgGridId);
                        ToLog("EntityId locked! = " + pilotedgGridId);
                    }
                }

                targetGate = PluginConfig.Gates.FirstOrDefault(); // TODO
                var raw_grids = MyCubeGridGroups.Static.GetGroups(GridLinkTypeEnum.Logical).GetGroupNodes(pilotedgGrid);
                raw_grids.SortNoAlloc((x, y) => x.BlocksCount.CompareTo(y.BlocksCount));
                raw_grids.Reverse();
                raw_grids.SortNoAlloc((x, y) => x.GridSizeEnum.CompareTo(y.GridSizeEnum));
                // if (!CheckCanSaveShip(grids, target_projector, steamid, player))
                int index = 0;
                var gridsOB = new MyObjectBuilder_CubeGrid[raw_grids.Count];
                int allPCU = 0;
                int blockCount = 0;
                Dictionary<ulong, Tuple<long, long, string>> ownersData = new Dictionary<ulong, Tuple<long, long, string>>();
                foreach (MyCubeGrid x in raw_grids)
                {
                    var ob = (MyObjectBuilder_CubeGrid)x.GetObjectBuilder(true);
                    gridsOB[index] = ob;
                    index++;
                    allPCU += x.BlocksPCU;
                    blockCount += x.BlocksCount;
                    foreach (var owner in x.SmallOwners)
                    {
                        var ownersteamid = Sync.Players.TryGetSteamId(owner);
                        if (ownersteamid != 0)
                        {
                            ownersData[ownersteamid] = new Tuple<long, long, string>(owner, 0, Sync.Players.TryGetIdentityNameFromSteamId(ownersteamid));
                        }
                    }
                }

                #region Find passengers

                List<ulong> passengers = new List<ulong>();
                foreach (var g in gridsOB)
                {
                    foreach (var b in g.CubeBlocks)
                    {
                        if (!(b is MyObjectBuilder_Cockpit c))
                        {
                            continue;
                        }

                        if (c.Pilot != null)
                        {
                            //var pilotSteamId = c.Pilot.PlayerSteamId;
                            //if (pilotSteamId != 0)
                            //{
                            //    ToLog("Found passages pilotSteamId." + pilotSteamId);
                            //    passengers.Add(pilotSteamId);
                            //}
                        }
                    }
                }

                #endregion

                Util.AdressAndPortFromString(targetGate.to_ServerAddress, out ushort target_port, out string target_ip);
                Util.AdressAndPortFromString(targetGate.to_APIAdress, out ushort target_API_port, out string target_API_ip);
                //string target_API_port = targetGate.to_apiPort;

                if (!API_requester.CheckSlots(target_API_ip + ":" + target_API_port))
                {
                    SendMsgToClient("No free slots on target server.", steamId);
                    UnlockGrid(pilotedgGridId);
                    return;
                }
                else
                {
                    SendMsgToClient("Target server is ready.", steamId);
                }

                /*
                }
                if (false)
                {
                if (!API_requester.CheckRestart(target_ip + ":" + target_API_port))
                {
                SendMsgToClient("Target server is restarting soon, please try later.", steamId);
                UnlockGrid(pilotedgGridId);
                return;
                }
                }*/

                if (targetGate.remote_limit_check)
                {
                    if (RemoteLimitsAPI.CheckLimits_request_remote(gridsOB, "http://" + target_API_ip + ":" + target_API_port + "/SJump/LimitCheck/" + steamId))
                    {
                        SendMsgToClient("Remote limits check passed.", steamId);
                    }
                    else
                    {
                        SendMsgToClient("Your ship exceeds target server limits. Jump aborted.", steamId);
                        UnlockGrid(pilotedgGridId);
                        return;
                    }
                }

                if (targetGate.blocks_subtype_blacklist?.Count > 0)
                {
                    if (Util.CheckBlackList_blocks(gridsOB, targetGate.blocks_subtype_blacklist, out List<string> reason))
                    {
                        SendMsgToClient("Block blacklist check passed.Ship clear.", steamId);
                    }
                    else
                    {
                        SendMsgToClient("Your ship have blacklisted blocks. Jump aborted.", steamId);
                        UnlockGrid(pilotedgGridId);
                        return;
                    }
                }

                if (targetGate.items_subtype_blacklist?.Count > 0)
                {
                    if (Util.CheckBlackList_items(gridsOB, targetGate.items_subtype_blacklist, out List<string> reason))
                    {
                        SendMsgToClient("Items blacklist check passed.Ship inventory allowed.", steamId);
                    }
                    else
                    {
                        SendMsgToClient("Your ship have blacklisted items in inventory. Jump aborted.", steamId);
                        UnlockGrid(pilotedgGridId);
                        return;
                    }
                }

                if (targetGate.promote_lvl > MySession.Static.GetUserPromoteLevel(steamId))
                {
                    ToLog("Wrong role for this gate steamid=" + steamId);
                    SendMsgToClient("You do not have the correct rank to proceed through this gate...Jump aborted.", steamId);
                    UnlockGrid(pilotedgGridId);
                    return;
                }

                ToLog($"Gate config: IP/port[{target_ip}:{target_port}] color[{targetGate.gate_color}]");

                GridsBackup.SaveShipsToFile(steamId, allPCU, blockCount, ref gridsOB, PluginConfig.BackupPath, out string name, out MyObjectBuilder_Definitions bp);


                if (String.IsNullOrWhiteSpace(PluginConfig.Server_alias))
                {
                    ToLog("[Jump out] Server alias not set! jump aborted.");
                    SendMsgToClient("Cant open wormhole, SJump not configured, contact admin.", steamId);
                    UnlockGrid(pilotedgGridId);
                    return;
                }

                JumpInfo jinfo = new JumpInfo()
                { PilotSteamId = steamId, TargetGate = Colors.Red, OwnersData = ownersData, CustomData = new List<string> { "customdata kek" } };

                string APIAdress = target_API_ip + ":" + target_API_port;
                if (!Util.SendPayload(bp, jinfo, APIAdress))
                {
                    SendMsgToClient("Cant open wormhole, SJump not configured, contact admin.", steamId);
                    UnlockGrid(pilotedgGridId);
                    return;
                }

                ToLog("[Jump out] Sended! ShipToJump: '" + gridsOB[0].DisplayName + "'");
                ToLog("[Jump out] Main pilot: " + requestedplayer.DisplayName +
                " steamId: " + steamId + " Grid: " + gridsOB[0]?.DisplayName +
                " To: " + target_ip +
                ":" + target_port);
                var timer = new System.Timers.Timer(5000);
                timer.AutoReset = false;
                timer.Elapsed += (a, b) => MyAPIGateway.Utilities.InvokeOnGameThread(() =>
                {
                    try
                    {
                        if (targetGate.jump_out_msg != null && targetGate.jump_out_msg != "")
                        {
                            MyAPIGateway.Utilities.SendMessage(targetGate.jump_out_msg);
                        }

                        if (!targetGate.copy_only)
                        {
                            foreach (var real_grid in raw_grids)
                            {
                                if (real_grid != null && !real_grid.MarkedForClose && !real_grid.Closed)
                                {
                                    real_grid.Close(); //TODO create explosion and clear depart area
                                }
                            }
                            foreach (var pass in passengers)
                            {                               
                                PlayerUtils.ClearCharacters(pass);                                                               
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MainLogic.ToLog("Exception in delete jumped grid. Possible hack / Error." + ex.ToString());
                    }
                    finally
                    {
                        UnlockGrid(pilotedgGridId);
                    }
                });
                timer.Start();
                Thread.Sleep(500);
                ToLog("passengers.Count: " + passengers.Count);
                foreach (var pass in passengers)
                {
                    SendPlayerToServer((target_ip + ":" + target_port), pass);                                   
                    ToLog("[Jump out] Passenger: " + " steamId: " + pass + " Grid: " + gridsOB[0]?.DisplayName +
                    " To: " + target_ip + ":" + target_port);
                }
            }
            catch (Exception e)
            {
                ToLog("DoJump Exception: " + e.ToString());
            }
            finally
            {
                UnlockGrid(pilotedgGridId);
            }
        }

        private void SetupGates()
        {
            if (PluginConfig.Gates == null) PluginConfig.Gates = new List<GateInfo>();
            if (PluginConfig.Gates.Any()) return;
            GateInfo gate_main = new GateInfo
            {
                gate_color = Colors.Black,
                to_ServerAddress = "127.0.0.1:27016",
                to_APIAdress = "127.0.0.1:27017",
                promote_lvl = MyPromoteLevel.None,
                jump_out_msg = "bye bye",
                jump_in_msg = "Incoming",
                depart_dist = 1000,
                minarrival_dist = 250,
                maxarrival_dist = 1250,
                remote_limit_check = true,
                reset_speed = true
            };
            PluginConfig.Gates = new List<GateInfo>() { gate_main };
           
        }
        private void UnlockGrid(long id)
        {
            lock (m_lockentities)
            {
                if (m_lockentities.Contains(id))
                {
                    ToLog("EntityId unlocked! = " + id);
                    m_lockentities.Remove(id);
                }
            }
        }

        private void SendPlayerToServer(string ipport, ulong psid)
        {
            if (this.IsTorch)
                ModCommunication.SendMessageTo(new JoinServerMessage(ipport), psid);
        }
        public static void ToLog(string text)
        {
            if (PluginConfig.LogEnabled)
            {
                if (PluginConfig.ColorLog)
                {
                    m_log.Warn(text);
                }
                else
                {
                    m_log.Info(text);
                }
            }
        }
    }
}
