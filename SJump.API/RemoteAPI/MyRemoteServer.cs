﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
//using VRage.Dedicated.RemoteAPI;
using NLog;
using SJump.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Torch.API;
using Torch.Managers;
using VRage;
using VRage.Utils;

namespace SJump.API
{
    public class MyRemoteServer
    {
        private class OrderedContractResolver : DefaultContractResolver
        {
            protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
            {
                return (from p in base.CreateProperties(type, memberSerialization)
                        orderby p.PropertyName
                        select p).ToList();
            }
        }

        private enum RequestResult
        {
            RequestUnhandled,
            RequestProcessed,
            RequestClosed
        }
        

        private readonly Regex UrlRegex = new Regex("/(?<name>SJump)/(?<resourceType>[a-zA-Z]*)");

        private readonly string m_plainTextContentType = "text/plain";

        private readonly string m_jsonContentType = "application/json";

        private readonly int m_validRequestInSeconds = 30;

        private HashSet<string> m_usedNonce = new HashSet<string>();

        private DateTime m_lastNonceReset;

        private Thread m_serverThread;

        private HttpListener m_httpListener;

        private Dictionary<RouteAttribute, Tuple<MyRemoteAPIControllerBase, MethodInfo>> m_routingTable = new Dictionary<RouteAttribute, Tuple<MyRemoteAPIControllerBase, MethodInfo>>();

        private StringBuilder m_finalBuilder = new StringBuilder(256);

        private StringBuilder m_outputBuilder = new StringBuilder(256);

        private Stopwatch m_performanceTimer = new Stopwatch();

        private JsonSerializer m_serializer = JsonSerializer.Create(new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            ContractResolver = new OrderedContractResolver()
        });

        public IReadOnlyDictionary<RouteAttribute, Tuple<MyRemoteAPIControllerBase, MethodInfo>> RouteTable => m_routingTable;

        public int Port
        {
            get;
            private set;
        }

        public bool IsRunning
        {
            get;
            private set;
        }

        public string SecurityKey
        {
            get;
            private set;
        }

        public MyRemoteServer(int port, string key)
        {
            /* AddController(new MyAsteroidsController());
             AddController(new MyBannedPlayersController());
             AddController(new MyCharactersController());
             AddController(new MyChatController());
             AddController(new MyDedicatedController());
             AddController(new MyFloatingObjectsController());
             AddController(new MyGridsController());
             AddController(new MyKickedPlayersController());
             AddController(new MyPlanetsController());
             AddController(new MyPlayersController());
             AddController(new MyPoweredGridsController());
             AddController(new MyPromotedPlayersController());
             AddController(new MySessionController());
             AddController(new MyDiscoveryController());
             AddController(new FoogsRemoteAPI.MyServerStatistic());
              AddController(new FoogsRemoteAPI.MyDetailGetters());     
              AddController(new FoogsRemoteAPI.NewAPI());   */

            AddController(new MyDiscoveryController());
            AddController(new SJumpRoutes());
            Port = port;
            SecurityKey = key;
            m_lastNonceReset = DateTime.UtcNow;
            m_serverThread = new Thread(Listen);
            m_serverThread.IsBackground = true;
            m_serverThread.Start();
        }

        private void Listen()
        {
            m_httpListener = new HttpListener();
            m_httpListener.Prefixes.Add("http://"+MainLogic.PluginConfig.ListenAlias+":" + Port.ToString() + "/SJump/");
            try
            {                
                m_httpListener.Start();
            }
            catch (HttpListenerException)
            {
                 MainLogic.ToLog("Remote API unable to start. Run dedicated server in administrator mode or use AllowRemoteHttp.bat file. Remote limit checks will not work!");
                
                return;
            }
            catch (Exception ex2)
            {
                
                    MainLogic.ToLog(ex2.ToString());
                
                return;
            }
            MainLogic.ToLog("Remote Server Listener started. Listening on port " + Port.ToString());
            IsRunning = true;
            do
            {
                try
                {
                    HttpListenerContext context = m_httpListener.GetContext();
                    m_performanceTimer.Reset();
                    m_performanceTimer.Start();
                    Process(context);
                    m_performanceTimer.Stop();
                }
                catch (HttpListenerException)
                {
                    IsRunning = false;
                    return;
                }
                catch (Exception ex4)
                {
                    if (MyLog.Default != null)
                    {
                        MyLog.Default.Error(ex4.ToString());
                    }
                    return;
                }
            }
            while (m_httpListener.IsListening);
            MainLogic.ToLog("Shutting down remote server.");
        }

        internal void Stop()
        {
            m_httpListener.Stop();
            m_serverThread.Join();
            m_httpListener.Close();
            IsRunning = false;
        }

        private void Process(HttpListenerContext context)
        {
            string text22 = "Remote API get request: " + context.Request.RawUrl;
            MainLogic.ToLog(text22);



            Match match = UrlRegex.Match(context.Request.RawUrl);
            if (match.Groups.Count < 2)
            {
                CloseBadRequest(context);
                string text = "Remote API - Not valid URL from IP: " + context.Request.RemoteEndPoint.Address.ToString();
                MainLogic.ToLog(text);
                return;
            }
            string value = match.Groups["name"].Value;
            if (value != "SJump")
            {
                CloseBadRequest(context);
                string text2 = "Remote API - Not valid service name from IP: " + context.Request.RemoteEndPoint.Address.ToString();
                MainLogic.ToLog(text2);
                return;
            }
            bool isAuthenticated = IsAuthenticated(context);
            if (ProcessController(context, isAuthenticated) == RequestResult.RequestUnhandled)
            {
                context.Response.StatusCode = 404;
                context.Response.OutputStream.Close();
            }
        }

        private void CloseBadRequest(HttpListenerContext context)
        {
            context.Response.StatusCode = 400;
            context.Response.OutputStream.Close();
        }

        private bool IsAuthenticated(HttpListenerContext context)
        {

            return context.Request.Headers.AllKeys.Contains(SecurityKey);                
        }

        private bool IsHashCorrect(string clientHash, byte[] computedHash)
        {
            if (string.IsNullOrEmpty(clientHash))
            {
                return false;
            }
            byte[] array = Convert.FromBase64String(clientHash);
            if (array.Length != computedHash.Length)
            {
                return false;
            }
            for (int i = 0; i < array.Length; i++)
            {
                if (computedHash[i] != array[i])
                {
                    return false;
                }
            }
            return true;
        }

        public void AddController(MyRemoteAPIControllerBase controller)
        {
            MethodInfo[] methods = controller.GetType().GetMethods();
            MethodInfo[] array = methods;
            foreach (MethodInfo methodInfo in array)
            {
                IEnumerable<RouteAttribute> customAttributes = methodInfo.GetCustomAttributes<RouteAttribute>();
                if (customAttributes != null)
                {
                    foreach (RouteAttribute item in customAttributes)
                    {
                        if (!m_routingTable.ContainsKey(item))
                        {
                            m_routingTable[item] = new Tuple<MyRemoteAPIControllerBase, MethodInfo>(controller, methodInfo);
                        }
                    }
                }
            }
            controller.RemoteServer = this;
        }

        private RequestResult ProcessController(HttpListenerContext context, bool isAuthenticated)
        {
            if (!isAuthenticated)
            {
                context.Response.StatusCode = 403;
                context.Response.OutputStream.Close();
                return RequestResult.RequestClosed;
            }
            List<string> list = new List<string>();
            string input = context.Request.RawUrl.Substring("SJump".Length + 1);
            Match match = RouteAttribute.RouteMatcher.Match(input);
            while (match.Length > 0)
            {
                list.Add(match.Value);
                match = match.NextMatch();
            }
            string httpMethod = context.Request.HttpMethod;
            if (httpMethod == "OPTIONS")
            {
                StringBuilder stringBuilder = new StringBuilder("OPTIONS");
                foreach (KeyValuePair<RouteAttribute, Tuple<MyRemoteAPIControllerBase, MethodInfo>> item in m_routingTable)
                {
                    Dictionary<string, string> parameters = null;
                    if (item.Key.Matches(list, out parameters) && (!item.Key.AuthenticationRequired || isAuthenticated))
                    {
                        stringBuilder.Append(", ");
                        stringBuilder.Append(item.Key.HttpMethod);
                    }
                }
                context.Response.ContentType = "text/plain";
                context.Response.AddHeader("Allow", stringBuilder.ToString());
                context.Response.StatusCode = 200;
                context.Response.OutputStream.Close();
                return RequestResult.RequestProcessed;
            }
            foreach (KeyValuePair<RouteAttribute, Tuple<MyRemoteAPIControllerBase, MethodInfo>> item2 in m_routingTable)
            {
                Dictionary<string, string> parameters2 = null;
                if ((item2.Key.HttpMethod == httpMethod) && item2.Key.Matches(list, out parameters2))
                {
                    if (item2.Key.AuthenticationRequired && !isAuthenticated)
                    {
                        context.Response.StatusCode = 403;
                        context.Response.OutputStream.Close();
                        return RequestResult.RequestClosed;
                    }
                    HandleController(context, item2.Key, item2.Value.Item1, item2.Value.Item2, parameters2, isAuthenticated);
                    return RequestResult.RequestProcessed;
                }
            }
            return RequestResult.RequestUnhandled;
        }

        private void HandleController(HttpListenerContext context, RouteAttribute route, MyRemoteAPIControllerBase controller, MethodInfo method, Dictionary<string, string> parameters, bool isAuthenticated)
        {
            context.Response.ContentType = m_jsonContentType;
            m_finalBuilder.Clear();
            m_outputBuilder.Clear();
            StringWriter stringWriter = new StringWriter(m_finalBuilder);
            StringWriter stringWriter2 = new StringWriter(m_outputBuilder);
            JsonTextWriter jsonTextWriter = new JsonTextWriter(stringWriter);
            JsonTextWriter jsonTextWriter2 = new JsonTextWriter(stringWriter2);
            try
            {
                string requestBody = GetRequestBody(context);
                List<object> request_body_content_fromJson = new List<object>();
                ParameterInfo[] route_parameters = method.GetParameters(); 
                foreach (ParameterInfo route_parameter in route_parameters)
                {
                    if (!parameters.ContainsKey(route_parameter.Name))
                    {
                        try
                        {
                            using (StringReader reader = new StringReader(requestBody))
                            {
                                using (JsonTextReader reader2 = new JsonTextReader(reader))
                                {
                                    object item = m_serializer.Deserialize(reader2, route_parameter.ParameterType);
                                    request_body_content_fromJson.Add(item);
                                }
                            }
                        }
                        catch
                        {
                            request_body_content_fromJson.Add(route_parameter.DefaultValue);
                        }
                    }
                    else
                    {
                        request_body_content_fromJson.Add(parameters[route_parameter.Name]);
                    }
                }
                controller.SetProcessData(context, route, jsonTextWriter2, isAuthenticated,requestBody);
                HttpStatusCode statusCode = HttpStatusCode.OK;
                if (method.ReturnType != typeof(HttpStatusCode))
                {
                    if (method.ReturnType == typeof(void))
                    {
                        method.Invoke(controller, request_body_content_fromJson.ToArray());
                        context.Response.ContentType = m_plainTextContentType;
                        context.Response.StatusCode = 204;
                        context.Response.OutputStream.Flush();
                        context.Response.OutputStream.Close();
                        return;
                    }
                    if ((method.ReturnType == typeof(StringBuilder)) || (!method.ReturnType.IsClass && !method.ReturnType.IsStruct()))
                    {
                        object obj2 = method.Invoke(controller, request_body_content_fromJson.ToArray());
                        context.Response.ContentType = m_plainTextContentType;
                        context.Response.StatusCode = 200;
                        byte[] bytes = Encoding.UTF8.GetBytes(obj2.ToString());
                        context.Response.OutputStream.Write(bytes, 0, bytes.Length);
                        context.Response.OutputStream.Flush();
                        context.Response.OutputStream.Close();
                        return;
                    }
                    object value = method.Invoke(controller, request_body_content_fromJson.ToArray());
                    m_serializer.Serialize(jsonTextWriter2, value);
                }
                else
                {
                    statusCode = (HttpStatusCode)method.Invoke(controller, request_body_content_fromJson.ToArray());
                }
                jsonTextWriter.WriteStartObject();
                if (m_outputBuilder.Length > 0)
                {
                    jsonTextWriter.WritePropertyName("data");
                    jsonTextWriter.WriteRawValue(m_outputBuilder.ToString());
                }
                WriteMeta(jsonTextWriter, route.ApiVersion);
                jsonTextWriter.WriteEndObject();
                context.Response.StatusCode = (int)statusCode;
            }
            catch (Exception ex)
            {
                RemoteException ex2 = ex as RemoteException;
                if (ex2 == null && ex.InnerException is RemoteException)
                {
                    ex2 = (ex.InnerException as RemoteException);
                }
                m_finalBuilder.Clear();
                if (ex2 != null)
                {
                    context.Response.StatusCode = (int)ex2.StatusCode;
                    WriteRemoteException(jsonTextWriter, ex2, route.ApiVersion);
                }
                else
                {
                    context.Response.StatusCode = 500;
                    WriteNormalException(jsonTextWriter, ex, route.ApiVersion);
                }
            }
            finally
            {
                controller.SetProcessData(null, null, null, false,"0");
                ((IDisposable)jsonTextWriter).Dispose();
                ((IDisposable)jsonTextWriter2).Dispose();
                stringWriter.Dispose();
                stringWriter2.Dispose();
            }
            if (m_finalBuilder.Length > 0)
            {
                byte[] bytes2 = Encoding.UTF8.GetBytes(m_finalBuilder.ToString());
                context.Response.OutputStream.Write(bytes2, 0, bytes2.Length);
            }
            context.Response.OutputStream.Flush();
            context.Response.OutputStream.Close();
        }

        private string GetRequestBody(HttpListenerContext context)
        {
            if (context == null || context.Request == null)
            {
                return string.Empty;
            }
            Stream inputStream = context.Request.InputStream;
            Encoding contentEncoding = context.Request.ContentEncoding;
           
            string result = string.Empty;
            using (StreamReader streamReader = new StreamReader(inputStream, contentEncoding))
            {
                result = streamReader.ReadToEnd();
            }
            inputStream.Close();
            return result;
        }

        private void WriteRemoteException(JsonTextWriter writer, RemoteException remoteException, string apiVersion)
        {
            writer.WriteStartObject();
            writer.WritePropertyName("error");
            writer.WriteStartObject();
            writer.WritePropertyName("message");
            writer.WriteValue(remoteException.Message);
            writer.WriteEndObject();
            WriteMeta(writer, apiVersion);
            writer.WriteEndObject();
        }

        private void WriteNormalException(JsonTextWriter writer, Exception exception, string apiVersion)
        {
            writer.WriteStartObject();
            writer.WritePropertyName("error");
            writer.WriteStartObject();
            writer.WritePropertyName("message");
            writer.WriteValue("A generic server error occurred.");
            writer.WriteEndObject();
            WriteMeta(writer, apiVersion);
            writer.WriteEndObject();
        }

        private void WriteMeta(JsonTextWriter writer, string apiVersion)
        {
            writer.WritePropertyName("meta");
            writer.WriteStartObject();
            writer.WritePropertyName("apiVersion");
            writer.WriteValue(apiVersion);
            writer.WritePropertyName("queryTime");
            writer.WriteValue(m_performanceTimer.Elapsed.TotalMilliseconds);
            writer.WriteEndObject();
        }
    }

    public class MyDiscoveryController : MyRemoteAPIControllerBase
    {
        // Token: 0x0600014F RID: 335 RVA: 0x00012A60 File Offset: 0x00010C60
        [Route(MyHttpMethods.GET, "/SJump/api", "1.0", false)]
        [Description("Method that returns the list of all valid routes.")]
        public MyDiscoveryController.DiscoveryResponse GetDiscovery()
        {
            MyDiscoveryController.DiscoveryResponse discoveryResponse = new MyDiscoveryController.DiscoveryResponse();
            string text = base.Context.Request.Url.ToString();
            string str = text.Substring(0, text.Length - 4);
            discoveryResponse.Routes = new List<MyDiscoveryController.DiscoveryResponse.RouteDescriptor>();
            foreach (KeyValuePair<RouteAttribute, Tuple<MyRemoteAPIControllerBase, MethodInfo>> keyValuePair in base.RemoteServer.RouteTable)
            {
                RouteAttribute key = keyValuePair.Key;
                MethodInfo item = keyValuePair.Value.Item2;
                if (!key.AuthenticationRequired || base.IsAuthenticated)
                {
                    MyDiscoveryController.DiscoveryResponse.RouteDescriptor routeDescriptor = new MyDiscoveryController.DiscoveryResponse.RouteDescriptor();
                    routeDescriptor.HttpMethod = key.HttpMethod;
                    routeDescriptor.Path = str + key.Route;
                    routeDescriptor.APIVersion = key.ApiVersion;
                    routeDescriptor.AuthenticationRequired = key.AuthenticationRequired;
                    DescriptionAttribute customAttribute = item.GetCustomAttribute<DescriptionAttribute>();
                    if (customAttribute != null)
                    {
                        routeDescriptor.Description = customAttribute.Description;
                    }
                    discoveryResponse.Routes.Add(routeDescriptor);
                }
            }
            return discoveryResponse;
        }

        // Token: 0x0200001D RID: 29
        public class DiscoveryResponse
        {
            // Token: 0x04000149 RID: 329
            [Description("A list of route descriptors.")]
            public List<MyDiscoveryController.DiscoveryResponse.RouteDescriptor> Routes;

            // Token: 0x0200001E RID: 30
            public class RouteDescriptor
            {
                // Token: 0x0400014A RID: 330
                [Description("HttpMethod for this route.")]
                public string HttpMethod;

                // Token: 0x0400014B RID: 331
                [Description("The path of this route.")]
                public string Path;

                // Token: 0x0400014C RID: 332
                [Description("A short description of what this route does.")]
                public string Description;

                // Token: 0x0400014D RID: 333
                [Description("The API version this route adheres to.")]
                public string APIVersion;

                // Token: 0x0400014E RID: 334
                [Description("Does this route require authentication to be accessed?")]
                public bool AuthenticationRequired;
            }
        }
    }
}
