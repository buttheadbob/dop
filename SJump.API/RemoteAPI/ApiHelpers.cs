﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Serialization;

namespace SJump.API
{ 
    public enum MyHttpMethods
    {
        // Token: 0x04000150 RID: 336
        GET,
        // Token: 0x04000151 RID: 337
        PUT,
        // Token: 0x04000152 RID: 338
        PATCH,
        // Token: 0x04000153 RID: 339
        POST,
        // Token: 0x04000154 RID: 340
        DELETE,
        // Token: 0x04000155 RID: 341
        OPTIONS
    }
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class RouteAttribute : Attribute, IComparable<RouteAttribute>
    {
        public static readonly Regex RouteMatcher = new Regex("(/([^/]*))");

        public string HttpMethod
        {
            get;
            private set;
        }

        public string Route
        {
            get;
            private set;
        }

        public string ApiVersion
        {
            get;
            private set;
        }

        public bool AuthenticationRequired
        {
            get;
            private set;
        }

        private List<string> RouteParts
        {
            get;
            set;
        }

        public RouteAttribute(MyHttpMethods httpMethod, string route, string apiVersion, bool authenticationRequired = true)
        {
            HttpMethod = httpMethod.ToString();
            Route = route;
            ApiVersion = apiVersion;
            AuthenticationRequired = authenticationRequired;
            RouteParts = new List<string>();
            Match match = RouteMatcher.Match(route);
            while (match.Length > 0)
            {
                RouteParts.Add(match.Value);
                match = match.NextMatch();
            }
        }

        public bool Matches(List<string> url, out Dictionary<string, string> parameters)
        {
            parameters = null;
            if (RouteParts == null)
            {
                return false;
            }
            if (url.Count != RouteParts.Count)
            {
                return false;
            }
            parameters = new Dictionary<string, string>();
            for (int i = 0; i < RouteParts.Count; i++)
            {
                string text = RouteParts[i];
                string text2 = url[i];
                if (text.StartsWith("/{") && text.EndsWith("}"))
                {
                    string key = text.Substring(2, text.Length - 3);
                    parameters[key] = text2.Substring(1);
                    continue;
                }
                string value = text2;
                if (text2.Contains("?"))
                {
                    value = text2.Substring(0, text2.IndexOf('?'));
                }
                if (!text.Equals(value, StringComparison.InvariantCultureIgnoreCase))
                {
                    return false;
                }
            }
            return true;
        }

        public int CompareTo(RouteAttribute other)
        {
            if (other.Route == Route)
            {
                return 0;
            }
            return other.Route.CompareTo(Route);
        }

        public override int GetHashCode()
        {
            string text = HttpMethod + ":";
            foreach (string routePart in RouteParts)
            {
                text = ((!routePart.EndsWith("}")) ? (text + routePart) : (text + "/"));
            }
            return text.GetHashCode();
        }

        public override string ToString()
        {
            return HttpMethod + ": " + Route;
        }
    }
    public class RemoteException : Exception
    {
        // Token: 0x1700002E RID: 46
        // (get) Token: 0x06000188 RID: 392 RVA: 0x00015209 File Offset: 0x00013409
        // (set) Token: 0x06000189 RID: 393 RVA: 0x00015211 File Offset: 0x00013411
        public HttpStatusCode StatusCode { get; private set; }

        // Token: 0x0600018A RID: 394 RVA: 0x0001521A File Offset: 0x0001341A
        public RemoteException(HttpStatusCode statusCode) : base(statusCode.ToString())
        {
            this.StatusCode = statusCode;
        }

        // Token: 0x0600018B RID: 395 RVA: 0x00015234 File Offset: 0x00013434
        public RemoteException(HttpStatusCode statusCode, string messageFormat, params object[] parameters) : base(string.Format(messageFormat, parameters))
        {
            this.StatusCode = statusCode;
        }
    }
    public class MyRemoteAPIControllerBase
    {
        private class OrderedContractResolver : DefaultContractResolver
        {
            protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
            {
                return (from p in base.CreateProperties(type, memberSerialization)
                        orderby p.PropertyName
                        select p).ToList();
            }
        }

        private JsonSerializer m_serializer = JsonSerializer.Create(new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            ContractResolver = new OrderedContractResolver()
        });

        internal HttpListenerContext Context
        {
            get;
            set;
        }
        internal string Req_body
        {
            get;
            set;
        }

        protected JsonWriter Writer
        {
            get;
            private set;
        }

        protected bool IsAuthenticated
        {
            get;
            private set;
        }

        protected RouteAttribute ActiveRoute
        {
            get;
            private set;
        }

        protected JsonSerializer Serializer => m_serializer;

        public MyRemoteServer RemoteServer
        {
            get;
            set;
        }

        internal void SetProcessData(HttpListenerContext context, RouteAttribute route, JsonTextWriter writer, bool isAuthenticated,string reqbodtstring)
        {
            Context = context;
            ActiveRoute = route;
            Writer = writer;
            IsAuthenticated = isAuthenticated;
            Req_body = reqbodtstring;
        }

        protected string GetQueryString(string value)
        {
            if (Context == null || Context.Request == null)
            {
                return string.Empty;
            }
            return Context.Request.QueryString.Get(value);
        }

        protected string GetRequestBody()
        {
            if (Context == null || Context.Request == null)
            {
                return string.Empty;
            }
            Stream inputStream = Context.Request.InputStream;
            Encoding contentEncoding = Context.Request.ContentEncoding;
            string empty = string.Empty;
            using (StreamReader streamReader = new StreamReader(inputStream, contentEncoding))
            {
                return streamReader.ReadToEnd();
            }
        }
    }
}
