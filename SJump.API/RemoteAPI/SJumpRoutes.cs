﻿using Sandbox;
using Sandbox.Engine.Multiplayer;
using Sandbox.Game.Gui;
using Sandbox.Game.World;
using SJump.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Torch.Utils;

namespace SJump.API
{
    internal class SJumpRoutes : MyRemoteAPIControllerBase
    {
        [Description("Check if grid fits player limit.")]
        [Route(MyHttpMethods.POST, "/ShipDock/{steamid}", "1.0", true)]
        public HttpStatusCode GetShip(string steamid)
        {
            if (MySession.Static == null || !MySession.Static.Ready || MyMultiplayer.Static == null)
            {
                return HttpStatusCode.NoContent;
            }

            if (string.IsNullOrEmpty(steamid))
            {
                MainLogic.ToLog("steamid == nullll");
                return HttpStatusCode.BadRequest;
            }

            ulong steamIdValue;

            if (!ulong.TryParse(steamid, out steamIdValue))
            {
                MainLogic.ToLog("steamid == null!!");
                return HttpStatusCode.BadRequest;
            }

            if (this.Req_body == null)
            {
                MainLogic.ToLog("Req_body == null!!");
                return HttpStatusCode.BadRequest;
            }



            if (Util.RecievePayLoad(Req_body))
            {
                return HttpStatusCode.OK;
            }


            return HttpStatusCode.Forbidden;
        }

        [Description("Check if grid fits player limit.")]
        [Route(MyHttpMethods.POST, "/LimitCheck/{steamid}", "1.0", true)]
        public HttpStatusCode CheckLimits(string steamid)
        {
            if (MySession.Static == null || !MySession.Static.Ready || MyMultiplayer.Static == null)
            {
                return HttpStatusCode.NoContent;
            }

            if (string.IsNullOrEmpty(steamid))
            {
                return HttpStatusCode.BadRequest;
            }

            ulong steamIdValue;

            if (!ulong.TryParse(steamid, out steamIdValue))
            {
                return HttpStatusCode.BadRequest;
            }

            if (this.Req_body == null)
            {
                MainLogic.ToLog("Req_body == null!!");
            }

            if (RemoteLimitsAPI.CheckLimitsRecieveRemotePacket(this.Req_body))
            {

                return HttpStatusCode.OK;
            }
            else
            {
                return HttpStatusCode.Forbidden;
            }
        }

        [Description("Check api status.")]
        [Route(MyHttpMethods.GET, "/status", "1.0", true)]
        public HttpStatusCode CheckStatus()
        {
            if (MySession.Static == null || !MySession.Static.Ready || MyMultiplayer.Static == null)
            {
                return HttpStatusCode.NoContent;
            }
            return HttpStatusCode.OK;
        }

        [Description("Check free slots.")]
        [Route(MyHttpMethods.GET, "/FreeSlots", "1.0", true)]
        public int CheckSlots()
        {
            if (MySession.Static == null || !MySession.Static.Ready || MyMultiplayer.Static == null)
            {
                return 0;
            }
            return MySession.Static.MaxPlayers - MySession.Static.Players.GetOnlinePlayers().Count;
        }


        [Description("Check restart soon.")]
        [Route(MyHttpMethods.GET, "/CheckRestart", "1.0", true)]
        public int CheckRestart()
        {
            if (MySession.Static == null || !MySession.Static.Ready || MyMultiplayer.Static == null)
            {
                return 0;
            }
            /*
            if (Patch.IsRestartSoon(10))
            {
                return 0;
            }*/
            //TODO ???
            return 1;
        }
    }
}
