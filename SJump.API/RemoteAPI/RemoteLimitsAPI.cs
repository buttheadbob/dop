﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.BZip2;
using Newtonsoft.Json;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.World;
using VRage.Game;
using VRage.Game.ModAPI;

namespace SJump.API
{    
        public static class RemoteLimitsAPI
        {//version 05.05.2019 serverlink

            private static readonly HttpClient client = new HttpClient();
            public static bool CheckLimits_future(MyObjectBuilder_CubeGrid[] grids)
            {
                int FinalBlocksCount = 0;
                int FinalBlocksPCU = 0;
                bool flag = true;
                Dictionary<long, Dictionary<string, int>> BlocksAndOwnerForLimits = new Dictionary<long, Dictionary<string, int>>();

                foreach (var myCubeGrid in grids)
                {
                    foreach (var slimblock in myCubeGrid.CubeBlocks)
                    {

                        MyDefinitionId defId = new MyDefinitionId(slimblock.TypeId, slimblock.SubtypeId);
                        MyCubeBlockDefinition myCubeBlockDefinition;
                        if (MyDefinitionManager.Static.TryGetCubeBlockDefinition(defId, out myCubeBlockDefinition))
                        {
                            string blockPairName = myCubeBlockDefinition.BlockPairName;
                            long blockowner = 0;

                            blockowner = slimblock.BuiltBy;

                            // else
                            // { Кины пидоры лимиты считаються по билдеру
                            // blockowner = slimblock.OwnerId;
                            //}
                            if (BlocksAndOwnerForLimits.ContainsKey(blockowner))
                            {
                                var dictforuser = BlocksAndOwnerForLimits[blockowner];
                                if (dictforuser.ContainsKey(blockPairName))
                                {
                                    Dictionary<string, int> blocksPerType;
                                    string key;
                                    (blocksPerType = dictforuser)[key = blockPairName] = blocksPerType[key] + 1;
                                }
                                else
                                {
                                    dictforuser.Add(blockPairName, 1);
                                }
                                BlocksAndOwnerForLimits[blockowner] = dictforuser;
                            }
                            else
                            {
                                BlocksAndOwnerForLimits.Add(blockowner, new Dictionary<string, int>
{{ blockPairName, 1 }});
                            }
                            FinalBlocksPCU += myCubeBlockDefinition.PCU; //HACK
                        }
                    }
                    if (MySession.Static.MaxGridSize != 0)
                    {
                        flag &= (myCubeGrid.CubeBlocks.Count <= MySession.Static.MaxGridSize);
                    }
                    FinalBlocksCount += myCubeGrid.CubeBlocks.Count;
                }
                bool result = true;
                foreach (var player in BlocksAndOwnerForLimits)
                {
                    result &= CheckLimitsInternal(true, player.Key, null, FinalBlocksPCU, FinalBlocksCount, flag ? 0 : (MySession.Static.MaxGridSize + 1), player.Value);
                }
                return result;
            }



            public static bool CheckLimits_current(HashSet<IMyCubeGrid> grids)
            {
                int FinalBlocksCount = 0;
                int FinalBlocksPCU = 0;
                bool flag = true;
                Dictionary<long, Dictionary<string, int>> BlocksAndOwnerForLimits = new Dictionary<long, Dictionary<string, int>>();

                foreach (MyCubeGrid myCubeGrid in grids)
                {
                    foreach (MySlimBlock slimblock in myCubeGrid.GetBlocks())
                    {
                        MyDefinitionId defId = new MyDefinitionId(slimblock.BlockDefinition.Id.TypeId, slimblock.BlockDefinition.Id.SubtypeId);
                        MyCubeBlockDefinition myCubeBlockDefinition;
                        if (MyDefinitionManager.Static.TryGetCubeBlockDefinition(defId, out myCubeBlockDefinition))
                        {
                            string blockPairName = myCubeBlockDefinition.BlockPairName;
                            long blockowner = 0;
                            if (slimblock.FatBlock == null)
                            {
                                blockowner = slimblock.BuiltBy;
                            }
                            else
                            {
                                blockowner = slimblock.OwnerId;
                            }
                            if (BlocksAndOwnerForLimits.ContainsKey(blockowner))
                            {
                                var dictforuser = BlocksAndOwnerForLimits[blockowner];
                                if (dictforuser.ContainsKey(blockPairName))
                                {
                                    Dictionary<string, int> blocksPerType;
                                    string key;
                                    (blocksPerType = dictforuser)[key = blockPairName] = blocksPerType[key] + 1;
                                }
                                else
                                {
                                    dictforuser.Add(blockPairName, 1);
                                }
                                BlocksAndOwnerForLimits[blockowner] = dictforuser;
                            }
                            else
                            {
                                BlocksAndOwnerForLimits.Add(blockowner, new Dictionary<string, int>
{{ blockPairName, 1 }});
                            }
                        }
                    }
                    if (MySession.Static.MaxGridSize != 0)
                    {
                        flag &= (myCubeGrid.BlocksCount <= MySession.Static.MaxGridSize);
                    }
                    FinalBlocksCount += myCubeGrid.BlocksCount;
                    FinalBlocksPCU += myCubeGrid.BlocksPCU;
                }
                bool result = true;
                foreach (var player in BlocksAndOwnerForLimits)
                {
                    result &= CheckLimitsInternal(false, player.Key, null, FinalBlocksPCU, FinalBlocksCount, flag ? 0 : (MySession.Static.MaxGridSize + 1), player.Value);
                }
                return result;
            }

            /// <summary>
            /// Посылает данные с лимитами для проверки на удалённом сервере, получает ответ.
            /// </summary>
            /// <returns></returns>
            public static bool CheckLimits_request_remote(MyObjectBuilder_CubeGrid[] grids, string url)
            {
                int FinalBlocksCount = 0;
                int FinalBlocksPCU = 0;
                bool flag = true;
                Dictionary<long, Dictionary<string, int>> BlocksAndOwnerForLimits = new Dictionary<long, Dictionary<string, int>>();

                foreach (var myCubeGrid in grids)
                {
                    foreach (var slimblock in myCubeGrid.CubeBlocks)
                    {

                        MyDefinitionId defId = new MyDefinitionId(slimblock.TypeId, slimblock.SubtypeId);
                        MyCubeBlockDefinition myCubeBlockDefinition;
                        if (MyDefinitionManager.Static.TryGetCubeBlockDefinition(defId, out myCubeBlockDefinition))
                        {
                            string blockPairName = myCubeBlockDefinition.BlockPairName;
                            long blockowner = 0;

                            blockowner = slimblock.BuiltBy;

                            // else
                            // { Кины пидоры лимиты считаються по билдеру
                            // blockowner = slimblock.OwnerId;
                            //}
                            if (BlocksAndOwnerForLimits.ContainsKey(blockowner))
                            {
                                var dictforuser = BlocksAndOwnerForLimits[blockowner];
                                if (dictforuser.ContainsKey(blockPairName))
                                {
                                    Dictionary<string, int> blocksPerType;
                                    string key;
                                    (blocksPerType = dictforuser)[key = blockPairName] = blocksPerType[key] + 1;
                                }
                                else
                                {
                                    dictforuser.Add(blockPairName, 1);
                                }
                                BlocksAndOwnerForLimits[blockowner] = dictforuser;
                            }
                            else
                            {
                                BlocksAndOwnerForLimits.Add(blockowner, new Dictionary<string, int>
{{ blockPairName, 1 }});
                            }
                            FinalBlocksPCU += myCubeBlockDefinition.PCU; //HACK
                        }
                    }
                    if (MySession.Static.MaxGridSize != 0)
                    {
                        flag &= (myCubeGrid.CubeBlocks.Count <= MySession.Static.MaxGridSize);
                    }
                    FinalBlocksCount += myCubeGrid.CubeBlocks.Count;
                }

                //
                var BlocksAndOwnerForLimits_ = new RemoteLimitsPacked { BlocksAndOwnerForLimits = BlocksAndOwnerForLimits, FinalBlocksPCU = FinalBlocksPCU, FinalBlocksCount = FinalBlocksCount, blocksCount = flag ? 0 : (MySession.Static.MaxGridSize + 1) };
                string BlocksAndOwnerForLimits_jsoned = JsonConvert.SerializeObject(BlocksAndOwnerForLimits_, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto,
                    TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full
                });

                byte[] BlocksAndOwnerForLimits_jsoned_packed;
                string totalStr = BlocksAndOwnerForLimits_jsoned;
                using (MemoryStream source = new MemoryStream(Encoding.UTF8.GetBytes(totalStr)))
                {
                    using (MemoryStream target = new MemoryStream())
                    {

                        BZip2.Compress(source, target, true, 4096);
                        BlocksAndOwnerForLimits_jsoned_packed = target.ToArray();
                        // largeCompressedText = Convert.ToBase64String(targetByteArray);
                    }
                }
                var content = new StringContent(Convert.ToBase64String(BlocksAndOwnerForLimits_jsoned_packed));
                // var content = new ByteArrayContent(BlocksAndOwnerForLimits_jsoned_packed);

                var req = new HttpRequestMessage(HttpMethod.Post, url);

                req.Content = content;
                req.Headers.Add(MainLogic.PluginConfig.RemoteApiKey, MainLogic.PluginConfig.RemoteApiKey);
                var sended_req = client.SendAsync(req);

                sended_req.Wait();
                // var x = client.PostAsync(url, content);

                var result1 = sended_req.Result.Content.ReadAsStringAsync();
                result1.Wait();
                string res = result1.Result;
                //send req

                //foreach (var player in BlocksAndOwnerForLimits)
                //{
                // result &= CheckLimitsInternal(true, player.Key, null, FinalBlocksPCU, FinalBlocksCount, flag ? 0 : (MySession.Static.MaxGridSize + 1), player.Value);
                //}
                MainLogic.ToLog("CheckLimits_request_remote url = " + url + " result string = " + res + sended_req.Result.StatusCode.ToString());
                return sended_req.Result.StatusCode == HttpStatusCode.OK;
                //return true;
            }
            public static bool CheckLimitsRecieveRemotePacket(string rawdata)
            {
                try
                {
                    string BlocksAndOwnerForLimits_jsoned;
                    byte[] BlocksAndOwnerForLimits_jsoned_packed = Convert.FromBase64String(rawdata);


                    using (MemoryStream source = new MemoryStream(BlocksAndOwnerForLimits_jsoned_packed))
                    {
                        using (MemoryStream target = new MemoryStream())
                        {
                            BZip2.Decompress(source, target, true);
                            BlocksAndOwnerForLimits_jsoned = Encoding.UTF8.GetString(target.ToArray());
                        }
                    }

                    var BlocksAndOwnerForLimits = JsonConvert.DeserializeObject<RemoteLimitsPacked>(BlocksAndOwnerForLimits_jsoned, new JsonSerializerSettings
                    {
                        TypeNameHandling = TypeNameHandling.Auto,
                        TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full
                    });

                    bool result = true;
                    foreach (var player in BlocksAndOwnerForLimits.BlocksAndOwnerForLimits)
                    {
                        var tmp = CheckLimitsInternal(true, player.Key, null, BlocksAndOwnerForLimits.FinalBlocksPCU, BlocksAndOwnerForLimits.FinalBlocksCount, BlocksAndOwnerForLimits.blocksCount, player.Value);
                        MainLogic.ToLog("CheckLimitsRecieveRemotePacket result for " + player.Key + "tmp=" + tmp);

                        result &= tmp;
                    }
                    return result;

                }
                catch (Exception ex)
                {
                    MainLogic.ToLog("CheckLimitsRecieveRemotePacket EXEPTION = " + ex.ToString());
                    return false;
                }
            }

            [JsonObject]
            public struct RemoteLimitsPacked
            {
                public int FinalBlocksPCU;
                public int FinalBlocksCount;
                public int blocksCount;
                public Dictionary<long, Dictionary<string, int>> BlocksAndOwnerForLimits;
            }
            private static bool CheckLimitsInternal(bool future_spawn, long ownerID, string blockName, int pcuToBuild, int blocksToBuild = 0, int blocksCount = 0, Dictionary<string, int> blocksPerType = null)
            {
                string text;
                MySession.LimitResult limitResult = IsFitsWorldLimitsInternal(out text, future_spawn, ownerID, blockName, pcuToBuild, blocksToBuild, blocksCount, blocksPerType);
                if (limitResult != MySession.LimitResult.Passed)
                {
                    return false;
                }
                return true;
            }

            private static MySession.LimitResult IsFitsWorldLimitsInternal(out string failedBlockType, bool future_spawn, long ownerID, string blockName, int pcuToBuild, int blocksToBuild = 0, int blocksCount = 0, Dictionary<string, int> blocksPerType_forcheck = null)
            {
                failedBlockType = null;
                if (MySession.Static.BlockLimitsEnabled == MyBlockLimitsEnabledEnum.NONE)
                {
                    return MySession.LimitResult.Passed;
                }
                MyIdentity myIdentity = MySession.Static.Players.TryGetIdentity(ownerID);
                if (MySession.Static.MaxGridSize != 0 && blocksCount + blocksToBuild > MySession.Static.MaxGridSize)
                {
                    return MySession.LimitResult.MaxGridSize;
                }
                if (myIdentity != null)
                {
                    MyBlockLimits blockLimits = myIdentity.BlockLimits;
                    if (MySession.Static.BlockLimitsEnabled == MyBlockLimitsEnabledEnum.PER_FACTION && MySession.Static.Factions.GetPlayerFaction(myIdentity.IdentityId) == null)
                    {
                        return MySession.LimitResult.NoFaction;
                    }
                    if (blockLimits != null)
                    {
                        if (MySession.Static.MaxBlocksPerPlayer != 0 && blockLimits.BlocksBuilt + blocksToBuild > blockLimits.MaxBlocks)
                        {
                            return MySession.LimitResult.MaxBlocksPerPlayer;
                        }
                        if (MySession.Static.TotalPCU != 0 && pcuToBuild > blockLimits.PCU)
                        {
                            return MySession.LimitResult.PCU;
                        }
                        if (blocksPerType_forcheck != null)
                        {
                            using (Dictionary<string, short>.Enumerator enumerator = MySession.Static.BlockTypeLimits.GetEnumerator())
                            {
                                while (enumerator.MoveNext())
                                {
                                    KeyValuePair<string, short> keyValuePair = enumerator.Current;
                                    if (blocksPerType_forcheck.ContainsKey(keyValuePair.Key))
                                    {
                                        int КолвоБлоковОпределленногоТипаВСтруктуре = blocksPerType_forcheck[keyValuePair.Key];
                                        MyBlockLimits.MyTypeLimitData ЛимитыИгрокаНаБлок;
                                        if (blockLimits.BlockTypeBuilt.TryGetValue(keyValuePair.Key, out ЛимитыИгрокаНаБлок))
                                        {
                                            if (future_spawn)
                                            {
                                                КолвоБлоковОпределленногоТипаВСтруктуре = КолвоБлоковОпределленногоТипаВСтруктуре + ЛимитыИгрокаНаБлок.BlocksBuilt;
                                            }
                                            else
                                            {
                                                //КолвоБлоковОпределленногоТипаВСтруктуре = ЛимитыИгрокаНаБлок.BlocksBuilt - КолвоБлоковОпределленногоТипаВСтруктуре;
                                            }
                                        }
                                        if (КолвоБлоковОпределленногоТипаВСтруктуре > MySession.Static.GetBlockTypeLimit(keyValuePair.Key))
                                        {
                                            return MySession.LimitResult.BlockTypeLimit;
                                        }
                                    }
                                }
                                return MySession.LimitResult.Passed;
                            }
                        }
                        short blockTypeLimit = MySession.Static.GetBlockTypeLimit(blockName);
                        if (blockTypeLimit > 0)
                        {
                            MyBlockLimits.MyTypeLimitData myTypeLimitData2;
                            if (blockLimits.BlockTypeBuilt.TryGetValue(blockName, out myTypeLimitData2))
                            {
                                blocksToBuild += myTypeLimitData2.BlocksBuilt;
                            }
                            if (blocksToBuild > blockTypeLimit)
                            {
                                return MySession.LimitResult.BlockTypeLimit;
                            }
                        }
                    }
                }
                return MySession.LimitResult.Passed;
            }

        }
    }
