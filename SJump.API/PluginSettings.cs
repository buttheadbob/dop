﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Torch;
using Torch.Collections;
using SJump.API;

namespace SJump.API
{
    public class PluginSettings : ViewModel
    {
        private bool _enabled;
        private bool _enable_log = true;
        private bool _colorLog = true;
        private bool _isBlockLimiter = true;
        private bool _is_keen_limits = true;
        private int _remoteAPIPort;
        private string _remoteAPIKey;
        private string _server_alias = "*";
        private string _backup_path;
        private string _listen_path;
        private List<GateInfo> _gates;

        public bool Enabled
        {
            get => _enabled;
            set { _enabled = value; OnPropertyChanged(); }
        }       

        public bool LogEnabled
        {
            get => _enable_log;
            set { _enable_log = value; OnPropertyChanged(); }
        }

        public bool ColorLog
        {
            get => _colorLog;
            set { _colorLog = value; OnPropertyChanged(); }
        }

        public bool IsBlockLimiter
        {
            get => _isBlockLimiter;
            set { _isBlockLimiter = value; OnPropertyChanged(); }
        }

        public List<GateInfo> Gates
        {
            get => _gates;
            set { _gates = value; OnPropertyChanged(); }
        }

        public int RemoteAPIPort
        {
            get => _remoteAPIPort;
            set { _remoteAPIPort = value; OnPropertyChanged(); }
        }

        public string RemoteApiKey
        {
            get => _remoteAPIKey;
            set { _remoteAPIKey = value; OnPropertyChanged(); }
        }

        public string Server_alias
        {
            get => _server_alias;
            set { _server_alias = value; OnPropertyChanged(); }
        }
        
        public string ListenAlias
        {
            get => _listen_path;
            set { _listen_path = value; OnPropertyChanged(); }
        }

        public string BackupPath
        {
            get => _backup_path;
            set { _backup_path = value; OnPropertyChanged(); }
        }
        
        public bool IsKeenLimits
        {
            get => _is_keen_limits;
            set { _is_keen_limits = value; OnPropertyChanged(); }
        }
    }
}