using System;
using System.Collections.Generic;
using System.Linq;
using NAPI;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.Screens.Helpers;
using Sandbox.Game.SessionComponents;
using Sandbox.Game.Weapons;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Scripts.Shared;
using Slime.Features;
using Torch.Managers.PatchManager;
using VRage.Game;
using VRage.Game.Entity;
using VRage.Game.ObjectBuilders.Components;
using VRageMath;

namespace Slime
{
    public class APIExtensions
    {
        public const int CALL_RESULT_NULL = 0;
        public const int CALL_RESULT_TRUE = 1;
        public const int CALL_RESULT_FALSE = 2;
        public const int NOCALL_RESULT_TRUE = 3;
        public const int NOCALL_RESULT_FALSE = 4;

        public static Func<MySafeZone, int, long, BoundingBoxD, int> APISafezone_IsActionAllowedAABB = (zone, action, arg3, arg4) => CALL_RESULT_NULL;
        public static Func<MySafeZone, int, long, Vector3, int> APISafezone_IsActionAllowedPoint = (zone, action, arg3, arg4) => CALL_RESULT_NULL;
        public static Func<MySafeZone, int, long, MyEntity, int> APISafezone_IsActionAllowedEntity = (zone, action, arg3, arg4) => CALL_RESULT_NULL;
        
        public static Func<int, ulong, long, BoundingBoxD, int> APIGlobal_IsActionAllowedAABB = (zone, action, arg3, arg4) => CALL_RESULT_NULL;
        public static Func<int, ulong, long, Vector3, int> APIGlobal_IsActionAllowedPoint = (zone, action, arg3, arg4) => CALL_RESULT_NULL;
        public static Func<int, ulong, long, MyEntity, int> APIGlobal_IsActionAllowedEntity = (zone, action, arg3, arg4) => CALL_RESULT_NULL;
        
        public static Func<Vector3, bool> CanMineHere = (position) => { return true; };
        public static Func<Vector3, bool> CanJumpHere = (position) => { return true; };
        public static Func<MyCubeGrid, bool> CanConvertToDynamic = (grid) => { return true; };
        
        public static Func<long, IMyTerminalBlock, bool> Med_CanUseSpawn = (identity, b) => { return true; };
        public static Action<long> Med_OnSpawnsRequested = (identity) => { };
        public static Action<IMyTerminalBlock, string, long, float> Med_CreateSpawn = CustomMedscreen.AddSpawn;
        
        private static Func<Dictionary<MyVoxelMaterialDefinition, int>, Vector3D, bool, bool> DrillInterceptor = (a,b,c) => { return true; };
        
        
        private static Func<long, MyDefinitionId, bool> CanUseBlock = (playerId, MyDefinitionId) =>
        {
            if (MySession.Static.ResearchEnabled && MySessionComponentResearch.Static.RequiresResearch(MyDefinitionId))
                return MySessionComponentResearch.Static.IsBlockUnlocked(playerId, MyDefinitionId);
            else
                return true;
        };

        private static Action<long, int> ChangePlayerTradedPCU = (player, extraPCU) => {
            MyIdentity myIdentity = MySession.Static.Players.TryGetIdentity(player);
            if (myIdentity != null)
            {
                myIdentity.BlockLimits.SetPCUFromServer(myIdentity.BlockLimits.PCU + extraPCU, extraPCU);
                myIdentity.BlockLimits.SetAllDirty();
            }
        };
        
        

        public static void Init(PatchContext patchContext)
        {
            ModConnection.SetValue ("MIG.APIExtender.ChangePlayerTradedPCU", ChangePlayerTradedPCU);

            ModConnection.Subscribe<Func<Vector3, bool>>("MIG.VoxelProtector.CanMineHere", (func) => { CanMineHere = func; Log.Info("MIG.VoxelProtector.CanMineHere changed"); });
            ModConnection.Subscribe<Func<Vector3, bool>>("MIG.APIExtender.CanJumpHere", (func) => { CanJumpHere = func; Log.Info("MIG.APIExtender.CanJumpHere changed"); });
            
            ModConnection.SetValue ("MIG.APIExtender.Med_CreateSpawn", Med_CreateSpawn);
            ModConnection.Subscribe<Action<long>>("MIG.APIExtender.Med_OnSpawnsRequested", (func) => { Med_OnSpawnsRequested = func; Log.Info("MIG.APIExtender.Med_OnSpawnsRequested changed"); });
            ModConnection.Subscribe<Func<long, IMyTerminalBlock, bool>>("MIG.APIExtender.Med_CanUseSpawn", (func) => { Med_CanUseSpawn = func; Log.Info("MIG.APIExtender.Med_CanUseSpawn changed"); });
            
            if (APIExtender.Config.APIDrillInterceptor)
            {
                patchContext.Prefix(typeof(MyDrillBase), typeof(APIExtensions), "OnDrillResults");
                ModConnection.Subscribe<Func<Dictionary<MyVoxelMaterialDefinition, int>, Vector3D, bool, bool>>("MIG.APIExtender.DrillInterceptor", (func) =>
                {
                    DrillInterceptor = func; 
                    Log.Info("MIG.APIExtender.DrillInterceptor changed");
                });
            }

            
            if (APIExtender.Config.APIResearchCanUseBlock)
            {
                patchContext.Prefix(typeof(MySessionComponentResearch), typeof(APIExtensions), "CanUse", new[] {"identityId", "id"});
                ModConnection.Subscribe<Func<long, MyDefinitionId, bool>>("MIG.APIExtender.CanUseBlock", (func) =>
                {
                    CanUseBlock = func; 
                    Log.Info("MIG.APIExtender.CanUseBlock changed");
                });
            }



            if (APIExtender.Config.APICanConvertToDynamic)
            {
                patchContext.Prefix(typeof(MyCubeGrid), typeof(APIExtensions), "OnConvertToDynamic");
                ModConnection.Subscribe<Func<MyCubeGrid, bool>>("MIG.APIExtender.CanConvertToDynamic", (func) =>
                {
                    CanConvertToDynamic = func; 
                    Log.Info("MIG.APIExtender.CanConvertToDynamic changed");
                });
            }
            
            if (APIExtender.Config.APISafezonesIsActionAllowed)
            {
                ModConnection.Subscribe<Func<MySafeZone, int, long, BoundingBoxD, int>>("MIG.APIExtender.APISafezone_IsActionAllowedAABB", (func) =>
                {
                    APISafezone_IsActionAllowedAABB = func; 
                    Log.Info("MIG.APIExtender.APISafezone_IsActionAllowedAABB changed");
                });
            
                ModConnection.Subscribe<Func<MySafeZone, int, long, Vector3, int>>("MIG.APIExtender.APISafezone_IsActionAllowedPoint", (func) =>
                {
                    APISafezone_IsActionAllowedPoint = func; 
                    Log.Info("MIG.APIExtender.APISafezone_IsActionAllowedPoint changed");
                });
            
                ModConnection.Subscribe<Func<MySafeZone, int, long, MyEntity, int>>("MIG.APIExtender.APISafezone_IsActionAllowedEntity", (func) =>
                {
                    APISafezone_IsActionAllowedEntity = func; 
                    Log.Info("MIG.APIExtender.APISafezone_IsActionAllowedEntity changed");
                });
            
            
                ModConnection.Subscribe<Func<int, ulong, long, BoundingBoxD, int>>("MIG.APIExtender.APIGlobal_IsActionAllowedAABB", (func) =>
                {
                    APIGlobal_IsActionAllowedAABB = func; 
                    Log.Info("MIG.APIExtender.APIGlobal_IsActionAllowedAABB changed");
                });
            
                ModConnection.Subscribe<Func<int, ulong, long, Vector3, int>>("MIG.APIExtender.APIGlobal_IsActionAllowedPoint", (func) =>
                {
                    APIGlobal_IsActionAllowedPoint = func; 
                    Log.Info("MIG.APIExtender.APIGlobal_IsActionAllowedPoint changed");
                });
            
                ModConnection.Subscribe<Func<int, ulong, long, MyEntity, int>>("MIG.APIExtender.APIGlobal_IsActionAllowedEntity", (func) =>
                {
                    APIGlobal_IsActionAllowedEntity = func; 
                    Log.Info("MIG.APIExtender.APIGlobal_IsActionAllowedEntity changed");
                });
            }
        }
        
        private static bool OnDrillResults(MyDrillBase __instance, Dictionary<MyVoxelMaterialDefinition, int> materials, Vector3D hitPosition, bool collectOre, Action<bool> OnDrillingPerformed)
        {
            return DrillInterceptor.Invoke(materials, hitPosition, collectOre);
        }
        
        private static bool CanUse (MySessionComponentResearch __instance, ref bool __result, long identityId, MyDefinitionId id)
        {
            __result = CanUseBlock(identityId, id);
            return false;
        }
        
        private static bool OnConvertToDynamic (MyCubeGrid __instance)
        {
            return CanConvertToDynamic(__instance);
        }

        public static bool HandleAPIAnswer(int answer, ref bool __result)
        {
            switch (answer)
            {
                case CALL_RESULT_NULL:
                    return true;
                case CALL_RESULT_TRUE:
                    __result = true;
                    return true;
                case CALL_RESULT_FALSE:
                    __result = false;
                    return true;
                case NOCALL_RESULT_TRUE:
                    __result = true;
                    return false;
                case NOCALL_RESULT_FALSE:
                    __result = true;
                    return false;
            }

            return true;
        }
    }
}