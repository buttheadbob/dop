using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Slime.GUI;
using Torch;

namespace Slime.Config
{

    public partial class CoreConfig : ViewModel
    {
        private bool m_apiResearchCanUseBlock = false;
        private bool m_canConvertToDynamic = false;
        private bool m_APISafezonesIsActionAllowed = false;
        private bool m_apiDrillInterceptor = false;
        
        [DisplayTab(Name = "Research:CanUseBlock", GroupName = "Features", Tab = "APIUnlocks", Order = 0, Description = "[API] Used for mods, being able to manipulate researches")]
        public bool APIResearchCanUseBlock { get => m_apiResearchCanUseBlock; set => SetValue(ref m_apiResearchCanUseBlock, value); }
        
        [DisplayTab(Name = "Grid:CanConvertToDynamic", GroupName = "Features", Tab = "APIUnlocks", Order = 0, Description = "[API] Used for mods, being able to block convert to dynamic")]
        public bool APICanConvertToDynamic { get => m_canConvertToDynamic; set => SetValue(ref m_canConvertToDynamic, value); }
        
        [DisplayTab(Name = "MyShipDrill:OnDrillResults", GroupName = "Features", Tab = "APIUnlocks", Order = 0, Description = "[API] Used for mods, being able to block convert to dynamic")]
        public bool APIDrillInterceptor { get => m_apiDrillInterceptor; set => SetValue(ref m_apiDrillInterceptor, value); }

        [DisplayTab(Name = "Safezones:IsActionAllowed", GroupName = "Features", Tab = "APIUnlocks", Order = 0, Description = "[API] Used for mods, being able to work with permessions")]
        public bool APISafezonesIsActionAllowed { get => m_APISafezonesIsActionAllowed; set => SetValue(ref m_APISafezonesIsActionAllowed, value); }
    }
    
    public partial class CoreConfig : ViewModel
    {
        private bool m_RemoteDeletingEnabled = false;
        private string m_RemoteDeletingEnemyText = "";
        private float m_RemoteDeletingRange = 5000;


        [DisplayTab(Name = "Enabled", GroupName = "RemoteDeleting", Tab = "RemoteDeleting", Order = 0)]
        public bool RemoteDeletingEnabled { get => m_RemoteDeletingEnabled; set => SetValue(ref m_RemoteDeletingEnabled, value); }
        
        [DisplayTab(Name = "Range", GroupName = "RemoteDeleting", Tab = "RemoteDeleting", Order = 0, Description = "Range in which remote delete checks for enemies")]
        public float RemoteDeletingRange { get => m_RemoteDeletingRange; set => SetValue(ref m_RemoteDeletingRange, value); }
        
        [DisplayTab(Name = "Enemy Text", GroupName = "RemoteDeleting", Tab = "RemoteDeleting", Order = 0, Description = "Appears to player, when an enemy is near, and grid deletion is blocked")]
        public string RemoteDeletingEnemyText { get => m_RemoteDeletingEnemyText; set => SetValue(ref m_RemoteDeletingEnemyText, value); }
    }

    public partial class CoreConfig : ViewModel
    {
        private bool _allowDamage = false;
        private bool _allowBuilding = false;
        private bool _allowDrilling = false;
        private bool _allowWelding = false;
        private bool _allowGrinding = false;
        private bool _allowVoxelHand = false;
        private bool _allowLandingGearLock = false;
        private bool _allowShooting = false;
        
        [DisplayTab(Name = "Allow damage", GroupName = "Global safe zones permissions override", Order = 0, Description = "Able to ignore individual safezones permissions")]
        public bool SafeZoneAllowDamage { get => _allowDamage; set => SetValue(ref _allowDamage, value); }

        [DisplayTab(Name = "Allow building", GroupName = "Global safe zones permissions override", Order = 0, Description = "Able to ignore individual safezones permissions")]
        public bool SafeZoneAllowBuilding { get => _allowBuilding; set => SetValue(ref _allowBuilding, value); }

        [DisplayTab(Name = "Allow drilling", GroupName = "Global safe zones permissions override", Order = 0, Description = "Able to ignore individual safezones permissions")]
        public bool SafeZoneAllowDrilling { get => _allowDrilling; set => SetValue(ref _allowDrilling, value); }

        [DisplayTab(Name = "Allow welding", GroupName = "Global safe zones permissions override", Order = 0, Description = "Able to ignore individual safezones permissions")]
        public bool SafeZoneAllowWelding { get => _allowWelding; set => SetValue(ref _allowWelding, value); }

        [DisplayTab(Name = "Allow grinding", GroupName = "Global safe zones permissions override", Order = 0, Description = "Able to ignore individual safezones permissions")]
        public bool SafeZoneAllowGrinding { get => _allowGrinding; set => SetValue(ref _allowGrinding, value); }

        [DisplayTab(Name = "Allow voxel hand", GroupName = "Global safe zones permissions override", Order = 0, Description = "Able to ignore individual safezones permissions")]
        public bool SafeZoneAllowVoxelHand { get => _allowVoxelHand; set => SetValue(ref _allowVoxelHand, value); }

        [DisplayTab(Name = "Allow landing gear locking", GroupName = "Global safe zones permissions override", Order = 0, Description = "Able to ignore individual safezones permissions")]
        public bool SafeZoneAllowLandingGearLock { get => _allowLandingGearLock; set => SetValue(ref _allowLandingGearLock, value); }

        [DisplayTab(Name = "Allow shooting", GroupName = "Global safe zones permissions override", Order = 0, Description = "Able to ignore individual safezones permissions")]
        public bool SafeZoneAllowShooting { get => _allowShooting; set => SetValue(ref _allowShooting, value); }
    }

    public partial class CoreConfig : ViewModel
    {
        private bool _protectVoxelsEnabled = false;
        private float _protectVoxelsFromDrilling = -1;
        private float _protectVoxelsFromWarheads = -1;
        private float _protectVoxelsFromCollisions = -1;
        private bool _protectVoxelsFromCollisionsGlobal = false;
        
        [DisplayTab(Name = "Enabled", GroupName = "VoxelProtection", Tab = "Voxels", Order = 0, Description = "Enables undestructable voxels near safezones.")]
        public bool ProtectVoxelsEnabled { get => _protectVoxelsEnabled; set => SetValue(ref _protectVoxelsEnabled, value); }

        [DisplayTab(Name = "From collisions GLOBALY", GroupName = "VoxelProtection", Tab = "Voxels", Order = 1, Description = "(Much faster than ProtectVoxelsFromCollisions)")]
        public bool ProtectVoxelsFromCollisionsGlobal { get => _protectVoxelsFromCollisionsGlobal; set => SetValue(ref _protectVoxelsFromCollisionsGlobal, value); }

        [DisplayTab(Name = "From collisions range", GroupName = "VoxelProtection", Tab = "Voxels", Order = 2, Description = "Range of protection from safezone")]
        public float ProtectVoxelsFromCollisions { get => _protectVoxelsFromCollisions; set => SetValue(ref _protectVoxelsFromCollisions, value); }

        [DisplayTab(Name = "From warheads range", GroupName = "VoxelProtection", Tab = "Voxels", Order = 3, Description = "Range of protection from safezone")]
        public float ProtectVoxelsFromWarheads { get => _protectVoxelsFromWarheads; set => SetValue(ref _protectVoxelsFromWarheads, value); }

        [DisplayTab(Name = "From drilling range", GroupName = "VoxelProtection", Tab = "Voxels", Order = 4, Description = "Range of protection from safezone")]
        public float ProtectVoxelsFromDrilling { get => _protectVoxelsFromDrilling; set => SetValue(ref _protectVoxelsFromDrilling, value); }

    }

    public partial class CoreConfig : ViewModel
    {
        private bool _enableMedScreen = false;
        private bool _enableMedScreenSorter = false;
        private bool _enableMedScreenFake = false;
        private bool _enableMedScreenRemover = false;
        private string _medScreenRemoverExcludedTags = "";
        
        [XmlIgnore]
        public List<string> MedScreenRemoverExcludedTagsList = new List<string>();
        
        [DisplayTab(Name = "Enabled", GroupName = "MedScreen", Order = 0, Description = "Faster join, faster spawn, sort and filter medbays.")]
        public bool EnableMedScreen { get => _enableMedScreen; set => SetValue(ref _enableMedScreen, value); }

        [DisplayTab(Name = "Sorter", GroupName = "MedScreen", Order = 1, Description = "Enable sorter feature.Sort rules: OurSpawms->")]
        public bool EnableMedScreenSorter { get => _enableMedScreenSorter; set => SetValue(ref _enableMedScreenSorter, value); }

        [DisplayTab(Name = "Fake", GroupName = "MedScreen", Order = 1, Description = "Inserts fake null spawn for faster load.")]
        public bool EnableMedScreenFake { get => _enableMedScreenFake; set => SetValue(ref _enableMedScreenFake, value); }

        [DisplayTab(Name = "Remover", GroupName = "MedScreen", Order = 2, Description = "Remove all enemy med's.")]
        public bool EnableMedScreenRemover { get => _enableMedScreenRemover; set => SetValue(ref _enableMedScreenRemover, value); }

        [DisplayTab(Name = "Remover faction tag exceptions", GroupName = "MedScreen", Order = 3, Description = "Remove all enemy med's EXEPT this tags.")]
        public string MedScreenRemoverExcludedTags
        {
            get => _medScreenRemoverExcludedTags;
            set
            {
                SetValue(ref _medScreenRemoverExcludedTags, value);
                MedScreenRemoverExcludedTagsList = _medScreenRemoverExcludedTags.Split(',').Where(x => !string.IsNullOrWhiteSpace(x) && x.Length >= 3).ToList();
            }
        }
    }

    public partial class CoreConfig : ViewModel
    {
        private bool _enableTurretsNotShootingFix = false;
        private bool _EnableDrillTreesFix = false;
        private bool _enableFallingThroughVoxelsFix = false;
        private bool _enableGasChangeThresholdFix = false;
        private int _gasChangeThreshold = 1;
        
        
        [DisplayTab(Name = "EnableTurretsNotShootingFix", GroupName = "Fixes", Tab = "Fixes", Order = 0, Description = "With this fix, turrets will always see targets")]
        public bool EnableTurretsNotShootingFix { get => _enableTurretsNotShootingFix; set => SetValue(ref _enableTurretsNotShootingFix, value); }

        [DisplayTab(Name = "EnableDrillTreesFix (No Longer can drill trees)", GroupName = "Fixes", Tab = "Fixes", Order = 0, Description = "Null pointer at at Sandbox.Game.Weapons.MyDrillBase.DrillEnvironmentSector(DetectionInfo entry, Single speedMultiplier, MyStringHash& targetMaterial)")]
        public bool EnableDrillTreesFix { get => _EnableDrillTreesFix; set => SetValue(ref _EnableDrillTreesFix, value); }

        [DisplayTab(Name = "Enable Falling through voxels Fix (Experimental)", Description = "Doesn't completly fix problem, only reduce amount of fallings | Drain simulation)", GroupName = "Fixes", Tab = "Fixes", Order = 0)]
        public bool FallingThroughVoxelsFix { get => _enableFallingThroughVoxelsFix; set => SetValue(ref _enableFallingThroughVoxelsFix, value); }

        [DisplayTab(Name = "Enable gasTank network flood disabler", GroupName = "FixGasProblem (OBSOLETE)", Order = 0)]
        public bool EnableGasChangeThresholdFix { get => _enableGasChangeThresholdFix; set => SetValue(ref _enableGasChangeThresholdFix, value); }

        [DisplayTab(Name = "GasChangeThreshold", GroupName = "FixGasProblem (OBSOLETE)", Order = 0)]
        public int GasChangeThreshold { get => _gasChangeThreshold; set => SetValue(ref _gasChangeThreshold, value); }

    }

    public partial class CoreConfig : ViewModel
    {
        private bool _enableBlueprintProtection = false;
        private bool _enablePlayerTracking = false;
        private uint _trackingFrequency = 1;

        [DisplayTab(Name = "EnableBlueprintProtection", GroupName = "Fixes", Tab = "Fixes", Order = 0, Description = "Preventing getting Items from blueprint stockpile")]
        public bool EnableBlueprintProtection { get => _enableBlueprintProtection; set => SetValue(ref _enableBlueprintProtection, value); }

        [DisplayTab(Name = "Enable", GroupName = "PlayerTracking", Order = 0)]
        public bool PlayerTracking { get => _enablePlayerTracking; set => SetValue(ref _enablePlayerTracking, value); }

        [DisplayTab(Name = "Frequency in Min", GroupName = "PlayerTracking", Order = 0)]
        public uint TrackingFrequencyMin { get => _trackingFrequency; set => SetValue(ref _trackingFrequency, value); }
    }
}