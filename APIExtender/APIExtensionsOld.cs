﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.Entities;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Torch.Managers.PatchManager;
using VRage.Collections;
using VRage.Game.ModAPI;
using VRage.Game.ObjectBuilders.Components;
using VRageMath;
using NAPI;
using Sandbox.Game.GameSystems;
using Sandbox.Game.Entities.Character;
using System.Diagnostics;

namespace Slime.Features
{
    public class APIExtensionsOld
    {

        static MethodInfo JumpRedirectMethod;
        static PropertyInfo jumpingGrid;

        public static void Init(PatchContext patchContext)
        {
            Type type;
            try
            {
                type = Ext2.getTypeByName("Slime.TorchConnection");
            }
            catch (Exception e)
            {
                Log.Error("Slime.TorchConnection not found. Skipping API intergration.");
                return;
            }

            tryConnect2(patchContext, type, nameof(SafeZoneActions), nameof(SafeZoneActions));
            tryConnect2(patchContext, type, nameof(SafeZonePlayersInGrids), nameof(SafeZonePlayersInGrids));
            tryConnect2(patchContext, type, nameof(SafeZoneFactions), nameof(SafeZoneFactions));
            
            tryConnect(patchContext, type, "CreateCharacter", new[] { "__result", "position", "velocity", "name", "model", "colormask" });
            tryConnect(patchContext, type, "SetSafezoneOptions", new[] { "zone", "radius", "factionsBlackList", "entitiesBlackList", "playersBlackList", "factions", "entities", "players", "allowedActions" });

            try
            {
                JumpRedirectMethod = type.easyMethod("AllowJumpDrive");
                jumpingGrid = typeof(MyGridJumpDriveSystem).easyProp("Grid");
                tryConnect(patchContext, typeof(MyGridJumpDriveSystem), "OnRequestJumpFromClient", new[] { "__instance", "jumpTarget", "userId" });
            }
            catch (Exception e)
            {
                Log.Error(e, "AllowJumpDrive Skipped");
            }
        }


        public static bool tryConnect(PatchContext patchContext, Type type, String name, String[] names)
        {
            MethodInfo mi1, mi2;
            try
            {
                mi1 = type.easyMethod(name);
            }
            catch (Exception e)
            {
                Log.Error("ERROR: Method in mod not found: " + name);
                return false;
            }

            try
            {
                mi2 = typeof(APIExtensionsOld).protectedMethod(names);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return false;
            }

            Log.Warn("PATCHING: " + name);
            patchContext.GetPattern(mi1).Prefixes.Add(mi2);
            return true;
        }

        //TODO Remove
        public static bool tryConnect2(PatchContext patchContext, Type type, String name, String name2)
        {
            MethodInfo mi1, mi2;
            try
            {
                mi1 = type.easyMethod(name);
            }
            catch (Exception e)
            {
                Log.Error("ERROR: Method in mod not found: " + name);
                return false;
            }

            try
            {
                mi2 = typeof(APIExtensionsOld).easyMethod(name2);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return false;
            }

            Log.Warn("PATCHING: " + name);
            patchContext.GetPattern(mi1).Prefixes.Add(mi2);
            return true;
        }


        public static bool CreateCharacter(ref IMyCharacter __result, MatrixD position, Vector3 velocity, string name, string model, Vector3? colormask)
        {
            __result = MyCharacter.CreateCharacter(position, velocity, name, model, colormask, null);
            return false;
        }

        //TODO update +Shape +AccessTypeGrids
        public static void SetSafezoneOptions(MySafeZone zone, float radius, bool factionsBlackList, bool entitiesBlackList, bool playersBlackList, List<long> factions, List<long> entities, List<long> players, int allowedActions)
        {
            var ob = (MyObjectBuilder_SafeZone)zone.GetObjectBuilder();

            ob.AccessTypeFactions = factionsBlackList ? MySafeZoneAccess.Blacklist : MySafeZoneAccess.Whitelist;
            ob.AccessTypePlayers = playersBlackList ? MySafeZoneAccess.Blacklist : MySafeZoneAccess.Whitelist;
            ob.AccessTypeGrids = entitiesBlackList ? MySafeZoneAccess.Blacklist : MySafeZoneAccess.Whitelist;
            ob.AccessTypeFloatingObjects = entitiesBlackList ? MySafeZoneAccess.Blacklist : MySafeZoneAccess.Whitelist;

            ob.Factions = factions.ToArray();
            ob.Players = players.ToArray();
            ob.Entities = entities.ToArray();

            ob.Radius = radius;
            ob.AllowedActions = (MySafeZoneAction)allowedActions;

            MySessionComponentSafeZones.UpdateSafeZone(ob, true);
        }

        public static bool OnRequestJumpFromClient(MyGridJumpDriveSystem __instance, Vector3D jumpTarget, long userId)
        {
            try
            {
                var grid = jumpingGrid.GetValue(__instance) as MyCubeGrid;
                Log.Warn("OnRequestJumpFromClient:" + userId + " g:" + grid.DisplayName + " " + jumpTarget);
                var result = true;
                //true is good
                result &= (bool)JumpRedirectMethod.Invoke(null, new object[] { grid, jumpTarget, userId }); 
                result &= APIExtensions.CanJumpHere.Invoke(grid.PositionComp.GetPosition());
                result &= APIExtensions.CanJumpHere.Invoke(jumpTarget);
                return result;  //if true process to orig jump method
            }
            catch (Exception e)
            {
                Log.Error(e);
                return true;
            }
        }

        private static void SafeZoneActions(MySafeZone zone, int allowedActions)
        {
            var ob = (MyObjectBuilder_SafeZone)zone.GetObjectBuilder();
            ob.AllowedActions = (MySafeZoneAction)allowedActions;
            MySessionComponentSafeZones.UpdateSafeZone(ob, true);
        }


        private static void SafeZonePlayersInGrids(MySafeZone zone, List<IMyPlayer> players)
        {
            var SafeZoneEntities = typeof(MySafeZone).GetField("m_containedEntities", BindingFlags.Instance | BindingFlags.Public) ?? throw new Exception("Failed to find patch method");
            var entities = (MyConcurrentHashSet<long>)SafeZoneEntities.GetValue(zone);
            //players = new List<IMyPlayer>();
            foreach (var ent in entities)
            {
                MyCubeGrid grid;
                if (!MyEntities.TryGetEntityById(ent, out grid)) continue;
                var player = MyAPIGateway.Players.GetPlayerControllingEntity(grid);
                players.Add(player);
            }
        }

        private static void SafeZoneFactions(MySafeZone zone, bool WhiteList, List<IMyFaction> factions)
        {
            //SafeZonePatch.Log.Fatal("SafeZoneFactions{0}{1}",zone==null?"Zone null":"Zone not null", factions==null?"Factions null":"Factions not null");
            var ob = (MyObjectBuilder_SafeZone)zone.GetObjectBuilder();
            var factionsIds = new long[factions.Count];
            for (var i = 0; i < factions.Count; i++)
            {
                factionsIds[i] = factions[i].FactionId;
            }
            ob.AccessTypeFactions = WhiteList ? MySafeZoneAccess.Whitelist : MySafeZoneAccess.Blacklist;
            ob.Factions = factionsIds.ToArray();
            MySessionComponentSafeZones.UpdateSafeZone(ob, true);
        }


        public static bool IsLimitedBlock(string blockPairName, ref bool __result)
        {
            return MySession.Static.BlockTypeLimits.ContainsKey(blockPairName);
        }

        public static long CreateNPC(IMyFaction faction)
        {
            if (MyAPIGateway.Session.isTorchServer()) Common.SendChatMessage("CreateNPC");
            return 0;
            //DONT WORK WITHOUT PLUGIN
        }

        public static void TransferPCU(List<IMySlimBlock> blocks, long newOwner)
        {
            if (MyAPIGateway.Session.isTorchServer()) Common.SendChatMessage("FreezerLockGrid");
            //DONT WORK WITHOUT PLUGIN
        }
    }
}
