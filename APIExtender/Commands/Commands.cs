﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using NAPI;
using NLog;
using NLog.Targets;
using NLog.Targets.Wrappers;
using Sandbox.Game.World;
using Torch;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage;
using VRage.Dedicated.RemoteAPI;
using VRage.Game.ModAPI;
using VRageMath;

namespace Slime.Commands
{

    public class Commands : CommandModule
    {
        private Dictionary<ulong, List<DateTime>> m_parsedInfo = new Dictionary<ulong, List<DateTime>>();

        private static Regex regex = new Regex("([\\d\\-.]+) ([\\d\\-.]+) ([\\d\\-.]+) ([\\d\\-.]+) ([\\d\\-.]+)( (.*))?");

        [Command("player-track", "Looking for people who have been around X meters in the past Y days.")]
        [Permission(MyPromoteLevel.Admin)]
        public void PlayerTrack(uint radius, float days, double x = 0, double y = 0, double z = 0)
        {
            bool DEBUG = false;
            //!player-track 7000 14
            var pathToLogFolder = Path.Combine(Environment.CurrentDirectory, "Logs");
            var logs = Directory.GetFiles(pathToLogFolder, "PlayerPositionHistory*", SearchOption.TopDirectoryOnly).Where((filePath) => File.GetLastWriteTimeUtc(filePath).AddDays(days) >= DateTime.UtcNow);
            if (!logs.Any())
            {
                Context.Respond("Cant locate logs files");
                return;
            }

            Vector3D checkposition;

            if (Context.Player != null && !(x == 0 && y == 0 && z == 0))
            {
                checkposition = Context.Player.GetPosition();
                Context.Respond($"Using your body position {checkposition}");
            }
            else
            {

                checkposition = new Vector3D(x, y, z);
                Context.Respond($"Using position {checkposition.ToString()}");

            }

            if (radius < 1)
            {
                Context.Respond("Radius is too small, use radius > 1");
                return;
            }

            if (days < 0)
            {
                Context.Respond("Days can't be less than zero");
                return;
            }
            var sb = new StringBuilder("Big-brother report:" + Environment.NewLine);
            double radiussqr = (double)radius * (double)radius;
            var currtime = DateTime.UtcNow;
            string[] logstrs;
            var Identities = MySession.Static.Players.GetAllIdentities();
            foreach (var logfile in logs)
            {
                logstrs = File.ReadAllLines(logfile);
                if (logstrs == null)
                {
                    Context.Respond("Log file is empty.");
                    return;
                }

                foreach (var line in logstrs)
                {
                    try
                    {
                        var m = regex.Matches(line);
                        for (int i = 0; i < m.Count; i++)
                        {
                            var match = m[i];
                            if (!uint.TryParse(match.Groups[1].Value, out uint uinttime))
                            {
                                continue;
                            }
                            var timestamp = uinttime.ToDateTimeFromUnixTimestamp();
                            var diff = currtime - timestamp;
                            if ((diff).TotalDays <= days)
                            {
                                if (!ulong.TryParse(match.Groups[2].Value, out ulong steamid))
                                {
                                    continue;
                                }
                                if (double.TryParse(match.Groups[3].Value, out double X) && double.TryParse(match.Groups[4].Value, out double Y) && double.TryParse(match.Groups[5].Value, out double Z))
                                {
                                    var recorded_pos = new Vector3D(X, Y, Z);
                                    if ((checkposition - recorded_pos).LengthSquared() <= radiussqr)
                                    {
                                        //Sync.Players.TryGetIdentity(identitybysteamid);
                                        // var player = MySession.Static.Players.TryGetPlayerBySteamId(steamid);
                                        var identId = Sandbox.Game.Multiplayer.Sync.Players.TryGetIdentityId(steamid);
                                        if (identId == 0)
                                        {
                                            continue;
                                        }
                                        var player = Sandbox.Game.Multiplayer.Sync.Players.TryGetIdentity(identId);
                                        if (m_parsedInfo.ContainsKey(steamid))
                                        {
                                            m_parsedInfo[steamid].Add(timestamp);
                                        }
                                        else
                                        {
                                            m_parsedInfo.Add(steamid, new List<DateTime>() { timestamp });
                                        }
                                        if (DEBUG)
                                        {
                                            sb.AppendLine($") {player.DisplayName}[{steamid}] was here {diff.ToString(@"d\.hh\:mm\:ss")} ago. {match.Groups[7]}");
                                        }
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Log.Warn("Exception while parse line." + ex.ToString());
                    }
                }
            }

            var maxdiffmin = APIExtender.Config.TrackingFrequencyMin;
            var groups = 1;
            foreach (var splayer in m_parsedInfo)
            {
                DateTime time_start = DateTime.MinValue;
                DateTime time_end = DateTime.MinValue;
                for (int i = 0; i < splayer.Value.Count; i++)
                {
                    bool needprint = false;
                    if (splayer.Value[i] == null) continue;

                    if (time_start == DateTime.MinValue)
                    {
                        time_start = splayer.Value[i];
                    }

                    if (time_end == DateTime.MinValue)
                    {
                        time_end = splayer.Value[i];
                    }
                    else
                    {
                        if ((splayer.Value[i] - time_end).TotalMinutes > maxdiffmin * 3)
                        {
                            needprint = true;
                        }
                        else
                        {
                            time_end = splayer.Value[i];
                        }
                    }

                    if (needprint || i == (splayer.Value.Count - 1))
                    {

                        //print group
                        var beingtime = (time_end - time_start);
                        var identId = Sandbox.Game.Multiplayer.Sync.Players.TryGetIdentityId(splayer.Key);
                        if (identId == 0)
                        {
                            continue;
                        }
                        var player = Sandbox.Game.Multiplayer.Sync.Players.TryGetIdentity(identId);
                        sb.AppendLine($"{groups}) {player.DisplayName}[{splayer.Key}] was here for {beingtime.ToString(@"d\.hh\:mm\:ss")}, {(currtime - time_start).ToString(@"d\.hh\:mm\:ss")} ago.");
                        //start new group
                        time_start = DateTime.MinValue;
                        time_end = DateTime.MinValue;
                        groups++;
                    }
                }
            }
            if (m_parsedInfo.Any())
            {
                Context.Respond(sb.ToString());
            }
            else
            {
                Context.Respond("No results.");
            }
        }
    }
}