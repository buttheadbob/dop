﻿using NAPI;
using Sandbox.Game.Screens.Helpers;
using Sandbox.Game.World;
using Scripts.Shared;
using System;
using Sandbox.Game.Entities;
using Sandbox.Game.SessionComponents;
using Torch.Managers.PatchManager;
using VRage.Game;
using VRageMath;

namespace Slime
{
    //MOD CONNECTION
    public static class JailBreak
    {
        public static void Init (PatchContext patchContext)
        {
            //ModConnection.Log("Subscribe to CanMineHere!");
                   Func<string, string, Func<object, object[], object>> invoke = Invoke;
            Func<string, string, Func<object, object>> getter = Getter;
            Func<string, string, Action<object, object>> setter = Setter;

            ModConnection.SetValue ("MIG.APIExtender.InvokeFabric", invoke);
            ModConnection.SetValue ("MIG.APIExtender.GetterFabric", getter);
            ModConnection.SetValue ("MIG.APIExtender.SetterFabric", setter);

            
        }

        public static Func<object, object[], object> Invoke (string type, string method)
        {
            var mi = Ext2.getTypeByName (type).easyMethod (method);
            return (obj, pars)=>mi.Invoke (obj, pars);
        }

        public static Func<object, object> Getter(string type, string field)
        {
            var mi = Ext2.getTypeByName(type).easyField(field, needThrow: false);
            if (mi != null)
            {
                return (obj) => mi.GetValue(obj);
            }
            var mi2 = Ext2.getTypeByName(type).easyProp(field);
            if (mi2 != null)
            {
                return (obj) => mi2.GetValue(obj);
            }
            return null;
        }

        public static Action<object, object> Setter(string type, string field)
        {
            var mi = Ext2.getTypeByName(type).easyField(field);
            if (mi != null)
            {
                return (obj, val) => mi.SetValue(obj, val);
            }
            var mi2 = Ext2.getTypeByName(type).easyProp(field);
            if (mi2 != null)
            {
                return (obj, val) => mi2.SetValue(obj, val);
            }
            return null;
        }

        //FABRIC
        //var invokeFabric = ModConnection.Get<Func<string, string, Func<object, object[], object>>>("MIG.APIExtender.InvokeFabric");
        //var sendAddGPS = invokeFabric("Sandbox.Game.Multiplayer.MyGpsCollection", "SendAddGps");
        //
        //MyGps myGps = new MyGps
        //{
        //    //ShowOnHud = showOnHud,
        //    //Name = name,
        //    //Description = description,
        //    //AlwaysVisible = alwaysVisible,
        //    //IsObjective = isObjective,
        //    //GPSColor = color,
        //    //DiscardAt = discardAt
        //};
        //sendAddGPS(MySession.Static.Gpss, new object[] { identity, ref myGps, 0, playSound });
    }
}
