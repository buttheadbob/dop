﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;
using Sandbox.Game.Entities;
using Sandbox.Game.World;
using VRage;

namespace Slime.Features
{
    public static class PlayerTracker
    {
        public static Logger FileLogger = LogManager.GetLogger("PlayerPositionHistory");

        public static void LogPlayerPositions()
        {
            var players = MySession.Static.Players.GetOnlinePlayers();
            if (players.Any())
            {
                var log = new StringBuilder();
                uint time = DateTime.UtcNow.ToUnixTimestamp();//var temp = a.ToDateTimeFromUnixTimestamp();
                foreach (var player in players)
                { //x,y,z,steamid,timestamp,comment
                    string comment = " as character";
                    var pos = player.GetPosition();
                    if(player?.Controller?.ControlledEntity is MyCockpit)
                    {
                        comment = "in Grid: " + (player?.Controller?.ControlledEntity as MyCockpit).CubeGrid.DisplayName;
                    }
                    log.Append($"{time} {player.Id.SteamId} {pos.X} {pos.Y} {pos.Z} {comment}");
                }
                FileLogger.Warn(log);
            }
        }
        public static void AddLogFile()
        {
            var rules = LogManager.Configuration.LoggingRules;
            for (int i = rules.Count - 1; i >= 0; i--)
            {

                var rule = rules[i];

                if (rule.LoggerNamePattern == "PlayerPositionHistory")
                    rules.RemoveAt(i);
            }

            var logTarget = new FileTarget
            {
                FileName = "Logs/PlayerPositionHistory-${shortdate:universalTime=True}.log",              
               Layout = "${var:logContent}", Encoding = Encoding.UTF8 };
            var logRule = new LoggingRule("PlayerPositionHistory", LogLevel.Debug, logTarget)
            {
                Final = true
            };
            rules.Insert(0, logRule);
            LogManager.Configuration.Reload();
        }
    }
}
