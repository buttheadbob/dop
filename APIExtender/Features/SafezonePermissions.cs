﻿using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Character;
using Torch.Managers.PatchManager;
using VRage.Game.Entity;
using VRage.Game.ObjectBuilders.Components;
using VRageMath;

namespace Slime.Features
{
    public static class SafezonePermissions
    {
        public static void Init(PatchContext patchContext)
        {
            bool voxels = APIExtender.Config.ProtectVoxelsEnabled;
            bool apiExtender = APIExtender.Config.APISafezonesIsActionAllowed;
            bool globalPermissions = APIExtender.Config.SafeZoneAllowDamage
                                     || APIExtender.Config.SafeZoneAllowBuilding
                                     || APIExtender.Config.SafeZoneAllowDrilling
                                     || APIExtender.Config.SafeZoneAllowWelding
                                     || APIExtender.Config.SafeZoneAllowGrinding
                                     || APIExtender.Config.SafeZoneAllowVoxelHand
                                     || APIExtender.Config.SafeZoneAllowLandingGearLock
                                     || APIExtender.Config.SafeZoneAllowShooting;
            
            if (voxels || globalPermissions)
            {
                Log.Error("Patching Permissions global");
                patchContext.Redirect2(typeof(MySessionComponentSafeZones), "IsActionAllowed", typeof(SafezonePermissions), "IsActionAllowedAABB", types: new[] { typeof(BoundingBoxD), typeof(MySafeZoneAction), typeof(long), typeof(ulong) }, types2: null);
                patchContext.Redirect2(typeof(MySessionComponentSafeZones), "IsActionAllowed", typeof(SafezonePermissions), "IsActionAllowedPoint", types: new[] { typeof(Vector3D), typeof(MySafeZoneAction), typeof(long), typeof(ulong) }, types2: null);
                patchContext.Redirect2(typeof(MySessionComponentSafeZones), "IsActionAllowed", typeof(SafezonePermissions), "IsActionAllowedEntity", types: new[] { typeof(MyEntity), typeof(MySafeZoneAction), typeof(long), typeof(ulong) }, types2: null);
            }

            if (apiExtender)
            {
                Log.Error("Patching Permissions local");
                patchContext.Redirect2(typeof(MySafeZone), "IsActionAllowed", typeof(SafezonePermissions), "IsActionAllowedAABBSafezone", types: new[] { typeof(BoundingBoxD), typeof(MySafeZoneAction), typeof(long) }, types2: null);
                patchContext.Redirect2(typeof(MySafeZone), "IsActionAllowed", typeof(SafezonePermissions), "IsActionAllowedPointSafezone", types: new[] { typeof(Vector3D), typeof(MySafeZoneAction), typeof(long) }, types2: null);
                patchContext.Redirect2(typeof(MySafeZone), "IsActionAllowed", typeof(SafezonePermissions), "IsActionAllowedEntitySafezone", types: new[] { typeof(MyEntity), typeof(MySafeZoneAction), typeof(long) }, types2: null);
            }
        }

        private static bool IsActionAllowedAABB(ref bool __result, BoundingBoxD aabb, MySafeZoneAction action, long sourceEntityId = 0L, ulong user = 0L)
        {
            //Log.Error($"IsActionAllowedAABB {aabb} {action} {sourceEntityId} {user}");
            var r = APIExtensions.APIGlobal_IsActionAllowedAABB((int) action, user, sourceEntityId, aabb);
            var e = APIExtensions.HandleAPIAnswer(r, ref __result);
            if (!e) return false;
            
            if (action == MySafeZoneAction.Drilling)
            {
                if (!UndestructableVoxels.CheckDrillingIsAllowed(ref __result, aabb, action, sourceEntityId, user))
                {
                    return false;
                }
            }
            return HandleGlobalPermissions(ref __result, action);
        }

        private static bool IsActionAllowedPoint(ref bool __result, Vector3D point, MySafeZoneAction action, long sourceEntityId = 0L, ulong user = 0L)
        {
            //Log.Error($"IsActionAllowed {point} {action} {sourceEntityId} {user}");
            var r = APIExtensions.APIGlobal_IsActionAllowedPoint((int) action, user, sourceEntityId, point);
            var e = APIExtensions.HandleAPIAnswer(r, ref __result);
            if (!e) return false;
            
            if (action == MySafeZoneAction.Drilling)
            {
                if (!UndestructableVoxels.CheckDrillingIsAllowed(ref __result, point, action, sourceEntityId, user))
                {
                    return false;
                }
            }

            return HandleGlobalPermissions(ref __result, action);
        }

        private static bool IsActionAllowedEntity(ref bool __result, MyEntity entity, MySafeZoneAction action, long sourceEntityId = 0L, ulong user = 0L)
        {
            //Log.Error($"IsActionAllowedEntity {entity} {action} {sourceEntityId} {user}");
            var r = APIExtensions.APIGlobal_IsActionAllowedEntity((int) action, user, sourceEntityId, entity);
            var e = APIExtensions.HandleAPIAnswer(r, ref __result);
            if (!e) return false;
            
            if (action == MySafeZoneAction.Drilling)
            {
                if (!UndestructableVoxels.CheckDrillingIsAllowed(ref __result, entity, action, sourceEntityId, user))
                {
                    return false;
                }
            }
            return HandleGlobalPermissions(ref __result, action);
        }

        private static bool IsActionAllowedAABBSafezone(MySafeZone __instance, ref bool __result, BoundingBoxD aabb, MySafeZoneAction action, long sourceEntityId = 0L)
        {
            var r = APIExtensions.APISafezone_IsActionAllowedAABB(__instance, (int) action, sourceEntityId, aabb);
            return APIExtensions.HandleAPIAnswer(r, ref __result);
        }

        private static bool IsActionAllowedPointSafezone(MySafeZone __instance, ref bool __result, Vector3D point, MySafeZoneAction action, long sourceEntityId = 0L)
        {
            var r = APIExtensions.APISafezone_IsActionAllowedPoint(__instance, (int) action, sourceEntityId, point);
            return APIExtensions.HandleAPIAnswer(r, ref __result);
        }

        private static bool IsActionAllowedEntitySafezone(MySafeZone __instance, ref bool __result, MyEntity entity, MySafeZoneAction action, long sourceEntityId = 0L)
        {
            var r = APIExtensions.APISafezone_IsActionAllowedEntity(__instance, (int) action, sourceEntityId, entity);
            return APIExtensions.HandleAPIAnswer(r, ref __result);
        }

        private static bool HandleGlobalPermissions(ref bool __result, MySafeZoneAction action)
        {
            switch (action)
            {
                case MySafeZoneAction.Drilling:
                    if (APIExtender.Config.SafeZoneAllowDrilling) { __result = true; return false; }
                    break;
                case MySafeZoneAction.Damage:
                    if (APIExtender.Config.SafeZoneAllowDamage) { __result = true; return false; }
                    break;
                case MySafeZoneAction.Shooting:
                    if (APIExtender.Config.SafeZoneAllowShooting) { __result = true; return false; }
                    break;
                case MySafeZoneAction.Welding:
                    if (APIExtender.Config.SafeZoneAllowWelding) { __result = true; return false; }
                    break;
                case MySafeZoneAction.Grinding:
                    if (APIExtender.Config.SafeZoneAllowGrinding) { __result = true; return false; }
                    break;
                case MySafeZoneAction.VoxelHand:
                    if (APIExtender.Config.SafeZoneAllowVoxelHand) { __result = true; return false; }
                    break;
                case MySafeZoneAction.Building:
                    if (APIExtender.Config.SafeZoneAllowBuilding) { __result = true; return false;}
                    break;
                case MySafeZoneAction.LandingGearLock:
                    if (APIExtender.Config.SafeZoneAllowLandingGearLock) { __result = true; return false; }
                    break;
            }
            return true;
        }
    }
}

/*
        private static bool IsActionAllowedAABB(ref bool __result, BoundingBoxD aabb, MySafeZoneAction action, long sourceEntityId = 0L, long user = 0L)
        {
            switch (action)
            {
                case MySafeZoneAction.Damage:
                    {
                        if (APIExtender.Config.SafeZoneAllowDamage)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Shooting:
                    {
                        if (APIExtender.Config.SafeZoneAllowShooting)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Drilling:
                    {
                        if (APIExtender.Config.SafeZoneAllowDrilling)
                        {
                            __result = true;
                            return false;
                        }

                        if (APIExtender.Config.ProtectVoxelsFromDrilling > 0 && UndestructableVoxels.IsVoxelsProtected(aabb, APIExtender.Config.ProtectVoxelsFromDrilling))
                        {
                            __result = false;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Welding:
                    {
                        if (APIExtender.Config.SafeZoneAllowWelding)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Grinding:
                    {
                        if (APIExtender.Config.SafeZoneAllowGrinding)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.VoxelHand:
                    {
                        if (APIExtender.Config.SafeZoneAllowVoxelHand)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Building:
                    {
                        if (APIExtender.Config.SafeZoneAllowBuilding)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.LandingGearLock:
                    {
                        if (APIExtender.Config.SafeZoneAllowLandingGearLock)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            return true; // process to vanilla check
        }

        private static bool IsActionAllowedPoint(ref bool __result, Vector3D point, MySafeZoneAction action, long sourceEntityId = 0L, long user = 0L)
        {
            switch (action)
            {
                case MySafeZoneAction.Damage:
                    {
                        if (APIExtender.Config.SafeZoneAllowDamage)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Shooting:
                    {
                        if (APIExtender.Config.SafeZoneAllowShooting)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Drilling:
                    {
                        if (APIExtender.Config.SafeZoneAllowDrilling)
                        {
                            __result = true;
                            return false;
                        }

                        if (APIExtender.Config.ProtectVoxelsFromDrilling > 0 && UndestructableVoxels.IsVoxelsProtected(point, APIExtender.Config.ProtectVoxelsFromDrilling))
                        {
                            __result = false;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Welding:
                    {
                        if (APIExtender.Config.SafeZoneAllowWelding)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Grinding:
                    {
                        if (APIExtender.Config.SafeZoneAllowGrinding)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.VoxelHand:
                    {
                        if (APIExtender.Config.SafeZoneAllowVoxelHand)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Building:
                    {
                        if (APIExtender.Config.SafeZoneAllowBuilding)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.LandingGearLock:
                    {
                        if (APIExtender.Config.SafeZoneAllowLandingGearLock)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            return true; // process to vanilla check
        }

        private static bool IsActionAllowedEntity(ref bool __result, MyEntity entity, MySafeZoneAction action, long sourceEntityId = 0L, long user = 0L)
        {
            switch (action)
            {
                case MySafeZoneAction.Damage:
                    {
                        if (APIExtender.Config.SafeZoneAllowDamage)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Shooting:
                    {
                        if (APIExtender.Config.SafeZoneAllowShooting)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Drilling:
                    {
                        if (APIExtender.Config.SafeZoneAllowDrilling)
                        {
                            __result = true;
                            return false;
                        }

                        if (entity is MyCharacter)
                        {
                            if (APIExtender.Config.ProtectVoxelsFromDrilling > 0 && UndestructableVoxels.IsVoxelsProtected(entity.PositionComp.WorldMatrixRef.Translation, APIExtender.Config.ProtectVoxelsFromDrilling))
                            {
                                __result = false;
                                return false;
                            }
                        }

                        if (entity is MyCubeGrid grid)
                        {
                            var sphere = grid.PositionComp.WorldVolume;
                            if (UndestructableVoxels.IsVoxelsProtected(sphere.Center, sphere.Radius + APIExtender.Config.ProtectVoxelsFromDrilling))
                            {
                                __result = false;
                                return false;
                            }
                        }

                        break;
                    }
                case MySafeZoneAction.Welding:
                    {
                        if (APIExtender.Config.SafeZoneAllowWelding)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Grinding:
                    {
                        if (APIExtender.Config.SafeZoneAllowGrinding)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.VoxelHand:
                    {
                        if (APIExtender.Config.SafeZoneAllowVoxelHand)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.Building:
                    {
                        if (APIExtender.Config.SafeZoneAllowBuilding)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                case MySafeZoneAction.LandingGearLock:
                    {
                        if (APIExtender.Config.SafeZoneAllowLandingGearLock)
                        {
                            __result = true;
                            return false;
                        }

                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            return true;
        }
*/