﻿using Sandbox.Game.Entities; 
using Sandbox.Game.Entities.Character; 
using Sandbox.Game.Weapons; 
using System; 
using System.Collections.Generic; 
using System.Reflection; 
using System.Runtime.CompilerServices;
using NAPI;
using Sandbox.Game.World.Generator;
using Torch.Managers.PatchManager;
using VRage.Game.Entity;
using Sandbox.Game.Multiplayer;
using Torch;
using static Sandbox.Game.World.MyPlayer;
using Sandbox.ModAPI;

namespace Slime.Features
{
    // DOP.Instance.Config. 
    static class RemoteDeleting { // : HackStatic 
        private static int counterBlocked = 0;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void InitHack(PatchContext patchContext)
        {
            if (!APIExtender.Config.RemoteDeletingEnabled) return;
            patchContext.Prefix(typeof(MyCubeGrid), "RemoveBlocksBuiltByID", typeof(RemoteDeleting), new[] { "__instance", "identityID" });
        }

        public static bool RemoveBlocksBuiltByID(MyCubeGrid __instance, long identityID)
        {
            var PlayerSteamId = Sync.Players.TryGetSteamId(identityID);
            var requesterPlayer = Sync.Players.TryGetPlayerBySteamId(PlayerSteamId);
            var PlayerName = "Unknown or Offline Player!";

            if (!APIExtender.Config.RemoteDeletingEnabled)
            {
                if (requesterPlayer != null)
                    Log.Warn($"RemoteDeleting : Player {requesterPlayer.DisplayName} Deleted Grid {__instance.DisplayName} with {__instance.BlocksCount} Blocks as GPS {__instance.PositionComp.GetPosition()}");
                else
                    Log.Warn($"RemoteDeleting : {PlayerName} Deleted Grid {__instance.DisplayName} with {__instance.BlocksCount} Blocks as GPS {__instance.PositionComp.GetPosition()}");

                return true;
            }
            else
            {
                if (requesterPlayer != null)
                    Log.Warn($"RemoteDeleting : Player {requesterPlayer.DisplayName} Trying to delete Grid {__instance.DisplayName} with {__instance.BlocksCount} Blocks as GPS {__instance.PositionComp.GetPosition()}");
                else
                    Log.Warn($"RemoteDeleting : {PlayerName} Trying to delete Grid {__instance.DisplayName} with {__instance.BlocksCount} Blocks as GPS {__instance.PositionComp.GetPosition()}");
            }

            if (requesterPlayer != null)
                PlayerName = requesterPlayer.DisplayName;

            var sphere = new VRageMath.BoundingSphereD(__instance.PositionComp.GetPosition(), APIExtender.Config.RemoteDeletingRange);
            var ent = MyEntities.GetEntitiesInSphere(ref sphere);
            var can = CheckCanDelete(identityID, ent, PlayerName);

            if (!can)
                return false;

            foreach (var x in __instance.GetFatBlocks())
            {
                if (x is MyShipDrill drill)
                    drill.Enabled = false;
                if (x is IMyProjector Projector)
                    Projector.Enabled = false;
            }

            Log.Warn($"RemoteDeleting : Grid : {__instance.DisplayName}  Deleted : by {PlayerName}");

            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool CheckCanDelete(long UseridentityID, List<MyEntity> ent, string PlayerName)
        {
            foreach (var x in ent)
            {
                if (x is MyCubeBlock bl && bl.OwnerId != 0)
                {
                    var cant = Relations.isEnemy(UseridentityID, bl.OwnerId);
                    if (cant)
                    {
                        var mess = APIExtender.Config.RemoteDeletingEnemyText;
                        if (!string.IsNullOrEmpty(mess)) { Common.ShowNotification(mess, 15000, "Red", UseridentityID); }

                        Log.Warn($"RemoteDeleting : {counterBlocked++}  Blocked : Near is block :{bl} {PlayerName} {bl.OwnerId}");
                        return false;
                    }
                }

                if (x is MyCharacter pl)
                {
                    var cant = MyIDModule.GetRelationPlayerPlayer(UseridentityID, pl.GetPlayerIdentityId()).AsNumber() == -1;
                    if (cant)
                    {
                        var mess = APIExtender.Config.RemoteDeletingEnemyText;
                        if (!string.IsNullOrEmpty(mess)) { Common.ShowNotification(mess, 15000, "Red", UseridentityID); }

                        Log.Warn($"RemoteDeleting : {counterBlocked++} Blocked : Enemy player is near! : Player Name :{pl.DisplayName} ID:{pl.GetPlayerIdentityId()}");
                        return false;
                    }
                }
            }

            return true;
        }
    }

    /*switch (Config.RemoteDeletingStrategy) { 
               case Config.FixRemoteDeleteStrategy.DISABLE_DRILLS: { 
                    

                   break; 
               } 
               case Config.FixRemoteDeleteStrategy.BLOCK_DELETE_GRIDS_WITH_DRILLS: { 
                   foreach (var x in __instance.GetFatBlocks()) { 
                       var drill = x as MyShipDrill; 
                       if (drill != null) { 
                           Common.ShowNotification("RemoteDeleting : Blocked. Please remove drills from grid.", 15000, "Red", identityID); 
                           return false; 
                       } 
                   } 

                   break; 
               } 
               case Config.FixRemoteDeleteStrategy.BLOCK_DELETE_GRIDS_WITH_WORKING_DRILLS: { 
                   foreach (var x in __instance.GetFatBlocks()) { 
                       var drill = x as MyShipDrill; 
                       if (drill != null && drill.Enabled) { 
                           Common.ShowNotification("RemoteDeleting : Blocked. Disable all drills on grid.", 15000, "Red", identityID); 
                           return false; 
                       } 
                   } 

                   break; 
               } 
           }*/
}