﻿using System;
using System.Collections.Generic;
using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.GameSystems;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using SpaceEngineers.Game.World;
using Torch;
using Torch.Managers.PatchManager;
using VRage.Game.Components;
using VRage.Network;
using VRageMath;

namespace Slime.Features
{
    public static class CustomMedscreen
    {
        private static readonly List<MySpaceRespawnComponent.MyRespawnPointInfo> m_respanwPointsCache = new List<MySpaceRespawnComponent.MyRespawnPointInfo>();
        public static void Patch(PatchContext patchContext)
        {
            if (APIExtender.Config.EnableMedScreen)
                patchContext.Prefix(typeof(MySpaceRespawnComponent), typeof(CustomMedscreen), nameof(GetAvailableRespawnPoints));
        }

        private static bool GetAvailableRespawnPoints(ref ClearToken<MySpaceRespawnComponent.MyRespawnPointInfo> __result, long? identityId, bool includePublicSpawns)
        {
            //Log.Info("Hit: prefix patch GetAvailableRespawnPoints: identityId= " + identityId + " includePublicSpawns= " + includePublicSpawns);
            long fixed_ownerId = identityId == null ? 0 : (long)identityId;
            m_respanwPointsCache.Clear();
            if (APIExtender.Config.EnableMedScreenFake)
                m_respanwPointsCache.Add(new MySpaceRespawnComponent.MyRespawnPointInfo() { MedicalRoomGridId = 0, MedicalRoomId = 0, MedicalRoomName = "Select Respawn", OwnerId = -1 });
            
            #region Keen code, copy from: MySpaceRespawnComponent.GetAvailableRespawnPoints
            foreach (MyRespawnComponent myRespawnComponent in MyRespawnComponent.GetAllRespawns())
            {
                MyTerminalBlock entity = myRespawnComponent.Entity;
               
                if (entity != null && !entity.Closed && !entity.MarkedForClose)
                {
                    if (!APIExtensions.Med_CanUseSpawn(fixed_ownerId, entity))
                        continue;
                        
                    //our code
                    if (APIExtender.Config.EnableMedScreenRemover)
                    {
                        //Enemies = 1, skip if enemies, ignore share if not in exclude list.
                        if (Relations.GetRelation(fixed_ownerId, entity.IDModule.Owner) == 1)
                        {
                            var faction = MySession.Static.Factions.GetPlayerFaction(entity.IDModule.Owner);
                            if (faction != null)
                            {
                                if (!APIExtender.Config.MedScreenRemoverExcludedTagsList.Contains(faction.Tag))
                                    continue;
                            }
                            continue;
                        }
                    }

                    MyCubeGrid cubeGrid = entity.CubeGrid;
                    if (cubeGrid != null)
                    {
                        MyCubeGridSystems gridSystems = cubeGrid.GridSystems;
                        if (gridSystems != null)
                            gridSystems.UpdatePower(); //hmm for what? test later.
                    }
                    entity.UpdateIsWorking(); //hmm for what? test later.

                    if (entity.IsWorking && (identityId == null || myRespawnComponent.CanPlayerSpawn(identityId.Value, includePublicSpawns)))
                    {
                        IMySpawnBlock mySpawnBlock = (IMySpawnBlock)entity;
                        var name = (!string.IsNullOrEmpty(mySpawnBlock.SpawnName)) ? mySpawnBlock.SpawnName : ((entity.CustomName != null) ? entity.CustomName.ToString() : (entity.Name ?? entity.ToString()));
                        AddSpawn(entity, name, entity.IDModule.Owner, myRespawnComponent.GetOxygenLevel());
                    }
                }
            }
            #endregion
            
            APIExtensions.Med_OnSpawnsRequested.Invoke(fixed_ownerId);

            if (APIExtender.Config.EnableMedScreenSorter)
            {
                var steamId = MyEventContext.Current.Sender.Value;

                if (MySession.Static.IsUserModerator(steamId))
                {
                    if (!MySession.Static.RemoteAdminSettings.GetValueOrDefault(steamId).HasFlag(AdminSettingsEnum.UseTerminals))
                        MedsSorter(fixed_ownerId);
                }
                else
                    MedsSorter(fixed_ownerId);
            }

            __result = new ClearToken<MySpaceRespawnComponent.MyRespawnPointInfo>
            {
                List = m_respanwPointsCache
            };
            return false;
        }

        private static void MedsSorter(long ownerId)
        {
            if (ownerId == 0)
                return;

            m_respanwPointsCache.Sort((a, b) =>
            {
                var v = GetTypeSortValue(a) - GetTypeSortValue(b);
                if (v != 0) return v;
                return -1;
            });

            int GetTypeSortValue(MySpaceRespawnComponent.MyRespawnPointInfo item)
            {
                try
                {
                    if (item == null)
                        return 7;

                    if (item.OwnerId == -1) return 0;
                    if (item.OwnerId == ownerId) return 1;
                    if (Relations.GetRelation(ownerId, item.OwnerId) == 3) return 2; //Friends = 3
                    if (Relations.GetRelation(ownerId, item.OwnerId) == 2) return 3; //Allies = 2
                    if (Relations.GetRelation(ownerId, item.OwnerId) == 0) return 4; //Neutral = 0
                    if (Relations.GetRelation(ownerId, item.OwnerId) == 1) return 5; //Enemies = 1               
                    else return 6;
                }
                catch(Exception ex)
                {
                     Log.Error("GetTypeSortValue Exception : " + ex.ToString());
                    return 7;
                }
            }
        }

        public static void AddSpawn(IMyTerminalBlock entity, string name, long ownerId, float oxygen)
        {
            MySpaceRespawnComponent.MyRespawnPointInfo myRespawnPointInfo = new MySpaceRespawnComponent.MyRespawnPointInfo
            {
                MedicalRoomId = entity.EntityId,
                MedicalRoomGridId = entity.CubeGrid.EntityId,
                MedicalRoomName = name,
                OxygenLevel = oxygen,
                OwnerId = ownerId
            };

            MatrixD worldMatrix = entity.WorldMatrix;
            Vector3D translation = worldMatrix.Translation;
            Vector3D vector3D = translation + worldMatrix.Up * 20.0 + entity.WorldMatrix.Right * 20.0 + worldMatrix.Forward * 20.0;
            Vector3D? vector3D2 = MyEntities.FindFreePlace(vector3D, 1f, 20, 5, 1f, null);

            if (vector3D2 == null)
                vector3D2 = new Vector3D?(vector3D);

            myRespawnPointInfo.PrefferedCameraPosition = vector3D2.Value;
            myRespawnPointInfo.MedicalRoomPosition = translation;
            myRespawnPointInfo.MedicalRoomUp = worldMatrix.Up;

            if (entity.CubeGrid.Physics != null)
                myRespawnPointInfo.MedicalRoomVelocity = entity.CubeGrid.Physics.LinearVelocity;

            m_respanwPointsCache.Add(myRespawnPointInfo);
        }
    }
}
