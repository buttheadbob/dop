﻿using System;
using System.Collections.Generic;
using System.Linq;
using NAPI;
using Sandbox.Engine.Multiplayer;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Slime;
using Torch.Managers.PatchManager;
using VRage.Game.Entity;
using VRage.Game.ObjectBuilders.Components;
using VRageMath;

namespace Slime.Features
{
    public static class UndestructableVoxels
    {
        //private static List<Pair<long, BoundingSphereD>> protectedAreas = new List<Pair<long, BoundingSphereD>>();
        public static void Patch(PatchContext patchContext)
        {
            if (!APIExtender.Config.ProtectVoxelsEnabled) return;

            Log.Error("Patching: UndestructableVoxels");


            if (APIExtender.Config.ProtectVoxelsFromWarheads > 0)
            {
                patchContext.Prefix(typeof(MyExplosions), typeof(UndestructableVoxels), "AddExplosion");
            }

            if (APIExtender.Config.ProtectVoxelsFromCollisions > 0 || APIExtender.Config.ProtectVoxelsFromCollisionsGlobal)
            {
                patchContext.Prefix(typeof(MyCubeGrid), typeof(UndestructableVoxels), "PerformCutouts");
            }
        }

        public static bool IsVoxelsProtected(Vector3 position, double extend = 0)
        {
            if (!APIExtender.Config.ProtectVoxelsEnabled) return false;
            
            foreach (var sz in MySessionComponentSafeZones.SafeZones)
            {
                double rad = sz.Radius;
                rad += extend;
                if ((position - sz.WorldMatrix.Translation).LengthSquared() <= rad * rad)
                {
                    return true; //Protect voxels in sf radius
                }
            }

            if (!APIExtensions.CanMineHere(position))
            {
                return true; //Mod checks, currentlty checks POI's config
            }

            return false;
        }


        public static bool IsVoxelsProtected(BoundingBoxD aabb, double extend = 0)
        {
            if (!APIExtender.Config.ProtectVoxelsEnabled) return false;
            
            var sp = new BoundingSphereD(Vector3D.Zero, 0);
            foreach (var sz in MySessionComponentSafeZones.SafeZones)
            {
                double rad = sz.Radius;
                rad += extend;
                sp.Center = sz.WorldMatrix.Translation;
                sp.Radius = rad;
                if (aabb.Intersects(ref sp))
                {
                    return true; //Protect voxels in sf radius
                }
            }

            if (!APIExtensions.CanMineHere(aabb.Center))
            {
                return true; //Mod checks, currentlty checks POI's config
            }

            return false;
        }

        private static bool AddExplosion(ref MyExplosionInfo explosionInfo, bool updateSync = true)
        {
            if (IsVoxelsProtected(explosionInfo.ExplosionSphere.Center, (float)explosionInfo.ExplosionSphere.Radius + APIExtender.Config.ProtectVoxelsFromWarheads))
            {
                explosionInfo.AffectVoxels = false;
            }
            return true;
        }

        private static bool PerformCutouts(MyCubeGrid __instance, List<MyGridPhysics.ExplosionInfo> explosions)
        {
            if (APIExtender.Config.ProtectVoxelsFromCollisionsGlobal)
            {
                explosions.Clear();
                return true;
            }

            for (var x = 0; x < explosions.Count; x++)
            {
                var e = explosions[x];
                if (IsVoxelsProtected(e.Position, e.Radius + APIExtender.Config.ProtectVoxelsFromCollisions))
                {
                    explosions.RemoveAt(x);
                    x--;
                }
            }

            return true;
        }

        public static bool CheckDrillingIsAllowed(ref bool __result, MyEntity entity, MySafeZoneAction action, long sourceEntityId, ulong user)
        {
            if (entity is MyCharacter)
            {
                if (APIExtender.Config.ProtectVoxelsFromDrilling > 0 && IsVoxelsProtected(entity.PositionComp.WorldMatrixRef.Translation, APIExtender.Config.ProtectVoxelsFromDrilling))
                {
                    __result = false;
                    return false;
                }
            }

            if (entity is MyCubeGrid grid)
            {
                var sphere = grid.PositionComp.WorldVolume;
                if (IsVoxelsProtected(sphere.Center, sphere.Radius + APIExtender.Config.ProtectVoxelsFromDrilling))
                {
                    __result = false;
                    return false;
                }
            }

            return true;
        }

        public static bool CheckDrillingIsAllowed(ref bool __result, BoundingBoxD aabb, MySafeZoneAction action, long sourceEntityId, ulong user)
        {
            if (APIExtender.Config.ProtectVoxelsFromDrilling > 0 && IsVoxelsProtected(aabb, APIExtender.Config.ProtectVoxelsFromDrilling))
            {
                __result = false;
                return false;
            }

            return true;
        }
        
        public static bool CheckDrillingIsAllowed(ref bool __result, Vector3 point, MySafeZoneAction action, long sourceEntityId, ulong user)
        {
            if (APIExtender.Config.ProtectVoxelsFromDrilling > 0 && IsVoxelsProtected(point, APIExtender.Config.ProtectVoxelsFromDrilling))
            {
                __result = false;
                return false;
            }
            return true;
        }
    }
}
