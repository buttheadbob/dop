﻿using NAPI;
using Sandbox.Game.Entities;
using System;
using System.Text.RegularExpressions;

namespace Slime.Features
{
    /*public static class AutoRenameGrids
    {
        public static void Init ()
        {
            FrameExecutor.addFrameLogic((x)=> {
                
            });
        }

        public static Random random = new Random();


        private static string GetSizeName (MyCubeGrid grid)
        {
            //
            if (grid.BlocksCount < 10)
            {
                return "TINY";
            }
            if (grid.BlocksCount < 100)
            {
                return "SMALL";
            }
            if (grid.BlocksCount < 500)
            {
                return "NORMAL";
            }
            if (grid.BlocksCount < 1500)
            {
                return "BIG";
            }

            if (grid.BlocksCount < 5000)
            {
                return "HUGE";
            }

            return "GIGANTIC";
        }


        

        public static void DoUpdate ()
        {
            var g = UtilitesEntity.GetGrid((x)=>x.Name.StartsWith ("Small Grid") || x.Name.StartsWith("Large Grid"));
            if (g == null) return;

            var connectedGrids = g.GetConnectedGrids(VRage.Game.ModAPI.GridLinkTypeEnum.Physical);
            connectedGrids.Sort((a, b) => ((MyCubeGrid)b).BlocksCount - ((MyCubeGrid)a).BlocksCount);

            var main = connectedGrids[0];



            string factionName = "NoFaction"; 
            string factionTag = "NONE";
            string factionTagOrPlayerName = factionTag;
            string owner = "NOBODY";
           
            long ownerIdentity = 0;
            long randomId = random.Next(100_000); 
            long gridId = g.EntityId;
            string isStatic = g.IsStatic ? "Static" : "Dynamic";
            string isStaticShort = g.IsStatic ? "Stat" : "Dyn";

            if (g.BigOwners.Count > 0)
            {
                var p = g.BigOwners[0];
                ownerIdentity = p;
                if (p != null)
                {
                    var identity = p.Identity();
                    if (identity != null)
                    {
                        owner = p.Identity().DisplayName;
                    }

                    var f = p.PlayerFaction();
                    if (f != null)
                    {
                        factionName = f.Name;
                        factionTag = f.Tag;
                        factionTagOrPlayerName = factionTag;
                    }
                }
            }

            


            
            var newName = SharpUtils.SmartReplace ("", (p)=>{
                switch (p.ToLower())
                {
                    
                    case "owner": return owner;
                    case "owneridentity": return ""+ownerIdentity;
                    case "randomid": return ""+ randomId;
                    case "gridid": return ""+ gridId;
                    case "isstatic": return isStatic;
                    case "isstaticshort": return isStaticShort;

                    case "factionname": return factionName;
                    case "factiontag": return factionTag;
                    case "factiontagorplayername": return factionTagOrPlayerName;

                    default:
                        return "???";
                }
            });
        }
    }*/
}
