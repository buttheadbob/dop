﻿using System.IO;
using System.Reflection;
using System.Windows.Controls;
using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.World;
using Scripts.Shared;
using Slime.Config;
using Slime.Features;
using Slime.Fixes;
using Slime.GUI;
using Torch;
using Torch.API;
using Torch.Managers.PatchManager;
using TorchPlugin;
using VRage.Game;
using VRage.Plugins;
using VRage.Utils;

namespace Slime
{
    public class APIExtender : CommonPlugin
    {
        public static CoreConfig Config => _config.Data;
        private static Persistent<CoreConfig> _config;
        public static uint counter = 0;
        public override void Init(ITorchBase torch)
        {
            base.Init(torch);
            _config = Persistent<CoreConfig>.Load(Path.Combine(StoragePath, "MIG-APIExtender.cfg"));
        }

        public override void Patch(PatchContext patchContext)
        {
            ModConnection.InitOnce();
            RemoteDeleting.InitHack(patchContext);
            SafezonePermissions.Init(patchContext);
            APIExtensionsOld.Init(patchContext);
            APIExtensions.Init(patchContext);
            ProjectorFixPatch.Patch(patchContext);
            DrillTreesFix.Patch(patchContext);
            FallingThroughVoxelsFix.Patch(patchContext);
            TurretsNotShootingFix.Patch(patchContext);
            UndestructableVoxels.Patch(patchContext);
            ChangeFillRatioFix.Patch(patchContext);
            PlayerTracker.AddLogFile();
            CustomMedscreen.Patch(patchContext);
            JailBreak.Init(patchContext);
        }

        public override void Update()
        {
            base.Update();
            if (!Config.PlayerTracking)
            {
                return;
            }
            counter++;
            if (counter % (3800 * Config.TrackingFrequencyMin) == 0)
            {
                PlayerTracker.LogPlayerPositions();
            }
        }

        public override void Dispose()
        {
            _config.Save(Path.Combine(StoragePath, "MIG-APIExtender.cfg"));
            base.Dispose();
        }

        public override UserControl CreateControl()
        {
            return new MainControl();
        }
    }
}