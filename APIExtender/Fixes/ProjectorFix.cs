﻿using System.Collections.Generic;
using Sandbox.Game.Entities.Blocks;
using Torch.Managers.PatchManager;
using VRage.Game;
using NAPI;
using Slime;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Game.Multiplayer;
using Torch;
using Sandbox.Game.World;

namespace Slime.Fixes
{
    public static class ProjectorFixPatch
    {
        public static void Patch(PatchContext ctx)
        {
            if (APIExtender.Config.EnableBlueprintProtection)
            {
                ctx.Redirect(typeof(MyProjectorBase), typeof(ProjectorFixPatch), "OnNewBlueprintSuccess");
                Log.Warn("PATCHING: MyProjectorBase->OnNewBlueprintSuccess");
            }
        }

        private static string FindOwnerPlayerName(MyProjectorBase __instance)
        {
            MyPlayer PlayerEntity = null;
            string PlayerName = "BUILDBYADMIN";

            if (__instance.BuiltBy == 0 && __instance.OwnerId == 0)
                return PlayerName;

            PlayerName = "NOTFOUND";
            _ = Sync.Players.TryGetPlayerId(__instance.OwnerId, out MyPlayer.PlayerId ProjectorPlayerID);
            if (ProjectorPlayerID != null)
                PlayerEntity = Sync.Players.GetPlayerById(ProjectorPlayerID);

            if (PlayerEntity != null)
                PlayerName = PlayerEntity.DisplayName;

            if (PlayerName == "NOTFOUND")
            {
                _ = Sync.Players.TryGetPlayerId(__instance.BuiltBy, out MyPlayer.PlayerId ProjectorBuildByPlayerID);
                if (ProjectorBuildByPlayerID != null)
                    PlayerEntity = Sync.Players.GetPlayerById(ProjectorBuildByPlayerID);

                if (PlayerEntity != null)
                    PlayerName = PlayerEntity.DisplayName;
                else
                    PlayerName = "NPC or Player is Offline";
            }

            return PlayerName;
        }

        private static void OnNewBlueprintSuccess(MyProjectorBase __instance, ref List<MyObjectBuilder_CubeGrid> projectedGrids)
        {
            if (__instance == null)
                Log.Warn("No projector?");

            string PlayerName = FindOwnerPlayerName(__instance);

            RemoveStockPile(projectedGrids[0], PlayerName);
        }

        private static void RemoveStockPile(MyObjectBuilder_CubeGrid grid, string PlayerName)
        {
            if (grid == null)
                return;

            foreach (var block in grid.CubeBlocks)
            {
                switch (block)
                {
                    case MyObjectBuilder_HydrogenEngine HydrogenEngine:
                        HydrogenEngine.Capacity = 0f;
                        break;
                    case MyObjectBuilder_GasTank AnyTank:
                        AnyTank.FilledRatio = 0f;
                        break;
                    case MyObjectBuilder_ProjectorBase projector:
                        projector.ProjectedGrids = null;
                        break;
                }

                if ((block.ComponentContainer?.Components) != null)
                {
                    foreach (var index in block.ComponentContainer?.Components)
                    {
                        var BlockInspected = block;

                        if (index.Component is MyObjectBuilder_Inventory inv)
                        {
                            if (inv.Volume != null)
                            {
                                if (BlockInspected.TypeId == typeof(MyObjectBuilder_CargoContainer))
                                {
                                    // will be set to correct volume from MyObjectBuilder_InventoryComponentDefinition by game code.
                                    inv.Volume = null;
                                }

                                var def = BlockInspected.Definition();
                                switch (def)
                                {
                                    // calc here is like this: inv.Volume = Number * 5000 = Actual InGame Inventory Volume.
                                    //case MyAssemblerDefinition:
                                    //case MySurvivalKitDefinition:
                                    //case MyRefineryDefinition:
                                    //case MyOxygenGeneratorDefinition:
                                    //case MyGasTankDefinition tank:
                                    case MyProductionBlockDefinition productionblock:
                                        var productionblockMaxVolume = (VRage.MyFixedPoint?)productionblock.InventoryMaxVolume;
                                        var productionblockVolume = (VRage.MyFixedPoint?)productionblock.InventorySize.Volume;

                                        if (productionblockMaxVolume > productionblockVolume)
                                            inv.Volume = productionblockMaxVolume;
                                        else
                                            inv.Volume = productionblockVolume;
                                        break;
                                    case MyStoreBlockDefinition storeblock:
                                        // some mods for economy have large inventory for custom store block, allow reasonable amount.
                                        if (inv.Volume > 65)
                                        {
                                            Log.Error($"Possible CHEATS detected High Storage Volume in projected block {BlockInspected.TypeId} Owned-BuildBy by {PlayerName}, Volume was {inv.Volume} and now reset to 60");
                                            inv.Volume = 60;
                                        }
                                        break;
                                    case MyReactorDefinition reactor:
                                        var reactorMaxVolume = (VRage.MyFixedPoint?)reactor.InventoryMaxVolume;
                                        var reactorVolume = (VRage.MyFixedPoint?)reactor.InventorySize.Volume;

                                        if (reactorMaxVolume > reactorVolume)
                                            inv.Volume = reactorMaxVolume;
                                        else
                                            inv.Volume = reactorVolume;
                                        break;
                                    case MyConveyorSorterDefinition sorter:
                                        inv.Volume = (VRage.MyFixedPoint?)sorter.InventorySize.Volume;
                                        break;
                                    case MyTargetDummyBlockDefinition dummy:
                                        inv.Volume = (VRage.MyFixedPoint?)dummy.InventorySize.Volume;
                                        break;
                                    case MyWeaponBlockDefinition weaponblock:
                                        if (weaponblock.InventoryMaxVolume > 0 && weaponblock.InventoryMaxVolume < 30)
                                            inv.Volume = (VRage.MyFixedPoint?)weaponblock.InventoryMaxVolume;
                                        else
                                            inv.Volume = 10;
                                        break;
                                    case MyShipConnectorDefinition connector:
                                        // ship connector default volume is 8 and has no normal definition, volume is = (base.BlockDefinition.Size * base.CubeGrid.GridSize * 0.8f).Volume
                                        if (inv.Volume > 16)
                                        {
                                            Log.Error($"Possible CHEATS detected High Storage Volume in projected block {BlockInspected.TypeId} Owned-BuildBy by {PlayerName}, Volume was {inv.Volume} and now reset to 8");
                                            inv.Volume = 8;
                                        }
                                        break;
                                    case MyShipDrillDefinition drillblock:
                                    case MyShipGrinderDefinition grinderblock:
                                    case MyShipWelderDefinition welderblock:
                                        // allow custom drill/welder/grinder to have large inventory, do not allow insane amount and reset to 25
                                        if (inv.Volume > 30)
                                        {
                                            Log.Error($"Possible CHEATS detected High Storage Volume in projected block {BlockInspected.TypeId} Owned-BuildBy by {PlayerName}, Volume was {inv.Volume} and now reset to 25");
                                            inv.Volume = 25;
                                        }
                                        break;
                                    default:
                                        if (inv.Volume > 20)
                                        {
                                            Log.Error($"Possible CHEATS detected High Storage Volume in projected block {BlockInspected.TypeId} Owned-BuildBy by {PlayerName}, Volume was {inv.Volume} and now reset to 10");
                                            inv.Volume = 10;
                                        }
                                        break;
                                }
                            }

                            inv.Mass = null;
                            if (inv.Items != null) inv.Items.Clear();
                        }
                    }
                }

                // clean all inventory blocks in projection.
                block.ConstructionInventory?.Clear();

                var t = block.GetType();
                t.GetField("Items")?.SetValue(block, null);
                t.GetField("Queue")?.SetValue(block, null);
                t.GetField("Inventory")?.SetValue(block, null);
                t.GetField("InputInventory")?.SetValue(block, null);
                t.GetField("OutputInventory")?.SetValue(block, null);

                if (block.ConstructionStockpile == null || block.ConstructionStockpile.Items.Length == 0)
                    continue;

                block.ConstructionStockpile = null;
            }
        }
    }
}
