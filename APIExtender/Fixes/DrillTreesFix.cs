﻿using NAPI;
using Sandbox.Game.Weapons;
using Sandbox.Game.Weapons.Guns;
using Slime;
using Torch.Managers.PatchManager;
using VRage.Utils;

namespace Slime.Fixes
{
    public static class DrillTreesFix
    {
        public static void Patch(PatchContext patchContext)
        {
            if (APIExtender.Config.EnableDrillTreesFix)
            {
                patchContext.Prefix(typeof(MyDrillBase), typeof(DrillTreesFix), "DrillEnvironmentSector");
            }
        }

        private static bool DrillEnvironmentSector(MyDrillBase __instance, out bool __result, MyDrillSensorBase.DetectionInfo entry, float speedMultiplier, out MyStringHash targetMaterial)
        {
            targetMaterial = MyStringHash.NullOrEmpty; //MyStringHash.GetOrCompute("Wood");
            __result = false;
            return false;
        }
    }

}
