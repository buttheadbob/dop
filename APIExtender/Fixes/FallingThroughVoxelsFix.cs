﻿using System.Reflection;
using NAPI;
using Sandbox.Game.Entities;
using Slime;
using Torch.Managers.PatchManager;
using VRage.Game.Entity;

namespace Slime.Fixes
{
    public static class FallingThroughVoxelsFix
    {
        private static MethodInfo UpdateFloraAndPhysicsInfo;
        public static void Patch(PatchContext patchContext)
        {
            if (APIExtender.Config.FallingThroughVoxelsFix)
            {
                UpdateFloraAndPhysicsInfo = typeof(MyPlanet).easyMethod("UpdateFloraAndPhysics");
                patchContext.Suffix(typeof(MyEntity), typeof(FallingThroughVoxelsFix), "UpdateAfterSimulation");
            }
        }

        public static void UpdateAfterSimulation(MyEntity __instance)
        {
            if (__instance is MyPlanet planet)
            {
                UpdateFloraAndPhysicsInfo.Invoke(planet, new object[] { false });
            }
        }
    }

}
