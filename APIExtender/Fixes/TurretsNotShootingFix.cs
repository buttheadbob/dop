﻿using System.Reflection;
using Torch.Managers.PatchManager;
using VRage.Game;
using NAPI;
using Sandbox;
using Sandbox.Game.Entities;
using Sandbox.Game.Weapons;
using VRage.Game.Components;

namespace Slime.Fixes
{
    public static class TurretsNotShootingFix
    {
        private static MethodInfo RotateModels;
        public static void Patch(PatchContext ctx)
        {
            if (APIExtender.Config.EnableTurretsNotShootingFix)
            {
                ctx.Prefix(typeof(MyLargeTurretBase), typeof(TurretsNotShootingFix), "Init");
                RotateModels = typeof(MyLargeTurretBase).easyMethod("RotateModels");
            }
        }

        private static void Init(MyLargeTurretBase __instance, MyObjectBuilder_CubeBlock objectBuilder, MyCubeGrid cubeGrid)
        {
            __instance.PositionComp.OnPositionChanged += pos => OnPositionChanged(pos, __instance);
        }

        private static void OnPositionChanged(MyPositionComponentBase myPositionComponentBase, MyLargeTurretBase __instance)
        {
            if ((ulong)__instance.EntityId % 100 == MySandboxGame.Static.SimulationFrameCounter % 100)
            {
                RotateModels.Invoke(__instance, null);
            }
        }
    }
}
