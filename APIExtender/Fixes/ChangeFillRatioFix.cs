﻿using System;
using NAPI;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Weapons;
using Sandbox.Game.Weapons.Guns;
using Slime;
using Torch.Managers.PatchManager;
using Torch.Utils;
using VRage.Utils;

namespace Slime.Fixes
{
    public static class ChangeFillRatioFix
    {
        public static void Patch(PatchContext patchContext)
        {
            if (APIExtender.Config.EnableGasChangeThresholdFix)
            {
                patchContext.Prefix(typeof(MyGasTank), typeof(ChangeFillRatioFix), "ChangeFillRatioAmount");
            }
        }

        private static bool ChangeFillRatioAmount(MyGasTank __instance, double newFilledRatio)
        {
            double old_InLitres = Math.Round(__instance.FilledRatio * __instance.Capacity, APIExtender.Config.GasChangeThreshold);
            double new_InLitres = Math.Round(newFilledRatio * __instance.Capacity, APIExtender.Config.GasChangeThreshold);
            if (old_InLitres == new_InLitres)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
