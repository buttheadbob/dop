using VRage.ObjectBuilders;

namespace RebelsCore.Enums
{
    internal static class BlockTypeBuilder
    {
        public static MyObjectBuilderType Refinery => MyObjectBuilderType.Parse("MyObjectBuilder_Refinery");
        public static MyObjectBuilderType Assembler => MyObjectBuilderType.Parse("MyObjectBuilder_Assembler");
        public static MyObjectBuilderType Drill => MyObjectBuilderType.Parse("MyObjectBuilder_Drill");
        public static MyObjectBuilderType Welder => MyObjectBuilderType.Parse("MyObjectBuilder_ShipWelder");
        public static MyObjectBuilderType Grinder => MyObjectBuilderType.Parse("MyObjectBuilder_ShipGrinder");
        public static MyObjectBuilderType Beacon => MyObjectBuilderType.Parse("MyObjectBuilder_Beacon");
    }
}