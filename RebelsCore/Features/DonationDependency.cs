﻿using System.Collections.Generic;

namespace Slime.Mod {
    class DonationDependency {
        public static bool GetDonationList(Dictionary<long, HashSet<string>> __result) {
            var lines = DOP.Instance.Config.DonationsData.Split('\n');
            __result = new Dictionary<long, HashSet<string>>();
            foreach (var x in lines) {
                var p = x.Split(',');
                if (p.Length > 1) {
                    var u = p[0];
                    long user;
                    if (long.TryParse(u, out user)) {
                        var options = new HashSet<string>();
                        for (var y = 1; y < p.Length; y++) {
                            options.Add(p[y]);
                        }
                        __result.Add(user, options);
                    }
                }
            }
            
            return false;
        }
    }
}