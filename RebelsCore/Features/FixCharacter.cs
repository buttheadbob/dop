﻿using System.Collections.Generic;
using Torch.Commands;
using Vector3D = VRageMath.Vector3D;
using Sandbox.ModAPI;
using Sandbox.Game.Entities.Character;
using VRageMath;
using VRage.Game.ModAPI;
using Torch.Commands.Permissions;
using NAPI;

namespace Slime.Features.Commands
{
	[Category("fix")]
    class FixCharacter : CommandModule {
        static Dictionary<ulong, long> lastActivated = new Dictionary<ulong, long>();
        static Dictionary<ulong, long> lastActivated2 = new Dictionary<ulong, long>();

        [Command("player", "Helps you, if you lost control on your character")]
        [Permission(MyPromoteLevel.None)]
        public void FixCharacterCommand() {
            if (Context.Player == null) return;

            if (!DOP.Instance.Config.FixCharacter) { return; }

            if (!this.DelayAction(lastActivated, DOP.Instance.Config.FixCharacterInterval)) { return; }

            RespawnChar(Context.Player);
        }

        [Command("cockpit", "Helps you, if you lost control on your character. Destroys cockpit")]
        [Permission(MyPromoteLevel.None)]
        public void FixCockpitCommand() {
            if (Context.Player == null) return;

            if (!DOP.Instance.Config.FixCharacter) { return; }

            if (!this.DelayAction(lastActivated2, DOP.Instance.Config.FixCharacterInterval)) { return; }

            var list = UtilitesEntity.GetBlocksFromAllGridsParalell((x) => {
                var cnt = x.FatBlock as IMyShipController;
                var ch = cnt?.Pilot as MyCharacter;
                if (ch == null) return false;
                return ch.ControlSteamId == Context.Player.SteamUserId;
            });

            foreach (var x in list) { x.CubeGrid.RazeBlock(x.Position); }

            var player = Context.Player;
            Settings.AddDelayer(120, ()=>RespawnChar(player));
        }

        public void RespawnChar(IMyPlayer pl) {
            var prev = (pl.Character);

            var matrix = new MatrixD(MatrixD.Identity);
            matrix.Translation = prev == null ? Vector3D.Zero : MyAPIGateway.Entities.FindFreePlace(prev.PositionComp.WorldMatrix.Translation, 5f) ?? Vector3D.Zero;

            var newChar = MyCharacter.CreateCharacter(MatrixD.Identity, Vector3.Zero, "Replicant", "Default_Astronaught", null, null);
            MyAPIGateway.Players.SetControlledEntity(pl.SteamUserId, newChar);

            if (prev != null) prev.Close();
        }
    }
}