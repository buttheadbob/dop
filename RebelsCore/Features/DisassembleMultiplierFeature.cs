﻿using Sandbox.Game.World;
using System.Runtime.CompilerServices;
using Sandbox.Definitions;
using Sandbox.Game.Entities.Cube;
using Torch.Managers.PatchManager;
using NAPI;

namespace Slime.Mod
{
	class DisassembleMultiplierFeature {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void InitHack(PatchContext patchContext) {
            patchContext.Prefix(typeof(MyAssembler), "GetEfficiencyMultiplierForBlueprint", typeof(DisassembleMultiplierFeature), new string[] { "__instance", "__result", "blueprint"});
        }

        public static bool ApplyPrivateSafezoneSettings(MyAssembler __instance, ref float __result, MyBlueprintDefinitionBase blueprint) {
            if (__instance.DisassembleEnabled) {
                __result = DOP.Instance.Config.DisassembleMultiplier * MySession.Static.AssemblerEfficiencyMultiplier;
                return false;
            } else {
                return false;
            }
        }
    }
}