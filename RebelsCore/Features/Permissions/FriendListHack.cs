﻿using NAPI;
using Sandbox.Game.Entities;
using Torch.Managers.PatchManager;
using VRage.Game;
using VRage.Game.Entity;

namespace Slime.Features
{
	class FriendListHack {
        public void InitHack(PatchContext patchContext) {
            patchContext.GetPattern(typeof(MyIDModule).easyMethod("GetRelationPlayerPlayer")).Prefixes.Add(typeof(FriendListHack).protectedMethod(new[] {"__result", "owner", "user", "share", "noFactionResult", "defaultFactionRelations", "defaultShareWithAllRelations"}));
            patchContext.GetPattern(typeof(MyIDModule).easyMethod("GetRelationPlayerBlock")).Prefixes.Add(typeof(FriendListHack).protectedMethod(new[] {"__result", "owner", "user", "defaultFactionRelations", "defaultNoFactionRelation"}));
        }

        public static bool GetRelationPlayerBlock(ref MyRelationsBetweenPlayerAndBlock __result, long owner, long user, MyOwnershipShareModeEnum share, MyRelationsBetweenPlayerAndBlock noFactionResult, MyRelationsBetweenFactions defaultFactionRelations, MyRelationsBetweenPlayerAndBlock defaultShareWithAllRelations) {
            __result = PermissionLogic.GetRelationPlayerBlock(owner, user, share, noFactionResult, defaultFactionRelations, defaultShareWithAllRelations);
            return false;
        }


        public static bool GetRelationPlayerPlayer(ref MyRelationsBetweenPlayers __result, long owner, long user, MyRelationsBetweenFactions defaultFactionRelations, MyRelationsBetweenPlayers defaultNoFactionRelation) {
            DOP.Log.Error("RelationPP:" + owner + " | " + user);
            if (owner == user) { __result = MyRelationsBetweenPlayers.Self; }

            __result = MyRelationsBetweenPlayers.Enemies;
            return false;
        }
    }
}