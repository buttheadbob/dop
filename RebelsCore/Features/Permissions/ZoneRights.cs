﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using VRage.Game.ObjectBuilders.Components;

namespace Slime.Features {
    class ZoneRights {
        int friendRights = 0;
        int factionFriendRights = 0;
        int factionAllies = 0;
        int other = 0;
        Dictionary<long, int> specific;

        static int DRILLING = (int) MySafeZoneAction.Drilling;
        static int BUILDING = (int) MySafeZoneAction.Building;
        static int WELDING = (int) MySafeZoneAction.Welding;
        static int GRINDING = (int) MySafeZoneAction.Grinding;
        static int ASSINGN = 512;

        public static StringBuilder GetRightsInfo(int r, StringBuilder sb) {
            if (sb == null) sb = new StringBuilder();

            if (r == 0) return sb.Append("None ");

            if ((r & DRILLING) == DRILLING) sb.Append("Drill ");
            if ((r & BUILDING) == BUILDING) sb.Append("Build ");
            if ((r & WELDING) == WELDING) sb.Append("Weld ");
            if ((r & GRINDING) == GRINDING) sb.Append("Grind ");
            if ((r & ASSINGN) == ASSINGN) sb.Append("Assign ");

            return sb;
        }

        public string PrintAllRights() {
            var sb = new StringBuilder();
            sb.Append("Friends: ");
            GetRightsInfo(friendRights, sb).Append("\n");
            sb.Append("FactionFriend: ");
            GetRightsInfo(factionFriendRights, sb).Append("\n");
            sb.Append("Allies: ");
            GetRightsInfo(factionAllies, sb).Append("\n");
            sb.Append("Other: ");
            GetRightsInfo(other, sb).Append("\n");

            if (specific != null && specific.Count != 0) {
                sb.Append("Specific:\n");
                foreach (var x in specific) {
                    sb.Append(x.Key).Append(": ");
                    GetRightsInfo(x.Value, sb).Append("\n");
                }
            }

            return sb.ToString();
        }

        public void SetRights(long userId, int Rights) {
            if (specific == null) specific = new Dictionary<long, int>();
            specific.Add(userId, Rights);
        }

        public void SetFriendRights(int Rights) { friendRights = Rights; }

        public void SetFactionFriendRights(int Rights) { factionFriendRights = Rights; }

        public void SetFactionAllyRights(int Rights) { factionFriendRights = Rights; }

        public void OtherRights(int Rights) { factionFriendRights = Rights; }

        public bool CheckRights(long userId, MySafeZoneAction action, bool friend, bool factionFriend, bool factionAlly) {
            var rights = GetRights(userId, friend, factionFriend, factionAlly);
            var a = (int) action;
            return (a & rights) == a;
        }

        public int GetRights(long userId, bool friend, bool factionFriend, bool factionAlly) {
            if (specific != null && specific.ContainsKey(userId)) { return specific[userId]; }

            var x = 0;

            if (friend) x |= friendRights;
            if (factionFriend) x |= factionFriendRights;
            if (factionAlly) x |= factionAllies;
            x |= other;

            return x;
        }


        public void Serialize(BinaryWriter bw) {
            bw.Write(friendRights);
            bw.Write(factionFriendRights);
            bw.Write(factionAllies);
            bw.Write(other);
            if (specific != null) {
                bw.Write(specific.Count);
                foreach (var x in specific) {
                    bw.Write(x.Key);
                    bw.Write(x.Value);
                }
            }
        }

        public void Deserialize(BinaryReader bw) {
            friendRights = bw.ReadInt32();
            factionFriendRights = bw.ReadInt32();
            factionAllies = bw.ReadInt32();
            other = bw.ReadInt32();

            var count = bw.ReadInt32();
            if (count != 0) {
                specific = new Dictionary<long, int>();
                for (var x = 0; x < count; x++) {
                    var k = bw.ReadInt64();
                    var v = bw.ReadInt32();
                    specific.Add(k, v);
                }
            }
        }
    }
}