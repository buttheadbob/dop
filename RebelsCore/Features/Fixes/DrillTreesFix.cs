﻿using NAPI;
using Sandbox.Game.Weapons;
using Sandbox.Game.Weapons.Guns;
using Torch.Managers.PatchManager;
using VRage.Utils;

namespace Slime.Features
{
	class DrillTreesFix {
        public void InitHack (PatchContext patchContext) {
            patchContext.Prefix(typeof(MyDrillBase), "DrillEnvironmentSector", typeof(DrillTreesFix), new[] {"__instance", "__result", "entry", "speedMultiplier", "targetMaterial"});
        }

        private static bool DrillEnvironmentSector(MyDrillBase __instance, out bool __result, MyDrillSensorBase.DetectionInfo entry, float speedMultiplier, out MyStringHash targetMaterial) {
            targetMaterial = MyStringHash.NullOrEmpty; //MyStringHash.GetOrCompute("Wood");
            __result = false;
            return false;
        }
    }
}