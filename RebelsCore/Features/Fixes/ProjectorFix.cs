﻿using VRage.Game;
using Sandbox.Game.Entities.Blocks;
using System;
using System.Collections.Generic;
using Torch.Managers.PatchManager;
using Sandbox.Common.ObjectBuilders;
using NAPI;

namespace Slime.Features
{
	class ProjectorFix {
        //[MethodImpl(MethodImplOptions.AggressiveInlining)]

        public void InitHack(PatchContext patchContext) {
            patchContext.Prefix(typeof(MyProjectorBase), "SetNewBlueprint", typeof(ProjectorFix), new[] {"__instance", "gridBuilders"});
        }

        private static bool SetNewBlueprint(MyProjectorBase __instance, List<MyObjectBuilder_CubeGrid> gridBuilders) {
            try {
                Filter(__instance, gridBuilders);
                return true;
            } catch (Exception e) { DOP.Log.Error(e, "INVOKE"); }

            return false;
        }

        private static void Filter(MyProjectorBase __instance, List<MyObjectBuilder_CubeGrid> projectedGrids) {
            foreach (var g in projectedGrids) {
                g.JumpDriveDirection = null;
                g.JumpRemainingTime = 0;

                for (var x = 0; x < g.CubeBlocks.Count; x++) { // in g.CubeBlocks) {
                    var block = g.CubeBlocks[x];

                    var proj = block as MyObjectBuilder_Projector;
                    if (proj != null) {
                        if (proj.ProjectedGrid != null) { Filter(__instance, new List<MyObjectBuilder_CubeGrid>() {proj.ProjectedGrid}); }
                        if (proj.ProjectedGrids != null) { Filter(__instance, proj.ProjectedGrids); }
                    }

                    var cc = block.ComponentContainer;
                    var oxy = block as MyObjectBuilder_OxygenGenerator;
                    if (oxy != null && oxy.Inventory != null) { oxy.Inventory.Clear(); }

                    var gas = block as MyObjectBuilder_GasTank;
                    if (gas != null && gas.Inventory != null) { gas.Inventory.Clear(); }

                    if (cc != null && cc.Components != null) {
                        foreach (var cmp in cc.Components) {
                            var inv = (cmp.Component as MyObjectBuilder_Inventory);
                            if (inv != null) { inv.Clear(); }
                        }
                    }
                }
            }
        }
    }
}