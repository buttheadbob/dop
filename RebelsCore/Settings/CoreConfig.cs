using Slime.GUI;
using System;
using System.Windows;
using Torch;
using Torch.Views;
using VRageMath;

namespace Slime.Config {
    public partial class CoreConfig : ViewModel {
        private string _authKey;
        private bool _pluginenabled;

        private bool _SAEnabled = false;
        private string _SAData = "";

        private bool _explosionProtectionEnabled;
        private int _explosionProtectionRadius = 20;
        

        private bool _projectorFix;
        private bool _treesFix;
        
        private int _trackInterval = 60 * 5;
        private bool _trackPhysics = false;
        private bool _trackCrashes = false;
        private bool _trackEnabled = false;
        private bool _trackPlayersCount = true;
        
        
            

        public string AuthKey { get { return _authKey; } set { SetValue<string>(ref _authKey, value); } }
        public bool PluginEnabled { get { return _pluginenabled; } set { SetValue(ref _pluginenabled, value); } }

        //==========================================================================================================================

        [DisplayTab(GroupName = "Explosions", Tab = "AntiGrief", Name = "Explosion Protection", Description = "Players can't kill your server by shooting in 99999 explosives")]
        public bool ExplosionProtectionEnabled { get { return _explosionProtectionEnabled; } set { SetValue(ref _explosionProtectionEnabled, value); } }

        [DisplayTab(GroupName = "Explosions", Tab = "AntiGrief", Name = "Explosion Protection Meters")]
        public int ExplosionProtectionMeters { get { return _explosionProtectionRadius; } set { SetValue(ref _explosionProtectionRadius, value); } }

        [DisplayTab(GroupName = "Projector", Tab = "AntiGrief", Name = "Fix Projector Hacks", Description = "Fixes known for me projector problems:\n1) Spawner of any items in any amounts\n2) Teleporter anywhere")]
        public bool FixProjector { get { return _projectorFix; } set { SetValue(ref _projectorFix, value); } }

        [DisplayTab(GroupName = "Projector", Tab = "AntiGrief", Name = "Fix trees", Description = "Fixes Sandbox.Game.Weapons.MyDrillBase.DrillEnvironmentSector")]
        public bool FixTrees { get { return _treesFix; } set { SetValue(ref _treesFix, value); } }


        //==========================================================================================================================
        //MyLargeGatlingTurret.OnModelChange()

        [DisplayTab(GroupName = "Tracking", Tab = "Features")]
        public bool TrackEnabled { get { return _trackEnabled; } set { SetValue(ref _trackEnabled, value); } }
        
        [DisplayTab(GroupName = "Tracking", Tab = "Features", Name = "TrackInterval (sec)")]
        public int TrackInterval { get { return _trackInterval; }
            set {
                var vv  = MathHelper.Clamp(value, 10, 15 * 60);
                SetValue(ref _trackInterval, vv);
            } }
        
        [DisplayTab(GroupName = "Tracking", Tab = "Features", Description = "How much time Havok takes time")]
        public bool TrackPhysics { get { return _trackPhysics; }
            set {
                SetValue(ref _trackPhysics, value);
            } }
        
        [DisplayTab(GroupName = "Tracking", Tab = "Features", Description = "Send crash-info to server")]
        public bool TrackCrashes { get { return _trackCrashes; }
            set {
                SetValue(ref _trackCrashes, value);
            } }

        [DisplayTab(GroupName = "Tracking", Tab = "Features")]
        public bool TrackPlayersCount { get { return _trackPlayersCount; } set { SetValue(ref _trackPlayersCount, value); } }
        
        
        [DisplayTab(GroupName = "SpecialAbilities", Tab = "API", Name = "Enabled")]
        public bool DonationsEnabled { get { return _SAEnabled; } set { SetValue(ref _SAEnabled, value); } }
        
        
        [DisplayTab(GroupName = "SpecialAbilities", Tab = "API", Name = "RawData")]
        public string DonationsData { get { return _SAData; } set { SetValue(ref _SAData, value); } }
    }
}