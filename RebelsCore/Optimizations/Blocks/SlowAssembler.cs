﻿using System;
using System.Collections.Generic;
using NAPI;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.World;
using Torch.Managers.PatchManager;
using VRage;
using VRage.Game;
using VRage.Game.ModAPI;

namespace Slime.Features
{
	/**
     * BETTER TO OPTIMIZE GRID SYSTEM & PULL REQUESTS
     */
	class HackAssembler : Hack {
        public HackAssembler() {
            ignoredSubtypes.Add("Trader");
            ignoredSubtypes.Add("TraderPlanet");
        }

        public override bool validateFat(IMyCubeBlock block, string sn) {
            return (block is MyAssembler) && base.validateFat(block, sn);
        }

        public override void InitHack(PatchContext patchContext) {  }

        public override void CreateHack(IMyCubeBlock block) { new SlowAssembler(block as MyAssembler); }
    }

    class SlowAssembler : SlowProductionBlock {
        //public const int SLOW = 30;
        //public const int MAX_SPEED_BOOST = 3;
        //public const int MAX_DELTA_TIME = (int) ((SLOW * 10 * 16.6d) * MAX_SPEED_BOOST); //300 frames * 16.6 ms * 10 times

        const int INTERVAL_ASS = 30;
        const float minRequired = 1.2f;
        const float maxRequired = 4f;

        MyAssembler block;
        AutoTimer timer = new AutoTimer(INTERVAL_ASS, DOP.Random.Next(INTERVAL_ASS)); //+5
        AutoTimer timer2 = new AutoTimer(INTERVAL_ASS / 5, DOP.Random.Next(INTERVAL_ASS));

        private readonly Dictionary<MyDefinitionId, MyFixedPoint> hasItems = new Dictionary<MyDefinitionId, MyFixedPoint>();
        private readonly Dictionary<MyDefinitionId, MyFixedPoint> allItems = new Dictionary<MyDefinitionId, MyFixedPoint>();


        static Dictionary<MyDefinitionId, MyFixedPoint> pulls = new Dictionary<MyDefinitionId, MyFixedPoint>() {
           {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Iron"), 60},
           {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Stone"), (MyFixedPoint) 2.5},
           {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Silicon"), 15},
           {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Nickel"), 12},
           {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Silver"), (MyFixedPoint) 3.3},
           {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Cobalt"), (MyFixedPoint) 22},
           {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Gold"), (MyFixedPoint) 1},
           {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Magnesium"), (MyFixedPoint) 0.33},
           //{ MyDefinitionId.Parse ("MyObjectBuilder_Ingot/Uranium"), 0 },
           //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/Platinum"), (MyFixedPoint) 0.1},
           //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/AluminumIngot"), (MyFixedPoint) 0.02},
           //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/CopperIngot"), (MyFixedPoint) 0.01},
           //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/PalladiumIngot"), (MyFixedPoint) 0.001},
           //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/Cez"), (MyFixedPoint) 0.001},
           //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/Rad"), (MyFixedPoint) 0.001},
           //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/Kaliforn"), (MyFixedPoint) 0.001}
       };

        static Dictionary<MyDefinitionId, MyFixedPoint> minimum = new Dictionary<MyDefinitionId, MyFixedPoint>() {
             {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Iron"), 2000},
             {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Stone"), 50},
             {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Silicon"), 15},
             {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Nickel"), 70},
             {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Silver"), 20},
             {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Cobalt"), 220},
             {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Gold"), 10},
             {MyDefinitionId.Parse("MyObjectBuilder_Ingot/Magnesium"), 2},
             //{ MyDefinitionId.Parse ("MyObjectBuilder_Ingot/Uranium"), 0 },
             //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/Platinum"), 2},
             //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/AluminumIngot"), (MyFixedPoint) 7.5},
             //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/CopperIngot"), 5},
             //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/PalladiumIngot"), 1},
             //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/Cez"), 30},
             //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/Rad"), 10},
             //{MyDefinitionId.Parse("MyObjectBuilder_Ingot/Kaliforn"), (MyFixedPoint) 0.75}
         };

        public SlowAssembler(MyAssembler assembler) : base(assembler) { block = assembler; }

        public float speed() { return MySession.Static.AssemblerSpeedMultiplier * (((MyAssemblerDefinition) block.BlockDefinition).AssemblySpeed + block.UpgradeValues["Productivity"]); }
        
        protected override bool Before10() {
            if (!DOP.Instance.Config.EnabledSlowdownAssembler) return true;
            if (block == null) return true;
            if (block.OutputInventory == null || block.InputInventory == null) return true;

            if (timer2.tick()) {
                pull();
                return false;
            } else if (timer.tick()) { return true; } else { return false; }
        }
        
        public void pull() {
            //TODO pull out components
            if (block.DisassembleEnabled) return;

            var mlt = extraTime > 0 ? 10 : 1;

            var inv = block.InputInventory;
            var spd = speed() * (INTERVAL_ASS / 60f);

            if (hasItems.Count == 0) {
                hasItems.Clear();
                inv.CountItems(hasItems);

                foreach (var x in pulls) {
                    var minReq = (MyFixedPoint) Math.Max((double) (x.Value * spd), (double) minimum[x.Key]) * minRequired * mlt;
                    var maxReq = x.Value * spd * maxRequired * mlt;

                    if (!hasItems.ContainsKey(x.Key)) { hasItems.Add(x.Key, maxReq); } else {
                        if (hasItems[x.Key] < minReq) { hasItems[x.Key] = maxReq - hasItems[x.Key]; } else { hasItems[x.Key] = 0; }
                    }
                }
            } else {
                while (hasItems.Count > 0) {
                    var x = hasItems.FirstPair();
                    hasItems.Remove(x.Key);
                    if (x.Value == 0) { continue; } else {
                        MyInventoryConstraint mic = new MyInventoryConstraint("");
                        mic.Add(x.Key);
                        //var got = MyGridConveyorSystem.PullAllRequest(block, block.InputInventory, block.OwnerId, mic, x.Value, false);
                        break;
                    }
                }
            }
        }
    }
}