﻿using NAPI;
using Sandbox.Game.Entities.Blocks;
using Torch.Managers.PatchManager;
using VRage.Game.ModAPI;

namespace Slime.Features
{
	class HackSensor : Hack {
        public override void InitHack(PatchContext patchContext) { patchContext.Redirect(typeof(MyGasGenerator), "UpdateAfterSimulation10"); }
        public override void CreateHack(IMyCubeBlock block) { new SlowSensor(block as MySensorBlock); }
        public override bool validateFat(IMyCubeBlock block, string sn) { return (block is MySensorBlock) && base.validateFat(block, sn); }
        public override void Clear() { }
    }

    class SlowSensor : SlowdownHack {
        private AutoTimer timer = new AutoTimer(6, DOP.Random.Next(6));
        public SlowSensor(MySensorBlock block) : base(block) { }

        protected override bool After10() {
            return !DOP.Instance.Config.EnabledSlowdownSensor || timer.tick();
        }
    }
}