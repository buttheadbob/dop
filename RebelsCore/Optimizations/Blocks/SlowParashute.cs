﻿using NAPI;
using SpaceEngineers.Game.Entities.Blocks;
using Torch.Managers.PatchManager;
using VRage.Game.ModAPI;

namespace Slime.Features
{
	class HackParachute : Hack {
        public override void CreateHack(IMyCubeBlock block) { new SlowParachute(block as MyParachute); }

        public override void InitHack(PatchContext patchContext) { patchContext.Redirect(typeof(MyParachute), "UpdateAfterSimulation", "UpdateBeforeSimulation10"); }

        public override bool validateFat(IMyCubeBlock block, string sn) { return (block is MyParachute) && base.validateFat(block, sn); }

        public override void Clear() { SlowH2Gen.hacked.Clear(); }
    }

    class SlowParachute : SlowdownHack {
        private AutoTimer timer = new AutoTimer(8, DOP.Random.Next(8));
        private AutoTimer antiHotMode = new AutoTimer(30);
        private AutoTimer timer10 = new AutoTimer(6, DOP.Random.Next(6));
        private bool hotMode = true;
        private MyParachute block;
        public SlowParachute(MyParachute block) : base(block) { this.block = block; }

        protected override bool After1() {
            if (!DOP.Instance.Config.EnabledSlowdownParachute) return true;
            return hotMode || timer.tick();
        }

        protected override bool Before10() {
            if (!DOP.Instance.Config.EnabledSlowdownParachute) return true;
            if (hotMode) {
                if (antiHotMode.tick()) {
                    hotMode = block.AutoDeploy && block.CubeGrid.Physics != null && block.CubeGrid.Physics.Gravity.Sum > 0.04;
                }
            }
            return hotMode || timer10.tick();
        }
    }
}