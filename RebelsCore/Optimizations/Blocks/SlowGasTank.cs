﻿using System;
using System.Reflection;
using NAPI;
using Sandbox.Game.Entities.Blocks;
using Torch.Managers.PatchManager;
using VRage.Game.ModAPI;

namespace Slime.Features
{
	class HackGasTank : Hack {
        public override void CreateHack(IMyCubeBlock block) { new SlowGasTank(block as MyGasTank); }

        public override void InitHack(PatchContext patchContext) {
            patchContext.Redirect(typeof(MyGasTank), "UpdateAfterSimulation");
            patchContext.Redirect(typeof(MyGasTank), "UpdateAfterSimulation10");
        }
        public override bool validateFat(IMyCubeBlock block, string sn) { return (block is MyGasTank) && base.validateFat(block, sn); }
    }

    class SlowGasTank : SlowdownHack {
        private static readonly MethodInfo method = typeof(MyGasTank).GetMethod("ExecuteGasTransfer", BindingFlags.Instance | BindingFlags.NonPublic) ?? throw new Exception("Failed to find patch method");
        private static readonly int INTERVAL_H2 = 20;
        //private static readonly int INTERVAL_H222 = 60;
        private static readonly int INTERVAL_H22 = 6;
        private MyGasTank block;
        private AutoTimer timer = new AutoTimer(INTERVAL_H2, DOP.Random.Next(INTERVAL_H2));

        //private AutoTimer timer2 = new AutoTimer(INTERVAL_H222, DOP.Random.Next(INTERVAL_H222));
        private AutoTimer timer10 = new AutoTimer(INTERVAL_H22, DOP.Random.Next(INTERVAL_H22));
        public SlowGasTank(MyGasTank block) : base(block) { this.block = block; }

        protected override bool After1() {
            if (!DOP.Instance.Config.EnabledSlowdownGasTank || timer.tick()) return true;
            //if (timer2.tick()) {
                method.Invoke(block, null);
            //}
            return false;
        }

        protected override bool After10() {
            return !DOP.Instance.Config.EnabledSlowdownGasTank ? true : timer10.tick();
        }
    }
}