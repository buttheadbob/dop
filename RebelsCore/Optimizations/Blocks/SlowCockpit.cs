﻿using NAPI;
using Sandbox.Game.Entities;
using Torch.Managers.PatchManager;
using VRage.Game.ModAPI;

namespace Slime.Features
{
	class HackCockpit : Hack {
        public override void CreateHack(IMyCubeBlock block) { new SlowCockpit(block as MyCockpit); }
        public override void InitHack(PatchContext patchContext) { patchContext.Redirect(typeof(MyCockpit), "UpdateBeforeSimulation", "UpdateBeforeSimulation10", "UpdateBeforeSimulation100", "UpdateAfterSimulation10", "UpdateAfterSimulation100"); }
        public override bool validateFat(IMyCubeBlock block, string sn) { return (block is MyCockpit) && base.validateFat(block, sn); }
    }

    class SlowCockpit : SlowdownHack {
        private AutoTimer timer100 = new AutoTimer(100, DOP.Random.Next(1000));
        private AutoTimer timer10 = new AutoTimer(1000, DOP.Random.Next(1000));
        private AutoTimer timer1 = new AutoTimer(1000, DOP.Random.Next(1000));

        private AutoTimer timerA10 = new AutoTimer(1000, DOP.Random.Next(1000));
        private AutoTimer timerA100 = new AutoTimer(1000, DOP.Random.Next(1000));

        public SlowCockpit(MyCockpit block) : base(block) { }
        protected override bool Before100() { return !DOP.Instance.Config.EnabledSlowdownCockpit ? true : timer100.tick(); }
        protected override bool Before10() { return !DOP.Instance.Config.EnabledSlowdownCockpit ? true : timer10.tick(); }
        protected override bool Before1() { return !DOP.Instance.Config.EnabledSlowdownCockpit ? true : timer1.tick(); }
        protected override bool After10() { return !DOP.Instance.Config.EnabledSlowdownCockpit ? true : timerA10.tick(); }
        protected override bool After100() { return !DOP.Instance.Config.EnabledSlowdownCockpit ? true : timerA100.tick(); }
    }
}