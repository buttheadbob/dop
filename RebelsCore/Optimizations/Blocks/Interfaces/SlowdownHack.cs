﻿using System.Runtime.CompilerServices;
using VRage.Game.Entity;

namespace Slime.Features {
    public abstract class SlowdownHack {
        protected virtual bool Before1() { return true; }
        protected virtual bool Before10() { return true; }
        protected virtual bool Before100() { return true; }
        protected virtual bool After1() { return true; }
        protected virtual bool After10() { return true; }
        protected virtual bool After100() { return true; }

        protected SlowdownHack(MyEntity entity) {
            SlowLogic.refsHack.Add(entity.EntityId, this);
            entity.OnMarkForClose += Destroy;
        }

        private void Destroy(MyEntity obj) {
            if (!SlowLogic.refsHack.ContainsKey(obj.EntityId)) { SlowLogic.refsHack.Add(obj.EntityId, this); }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static SlowdownHack get(MyEntity __instance) {
            SlowdownHack hack;
            if (SlowLogic.refsHack.TryGetValue(__instance.EntityId, out hack)) {
                return hack;
            }

            //DOP.LogSpamming(13636, (x,y)=>x.Append("Hack ").Append(__instance).Append(" Failed").Append(y));
            return null;
        }

        public static bool UpdateBeforeSimulation(MyEntity __instance) { return get(__instance)?.Before1() ?? true; }
        public static bool UpdateBeforeSimulation10(MyEntity __instance) { return get(__instance)?.Before10() ?? true; }
        public static bool UpdateBeforeSimulation100(MyEntity __instance) { return get(__instance)?.Before100() ?? true; }
        public static bool UpdateAfterSimulation(MyEntity __instance) { return get(__instance)?.After1() ?? true; }
        public static bool UpdateAfterSimulation10(MyEntity __instance) { return get(__instance)?.After10() ?? true; }
        public static bool UpdateAfterSimulation100(MyEntity __instance) { return get(__instance)?.After100() ?? true; }
    }
}