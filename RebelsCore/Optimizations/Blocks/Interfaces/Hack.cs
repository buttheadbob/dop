﻿using System.Collections.Generic;
using Torch.Managers.PatchManager;
using VRage.Game.ModAPI;

namespace Slime.Features {
    abstract class Hack {
        public HashSet<string> ignoredSubtypes = new HashSet<string>();

        public virtual bool validateFat(IMyCubeBlock block, string sn) { return !ignoredSubtypes.Contains(sn); }

        public virtual bool validateSlim(IMySlimBlock block, string type, string sn) { return false; }
        public virtual bool isSlim() { return false; }
        public virtual void CreateHack(IMyCubeBlock block) { }
        public virtual void CreateHack(IMySlimBlock block) { }
        public virtual void InitHack(PatchContext patchContext) { }
        public virtual void Clear() { }
    }
}