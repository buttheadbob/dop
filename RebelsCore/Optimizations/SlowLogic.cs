﻿using System;
using System.Collections.Generic;
using NAPI;
using ParallelTasks;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.Weapons;
using Sandbox.ModAPI;
using VRage.Game.Entity;

namespace Slime.Features
{
	class SlowLogic : Updatable {
        public static HashSet<Hack> blockHacks = new HashSet<Hack>();

        public static readonly Dictionary<long, SlowdownHack> refsHack = new Dictionary<long, SlowdownHack>();

        public static readonly Dictionary<MyCubeGrid, SlowdownShip> refsShips = new Dictionary<MyCubeGrid, SlowdownShip>();
        public static readonly Dictionary<MyCubeGrid, SlowdownShip> refsShipsToDelete = new Dictionary<MyCubeGrid, SlowdownShip>();

        static int frame = 0;

        public void Update1() {
            foreach (var x in refsShipsToDelete) { refsShips.Remove(x.Key); }
            foreach (var x in refsShips) { x.Value.Tick(); }
        }

        public static void Clear() {
            refsShips.Clear();
            refsShipsToDelete.Clear();
            MyEntities.OnEntityAdd -= EntityAdded;
        }

        public static void Init() {
            var ent = MyEntities.GetEntities();
            foreach (var e in ent) EntityAdded(e);
            MyEntities.OnEntityAdd += EntityAdded;
        }

        private static void EntityAdded(MyEntity myEntity) {
            if (myEntity is MyCubeGrid x) { slowGrid(x); }
        }

        private static void slowGrid(MyCubeGrid x) {
            x.OnFatBlockAdded -= X_OnFatBlockAdded;
            x.OnFatBlockAdded += X_OnFatBlockAdded;
            x.OnFatBlockRemoved -= X_OnFatBlockRemoved;
            x.OnFatBlockRemoved += X_OnFatBlockRemoved;

            var blocks = x.GetBlocks();
            var filtered = new HashSet<MyCubeBlock>();
            var filtered2 = new HashSet<MySlimBlock>();
            
            HashSet<MyLargeTurretBase> turrets = new HashSet<MyLargeTurretBase>();
            HashSet<MyProductionBlock> productionBlocks = new HashSet<MyProductionBlock>();
            HashSet<MyCryoChamber> cryos = new HashSet<MyCryoChamber>();
            HashSet<MyProjectorBase> projectors = new HashSet<MyProjectorBase>();
            HashSet<MyCubeBlock> excluded = new HashSet<MyCubeBlock>();
            HashSet<IMyFunctionalBlock> excludedWorking = new HashSet<IMyFunctionalBlock>();

            Parallel.ForEach(blocks, xx => {
                try {
                    var fat = xx.FatBlock;
                    var sn = xx.BlockDefinition.Id.SubtypeName;
                    if (fat != null) {
                        switch (fat) {
                            case MyLargeTurretBase bb: {
                                lock (turrets) { turrets.Add(bb); }
                                break;
                            }
                            case MyProductionBlock bb: {
                                lock (productionBlocks) { productionBlocks.Add(bb); }
                                break;
                            }
                            case MyCryoChamber bb: {
                                lock (cryos) { cryos.Add(bb); }
                                break;
                            }
                            case MyProjectorBase bb: {
                                lock (projectors) { projectors.Add(bb); }
                                break;
                            }
                        }

                        foreach (var k in blockHacks) {
                            if (!k.validateFat(fat, sn)) continue;
                            lock (filtered2) {
                                filtered.Add(fat);
                            }
                        }
                    } else {
                        var type = xx.BlockDefinition.Id.TypeId.ToString();
                        foreach (var k in blockHacks) {
                            if (!k.isSlim() || !k.validateSlim(xx, type, sn)) continue;
                            lock (filtered2) {
                                filtered2.Add(xx);
                            }
                        }
                    }
                } catch (Exception e) { Log.Fatal(e, "SlowGrid" + x + " " + xx.BlockDefinition.Id.SubtypeName); }
            });

            refsShips.Add(x, new ShipSubpart(x, turrets, productionBlocks, cryos, projectors, excluded, excludedWorking));

            foreach (var y in filtered) { slowBlock(y); }
        }


        private static void X_OnFatBlockAdded(MyCubeBlock obj) {
            slowBlock(obj);
            if (!refsShips.ContainsKey(obj.CubeGrid)) return;
            
            var grid = refsShips[obj.CubeGrid];
            ((ShipSubpart) grid).BlockAddedOrRemoved(obj, true);
        }

        private static void X_OnFatBlockRemoved(MyCubeBlock obj) {
            if (!refsShips.ContainsKey(obj.CubeGrid)) return;
            var grid = refsShips[obj.CubeGrid];
            ((ShipSubpart) grid).BlockAddedOrRemoved(obj, false);
        }

        private static void slowBlock(MyCubeBlock fat) {
            if (fat == null || refsHack.ContainsKey(fat.EntityId)) return;
            var sn = fat.BlockDefinition.Id.SubtypeName;
            foreach (var x in blockHacks) {
                if (x.validateFat(fat, sn)) { x.CreateHack(fat); }
            }
        }
    }
}