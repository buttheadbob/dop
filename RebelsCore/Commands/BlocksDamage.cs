﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog.Fluent;
using RebelsCore.Enums;
using RebelsCore.Utilities;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Torch.Commands;
using VRage.ObjectBuilders;

namespace RebelsCore.Commands
{
    [Category("damage")]
    public class BlocksDamage : CommandModule
    {
        public RebelsCore Plugin => (RebelsCore) Context.Plugin;
        
        [Command("test", "Test command")]
        public void Test()
        {
            try
            {
                var subtypeId = "LargeBlockDrill";
                var gridsAll = MyEntities.GetEntities().OfType<MyCubeGrid>().ToList();
                var blocksAll = gridsAll.SelectMany(grid =>
                    grid.GetBlocks()).ToList();
//                var refineryBlocks = blocksAll.Where(block => block.BlockDefinition.Id.TypeId == BlockTypeBuilder.Refinery).ToList();
                var largeRefineryBlocks = blocksAll.Where(block => block.BlockDefinition.Id.SubtypeName == subtypeId).ToList();

//                UtilitiesDamageIntegrity.DamageBlocks(blocksToDamage, integrityDamage);
//                Context.Respond($"Damaged {blocksToDamage.Count} refinery blocks by {integrityDamage} integrity.");
                Context.Respond($"gridsAll: {gridsAll}");
                Context.Respond($"blocksAll: {blocksAll}");
//                Context.Respond($"gridsAll: {refineryBlocks}");
                Context.Respond($"gridsAll: {largeRefineryBlocks}");
            }
            catch (Exception e)
            {
                Log.Error($"ERROR: {e.Message}");
            }
        }
        
        /*[Command("block", "Damage block of subtypeId by integrity")]
        public void DamageBlock(string subtypeId, float integrityDamage)
        {
            try
            {
                var blocksToDamage = (List<MySlimBlock>) UtilitesEntity.GetBlocksBySubtypeId(subtypeId);
                UtilitiesDamageIntegrity.DamageBlocks(blocksToDamage, integrityDamage);
                Context.Respond($"Damaged {blocksToDamage.Count} blocks of subtype {subtypeId} by {integrityDamage} integrity.");
            }
            catch (Exception e)
            {
                Log.Error($"ERROR: {e.Message}");
            }
        }*/

        [Command("refinery", "Damage refinery by integrity")]
        public void DamageRefinery(string subtypeId, float integrityDamage)
        {
            try
            {
                var blocksToDamage = (List<MySlimBlock>) UtilitesEntity.GetRefineryBySubtypeId(subtypeId);
                UtilitiesDamageIntegrity.DamageBlocks(blocksToDamage, integrityDamage);
                Context.Respond($"Damaged {blocksToDamage.Count} refinery blocks by {integrityDamage} integrity.");
            }
            catch (Exception e)
            {
                Log.Error($"ERROR: {e.Message}");
            }
        }
        
        [Command("assembler", "Damage assembler by integrity")]
        public void DamageAssembler(string subtypeId, float integrityDamage)
        {
            try
            {
                var blocksToDamage = (List<MySlimBlock>) UtilitesEntity.GetAssemblerBySubtypeId(subtypeId);
                UtilitiesDamageIntegrity.DamageBlocks(blocksToDamage, integrityDamage);
                Context.Respond($"Damaged {blocksToDamage.Count} assembler blocks by {integrityDamage} integrity.");
            }
            catch (Exception e)
            {
                Log.Error($"ERROR: {e.Message}");
            }
        }
        
        [Command("drill", "Damage drill by integrity")]
        public void DamageDrill(string subtypeId, float integrityDamage)
        {
            try
            {
                var blocksToDamage = (List<MySlimBlock>) UtilitesEntity.GetDrillBySubtypeId(subtypeId);
                UtilitiesDamageIntegrity.DamageBlocks(blocksToDamage, integrityDamage);
                Context.Respond($"Damaged {blocksToDamage.Count} drill blocks by {integrityDamage} integrity.");
            }
            catch (Exception e)
            {
                Log.Error($"ERROR: {e.Message}");
            }
        }
        [Command("welder", "Damage welder by integrity")]
        public void DamageWelder(string subtypeId, float integrityDamage)
        {
            try
            {
                var blocksToDamage = (List<MySlimBlock>) UtilitesEntity.GetWelderBySubtypeId(subtypeId);
                UtilitiesDamageIntegrity.DamageBlocks(blocksToDamage, integrityDamage);
                Context.Respond($"Damaged {blocksToDamage.Count} welder blocks by {integrityDamage} integrity.");
            }
            catch (Exception e)
            {
                Log.Error($"ERROR: {e.Message}");
            }
        }
        [Command("grinder", "Damage grinder by integrity")]
        public void DamageGrinder(string subtypeId, float integrityDamage)
        {
            try
            {
                var blocksToDamage = (List<MySlimBlock>) UtilitesEntity.GetGrinderBySubtypeId(subtypeId);
                UtilitiesDamageIntegrity.DamageBlocks(blocksToDamage, integrityDamage);
                Context.Respond($"Damaged {blocksToDamage.Count} grinder blocks by {integrityDamage} integrity.");
            }
            catch (Exception e)
            {
                Log.Error($"ERROR: {e.Message}");
            }
        }
        [Command("beacon", "Damage beacon by integrity")]
        public void DamageBeacon(string subtypeId, float integrityDamage)
        {
            try
            {
                var blocksToDamage = (List<MySlimBlock>) UtilitesEntity.GetBeaconBySubtypeId(subtypeId);
                UtilitiesDamageIntegrity.DamageBlocks(blocksToDamage, integrityDamage);
                Context.Respond($"Damaged {blocksToDamage.Count} beacon blocks by {integrityDamage} integrity.");
            }
            catch (Exception e)
            {
                Log.Error($"ERROR: {e.Message}");
            }
        }
    }
}