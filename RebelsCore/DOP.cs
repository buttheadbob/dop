﻿using System;
using System.IO;
using System.Windows.Controls;
using NAPI;
using NLog;
using Slime.Config;
using Slime.GUI;
using Torch;
using Torch.API;
using Torch.API.Managers;
using Torch.API.Plugins;
using Torch.API.Session;
using Torch.Commands;
using Torch.Session;

namespace Slime
{
	public class DOP : TorchPluginBase, IWpfPlugin {
        public static DOP Instance { get; private set; }
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public static readonly Random Random = new Random();
        

        public static void UpdateUI(Action<MainControl> action) {
            try {
                if (Instance._control != null) {
                    Instance._control.Dispatcher.Invoke(() => {
                        try { action.Invoke(DOP.Instance._control as MainControl); } catch (Exception e) { DOP.Log.Error(e, "Something wrong in executing function:" + action); }
                    });
                }
            } catch (Exception e) { DOP.Log.Error(e, "Cant UpdateUI"); }
        }


        private Persistent<CoreConfig> _config;
        public UserControl _control;
        private TorchSessionManager _torchSessionManager;
        public CoreConfig Config => _config?.Data;
        public ITorchSession CurrentSession { get; set; }
        public CommandManager CommandManager { get; set; }

        public UserControl GetControl() {
            if (_control == null) { _control = new MainControl(); }

            return _control;
        }

        public static int counter = 0;
        public static int next = Random.Next(10 * 60 * 60);

        public static SimTracker simTracker = new SimTracker();

        public override void Init(ITorchBase torch) {
            base.Init(torch);
            Instance = this;

			//var factory = typeof(MyComponentFactory).easyField("m_objectFactory").GetValue(null) as MyObjectFactory<MyComponentBuilderAttribute, MyComponentBase>;
			//factory.Re

			_config = Persistent<CoreConfig>.Load(Path.Combine(StoragePath, "DeepOptimizationsPlugin.cfg"));

            _torchSessionManager = Torch.Managers.GetManager<TorchSessionManager>() ?? throw new Exception("Couldn't init plugin");
            _torchSessionManager.SessionStateChanged += SessionChanged;

            AppDomain.CurrentDomain.UnhandledException += HandleException;

            var pgmr = new DependencyManager(torch);
            torch.Managers.AddManager(pgmr);
        }

        private void HandleException(object sender, UnhandledExceptionEventArgs e) {
            Log.Fatal("DOP FATAL EXCEPTION CAUGHT! " + e);
            Log.Fatal(e.ExceptionObject as Exception, "DOP:FATAL:" + sender);
        }

        public override void Update() {
            Settings.Update();
            counter++;
        }

        public override void Dispose() {
            if (_torchSessionManager != null) _torchSessionManager.SessionStateChanged -= SessionChanged;
            _torchSessionManager = null;
        }
		

        private void SessionChanged(ITorchSession session, TorchSessionState state) {
            switch (state) {
                case TorchSessionState.Loaded:
                    Settings.Loaded(session.Torch);
                    break;
                case TorchSessionState.Unloading:
                    Settings.Unloaded();
                    break;
            }
        }
    }
}