using VRage.Game;
using VRage.Game.ModAPI;
using System;
using Sandbox.Definitions;
using Sandbox.ModAPI;

namespace Slime {
    public class CustomDamageSystem {
        public static void Init() { MyAPIGateway.Session.DamageSystem.RegisterBeforeDamageHandler(1, HandleDamage); }

        private static void HandleDamage(object target, ref MyDamageInformation damage) { //Paralell
            try {
                
                var slimBlock = target as IMySlimBlock;
                if (DOP.Instance.Config.VoxelDamageTweaksEnabled) {
                    var speed = DOP.Instance.Config.VoxelDamageTweaksMaxSpeed * DOP.Instance.Config.VoxelDamageTweaksMaxSpeed;
                    if (slimBlock != null && damage.Type == MyDamageType.Deformation) {
                        var ph = slimBlock.CubeGrid.Physics;
                        if (ph != null && ph.LinearVelocity.LengthSquared() < speed) { //900 = 30*30 ms
                            var attacker = MyAPIGateway.Entities.GetEntityById(damage.AttackerId);
                            if (attacker == null) { //by voxel
                                damage.Amount = 0;
                                damage.IsDeformation = false;
                                return;
                            }
                        }
                    }
                }

                if (DOP.Instance.Config.AntiRammingStations) {
                    if ((damage.Type == MyDamageType.Fall || damage.Type == MyDamageType.Deformation) && slimBlock.CubeGrid.IsStatic) {
                        damage.Amount = 0;
                        damage.IsDeformation = false;
                        return;
                    }
                }
                
            } catch (Exception e) { DOP.Log.Error(e, "CustomDamageSystem"); }
        }
    }
}