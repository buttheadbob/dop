﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using Sandbox.ModAPI;
using VRage.ModAPI;
using VRageMath;

namespace Foogs
{
    [ProtoContract]
    public enum MessageType : byte
    {
        SaveGridReq,
        LoadGridReq,
        ClearGridReq,
        ShowGridReq,
        GetBlockStatus,
        SetBlockStatus
    }

    public struct MsgInfo
    {
        public readonly ulong SteamId;
        public readonly IMyEntity MyGrid;
        public readonly IMyProjector Projector;
        public readonly ReqType Action;
        public readonly bool Safe_checks;
        public readonly MatrixD pos;
        public readonly BoundingBoxD WAABB;
        public MsgInfo(ulong steamId, ReqType action, IMyProjector projector1, IMyEntity myGrid, bool safe_checks, MatrixD poss, BoundingBoxD waabb)
        {
            SteamId = steamId;
            Projector = projector1;
            MyGrid = myGrid;
            Action = action;
            Safe_checks = safe_checks;
            pos = poss;
            WAABB = waabb;
        }
    }

    [ProtoContract]   
    public struct ModStorageGrid
    {
        [ProtoMember(1)]
        public ulong Original_steamid;
        [ProtoMember(2)]
        public long Original_blockid;
        [ProtoMember(3)]
        public string Path;
        [ProtoMember(4)]
        public Dictionary<string, double> Invitems;
        [ProtoMember(5)]
        public Dictionary<string, double> InvitemsNew;
        public ModStorageGrid(ulong orig_steamid, long orig_blockid, string path, Dictionary<string,double> invitemsnew)
        {
            Original_steamid = orig_steamid;
            Original_blockid = orig_blockid;
            Path = path;
            InvitemsNew = invitemsnew;
            Invitems = null;
        }

        public string GetStoragePath()
        {
            if (string.IsNullOrWhiteSpace(Path)) return null;
            return System.IO.Path.Combine(System.IO.Path.Combine(SGarage.Instance.Config.GaragePath, Original_steamid.ToString()), Path + ".sbc");
        }

    }

    [ProtoContract]   
    public struct ModStorageSettings
    {
        [ProtoMember(1), System.ComponentModel.DefaultValue("false")]
        public bool ShareBuilder;
        [ProtoMember(2), System.ComponentModel.DefaultValue("0")]
        public int InvAccesPut;
        [ProtoMember(3), System.ComponentModel.DefaultValue("0")]
        public int InvAccesGet;
        [ProtoMember(4)]
        public Vector3I PosOffset;
        [ProtoMember(5)]
        public Vector3I RotOffset;
        public ModStorageSettings(bool shareBuilder, int invAccesPut, int invAccesGet, Vector3I posOffset, Vector3I rotOffset)
        {
            ShareBuilder = shareBuilder;
            InvAccesPut = invAccesPut;
            InvAccesGet = invAccesGet;
            PosOffset = posOffset;
            RotOffset = rotOffset;
        }
    }

    public enum ReqType
    {
        Save,
        Load,
        Clear,
        Show
    }


    [ProtoContract]
    public enum BlockState
    {
        [ProtoEnum]
        None = 0,
        [ProtoEnum]
        Busy = 1,
        [ProtoEnum]
        Empty_ReadyForSave = 2,//any
        [ProtoEnum]
        Contains_ReadyForLoad = 3
    }

    [ProtoContract]
    public struct ClientReq
    {
        [ProtoMember(2)]
        public long GridId;
        [ProtoMember(3)]
        public ulong SteamId;
        [ProtoMember(4)]
        public long BLOCK_entityId;
        [ProtoMember(5)]
        public Vector3D Pos;
        [ProtoMember(6)]
        public Vector3D r_forward;
        [ProtoMember(7)]
        public Vector3D r_up;
        [ProtoMember(8)]
        public BoundingBoxD WAABB;
    }

    [ProtoContract]
    public struct BlockStatus
    {
        [ProtoMember(1)]
        public ulong SteamId;
        [ProtoMember(2)]
        public long BlockEntId;
        [ProtoMember(3)]
        public Dictionary<string, DateTime> Cooldowns;
        [ProtoMember(4, IsRequired = true), System.ComponentModel.DefaultValue(BlockState.None)]
        public BlockState State;
        [ProtoMember(5)]
        public bool ShareBuilder;
        [ProtoMember(6)]
        public int InvAccesPut; //MyOwnershipShareModeEnum
        [ProtoMember(7)]
        public int InvAccesGet;//MyOwnershipShareModeEnum
        [ProtoMember(8), System.ComponentModel.DefaultValue("")]
        public string SavedShipInfo; //>Name:[]Blocks:[] PCU:[] Mass:[] kg<
    }
}
