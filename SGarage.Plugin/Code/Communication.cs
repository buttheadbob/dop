﻿using System;
using System.Collections.Generic;
using Sandbox.ModAPI;
using VRage.Game;
using VRageMath;

namespace Foogs
{
    public static class Communication
    {
        public const ushort NETWORK_ID = 10666;
        public static void RegisterHandlers()
        {
            MyAPIGateway.Multiplayer.RegisterSecureMessageHandler(NETWORK_ID, MessageHandler);
            SGarage.Instance.SomeLog("Register communication handlers");
        }

        public static void UnregisterHandlers()
        {
            MyAPIGateway.Multiplayer.RegisterSecureMessageHandler(NETWORK_ID, MessageHandler);
        }

        private static void MessageHandler(ushort port, byte[] bytes, ulong playerId, bool isFromServer)
        {
            try
            {
                var type = (MessageType)bytes[0];
               // SGarage.Instance.SomeLog($"Received message: {bytes[0]}: {type}");
                var data = new byte[bytes.Length - 1];
                Array.Copy(bytes, 1, data, 0, data.Length);
                switch (type)
                {
                    case MessageType.SaveGridReq:
                        OnSaveGridReq(data);
                        break;
                    case MessageType.ClearGridReq:
                        OnClearGridReq(data);
                        break;
                    case MessageType.LoadGridReq:
                        OnLoadGridReq(data);
                        break;
                    case MessageType.ShowGridReq:
                        OnShowGridReq(data);
                        break;
                    case MessageType.GetBlockStatus:
                        OnClientReqGetBlockStatus(data);
                        break;
                    case MessageType.SetBlockStatus:
                        OnClientReqSetBlockStatus(data);
                        break;
                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                SGarage.Instance.SomeLog($"Error during message handle! {ex}");
            }
        }

        #region Receive
        private static void OnSaveGridReq(byte[] data)
        {
            var savegridreq = MyAPIGateway.Utilities.SerializeFromBinary<ClientReq>(data);
            IMyProjector p = MyAPIGateway.Entities.GetEntityById(savegridreq.BLOCK_entityId) as IMyProjector;
            //cheat check
            var grid = MyAPIGateway.Entities.GetEntityById(savegridreq.GridId);
            if (grid == null)
            {
                SGarage.Instance.SomeLog($"Grid find failed, try again.");
                return;
            }
            lock (MainLogic.ActionQueue)
            {
                MainLogic.ActionQueue.Enqueue(new MsgInfo
                (savegridreq.SteamId, ReqType.Save, p, grid, true, MatrixD.Zero, BoundingBoxD.CreateInvalid()));
            }
        }

        private static void OnLoadGridReq(byte[] data)
        {
            var loadgridreq = MyAPIGateway.Utilities.SerializeFromBinary<ClientReq>(data);
            IMyProjector p = MyAPIGateway.Entities.GetEntityById(loadgridreq.BLOCK_entityId) as IMyProjector;
            //cheat check
            var m = new MatrixD();
            m.Translation = loadgridreq.Pos;
            m.Up = loadgridreq.r_up;
            m.Forward = loadgridreq.r_forward;
            lock (MainLogic.ActionQueue)
            {
                MainLogic.ActionQueue.Enqueue(new MsgInfo
                (loadgridreq.SteamId, ReqType.Load, p, null, true, m, loadgridreq.WAABB));
            }
        }

        private static void OnShowGridReq(byte[] data)
        {
            var showgridreq = MyAPIGateway.Utilities.SerializeFromBinary<ClientReq>(data);
            IMyProjector p = MyAPIGateway.Entities.GetEntityById(showgridreq.BLOCK_entityId) as IMyProjector;
            //cheat check
            lock (MainLogic.ActionQueue)
            {
                MainLogic.ActionQueue.Enqueue(new MsgInfo
                (showgridreq.SteamId, ReqType.Show, p, null, true, MatrixD.Zero, BoundingBoxD.CreateInvalid()));
            }
        }

        private static void OnClearGridReq(byte[] data)
        {
            var loadgridreq = MyAPIGateway.Utilities.SerializeFromBinary<ClientReq>(data);
            IMyProjector p = MyAPIGateway.Entities.GetEntityById(loadgridreq.BLOCK_entityId) as IMyProjector;
            //cheat check
            lock (MainLogic.ActionQueue)
            {
                MainLogic.ActionQueue.Enqueue(new MsgInfo
                (loadgridreq.SteamId, ReqType.Clear, p, null, true, MatrixD.Zero, BoundingBoxD.CreateInvalid()));
            }
        }

        private static void OnClientReqSetBlockStatus(byte[] data)
        {
            MainLogic.OnClientReqSetBlockStatus(data);
        }
        private static void OnClientReqGetBlockStatus(byte[] data)
        {
            MainLogic.OnClientReqGetBlockStatus(data);
        }
        #endregion
        #region Send

        /// <summary>
        /// Send block status to one player with personal cooldowns
        /// </summary>
        /// <param name="steamid"></param>
        /// <param name="block_entid"></param>
        /// <param name="state"></param>
        /// <param name="new_cooldown"></param>
        public static void SendBlockStatusToPlayer(ulong steamid, long block_entid, BlockState state, Dictionary<string, DateTime> new_cooldown, bool keepbuider, string myinfo)
        {
            SendToClient(MessageType.SetBlockStatus, new BlockStatus
            {
                BlockEntId = block_entid,
                Cooldowns = new_cooldown,
                State = state,
                SteamId = steamid,
                ShareBuilder = keepbuider,
                InvAccesGet = 1,
                InvAccesPut = 1,
                SavedShipInfo = myinfo
            }, steamid);
        }

        /// <summary>
        /// Send new block status to all players, without cooldowns
        /// </summary>
        /// <param name="block_entid"></param>
        /// <param name="state"></param>
        public static void SendBlockStatusToAll(long block_entid, BlockState state, bool keepbuiler, string myinfo)
        {
            BroadcastToClients(MessageType.SetBlockStatus, new BlockStatus
            {
                BlockEntId = block_entid,
                State = state,
                SteamId = 0,
                ShareBuilder = keepbuiler,
                SavedShipInfo = myinfo
            });
        }
        #endregion

        #region Helpers
        public static void BroadcastToClients(MessageType type, object obj)
        {
            byte[] data = MyAPIGateway.Utilities.SerializeToBinary(obj);
            var newData = new byte[data.Length + 1];
            newData[0] = (byte)type;
            data.CopyTo(newData, 1);
            MyAPIGateway.Utilities.InvokeOnGameThread(() => { MyAPIGateway.Multiplayer.SendMessageToOthers(NETWORK_ID, newData); });
        }

        public static void SendToClient(MessageType type, object obj, ulong recipient)
        {
            byte[] data = MyAPIGateway.Utilities.SerializeToBinary(obj);
            var newData = new byte[data.Length + 1];
            newData[0] = (byte)type;
            data.CopyTo(newData, 1);
            MyAPIGateway.Utilities.InvokeOnGameThread(() => { MyAPIGateway.Multiplayer.SendMessageTo(NETWORK_ID, newData, recipient); });
        }
        #endregion
    }
}
