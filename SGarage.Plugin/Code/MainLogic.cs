using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.GameSystems;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.Weapons;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Torch;
using VRage;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.Utils;
using VRageMath;

namespace Foogs
{
    public static class MainLogic
    {
        private const int PROJECTION_TTL = 1;
        public static Guid STORAGE_GUID = new Guid("49d74f80-540e-4e6e-9e16-3928719b5f0d");
        public static Guid SETTINGS_GUID = new Guid("308830f6-0ebe-41cb-b729-27a4fb851c7e");
        private static Thread m_queue = null;
        private static Dictionary<long, Dictionary<string, DateTime>> m_all_cooldowns;
        private static Dictionary<long, DateTime> m_projections_times = new Dictionary<long, DateTime>();//Должно быть в lock 
        private static MethodInfo m_myProgrammableBlockKillProgramm;
        private static System.Timers.Timer m_timerCheck = new System.Timers.Timer(5000);
        private static MethodInfo SendNewBlueprint;

        public static MyStringHash[] garage_subtypes_h = { MyStringHash.GetOrCompute("LargeGarage") };
        public static Queue<MsgInfo> ActionQueue = new Queue<MsgInfo>();

        public static void Init()
        {
            try
            {
                SendNewBlueprint = typeof(MyProjectorBase).GetMethod("SendNewBlueprint", BindingFlags.Instance | BindingFlags.NonPublic);
            }
            catch (Exception e)
            {
                SGarage.Instance.SomeLog("Reflection error! MyProjectorBase.SendNewBlueprint not found" + e.ToString(), 2);
                throw new Exception("Reflection error! MyProjectorBase.SendNewBlueprint not found");
            }
            try
            {
                m_myProgrammableBlockKillProgramm = typeof(MyProgrammableBlock).GetMethod("OnProgramTermination", BindingFlags.Instance | BindingFlags.NonPublic);
            }
            catch (Exception e)
            {
                SGarage.Instance.SomeLog("Reflection error! MyProgrammableBlock.OnProgramTermination not found" + e.ToString(), 2);
            }

            if (!string.IsNullOrEmpty(SGarage.Instance.Config.GaragePath))
            {
                if (!Directory.Exists(SGarage.Instance.Config.GaragePath))
                {
                    SGarage.Instance.SomeLog($"Garage backup directory created!");
                    Directory.CreateDirectory(SGarage.Instance.Config.GaragePath);
                }
            }
            else
            {

                SGarage.Instance.SomeLog($"Garage path is empty! Fatal error!", 2);
                throw new Exception("Garage path is empty! Fatal error!");
            }

            m_queue = new Thread(ProcessQueue);
            m_queue.Priority = ThreadPriority.Lowest;
            m_queue.Start();

            
            Communication.RegisterHandlers();
            InterModComms.RegisterHandlers();

            m_timerCheck.Elapsed += (a, b) =>
            {
                TryCheckProjectionsTimers();
            };
            m_timerCheck.Start();

            if (MyAPIGateway.Utilities.GetVariable("SlimGarage", out string value))
            {
                m_all_cooldowns = MyAPIGateway.Utilities.SerializeFromBinary<Dictionary<long, Dictionary<string, DateTime>>>(Convert.FromBase64String(value));
            }
            else
            {
                m_all_cooldowns = new Dictionary<long, Dictionary<string, DateTime>>();
            }

            SaveCooldowns();
            InterModComms.SendFuncToMod();
        }

        public static void RegisterGarageGUIDs()
        {
            try
            {
                SGarage.Instance.SomeLog($"RegisterGarageGUIDs start.");
                bool needwrite = false;
                bool flagSTORAGE_GUID = false;
                bool flagSETTINGS_GUID = false;
                var def = MyDefinitionManager.Static.GetEntityComponentDefinitions<VRage.Game.Definitions.MyModStorageComponentDefinition>();

                foreach (var modcompoment in def)
                {
                    if (modcompoment.RegisteredStorageGuids.Contains(STORAGE_GUID))
                    {
                        flagSTORAGE_GUID = true;
                    }

                    if (modcompoment.RegisteredStorageGuids.Contains(SETTINGS_GUID))
                    {
                        flagSETTINGS_GUID = true;
                    }
                }

                foreach (var modcompoment in def)
                {
                    var guids = modcompoment.RegisteredStorageGuids.ToList();
                    if (!flagSTORAGE_GUID && !guids.Contains(STORAGE_GUID))
                    {
                        guids.Add(STORAGE_GUID);
                        needwrite = true;
                        flagSTORAGE_GUID = true;
                    }

                    if (!flagSETTINGS_GUID && !guids.Contains(SETTINGS_GUID))
                    {
                        guids.Add(SETTINGS_GUID);
                        needwrite = true;
                        flagSETTINGS_GUID = true;
                    }

                    if (needwrite)
                    {
                        SGarage.Instance.SomeLog($"GUID added.");
                        modcompoment.RegisteredStorageGuids = guids.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                SGarage.Instance.SomeLog($"Exception at RegisterGarageGUIDs, can cause lost all garage data!", 2);
            }
        }

        public static void Dispose()
        {
            Communication.UnregisterHandlers();
            InterModComms.UnregisterHandlers();
            m_queue.Abort();
            m_queue = null;
        }

        //spread our invoke queue over many updates to avoid lag spikes and collisions!
        private static void ProcessQueue()
        {
            Thread.Sleep(2000);
            SGarage.Instance.SomeLog($"Garage process queue start");
            while (true)
            {
                Thread.Sleep(1000);
                try
                {
                    if (ActionQueue.Count == 0)
                    {
                        continue;
                    }

                    MsgInfo item;
                    if (!ActionQueue.TryDequeue(out item))
                    {
                        continue;
                    }

                    switch (item.Action)
                    {
                        case ReqType.Save:
                            SaveGrid(ref item);
                            break;
                        case ReqType.Load:
                            LoadGrid(ref item);
                            break;
                        case ReqType.Clear:
                            GrindGrid(ref item);
                            break;
                        case ReqType.Show:
                            ShowGrid(ref item);
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    SGarage.Log.Error($"Garage main loop exception catch: " + ex.ToString());
                }
            }
        }

        #region player actions
        /// <summary>
        /// Show projection (load into projector block)
        /// </summary>
        private static void ShowGrid(ref MsgInfo info)
        {
            var steamid = info.SteamId;
            //SGarage.Instance.SendMsgToChat("Show garage started.", steamid);
            var player_identity = Sync.Players.TryGetIdentityId(steamid);

            if (player_identity == 0)
            {
                SGarage.Instance.SendMsgToChat($"Player not found: {steamid}", steamid);
            }

            if (info.Projector == null)
            {
                SGarage.Instance.SendMsgToChat("Garage is null.", steamid);
                return;
            };

            var target_projector = info.Projector as MyProjectorBase;

            if (TryGetBlockGridStorage(target_projector, out ModStorageGrid gridinfo))
            {
                MyObjectBuilder_Definitions myObjectBuilder_Definitions = Sandbox.Game.GUI.MyBlueprintUtils.LoadPrefab(gridinfo.GetStoragePath());

                if (myObjectBuilder_Definitions == null)
                {
                    SGarage.Instance.SendMsgToChat("Cant read ship from garage", info.SteamId);
                    return;
                }
                var gridss = myObjectBuilder_Definitions.ShipBlueprints[0].CubeGrids.ToList();
                var settings = GetBlockSettingsStorage(info.Projector);

                info.Projector.SetProjectedGrid(gridss[0]);
                Thread.Sleep(20); // Делаем паузу между сообщениями. Чтобы сообщение отправилось первым.
                var TargetProjector = info.Projector as MyProjectorBase;
                SendNewBlueprint.Invoke(TargetProjector, new object[] { gridss });
                lock (m_projections_times)
                {
                    m_projections_times[info.Projector.EntityId] = DateTime.Now.ToUniversalTime().AddMinutes(PROJECTION_TTL);
                }
                Thread.Sleep(33); //чтобы успела появиться проекция.
                info.Projector.ProjectionRotation = settings.RotOffset;
                info.Projector.ProjectionOffset = settings.PosOffset;
                TargetProjector.SendNewOffset(settings.PosOffset, settings.RotOffset, 1, false);
                //SGarage.Instance.SendMsgToChat("Show garage end.", steamid);
            }
        }

        /// <summary>
        /// Clear all data from block and push items to cargo's in grid, or print error
        /// </summary>
        private static void GrindGrid(ref MsgInfo info)
        {
            // SGarage.Instance.SendMsgToChat("Grind garage started.", info.SteamId);
            var player_identity = Sync.Players.TryGetIdentityId(info.SteamId);
            BlockState newstate;
            if (TryGetBlockGridStorage(info.Projector, out ModStorageGrid gridstorage))
            {
                if (!HasPlayerAccess(ref info))
                {
                    SGarage.Instance.SendMsgToChat("You can't use this garage, u not have rights!", info.SteamId);
                    newstate = BlockState.Busy;
                }
                else
                {
                    List<MyInventory> cargosneighbours;
                    List<MyInventory> accessible_inv;

                    CargoUtils.GetNeighboursAndTargetCargos(info.Projector, out cargosneighbours, out accessible_inv);
                    if (CargoUtils.CanPutToCargos(info.Projector, cargosneighbours, accessible_inv))
                    {
                        CargoUtils.TransferToCargos(info.Projector, cargosneighbours, accessible_inv);
                        ClearGarage(ref info);
                        newstate = BlockState.Empty_ReadyForSave;
                        SGarage.Instance.SendMsgToChat("Inventory transfered.", info.SteamId);
                    }
                    else
                    {
                        SGarage.Instance.SendMsgToChat("Inventory not transfered. No available space.", info.SteamId);
                        newstate = BlockState.Contains_ReadyForLoad;
                    }
                }
            }
            else
            {
                SGarage.Instance.SendMsgToChat("This garage is corrupted, fixing now...", info.SteamId);
                SGarage.Log.Error($"Error while GetGridStorage in grind request! data is null. OwnerId:[{info.Projector.OwnerId}] BuiltBy:[{(info.Projector as MyProjectorBase).BuiltBy}] EntityId:[{info.Projector.EntityId}]");
                ClearGarage(ref info);
                newstate = BlockState.Contains_ReadyForLoad;
            }

            GetCooldown(player_identity, out Dictionary<string, DateTime> cooldown);
            ModStorageSettings blocksetting = GetBlockSettingsStorage(info.Projector);
            Communication.SendBlockStatusToAll(info.Projector.EntityId, newstate, blocksetting.ShareBuilder, "");
            Communication.SendBlockStatusToPlayer(info.SteamId, info.Projector.EntityId, newstate, cooldown, blocksetting.ShareBuilder, "");
        }

        /// <summary>
        /// Just delete all data
        /// </summary>
        private static void ClearGarage(ref MsgInfo info)
        {
            info.Projector.SetProjectedGrid(null);
            info.Projector.Storage.Remove(info.Projector.Storage.Where(x => x.Key == STORAGE_GUID).FirstOrDefault());
            info.Projector.GetInventory().Clear();
            SGarage.Instance.SendMsgToChat("Garage cleared.", info.SteamId);
        }

        private static void LoadGrid(ref MsgInfo info)
        {
            try
            {
                var t_projector = (info.Projector as MyProjectorBase);
                var player_steamid = info.SteamId;
                SGarage.Instance.SendMsgToChat("Load from garage started.", player_steamid);
                var player_identity = Sync.Players.TryGetIdentityId(player_steamid);
                var player = Sync.Players.TryGetIdentity(player_identity);
                if (player == null)
                {
                    SGarage.Instance.SendMsgToChat("Cant find you. Contact admin.", player_steamid);
                    SendCurrentStatus(t_projector, player_steamid);
                    return;
                }

                SGarage.Instance.SomeLog($"Load from garage started from player [{player.DisplayName}], grid: [{t_projector.CubeGrid?.DisplayName}], blockId:[{info.Projector.EntityId}]", 1);

                if (HasCooldown("SlimGarage.LoadButton", player_identity))
                {
                    SGarage.Instance.SendMsgToChat("You have a cool-down. Aborted.", info.SteamId);
                    SendCurrentStatus(t_projector, player_steamid);
                    return;
                }

                if (info.Projector.GetInventory().ItemCount == 0)
                {
                    ClearGarage(ref info);
                    SGarage.Instance.SendMsgToChat("Incorrect state cleared.", info.SteamId);
                    SendCurrentStatus(t_projector, player_steamid);
                    return;
                }

                if (!TryGetBlockGridStorage(t_projector, out ModStorageGrid gridstorage))
                {
                    SGarage.Instance.SendMsgToChat("Grid data storage corrupted, try later or report to Admin.", info.SteamId);
                    SGarage.Log.Error($"Error while GetGridStorage in load request! data is null. OwnerId:[{t_projector.OwnerId}] BuiltBy:[{(t_projector as MyProjectorBase).BuiltBy}] EntityId:[{t_projector.EntityId}]");
                    SendCurrentStatus(t_projector, player_steamid);
                    return;
                }

                if (!HasPlayerAccess(ref info))
                {
                    SGarage.Instance.SendMsgToChat("You cant use this garage, u not have rights!", player_steamid);
                    SendCurrentStatus(t_projector, player_steamid);
                    return;
                }

                if (gridstorage.Invitems != null) //TODO Invitems is obsolute remove later
                {
                    if (!HasItemsForLoad(gridstorage.Invitems, t_projector, out string missitem))
                    {
                        SGarage.Instance.SendMsgToChat("Not enough items in inventory, can't reconstruct this grid. Missing item: " + missitem, info.SteamId);
                        SendCurrentStatus(t_projector, player_steamid);
                        return;
                    }
                }

                if (gridstorage.InvitemsNew != null)
                {
                    if (!HasItemsForLoadNew(gridstorage.InvitemsNew, t_projector, out string missitem))
                    {
                        SGarage.Instance.SendMsgToChat("Not enough items in inventory, can't reconstruct this grid. Missing item(s): " + missitem, info.SteamId);
                        SendCurrentStatus(t_projector, player_steamid);
                        return;
                    }
                }

                var grav = MyGravityProviderSystem.CalculateNaturalGravityInPoint(info.pos.Translation, out float gravmult);
                if (gravmult <= 0.99 && gravmult >= 0.05)
                {
                    SGarage.Instance.SendMsgToChat("Cant load in that small gravity, operation aborted!", info.SteamId);
                    SendCurrentStatus(t_projector, player_steamid);
                    return;
                }

                var userpath = gridstorage.GetStoragePath();
                MyObjectBuilder_Definitions myObjectBuilder_Definitions = Sandbox.Game.GUI.MyBlueprintUtils.LoadPrefab(userpath);
                if (myObjectBuilder_Definitions == null)
                {
                    SGarage.Instance.SendMsgToChat("Cant get ship from garage, contact admin.", info.SteamId);
                    SendCurrentStatus(t_projector, player_steamid);
                    return;
                }
                MyObjectBuilder_CubeGrid[] cubeGrids = myObjectBuilder_Definitions.ShipBlueprints[0].CubeGrids;
                var blocksetting = GetBlockSettingsStorage(t_projector);
                SpawnUtils.RemapOwnership(cubeGrids, player.IdentityId, blocksetting.ShareBuilder);
                SpawnUtils.SpawnSomeGrids(cubeGrids, ref info, player_identity, userpath);
            }
            catch (Exception ex)
            {
                SGarage.Instance.SendMsgToChat("Exception while load grid, contact admin.", info.SteamId);
                SGarage.Log.Error(ex, $"Exception while load grid send logs too Foogs!");
                SendCurrentStatus(info.Projector, info.SteamId);
            }
        }

        private static void SaveGrid(ref MsgInfo info)
        {
            //TODO maybe grid lock but later
            ulong player_steamid = info.SteamId;
            var player_identity = Sync.Players.TryGetIdentityId(player_steamid);
            var player = Sync.Players.TryGetIdentity(player_identity);
            SGarage.Instance.SendMsgToChat("Ship save in progress", player_steamid);
            var grid = (MyCubeGrid)info.MyGrid;
            var target_projector = (info.Projector as MyProjectorBase);

            if (grid == null || grid.MarkedForClose || grid.Closed)
            {
                SGarage.Instance.SendMsgToChat("Ship is null.", player_steamid);
                SendCurrentStatus(target_projector, player_steamid);
                return;
            }

            if (target_projector == null)
            {
                SGarage.Instance.SendMsgToChat("Garage is null.", player_steamid);
                SendCurrentStatus(target_projector, player_steamid);
                return;
            }

            SGarage.Instance.SomeLog($"Save to garage started from player {player.DisplayName}, grid: {grid?.DisplayName}, blockId:[{info.Projector.EntityId}]", 1);
            if (target_projector.CubeGrid.Physics.LinearVelocity.Length() > 1 | target_projector.CubeGrid.Physics.AngularVelocity.Length() > 2)
            {
                SGarage.Instance.SendMsgToChat("Stop your ship before use garage!.", player_steamid);
                SendCurrentStatus(target_projector, player_steamid);
                return;
            }

            if (GetBlockState(target_projector) != BlockState.Empty_ReadyForSave)
            {
                SGarage.Instance.SendMsgToChat("Something wrong with garage, report to Foogs!", player_steamid);
                SendCurrentStatus(target_projector, player_steamid);
                return;
            }

            if (!HasPlayerAccess(ref info))
            {
                SGarage.Instance.SendMsgToChat("You cant use this garage, u not have rights!", player_steamid);
                SendCurrentStatus(target_projector, player_steamid);
                return;
            }

            var grids = MyCubeGridGroups.Static.GetGroups(GridLinkTypeEnum.Logical).GetGroupNodes(grid);
            grids.SortNoAlloc((x, y) => x.BlocksCount.CompareTo(y.BlocksCount));
            grids.Reverse();
            grids.SortNoAlloc((x, y) => x.GridSizeEnum.CompareTo(y.GridSizeEnum));

            if (!CheckCanSaveShip(grids, target_projector, player_steamid, player))
            {
                SendCurrentStatus(target_projector, player_steamid);
                return;
            }

            int index = 0;
            int TotalPCU = 0;
            int TotalBlocks = 0;
            var gridstosave = new MyObjectBuilder_CubeGrid[grids.Count];
            TorchBase.Instance.InvokeAsync(() =>
            {
                try
                {
                    foreach (MyCubeGrid x in grids)
                    {
                        foreach (var c in x.GetFatBlocks())
                        {
                            if (c is MyCockpit)
                            {
                                (c as MyCockpit).RemovePilot();
                            }

                            if (c is MyProgrammableBlock)
                            {
                                try
                                {
                                    m_myProgrammableBlockKillProgramm.Invoke((c as MyProgrammableBlock), new object[] { MyProgrammableBlock.ScriptTerminationReason.None });
                                }
                                catch (Exception ex)
                                {
                                    SGarage.Log.Error(ex, "MyProgrammableBlock hack eval");
                                }
                            }

                            if (c is MyShipDrill)
                            {
                                (c as MyShipDrill).Enabled = false;
                            }
                        }

                        TotalPCU += x.BlocksPCU;
                        TotalBlocks += x.BlocksCount;
                        var ob = (MyObjectBuilder_CubeGrid)x.GetObjectBuilder(true);
                        gridstosave[index] = ob;
                        index++;
                    }
                }
                catch (Exception ex)
                {
                    SGarage.Log.Error(ex, "(SaveGrid)Exception in block disables ex");
                }
            }).Wait();
            // Disassemble grid to components.
            var grid_components = ReassemblyUtils.GetAllComponentsAndInventories(ref gridstosave);
            foreach (var comp in grid_components)
            {
                if (comp.Key.SubtypeName == "AdminComponent")
                {
                    SGarage.Instance.SendMsgToChat("You trying to save a grid with AdminComponent! Thats illegal!", player_steamid);
                    SendCurrentStatus(target_projector, player_steamid);
                    return;
                }
            }
            if (SpawnUtils.SaveShipsToFile(player_steamid, TotalPCU, TotalBlocks, ref gridstosave, out string filename, out MyObjectBuilder_Definitions obs))
            {
                try
                {
                    if (target_projector.Storage == null)
                    {
                        target_projector.Storage = new MyModStorageComponent();
                        target_projector.Components.Add<MyModStorageComponentBase>(target_projector.Storage);
                    }
                    // Put to inv.
                    CargoUtils.AddToInventory(target_projector.GetInventory(), grid_components, false);
                    var grid_componentsFix = new Dictionary<string, double>();
                    foreach (var item in grid_components)
                    {
                        grid_componentsFix.Add(item.Key.ToString(), (double)item.Value);
                    }
                    // Save to ship info to storage + strong base64 protection
                    var data = new ModStorageGrid(player_steamid, target_projector.EntityId, filename, grid_componentsFix);
                    target_projector.SetStorageData(STORAGE_GUID, Convert.ToBase64String(MyAPIGateway.Utilities.SerializeToBinary(data)));

                    //Create/save block settings to storage
                    var block_settings = GetBlockSettingsStorage(target_projector);
                    target_projector.SetStorageData(SETTINGS_GUID, Convert.ToBase64String(MyAPIGateway.Utilities.SerializeToBinary(block_settings)));


                    var shipinfo = SpawnUtils.GetStoredGridInfo(ref gridstosave);
                    Communication.SendBlockStatusToAll(target_projector.EntityId, BlockState.Contains_ReadyForLoad, block_settings.ShareBuilder, shipinfo);
                    Dictionary<string, DateTime> cooldowns;
                    if (!m_all_cooldowns.TryGetValue(player_identity, out cooldowns))
                    {
                        cooldowns = new Dictionary<string, DateTime>();
                    }
                    Communication.SendBlockStatusToPlayer(info.SteamId, target_projector.EntityId, BlockState.Contains_ReadyForLoad, cooldowns, block_settings.ShareBuilder, shipinfo);

                    SaveCooldowns();
                    SGarage.Instance.SendMsgToChat("Grid saved to the garage!", player_steamid);
                    MyAPIGateway.Utilities.InvokeOnGameThread(() =>
               {
                   foreach (MyCubeGrid g in grids)
                   {
                       g.Close();
                   }
               });
                }
                catch (Exception ex)
                {
                    SGarage.Log.Error(ex, "(SaveGrid) Exception in save logic2");
                    SGarage.Instance.SendMsgToChat("ERROR WHILE SAVE REPORT TO FOOGS!", player_steamid);
                }
            }
            else
            {
                SGarage.Instance.SendMsgToChat("Error during save,contact admin!", player_steamid);
            }
        }
        #endregion
        #region helpers
        public static bool HasPlayerAccess(ref MsgInfo info)
        {
            if ((info.Projector as MyTerminalBlock).HasPlayerAccess(info.Projector.OwnerId))
            {
                return true;
            }
            return false;
        }

        private static bool HasItemsForLoadNew(Dictionary<string, double> mustHaveItems, MyProjectorBase projector, out string missingItems)
        {
            missingItems = "";
            var items = projector.GetInventory().GetItems();
            if (items.Count == 0) return false;

            var curritems = new Dictionary<MyDefinitionId, MyFixedPoint>();
            foreach (var item in items)
            {

                if (item.Amount == 0) continue;
                var defId = item.Content.GetObjectId();
                if (curritems.ContainsKey(defId))
                {
                    curritems[defId] += (MyFixedPoint)(double)item.Amount;
                }
                else
                {
                    curritems[defId] = (MyFixedPoint)(double)item.Amount;
                }
            }
            bool result = true;
            var strBuild = new StringBuilder();
            foreach (var item in mustHaveItems)
            {
                var itemQuantity = (MyFixedPoint)item.Value;
                if (itemQuantity == 0 || item.Value == 0) continue;
                if (!MyDefinitionId.TryParse(item.Key, out MyDefinitionId itemDefId))
                {
                    continue;
                }

                if (curritems.ContainsKey(itemDefId))
                {
                    if (curritems[itemDefId] < itemQuantity)
                    {
                        strBuild.AppendLine($"{itemDefId.SubtypeName} x {(double)(itemQuantity - curritems[itemDefId])} ");
                        result = false;
                    }
                }
                else
                {
                    strBuild.AppendLine($"{itemDefId.SubtypeName} x {(double)(itemQuantity)}");
                    result = false;
                }
            }
            missingItems = strBuild.ToString();
            return result;
        }

        /// <summary>
        /// Check for modifications in inventory (Player can steal some components). Returns true is there is enough items.
        /// </summary>
        [Obsolete]
        private static bool HasItemsForLoad(Dictionary<string, double> olditems, IMyProjector projector, out string missingitem)
        {
            //TODO remove
            var inv = projector.GetInventory();
            missingitem = "";
            var items = (inv as MyInventory).GetItems();
            if (items.Count == 0) return false;

            var curritems = new Dictionary<MyStringHash, MyFixedPoint>();
            foreach (var item in items)
            {

                if (item.Amount == 0) continue;
                var id = item.Content.SubtypeId;
                if (curritems.ContainsKey(id))
                {
                    curritems[id] += (MyFixedPoint)(double)item.Amount;
                }
                else
                {
                    curritems[id] = (MyFixedPoint)(double)item.Amount;
                }
            }

            var musthaveitems = new Dictionary<MyStringHash, MyFixedPoint>();
            foreach (var item in olditems)
            {
                var temp = (MyFixedPoint)item.Value;
                if (temp == 0 || item.Value == 0) continue;
                musthaveitems.Add(MyStringHash.GetOrCompute(item.Key), temp);
            }

            foreach (var subname in musthaveitems.Keys)
            {
                if (curritems.ContainsKey(subname))
                {
                    if (curritems[subname] < musthaveitems[subname])
                    {
                        missingitem = subname.String + " x " + (double)(musthaveitems[subname] - curritems[subname]);
                        return false;
                    }
                }
                else
                {
                    missingitem = subname.String + " x " + (double)(musthaveitems[subname]);
                    return false;
                }
            }
            return true;
        }

        public static void AfterSpawn(ref MsgInfo info)
        {
            ulong steamid = info.SteamId;
            var player_identity = Sync.Players.TryGetIdentityId(steamid);
            ClearGarage(ref info);
            var proj = info.Projector;
            SetCooldown("SlimGarage.LoadButton", player_identity, SGarage.Instance.Config.LoadCooldown);
            var blocksetting = GetBlockSettingsStorage(proj);
            Communication.SendBlockStatusToAll(proj.EntityId, BlockState.Empty_ReadyForSave, blocksetting.ShareBuilder, "");
            Communication.SendBlockStatusToPlayer(info.SteamId, proj.EntityId, BlockState.Empty_ReadyForSave, m_all_cooldowns[player_identity], blocksetting.ShareBuilder, "");
            SaveCooldowns();
            SGarage.Instance.SendMsgToChat("Load ended.", info.SteamId);
        }

        public static void SendCurrentStatus(IMyProjector p, ulong steamid)
        {
            // SGarage.Instance.SomeLog($"Player requested.Sending back block status to " + steamid);
            var player_identity = Sync.Players.TryGetIdentityId(steamid);
            var state = GetBlockState(p);
            var settings = GetBlockSettingsStorage(p);
            GetCooldown(player_identity, out Dictionary<string, DateTime> cooldown);
            var gridsInfo = GetShipInfo(p);
            Communication.SendBlockStatusToPlayer(steamid, p.EntityId, state, cooldown, settings.ShareBuilder, gridsInfo);
        }

        private static bool HasCooldown(string name, long player_id)
        {
            var curr_time = DateTime.Now.ToUniversalTime();
            if (m_all_cooldowns.ContainsKey(player_id))
            {
                if (m_all_cooldowns[player_id].ContainsKey(name))
                {
                    if (m_all_cooldowns[player_id][name] > curr_time)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private static bool CheckCanSaveShip(List<MyCubeGrid> grids, IMyProjector target_projector, ulong steamid, MyIdentity player)
        {
            foreach (MyCubeGrid g in grids)
            {
                if (g.IsPreview)
                {
                    var translated_msg = "Try save projection is bad, u are poor guy. Disable it pls.";
                    SGarage.Instance.SendMsgToChat(translated_msg, steamid);
                    SGarage.Instance.SomeLog($"Player trying to silent cheat (save a projection), operation aborted! Player { player.DisplayName} [{ steamid}]", 1);
                    SendCurrentStatus(target_projector, steamid);
                    return false;
                }

                var blocks = g.GetFatBlocks();
                foreach (var b in blocks)
                {
                    if (garage_subtypes_h.Contains(b.BlockDefinition.Id.SubtypeId) && b.EntityId == target_projector.EntityId)
                    {
                        SGarage.Instance.SendMsgToChat("Cant save garage into garage, result is black hole. Operation aborted.", steamid);
                        SGarage.Instance.SomeLog($"Cant save garage into garage, result is black hole, operation aborted! " +
                        $"Player: " + player.DisplayName + '(' + steamid + ')');
                        return false;
                    }

                    if (b is MyCockpit)
                    {
                        if ((b as MyCockpit).Pilot != null)
                        {
                            SGarage.Instance.SendMsgToChat("Grid have pilot in: [ " + b.DisplayNameText + " ], operation aborted!", steamid);
                            SGarage.Instance.SomeLog($"Grid have pilot in : [ " + b.DisplayNameText + " ], operation aborted! " +
                            $"Player: " + player.DisplayName + '(' + steamid + ')');
                            return false;
                        }
                    }
                }
            }

            if (!InterModComms.Garage_Mod_SaveChecks.Invoke(grids.ToArray<IMyCubeGrid>(), player.IdentityId))
            {
                SGarage.Instance.SendMsgToChat("Grid have enemy spec block, operation aborted!", steamid);
                SGarage.Instance.SomeLog($"Grid have enemy spec block, operation aborted! " +
                $"Player: " + player.DisplayName + '(' + steamid + ')');
                return false;
            }

            return true;
        }

        private static bool GetCooldown(long player_id, out Dictionary<string, DateTime> cooldown)
        {
            if (m_all_cooldowns.ContainsKey(player_id))
            {
                cooldown = m_all_cooldowns[player_id];
                return true;
            }

            cooldown = new Dictionary<string, DateTime>();
            return false;
        }

        private static void SetCooldown(string name, long player_id, int minutes)
        {
            var curr_time = DateTime.Now.ToUniversalTime();
            var cooldown_time = curr_time.AddMinutes(minutes);
            var new_client_cooldown = new Dictionary<string, DateTime>() { { name, cooldown_time } };
            if (m_all_cooldowns.ContainsKey(player_id))
            {
                if (m_all_cooldowns[player_id].ContainsKey(name))
                {
                    m_all_cooldowns[player_id][name] = cooldown_time;
                }
                else
                {
                    m_all_cooldowns[player_id].Add(name, cooldown_time);
                }
            }
            else
            {
                m_all_cooldowns.Add(player_id, new_client_cooldown);
            }
        }
        /// <summary>
        /// LOAD BP FROM DISK! Blocking!
        /// </summary>
        private static string GetShipInfo(IMyProjector p)
        {
            try
            {
                if (p != null)
                {
                    var rawstoragedata = p.GetStorageData(STORAGE_GUID);
                    if (rawstoragedata != null)
                    {
                        var storage = MyAPIGateway.Utilities.SerializeFromBinary<ModStorageGrid>(Convert.FromBase64String(rawstoragedata));
                        if (String.IsNullOrWhiteSpace(storage.Path))
                        {
                            SGarage.Instance.SomeLog($"[GetShipInfo] user path not exist.", 1);
                        }
                        else
                        {
                            string userpath = storage.GetStoragePath();
                            if (File.Exists(userpath))
                            {
                                MyObjectBuilder_Definitions myObjectBuilder_Definitions = Sandbox.Game.GUI.MyBlueprintUtils.LoadPrefab(userpath);
                                return SpawnUtils.GetStoredGridInfo(ref myObjectBuilder_Definitions.ShipBlueprints[0].CubeGrids);
                            }
                            else
                            {
                                SGarage.Instance.SomeLog($"[GetShipInfo] user path not defined.", 1);
                            }
                        }
                    }
                }
                return "NoInfo(null)";
            }
            catch (Exception ex)
            {
                SGarage.Log.Error(ex, "GetShipInfo exception!");
                return "";
            }
        }

        private static void SaveCooldowns()
        {
            MyAPIGateway.Utilities.SetVariable("SlimGarage", Convert.ToBase64String(MyAPIGateway.Utilities.SerializeToBinary(m_all_cooldowns)));
        }

        private static void SaveProjectionSettings(IMyProjector p)
        {
            var t = GetBlockSettingsStorage(p);
            SetBlockSettingsStorage(p, new ModStorageSettings(t.ShareBuilder, t.InvAccesPut, t.InvAccesGet, p.ProjectionOffset, p.ProjectionRotation), p.OwnerId);
        }

        #endregion

        #region block settings
        //Invokes from game thread! Don't block!!
        public static void OnClientReqSetBlockStatus(byte[] data)
        {
            var reqstatus = MyAPIGateway.Utilities.SerializeFromBinary<BlockStatus>(data);
            if (reqstatus.SteamId != 0)
            {
                var player_identity = Sync.Players.TryGetIdentityId(reqstatus.SteamId);
                IMyProjector p = MyAPIGateway.Entities.GetEntityById(reqstatus.BlockEntId) as IMyProjector;
                var newsettings = new ModStorageSettings(reqstatus.ShareBuilder, reqstatus.InvAccesPut, reqstatus.InvAccesPut, p.ProjectionOffset, p.ProjectionRotation);
                if (SetBlockSettingsStorage(p, newsettings, player_identity))
                {
                    var stus = GetBlockState(p);
                    var gridinfo = GetShipInfo(p);
                    GetCooldown(player_identity, out Dictionary<string, DateTime> cooldown);
                    Communication.SendBlockStatusToPlayer(reqstatus.SteamId, reqstatus.BlockEntId, stus, cooldown, reqstatus.ShareBuilder, gridinfo);
                    Communication.SendBlockStatusToAll(reqstatus.BlockEntId, stus, reqstatus.ShareBuilder, gridinfo);
                }
            }
        }

        //Invokes from game thread! Don't block!!
        public static void OnClientReqGetBlockStatus(byte[] data)
        {
            var reqstatus = MyAPIGateway.Utilities.SerializeFromBinary<BlockStatus>(data);
            if (reqstatus.SteamId != 0)
            {
                IMyProjector p = MyAPIGateway.Entities.GetEntityById(reqstatus.BlockEntId) as IMyProjector;
                var stus = GetBlockState(p);
                var player_identity = Sync.Players.TryGetIdentityId(reqstatus.SteamId);
                GetCooldown(player_identity, out Dictionary<string, DateTime> cooldown);
                var settings = GetBlockSettingsStorage(p);
                var mmyinfo = GetShipInfo(p);
                Communication.SendBlockStatusToPlayer(reqstatus.SteamId, reqstatus.BlockEntId, stus, cooldown, settings.ShareBuilder, mmyinfo);
            }
        }

        private static BlockState GetBlockState(IMyProjector p)
        {
            try
            {
                if (p != null)
                {
                    if (TryGetBlockGridStorage(p, out ModStorageGrid info))
                    {
                        var path = info.GetStoragePath();
                        if (path == null)
                        {
                            SGarage.Instance.SomeLog($"[GetBlockState] user path not exist.", 1);
                            return BlockState.None;
                        }

                        if (!File.Exists(path))
                        {
                            SGarage.Instance.SomeLog($"[GetBlockState] file {path} not exists.", 1);
                            return BlockState.None;
                        }

                        return BlockState.Contains_ReadyForLoad;
                    }
                    else
                    {
                        return BlockState.Empty_ReadyForSave;
                    }
                }
                else
                {
                    SGarage.Log.Error($"[GetBlockState] projector is null");
                    return BlockState.None;
                }
            }
            catch (Exception ex)
            {
                SGarage.Log.Error($"GetBlockState exception = " + ex.ToString());
                return BlockState.Empty_ReadyForSave;
            }
        }

        private static bool SetBlockSettingsStorage(IMyProjector p, ModStorageSettings newstatus, long player_id)
        {
            if (p != null)
            {
                var TargetProjector = p as MyProjectorBase;
                if (TargetProjector.BuiltBy == player_id)
                {
                    if (TargetProjector.Storage == null)
                    {
                        TargetProjector.Storage = new MyModStorageComponent();
                        TargetProjector.Components.Add<MyModStorageComponentBase>(TargetProjector.Storage);
                    }

                    TargetProjector.SetStorageData(SETTINGS_GUID, Convert.ToBase64String(MyAPIGateway.Utilities.SerializeToBinary(newstatus)));
                    return true;
                }
            }
            return false;
        }

        public static bool TryGetBlockGridStorage(IMyProjector p, out ModStorageGrid info)
        {
            var rawstoragedata = p.GetStorageData(STORAGE_GUID);
            if (rawstoragedata != null)
            {
                info = MyAPIGateway.Utilities.SerializeFromBinary<ModStorageGrid>(Convert.FromBase64String(rawstoragedata));
                return true;
            }
            info = new ModStorageGrid();
            return false;
        }

        /// <summary>
        /// Получить настройки блока. Если их нет вернуть по умолчанию.
        /// </summary>
        public static ModStorageSettings GetBlockSettingsStorage(IMyProjector p)
        {
            try
            {
                if (p != null)
                {
                    var rawstoragedata = p.GetStorageData(SETTINGS_GUID);
                    if (rawstoragedata != null)
                    {
                        var storage = MyAPIGateway.Utilities.SerializeFromBinary<ModStorageSettings>(Convert.FromBase64String(rawstoragedata));
                        return storage;
                    }
                }

                return new ModStorageSettings(false, 0, 0, Vector3I.Zero, Vector3I.Zero);
            }
            catch (Exception ex)
            {
                SGarage.Log.Error($"GetBlockSettingsStorage exception! {ex} OwnerId:[{p.OwnerId}]");
                return new ModStorageSettings(false, 0, 0, Vector3I.Zero, Vector3I.Zero);
            }
        }

        private static void TryCheckProjectionsTimers()
        {
            try
            {
                if (m_projections_times?.Count == 0) return;

                var currtime = DateTime.Now.ToUniversalTime();
                var tooff = new List<long>();

                foreach (var item in m_projections_times)
                {
                    if (item.Value < currtime)
                    {
                        tooff.Add(item.Key);
                    }
                }

                if (tooff.Any())
                {
                    MyAPIGateway.Utilities.InvokeOnGameThread(() =>
                    {
                        try
                        {
                            lock (m_projections_times)
                            {
                                foreach (var proj in tooff)
                                {
                                    var ent_p = MyAPIGateway.Entities.GetEntityById(proj);
                                    if (ent_p != null)
                                    {
                                        MyProjectorBase p = ent_p as MyProjectorBase;

                                        (p as IMyProjector).SetProjectedGrid(null);
                                        SaveProjectionSettings(p);
                                    }

                                    m_projections_times.Remove(proj);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            SGarage.Log.Error(ex, "TryCheckProjectionsTimers in-game catch!");
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                SGarage.Log.Error(ex, "TryCheckProjectionsTimers catch!");
            }
        }
        #endregion
    }
}
