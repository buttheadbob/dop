﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.ModAPI;
using SharedLibTorch;
using VRage;
using VRage.Game;
using VRage.Game.Entity;
using VRage.Game.ModAPI.Ingame;
using VRage.ObjectBuilders;
using VRageMath;

namespace Foogs
{
    public static class CargoUtils
    {
        private static MyItemType SteelPlate = MyItemType.MakeComponent("SteelPlate");

        //hack but works perfect
        public static bool IsConnectedToOneOf(MyInventory inv, List<MyInventory> neighbours)
        {
            foreach (var x in neighbours)
            {
                if ((x as VRage.Game.ModAPI.IMyInventory).CanTransferItemTo(inv, SteelPlate))
                {
                    return true;
                }
            }
            return false;
        }

        public static void GetNeighboursAndTargetCargos(IMyProjector target_projector, out List<MyInventory> cargosneighbours, out List<MyInventory> accessible_inv)
        {
            MyCubeGrid grid = target_projector.CubeGrid as MyCubeGrid;
            cargosneighbours = new List<MyInventory>();
            accessible_inv = new List<MyInventory>();
            var neighbours = new List<MySlimBlock>();

            var pos = target_projector.Position;
            neighbours.Add(grid.GetCubeBlock(pos - new Vector3I(1, 0, 0)));
            neighbours.Add(grid.GetCubeBlock(pos - new Vector3I(0, 1, 0)));
            neighbours.Add(grid.GetCubeBlock(pos - new Vector3I(0, 0, 1)));
            neighbours.Add(grid.GetCubeBlock(pos - new Vector3I(-1, 0, 0)));
            neighbours.Add(grid.GetCubeBlock(pos - new Vector3I(0, -1, 0)));
            neighbours.Add(grid.GetCubeBlock(pos - new Vector3I(0, 0, -1)));

            foreach (var block in neighbours)
            {
                if (block == null || block.FatBlock == null || !(block.FatBlock is MyCargoContainer)) continue;

                if ((block.FatBlock as MyTerminalBlock).HasPlayerAccess(target_projector.OwnerId))
                {
                    cargosneighbours.Add(block.FatBlock.GetInventory());
                }
            }
            SGarage.Instance.SomeLog($"cargosneighbours{cargosneighbours.Count}");
            //scan allblocks for connected inventories
            foreach (var block in grid.GetFatBlocks())
            {
                //cargo only
                if (!(block is MyCargoContainer)) continue;
                if (!(block as MyTerminalBlock).HasPlayerAccess(target_projector.OwnerId)) continue;
                var inv = block.GetInventory(0);
                accessible_inv.Add(inv);
            }
            SGarage.Instance.SomeLog($"accessible_inv {accessible_inv.Count}");
            //sort cargos
            accessible_inv.OrderByDescending(x => (x.MaxVolume - x.CurrentVolume));
        }

        public static void TransferToCargos(IMyProjector target_projector, List<MyInventory> cargosneighbours, List<MyInventory> accessible_inv)
        {
            //create list for items
            var itemstotransfer = (target_projector.GetInventory() as MyInventory).GetItems();
            List<Tuple<MyObjectBuilder_Base, MyFixedPoint>> itemstospawn = new List<Tuple<MyObjectBuilder_Base, MyFixedPoint>>(itemstotransfer.Count);
            foreach (var item in itemstotransfer)
            {
                itemstospawn.Add(new Tuple<MyObjectBuilder_Base, MyFixedPoint>(item.Content, item.Amount));
            }

            //check avialable space
            foreach (MyInventory cargoinv in accessible_inv)
            {
                if (!IsConnectedToOneOf(cargoinv, cargosneighbours))
                {
                    continue;
                }

                for (int i = 0; i < itemstotransfer.Count; i++)
                {
                    var curritemdef = itemstotransfer[i].Content.GetId();
                    var fitsamount = cargoinv.ComputeAmountThatFits(curritemdef);

                    if (itemstospawn[i].Item2 > 0)
                    {
                        //we can put item (enough space)
                        if (fitsamount >= itemstospawn[i].Item2)
                        {
                            InventoryUtils.ForceAddItems(cargoinv, itemstospawn[i].Item2, itemstospawn[i].Item1, null);
                            itemstospawn[i] = new Tuple<MyObjectBuilder_Base, MyFixedPoint>(itemstospawn[i].Item1, 0);
                        }
                        else if (fitsamount > 50)
                        {
                            InventoryUtils.ForceAddItems(cargoinv, fitsamount, itemstospawn[i].Item1, null);
                            itemstospawn[i] = new Tuple<MyObjectBuilder_Base, MyFixedPoint>(itemstospawn[i].Item1, itemstospawn[i].Item2 - fitsamount);
                        }
                    }
                }

                InventoryUtils.SortInventory(cargoinv);
                cargoinv.Refresh();
            }
        }

        public static bool CanPutToCargos(IMyProjector target_projector, List<MyInventory> cargosneighbours, List<MyInventory> accessible_inv)
        {
            var toTransfer = target_projector.GetInventory().CurrentVolume;
            //check available space
            foreach (var inv in accessible_inv)
            {
                if (!IsConnectedToOneOf(inv, cargosneighbours))
                {
                    continue;
                }

                var freespace = inv.MaxVolume - inv.CurrentVolume;
                toTransfer -= freespace;

                if (toTransfer <= 0)
                {
                    return true;
                }
            }
            return false;
        }

        public static void AddToInventory(VRage.Game.ModAPI.IMyInventory inventory, Dictionary<MyDefinitionId, MyFixedPoint> items, bool forcePush)
        {
            foreach (var item in items)
            {
                var amount = item.Value;
                var defId = item.Key;
                var someObject = MyObjectBuilderSerializer.CreateNewObject(item.Key);
                MyObjectBuilder_PhysicalObject physObject = null;

                if (someObject is MyObjectBuilder_PhysicalObject)
                {
                    physObject = (MyObjectBuilder_PhysicalObject)someObject;
                }

                var gasContainer = someObject as MyObjectBuilder_GasContainerObject;
                if (gasContainer != null)
                {
                    gasContainer.GasLevel = 1f;
                }

                MyObjectBuilder_InventoryItem inventoryItem = new MyObjectBuilder_InventoryItem { Amount = amount, PhysicalContent = physObject };

                if (forcePush)
                {
                    InventoryUtils.ForceAddItems(inventory as MyInventory, amount, inventoryItem.PhysicalContent, null);
                }
                else
                {
                    if (inventory.CanItemsBeAdded(inventoryItem.Amount, defId))
                    {
                        inventory.AddItems(inventoryItem.Amount, inventoryItem.PhysicalContent, -1);
                    }
                    else
                    {
                        SGarage.Instance.SomeLog("Garage exception / error: AddToInventory() Wrong calculated space!");
                    }
                }
            }
            InventoryUtils.SortInventory((MyInventory)inventory);
        }
    }
}
