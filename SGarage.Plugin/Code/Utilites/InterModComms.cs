﻿using System;
using System.Collections.Generic;
using Sandbox.ModAPI;
using VRage.Game;
using VRage.Game.ModAPI;

namespace Foogs
{
    public static class InterModComms
    {
        private const long NETWORK_ID = 10666;
        public static Func<MyObjectBuilder_CubeGrid[], long, KeyValuePair<bool,Dictionary<long, Dictionary<string, int>>>> Garage_Mod_LoadChecks = null;//Server mod check
        public static Func<IMyCubeGrid[], long, bool> Garage_Mod_SaveChecks = null;//Server mod check

        public static void RegisterHandlers()
        {
            SGarage.Instance.SomeLog("Register InterModComms handlers");
            MyAPIGateway.Utilities.RegisterMessageHandler(NETWORK_ID, RegisterComponents);
        }

        public static void UnregisterHandlers()
        {
            SGarage.Instance.SomeLog("Unregister InterModComms handlers");
            MyAPIGateway.Utilities.UnregisterMessageHandler(NETWORK_ID, RegisterComponents);
        }

        #region InterModMsg
        public static void SendFuncToMod()
        {
            var myobj = new Dictionary<string, object>();
            Func<MyObjectBuilder_CubeGrid[], KeyValuePair<bool, Dictionary<long, Dictionary<string, int>>>> func = Limits.VanilaLimitsAPI.CheckLimits_future;
            myobj["Garage_CheckLimits_future"] = func;
            MyAPIGateway.Utilities.SendModMessage(NETWORK_ID, myobj);
            // SGarage.Instance.SomeLog("SendModMessage CheckLimits_future sended.");
        }

        private static void RegisterComponents(object data)
        {
            if (data is Dictionary<string, object>)
            {
                var dict = (data as Dictionary<string, object>);
                if (dict.ContainsKey("Garage_Mod_LoadChecks"))
                {
                    Garage_Mod_LoadChecks = (Func<MyObjectBuilder_CubeGrid[], long, KeyValuePair<bool,Dictionary<long, Dictionary<string, int>>>>)dict["Garage_Mod_LoadChecks"];
                }

                if (dict.ContainsKey("Garage_Mod_SaveChecks"))
                {
                    Garage_Mod_SaveChecks = (Func<IMyCubeGrid[], long, bool>)dict["Garage_Mod_SaveChecks"];
                }
            }
            // SGarage.Instance.SomeLog("Garage_Mod_doCheck received.");
        }
        #endregion
    }
}
