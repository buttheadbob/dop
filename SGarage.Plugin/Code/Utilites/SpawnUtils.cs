﻿using Havok;
using Sandbox;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using SpaceEngineers.Game.EntityComponents.DebugRenders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Torch;
using Torch.Mod;
using Torch.Mod.Messages;
using VRage;
using VRage.Game;
using VRage.ObjectBuilders;
using VRage.Utils;
using VRageMath;

namespace Foogs
{
    public static class SpawnUtils
    {
        private static readonly Random Random = new Random();

        public static bool SaveShipsToFile(ulong steamId, int totalpcu, int totalblocks, ref MyObjectBuilder_CubeGrid[] grids_objectbuilders, out string filename, out MyObjectBuilder_Definitions ob)
        {
            string gridname = "";
            if (grids_objectbuilders[0].DisplayName.Length > 30)
            {
                gridname = grids_objectbuilders[0].DisplayName.Substring(0, 30);
            }
            else
            {
                gridname = grids_objectbuilders[0].DisplayName;
            }
            string filenameexported = DateTime.Now.ToShortDateString() + "_" + DateTime.Now.ToLongTimeString() + "_" + totalpcu + "_" + totalblocks + "_" + gridname + "_" + Random.Next();
            var a1 = Path.GetInvalidPathChars();
            var b1 = Path.GetInvalidFileNameChars();

            var arraychar = a1.Concat(b1);
            foreach (char c in arraychar)
            {
                filenameexported = filenameexported.Replace(c.ToString(), ".");
            }
            filename = filenameexported;
            return SaveToFile(ref grids_objectbuilders, steamId, filenameexported, out ob); //backup grid gruop to folder
        }

        private static bool SaveToFile(ref MyObjectBuilder_CubeGrid[] gridstotpob, ulong steamid, string filenameexported, out MyObjectBuilder_Definitions ob)
        {
            MyObjectBuilder_ShipBlueprintDefinition myObjectBuilder_ShipBlueprintDefinition = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_ShipBlueprintDefinition>();
            myObjectBuilder_ShipBlueprintDefinition.Id = new MyDefinitionId(new MyObjectBuilderType(typeof(MyObjectBuilder_ShipBlueprintDefinition)), MyUtils.StripInvalidChars(filenameexported));
            myObjectBuilder_ShipBlueprintDefinition.DLCs = GetNecessaryDLCs(myObjectBuilder_ShipBlueprintDefinition.CubeGrids);
            myObjectBuilder_ShipBlueprintDefinition.CubeGrids = gridstotpob;
            myObjectBuilder_ShipBlueprintDefinition.RespawnShip = false;
            myObjectBuilder_ShipBlueprintDefinition.DisplayName = filenameexported;
            myObjectBuilder_ShipBlueprintDefinition.OwnerSteamId = Sync.MyId;
            //myObjectBuilder_ShipBlueprintDefinition.CubeGrids[0].DisplayName = blueprintDisplayName;
            MyObjectBuilder_Definitions myObjectBuilder_Definitions = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_Definitions>();
            myObjectBuilder_Definitions.ShipBlueprints = new MyObjectBuilder_ShipBlueprintDefinition[1];
            myObjectBuilder_Definitions.ShipBlueprints[0] = myObjectBuilder_ShipBlueprintDefinition;
            string mypathdir = Path.Combine(SGarage.Instance.Config.GaragePath, steamid.ToString());
            if (!Directory.Exists(mypathdir))
            {
                Directory.CreateDirectory(mypathdir);
            }

            string finalpath = Path.Combine(mypathdir, filenameexported + ".sbc");
            ob = myObjectBuilder_Definitions;
            var flag = MyObjectBuilderSerializer.SerializeXML(finalpath, false, myObjectBuilder_Definitions, null);
            if (flag)
                MyObjectBuilderSerializer.SerializePB(finalpath + MyObjectBuilderSerializer.ProtobufferExtension, true, myObjectBuilder_Definitions);

            return flag;
        }

        private static string[] GetNecessaryDLCs(MyObjectBuilder_CubeGrid[] cubeGrids)
        {
            if (cubeGrids.IsNullOrEmpty())
            {
                return null;
            }

            HashSet<string> hashSet = new HashSet<string>();
            for (int i = 0; i < cubeGrids.Length; i++)
            {
                foreach (MyObjectBuilder_CubeBlock builder in cubeGrids[i].CubeBlocks)
                {
                    MyCubeBlockDefinition cubeBlockDefinition = MyDefinitionManager.Static.GetCubeBlockDefinition(builder);
                    if (cubeBlockDefinition != null && cubeBlockDefinition.DLCs != null && cubeBlockDefinition.DLCs.Length != 0)
                    {
                        for (int j = 0; j < cubeBlockDefinition.DLCs.Length; j++)
                        {
                            hashSet.Add(cubeBlockDefinition.DLCs[j]);
                        }
                    }
                }
            }
            return hashSet.ToArray();
        }

        /// <summary>
        /// Remap, and spawn given grids group. And set correct position/rotation
        /// </summary>
        public static void SpawnSomeGrids(MyObjectBuilder_CubeGrid[] cubeGrids, ref MsgInfo info, long player_identity, string path)
        {
            SpawnCounter.SpawnCallback counter = null;
            //steal projection pos from client
            MatrixD projection_matrix = info.pos;
            MyAPIGateway.Entities.RemapObjectBuilderCollection(cubeGrids);
            counter = new SpawnCounter.SpawnCallback(cubeGrids.Count(), path, ref info);
            Vector3D projection_pos = projection_matrix.Translation;

            SetNewRotationGroup(ref cubeGrids, projection_matrix.Forward, projection_matrix.Up);
            Vector3D first_oldpos = cubeGrids[0].PositionAndOrientation.GetValueOrDefault().Position + Vector3D.Zero;
            BoundingBoxD pasteAABB_firstGrid = info.WAABB;
            for (int i = 0; i < cubeGrids.Length; i++)
            {
                // block stuff
                foreach (MyObjectBuilder_CubeBlock block in cubeGrids[i].CubeBlocks)
                {
                    if (block is MyObjectBuilder_Cockpit)
                    {
                        (block as MyObjectBuilder_Cockpit).Pilot = null;
                    }

                    if (block is MyObjectBuilder_CryoChamber)
                    {
                        (block as MyObjectBuilder_CryoChamber).Pilot = null;
                    }

                    if (block is MyObjectBuilder_LandingGear)
                    {
                        (block as MyObjectBuilder_LandingGear).AttachedEntityId = null;
                        (block as MyObjectBuilder_LandingGear).IsLocked = false;
                        (block as MyObjectBuilder_LandingGear).LockMode = SpaceEngineers.Game.ModAPI.Ingame.LandingGearMode.Unlocked;
                        (block as MyObjectBuilder_LandingGear).AutoLock = true;
                        (block as MyObjectBuilder_LandingGear).Enabled = true;
                    }

                    if (block is MyObjectBuilder_Drill)
                    {
                        (block as MyObjectBuilder_Drill).Enabled = false;
                    }
                }
                // set new position
                var ob = cubeGrids[i];
                if (i == 0)
                {
                    if (ob.PositionAndOrientation.HasValue)
                    {
                        var posiyto = ob.PositionAndOrientation.GetValueOrDefault();
                        posiyto.Position = projection_pos;
                        ob.PositionAndOrientation = posiyto;
                    }
                }
                else
                {
                    var o = ob.PositionAndOrientation.GetValueOrDefault();
                    o.Position = projection_pos + o.Position - first_oldpos;
                    ob.PositionAndOrientation = o;
                }
                var check = InterModComms.Garage_Mod_LoadChecks.Invoke(cubeGrids, player_identity);
                if (!check.Key)
                {
                    ReportOverlimits(info.SteamId, check.Value);
                    SGarage.Instance.SendMsgToChat("Can't spawn, not enough block limits!", info.SteamId);
                    MainLogic.SendCurrentStatus(info.Projector, info.SteamId);
                    return;
                }
            }
            var SteamId = info.SteamId;
            var Projector = info.Projector;
            for (int i = 0; i < cubeGrids.Length; i++)
            {
                if (i == 0)
                {
                    var gridbb = cubeGrids[i].CalculateBoundingBox();
                    gridbb.Translate(projection_matrix.Translation);
                }
            }
            //this place check should be per grid but later------------------------------------------------------------------------
            string entname = "";
            PlacementResult result = PlacementResult.Error;
            TorchBase.Instance.InvokeAsync(() => IsFreePlaceForPlacement(out result, out entname, SteamId, pasteAABB_firstGrid)).Wait();
            if (result != PlacementResult.Ok)
            {
                if (result == PlacementResult.Collision)
                {
                    SGarage.Instance.SendMsgToChat($"Area not clear can't spawn! Fail because collision with:[{entname}]", SteamId);
                }
                else
                {
                    SGarage.Instance.SendMsgToChat($"Area not clear can't spawn! Fail because: [{result}]", SteamId);
                }

                MainLogic.SendCurrentStatus(Projector, SteamId);
                return;
            }
            //------------------------------------------------------------------------------------------------------------------
            for (int i = 0; i < cubeGrids.Length; i++)
            {
                MyAPIGateway.Entities.CreateFromObjectBuilderParallel(cubeGrids[i], false, counter.Increment);
            }
        }

        private static void ReportOverlimits(ulong steamId, Dictionary<long, Dictionary<string, int>> overlimits)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Block Limits:");
            sb.AppendLine("------------------------------------------------------------------------------");
            var players = MySession.Static.Players.GetAllIdentities();
            foreach (var player in overlimits)
            {
                var found = players.Where(x => x.IdentityId == player.Key);
                var name = player.Key.ToString();
                if (found.Count() >= 1)
                {
                    name = found.First().DisplayName;
                }

                sb.AppendLine("Overlimits for " + name);
                foreach (var overlim in player.Value)
                {
                    sb.AppendLine($") {overlim.Key} -> [ {overlim.Value} ]");
                }
                sb.AppendLine("------------------------------------------------------------------------------");
            }
            ModCommunication.SendMessageTo(new DialogMessage("Garage overlimits report", "", sb.ToString()), steamId);
        }

        public enum PlacementResult
        {
            Error,
            Ok,
            SafeZone,
            OutOfWorld,
            Collision,
            Voxel
        }
        private static bool IsFreePlaceForPlacement(out PlacementResult result, out string entname, ulong steamid, BoundingBoxD orig_bb)
        {
            entname = "";
            var rad = (orig_bb.Center - orig_bb.Min).Length();
            rad = rad * 0.5;
            BoundingBoxD bb = new BoundingSphereD(orig_bb.Center, rad).GetBoundingBox();
            bool flag = true; //true is good
            result = PlacementResult.Ok;

            if (!MyEntities.IsInsideWorld(bb.Center))
            {
                flag = false;
                result = PlacementResult.OutOfWorld;
            }

            if (!MySessionComponentSafeZones.IsActionAllowed(bb, VRage.Game.ObjectBuilders.Components.MySafeZoneAction.Building, 0L, steamid))
            {
                entname = "SafeZone";
                flag = false;
                result = PlacementResult.SafeZone;
            }

            if (flag)
            {
                var matr = new MatrixD();
                matr.Translation = bb.Center;
                if (IsAabbInsideVoxel(ref matr, ref bb))
                {
                    entname = "Voxels";
                    result = PlacementResult.Voxel;
                    return flag;
                }
            }

            if (flag)
            {
                var a = MyEntities.GetEntitiesInAABB(ref bb, true);
                foreach (var ent in a)
                {
                    if (ent is MyCubeGrid)
                    {
                        MySafeZone mySafeZone = null;
                        foreach (MySafeZone mSafeZone in MySessionComponentSafeZones.SafeZones)
                        {
                            if ((mSafeZone.WorldMatrix.Translation - orig_bb.Center).LengthSquared() < mSafeZone.Radius * mSafeZone.Radius)
                            {
                                mySafeZone = mSafeZone;
                            }
                        }
                        if (IsShapePenetrating(orig_bb.Center, (float)rad, mySafeZone))
                        {
                            entname = ent.DisplayName;
                            result = PlacementResult.Collision;
                            flag = false;
                            SGarage.Log.Info("IsFreePlaceForPlacement place is not free");
                            break;
                        }
                    }
                }
            }

            return flag;
        }

        public static bool IsShapePenetrating(Vector3D center, float radius, MySafeZone zone)
        {
            HkShape shape = new HkSphereShape(radius);
            Quaternion identity = Quaternion.Identity;
            try
            {
                return MyEntities.IsShapePenetrating(shape, ref center, ref identity, 15, zone);
            }
            finally
            {
                shape.RemoveReference();
            }

        }
        public static bool IsAabbInsideVoxel(ref MatrixD worldMatrix, ref BoundingBoxD abbbox)
        {
            List<MyVoxelBase> list = new List<MyVoxelBase>();
            MyGamePruningStructure.GetAllVoxelMapsInBox(ref abbbox, list);
            foreach (MyVoxelBase myVoxelBase in list)
            {
                if (myVoxelBase.IsAnyAabbCornerInside(ref worldMatrix, abbbox))
                {
                    return true;
                }
            }
            return false;
        }

        public static string GetStoredGridInfo(ref MyObjectBuilder_CubeGrid[] grids)
        {
            int FinalBlocksCount = 0;
            int FinalBlocksPCU = 0;
            float FinalBlocksMass = 0;
            string subsnames = "";
            string subs = "";
            foreach (var myCubeGrid in grids)
            {
                if (myCubeGrid != grids[0])
                {
                    subsnames += myCubeGrid.DisplayName + ",";
                }
                foreach (var slimblock in myCubeGrid.CubeBlocks)
                {

                    MyDefinitionId defId = new MyDefinitionId(slimblock.TypeId, slimblock.SubtypeId);
                    MyCubeBlockDefinition myCubeBlockDefinition;
                    if (MyDefinitionManager.Static.TryGetCubeBlockDefinition(defId, out myCubeBlockDefinition))
                    {

                        FinalBlocksPCU += myCubeBlockDefinition.PCU; //HACK
                        FinalBlocksMass += myCubeBlockDefinition.Mass;
                    }
                }
                FinalBlocksCount += myCubeGrid.CubeBlocks.Count;
            }
            ////>Name:[]Blocks:[] PCU:[] Mass:[] kg<
            if (grids.Length > 1)
            {
                subs = $"Subgrids: [{ subsnames.TrimEnd(',')}]{Environment.NewLine}";
            }

            string result = $"Main ship: [{grids[0].DisplayName}]{Environment.NewLine}{subs}Total blocks: [{FinalBlocksCount}]{Environment.NewLine}Total PCU: [{FinalBlocksPCU}] {Environment.NewLine}{PrintLimits(grids)}";
            // SGarage.SomeLog($"GetStoredGridInfo [{result}]");
            return result;
        }

        public static StringBuilder PrintLimits(MyObjectBuilder_CubeGrid[] grids)
        {
            Dictionary<string, int> BlocksAndOwnerForLimits = new Dictionary<string, int>();
            foreach (var myCubeGrid in grids)
            {
                foreach (var slimblock in myCubeGrid.CubeBlocks)
                {
                    MyDefinitionId defId = new MyDefinitionId(slimblock.TypeId, slimblock.SubtypeId);
                    MyCubeBlockDefinition myCubeBlockDefinition;
                    if (MyDefinitionManager.Static.TryGetCubeBlockDefinition(defId, out myCubeBlockDefinition))
                    {
                        string blockPairName = myCubeBlockDefinition.BlockPairName;
                        short blockTypeLimit = MySession.Static.GetBlockTypeLimit(blockPairName);
                        if (blockTypeLimit > 0)
                        {
                            if (BlocksAndOwnerForLimits.ContainsKey(blockPairName))
                            {
                                BlocksAndOwnerForLimits[blockPairName] = BlocksAndOwnerForLimits[blockPairName] + 1;
                            }
                            else
                            {
                                BlocksAndOwnerForLimits[blockPairName] = 1;
                            }
                        }
                    }
                }
            }
            StringBuilder text = new StringBuilder();
            if (BlocksAndOwnerForLimits.Any())
            {

                text.AppendLine($"Limits:{Environment.NewLine}");
                foreach (var item in BlocksAndOwnerForLimits)
                {
                    text.AppendLine($"[ {item.Key} - {item.Value} ]");
                }
            }
            return text;
        }

        private static void SetNewRotationGroup(ref MyObjectBuilder_CubeGrid[] grids, Vector3D forward, Vector3D up)
        {
            Vector3 m_pasteDirUp = up;
            Vector3 m_pasteDirForward = forward;
            var main_grid = grids[0];
            var main_grid_pos = main_grid.PositionAndOrientation.Value.Position;
            var main_grid_matrix = main_grid.PositionAndOrientation.Value.GetMatrix();

            int i = 0;
            var rotation_matrix = Matrix.Multiply(Matrix.Invert(main_grid_matrix), Matrix.CreateWorld((Vector3D)main_grid_pos, m_pasteDirForward, m_pasteDirUp));

            while (i < grids.Length && i <= grids.Length - 1)
            {
                //copied from UpdateGridTransformations
                if (grids[i].PositionAndOrientation != null)
                {
                    grids[i].CreatePhysics = true;
                    grids[i].DestructibleBlocks = true;
                    grids[i].EnableSmallToLargeConnections = true;
                    grids[i].PositionAndOrientation = new MyPositionAndOrientation?(new MyPositionAndOrientation(grids[i].PositionAndOrientation.Value.GetMatrix() * rotation_matrix));
                    grids[i].PositionAndOrientation.Value.Orientation.Normalize();
                    i++;
                }
            }
        }

        public static void RemapOwnership(MyObjectBuilder_CubeGrid[] cubeGrids, long new_owner, bool ShareBuilder)
        {
            foreach (var grid in cubeGrids)
            {
                foreach (var block in grid.CubeBlocks)
                {
                    if (!ShareBuilder)
                    {
                        block.BuiltBy = new_owner;
                    }
                    block.Owner = new_owner;
                    block.ShareMode = MyOwnershipShareModeEnum.Faction;
                }
            }
        }
    }
}
