﻿using System;
using System.Collections.Generic;
using System.Text;
using Sandbox.Definitions;
using Sandbox.Game.World;
using VRage.Game;
using VRage.Utils;

namespace Foogs
{
    public static class Limits
    {
        public static int CheckLimits_PCUCount(MyObjectBuilder_CubeGrid[] gruop)
        {
            int pcusOfGroup = 0;
            long blockCountOfGroup = 0L;

            foreach (var grid in gruop)
            {
                var blocks = grid.CubeBlocks;
                foreach (var block in blocks)
                {
                    MyDefinitionId defId = new MyDefinitionId(block.TypeId, block.SubtypeId);
                    MyCubeBlockDefinition myCubeBlockDefinition;
                    if (MyDefinitionManager.Static.TryGetCubeBlockDefinition(defId, out myCubeBlockDefinition))
                    {
                        string blockPairName = myCubeBlockDefinition.BlockPairName;
                        pcusOfGroup += myCubeBlockDefinition.PCU;
                        blockCountOfGroup++;
                    }
                }
            }
            return pcusOfGroup;
        }

        public static class VanilaLimitsAPI
        {
            /// <summary>
            /// Returns true if all good.
            /// </summary>
            /// <param name="grids"></param>
            /// <returns></returns>
            public static KeyValuePair<bool, Dictionary<long, Dictionary<string, int>>> CheckLimits_future(MyObjectBuilder_CubeGrid[] grids)
            {
                int MaxBlockCount = 0;
                Dictionary<long, int> BlocksPCU = new Dictionary<long, int>();
                Dictionary<long, int> BlockCount = new Dictionary<long, int>();
                Dictionary<long, Dictionary<string, int>> BlocksAndBuilders = new Dictionary<long, Dictionary<string, int>>();
                foreach (var myCubeGrid in grids)
                {
                    foreach (var slimblock in myCubeGrid.CubeBlocks)
                    {
                        MyDefinitionId defId = new MyDefinitionId(slimblock.TypeId, slimblock.SubtypeId);
                        MyCubeBlockDefinition myCubeBlockDefinition;
                        if (MyDefinitionManager.Static.TryGetCubeBlockDefinition(defId, out myCubeBlockDefinition))
                        {
                            string blockPairName = myCubeBlockDefinition.BlockPairName;
                            long blockBuilder = slimblock.BuiltBy;
                            if (blockBuilder == 0) //skip builded by nobody
                            {
                                continue;
                            }

                            if (BlocksAndBuilders.ContainsKey(blockBuilder))
                            {
                                var dictforuser = BlocksAndBuilders[blockBuilder];
                                if (dictforuser.ContainsKey(blockPairName))
                                {
                                    Dictionary<string, int> blocksPerType = dictforuser;
                                    (blocksPerType)[blockPairName] += 1;
                                }
                                else
                                {
                                    dictforuser[blockPairName] = 1;
                                }
                                BlocksAndBuilders[blockBuilder] = dictforuser;
                            }
                            else
                            {
                                BlocksAndBuilders[blockBuilder] = new Dictionary<string, int> { { blockPairName, 1 } };
                            }

                            if (BlocksPCU.ContainsKey(blockBuilder))
                                BlocksPCU[blockBuilder] += myCubeBlockDefinition.PCU;
                            else
                                BlocksPCU[blockBuilder] = myCubeBlockDefinition.PCU;

                            if (BlockCount.ContainsKey(blockBuilder))
                                BlockCount[blockBuilder] += 1;
                            else
                                BlockCount[blockBuilder] = 1;
                        }
                    }

                    MaxBlockCount = Math.Max(MaxBlockCount, myCubeGrid.CubeBlocks.Count);
                }
                bool result = true;
                var overLimitsAll = new Dictionary<long, Dictionary<string, int>>();
                MySession.LimitResult checkResult;
                foreach (var player in BlocksAndBuilders)
                {
                    Dictionary<string, int> overLimits;
                    result &= CheckLimitsInternal(out overLimits, out checkResult, player.Key, player.Value, BlocksPCU[player.Key], BlockCount[player.Key], MaxBlockCount);
                    if(checkResult == MySession.LimitResult.BlockTypeLimit)
                    {
                        overLimitsAll[player.Key] = overLimits;
                    }
                }

                return new KeyValuePair<bool, Dictionary<long, Dictionary<string, int>>>(result, overLimitsAll);
            }

            private static bool CheckLimitsInternal(out Dictionary<string, int> overLimits, out MySession.LimitResult checkResult,
            long playerID, Dictionary<string, int> blocksForCheck, int blocksPCU, int blocksToBuild = 0, int maxBlockCount = 0)
            {
                overLimits = new Dictionary<string, int>();
                if (MySession.Static.BlockLimitsEnabled == MyBlockLimitsEnabledEnum.NONE)
                {
                    checkResult = MySession.LimitResult.Passed;
                    return false;
                }

                if (MySession.Static.MaxGridSize != 0 && maxBlockCount > MySession.Static.MaxGridSize)
                {
                    checkResult = MySession.LimitResult.MaxGridSize;
                    return false;
                }

                MyIdentity myIdentity = MySession.Static.Players.TryGetIdentity(playerID);
                if (myIdentity == null)
                {
                    MyLog.Default.WriteLineAndConsole($"CheckLimitsInternal ERROR: Player Identity {playerID} not found!");
                    checkResult = MySession.LimitResult.Passed;
                    return true;
                }

                MyBlockLimits playerLimits = myIdentity.BlockLimits;
                if (MySession.Static.BlockLimitsEnabled == MyBlockLimitsEnabledEnum.PER_FACTION && MySession.Static.Factions.GetPlayerFaction(myIdentity.IdentityId) == null)
                {
                    checkResult = MySession.LimitResult.NoFaction;
                    return false;
                }

                if (playerLimits != null)
                {
                    if (MySession.Static.MaxBlocksPerPlayer != 0 && playerLimits.BlocksBuilt + blocksToBuild > playerLimits.MaxBlocks)
                    {
                        checkResult = MySession.LimitResult.MaxBlocksPerPlayer;
                        return false;
                    }

                    if (MySession.Static.TotalPCU != 0 && blocksPCU > playerLimits.PCU)
                    {
                        checkResult = MySession.LimitResult.PCU;
                        return false;
                    }

                    if (blocksForCheck != null)
                    {

                        foreach (var limRule in MySession.Static.BlockTypeLimits)
                        {
                            if (blocksForCheck.ContainsKey(limRule.Key))
                            {
                                int blockstobuild = blocksForCheck[limRule.Key];
                                MyBlockLimits.MyTypeLimitData playerBlockTypeLimit;
                                if (playerLimits.BlockTypeBuilt.TryGetValue(limRule.Key, out playerBlockTypeLimit))
                                {
                                    if ((playerBlockTypeLimit.BlocksBuilt + blockstobuild) > limRule.Value)
                                    {
                                        //save overlimit to string
                                        if (overLimits.ContainsKey(limRule.Key))
                                            overLimits[limRule.Key] += 1;
                                        else
                                            overLimits[limRule.Key] = 1;
                                    }
                                }
                            }
                        }

                        if (overLimits.Count > 0)
                        {
                            checkResult = MySession.LimitResult.BlockTypeLimit;
                            return false;
                        }
                    }
                }

                checkResult = MySession.LimitResult.Passed;
                return true;
            }
        }
    }
}