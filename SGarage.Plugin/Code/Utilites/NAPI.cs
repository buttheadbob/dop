﻿using System;
using System.Globalization;
using System.Text;
using Sandbox.Game.EntityComponents;
using VRage.Game.Components;
using VRage.ModAPI;

namespace Foogs
{
    public static class Serialization
    {
        public static MyModStorageComponentBase GetOrCreateStorage(this IMyEntity entity) { return entity.Storage = entity.Storage ?? new MyModStorageComponent(); }

        public static string GetStorageData(this IMyEntity entity, Guid guid)
        {
            if (entity.Storage == null) return null;
            string data = null;
            if (entity.Storage.TryGetValue(guid, out data))
            {
                return data;
            }
            else
            {
                return null;
            }
        }

        public static void SetStorageData(this IMyEntity entity, Guid guid, String data)
        {
            if (entity.Storage == null && data == null)
            {
                return;
            }
            entity.GetOrCreateStorage().SetValue(guid, data);
        }
    }
}
