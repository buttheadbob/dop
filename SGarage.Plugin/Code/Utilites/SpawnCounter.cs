﻿using System.Collections.Generic;
using System.IO;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using VRage.ModAPI;

namespace Foogs
{
    public static class SpawnCounter
    {
        public class SpawnCallback
        {
            private int _counter;
            private List<IMyEntity> _entlist;
            private readonly int _maxCount;
            private readonly string _path;
            private readonly MsgInfo _info;

            public SpawnCallback(int count, string path, ref MsgInfo info)
            {
                _counter = 0;
                _entlist = new List<IMyEntity>();
                _maxCount = count;
                _path = path;
                _info = info;
            }

            public void Increment(IMyEntity ent)
            {
                _counter++;
                _entlist.Add(ent);

                if (_counter < _maxCount)
                {
                    return;
                }
                FinalSpawnCallback(_entlist, _path, _info);
            }

            private static void FinalSpawnCallback(List<IMyEntity> grids, string path, MsgInfo info)
            {
                SGarage.Instance.SomeLog("FinalSpawnCallback!");
                MyAPIGateway.Parallel.StartBackground(() =>
                {
                    try
                    {
                        MainLogic.AfterSpawn(ref info);
                        File.Delete(path + VRage.ObjectBuilders.MyObjectBuilderSerializer.ProtobufferExtension);
                    }
                    catch
                    {
                        SGarage.Instance.SomeLog("(FinalSpawnCallback) Error while AfterSpawn / rename file in garage. " + path,3);
                    }
                });               

                foreach (MyCubeGrid ent in grids)
                {
                    ent.DetectDisconnectsAfterFrame();
                    MyAPIGateway.Entities.AddEntity(ent, true);
                }
            }
        }
    }
}
