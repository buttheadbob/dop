﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI;
using VRage.Game.Components;

namespace Foogs
{
    public static class ExportImportTool
    {        
        public static void Export()
        {
            List<GarageInfo> data_hive_dict = new List<GarageInfo>();            
            var blocks = GetFatBlocksFromAllGrids((x) => (x.BlockDefinition.Id.SubtypeId == Foogs.MainLogic.garage_subtypes_h.FirstOrDefault()));

            foreach (var b in blocks)
            {
                try
                {
                    if (b.FatBlock is IMyProjector)
                    {
                        var blockEntityId = b.FatBlock.EntityId;

                        SGarage.Log.Error($"Process block id = [{blockEntityId}]");
                        MainLogic.TryGetBlockGridStorage(b.FatBlock as IMyProjector, out ModStorageGrid grid);
                        var blocksetting = MainLogic.GetBlockSettingsStorage(b.FatBlock as IMyProjector);

                        if (grid.Original_blockid == 0)
                        {
                            SGarage.Log.Error($"Original_blockid = 0, skip this block! blockEntityId [{blockEntityId}]");
                            continue;
                        }

                        if (grid.Original_blockid != blockEntityId)
                        {
                            SGarage.Log.Error($"Block id not match! Original_blockid ={grid.Original_blockid} != blockEntityId {blockEntityId}");
                        }

                        if (!(System.IO.File.Exists(grid.Path)))
                        {
                            SGarage.Log.Error($"Grid file not exists!");
                        }

                        data_hive_dict.Add(new GarageInfo() { blockid = blockEntityId, gridinfo = grid, settings = blocksetting });
                    }
                }
                catch (Exception ex)
                {
                    SGarage.Log.Error($"[SlimGarage] Export exeption! =" + ex.ToString());
                }
            }

            SGarage.Log.Error($"Saved {data_hive_dict.Count()} garages.");

            string totalStr = JsonConvert.SerializeObject(data_hive_dict, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All,
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full
            });

            File.WriteAllText(Path.Combine(Path.Combine(Environment.CurrentDirectory, "Instance"), "GarageExportedData.json"), totalStr);            
        }

        public static void Import()
        {
            List<GarageInfo> data_hive_dict = new List<GarageInfo>();
            try
            {
                var path = Path.Combine(Path.Combine(Environment.CurrentDirectory, "Instance"), "GarageExportedData.json");
                var str = File.ReadAllText(path);
               
                data_hive_dict = JsonConvert.DeserializeObject<List<GarageInfo>>(str, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All,
                    TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full
                });
            }
            catch (FileNotFoundException ex)
            {
                SGarage.Log.Error($"[SlimGarage] Imported file not found! path:{ex}");
            }
            catch (Exception ex)
            {
                SGarage.Log.Error($"[SlimGarage] file parse exception! {ex}");
            }
            SGarage.Log.Error($"Start import {data_hive_dict.Count()} garages.");
            var blocks = GetFatBlocksFromAllGrids((x) => (x.BlockDefinition.Id.SubtypeId == Foogs.MainLogic.garage_subtypes_h.FirstOrDefault()));
            int restored_count = 0;
            foreach (var garabeinfo in data_hive_dict)
            {
                try
                {
                    var bl = blocks.Where(x => x.FatBlock.EntityId == garabeinfo.blockid);
                    if (!bl.Any()) continue;

                    IMyProjector prj = bl.FirstOrDefault().FatBlock as IMyProjector;
                    prj.SetProjectedGrid(null);

                    if (prj.Storage == null)
                    {
                        prj.Storage = new MyModStorageComponent();
                        prj.Components.Add<MyModStorageComponentBase>(prj.Storage);
                    }
                    else
                    {
                        if (prj.Storage != null && prj.Storage.Where(x => x.Key == MainLogic.STORAGE_GUID).Any())
                        {
                            prj.Storage.Remove(prj.Storage.Where(x => x.Key == MainLogic.STORAGE_GUID).FirstOrDefault());
                        }
                        if (prj.Storage != null &&  prj.Storage.Where(x => x.Key == MainLogic.SETTINGS_GUID).Any())
                        {
                            prj.Storage.Remove(prj.Storage.Where(x => x.Key == MainLogic.SETTINGS_GUID).FirstOrDefault());
                        }
                        if (prj.Storage != null && prj.Storage.Where(x => x.Key == MainLogic.STORAGE_GUID).Any())
                        {
                            prj.Storage.Remove(prj.Storage.Where(x => x.Key == MainLogic.STORAGE_GUID).FirstOrDefault());
                        }
                        if (prj.Storage != null && prj.Storage.Where(x => x.Key == MainLogic.SETTINGS_GUID).Any())
                        {
                            prj.Storage.Remove(prj.Storage.Where(x => x.Key == MainLogic.SETTINGS_GUID).FirstOrDefault());
                        }
                    }

                    prj.SetStorageData(MainLogic.STORAGE_GUID, Convert.ToBase64String(MyAPIGateway.Utilities.SerializeToBinary(garabeinfo.gridinfo)));                    
                    prj.SetStorageData(MainLogic.SETTINGS_GUID, Convert.ToBase64String(MyAPIGateway.Utilities.SerializeToBinary(garabeinfo.settings)));
                    restored_count++;
                }
                catch (Exception ex)
                {
                    SGarage.Log.Error($"[SlimGarage] Import exception! =" + ex.ToString());
                }
            }
            SGarage.Log.Error($"Restored {restored_count} garages.");
        }

        //cant reference napi i dont know why.
        public static List<MySlimBlock> GetFatBlocksFromAllGrids(Func<MySlimBlock, bool> filter) { return MyEntities.GetEntities().OfType<MyCubeGrid>().SelectMany(grid => grid.GetFatBlocks().Select((x) => x.SlimBlock).Where(filter)).ToList(); }

        [JsonObject]
        [Serializable]
        public class GarageInfo
        {            
            public long blockid;
            public ModStorageSettings settings;
            public ModStorageGrid gridinfo;
        }
    }
}
