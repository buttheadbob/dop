﻿using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using VRage;
using VRage.Game;
using VRage.Game.ObjectBuilders.ComponentSystem;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.ObjectBuilders;

namespace Foogs
{
    public static class ReassemblyUtils
    {
        public static Dictionary<MyDefinitionId, MyFixedPoint> GetAllComponentsAndInventories(ref MyObjectBuilder_CubeGrid[] gridsOB)
        {
            var gridComp = new Dictionary<MyDefinitionId, MyFixedPoint>();
            var invComp = new Dictionary<MyDefinitionId, MyFixedPoint>();
            try
            {
                var grids = gridsOB;
                foreach (var grid in grids)
                {
                    var blocks = new List<MyObjectBuilder_CubeBlock>();
                    blocks = grid.CubeBlocks;

                    foreach (var block in blocks)
                    {
                        MyCubeBlockDefinition blockDef;
                        if (block is MyObjectBuilder_TerminalBlock)
                        {
                            blockDef = MyDefinitionManager.Static.GetCubeBlockDefinition(block);
                        }
                        else
                        {
                            blockDef = MyDefinitionManager.Static.GetCubeBlockDefinition(block);
                        }
                        #region Go through component List + exclude construction level.
                        foreach (var component in blockDef.Components)
                        {
                            if (!gridComp.ContainsKey(component.Definition.Id))
                            {
                                gridComp.Add(component.Definition.Id, 0);
                            }
                            var a = gridComp[component.Definition.Id];
                            var b = component.Count;
                            var c = a + b;
                            gridComp[component.Definition.Id] = c;
                        }

                        // This will subtract off components missing from a partially built cube.
                        // This also includes the Construction Inventory.
                        var missingComponents = GetMissingComponents(block, blockDef);
                        foreach (var kvp in missingComponents)
                        {
                            var definitionid = new MyDefinitionId(typeof(MyObjectBuilder_Component), kvp.Key);
                            gridComp[definitionid] -= kvp.Value;
                        }
                        #endregion
                        if (block is MyObjectBuilder_TerminalBlock)
                        {
                            var tank = block as Sandbox.Common.ObjectBuilders.MyObjectBuilder_OxygenTank;
                            var gasTankDef = blockDef as MyGasTankDefinition;
                            MyDefinitionId definitionid;

                            if (gasTankDef != null && tank != null)
                            {
                                float volumeinlitres = gasTankDef.Capacity * tank.FilledRatio;
                                MyObjectBuilder_Base someobject = MyObjectBuilderSerializer.CreateNewObject(gasTankDef.StoredGasId);
                                if (someobject is MyObjectBuilder_GasProperties)
                                {
                                    // SGarage.SomeLog($"Gas tank detected! {tank.Name}");
                                    //SGarage.SomeLog($"Capacity: [{gasTankDefintion.Capacity}] FilledRatio: [{tank.FilledRatio}] volumeinlitres: [{volumeinlitres}]");
                                    GasToOre((MyObjectBuilder_GasProperties)someobject, volumeinlitres, out definitionid, out MyFixedPoint kg);
                                    // SGarage.SomeLog($"definitionid: [{definitionid.SubtypeName}] kg: [{kg}]");
                                    if (!invComp.ContainsKey(definitionid))
                                        invComp.Add(definitionid, 0);
                                    invComp[definitionid] += kg;
                                }
                            }
                            #region Go through all other Inventories for components/items.
                            // Inventory check
                            if (block.ComponentContainer != null)
                            {
                                var compData = block.ComponentContainer.Components.Find(s => s.Component.TypeId == typeof(MyObjectBuilder_Inventory));
                                if (compData != null)
                                {
                                    var inventory = (compData.Component as MyObjectBuilder_Inventory).Items;
                                    if (inventory == null || inventory.Count == 0) continue;


                                    var list = inventory;
                                    foreach (var item in list)
                                    {

                                        var id = item.PhysicalContent.GetId();
                                        if (!invComp.ContainsKey(id))
                                            invComp.Add(id, 0);
                                        invComp[id] += item.Amount;

                                        // Go through Gas bottles.
                                        var gasContainer = item.PhysicalContent as MyObjectBuilder_GasContainerObject;
                                        if (gasContainer != null)
                                        {
                                            var defintion = (MyOxygenContainerDefinition)MyDefinitionManager.Static.GetPhysicalItemDefinition(item.PhysicalContent.GetId());

                                            float volumeinlitres = defintion.Capacity * gasContainer.GasLevel;

                                            MyObjectBuilder_Base someobject = MyObjectBuilderSerializer.CreateNewObject(defintion.StoredGasId);

                                            if (someobject is MyObjectBuilder_GasProperties)
                                            {
                                                GasToOre((MyObjectBuilder_GasProperties)someobject, volumeinlitres, out definitionid, out MyFixedPoint kg);
                                                if (!invComp.ContainsKey(definitionid))
                                                    invComp.Add(definitionid, 0);
                                                invComp[definitionid] += kg;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                }

                foreach (var item in gridComp)
                {
                    if (invComp.ContainsKey(item.Key))
                    {
                        invComp[item.Key] += item.Value;
                    }
                    else
                    {
                        invComp.Add(item.Key, item.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                SGarage.Instance.SomeLog($"(GetAllComponentsAndInventories) Exception: {ex}", 2);
            }
            return invComp;
        }

        public static readonly float ICE_LITER_TO_KG = 1;
        public static readonly float OIL_LITER_TO_KG = 0.05f;
        public static readonly float KERO_LITER_TO_KG = 1;
        private static void GasToOre(MyObjectBuilder_GasProperties gaspropertis, float litres, out MyDefinitionId definitionid, out MyFixedPoint kg)
        {
            float ratio;
            string name;

            if (gaspropertis.SubtypeName.Contains("Kerosene"))
            {
                ratio = KERO_LITER_TO_KG;
                name = "FrozenOil";
            }
            else
            {
                if (gaspropertis.SubtypeName.Contains("Oil"))
                {
                    ratio = OIL_LITER_TO_KG;
                    name = "FrozenOil";
                }
                else
                {
                    ratio = ICE_LITER_TO_KG;
                    name = "Ice";
                }
            }

            MyObjectBuilder_Ore physicalContent = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_Ore>(name);
            definitionid = physicalContent.GetId();
            kg = (MyFixedPoint)ratio * litres;
        }

        private static Dictionary<string, int> GetMissingComponents(MyObjectBuilder_CubeBlock block, MyCubeBlockDefinition blockDefintion)
        {
            var result_Dictionary = new Dictionary<string, int>();
            var stack = new MyComponentStack(blockDefintion, block.IntegrityPercent, block.BuildPercent);
            stack.GetMissingComponents(result_Dictionary);
            return result_Dictionary;
        }
    }
}