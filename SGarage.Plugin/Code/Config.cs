﻿using Torch;

namespace Foogs
{
    public class FoogsGarageConfig : ViewModel
    {
        private bool _enable;
        private string _garagepath;
        private int _loadcooldown;
        
        public bool Enabled { get => _enable; set => SetValue(ref _enable, value); }
        public string GaragePath { get => _garagepath; set { SetValue(ref _garagepath, value); } }
        public int LoadCooldown { get => _loadcooldown; set { SetValue(ref _loadcooldown, value); } }
    }
}