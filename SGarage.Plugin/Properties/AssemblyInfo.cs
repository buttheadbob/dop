﻿using System.Resources;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FoogsSlimGarage")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("Foogs")]
[assembly: AssemblyProduct("Plugin for Torch")]
[assembly: AssemblyCopyright("Copyright © Foogs 2020")]
[assembly: AssemblyTrademark("Foogs")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

[assembly: Guid("49d74f80-540e-4e6e-9e16-3928719b5f0d")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
#else
[assembly: AssemblyConfiguration("Release")]
[assembly: AssemblyVersion("1.2.17")]
[assembly: AssemblyFileVersion("1.2.17")]
[assembly: NeutralResourcesLanguage("en")]
#endif