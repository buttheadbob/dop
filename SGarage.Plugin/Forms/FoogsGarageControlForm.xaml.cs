﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Foogs
{
    /// <summary>
    /// Логика взаимодействия для FoogsShipsFixControlForm.xaml
    /// </summary>
    public partial class FoogsGarageControlForm : UserControl
    {
        public FoogsGarageControlForm()
        {
            InitializeComponent();
            string temp = SGarage.Instance.Config.GaragePath;
            SGarage.Instance.Config.GaragePath = temp;
        }

        private void Textbox_garage_path_LostFocus(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(this.textbox_garage_path.Text))
            {
                this.textbox_garage_path.Background = new SolidColorBrush(Colors.LightGreen);
                this.textbox_garage_path.Background.Opacity = 70;
            }
            else
            {
                this.textbox_garage_path.Background = new SolidColorBrush(Colors.Red);
                this.textbox_garage_path.Background.Opacity = 60;
            }
        }

        private void btn_export_grids_Click(object sender, RoutedEventArgs e)
        {
            ExportImportTool.Export();
        }

        private void btn_import_grids_Click(object sender, RoutedEventArgs e)
        {
            ExportImportTool.Import();
        }
    }
}
