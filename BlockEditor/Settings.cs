﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;

namespace BlockEditor
{
    public class Settings
    {
        public const string DefaultScript = "DefaultScript";
        public const string GameContentPath = "GameContentPath";
        public const string DefaultLoadedPaths = "DefaultLoadedPaths";
        public const string FavoriteFields = "FavoriteFields";
        public const string RGBMapping = "RGBMapping";
        public const string DefaultPlanet = "DefaultPlanet";
        public const string DefaultPlanetImages = "DefaultPlanetImages";
        
        public static Dictionary<string, string> Data = new Dictionary<string, string>();

        public static void Save()
        {
            var sb = new StringBuilder();
            foreach (var l in Data)
            {
                sb.AppendLine(l.Key+"="+l.Value);
            }
            File.WriteAllText("settings.txt", sb.ToString());
        }
        
        public static void Init()
        {
            if (File.Exists("settings.txt"))
            {
                var settings = File.ReadAllText("settings.txt");
                var lines = settings.Split(new string[] {"\n","\r","\r\n"}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var l in lines)
                {
                    var data = l.Split("=");
                    if (data.Length == 2)
                    {
                        Data[data[0]] = data[1];
                    }
                }
            }

            if (!Settings.Data.ContainsKey(Settings.GameContentPath))
            {
                MessageBox.Show("Please set GameContentPath");
                Settings.Data[GameContentPath] = FolderPicker.Show();
            }
              
                
            if (!Settings.Data.ContainsKey(Settings.DefaultScript))
            {
                MessageBox.Show("Please set Default Script");
                Settings.Data[DefaultScript] = GlobalEditor.OpenFileDialog()[0];
            }
            
            if (!Settings.Data.ContainsKey(Settings.DefaultLoadedPaths))
            {
                var s = "";
                while (true)
                {
                    var r = MessageBox.Show("Do you want to add folder that loaded by default? (You can set multiple)", "Block editor", MessageBoxButton.YesNo);
                    if (r != MessageBoxResult.OK)
                    {
                        break;
                    }

                    var dlg = GlobalEditor.OpenFolderDialog();
                    if (dlg == null || dlg.Length == 0)
                    {
                        break;
                    }
                    
                    s += dlg[0]+",";
                }

                Settings.Data[DefaultLoadedPaths] = s;
            }
            
            if (!Settings.Data.ContainsKey(Settings.RGBMapping))
            {
                MessageBox.Show("Please set RGBMapping.xml for PlanetEditor");
                Settings.Data[RGBMapping] = GlobalEditor.OpenFileDialog()[0];
            }
            
            if (!Settings.Data.ContainsKey(Settings.DefaultPlanet))
            {
                MessageBox.Show("Please set DefaultPlanetDefinition for PlanetEditor");
                Settings.Data[DefaultPlanet] = GlobalEditor.OpenFileDialog()[0];
            }

            if (!Settings.Data.ContainsKey(Settings.DefaultPlanetImages))
            {
                MessageBox.Show("Please set DefaultPlanet Images folder for PlanetEditor");
                Settings.Data[DefaultPlanetImages] = GlobalEditor.OpenFolderDialog()[0];
            }
            
            Save();
            
        }
    }
}