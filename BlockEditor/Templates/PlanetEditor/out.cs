using System;
using System.Collections;
using System.Collections.Generic;
using BlockEditor.PlanetData;

public class PlanetEditorScript
{
    private const byte NONE = 0;
    private const byte ROAD = 1;
    
    private bool PreprocessFilter_000(PlanetSide ps, OreGeneratorDefinition definition, OreGeneratorArgument argument) {
    //GENERATED CODE
        
argument.Args = definition.PreprocessRules[0].Arguments;
if (!ClearOre(argument)) return false;


    //GENERATED CODE
    return true;
}

private List<int> CanPlaceFilter_000(PlanetSide ps, OreGeneratorLayer l, OreGeneratorArgument argument) {
    List<int> availablePlaces = new List<int>();
            
    for (var i=0; i<ps.mWH * ps.mWH; i++)
    {
        argument.I = i;
        //GENERATED CODE
        
argument.Args = l.CanPlaceRules[0].Arguments;
if (!argument.PlanetSide.GetCanPlace(i)) continue;
if (!SkipXY(argument))
{
    argument.PlanetSide.SetLocalCanPlace(i, false);
    continue;
}
argument.Args = l.CanPlaceRules[1].Arguments;
if (!argument.PlanetSide.GetCanPlace(i)) continue;
if (!IsBiome(argument))
{
    argument.PlanetSide.SetLocalCanPlace(i, false);
    continue;
}
argument.Args = l.CanPlaceRules[2].Arguments;
if (!argument.PlanetSide.GetCanPlace(i)) continue;
if (!IsHigherThan(argument))
{
    argument.PlanetSide.SetLocalCanPlace(i, false);
    continue;
}

        //GENERATED CODE
        
        if (argument.PlanetSide.GetCanPlace(i))
        {
            
            availablePlaces.Add(i);
        }
    }

    //foreach (var ap in availablePlaces)
    //{
    //    var px = argument.PlanetSide.material.GetFoliage(ap);
    //    if (px >=150 && px<=154) continue;
//
    //    argument.I = ap;
    //    argument.Args = l.CanPlaceRules[1].Arguments;
    //    IsBiome(argument);
    //}

    return availablePlaces;
}

private bool PrePlaceFilter_000(PlanetSide ps, OreGeneratorLayer l, OreGeneratorArgument argument) {
    //GENERATED CODE
        

    //GENERATED CODE
    return true;
}

private bool AfterPlaceFilter_000(PlanetSide ps, OreGeneratorLayer l, OreGeneratorArgument argument) {
    //GENERATED CODE
        
argument.Args = l.AfterPlaceRules[0].Arguments;
if (!MarkCantPlaceGlobal(argument)) return false;
argument.Args = l.AfterPlaceRules[1].Arguments;
if (!MarkCantPlaceLocal(argument)) return false;

    //GENERATED CODE
    return true;
}


    
        
        
        
        
        
    public bool MarkMountain(OreGeneratorArgument Arg)
    {
        for (int i = 0; i < Arg.PlanetSide.material.data.Length / 3; i++)
        {
            Arg.I = i;
            Arg.GlobalData.data[Arg.I] |= (byte)(Arg.GetMaxHeightDifference(Arg.X, Arg.Y, Arg.Args[0].Int) > Arg.Args[1].Int ? Arg.Args[2].Int : 0);
        }
        return true;
    }
    
    public bool MarkRoad (OreGeneratorArgument Arg)
    {
        for (int i = 0; i < Arg.PlanetSide.material.data.Length / 3F; i++)
        {
            Arg.I = i;
            var f = Arg.PlanetSide.material.GetFoliage(Arg.X, Arg.Y);
            if (Arg.Args[1].ByteArr.Contains(f))
            {
                var r = Arg.Args[0].Int;
                Arg.GlobalData.For(Arg.X - r, Arg.Y - r, Arg.X + r, Arg.Y + r, (ii, x, y) =>
                {
                    Arg.GlobalData.data[ii] |= ROAD;
                    return true;
                });
            }
        }
        return true;
    }
    
    public bool ClearOre (OreGeneratorArgument Arg)
    {
        for (int i = 0; i < Arg.PlanetSide.material.data.Length / 3; i++)
        {
            Arg.I = i;
            Arg.PlanetSide.material.SetOre(Arg.X, Arg.Y, 0); 
        }
        return true;
    }
    
    public bool ExactPlace(OreGeneratorArgument Arg)
    {
        return Arg.X == Arg.Args[0].Int && Arg.Y == Arg.Args[1].Int;
    }
    
    public bool Margin (OreGeneratorArgument Arg)
    {
        return (Arg.X >= Arg.Args[0].Int && Arg.Y >= Arg.Args[1].Int &&
                Arg.X <= Arg.WH - Arg.Args[2].Int  && Arg.Y <= Arg.WH - Arg.Args[3].Int);
    }
    
    
    public bool MarkCantPlaceGlobal(OreGeneratorArgument Arg)
    {
        var sx = Arg.X - Arg.Args[0].Int;
        var ex = Arg.X + Arg.Args[0].Int;
        var sy = Arg.Y - Arg.Args[1].Int;
        var ey = Arg.Y + Arg.Args[1].Int;
        Arg.PlanetSide.canPlaceGlobal.For(sx, sy, ex, ey, (i, x, y) =>
        {
            Arg.PlanetSide.canPlaceGlobal.Set(i, 0, 0);
            return true;
        });
        return true;
    }
    
    public bool MarkCantPlaceLocal(OreGeneratorArgument Arg)
    {
        var sx = Arg.X - Arg.Args[0].Int;
        var ex = Arg.X + Arg.Args[0].Int;
        var sy = Arg.Y - Arg.Args[1].Int;
        var ey = Arg.Y + Arg.Args[1].Int;
        Arg.PlanetSide.canPlaceLocal.For(sx, sy, ex, ey, (i, x, y) =>
        {
            Arg.PlanetSide.canPlaceLocal.Set(i, 0, 0);
            return true;
        });
        return true;
    }
    
    public bool MarkNear(OreGeneratorArgument Arg)
    {
        var sx = Arg.X - Arg.Args[0].Int;
        var ex = Arg.X + Arg.Args[0].Int;
        var sy = Arg.Y - Arg.Args[1].Int;
        var ey = Arg.Y + Arg.Args[1].Int;
        Arg.GlobalData.For(sx, sy, ex, ey, (i, x, y) =>
        {
            Arg.GlobalData.data[i] |= (byte)Arg.Args[2].Int;
            return true;
        });
        return true;
    }
    
    public bool HasFoliage (OreGeneratorArgument Arg)
    {
        return ((IList) Arg.Args[0].ByteArr).Contains(Arg.PlanetSide.material.GetFoliage(Arg.I*3));
    }
    
    public bool HasVoxel (OreGeneratorArgument Arg)
    {
        return ((IList) Arg.Args[0].ByteArr).Contains(Arg.PlanetSide.material.GetVoxel(Arg.I*3));
    }
    
    public bool HasOre (OreGeneratorArgument Arg)
    {
        return ((IList) Arg.Args[0].ByteArr).Contains(Arg.PlanetSide.material.GetOre(Arg.I*3));
    }

    public bool IsBiome (OreGeneratorArgument Arg)
    {
        var foliage = Arg.Definition.Mapping.GSubDict;
        var pixel = Arg.PlanetSide.material.GetFoliage(Arg.I*3);
        foreach (var variant in Arg.Args[0].ByteArr)
        {
            if (!foliage.ContainsKey(variant)) continue;
            if (foliage[variant].Contains(pixel))
            {
                return true;
            }
        }

        return false;
    }
    
    public bool IsHigherThan (OreGeneratorArgument Arg)
    {
        return Arg.GetHeight(Arg.X, Arg.Y) >= Arg.Args[0].Double;
    }
    
    public bool IsLowerThan (OreGeneratorArgument Arg)
    {
        return Arg.GetHeight(Arg.X, Arg.Y) <= Arg.Args[0].Double;
    }

    public bool SkipXY (OreGeneratorArgument Arg)
    {
        return ((Arg.X % Arg.Args[0].Int) == 0 && (Arg.Y % Arg.Args[1].Int) == 0);
    }
    public bool HasMark(OreGeneratorArgument Arg)
    {
        var flag = (byte)Arg.Args[0].Int;
        var data = Arg.GlobalData.data[Arg.I];
        if (data != 0)
        {
            var wtf = 0;
        }
        return (data | flag) == data;
    }
    
    public bool HasNear(OreGeneratorArgument arg)
    {
        var searchFor = arg.Args[0].ByteArr;
        var type = ((byte)arg.Args[1].Double);
        var radius = ((int)arg.Args[2].Double);
        var percentage = arg.Args[4].Double;
        var more = arg.Args[5].Double == 1;

        return arg.PlanetSide.material.Search (arg.I, searchFor, type, radius, arg.Args[3].String, percentage, more);
    }
}