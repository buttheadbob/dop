﻿argument.Args = l.CanPlaceRules[<INDEX>].Arguments;
if (!argument.PlanetSide.GetCanPlace(i)) continue;
if (!<FX_NAME>(argument))
{
    argument.PlanetSide.SetLocalCanPlace(i, false);
    continue;
}
