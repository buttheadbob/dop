using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using BlockEditor.Editors;
using BlockEditor.Serialization;
using RestSharp.Serializers;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.ObjectBuilders;
using BlockEditor;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using GasTank = Sandbox.Common.ObjectBuilders.Definitions.MyObjectBuilder_GasTankDefinition;
using Battery = Sandbox.Common.ObjectBuilders.Definitions.MyObjectBuilder_BatteryBlockDefinition;

public class Script {

    public static object CallMethod(string className, string method, object instance, object[] data)
    {
        var t = typeof(BlockEditor.SE.FormulaArgument).Assembly.GetType(className);
        var m = t.GetMethod(method, BindingFlags.Static | BindingFlags.Public | BindingFlags.Default | BindingFlags.Instance | BindingFlags.NonPublic);
        return m.Invoke(instance, data);
    }

    public static Func<string, MyObjectBuilder_DefinitionBase> GetBlock;
    public static Func<string, double> GetMass;

    public object Formula (object o, BlockEditor.SE.FormulaArgument arg) {
        var def = o as MyObjectBuilder_DefinitionBase;
        var cube = o as MyObjectBuilder_CubeBlockDefinition;
        var cmp = o as MyObjectBuilder_ComponentDefinition;
        var thrust = o as MyObjectBuilder_ThrustDefinition;
        var engine = o as MyObjectBuilder_HydrogenEngineDefinition;
        var reactor = o as MyObjectBuilder_ReactorDefinition;
        
        GetMass = arg.GetMass;
        GetBlock = arg.GetBlock;
        
        double.TryParse(arg.Arg1, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg1);
        double.TryParse(arg.Arg2, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg2);
        double.TryParse(arg.Arg3, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg3);
        var Arg1 = arg.Arg1;
        var Arg2 = arg.Arg2;
        var Arg3 = arg.Arg3;
        var Mass = arg.Mass;
        var Tier = arg.Tier;
        var SizeMlt = arg.SizeMlt;
        var BlockSizeMlt = cube.CubeSize == MyCubeSize.Large ? SizeMlt : 1f;
        var Volume = cube.Size.X*cube.Size.Y*cube.Size.Z;    
        
        //var mass = cube?.GetMass() ?? 0;
        var G = 9.8;
        return <YOUR CODE HERE>;
    }
    
    public static double GetTankMass(string b, double weight, double perc = 1d)
    {
       return GetMass("GasTankDefinition/"+b) + (GetBlock("GasTankDefinition/"+b) as GasTank).Capacity * perc * weight;
    }
    
    public static double Invert (double d) {
        if (d < 0) {
            d = -1d / d;
        }
        
        return d;
    }
}