﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace BlockEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        void App_Startup(object sender, StartupEventArgs e)
        {
            Window window;
            if (e.Args.Contains("PlanetEditor"))
            {
                window = new PlanetEditor2();
            }
            else if (e.Args.Contains("PicEditor"))
            {
                window = new PicEditor();
            }
            else
            {
                window = new MainWindow();
            }
            window.Show();
        }

        public static Dispatcher Dispatcher;
    }
}