﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BlockEditor.PlanetData;
using BlockEditor.SE;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using VRage.Game;

using OreGenFx = System.Func<BlockEditor.PlanetData.PlanetSide, BlockEditor.PlanetData.OreGeneratorLayer, BlockEditor.PlanetData.OreGeneratorArgument, bool>;
using OrePreprocessGenFx = System.Func<BlockEditor.PlanetData.PlanetSide, BlockEditor.PlanetData.OreGeneratorDefinition, BlockEditor.PlanetData.OreGeneratorArgument, bool>;
using OreGenCanPlace = System.Func<BlockEditor.PlanetData.PlanetSide, BlockEditor.PlanetData.OreGeneratorLayer, BlockEditor.PlanetData.OreGeneratorArgument, System.Collections.Generic.List<int>>;

namespace BlockEditor
{
    public class ScriptManager {
        public static string SCRIPT_EXAMPLE = "";
        public static string SCRIPT_TEMPLATE = "<YOUR CODE HERE>";
        public static string FILTER_SCRIPT_TEMPLATE = "<YOUR CODE HERE>";
        public static string FORMULA_SCRIPT_TEMPLATE = "<YOUR CODE HERE>";

        public static void Init()
        {
            ReadFile("Templates/FilterScriptTemplate.cs", ref FILTER_SCRIPT_TEMPLATE);
            ReadFile("Templates/ScriptTemplate.cs", ref SCRIPT_TEMPLATE);
            ReadFile("Templates/ScriptExample.cs", ref SCRIPT_EXAMPLE);
            ReadFile("Templates/FormulaScriptTemplate.cs", ref FORMULA_SCRIPT_TEMPLATE);
        }

        private static void ReadFile(string path, ref string where)
        {
            if (File.Exists(path))
            {
                where = File.ReadAllText(path);
            }
            else
            {
                throw new Exception($"File {path} not found");
            }
        }

        public static Func<object, FormulaArgument, object> CreateFormula(string otxt)
        {
            var txt = FORMULA_SCRIPT_TEMPLATE.Replace("<YOUR CODE HERE>", otxt);
            
            var assembly = InnerCompile(txt, out var errors);
            
            if (errors != null && errors.Count() >= 0)
            {
                var sb = new StringBuilder();
                foreach (var e in errors)
                {
                    sb.AppendLine(e.ToString());
                }
                Output.Info("Filter error:" + sb);
                return null;
            }

            
            
            if (assembly != null)
            {
                var type = assembly.GetType("Script");
                var obj = Activator.CreateInstance(type);
                var method = type.GetMethod("Formula", BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic);
                return (filter, arg) =>
                {
                    try
                    {
                        return method.Invoke(obj, new object[] {filter, arg});
                    }
                    catch (Exception e)
                    {
                        Output.Error(e, $"FORMULA [{arg}] [{filter}]");
                        return 0;
                    }
                };
            }

            return null;
        }
        
        public static Func<object, bool> CreateFilter(string otxt)
        {
            var txt = FILTER_SCRIPT_TEMPLATE.Replace("<YOUR CODE HERE>", otxt);
            
            var assembly = InnerCompile(txt, out var errors);
            
            if (errors != null && errors.Count() >= 0)
            {
                var sb = new StringBuilder();
                foreach (var e in errors)
                {
                    sb.AppendLine(e.ToString());
                }
                Output.Info("Filter error:" + sb);
                return (filter) => true;
            }
            
            if (assembly != null)
            {
                var type = assembly.GetType("Script");
                var obj = Activator.CreateInstance(type);
                return ExtractFunctions(obj, Wrapper1<object, bool>, (m) => m.Name == "Filter")["Filter"];
            }
            else
            {
                return (filter) => true;
            }
        }

        
        
        public static Assembly Compile(string txt, out IEnumerable<Diagnostic> errors)
        {
            if (!txt.StartsWith("//CUSTOM"))
            {
                txt = SCRIPT_TEMPLATE.Replace("<YOUR CODE HERE>", txt);
            }

            return InnerCompile(txt, out errors);
        }
        
        public static Dictionary<string, T> ExtractFunctions<T>(object instance, Func<MethodInfo,object,T> Creator, Func<MemberInfo, bool> filter = null)
        {
            var type = instance.GetType();
            var methods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic);

            var d = new Dictionary<string, T>();
            foreach (var m in methods)
            {
                if (filter == null || !filter (m)) continue;
                d[m.Name] = Creator(m,instance);
            }

            return d;
        }

        private static MetadataReference[] m_References;
        private static MetadataReference[] References {
            get {
                
                if (m_References == null)
                {
                    var list = new HashSet<string>();
                    var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                    for (int i = 0; i < assemblies.Length; i++)
                    {
                        list.Add(assemblies[i].Location);//, 
                    }
                    
                    
                    
                    var files = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.dll", SearchOption.AllDirectories);
                    foreach (var file in files)
                    {
                        list.Add(file);
                    }

                    var arr = new List<MetadataReference>();
                    Parallel.ForEach(list, (s) =>
                    {
                        if (s.ToLower().Contains("magick")) return;
                        var m = MetadataReference.CreateFromFile(s);
                        lock (arr)
                        {
                            arr.Add(m);
                        }
                    });
                   
                    m_References = arr.ToArray();
                }
                return m_References;
            }
        }
        
        private static Assembly InnerCompile(string txt, out IEnumerable<Diagnostic> errors)
        {
            // define source code, then parse it (to the type used for compilation)
            SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(txt);

            // define other necessary objects for compilation
            string assemblyName = Path.GetRandomFileName();


            

            // analyse and generate IL code from syntax tree
            CSharpCompilation compilation = CSharpCompilation.Create(
                assemblyName,
                syntaxTrees: new[] {syntaxTree},
                references: References,
                options: new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));

            using (var ms = new MemoryStream()) {
            using (var ms2 = new MemoryStream()) {
                // write IL code into memory
                EmitResult result = compilation.Emit(ms, ms2);

                if (!result.Success)
                {
                    // handle exceptions
                    IEnumerable<Diagnostic> failures = result.Diagnostics.Where(diagnostic =>
                        diagnostic.IsWarningAsError ||
                        diagnostic.Severity == DiagnosticSeverity.Error);

                    errors = failures;
                    return null;
                    
                }

                errors = null;
                ms.Seek(0, SeekOrigin.Begin);
                ms2.Seek(0, SeekOrigin.Begin);
                return Assembly.Load(ms.ToArray(), ms2.ToArray());
            }
            }
        }

        public class OreGeneratorFunctions
        {
            public OrePreprocessGenFx PreprocessFunc;
            
            public List<OreGenCanPlace> CanPlaceFuncs = new List<OreGenCanPlace>();
            public List<OreGenFx> PrePlaceFuncs = new List<OreGenFx>();
            public List<OreGenFx> AfterPlaceFuncs = new List<OreGenFx>();
        }
        
        

        public static OreGeneratorFunctions CompileOreGeneratorClass(object instance)
        {
            var outClass = new OreGeneratorFunctions();

            var fx0 = ExtractFunctions(instance, Wrapper4 <PlanetSide, OreGeneratorDefinition, OreGeneratorArgument, bool>, 
                (x) => x.Name.StartsWith("PreprocessFilter"));

            var fx1 = ExtractFunctions(instance, Wrapper4 <PlanetSide, OreGeneratorLayer, OreGeneratorArgument, List<int>>, 
                (x) => x.Name.StartsWith("CanPlaceFilter"));
            var fx2 = ExtractFunctions(instance, Wrapper4 <PlanetSide, OreGeneratorLayer, OreGeneratorArgument, bool>,
                (x) => x.Name.StartsWith("PrePlaceFilter"));
            var fx3 = ExtractFunctions(instance, Wrapper4 <PlanetSide, OreGeneratorLayer, OreGeneratorArgument, bool>,
                (x) => x.Name.StartsWith("AfterPlaceFilter"));
            

            outClass.PreprocessFunc = SortByKey(fx0, String.Compare)[0];
            outClass.CanPlaceFuncs =  SortByKey(fx1, String.Compare);
            outClass.PrePlaceFuncs =  SortByKey(fx2, String.Compare);
            outClass.AfterPlaceFuncs =  SortByKey(fx3, String.Compare);

            return outClass;
        }
        
        public static object CompileOreGeneratorClass(string code)
        {
            var assembly = InnerCompile(code, out var errors);
            if (errors != null)
            {
                var sb = new StringBuilder();
                foreach (var e in errors)
                {
                    sb.AppendLine(""+e);
                }

                MessageBox.Show("Error on compiling: \n" + sb.ToString());
                return null;
            }

            if (assembly == null) return null;

            var type = assembly.GetType("PlanetEditorScript");
            return Activator.CreateInstance(type);
        }

        public static Func<A,B,C,D> Wrapper4<A,B,C,D>(MethodInfo m, object o)
        {
            return (a, b, c) =>
            {
                try
                {
                    return (D) m.Invoke(o, new object[] {a, b, c});
                }
                catch (Exception e)
                {
                    var wtf = 0;
                    throw e;
                }
                
            };
        }
        
        public static Action<A,B,C> Wrapper4<A,B,C>(MethodInfo m, object o)
        {
            return (a, b, c) =>
            {
                try
                {
                    m.Invoke(o, new object[] {a, b, c});
                }
                catch (Exception e)
                {
                    var wtf = 0;
                    throw e;
                }
                
            };
        }
        
        public static Func<A,B> Wrapper1<A,B>(MethodInfo m, object o)
        {
            return (a) =>
            {
                return (B) m.Invoke(o, new object[] {a});
            };
        }
        
        public static Func<object[],A> Wrapper<A>(MethodInfo m, object o)
        {
            return (a) =>
            {
                return (A) m.Invoke(o, a);
            };
        }
        
        private static List<V> SortByKey<K,V>(Dictionary<K, V> dict, Func<K,K, int> sorter)
        {
            var list = new List<KeyValuePair<K, V>>();
            foreach (var kv in dict)
            {
                list.Add(kv);
            }
            
            list.Sort((a,b)=>sorter(a.Key,b.Key));

            var list2 = new List<V>();
            foreach (var item in list)
            {
                list2.Add(item.Value);
            }

            return list2;
        }
        private static string GenerateFunctionGroup(int index, string globalTemplate, string fxTemplate, string fxName, Formula[] fx)
        {
            var txt = new StringBuilder();
            int index2 = 0;
            if (fx != null)
            {
                foreach (var fxCan in fx)
                {
                    var replace = fxTemplate
                        .Replace("<FX_NAME>", fxCan.FxName)
                        .Replace("<INDEX>", $"{index2}");
                    txt.Append(replace);

                    index2++;
                }
            }
            
            
                
            var s1 = globalTemplate
                .Replace("<FxName>", $"{fxName}_{index:000}")
                .Replace("<YOUR CODE HERE>", txt.ToString());

            return s1;
        }
        public static string GenerateOreGeneratorClassCode(OreGeneratorDefinition definition)
        {
            string ClassTemplate = null;
            string PreprocessFilter = null;
            string PreprocessFilter_Formula = null;
            string CanPlaceFilter = null;
            string CanPlaceFilter_Formula = null;
            string PrePlaceFilter = null;
            string PrePlaceFilter_Formula = null;
            string AfterPlaceFilter = null;
            string AfterPlaceFilter_Formula = null;
            
            ReadFile(definition.CodeGenerator.BaseClass, ref ClassTemplate);
            
            ReadFile(definition.CodeGenerator.PreprocessFilter, ref PreprocessFilter);
            ReadFile(definition.CodeGenerator.PreprocessFilter_Formula, ref PreprocessFilter_Formula);
            
            ReadFile(definition.CodeGenerator.CanPlaceFilter, ref CanPlaceFilter);
            ReadFile(definition.CodeGenerator.CanPlaceFilter_Formula, ref CanPlaceFilter_Formula);
            ReadFile(definition.CodeGenerator.PrePlaceFilter, ref PrePlaceFilter);
            ReadFile(definition.CodeGenerator.PrePlaceFilter_Formula, ref PrePlaceFilter_Formula);
            ReadFile(definition.CodeGenerator.AfterPlaceFilter, ref AfterPlaceFilter);
            ReadFile(definition.CodeGenerator.AfterPlaceFilter_Formula, ref AfterPlaceFilter_Formula);
            
            var sb = new StringBuilder();

            var s0= GenerateFunctionGroup(0, PreprocessFilter, PreprocessFilter_Formula, "PreprocessFilter",definition.PreprocessRules);
            sb.AppendLine(s0);
            
            int index = 0;
            foreach (var fx in definition.Layers)
            {
                var s1= GenerateFunctionGroup(index, CanPlaceFilter, CanPlaceFilter_Formula, "CanPlaceFilter", fx.CanPlaceRules);
                var s2= GenerateFunctionGroup(index, PrePlaceFilter, PrePlaceFilter_Formula, "PrePlaceFilter", fx.PrePlaceRules);
                var s3= GenerateFunctionGroup(index, AfterPlaceFilter, AfterPlaceFilter_Formula, "AfterPlaceFilter", fx.AfterPlaceRules);

                sb.AppendLine(s1);
                sb.AppendLine(s2);
                sb.AppendLine(s3);

                index++;
            }
            
            var code = ClassTemplate.Replace("<OTHER_FUNCTIONS>", sb.ToString());
            File.WriteAllText("Templates/out.cs",code);

            return code;
        }

        
    }
}