﻿using System;
using System.Text;
using System.Windows;
using VRage.Game;

namespace BlockEditor
{
    public class Output
    {
        public static void Error(Exception e, string Description = null)
        {
            if (MainWindow.INSTANCE == null)
            {
                MessageBox.Show(e.ToString());
                return;
            }
            GUI.InvokeGui(() =>
            {
                var txt = (Description ?? "") + e + "\n\n";
                var t = MainWindow.INSTANCE.ErrorLogOutput.Text;
                if (t.Length > 100000)
                {
                    t = t.Substring(t.Length - 100000);
                }

                MainWindow.INSTANCE.ErrorLogOutput.Text = t + txt;
            });
        }
        
        public static void Error(string Description)
        {
            if (MainWindow.INSTANCE == null)
            {
                MessageBox.Show(Description);
                return;
            }
            GUI.InvokeGui(() =>
            {
                var txt = Description + "\n\n";
                var t = MainWindow.INSTANCE.ErrorLogOutput.Text;
                if (t.Length > 100000)
                {
                    t = t.Substring(t.Length - 100000);
                }

                MainWindow.INSTANCE.ErrorLogOutput.Text = t + txt;
            });
        }
        
        
        public static void Verbose(Exception e, string Description = null)
        {
            GUI.InvokeGui(() =>
            {
                var txt = (Description ?? "") + e + "\n\n";
                var t = MainWindow.INSTANCE.VerboseLogOutput.Text;
                if (t.Length > 100000)
                {
                    t = t.Substring(t.Length - 100000);
                }

                MainWindow.INSTANCE.VerboseLogOutput.Text = t + txt;
            });
        }
        
        public static void Verbose (string Description)
        {
            GUI.InvokeGui(() =>
            {
                var txt = Description + "\n\n";
                var t = MainWindow.INSTANCE.VerboseLogOutput.Text;
                if (t.Length > 100000)
                {
                    t = t.Substring(t.Length - 100000);
                }

                MainWindow.INSTANCE.VerboseLogOutput.Text = t + txt;
            });
        }
        
        public static T GetQDefinition<T>(string id) where T : MyObjectBuilder_DefinitionBase
        {
            var sid = id.ToId();
            T t = default(T);
            GlobalEditor.ForAll<T>((x) =>
            {
                if (x.Id.SubtypeName == sid.SubtypeName)
                {
                    if (x.TypeId == sid.TypeId && x is T tt)
                    {
                        t = tt;
                    }
                }
            });
        
            return t;
        }

        
        public static void Info(string Description)
        {
            var txt = (Description ?? "") + "\n\n";
            GUI.InvokeGui(() =>
            {
                var t = MainWindow.INSTANCE.LogOutput.Text;
                if (t.Length > 100000)
                {
                    t = t.Substring(t.Length - 100000);
                }

                MainWindow.INSTANCE.LogOutput.Text = t + txt;
            });
        }
    }
}