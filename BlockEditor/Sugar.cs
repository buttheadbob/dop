﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Globalization;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Documents;
using BlockEditor.GuiData;
using Slime.Geom;
using VRage;
using VRage.ObjectBuilders;

namespace BlockEditor
{
    public static class Sugar
    {
        public static string NextString (this Random r, int count = 10)
        {
            var alphabet = "qwertyuiopasdfghjklzxcvbnm";
            var s = "";
            for (int x = 0; x < count; x++)
            {
                s += alphabet[r.Next(alphabet.Length)];
            }

            return s;
        }
        
        public static Rectangle GetRectangle(this System.Windows.Point r1, System.Windows.Point r2)
        {
            return new Rectangle((int)Math.Min(r1.X, r2.X), (int)Math.Min(r1.Y, r2.Y), (int)Math.Abs(r1.X- r2.X), (int)Math.Abs(r1.Y - r2.Y));
        }
        
        public static Rectangle GetRectangle(this Point r1, Point r2)
        {
            return new Rectangle((int)Math.Min(r1.X, r2.X), (int)Math.Min(r1.Y, r2.Y), (int)Math.Abs(r1.X- r2.X), (int)Math.Abs(r1.Y - r2.Y));
        }
        
        public static string SubstringLast(this string s, string a)
        {
            var i = s.LastIndexOf(a);
            if (i == -1) return s;
            return s.Substring(0, i);
        }

        public static double[] AsNumbers(this string txt)
        {
            var strings = txt.Split(",");
            var doubles = new double[strings.Length];
            int i = 0;
            foreach (var s in strings)
            {
                if (double.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out var d))
                {
                    doubles[i] = d;
                    i++;
                }
            }

            return doubles;
        }
        
        public static void Shuffle<T>(this IList<T> list, Random random)  
        {  
            int n = list.Count;  
            while (n > 1) {  
                n--;  
                int k = random.Next(n + 1);  
                T value = list[k];  
                list[k] = list[n];  
                list[n] = value;  
            }  
        }

        public static int Sort(MemberInfo a, MemberInfo b)
        {
            if (a.Name == "Id") return -1;
            if (b.Name == "Id") return 1;
            
            var at = a.DeclaringType;
            var bt = b.DeclaringType;

            var aa = 0;
            var bb = 0;
                
                
            while (at.BaseType != null)
            {
                at = at.BaseType;
                aa++;
            }
            while (bt.BaseType != null)
            {
                bt = bt.BaseType;
                bb++;
            }

            return aa - bb;
        } 
        
        public static SerializableDefinitionId ToId(this string s)
        {
            s.GetTypeSubtype(out var type, out var subtype);

            try
            {
                return new SerializableDefinitionId(MyObjectBuilderType.Parse($"MyObjectBuilder_{type}"), subtype);//MyObjectBuilder_BlueprintDefinition
            }
            catch (Exception e)
            {
                Output.Error(e, s);
                throw e;
            }
        }
        
        public static string[] Split(this string s, string by, StringSplitOptions options = StringSplitOptions.None)
        {
            return s.Split(new string[]{by}, StringSplitOptions.None);
        }
        
        public static string[] Split(this string s, string by1, string by2, StringSplitOptions options = StringSplitOptions.None)
        {
            return s.Split(new string[]{by1,by2}, StringSplitOptions.None);
        }
        
        public static int Index<T>(this T[] arr, T val)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].Equals(val)) return i;
            }

            return -1;
        }
        
        public static void GetTypeSubtype(this string s, out string type, out string subtype)
        {
            var i = s.Split(new string[]{"/"}, StringSplitOptions.None);
            type = i[0];
            subtype = i[1];
        }
        
        public static Type GetMemberType(this MemberInfo member)
        {
            Type tt = null;
            if (member is FieldInfo fi) tt = fi.FieldType;
            if (member is PropertyInfo pi) tt = pi.PropertyType;
            return tt;
        }

        public static T Next<T> (this Random random, T[] array)
        {
            if (array.Length == 0) return default (T);
            return array[random.Next()%array.Length];
        }
        
        public static T Next<T> (this Random random, IList<T> array)
        {
            if (array.Count == 0) return default (T);
            return array[random.Next()%array.Count];
        }
        
        public static void Sum<T>(this Dictionary<T, int> dict, Dictionary<T, int> other)
        {
            foreach (var x in other)
            {
                dict.Sum(x.Key, x.Value);
            }
        }
        
        public static void Sum<T>(this Dictionary<T, int> dict, T key, int value)
        {
            if (!dict.ContainsKey(key)) { dict[key] = value; } else { dict[key] = dict[key] + value; }
        }
        
        public static T[] Add<T>(this T[] arr, T copy)
        {
            var newOne = new T[arr.Length+1];
            arr.CopyTo(newOne, 0);
            newOne[newOne.Length - 1] = copy;
            return newOne;
        }

        public static void EasySetValue(this MemberInfo methodInfo, object obj, object value)
        {
            try
            {
                
                if (value == null)
                {
                    methodInfo.SetValue(obj, null);
                    return;
                }
                
                var targetType = methodInfo.GetMemberType();
                var valueType = value.GetType();

                if (targetType == valueType)
                {
                    methodInfo.SetValue(obj, value);
                    return;
                }

                double d;
                if (value is int i)
                {
                    d = i;
                }
                else
                {
                    d = (double) value;
                }
                
                 

                if (targetType == typeof(double) || targetType == typeof(double?))
                {
                    value = d;
                } else if (targetType == typeof(float) || targetType == typeof(float?))
                {
                    value = (float)d;
                }else if (targetType == typeof(long) || targetType == typeof(long?))
                {
                    value = (long)d;
                }else if (targetType == typeof(int) || targetType == typeof(int?))
                {
                    value = (int)d;
                }else if (targetType == typeof(ulong) || targetType == typeof(ulong?))
                {
                    value = (ulong) d;
                }else if (targetType == typeof(uint) || targetType == typeof(uint?))
                {
                    value = (uint)d;
                }else if (targetType == typeof(short) || targetType == typeof(short?))
                {
                    value = (short)d;
                }else if (targetType == typeof(ushort) || targetType == typeof(ushort?))
                {
                    value = (ushort)d;
                }else if (targetType == typeof(MyFixedPoint) || targetType == typeof(MyFixedPoint?))
                {
                    value = (MyFixedPoint)d;
                }else if (targetType == typeof(byte) || targetType == typeof(byte?))
                {
                    value = (byte)d;
                }else if (targetType == typeof(bool) || targetType == typeof(bool?))
                {
                    value = d == 1;
                }
                
                methodInfo.SetValue(obj, value);
            }
            catch (Exception e)
            {
                Output.Error(e, "Error on setting: " + methodInfo);
            }
        }

        public static T GetOrNew<K, T>(this Dictionary<K, T> dict, K key) where T : new()
        {
            if (!dict.ContainsKey(key))
            {
                dict[key] = new T();
            }

            return dict[key];
        }


        public static List<object> Flatten(IList items)
        {
            var Copy = new List<object>();
            foreach (var tt in items)
            {
                object tag = tt;
                if (tt is TextBlock tb)
                {
                    tag = tb.Tag;
                }
                
                
                if (tag is DefinitionFile df) {
                    foreach (var o in df.AllItems)
                    {
                        Copy.Add(o);
                    }
                } else if (tag is Mod mod) {
                    foreach (var dff in mod.Definitions)
                    {
                        foreach (var o in dff.AllItems)
                        {
                            Copy.Add(o);
                        }
                    }
                } else {
                    Copy.Add(tag);
                }
            }

            return Copy;
        }

        public static P ToP(this Point p)
        {
            return new P(p.X, p.Y);
        }
        
        public static P ToP(this System.Windows.Point p)
        {
            return new P((int)p.X, (int)p.Y);
        }

        public static P GetClosest(this List<P> points, P point)
        {
            var closestDistance = double.MaxValue;
            P closest = null;
            foreach (var p in points)
            {
                var distance = point.Distance(p);
                if (distance < closestDistance)
                {
                    closest = p;
                    closestDistance = distance;
                }
            }

            return closest;
        }

        public static double GetAverage(this IList<double> collection)
        {
            var sum = 0d;
            foreach (var d in collection) sum += d;
            return sum / collection.Count;
        }
        
        public static int GetAverage(this IList<int> collection)
        {
            var sum = 0;
            foreach (var d in collection) sum += d;
            return sum / collection.Count;
        }
        
        public static ushort GetAverage(this IList<ushort> collection)
        {
            ushort sum = 0;
            foreach (var d in collection) sum += d;
            return (ushort)(sum / collection.Count);
        }
        
        public static bool IsBetween(this double angle, double minA, double maxA)
        {
            if ((minA <= angle && angle <= maxA) 
                || (minA-2*Math.PI <= angle && angle <= maxA-2*Math.PI)
                || (minA+2*Math.PI <= angle && angle <= maxA+2*Math.PI))
            {
                return true;
            }
            return false;
        }
    }
}