﻿using System;
using System.Drawing;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using PlanetGenerator2.MapData;

namespace Clicker
{
    public partial class LayerView : UserControl
    {
        internal static class ResourceAccessor
        {
            public static Uri Get(string resourcePath)
            {
                var uri = string.Format(
                    "pack://application:,,,/{0};component/{1}"
                    , Assembly.GetExecutingAssembly().GetName().Name
                    , resourcePath
                );

                return new Uri(uri);
            }
        }
        
        public LayerView()
        {
            InitializeComponent();
        }

        private Layer Layer;
        
        public void Apply(Layer layer)
        {
            Layer = layer;
            Layer.VisibiltyChanged += LayerOnVisibilityChanged;
            Name.Text = layer.Name;
            //Thumbnail.Source = layer.Name;
            Hide.Source = new BitmapImage(ResourceAccessor.Get(Layer.Visible ? "Icons/visibility.png" : "Icons/visibility_off.png")); //Visible.ToImageSource();
            Name.Text = layer.Name;
        }

        private void LayerOnVisibilityChanged(bool obj)
        {
            Hide.Source = new BitmapImage(ResourceAccessor.Get(obj ? "Icons/visibility.png" : "Icons/visibility_off.png"));
        }

        private void HideOnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Layer.Visible = !Layer.Visible;
        }
        
        private void RefreshOnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Layer.Refresh();
        }

        private void OnMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            Layer.GenerateMenu(sender);
        }

        private void Opacity_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Layer == null) return;
            if (Layer.Control == null) return;
            Layer.Control.Opacity = e.NewValue;
        }
    }
}