﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Color = VRageMath.Color;

namespace Clicker
{
    public class Layer
    {
        public string Name { get; private set; }
        public FrameworkElement Control { get; private set; }
        public Grid GroupControl { get; private set; }
        public Layer ParentLayer  { get; private set; }


        public event Action<bool> VisibiltyChanged;
        
        private bool m_visible = true;
        public bool Visible
        {
            get
            {
                return m_visible;
            }
            set
            {
                if (m_visible == value) return;
                m_visible = value;
                
                
                if (GroupControl != null)
                {
                    OnVisibilityChanged();
                }
                
                VisibiltyChanged?.Invoke(value);
            }
        }

        public List<Layer> Layers = new List<Layer>();

        public void Init(string name, FrameworkElement control, Grid groupControl = null)
        {
            this.Name = name;
            this.Control = control;
            

            if (groupControl == null)
            {
                GroupControl = new Grid();
                GroupControl.Width = 2048;//1920;
                GroupControl.Height = 2048;//
                GroupControl.HorizontalAlignment = HorizontalAlignment.Left;
                GroupControl.VerticalAlignment = VerticalAlignment.Top;
            }
            else
            {
                this.GroupControl = groupControl;
            }

            GroupControl.Visibility = Visible ? Visibility.Visible : Visibility.Hidden;
            

            if (control != null)
            {
                Control.Tag = this;
                GroupControl.Children.Add(control);
            }
        }

        private void OnAddedToLayer(Layer parent)
        {
            ParentLayer = parent;
        }

        public double Zoom = 1;
        public virtual void OnZoomChanged(double from, double to)
        {
            Zoom = to;
        }

        public void AddLayer(Layer layer)
        {
            Layers.Add(layer);
            layer.OnAddedToLayer(this);
            GroupControl.Children.Add(layer.GroupControl);
        }

        public virtual void OnVisibilityChanged()
        {
            GroupControl.Visibility = Visible ? Visibility.Visible : Visibility.Hidden;
        }

        public void GenerateMenu(object sender)
        {
            var menu = new ContextMenu();
            menu.PlacementTarget = (UIElement)sender;
            menu.IsOpen = true;
            
            GenerateMenu(menu);
        }

        public virtual void GenerateMenu(ContextMenu menu) { }

        public virtual void Refresh() { }

        public void Detach()
        {
            var parent = GroupControl.Parent as Grid;
            parent.Children.Remove(GroupControl);

            ParentLayer.Layers.Remove(this);
            this.ParentLayer = null;
        }
    }
}