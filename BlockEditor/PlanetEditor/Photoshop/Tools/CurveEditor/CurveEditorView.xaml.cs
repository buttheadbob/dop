﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Clicker;
using Slime.Geom;

namespace BlockEditor.PlanetEditor.Photoshop.Tools.CurveEditor
{
    public partial class CurveEditorView : UserControl
    {
        public CurveEditorView()
        {
            InitializeComponent();
        }

        public Action<CurveEditorTool.Mode> OnModeChanged;
        public Action<CurveEditorTool.Style> OnStyleChanged;
        public Action<List<P>> OnPointsChanged;
        public Action<double> PenWidthChanged;
        
        private bool skipPointsEvent = false;

        public void SetPoints(List<P> points)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var point in points)
            {
                sb.Append(point);
                sb.AppendLine();
            }

            skipPointsEvent = true;
            Points.Text = sb.ToString();
            skipPointsEvent = false;
        }

        private List<P> ParsePoints()
        {
            var lines = Points.Text.Split("\r\n","\n");
            var l = new List<P>();
            foreach (var line in lines)
            {
                var pp = P.From(line);
                if (pp != null) l.Add(pp);
            }

            return l;
        }

        
        private void TextBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            if (skipPointsEvent) return;
            var points = ParsePoints();
            OnPointsChanged?.Invoke(points);
        }

        private void RadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            var name = (sender as RadioButton).Content as string;
            if (Enum.TryParse<CurveEditorTool.Mode>(name, out var mode))
            {
                OnModeChanged?.Invoke(mode);
            }
            
            if (Enum.TryParse<CurveEditorTool.Style>(name, out var style))
            {
                OnStyleChanged?.Invoke(style);
            }
        }

        private void RangeBase_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            PenWidthChanged?.Invoke(e.NewValue);
        }
    }
}