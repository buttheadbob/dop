﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using BlockEditor;
using BlockEditor.Layers;
using BlockEditor.PlanetData;
using BlockEditor.PlanetEditor.Photoshop.Tools.CurveEditor;
using Clicker;
using GemCraft2;
using Slime.Geom;
using Point = System.Windows.Point;
using UserControl = System.Windows.Controls.UserControl;

namespace BlockEditor.PlanetEditor.Photoshop.Tools.CurveEditor
{
    public class CurveEditorLogic
    {
        private List<P> InnerPixels;
        private Dictionary<P,int> InnerPixelsIndexes = new Dictionary<P, int>();
        private HashSet<P> OuterPixels;
        public List<P> KeyPoints = new List<P>();
        public Dictionary<P, double> Border = new Dictionary<P, double>();

        private CurveEditorTool Tool;
        public Pic Surface
        {
            get => Tool.Surface;
        }
        public Bitmap Bitmap
        {
            get => Tool.Bitmap;
        }

        public int PenWidth = 14;
        public int SmoothWidth = 11;

        public CurveEditorLogic(CurveEditorTool tool)
        {
            this.Tool = tool;
        }
        
        public void Draw()
        {
            if (KeyPoints.Count >= 2)
            {
                DrawLine();
                ExtractInfo();
                PostProcess();
            }
        }
        
        private void DrawLine()
        {
            var brush1 = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            var pen1 = new Pen(brush1);
            pen1.Width = 1;

            using (var graphics = Graphics.FromImage(Bitmap))
            {
                graphics.Clear(Color.Black);

                var pp = new List<System.Drawing.Point>();
                foreach (var p in KeyPoints)
                {
                    pp.Add(new System.Drawing.Point(p.x, p.y));
                }
            
                graphics.DrawCurve(pen1, pp.ToArray());
            }
        }
        
        private void ExtractInfo()
        {
            Surface.CopyFrom(Bitmap);
            
            GlyphFinder gf = new GlyphFinder();
            gf.Init(new PicSimilarColor(Surface, 1), new EightSides());
            gf.StartWave(KeyPoints[0]);


            var last = KeyPoints.Last();
            var pixels = gf.FindWayBack(last.x, last.y);

            InnerPixelsIndexes.Clear();
            int i = 0;
            foreach (var p in pixels)
            {
                InnerPixelsIndexes[p] = (pixels.Count- 1 - i);
                i++;
            }
            
            

            InnerPixels = pixels;
            OuterPixels = GenerateOuterPixels(InnerPixels, PenWidth).ToHashSet();
            var BorderPixels = GenerateOuterPixels(InnerPixels, PenWidth+SmoothWidth).ToHashSet();
            
            //GlyphFinder gf2 = new GlyphFinder();
            //gf2.Init(new PicChecker(Surface, (a,b)=>OuterPixels.Contains(b)), new FourSides());
            //gf2.StartWave(InnerPixels[0]);
            //
            //Border.Clear();
            //for (int j = 1; j <= SmoothWidth; j++)
            //{
            //    var smooth = gf2.GetBorder(j,j);
            //    foreach (var p in smooth)
            //    {
            //        if (!Border.ContainsKey(p))
            //        {
            //            Border.Add(p.Copy(), SmoothWidth-j+1);
            //        }
            //    }
            //}
            
        }


        private void PostProcess()
        {
            Tool.Surface.FillAll(0,0,0);
            
            foreach (var p in OuterPixels)
            {
                Surface.SetColor(p, 255, 0, 0);
            }
            
            foreach (var p in InnerPixels)
            {
                Surface.SetColor(p, 0, 255, 0);
            }
            
            foreach (var p in Border)
            {
                Surface.SetSomeColor(p.Key.x, p.Key.y, -1, 255, (int)(p.Value*50));
            }

            var w = 2;
            foreach (var p in KeyPoints)
            {
                for (var x=p.x-w; x<=p.x+w; x++)
                {
                    if (x < 0) continue;
                    for (var y=p.y-w; y<=p.y+w; y++)
                    {
                        if (y < 0) continue;
                        Surface.SetColor(x,y, 100, 100, 255);
                    }
                }
            }
            
        }
        
        public void Apply(Action<int, int, double, double> apply, Action<int, int, double> applySmoothing)
        {
            var d = GetPriorities(InnerPixels, OuterPixels, PenWidth);

            foreach (var kv in d)
            {
                var sum = 0d;
                foreach (var v in kv.Value)
                {
                    var i = InnerPixelsIndexes[v.Key];
                    sum += i;// * v.Value;
                }

                sum /= kv.Value.Count;

                
                var smoothing = Border.GetValueOrDefault(kv.Key, 0);
                apply(kv.Key.x, kv.Key.y, sum / InnerPixels.Count, smoothing);
            }
            
            foreach (var kv in Border)
            {
                applySmoothing(kv.Key.x, kv.Key.y, kv.Value);
            }
        }
        
        
        

        private List<P> GenerateOuterPixels(List<P> pixels, int w)
        {
            HashSet<P> set = new HashSet<P>();
            foreach (var pixel in pixels)
            {
                pixel.InCircle(w,(x,y) =>
                {
                    set.Add(new P(x, y));
                }, (x, y) => x >= 0 && y >= 0);
            }

            return set.ToList();
        }
        
        

        public static Dictionary<P, List<KeyValuePair<P, double>>> GetPriorities(List<P> pixels, ICollection<P> pixels2, double r)
        {
            Dictionary<P, List<KeyValuePair<P, double>>> dict = new Dictionary<P, List<KeyValuePair<P, double>>>();
            foreach (var p in pixels2)
            {
                for (var x=0; x<pixels.Count; x++)
                {
                    var d = p.Distance(pixels[x]);
                    if (d <= r)
                    {
                        dict.GetOrNew(p).Add(new KeyValuePair<P, double>(pixels[x], d));
                    }
                }
            }

            return dict;
        }

        public P GetStart()
        {
            return InnerPixels[InnerPixels.Count - 1];
        }
        
        public P GetEnd()
        {
            return InnerPixels[0];
        }
    }
    
}