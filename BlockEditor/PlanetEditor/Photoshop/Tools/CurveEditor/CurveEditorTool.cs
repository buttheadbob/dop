﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using BlockEditor;
using BlockEditor.Layers;
using BlockEditor.PlanetData;
using BlockEditor.PlanetEditor.Photoshop.Tools.CurveEditor;
using Clicker;
using GemCraft2;
using Slime.Geom;
using Point = System.Windows.Point;
using UserControl = System.Windows.Controls.UserControl;

namespace BlockEditor.PlanetEditor.Photoshop.Tools.CurveEditor
{
    public class MagicWandTool : IPhotoshopTool
    {
        private List<P> Selections = new List<P>();
        private Clicker.Photoshop Photoshop;
        private ImageLayer ImageLayer;
        private Pic Surface;
        
        public MagicWandTool()
        {
            
        }
        
        public void Attach(Clicker.Photoshop photoshop)
        {
            Photoshop = photoshop;
            Photoshop.OnLayerSelectionChanged += PhotoshopOnOnLayerSelectionChanged;
            ImageLayer = new ImageLayer("LineTool", ()=>Surface.ToBitmap(), autoLoad:false);
            Photoshop.AddLayer(ImageLayer);
        }

        private void PhotoshopOnOnLayerSelectionChanged(List<Layer> obj)
        {
            
        }

        public void Detach(Clicker.Photoshop photoshop)
        {
            Photoshop = null;
        }

        public void AddViews(Grid parent)
        {
            
        }

        public void RemoveViews(Grid parent)
        {
            
        }
    }
    
    public class CurveEditorTool : IPhotoshopTool
    {
        private Clicker.Photoshop photoshop;
        public Pic Surface;
        public Bitmap Bitmap;
        private ImageLayer ImageLayer;
        private ImageLayer AttachToLayer;
        public CurveEditorLogic Logic;
        private int W, H;
        
        

        private CurveEditorView EditorView;

        public CurveEditorTool(Clicker.Photoshop photoshop, ImageLayer l)
        {
            this.photoshop = photoshop;
            Logic = new CurveEditorLogic(this);
            
            photoshop.OnMouseUp += PhotoshopOnOnMouseUp;
            photoshop.OnMouseDown += PhotoshopOnOnMouseDown;
            photoshop.OnTickPoint += PhotoshopOnTick;

            this.AttachToLayer = l;
        }

        


        private Mode CurrentMode = Mode.Add;
        private Style CurrentStyle = Style.Curve;
        
        public enum Mode
        {
            Add,
            Remove,
            Edit
        }
        
        public enum Style
        {
            Line,
            Curve
        }
        

        private void OnPointsChanged(List<P> obj)
        {
            Logic.KeyPoints.Clear();
            Logic.KeyPoints.AddRange(obj);
            Logic.Draw();
            ImageLayer.Refresh();
        }

        private void OnStyleChanged(Style obj)
        {
            CurrentStyle = obj;
            Logic.Draw();
            ImageLayer.Refresh();
        }

        private void OnModeChanged(Mode obj)
        {
            CurrentMode = obj;
        }

        private void PointsChanged()
        {
            Logic.Draw();
            ImageLayer.Refresh();
            EditorView.SetPoints(Logic.KeyPoints);
        }

        private P SelectedPoint;
        
        private void PhotoshopOnOnMouseDown(Point arg1, Point scaled, MouseButtonEventArgs arg3)
        {
            if (CurrentMode == Mode.Add)
            {
                Logic.KeyPoints.Add(scaled.ToP());
                PointsChanged();
            }
            if (CurrentMode == Mode.Remove) {
                
                Logic.KeyPoints.Add(scaled.ToP());
                PointsChanged();
            }
            
            if (CurrentMode == Mode.Edit)
            {
                SelectedPoint = Logic.KeyPoints.GetClosest(scaled.ToP());
                SelectedPoint.x = (int)scaled.X;
                SelectedPoint.y = (int)scaled.Y;
                PointsChanged();
            }
            
            //if (arg3.ChangedButton == MouseButton.Middle)
            //{
            //    CurrentMode = Mode.Editing;
            //}
        }
        
        private void PhotoshopOnTick(Point arg1, Point scaled)
        {
            if (CurrentMode == Mode.Edit)
            {
                if (SelectedPoint != null)
                {
                    SelectedPoint.x = (int)scaled.X;
                    SelectedPoint.y = (int)scaled.Y;
                    PointsChanged();
                }
            }
        }

        private void PhotoshopOnOnMouseUp(Point arg1, Point scaled, MouseButtonEventArgs arg3)
        {
            SelectedPoint = null;
        }

        private void OnBitmapLoaded(Bitmap obj)
        {
            if (Surface != null) return;
            if (obj == null) return;
            Bitmap = new Bitmap(obj.Width, obj.Height);
            Surface = new Pic(Bitmap);
            Surface.FillAll(0,255,0);
            ImageLayer.Refresh();
        }

        public void Attach(Clicker.Photoshop photoshop)
        {
            ImageLayer = new ImageLayer("LineTool", ()=>Surface.ToBitmap(), autoLoad:false);
            AttachToLayer.OnBitmapLoaded += OnBitmapLoaded;
            OnBitmapLoaded(AttachToLayer.Bitmap);
            photoshop.AddLayer(ImageLayer);
        }
        
        public void AddViews(Grid Parent)
        {
            var cev = new CurveEditorView();
            Parent.Children.Add(cev);
            
            cev.OnModeChanged += OnModeChanged;
            cev.OnStyleChanged += OnStyleChanged;
            cev.OnPointsChanged += OnPointsChanged;
            cev.PenWidthChanged += PenWidthChanged;

            EditorView = cev;
        }

        private void PenWidthChanged(double obj)
        {
            Logic.PenWidth = (int)obj;
        }

        public void RemoveViews(Grid parent)
        {
            photoshop.RemoveLayer(ImageLayer);
            ImageLayer = null;
        }

        public void Detach(Clicker.Photoshop photoshop)
        {
            
        }
    }
}