﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using BlockEditor;
using BlockEditor.Layers;
using BlockEditor.PlanetData;
using BlockEditor.PlanetEditor.Photoshop.Tools.CurveEditor;
using GemCraft2;
using Slime.Geom;
using Point = System.Windows.Point;
using UserControl = System.Windows.Controls.UserControl;

namespace Clicker
{
    public interface IPhotoshopTool
    {
        void Attach(Photoshop photoshop);
        void Detach(Photoshop photoshop);
        void AddViews(Grid parent);
        void RemoveViews(Grid parent);
    }
    
    public partial class Photoshop : UserControl
    {
        private GlyphFinder Finder;
        private Rectangle Rectangle;
        private Layer TopLayer = new Layer();

        public event Action<Rectangle?> OnSelectAreaUpdate;
        public event Action<Rectangle?> OnTick;
        public event Action<Point, Point> OnTickPoint;
        public event Action<StringBuilder, Point, Point> GenerateInfo;
        public event Action<Point, Point, MouseButtonEventArgs> OnMouseUp;
        public event Action<Point, Point, MouseButtonEventArgs> OnMouseDown;
        public event Action<double, double> OnZoomChanged;
        public event Action<List<Layer>> OnLayerSelectionChanged;

        private Point mouseDown = new Point(100,100);
        private bool IsMouseDown = false;
        
        private int m_prevZoom = 1;

        private IPhotoshopTool SelectedTool = null;
        private List<IPhotoshopTool> Tools = new List<IPhotoshopTool>();

        public void AddTool(IPhotoshopTool tool)
        {
            Tools.Add(tool);
            tool.Attach(this);
        }

        public void SelectTool(IPhotoshopTool tool)
        {
            if (SelectedTool != null)
            {
                SelectedTool.RemoveViews(ToolsContainer);
            }
            ToolsContainer.Children.Clear();
            
            SelectedTool = tool;
            if (tool == null) return;
            SelectedTool.AddViews(ToolsContainer);
        }
        
        
        public List<Layer> GetLayers(Layer layer = null, List<Layer> layers = null)
        {
            if (layers == null) layers = new List<Layer>();
            if (layer == null) layer = TopLayer;
            
            
            layers.Add(layer);
            foreach (var v in TopLayer.Layers)
            {
                GetLayers(v, layers);
            }

            return layers;
        }

        private double m_zoom = 1f; 
        public double Zoom
        {
            get => m_zoom;
            set
            {
                if (value <= 0) return;
                var newV = Math.Round(value * 10d) / 10d;
                var dz = Math.Abs(m_zoom - newV);
                if (newV < 1)
                {
                    if (dz < 0.1) return;
                }
                else if (newV < 3)
                {
                    if (dz < 0.5) return;
                }
                else
                {
                    if (dz < 1) return;
                }

                var old = m_zoom;
                m_zoom = newV;
                ZoomSlider.Value = m_zoom;
                OnZoomChanged?.Invoke(old, newV);
            }
        }
        
        public Photoshop()
        {
            InitializeComponent();
            var timer = new DispatcherTimer();
            timer.Tick += TimerOnTick;
            timer.Interval = TimeSpan.FromMilliseconds(16);
            timer.Start();
            
            TopLayer.Init("TopLayer", null, ObjectsContainer);
            OnZoomChanged += OnOnZoomChanged;
        }

        private void OnOnZoomChanged(double oldValue, double newValue)
        {
            ZoomChanged(TopLayer, oldValue, newValue);
        }

        private void TimerOnTick(object sender, EventArgs e)
        {
            Point rr2;
            try
            {
                var pos = System.Windows.Forms.Cursor.Position;
                rr2 = ObjectsContainer.PointFromScreen(new Point(pos.X, pos.Y));
            }
            catch (Exception ee)
            {
                return;
            }
            
            if (IsMouseDown)
            {
                var rr1 = mouseDown;
                var rr = rr1.GetRectangle(rr2);
                OnTick?.Invoke(rr);
            }
            else
            {
                var pp3 = new Point(rr2.X/Zoom, rr2.Y/Zoom);

                var sb = new StringBuilder();
                sb.Append(rr2).Append(" ").Append(pp3);
                GenerateInfo?.Invoke(sb, rr2, pp3);
                OnTickPoint?.Invoke(rr2, pp3);

                Test.Content = sb.ToString();
            }
        }

        public Point Position(Point e)
        {
            //TODO!
            //var xd = ObjectsContainer.Source.Width / ObjectsContainer.ActualWidth;
            //var yd = ObjectsContainer.Source.Height / ObjectsContainer.ActualHeight;

            var xd = 2048;
            var yd = 2048;
            var mouseUp = e;
            var d = Math.Min(xd, yd);
            var x1  = (int)(mouseUp.X * d);
            var y1  = (int)(mouseUp.Y * d);
            
            return new Point(x1, y1);
        }

        private void ZoomChanged(Layer layer, double oldValue, double newValue)
        {
            layer.OnZoomChanged(oldValue, newValue);
            foreach (var sublayer in layer.Layers)
            {
                ZoomChanged(sublayer, oldValue, newValue);
            }
        }
        
        private void Zoom_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Zoom = ZoomSlider.Value;
        }

        public void AddLayer(Layer layer, Layer parent = null)
        {
            parent = parent ?? TopLayer;
            parent.AddLayer(layer);
            PopulateTreeView();
        }
        
        public void RemoveLayer(Layer layer)
        {
            if (layer == TopLayer) throw new Exception("Trying to remove TopLayer");
            layer.Detach();
        }

        public void PopulateTreeView()
        {
            ObjectsTree.Visibility = Visibility.Collapsed;
            ObjectsTree.Items.Clear();
            AddLayerToTree(TopLayer, ObjectsTree.Items);
            ObjectsTree.Visibility = Visibility.Visible;
        }

        
        private void AddLayerToTree(Layer layers, ItemCollection collection)
        {
            foreach (var layer in layers.Layers)
            {
                var view = new LayerView();
                view.Apply(layer);
                
                var treeNode = new TreeViewItem();
                treeNode.Tag = layer;
                treeNode.Header = view;
                treeNode.IsExpanded = true;
                collection.Add(treeNode);
                
                AddLayerToTree(layer, treeNode.Items);
            }
        }
        private void ObjectsTree_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            
        }

        

        public Layer GetLayer(string name, Layer parent = null)
        {
            if (parent == null) parent = TopLayer;
            if (parent.Name == name)
            {
                return parent;
            }
            
            foreach (var l in parent.Layers)
            {
                var r = GetLayer(name, l);
                if (r != null) return r;
            }

            return null;
        }

        private void ObjectsContainer_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            var rr2 = GetMousePosition();
            var pp3 = new Point(rr2.X/Zoom, rr2.Y/Zoom);
            OnMouseDown?.Invoke(rr2, pp3, e);
        }
        
        private void ObjectsContainer_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            var rr2 = GetMousePosition();
            var pp3 = new Point(rr2.X/Zoom, rr2.Y/Zoom);
            OnMouseUp?.Invoke(rr2, pp3, e);
        }

        private Point GetMousePosition()
        {
            var pos = System.Windows.Forms.Cursor.Position;
            return ObjectsContainer.PointFromScreen(new Point(pos.X, pos.Y));
        }
    }
}