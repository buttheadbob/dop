﻿using System.Drawing;
using System.Xml.Serialization;

namespace Clicker.POE
{
    
    public class BitmapArea
    {
        [XmlIgnore]
        public Bitmap Cropped;
        
        [XmlIgnore]
        public Rectangle Rectangle = new Rectangle(0,0,0,0);

        [XmlIgnore] 
        public object Data;
        
        [XmlAttribute("FilePath")]
        public string File="";
        
        [XmlAttribute("N")]
        public string Name="";
        
        [XmlAttribute("X")]
        public int X
        {
            get { return Rectangle.X; }
            set { Rectangle.X = value; }
        }

        public override string ToString()
        {
            return $"{Name} {X}-{Y}:{W}-{H}";
        }

        [XmlAttribute("Y")]
        public int Y
        {
            get { return Rectangle.Y; }
            set { Rectangle.Y = value; }
        }
        
        [XmlAttribute("W")]
        public int W
        {
            get { return Rectangle.Width; }
            set { Rectangle.Width = value; }
        }
        
        [XmlAttribute("H")]
        public int H
        {
            get { return Rectangle.Height; }
            set { Rectangle.Height = value; }
        }

        public BitmapArea()
        {
            
        }
        
        public BitmapArea(Bitmap cropped, Rectangle rectangle, string name = "")
        {
            Cropped = cropped;
            Rectangle = rectangle;
            Name = name;
        }
    }
}