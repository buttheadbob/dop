﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using BlockEditor.Images;
using BlockEditor.Serialization;
using PlanetGenerator2.MapData;

namespace BlockEditor.PlanetData
{
    public class Planet
    {
        public OreGeneratorDefinition Definition;
        public Dictionary<string, PlanetSide> Sides = new Dictionary<string, PlanetSide>();

        public void LoadDefinition(string path)
        {
            var definition = new XReader().Read<OreGeneratorDefinition>(path);
            definition.Mapping?.OnDeserialized();
            
            Definition = definition;
            
            object instance;
            if (true)
            {
                var code = ScriptManager.GenerateOreGeneratorClassCode(Definition);
                instance = ScriptManager.CompileOreGeneratorClass(code);
            }
            else
            {
                instance = new PlanetEditorScript();
            }

            var fx = ScriptManager.CompileOreGeneratorClass(instance);
            if (fx == null) return;
            
            Definition.Init(fx);
        }
        
        public void LoadImages(string planetPath)
        {
            var files = Directory.EnumerateFiles(planetPath).ToList();
            //Parallel.ForEach(files, (x) =>
            files.ForEach((x)=>
            {
                try
                {
                    var name = Path.GetFileName(x);
                    if (name.EndsWith("_add.png")) return;
                        
                        
                    PlanetSide planetSide = null;
                    lock (Sides)
                    {
                        var nn = name.Replace(".png", "").Replace("_mat", "");
                        if (!Sides.TryGetValue(nn, out planetSide))
                        {
                            Sides[nn] = new PlanetSide(this, nn);
                            planetSide = Sides[nn];
                        }
                    }

                    if (name.EndsWith("_mat.png"))
                    {
                        planetSide.SetMaterial(new Pic(x));
                        var wtf = 0;
                    }
                    else
                    {
                        planetSide.heightmap = MyImage.Load(x, true).Data as ushort[];
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Wtf? " + e);
                }
            });

            foreach (var p in Sides)
            {
                p.Value.Init();
            }
        }


        public void DoSides(OreGeneratorDefinition definition)
        {
            Parallel.ForEach(Sides, (x) =>
            {
                DoSide(x.Value, definition);
            });
            
            foreach (var x in Sides)
            {
                var bmp = x.Value.material.ToBitmap();
                var name = definition.Output + "/" + x.Value.Name + "_mat.png";
                bmp.Save(name, ImageFormat.Png);
            }
        }
        public void DoSide(PlanetSide ps, OreGeneratorDefinition definition)
        {
            var argument = new OreGeneratorArgument();
            argument.GlobalData = new ByteLayerArray(ps.mWH,ps.mWH, 1);
            argument.PlanetSide = ps;
            argument.Definition = definition;

            var d = DateTime.Now;
            definition.Funcs.PreprocessFunc?.Invoke(ps, definition, argument);

            foreach (var l in definition.Layers)
            {
                DoLayer(ps, definition, l, argument);
            }

            //Output.Error($"{DateTime.Now - d} passed");

        }


        public void Calculate(byte Color)
        {
            var dict = new Dictionary<byte, int>();
            foreach (var s in Sides)
            {
                dict.Sum(s.Value.material.Calculate(Color));
            }

            var total = 0;
            foreach (var kv in dict)
            {
                total += kv.Value;
            }

            var byGroup = new Dictionary<byte, int>(); 
            var m = PlanetEditor2.Instance.ColorMapping.GetMapping(Color);
            List<string> texts1 = new List<string>();
            List<string> texts2 = new List<string>();
            foreach (var kv in dict)
            {
                var colorMapping = m.GetValueOrDefault(kv.Key, null);
                var name = colorMapping?.Name ?? $"Unknown[{kv.Key}]";
                var volume = kv.Value * colorMapping?.Depth ?? 1;
                texts1.Add($"{name}: {kv.Value} {volume} {100d*kv.Value / total}%");
                
                byGroup.Sum(colorMapping?.SubId ?? 128, kv.Value);
            }
            
            foreach (var kv in byGroup)
            {
                texts2.Add($"{kv.Key}: {kv.Value} : {(100d*kv.Value / total) :F3}%");
            }
            
            texts1.Sort();
            texts2.Sort();
            
            MessageBox.Show(String.Join("\r\n", texts1) + "\r\n==============\r\n" + String.Join("\r\n", texts2));
        }
        
        private void DoLayer(PlanetSide ps, OreGeneratorDefinition generatorDefinition, OreGeneratorLayer l, OreGeneratorArgument argument)
        {
            if (!l.Sides.HasFlag(ps.GetSide())) return;
            var rand = l.Seed == -1 ? new Random() : new Random(l.Seed);
            

            ps.ResetLocalCanPlace();
            
            var availablePlaces = l.CanPlaceFx.Invoke(ps, l, argument);
            availablePlaces.Shuffle(rand);
            
            int added = 0;
            
            foreach (var v in availablePlaces)
            {
                var skip = !ps.GetCanPlace(v);
                if (skip) 
                    continue;

                skip = (!l.PrePlaceFx?.Invoke(ps, l, argument)) ?? true;
                if (skip)
                    continue;

                argument.I = v;
                
                var list = new List<OreGeneratorImage>(l.Variants);
                list.Shuffle(rand);
                
                var bmp = list[0].Picture;
                bmp = new Distortion().Distort(list[0].Distortion, bmp, Pic.R);
                bmp = Pic.RotateFlip(bmp, rand.Next(new[]
                {
                    RotateFlipType.Rotate90FlipNone,
                    RotateFlipType.Rotate180FlipNone,
                    RotateFlipType.Rotate270FlipNone,
                    RotateFlipType.Rotate90FlipX,
                    RotateFlipType.Rotate180FlipX,
                    RotateFlipType.Rotate270FlipX,
                    RotateFlipType.Rotate90FlipXY,
                    RotateFlipType.Rotate180FlipXY,
                    RotateFlipType.Rotate270FlipXY,
                }));

                var replaceId = generatorDefinition.Mapping.BStringDict.GetValueOrDefault(l.OreName, null)?.Id ?? 0;
                var mergeResult = ps.material.MergeCentered(bmp, v * 3, Pic.ORE, Pic.R,(map, original) => map == 255 ? replaceId : original);
                
                if (!mergeResult) continue;
                
                added++;

                l.AfterPlaceFx?.Invoke(ps, l, argument);
                
                if (added >= l.Maximum) return;
            }
        }
    }
}