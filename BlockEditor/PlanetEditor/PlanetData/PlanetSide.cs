﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using EmptyKeys.UserInterface.Media;
using VRageMath;
using Color = System.Drawing.Color;

namespace BlockEditor.PlanetData
{

    public class PlanetSide {
        public Pic material;
        public Pic staticInfo;
        public ByteLayerArray canPlaceGlobal;
        public ByteLayerArray canPlaceLocal;
        public int mWH = -1;
        public ushort[] heightmap;
        public int heightmapW;
        public string Name;

        public PlanetSide(Planet p, string name)
        {
            Name = name;
        }

        public void SetMaterial(Pic material)
        {
            this.material = material;
            staticInfo = new Pic(material.wh, material.wh, 3);
        }

        public bool GetCanPlace(int i)
        {
            return canPlaceLocal.Get(i,0) == 1 && canPlaceGlobal.Get(i,0) == 1;
        }
        
        public void SetGlobalCanPlace(int i, bool can)
        {
            canPlaceGlobal.Set(i,0, (byte)(can ? 1 : 0));
        }
        
        public void SetLocalCanPlace(int i, bool can)
        {
            canPlaceLocal.Set(i,0, (byte)(can ? 1 : 0));
        }
        
        public void ResetLocalCanPlace()
        {
            canPlaceLocal.FillAll(0, 1);   
        }

        public void Init()
        {
            mWH = material.w;
            canPlaceGlobal = new ByteLayerArray(material.w, material.h, 1);
            canPlaceLocal = new ByteLayerArray(material.w, material.h, 1);
            
            canPlaceGlobal.FillAll(0, 1);

            heightmapW = (int)Math.Sqrt(heightmap.Length);
        }

        public OreGeneratorSides GetSide()
        {
            return OreGeneratorSides.Up;
        }


        public Bitmap GetHeightMapAsBitmap()
        {
            var bitmap = new Bitmap(heightmapW, heightmapW, PixelFormat.Format24bppRgb);
            var pic = new Pic(bitmap);

            for (int x = 0; x < heightmapW; x++)
            {
                for (int y = 0; y < heightmapW; y++)
                {
                    var v = heightmap[x+y*heightmapW];

                    //var g = v % 256;
                    var r = v / 256;
                    pic.Set(x,y, Pic.R, (byte)r);
                    pic.Set(x,y, Pic.G, (byte)r);
                    pic.Set(x,y, Pic.B, (byte)r);
                }   
            }

            return pic.ToBitmap();
        }
        
        public Bitmap GetHeightMapAsWaterLevel(ulong deep, ulong mid, ulong beachLevel, ulong oreLevel)
        {
            var bitmap = new Bitmap(heightmapW, heightmapW, PixelFormat.Format24bppRgb);
            var pic = new Pic(bitmap);

            var colors = new Color[]
            {
                Color.FromArgb(0, 0, 255),
                Color.FromArgb(75, 75, 255),
                Color.FromArgb(255, 255, 0),
                Color.FromArgb(125, 75, 0),
            };
            
            var deeps = new ulong[]
            {
                deep,
                mid,
                beachLevel,
                oreLevel
            };

            for (int x = 0; x < heightmapW; x++)
            {
                for (int y = 0; y < heightmapW; y++)
                {
                    
                    var v = heightmap[x+y*heightmapW];
                    for (var d =0; d<deeps.Length; d++)
                    {
                        if (v <= deeps[d])
                        {
                            //var c1 = colors[d-1];
                            //var c2 = colors[d];
                            //var dd = (float)(v-deeps[d-1]) / (deeps[d] - deeps[d-1]);
                            //var r = MathHelper.Lerp(c1.R, c2.R, dd);
                            //var g = MathHelper.Lerp(c1.R, c2.R, dd);
                            //var b = MathHelper.Lerp(c1.R, c2.R, dd);
                            pic.SetColor(x,y, colors[d]);
                            break;
                        }
                    }
                    
                    //pic.Set(x,y, Color.Black);
                }   
            }

            return pic.ToBitmap();
        }

        public ushort GetHeightAt(double x2, double y2)
        {
            var r = (double) heightmapW / (double) material.w;
            x2 *= r;
            y2 *= r;
            var x = (int) x2;
            var y = (int) y2;
            if (x < 0 || x > heightmapW || y < 0 || y > heightmapW)
            {
                return 0;
            }
            var value = heightmap[x+y*heightmapW];
            return value;
        }

        public void SetHeightAt(int x, int y, ushort value)
        {
            heightmap[x + y * heightmapW] = value;
        }
    }
}