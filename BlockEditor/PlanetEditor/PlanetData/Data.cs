﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml.Serialization;
using BlockEditor.SE;
using OreGenFx = System.Func<BlockEditor.PlanetData.PlanetSide, BlockEditor.PlanetData.OreGeneratorLayer, BlockEditor.PlanetData.OreGeneratorArgument, bool>;
using OreGenCanPlace = System.Func<BlockEditor.PlanetData.PlanetSide, BlockEditor.PlanetData.OreGeneratorLayer, BlockEditor.PlanetData.OreGeneratorArgument, System.Collections.Generic.List<int>>;


namespace BlockEditor.PlanetData
{

    public class RBGMapping
    {
        public List<ColorMapping> RMapping = new List<ColorMapping>();
        public List<ColorMapping> GMapping = new List<ColorMapping>();
        public List<ColorMapping> BMapping = new List<ColorMapping>();

        [XmlIgnore] public Dictionary<byte, ColorMapping> RDict = new Dictionary<byte, ColorMapping>();
        [XmlIgnore] public Dictionary<byte, ColorMapping> GDict = new Dictionary<byte, ColorMapping>();
        [XmlIgnore] public Dictionary<byte, ColorMapping> BDict = new Dictionary<byte, ColorMapping>();
        [XmlIgnore] public Dictionary<byte, byte[]> RSubDict = new Dictionary<byte, byte[]>();
        [XmlIgnore] public Dictionary<byte, byte[]> GSubDict = new Dictionary<byte, byte[]>();
        [XmlIgnore] public Dictionary<byte, byte[]> BSubDict = new Dictionary<byte, byte[]>();
        [XmlIgnore] public Dictionary<string, ColorMapping> RStringDict = new Dictionary<string, ColorMapping>();
        [XmlIgnore] public Dictionary<string, ColorMapping> GStringDict = new Dictionary<string, ColorMapping>();
        [XmlIgnore] public Dictionary<string, ColorMapping> BStringDict = new Dictionary<string, ColorMapping>();

        public void OnDeserialized()
        {
            foreach (var c in RMapping)
            {
                if (RSubDict.TryGetValue(c.SubId, out var prev))
                {
                    RSubDict[c.SubId] = prev.Add(c.SubId);
                }
                else
                {
                    RSubDict[c.SubId] = new byte[] {c.Id};
                }
                
                RDict[c.Id] = c;
                RStringDict[c.Name] = c;
            }
            
            foreach (var c in GMapping)
            {
                if (GSubDict.TryGetValue(c.SubId, out var prev))
                {
                    GSubDict[c.SubId] = prev.Add(c.Id);
                }
                else
                {
                    GSubDict[c.SubId] = new byte[] {c.Id};
                }
                
                GDict[c.Id] = c;
                GStringDict[c.Name] = c;
            }
            
            foreach (var c in BMapping)
            {
                if (BSubDict.TryGetValue(c.SubId, out var prev))
                {
                    BSubDict[c.SubId] = prev.Add(c.SubId);
                }
                else
                {
                    BSubDict[c.SubId] = new byte[] {c.Id};
                }
                
                BDict[c.Id] = c;
                BStringDict[c.Name] = c;
            }
        }

        public void OnSerialize()
        {
            foreach (var c in RDict)
            {
                RMapping.Add(c.Value);
            }
            
            foreach (var c in GDict)
            {
                GMapping.Add(c.Value);
            }
            
            foreach (var c in BDict)
            {
                BMapping.Add(c.Value);
            }
        }

        public Dictionary<byte, ColorMapping> GetMapping(byte color)
        {
            switch (color)
            {
                case Pic.B: return BDict;
                case Pic.G: return GDict;
                case Pic.R: return RDict;
                default: return null;
            }
        }
    }
    
    public class ColorMapping
    {
        [XmlAttribute("Id")]
        public byte Id;
        
        [XmlAttribute("SubId")]
        public byte SubId;
        
        [XmlAttribute("Name")]
        public string Name;
        
        [XmlAttribute("A")]
        public int A = 255;
        [XmlAttribute("R")]
        public int R;
        [XmlAttribute("G")]
        public int G;
        [XmlAttribute("B")]
        public int B;
        
        [XmlAttribute("Depth")]
        public int Depth;

        [XmlIgnore] public Color Color => Color.FromArgb(A, R, G, B);
    }

    public class OreCodeGenerator
    {
        [XmlAttribute()] public string BaseClass;
        
        [XmlAttribute()] public string PreprocessFilter;
        [XmlAttribute()] public string PreprocessFilter_Formula;
        
        [XmlAttribute()] public string PrePlaceFilter;
        [XmlAttribute()] public string PrePlaceFilter_Formula;
        [XmlAttribute()] public string CanPlaceFilter;
        [XmlAttribute()] public string CanPlaceFilter_Formula;
        [XmlAttribute()] public string AfterPlaceFilter;
        [XmlAttribute()] public string AfterPlaceFilter_Formula;
    }
    public class OreGeneratorDefinition
    {
        [XmlElement] 
        public OreCodeGenerator CodeGenerator;
        
        [XmlElement]
        public String RuleSet;
        
        [XmlElement]
        public String Output;

        [XmlElement] 
        public RBGMapping Mapping;
        
        [XmlArrayItem("Rule")]
        public Formula[] PreprocessRules;

        [XmlArrayItem("Layer")]
        public OreGeneratorLayer[] Layers;

        [XmlIgnore] 
        public ScriptManager.OreGeneratorFunctions Funcs = null;
        public void Init(ScriptManager.OreGeneratorFunctions funcs)
        {
            this.Funcs = funcs;

            int x = 0;
            foreach (var layer in Layers)
            {
                layer.CanPlaceFx = Funcs.CanPlaceFuncs[x];
                layer.AfterPlaceFx = Funcs.AfterPlaceFuncs[x];
                layer.PrePlaceFx = Funcs.PrePlaceFuncs[x];
                x++;
            }
        }
    }


    public enum OreGeneratorMerge
    {
        None, Lowest, Highest, Latest 
    }
    
    [Flags]
    public  enum OreGeneratorSides
    {
        Front, Back, Left, Right, Up, Down
    }

    public class Args
    {
        public string String;
        public double Double = Double.NaN;
        public int Int = int.MaxValue;
        public double[] DoubleArr;
        public byte[] ByteArr;

        public Args(string s)
        {
            String = s;
            if (s != null)
            {
                if (!double.TryParse(s, out Double))
                {
                    Double = Double.NaN;
                }

                Int = (int)Double;
                DoubleArr = s.AsNumbers();

                if (DoubleArr != null)
                {
                    ByteArr = new byte[DoubleArr.Length];
                    for (int i = 0; i < DoubleArr.Length; i++)
                    {
                        ByteArr[i] = (byte) DoubleArr[i];
                    }
                }
                
            }
        }
    }

    public class OreGeneratorArgument
    {
        public PlanetSide PlanetSide;
        public int I;
        
        //public ByteLayerArray BiomeData;
        public ByteLayerArray GlobalData;
        public OreGeneratorDefinition Definition;

        public int WH
        {
            get
            {
                return PlanetSide.mWH;
            }
        }
        
        public int X
        {
            get
            {
                return I % WH;
            }
        }
        
        public int Y
        {
            get
            {
                return I / WH;
            }
        }
        
        public Args[] Args;
        
        public ushort GetHeight(int argX, int argY)
        {
            var w = PlanetSide.heightmapW;
            var x = argX * PlanetSide.heightmapW / PlanetSide.material.w;
            var y = argY * PlanetSide.heightmapW / PlanetSide.material.h;
            return PlanetSide.heightmap[x + y * PlanetSide.heightmapW];
        }
        
        public int GetMaxHeightDifference(int argX, int argY, int r)
        {
            var w = PlanetSide.heightmapW;
            
            var zx = (argX) * w / PlanetSide.material.w;
            var zy = (argY) * w / PlanetSide.material.h;
            
            var sx = (argX-r) * w / PlanetSide.material.w;
            var sy = (argY-r) * w / PlanetSide.material.h;
            var ex = (argX+r) * w / PlanetSide.material.w;
            var ey = (argY+r) * w / PlanetSide.material.h;

            if (sx == ex)
            {
                sx -= 1;
                ex += 1;
            }
            
            if (sy == ey)
            {
                sy -= 1;
                ey += 1;
            }

            var original = PlanetSide.heightmap[zx + zy * PlanetSide.heightmapW];
            var maxDelta = 0;
            sx = Math.Max(0, sx);
            sy = Math.Max(0, sy);
            ex = Math.Min(w-1, sx);
            ey = Math.Min(w-1, ey);
            
            for (int x = sx; x<=ex; x++)
            {
                for (int y = sy; y<=ey; y++)
                {
                    var h = PlanetSide.heightmap[x + y * PlanetSide.heightmapW];
                    if (Math.Abs(original - h) > maxDelta)
                    {
                        maxDelta = Math.Abs(original - h);
                    } 
                }
            }
            
            return maxDelta;
        }
    }
    
    public class OreGeneratorLayer
    {
        [XmlAttribute("Merge")] 
        public OreGeneratorMerge Merge = OreGeneratorMerge.Latest;
        
        [XmlAttribute("Seed")] 
        public int Seed = 0;
        
        [XmlAttribute("Maximum")] 
        public int Maximum = int.MaxValue;

        [XmlAttribute("OreName")] 
        public string OreName = "Unknown";
        
        [XmlAttribute("Sides")] 
        public OreGeneratorSides Sides = OreGeneratorSides.Up | OreGeneratorSides.Down | OreGeneratorSides.Left | OreGeneratorSides.Right | OreGeneratorSides.Front | OreGeneratorSides.Back;

        [XmlArrayItem("Item")]
        public OreGeneratorImage[] Variants;

        [XmlArrayItem("Rule")]
        public Formula[] CanPlaceRules;
        
        [XmlArrayItem("Rule")]
        public Formula[] PrePlaceRules;

        [XmlArrayItem("Rule")]
        public Formula[] AfterPlaceRules;
        
        public OreGenCanPlace CanPlaceFx;
        public OreGenFx PrePlaceFx;
        public OreGenFx AfterPlaceFx;

    }

    
    
    

    
    public class OreGeneratorImage
    {
        [XmlAttribute]
        public string Src = "";
        
        [XmlAttribute]
        public bool Rotate = true;
        
        [XmlAttribute]
        public int Distortion = 0;
        


        [XmlIgnore]
        private Pic m_picture = null;

        [XmlIgnore]
        public Pic Picture
        {
            get
            {
                if (m_picture == null)
                {
                    m_picture = new Pic(Src);
                }

                return m_picture;
            }
        }
    }
}