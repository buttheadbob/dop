﻿using System;
using System.Collections.Generic;
using GemCraft2;
using Slime.Geom;
using VRageMath;
using Line = Slime.Geom.Line;

namespace BlockEditor
{
    
    
    
    public class PixelLine
    {
        public P from;
        public P to;
        public List<P> pixels;

        public PixelLine(P from, P to, List<P> pixels)
        {
            this.from = from;
            this.to = to;
            this.pixels = pixels;
        }
        
        
        
        //Not Ideal
        public static PixelLine Find(List<P> pixels, P from, P to, double tolerance = 1, bool test = false, int sphereEndRadius = 0)
        {
            var list = new List<P>();
            var l = new Line(from, to);
            
            foreach (var c in pixels)
            {
                if (l.Distance(c) < tolerance && (!test || l.BelongsToSegment(c))) //)
                {
                    list.Add(c);
                }
            }
            
            return new PixelLine(from, to, list);
        }
        
        

        private static int IsToLeftOrRight(P A, P B, P P)
        {
            return Math.Sign((B.x - A.x) * (P.y - A.y) - (B.y - A.y) * (P.x - A.x));
        }
    }
}