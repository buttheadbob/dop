﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows;
using System.Windows.Media;
using BlockEditor.Layers;
using BlockEditor.PlanetData;
using BlockEditor.PlanetEditor.Photoshop.Tools.CurveEditor;
using BlockEditor.Serialization;
using Curves;
using GemCraft2;
using Slime.Geom;
using VRage.Render.Image;
//using VRage.Render.Image;
using Color = VRageMath.Color;
using LinearGradientBrush = System.Drawing.Drawing2D.LinearGradientBrush;
using Pen = System.Drawing.Pen;
using Point = System.Windows.Point;

namespace BlockEditor
{
    public partial class PicEditor : Window
    {
        private ImageLayer l1;
        private ImageLayer l2;
        private ImageLayer l3;
        private ImageLayer l4;

        private static double POW = 1.5d;
        private static Func<double, double> interpolation = (d) => Math.Pow(d, POW)+0.1;


        private Pic DebugSurface;
        private Pic DebugSurface2;

        private Dictionary<P, List<PixelLine>> MergeInfo;
        private readonly CurveEditorTool Tool;

        public static Dictionary<P, List<P>> GetClosest(List<P> pixels, List<P> pixels2)
        {
            Dictionary<P, List<P>> dict = new Dictionary<P, List<P>>();
            foreach (var p in pixels2)
            {
                var listClosest = new List<P>();
                var closestDistance = 999999999999999999d;
                for (var x=0; x<pixels.Count; x++)
                {
                    var d = p.Distance(pixels[x]);
                    if (d == closestDistance)
                    {
                        listClosest.Add(pixels[x]);
                    } 
                    else if (d < closestDistance)
                    {
                        listClosest.Clear();
                        listClosest.Add(pixels[x]);
                        closestDistance = d;
                    }
                }
                
                dict.Add(p, listClosest);
            }

            return dict;
        }
        
        public PicEditor()
        {
            App.Dispatcher = Dispatcher;
            ScriptManager.Init();
            X.Init();
            InitializeComponent();
            
            DebugSurface = new Pic("C:\\Users\\slime\\AppData\\Roaming\\SpaceEngineers\\Mods\\Generator\\test.png");

            DebugSurface.ForAll((i, x, y) =>
            {
                DebugSurface.SetOre(x, y, 0);
                DebugSurface.SetFoliage(x, y, 0);
                return true;
            });
            
            DebugSurface2 = new Pic(400, 400, 3);
            var l5 = new ImageLayer("L5", () => DebugSurface.ToBitmap(), autoLoad:true);
            var l6 = new ImageLayer("L6", () => DebugSurface2.ToBitmap(), autoLoad:true);
            PhotoshopView.AddLayer(l5);
            PhotoshopView.AddLayer(l6);

            Tool = new CurveEditorTool(PhotoshopView, l5);
            PhotoshopView.AddTool(Tool);
            PhotoshopView.SelectTool(Tool);
            
            PhotoshopView.GenerateInfo += PhotoshopViewOnGenerateInfo;

            Settings.Init();
        }

        private void PhotoshopViewOnGenerateInfo(StringBuilder arg1, Point arg2, Point scaled)
        {
            if (scaled.X<0 || scaled.Y<0 || scaled.X >= DebugSurface2.w || scaled.Y >= DebugSurface2.h) return;
            var color = DebugSurface.GetColor((int)scaled.X,(int)scaled.Y);
            var color2 = DebugSurface2.GetColor((int)scaled.X,(int)scaled.Y);

            arg1.Append(" Color:" + color + " / " + color2);
        }

        public void Apply()
        {
            var start = Tool.Logic.GetStart();
            var end = Tool.Logic.GetEnd();
            
            var h1 = DebugSurface.GetR(start.x, start.y);
            var h2 = DebugSurface.GetR(end.x, end.y);
            //var copy = new Pic(DebugSurface);
            Tool.Logic.Apply((x,y,v,smoothing) =>
            {
                var vv = h1+(double)(h2 - h1) * v;
                if (smoothing > 0)
                {
                    var original = (int)DebugSurface.GetR(x, y);
                    vv = (vv + original * smoothing) / (1d+smoothing);
                }
                DebugSurface2.SetVoxel(x, y, (byte)vv); 
            }, (x, y, v) =>
            {
                if (v<=0) return;
            });
        }

        private void PowSlider_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            POW = PowSlider.Value;
            l3?.Refresh();
            l4?.Refresh();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            Apply();
        }
    }
}