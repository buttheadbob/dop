﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using BlockEditor.Layers;
using BlockEditor.PlanetData;
using BlockEditor.Serialization;
using GemCraft2;
using NLog.Fluent;
using Slime.Geom;
using VRageMath;
using Color = System.Drawing.Color;
using Point = System.Windows.Point;

namespace BlockEditor
{
    
    public interface ILakeCreatorProvider
    {
        int GetWidth();
        int GetHeight();
        List<P> GetBorders1();
        List<P> GetBorders2();
        List<P> GetAllPixels1();
        List<P> GetAllPixels2();
        P GetCenter();
    }

    public interface ILakeApplyFx <T>
    {
        T Apply(ILakeCreatorProvider provider, Dictionary<P, List<PixelLine>> affected, Func<double, double> interpolate);
    }

    public class LakeApplyPic : ILakeApplyFx<Pic>
    {
        private Pic Layer1;
        private Pic Layer2;

        public LakeApplyPic(Pic layer1, Pic layer2)
        {
            Layer1 = layer1;
            Layer2 = layer2;
        }
        
        public Pic Apply(
            ILakeCreatorProvider provider, 
            Dictionary<P, List<PixelLine>> affected, 
            Func<double, double> interpolate)
        {
            Pic Result = new Pic(Layer1.w, Layer1.h, 3);
            
            foreach (var v in affected)
            {
                var conflictingLines = v.Value;

                double r = 0, g = 0, b = 0;
                foreach (var pl in conflictingLines)
                {
                    var d1 = pl.from.Distance(pl.to);
                    var d2 = pl.from.Distance(v.Key);
                    var influence = 1 - d2 / d1;

                    influence = MathHelper.Clamp(influence, 0, 1);

                    var rr1 = Layer1.GetR(pl.to.x, pl.to.y);
                    var gg1 = Layer1.GetG(pl.to.x, pl.to.y);
                    var bb1 = Layer1.GetB(pl.to.x, pl.to.y);
                    
                    var rr2 = Layer2.GetR(pl.from.x, pl.from.y);
                    var gg2 = Layer2.GetG(pl.from.x, pl.from.y);
                    var bb2 = Layer2.GetB(pl.from.x, pl.from.y);

                    r += MathHelper.Lerp(rr1, rr2, influence);
                    g += MathHelper.Lerp(gg1, gg2, influence);
                    b += MathHelper.Lerp(bb1, bb2, influence);
                }

                var rrr = r / conflictingLines.Count;
                var ggg = g / conflictingLines.Count;
                var bbb = b / conflictingLines.Count;

                Result.SetColor(v.Key.x, v.Key.y, (byte)rrr,(byte)ggg,(byte)bbb);
            }
            
            foreach (var p in provider.GetAllPixels2())
            {
                Result.SetColor(p.x, p.y, Layer2.GetR(p.x, p.y), Layer2.GetG(p.x, p.y), Layer2.GetB(p.x, p.y));
            }

            return Result;
        }
    } 

    public class LakeApplyUShort : ILakeApplyFx<ushort[,]>
    {
        private ushort[,] Layer1;
        private ushort[,] Layer2;

        public LakeApplyUShort(ushort[,] layer1, ushort[,] layer2)
        {
            Layer1 = layer1;
            Layer2 = layer2;
        }

        public ushort[,] Apply(
            ILakeCreatorProvider provider, 
            Dictionary<P, List<PixelLine>> affected, 
            Func<double, double> interpolate)
        {
            //var dt = DateTime.Now;
            ushort[,] result = new ushort[provider.GetWidth(), provider.GetHeight()];
            
            foreach (var v in affected)
            {
                var conflictingLines = v.Value;

                double vv = 0;
                foreach (var pl in conflictingLines)
                {
                    var d1 = pl.from.Distance(pl.to);
                    var d2 = pl.from.Distance(v.Key);
                    var influence = 1 - d2 / d1;

                    influence = MathHelper.Clamp(influence, 0, 1);

                    influence = interpolate(influence);
                    
                    influence = MathHelper.Clamp(influence, 0, 1);
                    
                    var v1 = Layer1[v.Key.x, v.Key.y];
                    var v2 = Layer2[pl.from.x, pl.from.y];
                    vv += MathHelper.Lerp(v1, v2, influence);
                }

                
                var r = (ushort)(vv / conflictingLines.Count);
                result[v.Key.x, v.Key.y] = r;
            }

            foreach (var p in provider.GetAllPixels2())
            {
                result[p.x, p.y] = Layer2[p.x, p.y];
            }

            //var dt2 = DateTime.Now;
            //MessageBox.Show($"Apply {(dt2 - dt)}");
            return result;
        }
    }



    public class LakeProvider : ILakeCreatorProvider
    {
        private Pic Layer1;
        private Pic Layer2;

        private GlyphFinder gf1;
        private GlyphFinder gf2;
            
        private P Center = new P(87, 77);

        public LakeProvider(Pic Layer1, Pic Layer2)
        {
            if (Layer1.w != Layer2.w)
                throw new Exception($"Layer1.w != Layer2.w {Layer1.w}:{Layer2.h}");
            if (Layer1.h != Layer2.h)
                throw new Exception($"Layer1.h != Layer2.h {Layer1.w}:{Layer2.h}");
            
            this.Layer1 = Layer1;
            this.Layer2 = Layer2;

            gf1 = new GlyphFinder();
            gf1.Init(new NotExactColor(Layer1, 255, 255, 255), new FourSides());
            gf1.StartWave(new P(Layer1.w / 2, Layer1.w / 2));

            gf2 = new GlyphFinder();
            gf2.Init(new NotExactColor(Layer2, 255, 255, 255), new FourSides());
            gf2.StartWave(Center);
        }

        public int GetWidth() { return Layer1.w; }
        public int GetHeight() { return Layer1.h; }
        public List<P> GetBorders1() { return gf1.GetBorder(); }
        public List<P> GetBorders2() { return gf2.GetBorder(); }
        public List<P> GetAllPixels1() { return gf1.GetAllPixels(); }
        public List<P> GetAllPixels2() { return gf2.GetAllPixels(); }
        public P GetCenter() { return Center; }

        private ushort[,] Result;
    }
    
    public class LakeCreatorAlgorithm
    {
        private int w, h;
        public List<P> borders1 = new List<P>();
        public List<P> borders2 = new List<P>();

        public List<P> allPixels1 = new List<P>();
        public List<P> allPixels2 = new List<P>();
        public List<P> allPixels1Minus2 = new List<P>();
        
        public double angleDifference1 = Math.PI / 20;

        private ILakeCreatorProvider provider;

        

        public void Init(ILakeCreatorProvider provider)
        {
            this.provider = provider;

            w = provider.GetWidth();
            h = provider.GetHeight();
            borders1 = provider.GetBorders1();
            borders2 = provider.GetBorders2();
            allPixels1 = provider.GetAllPixels1();
            allPixels2 = provider.GetAllPixels2();

            foreach (var p in allPixels1)
            {
                if (!allPixels2.Contains(p))
                {
                    allPixels1Minus2.Add(p);
                }
            }

            foreach (var p in allPixels1)
            {
                if (!allPixels2.Contains(p))
                {
                    allPixels1Minus2.Add(p);
                }
            }
        }

        public Dictionary<P, List<PixelLine>> GetMergeInfo()
        {
            var dt = DateTime.Now;
            Dictionary<P, List<PixelLine>> Affected = new Dictionary<P, List<PixelLine>>();
            Parallel.ForEach(borders2, (c) =>
            {
                try
                {
                    var t = GetMergeInfo(c.x, c.y, provider.GetCenter());
                    lock (Affected)
                    {
                        foreach (var kv in t)
                        {
                            Affected.GetOrNew(kv.Key).AddRange(kv.Value);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            });
            
            foreach (var p in allPixels2)
            {
                Affected.Remove(p);
            }

            var dt2 = DateTime.Now;
            //MessageBox.Show($"GetMergeInfo {(dt2 - dt)}");
            return Affected;
        }
        


        private Dictionary<P, List<PixelLine>> GetMergeInfo(int x, int y, P center)
        {
            Dictionary<P, List<PixelLine>> affected = new Dictionary<P, List<PixelLine>>();
            var angle = Math.Atan2(x - center.x, y - center.y);
            var minAngle1 = angle - angleDifference1;
            var maxAngle1 = angle + angleDifference1;

            var edgeList1 = new List<P>();
            var filtered = new List<P>();
            
            foreach (var c in borders1)
            {
                if (Math.Atan2(c.x - center.x, c.y - center.y).IsBetween(minAngle1, maxAngle1))
                {
                    edgeList1.Add(c);
                }
            }
            
            foreach (var c in allPixels1Minus2)
            {
                if (Math.Atan2(c.x - center.x, c.y - center.y).IsBetween(minAngle1, maxAngle1))
                {
                    filtered.Add(c);
                }
            }
            
            foreach (var e1 in edgeList1)
            {
                var pl = PixelLine.Find(filtered, new P (x,y), e1);
                foreach (var p in pl.pixels)
                {
                    affected.GetOrNew(p).Add(pl);
                }
            }

            return affected;
        }
    }
}