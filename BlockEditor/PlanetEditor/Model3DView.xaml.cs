﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Color = System.Drawing.Color;
using Point = System.Windows.Point;

namespace BlockEditor.PlanetEditor
{
    
    
    
    public partial class Model3DView : UserControl
    {
        public Model3DView()
        {
            InitializeComponent();
            // Basic3DShapeExample();
            
            //Draw(new ushort[2,2]);
        }

        private PointLight pointLight;
        public void Draw(ushort[,] hmap, float maxH = 10, Bitmap bitmap = null)
        {
	        float minHeightValue = 0;
	        float maxHeightValue = 0;
	        for (var y = 0; y < hmap.GetLength(1); y++) {
		        for (var x = 0; x < hmap.GetLength(0); x++) {
			        float val = hmap[x, y];
			        if (val < minHeightValue) minHeightValue = val;
			        if (val > maxHeightValue) maxHeightValue = val;
		        }
	        }
	        
	        _MyModel3DGroup.Children.Clear();
	        
	        pointLight = new PointLight(Colors.White, new Point3D(-10, -10, maxHeightValue));
	        _MyModel3DGroup.Children.Add(pointLight);
	        _DrawTerrain(hmap, hmap.GetLength(0), maxH, minHeightValue,maxHeightValue, 0,0, hmap.GetLength(0),hmap.GetLength(1), bitmap);
        }
        
        private void _DrawTerrain(ushort[,] terrainMap, int terrainSize, float maxH, float minHeightValue, float maxHeightValue, int posX, int posY, int maxPosX, int maxPosY, Bitmap bitmap = null)
        {
            float halfSize = terrainSize / 2;
            float halfheight = (maxHeightValue - minHeightValue) / 2;
            float dheight = (maxHeightValue - minHeightValue);

            // creation of the terrain
            
            //LinearGradientBrush myHorizontalGradient = new LinearGradientBrush();
            //myHorizontalGradient.StartPoint = new Point(0, 0);
            //myHorizontalGradient.EndPoint = new Point(1, 100);
            //myHorizontalGradient.GradientStops.Add(new GradientStop(Colors.White, 0.0));
            //myHorizontalGradient.GradientStops.Add(new GradientStop(Colors.White, 0.02));
            //myHorizontalGradient.GradientStops.Add(new GradientStop(Colors.Red, 0.02));
            //myHorizontalGradient.GradientStops.Add(new GradientStop(Colors.Red, 0.03));
            //myHorizontalGradient.GradientStops.Add(new GradientStop(Colors.Blue, 0.04));
            //myHorizontalGradient.GradientStops.Add(new GradientStop(Colors.Blue, 0.05));
            //myHorizontalGradient.GradientStops.Add(new GradientStop(Colors.Black, 1.0));
            
            SolidColorBrush brush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255,255, 255));
            //ImageBrush brush = new ImageBrush(bitmap.ToImageSource());

            
            
            Point3DCollection point3DCollection = new Point3DCollection();
            Int32Collection triangleIndices = new Int32Collection();

            var xyscale = 3;
           
			
            //adding point
            for (var y = posY; y < maxPosY; y++) {
                for (var x = posX; x < maxPosX; x++)
                {
	                var h = terrainMap[x, y];
	                var hh = ((double) h / dheight) * maxH;
	                
                    point3DCollection.Add(new Point3D((x - halfSize)*xyscale, (y - halfSize)*xyscale, hh));
                }
            }
            
            
            PointCollection myTextureCoordinatesCollection = new PointCollection();
            
            var mesh = new MeshGeometry3D();
            //defining triangles
            int ind1 = 0;
            int ind2 = 0;
            int xLenght = maxPosX - posX;
            for (var y = 0; y < maxPosY - posY - 1; y++) {
                for (var x = 0; x < maxPosX - posX - 1; x++) {
                    ind1 = x + y * (xLenght);
                    ind2 = ind1 + (xLenght);

                    //first triangle
                    triangleIndices.Add(ind1);
                    triangleIndices.Add(ind2 + 1);
                    triangleIndices.Add(ind2);

                    //second triangle
                    triangleIndices.Add(ind1);
                    triangleIndices.Add(ind1 + 1);
                    triangleIndices.Add(ind2 + 1);
                    
                    //mesh.TextureCoordinates.Add(new Point((0),(0)));
                    //mesh.TextureCoordinates.Add(new Point((0),(0.1d)));
                    //mesh.TextureCoordinates.Add(new Point((0.1d),(0)));
                    ////
                    //mesh.TextureCoordinates.Add(new Point((0),(0.1d)));
                    //mesh.TextureCoordinates.Add(new Point((0.1d),(0)));
                    //mesh.TextureCoordinates.Add(new Point((0.1d),(0.1d)));
                    
                    
                    //mesh.TextureCoordinates.Add(new Point((x+0)/2048d,(y+0)/2048d));
                    //mesh.TextureCoordinates.Add(new Point((x+0)/2048d,(y+1)/2048d));
                    //mesh.TextureCoordinates.Add(new Point((x+1)/2048d,(y+0)/2048d));
//
                    //mesh.TextureCoordinates.Add(new Point((x+0)/2048d,(y+1)/2048d));
                    //mesh.TextureCoordinates.Add(new Point((x+1)/2048d,(y+0)/2048d));
                    //mesh.TextureCoordinates.Add(new Point((x+1)/2048d,(y+1)/2048d));
                }
            }
            
            
            mesh.Positions = point3DCollection;
            mesh.TriangleIndices = triangleIndices;
            //mesh.TextureCoordinates = myTextureCoordinatesCollection;
            
            GeometryModel3D myTerrainGeometryModel = new GeometryModel3D(mesh, new DiffuseMaterial(brush));
            _MyModel3DGroup.Children.Add(myTerrainGeometryModel);
        }
        
        
        
        #region mouse interaction

		/**
		 * <summary>
		 * Method that zoom in and out on mouse wheel. Reference Code: https://www.codeproject.com/Articles/23332/WPF-D-Primer
		 * </summary>
		 *
		 * <param name="sender">sender object</param>
		 * <param name="e">arguments</param>
		 */
		private void _Viewport3DMouseWheel(object sender, MouseWheelEventArgs e)
		{
			_MainPerspectiveCamera.Position = new Point3D(
											_MainPerspectiveCamera.Position.X,
											_MainPerspectiveCamera.Position.Y,
											_MainPerspectiveCamera.Position.Z - e.Delta / 2D);
		}

		/**
		 * <summary>
		 * variable to control the viewport rotation through the mouse
		 * </summary>
		 */
		private bool _MouseDownFlag;

		/**
		 * <summary>
		 * variable to control the viewport rotation through the mouse
		 * </summary>
		 */
		private Point _MouseLastPos;

		/**
		 * <summary>
		 * Method to control the viewport rotation through the mouse. Reference Code: https://www.codeproject.com/Articles/23332/WPF-D-Primer
		 * </summary>
		 *
		 * <param name="sender">sender object</param>
		 * <param name="e">arguments</param>
		 */
		private void _Viewport3DMouseUp(object sender, MouseButtonEventArgs e)
		{
			_MouseDownFlag = false;
		}

		/**
		 * <summary>
		 * Method to control the viewport rotation through the mouse. Reference Code: https://www.codeproject.com/Articles/23332/WPF-D-Primer
		 * </summary>
		 *
		 * <param name="sender">sender object</param>
		 * <param name="e">arguments</param>
		 */
		private void _Viewport3DMouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.LeftButton != MouseButtonState.Pressed) return;
			_MouseDownFlag = true;
			Point pos = Mouse.GetPosition(_MyViewport3D);
			_MouseLastPos = new Point(pos.X - _MyViewport3D.ActualWidth / 2, _MyViewport3D.ActualHeight / 2 - pos.Y);
		}

		/**
		 * <summary>
		 * Method to control the viewport rotation through the mouse. Reference Code: https://www.codeproject.com/Articles/23332/WPF-D-Primer
		 * </summary>
		 *
		 * <param name="sender">sender object</param>
		 * <param name="e">arguments</param>
		 */
		private void _Viewport3DMouseMove(object sender, MouseEventArgs e)
		{
			if (!_MouseDownFlag) return;
			Point pos = Mouse.GetPosition(_MyViewport3D);
			Point actualPos = new Point(pos.X - _MyViewport3D.ActualWidth / 2, _MyViewport3D.ActualHeight / 2 - pos.Y);
			double dx = actualPos.X - _MouseLastPos.X;
			double dy = actualPos.Y - _MouseLastPos.Y;
			double mouseAngle = 0;

			if (dx != 0 && dy != 0) {
				mouseAngle = Math.Asin(Math.Abs(dy) / Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2)));

				if (dx < 0 && dy > 0) mouseAngle += Math.PI / 2;
				else if (dx < 0 && dy < 0) mouseAngle += Math.PI;
				else if (dx > 0 && dy < 0) mouseAngle += Math.PI * 1.5;
			}
			else if (dx == 0 && dy != 0) {
				mouseAngle = Math.Sign(dy) > 0 ? Math.PI / 2 : Math.PI * 1.5;
			}
			else if (dx != 0 && dy == 0) {
				mouseAngle = Math.Sign(dx) > 0 ? 0 : Math.PI;
			}

			double axisAngle = mouseAngle + Math.PI / 2;

			Vector3D axis = new Vector3D(Math.Cos(axisAngle) * 4, Math.Sin(axisAngle) * 4, 0);

			double rotation = 0.02 * Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));

			Transform3DGroup group = _MyModel3DGroup.Transform as Transform3DGroup;

			if (group == null) {
				group = new Transform3DGroup();
				_MyModel3DGroup.Transform = group;
			}

			QuaternionRotation3D r =
				 new QuaternionRotation3D(
				 new Quaternion(axis, rotation * 180 / Math.PI));
			group.Children.Add(new RotateTransform3D(r));

			_MouseLastPos = actualPos;
		}

		#endregion mouse interaction


		public void InnerKeyUp(object sender, KeyEventArgs e)
		{
			
		}
		public void InnerKeyDown(KeyEventArgs e)
		{
			if (pointLight == null) return;
			var movement = new Vector3D();
			if (e.Key == Key.W) movement.Y += 3d;
			if (e.Key == Key.A) movement.X -= 3d;
			if (e.Key == Key.S) movement.Y -= 3d;
			if (e.Key == Key.D) movement.X += 3d;
			if (e.Key == Key.C) movement.Z -= 15d;
			if (e.Key == Key.Space) movement.Z += 15d;

			_MainPerspectiveCamera.Position = Point3D.Add(_MainPerspectiveCamera.Position, movement);
			pointLight.Position = _MainPerspectiveCamera.Position;
			 
			var rotate = 0d;
			var rotateUpDown = 0d;
			if (e.Key == Key.E) rotate += 0.05d;
			if (e.Key == Key.Q) rotate -= 0.05d;

			if (e.Key == Key.Up) rotateUpDown += 0.01d;
			if (e.Key == Key.Down) rotateUpDown -= 0.01d;

			Transform3DGroup group = _MyModel3DGroup.Transform as Transform3DGroup;

			if (group == null) {
				group = new Transform3DGroup();
				_MyModel3DGroup.Transform = group;
			}

			QuaternionRotation3D r =
				new QuaternionRotation3D(
					new Quaternion(new Vector3D(0, 0, 1), rotate * 180 / Math.PI));
			group.Children.Add(new RotateTransform3D(r));

			_MainPerspectiveCamera.LookDirection += new Vector3D(0, rotateUpDown, 0);
		}

    }
}