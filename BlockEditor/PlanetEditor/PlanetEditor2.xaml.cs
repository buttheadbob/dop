﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using BlockEditor.Layers;
using BlockEditor.Library;
using BlockEditor.PlanetData;
using BlockEditor.PlanetEditor.Photoshop.Tools.CurveEditor;
using BlockEditor.Serialization;
using Curves;
using ImageMagick;
using PlanetGenerator2.MapData;
using Slime.Geom;
using Point = System.Windows.Point;
using ToolTip = System.Windows.Controls.ToolTip;

namespace BlockEditor
{
    public partial class PlanetEditor2 : Window
    {
        private static string Path = "C:\\Users\\slime\\AppData\\Roaming\\SpaceEngineers\\Mods";
        private Planet planet = null;
        public static PlanetEditor2 Instance = null;
        public string Side = "up";
 
        
        public static List<string> Sides = new List<string>() { "up", "down", "left", "right", "back", "front" };
        public RBGMapping ColorMapping = new RBGMapping();
        
        public PlanetEditor2()
        {
            Instance = this;
            App.Dispatcher = Dispatcher;
            ScriptManager.Init();
            X.Init();
            InitializeComponent();
            
            Settings.Init();
            planet = new Planet();
            planet.LoadDefinition(Settings.Data[Settings.DefaultPlanet]);
            planet.LoadImages($"{Path}\\Generator\\Trelan");//Settings.Data[Settings.DefaultPlanetImages]);

            //Extract();
            
            Show(planet.Sides[Side]);

            ScriptManager.GenerateOreGeneratorClassCode(planet.Definition);

            ColorMapping = planet.Definition.Mapping;
            //planet.Calculate(Pic.R);
            //planet.Calculate(Pic.G);

            PhotoshopView.OnMouseUp += PhotoshopViewOnOnMouseUp;
            PhotoshopView.GenerateInfo += PhotoshopGenerateInfo;
        }

        private void PhotoshopGenerateInfo(StringBuilder arg1, Point unscalePoint, Point scaledPoint)
        {
            var x2 = (int)scaledPoint.X;
            var y2 = (int)scaledPoint.Y;

            if (planet == null || planet.Sides == null || !planet.Sides.ContainsKey(Side)) return;
            if (x2 < 0 || x2 >= planet.Sides[Side].material.w || y2 < 0 || y2 >= planet.Sides[Side].material.w)
            {
                PhotoshopView.Test.Content = "";
                return;
            }

            var material = planet.Sides[Side].material;
            var r = material.GetR(x2, y2);
            var g = material.GetG(x2, y2);
            var b = material.GetB(x2, y2);

            var biome = ColorMapping.RDict.GetValueOrDefault(r)?.Name ?? $"Unknown biome";
            var ore = ColorMapping.BDict.GetValueOrDefault(b)?.Name ?? $"Unknown ore";
            var foliage = ColorMapping.GDict.GetValueOrDefault(g)?.Name ?? $"Unknown foliage";
            var height = planet.Sides[Side].GetHeightAt((int)x2, (int)y2);

            arg1.Clear();
            arg1.Append($"x{x2} y{y2}= {biome}[{r}] : {foliage}[{g}] : {ore}[{b}] : H {height}");
        }


        public static ushort[,] Extract(ushort[] original, int w, int h, int startX, int startY, int endX, int endY)
        {
            startX = Math.Max(0, startX);
            startY = Math.Max(0, startY);
            endX = Math.Min(w, endX);
            endY = Math.Min(h, endY);

            var result = new ushort[endX - startX, endY - startY];
            
            for (int x = startX; x < endX; x++)
            {
                for (int y = startY; y < endY; y++)
                {
                    result[x - startX, y - startY] = original[x + y * h];
                }   
            }

            return result;
        }

        private List<P> lastClicks = new List<P>();
        private List<P> rightClicks = new List<P>();
        
        private void PhotoshopViewOnOnMouseUp(Point unscaled, Point scaled, MouseButtonEventArgs e)
        {
            /*if (e.ChangedButton == MouseButton.Right)
            {
                lastClicks.Clear();
            }
            
            if (e.ChangedButton == MouseButton.Right)
            {
                rightClicks.Add(new P ((int)scaled.X, (int)scaled.Y));
            }

            if (e.ChangedButton == MouseButton.Left)
            {
                lastClicks.Add(new P ((int)scaled.X, (int)scaled.Y));
            }

            if (lastClicks.Count == 4)
            {
                var v1 = planet.Sides[Side].GetHeightAt(lastClicks[2].x, lastClicks[2].y); 
                var v2 = planet.Sides[Side].GetHeightAt(lastClicks[3].x, lastClicks[3].y); 
                var ladder = BezierCurve.LadderAuto(
                    lastClicks[0],
                    lastClicks[1], 
                    v1, 
                    v2, 
                    3,
                    6, 
                    Math.PI, true);

                foreach (var kv in ladder)
                {
                    planet.Sides[Side].SetHeightAt(kv.Key.x, kv.Key.y, (ushort)kv.Value.GetAverage()); 
                }

                PhotoshopView.GetLayer("Height").Refresh();
                lastClicks.Clear();
            }
            //heightMapSettings.clickX = (int)scaled.X;
            //heightMapSettings.clickY = (int)scaled.Y;
            */
        }

        private HeightMapSettings heightMapSettings = new HeightMapSettings();

        class HeightMapSettings
        {
            public int clickX;
            public int clickY;

            public static float POW = 0;
            public static float Extra;
            public static float Deep = 3000;
            public static bool CopyHeightMap = true;

            public Pic mask1;
            public Pic mask2;
            private ushort[] OriginalHeightMap;

            private PlanetEditor2 planetEditor2;

            public void Init(PlanetEditor2 planetEditor2, ushort[] originalHeightMap)
            {
                mask1 = new Pic($"{Path}\\Generator\\merge-1.png");
                mask2 = new Pic($"{Path}\\Generator\\merge-2.png");

                this.OriginalHeightMap = originalHeightMap;
                this.planetEditor2 = planetEditor2;
            }

            public static double Interpolate(double v)
            {
                return Math.Pow(v, POW) + Extra;
            }

            public void PrepareLayers(out ushort[,] layer1, out ushort[,] layer2)
            {
                var y2 = clickX;
                var x2 = clickY;

                var side = planetEditor2.planet.Sides[planetEditor2.Side];
                layer1 = Extract(side.heightmap, side.heightmapW, side.heightmapW, x2, y2, x2 + 128, y2 + 128);
                layer2 = new ushort[layer1.GetLength(0), layer1.GetLength(1)];
                var deep = 3000;
                for (int x = 0; x < mask1.w; x++)
                {
                    for (int y = 0; y < mask1.h; y++)
                    {
                        if (mask1.IsColor(x, y, 255, 255, 255)) layer1[x, y] = 0; //Apply mask;
                        if (mask2.IsColor(x, y, 255, 255, 255)) layer2[x, y] = 0; //Apply mask;
                        else layer2[x, y] = (ushort) (layer1[x, y] - deep);
                    }
                }
            }

            public ushort[,] Apply(ushort[,] layer1, ushort[,] layer2)
            {
                var lca = new LakeCreatorAlgorithm();
                LakeProvider provider = new LakeProvider(mask1, mask2);
                lca.Init(provider);
                var applyer = new LakeApplyUShort(layer1, layer2);
                var result = applyer.Apply(provider, lca.GetMergeInfo(), Interpolate);
                return result;
            }

            public void Apply()
            {
                PrepareLayers(out var layer1, out var layer2);
                var result = Apply(layer1, layer2);

                var y2 = clickX;
                var x2 = clickY;
                var side = planetEditor2.planet.Sides[planetEditor2.Side];
                side.heightmap.Merge(result, side.heightmapW, side.heightmapW, x2, y2, x2 + 128, y2 + 128,
                    (oldV, newV) => newV == 0 ? oldV : newV);
                planetEditor2.hView?.Refresh();
                planetEditor2.wView?.Refresh();

                var preview = side.heightmap
                    .ConvertTo2Dim(side.heightmapW, side.heightmapW)
                    .Cut(x2 - 256, y2 - 256, x2 + 128 + 256, y2 + 128 + 256);

                planetEditor2.View3D.Draw(preview, 500, null); //new ushort[200,200]
                planetEditor2.View3D.Visibility = Visibility.Visible;
            }
        }

        private HeightMapLayer hView;
        private HeightMapLayer wView;

        private CurveEditorTool Tool;
        
        public void Show(PlanetSide ps)
        {
            var mainView = new PlanetSideLayer(planet, ps, ps.Name, ()=>ps.material.data.ToBitmap(), autoLoad: true);
            var rView = new SingleColorLayer(ps, Pic.R, "R", ()=>ps.material.RemapColor(Pic.R, ColorMapping.RDict).ToBitmap(), autoLoad: true);
            var gView = new SingleColorLayer(ps, Pic.G, "G", ()=>ps.material.RemapColor(Pic.G, ColorMapping.GDict).ToBitmap(), autoLoad: true);
            var bView = new SingleColorLayer(ps, Pic.B, "B", ()=>ps.material.RemapColor(Pic.B, ColorMapping.BDict).ToBitmap(), autoLoad: true);
            ////var bView2 = new SingleColorLayer(ps, Pic.B, "B2", ()=>ps.material.RemapColor(Pic.B, ColorMapping.BDict).ToBitmap(alphaChannel:true), autoLoad: true);
            hView = new HeightMapLayer(planet, ps, "Height", () => ps.GetHeightMapAsBitmap(), autoLoad:true);
            wView = new HeightMapLayer(planet, ps, "Water", () => ps.GetHeightMapAsWaterLevel(8000, 8500, 9000, 9500), autoLoad:true);

            PhotoshopView.AddLayer(mainView);
            PhotoshopView.AddLayer(rView, mainView);
            PhotoshopView.AddLayer(gView, mainView);
            PhotoshopView.AddLayer(bView, mainView);
            ////PhotoshopView.AddLayer(bView2, mainView);
            PhotoshopView.AddLayer(hView, mainView);
            PhotoshopView.AddLayer(wView, mainView);

            Tool = new CurveEditorTool(PhotoshopView, hView);
            PhotoshopView.AddTool(Tool);
            PhotoshopView.SelectTool(Tool);
        }

        private void DoPlanet(object sender, RoutedEventArgs e)
        {
            planet.DoSides(planet.Definition);
            
            planet.Calculate(Pic.B);
        }

        private void ExportHeightmap(object sender, RoutedEventArgs e)
        {
            var side = planet.Sides[Side];
            //var copy = new short[side.heightmap.Length];
            //for (int i = 0; i < side.heightmap.Length; i++)
            //{
            //    var v = side.heightmap[i];
            //    copy[i] = (short) (((int) v));// - ushort.MaxValue/2);
            //}
            SavePixels($"{Path}\\nebula_mod_pack\\Data\\PlanetDataFiles\\Medieval\\{Side}-c.png", side.heightmap);
            SavePixels($"D:\\SteamLibrary\\steamapps\\workshop\\content\\244850\\2636128625\\Data\\PlanetDataFiles\\Trelan\\{Side}-c.png", side.heightmap);
        }
        
        public static void SavePixels(string output, ushort[] pixels)
        {
            using (MagickImage image = new MagickImage($"{Path}\\Generator\\example.png"))
            {
                var pp = image.GetPixels();
                pp.SetArea(0,0, image.Width, image.Height, pixels);
                using (var fs = new FileStream(output, FileMode.Create))
                {
                    image.Write(fs);
                }
            }
        }

        private void MyGrid_OnKeyDown(object sender, KeyEventArgs e)
        {
            View3D.InnerKeyDown(e);
        }

        private void MyGrid_OnKeyUp(object sender, KeyEventArgs e)
        {
            View3D.InnerKeyDown(e);
        }
        
        
        
        
        private void Undo_OnClick(object sender, RoutedEventArgs e)
        {
			
        }

        private void PowSlider_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
		    	
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            View3DContainer.Visibility = Visibility.Hidden;
            View3D.Draw(new ushort[2,2], 10f, null); //free
        }

        private void ApplyTool(object sender, RoutedEventArgs e)
        {
            var start = Tool.Logic.GetStart();
            var end = Tool.Logic.GetEnd();

            var h1 = planet.Sides[Side].GetHeightAt(start.x, start.y);
            var h2 = planet.Sides[Side].GetHeightAt(end.x, end.y);

            var h = planet.Sides[Side].heightmap;
            Tool.Logic.Apply((x,y,v,smoothing) =>
            {
                var vv = h1+(double)(h2 - h1) * v;
                if (smoothing > 0)
                {
                    var original = (int)planet.Sides[Side].GetHeightAt(x, y);
                    vv = (vv + original * smoothing) / (1d+smoothing);
                }
                planet.Sides[Side].SetHeightAt(x, y, (ushort)vv); 
            }, (x, y, v) =>
            {
                if (v<=0) return;
                
                
            });
        }
    }
}