﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Clicker;
using Clicker.POE;
using GemCraft2;
using GemCraft2.Mine;
using RestSharp;
using Clipboard = System.Windows.Clipboard;
using Color = System.Drawing.Color;
using Control = System.Windows.Controls.Control;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using Point = System.Windows.Point;

namespace BlockEditor.Layers
{
    public class SelectionLayer : Layer
    {
        public SelectionLayer()
        {
            var view = new AreaSelection();
            var b = new BitmapArea(null, new Rectangle(10, 10, 50, 50), "Tst");
            view.Apply(b, 1, true);
            view.Visibility = Visibility.Visible;
            view.Margin = new Thickness(50, 50, 0,0);
            view.Width = 50;
            view.Height = 50;
                
            Init("Tst", view);
        }
    }
}