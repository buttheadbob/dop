﻿using System;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using BlockEditor.Images;
using BlockEditor.PlanetData;
using BlockEditor.Serialization;
using Clicker;
using PlanetGenerator2.MapData;
using Image = System.Windows.Controls.Image;

namespace BlockEditor.Layers
{
    public class ImageLayer : Layer
    {
        private Func<Bitmap> GetBitmap = null;

        public Bitmap Bitmap;
        
        public event Action<Bitmap> OnBitmapLoaded;

        public void GetWidth()
        {
            
        }

        public void GetHeight()
        {
            
        }
        
        public ImageLayer(string name, Bitmap bitmap, bool visible = true)
        {
            Init(name, GetView (visible, bitmap));
            Bitmap = bitmap;
            GroupControl.Visibility = visible ? Visibility.Visible : Visibility.Hidden;
        }
        
        public ImageLayer(string name, Func<Bitmap> bitmap, bool autoLoad = false)
        {
            Visible = false;
            GetBitmap = bitmap;
            Init(name, GetView (false, null));
            GroupControl.Visibility = Visibility.Hidden;
            if (autoLoad)
            {
                LoadImage();
            }
        }

        private Image GetView(bool visible = true, Bitmap bitmap = null)
        {
            var Image = new Image();
            Image.VerticalAlignment = VerticalAlignment.Stretch;
            Image.HorizontalAlignment = HorizontalAlignment.Stretch;
            Image.Width = bitmap?.Width ?? 2048;
            Image.Height = bitmap?.Height ?? 2048;
            Image.Source = bitmap?.ToImageSource();
            return Image;
        }

        public override void OnZoomChanged(double from, double to)
        {
            base.OnZoomChanged(from, to);
            var source = (Control as Image).Source as BitmapImage;
            if (source == null) return;
            var w = source.PixelWidth * to;
            var h = source.PixelHeight * to;
            GroupControl.Width = w;
            GroupControl.Height = h;
            Control.Width = w;
            Control.Height = h;
        }
        
        public override void Refresh()
        {
            LoadImage();
        }

        void LoadImage()
        {
            GUI.InvokeParallel(() =>
            {
                try
                {
                    //var bitmap = ;
                    GUI.InvokeGui(() =>
                    {
                        Bitmap = GetBitmap();
                        var Image = (Control as Image);
                        Image.Width = (int)(Bitmap.Width * Zoom);
                        Image.Height = (int)(Bitmap.Height * Zoom);
                        Image.Source = Bitmap.ToImageSource();
                        Image.Margin = new Thickness(0, 0, 0, 0);
                        Image.HorizontalAlignment = HorizontalAlignment.Left;
                        Image.VerticalAlignment = VerticalAlignment.Top;
                        OnBitmapLoaded?.Invoke(Bitmap);
                        Visible = true;
                    });
                }
                catch (Exception e)
                {
                    var wtf = 0;
                    wtf++;
                }

                
            });

            GUI.Execute();
        }

        public override void OnVisibilityChanged()
        {
            base.OnVisibilityChanged();
            if (GroupControl.Visibility == Visibility.Visible && (Control as Image).Source == null)
            {
                LoadImage();
            }
        }
    }
}