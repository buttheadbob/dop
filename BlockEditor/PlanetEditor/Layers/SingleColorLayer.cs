﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using BlockEditor.PlanetData;

namespace BlockEditor.Layers
{
    public class SingleColorLayer : ImageLayer
    {
        private byte Color;
        private PlanetSide PlanetSide;
        public SingleColorLayer(PlanetSide ps, byte color, string name, Bitmap bitmap, bool autoLoad = false) : base(name, bitmap, autoLoad)
        {
            this.Color = color;
            this.PlanetSide = ps;
        }

        public SingleColorLayer(PlanetSide ps, byte color, string name, Func<Bitmap> bitmap, bool autoLoad = false) : base(name, bitmap, autoLoad)
        {
            this.Color = color;
            this.PlanetSide = ps;
        }

        public override void GenerateMenu(ContextMenu menu)
        {
            base.GenerateMenu(menu);
            GlobalEditor.AddMenu(menu, new List<object>() { this }, "Calculate", Calculate);
            
        }

        private void Calculate(List<object> selected)
        {
            var d = PlanetSide.material.Calculate(Color);
            var m = PlanetEditor2.Instance.ColorMapping.GetMapping(Color);
            List<string> texts = new List<string>();
            foreach (var kv in d)
            {
                var colorMapping = m.GetValueOrDefault(kv.Key, null);
                var name = colorMapping?.Name ?? $"Unknown[{kv.Key}]";
                var volume = kv.Value * colorMapping?.Depth ?? 1;
                texts.Add($"{name}: {kv.Value} {volume}");
            }
            
            texts.Sort();

            MessageBox.Show(String.Join("\r\n", texts));
        }
    }
}