﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using BlockEditor.Images;
using BlockEditor.PlanetData;
using Clicker;
using PlanetGenerator2.MapData;

namespace BlockEditor.Layers
{
    public class DistortionLayer : Layer
    {
        private Pic Original;
        public DistortionLayer(string originalPath)
        {
            Original = new Pic(originalPath);
            var fileName = Path.GetFileName(originalPath);
            var Image = new Image();
            Image.VerticalAlignment = VerticalAlignment.Stretch;
            Image.HorizontalAlignment = HorizontalAlignment.Stretch;
            Image.Source = Original.ToBitmap().ToImageSource();
            Image.Width = 1920;
            Image.Height = 1080;
            Image.Source = Original.ToBitmap().ToImageSource();
                
            Init(fileName, Image);
        }

        public void Distortion()
        {
            Distortion d = new Distortion();
            Pic outpic = Original;
            for (var x = 0; x <= (int) 3; x++)
            {
                outpic = d.Distort(outpic, Pic.R);
            }
                
            (Control as Image).Source = Original.ToBitmap().ToImageSource();
        }
    }
}