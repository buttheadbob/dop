﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using BlockEditor.PlanetData;

namespace BlockEditor.Layers
{
    public class WaterLayer : ImageLayer
    {
        private PlanetSide PlanetSide;
        private Planet Planet;

        public WaterLayer(Planet pl, PlanetSide ps, string name, Func<Bitmap> bitmap, bool autoLoad = false) : base(name, bitmap, autoLoad)
        {
            this.PlanetSide = ps;
            this.Planet = pl;
        }
    }
}