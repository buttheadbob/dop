﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using BlockEditor.PlanetData;

namespace BlockEditor.Layers
{
    public class HeightMapLayer : ImageLayer
    {
        private PlanetSide PlanetSide;
        private Planet Planet;

        public HeightMapLayer(Planet pl, PlanetSide ps, string name, Func<Bitmap> bitmap, bool autoLoad = false) : base(name, bitmap, autoLoad)
        {
            this.PlanetSide = ps;
            this.Planet = pl;
        }

        public override void GenerateMenu(ContextMenu menu)
        {
            base.GenerateMenu(menu);
            GlobalEditor.AddMenu(menu, new List<object>() { this }, "Generate", Generate);
        }

        private void Generate(List<object> selected)
        {
            Refresh();
        }
    }
}