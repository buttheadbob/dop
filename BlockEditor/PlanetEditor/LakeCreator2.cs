﻿using System;
using System.Collections.Generic;
using BlockEditor.PlanetData;
using GemCraft2;
using Slime.Geom;
using VRageMath;
using Color = System.Drawing.Color;

namespace BlockEditor
{
    public class LakeCreator
    {
        public List<Pic> Layers = new List<Pic>();

        public List<P> borders1 = new List<P>();
        public List<P> borders2 = new List<P>();
        
        public List<P> allPixels1 = new List<P>();
        public List<P> allPixels2 = new List<P>();
        private List<Belong> belongs;
        class Belong
        {
            public P Center;
            public List<KeyValuePair<P,double>> Layer1Pixels = new List<KeyValuePair<P,double>>();
            public List<KeyValuePair<P,double>> Layer2Pixels = new List<KeyValuePair<P,double>>();
        }

        

        public void Init(string path, string path2)
        {
            Layers.Add(new Pic(path));
            Layers.Add(new Pic(path2));

            var merged = new Pic(Layers[0].w, Layers[0].h, 3);
            Merge2Pic(merged, Layers[0], Layers[1]);
            Layers.Add(merged);

            P center = null;
            for (int x = 0; x < Layers[1].w; x += 3)
            {
                for (int y = 0; y < Layers[1].h; y += 3)
                {
                    if (!Layers[1].IsColor(x, y, 255, 255, 255))
                    {
                        center = new P(x, x);
                    }
                }
            }
            
            
            GlyphFinder gf1 = new GlyphFinder();
            gf1.Init(new PicSimilarColor(Layers[0], 1), new FourSides());
            gf1.StartWave(center);

            GlyphFinder gf2 = new GlyphFinder();
            gf2.Init(new PicSimilarColor(Layers[1], 1), new FourSides());
            gf2.StartWave(center);

            borders1 = gf1.GetBorder();
            borders2 = gf2.GetBorder();
            allPixels1 = gf1.GetAllPixels();
            allPixels2 = gf2.GetAllPixels();

            foreach (var l in borders1)
            {
                Layers[2].SetColor(l.x, l.y, Color.Black);
            }
                
            foreach (var l in borders2)
            {
                Layers[2].SetColor(l.x, l.y, Color.Black);
            }

            Layers.Add(new Pic(Layers[0].w, Layers[0].h, 3));
            Layers.Add(new Pic(Layers[0].w, Layers[0].h, 3));
        }

        private void InitBelongs()
        {
            belongs = new List<Belong>();
            var colorIds = Layers[1].FindColors(255, 0, 0);
            foreach (var id in colorIds)
            {
                Layers[1].Idx(id, out var xx, out var yy, out var off);
                var b = new Belong();
                b.Center = new P(xx, yy);
                belongs.Add(b);
            }
            
            foreach (var p in borders2)
            {
                List<double> distances = new List<double>();
                var sum = 0d;
                foreach (var b in belongs)
                {
                    var d = b.Center.Distance(p);
                    distances.Add(d);
                    sum += d;
                }
                

                for (int i = 0; i < belongs.Count; i++)
                {
                    var v = interpolate(distances[i] / sum);
                    if (v > 0)
                    {
                        belongs[i].Layer1Pixels.Add(new KeyValuePair<P, double>(p, v));
                    }
                    
                }
            }
        }

        private Func<double, double> interpolate = (d) => d;

        private static void Merge2Pic(Pic merged, Pic p1, Pic p2)
        {
            for (int x = 0; x < merged.w; x++)
            {
                for (int y = 0; y < merged.h; y++)
                {
                    var r2 = p2.GetR(x,y);
                    var g2 = p2.GetG(x,y);
                    var b2 = p2.GetB(x,y);

                    if (r2 != 255 && g2 != 255 && b2 != 255)
                    {
                        merged.SetColor(x,y,r2,g2,b2);
                    }
                    else
                    {
                        var r = p1.GetR(x,y);
                        var g = p1.GetG(x,y);
                        var b = p1.GetB(x,y);
                        merged.SetColor(x,y,r,g,b);
                    }
                }   
            }
        }
        

        public void MergeSmoozie()
        {
            Layers[3].FillAll(0,0,0);
            Layers[4].FillAll(0,0,0);
            Dictionary<P, List<PixelLine>> affected = new Dictionary<P, List<PixelLine>>();
            foreach (var c in borders2)
            {
                MergeSmoozie(c, affected);
            }

            foreach (var p in allPixels2)
            {
                affected.Remove(p);
            }
        }
        
        public void MergeSmoozie(P e2, Dictionary<P, List<PixelLine>> affected, int cx = 68, int cy = 68)
        {
            var angle = Math.Atan2(e2.x - cx, e2.x - cy);
            var min = angle - Math.PI / 20;
            var max = angle + Math.PI / 20;

            var edgeList1 = new List<P>();
            var edgeList2 = new List<P>();
            
            foreach (var c in borders2)
            {
                if (Math.Atan2(c.x - cx, c.y - cy).IsBetween (min, max))
                {
                    edgeList2.Add(c);
                    //Layers[3].SetColor(c.x, c.y, Color.Blue);
                }
            }

            foreach (var c in borders1)
            {
                if (Math.Atan2(c.x - cx, c.y - cy).IsBetween(min, max))
                {
                    edgeList1.Add(c);
                    //Layers[3].SetColor(c.x, c.y, Color.Yellow);
                }
            }
            
            var filtered = new List<P>();
            foreach (var c in allPixels1)
            {
                if (Math.Atan2(c.x - cx, c.y - cy).IsBetween(min, max))
                {
                    filtered.Add(c);
                    //Layers[3].SetColor(c.x, c.y, Color.Coral);
                }
            }
            
            var lines = new List<PixelLine>();
            foreach (var e1 in edgeList1)
            {
                var pl = PixelLine.Find(filtered, e2, e1);
                lines.Add(pl);
                foreach (var p in pl.pixels)
                {
                    affected.GetOrNew(p).Add(pl);
                }
            }
        }

        public void ShowInfo(int x, int y, int cx = 68, int cy = 68)
        {
            Layers[3].FillAll(0,0,0);
            Layers[4].FillAll(0,0,0);
            
            Layers[3].SetColor(x, y, Color.Green);

            var angle = Math.Atan2(x - cx, y - cy);
            var min = angle - Math.PI / 8;
            var max = angle + Math.PI / 8;

            var edgeList1 = new List<P>();
            var edgeList2 = new List<P>();
            
            foreach (var c in borders2)
            {
                if (Math.Atan2(c.x - cx, c.y - cy).IsBetween (min, max))
                {
                    edgeList2.Add(c);
                    Layers[3].SetColor(c.x, c.y, Color.Blue);
                }
            }

            
            foreach (var c in borders1)
            {
                if (Math.Atan2(c.x - cx, c.y - cy).IsBetween(min, max))
                {
                    edgeList1.Add(c);
                    Layers[3].SetColor(c.x, c.y, Color.Yellow);
                }
            }


            var filtered = new List<P>();
            foreach (var c in allPixels1)
            {
                if (Math.Atan2(c.x - cx, c.y - cy).IsBetween(min, max))
                {
                    filtered.Add(c);
                    Layers[3].SetColor(c.x, c.y, Color.Coral);
                }
            }

            var rand = new Random();
            var lines = new List<PixelLine>();
            Dictionary<P, List<PixelLine>> affected = new Dictionary<P, List<PixelLine>>();
            foreach (var e2 in edgeList2)
            {
                foreach (var e1 in edgeList1)
                {
                    var pl = PixelLine.Find(filtered, e2, e1);
                    lines.Add(pl);
                    foreach (var p in pl.pixels)
                    {
                        affected.GetOrNew(p).Add(pl);
                    }
                }
            }

            lines.Shuffle(rand);
            foreach (var pl in lines)
            {
                var r = (byte) (50 + rand.Next(128));
                var g = (byte) (50 + rand.Next(128));
                var b = (byte) (50 + rand.Next(128));
                foreach (var c in pl.pixels)
                {
                    Layers[4].SetColor(c.x, c.y, r,g,b);
                }
                break;
            }
        }

    }
}