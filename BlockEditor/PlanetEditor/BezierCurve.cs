using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using BlockEditor;
using BlockEditor.PlanetData;
using GemCraft2;
using Slime.Geom;
using VRageMath;
using Line = Slime.Geom.Line;
using Point = System.Drawing.Point;

namespace Curves
{
    public class LadderCanMove : PointChecker
    {
        public Dictionary<P, List<double>> Ladder;
        private int sx,sy,ex,ey;
        public LadderCanMove(Dictionary<P, List<double>> ladder)
        {
            Ladder = ladder;

            var minX = int.MaxValue;
            var maxX = -1;
            var minY = int.MaxValue;
            var maxY = -1;

            foreach (var kv in ladder)
            {
                minX = Math.Min(minX, kv.Key.x);
                maxX = Math.Max(maxX, kv.Key.x);
                minY = Math.Min(minY, kv.Key.y);
                maxY = Math.Max(maxY, kv.Key.y);
            }

            sx = minX;
            sy = minY;
            ex = maxX;
            ey = maxY;
        }

        public void Startwave(GlyphFinder finder, int x, int y) { }
        public bool CanMove(P from, P to) { return Ladder.ContainsKey(to); }
        public int GetWidth() { return ex+1; }// - sx;
        public int GetHeight() { return ey+1; }// - sy;
    }
    
    class BezierCurve
    {

        private static void AddCircle(P offset, int sphereEndRadius, double value, Dictionary<P, List<double>> dict)
        {
            for (var x = -sphereEndRadius; x < sphereEndRadius; x++)
            {
                for (var y = -sphereEndRadius; y < sphereEndRadius; y++)
                {
                    if (P.Distance(x, y, 0, 0) < sphereEndRadius)
                    {
                        dict.GetOrNew(new P(offset.x + x, offset.y + y)).Add(value);
                    }
                }
            }
        }
        
        
        
        private static PixelLine DrawLine(P start, P end, double startV, double endV, int ladderWidth, Dictionary<P, List<double>> values, List<P> pixels = null)
        {
            var minX = Math.Min(end.x, start.x) - ladderWidth - 5;
            var maxX = Math.Max(end.x, start.x) + ladderWidth + 5;
            var minY = Math.Min(end.y, start.y) - ladderWidth - 5;
            var maxY = Math.Max(end.y, start.y) + ladderWidth + 5;
            
            if (pixels == null)
            {
                pixels = new List<P>((maxX-minX)*(maxY-minY));
            }
            else
            {
                pixels.Clear();
            }
            
            for (int x = minX; x < maxX ; x++)
            {
                for (int y = minY; y < maxY ; y++)
                {
                    pixels.Add(new P(x,y));
                }
            }
            
            
            var line = PixelLine.Find(pixels, start, end, ladderWidth, true);
            var startP = new Vector2(start.x, start.y);


            var lineLength = start.Distance(end);
            foreach (var p in line.pixels)
            {
                var linepoint = Line.GetClosestPoint(start, end, p, out _);
                var distance = (linepoint - startP).Length();

                var newValue = startV + (endV - startV) / lineLength * distance;
                values.GetOrNew(p).Add(newValue);
            }

            return line;
        }

        private static void Turn(P start, P firstStepEnd, double degree, float distance)
        {
            var vector3 = new Vector3 (firstStepEnd.x - start.x, firstStepEnd.y - start.y, 0);
            var newVector = Vector3.RotateAndScale(vector3, Matrix.CreateRotationZ((float) degree));
            newVector.Normalize();
            newVector *= distance;
            
            start.x = firstStepEnd.x;
            start.y = firstStepEnd.y;
            firstStepEnd.x = (int)newVector.X + start.x;
            firstStepEnd.y = (int)newVector.Y + start.y;
        }

        private static void DrawLine2(P start, P end, double startV, double endV, int ladderWidth, Dictionary<P, List<double>> values, List<P> pixels)
        {
            var t = new Vector2(end.x - start.x, end.y - start.y);
            var mid1 = new P(start.x + (int) (t.X * 0.1), start.y + (int) (t.Y * 0.1));
            var mid2 = new P(start.x + (int) (t.X * 0.9), start.y + (int) (t.Y * 0.9));

                
            DrawLine(start, mid1, startV, startV, ladderWidth, values, pixels);
            DrawLine(mid1, mid2, startV, endV, ladderWidth, values, pixels);
            DrawLine(mid2, end, endV, endV, ladderWidth, values, pixels);
        }
        public static Dictionary<P, List<double>> Ladder(P[] points, double[] values, int ladderWidth)
        {
            var dict = new Dictionary<P, List<double>>();
            var pixels = new List<P>();

            for (int i = 0; i < points.Length-1; i++)
            {
                DrawLine2(points[i],points[i+1], values[i], values[i+1], ladderWidth, dict, pixels);
            }

            //for (int i = 0; i < points.Length-1; i++)
            //{
            //    AddCircle(points[i], (int)(ladderWidth*1.5d), values[i], dict);
            //}
            
            return dict;

        }


        public static void TurnSmooth(P startP, P endP, double startValue, double endValue, int ladderWidth, double degree, Dictionary<P, List<double>> dict)
        {
            var maxDegree = Math.PI / 12; //15*

            var times = (int)Math.Abs(degree / maxDegree)+1;
            var turnDegree = degree / times;

    
            for (int i = 0; i < times; i++)
            {
                Turn(startP, endP, turnDegree, 2);
                DrawLine(startP, endP, startValue, endValue, ladderWidth, dict);
            }
        }
        
        public static Dictionary<P, List<double>> LadderAuto(P pointStart, P pointEnd, double startValue, double endValue, int ladderWidth, int ladders, double degree, bool negateDegree)
        {
            var dict = new Dictionary<P, List<double>>();
            var pixels = new List<P>();

            var ladderLength = pointStart.Distance(pointEnd);
            var values = new double[ladders * 3];
            var points = new P[ladders*2];
            var startP = pointStart.Copy();
            var endP = pointEnd.Copy();
            var dv = (endValue - startValue)/ladders;

            for (int i = 0; i < ladders; i++)
            {
                points[i * 2 + 0] = startP;
                points[i * 2 + 1] = endP;
                
                values[i * 2 + 0] = startValue + dv * i;
                values[i * 2 + 1] = startValue + dv * (i+1);
                
                
                startP = startP.Copy();
                endP = endP.Copy();
                
                TurnSmooth(
                    startP, 
                    endP, 
                    values[i * 2 + 1], 
                    values[i * 2 + 1], 
                    ladderWidth, degree, dict);
                
                startP = startP.Copy();
                endP = endP.Copy();

                Turn(startP, endP, 0, (float)ladderLength);
                //startP = startP.Copy();
                //endP = endP.Copy();
                //Turn(startP, endP, degree / 2, ladderWidth * 1.8f);//);
                //
                //Turn(startP, endP, degree / 2, ladderWidth * 1.8f);
                
                

                if (negateDegree) degree = -degree;
            }

            for (int i = 0; i < points.Length-1; i+=2)
            {
                //DrawLine2(points[i],points[i+1], values[i], values[i+1], ladderWidth, dict, pixels);
            }
            
            //for (int i = 1; i < points.Length-1; i+=2)
            //{
            //    DrawLine(points[i],points[i+1], values[i], values[i+1], ladderWidth, dict, pixels);
            //}
            
            //for (int i = 0; i < points.Length-1; i++)
            //{
            //    AddCircle(points[i], (int)(ladderWidth*1.5d), values[i], dict);
            //}
            
            return dict;

        }
        
        public static void Bezier(int n, Point P1, Point P2, Point P3, List<P> list)
        {
            if (n > 0)
            {
                Point P12 = new Point((P1.X + P2.X) / 2, (P1.Y + P2.Y) / 2);
                Point P23 = new Point((P2.X + P3.X) / 2, (P2.Y + P3.Y) / 2);
                Point P123 = new Point((P12.X + P23.X) / 2, (P12.Y + P23.Y) / 2);

                Bezier(n - 1, P1, P12, P123, list);
                Bezier(n - 1, P123, P23, P3, list);
            }
            else
            {
                list.Add(new P(P1.X, P1.Y));
                list.Add(new P(P2.X, P2.Y));
                
                list.Add(new P(P2.X, P2.Y));
                list.Add(new P(P3.X, P3.Y));
            }
        }
    }
}
