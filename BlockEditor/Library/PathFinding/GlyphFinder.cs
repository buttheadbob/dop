﻿using System;
using System.Collections.Generic;
using System.Drawing;
using BlockEditor.PlanetData;
using GemCraft2.Mine;
using Slime.Geom;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

namespace GemCraft2
{
    public class Any : PointChecker
    {
        private int w;
        private int h;
        
        public Any(int w, int h)
        {
            this.w = w;
            this.h = h;
        }
        
        public void Startwave(GlyphFinder finder, int x, int y) { }
        public bool CanMove(P @from, P to) { return true; }
        public int GetWidth() { return w; }
        public int GetHeight() { return h; }
    }
    public class UshortNotZero : PointChecker
    {
        private ushort[,] Data;
        public UshortNotZero(ushort[,] data)
        {
            this.Data = data;
        }
        public void Startwave(GlyphFinder finder, int x, int y) {}
        public bool CanMove(P from, P to)
        {
            return Data[to.x, to.y] != 0;
        }

        public int GetWidth()
        {
            return Data.GetLength(0);
        }

        public int GetHeight()
        {
            return Data.GetLength(1);
        }
    }
    
    public class PicSimilarColor : PointChecker
    {
        private GlyphFinder finder;
        private Pic Data;
        
        private int sensitivity;

        public PicSimilarColor(Pic data, int sensitivity = 1)
        {
            this.Data = data;
            this.sensitivity = sensitivity;
        }
        
        public void Startwave(GlyphFinder finder, int x, int y)
        {
            this.finder = finder;
        }

        public bool CanMove(P from, P to)
        {
            return ColorUtils.AreSame(
                Data.Idx(from.x, from.y), 
                Data.Idx(to.x, to.y), 
                Data.data, 
                Data.data, 
                sensitivity);
        }

        public int GetWidth()
        {
            return Data.w;
        }

        public int GetHeight()
        {
            return Data.h;
        }
    }


    public class PicChecker : PointChecker
    {
        private Pic Data;
        private Func<P,P, bool> Func;
        public PicChecker (Pic data, Func<P,P, bool> func)
        {
            this.Data = data;
            this.Func = func;
        }
        public void Startwave(GlyphFinder finder, int x, int y) {}
        public bool CanMove(P @from, P to)
        {
            return Func(from, to);
        }

        public int GetWidth(){ return Data.w; }
        public int GetHeight() { return Data.h; }
    }
    
    public class NotExactColor : PointChecker
    {
        private GlyphFinder finder;
        private Pic Data;
        
        private int r;
        private int g;
        private int b;

        public NotExactColor(Pic data, int r, int g, int b)
        {
            this.Data = data;
            this.r = r;
            this.g = g;
            this.b = b;
        }
        
        public void Startwave(GlyphFinder finder, int x, int y)
        {
            this.finder = finder;
        }

        public bool CanMove(P from, P to)
        {
            return !Data.IsColor(to.x, to.y, r, g, b);
        }

        public int GetWidth()
        {
            return Data.w;
        }

        public int GetHeight()
        {
            return Data.h;
        }
    }

    public interface PointChecker
    {
        void Startwave(GlyphFinder finder, int x, int y);
        bool CanMove(P from, P to);
        int GetWidth();
        int GetHeight();
    }
    
    public interface WaveGenerator
    {
        void Generate(GlyphFinder finder, P from, int frame);
    }

    public class FourSides : WaveGenerator
    {
        public void Generate(GlyphFinder finder, P from, int frame)
        {
            finder.Test(from, 1, 0, frame+1);
            finder.Test(from, -1, 0, frame+1);
            finder.Test(from, 0, 1, frame+1);
            finder.Test(from, 0, -1, frame+1);
        }
    }
    
    public class EightSides : WaveGenerator
    {
        public void Generate(GlyphFinder finder, P from, int frame)
        {
            finder.Test(from, 1, 0, frame+1);
            finder.Test(from, -1, 0, frame+1);
            finder.Test(from, 0, 1, frame+1);
            finder.Test(from, 0, -1, frame+1);
            
            finder.Test(from, 1, 1, frame+1);
            finder.Test(from, 1, -1, frame+1);
            finder.Test(from, -1, 1, frame+1);
            finder.Test(from, -1, -1, frame+1);
        }
    }
    
    public class ManySides : WaveGenerator
    {
        private int sx, sy, ex, ey;

        public ManySides(int sx, int sy, int ex, int ey)
        {
            this.sx = sx;
            this.sy = sy;
            this.ex = ex;
            this.ey = ey;
        }

        public void Generate(GlyphFinder finder, P from, int frame)
        {
            for (int x = sx; x <= ex; x++)
            {
                for (int y = sy; y <= ey; y++)
                {
                    finder.Test(from, x, y, frame+1);
                }
            }
        }
    }
    
    public class EightSidesDiagonalWeight : WaveGenerator
    {
        public void Generate(GlyphFinder finder, P from, int frame)
        {
            finder.Test(from, 1, 0, frame+10);
            finder.Test(from, -1, 0, frame+10);
            finder.Test(from, 0, 1, frame+10);
            finder.Test(from, 0, -1, frame+10);
            
            finder.Test(from, 1, 1, frame+14);
            finder.Test(from, 1, -1, frame+14);
            finder.Test(from, -1, 1, frame+14);
            finder.Test(from, -1, -1, frame+14);
        }
    }
    
    
    
    public class GlyphFinder : WaveAlgorythm
    {
        public int minX=999999, minY=999999;
        public int maxX, maxY;

        public Rectangle bounds;
        private PointChecker canMoveFunc;
        private WaveGenerator generator;
        
        public void Init (PointChecker canMoveFunc, WaveGenerator generator = null, Rectangle? bounds=null)
        {
            this.generator = generator ?? new FourSides();
            this.canMoveFunc = canMoveFunc;

            Init (canMoveFunc.GetWidth(), canMoveFunc.GetHeight());
            
            if (bounds == null)
            {
                this.bounds = new Rectangle(new Point(0,0), new Size(canMoveFunc.GetWidth(), canMoveFunc.GetHeight()));
            }
            else
            {
                this.bounds = bounds.Value;
            }
        }

        public override void StartWave(P point)
        {
            point = point.Copy();
            canMoveFunc.Startwave(this, point.x, point.y);
            base.StartWave(point);
            
        }

        protected override void GenerateWave(P p, int frame)
        {
            generator.Generate(this, p, frame);
        }

        public void Test(P p, int dx, int dy, int wouldBeAtFrame)
        {
            if (p.y + dy >= bounds.Top && p.y + dy < bounds.Bottom && p.x + dx >= bounds.Left && p.x + dx < bounds.Right)
            {
                var to = new P(p.x + dx,p.y + dy);
                if (!CanVisit(p, to, wouldBeAtFrame))
                {
                    return;
                }
                if(CanMove(p, to))
                {
                    AddPoint(to, wouldBeAtFrame);
                }
            }
        }

        public override bool CanMove(P from, P to)
        {
            return canMoveFunc.CanMove(from, to);
        }

        protected override void Visit(P p, int wouldBeAtFrame)
        {
            base.Visit (p, wouldBeAtFrame);


            minX = Math.Min(minX, p.x);
            maxX = Math.Max(maxX, p.x);
            minY = Math.Min(minY, p.y);
            maxY = Math.Max(maxY, p.y);
        }

        public Rectangle GetRectangle()
        {
            return new Rectangle(minX, minY, maxX - minX, maxY - minY);
        }
        
        public List<P> GetBorder(int dx = 1, int dy = 1)
        {
            List<P> borders = new List<P>();
            for (int x = minX; x <= maxX; x++)
            {
                for (int y = minY; y <= maxY; y++)
                {
                    if (GetValue(x, y) >= 0 && IsBorder (x, y, dx, dy))
                    {
                        borders.Add(new P(x,y));                
                    }
                }
            }

            return borders;
        }
        
        public List<P> GetAllPixels()
        {
            List<P> pixels = new List<P>();
            for (int x = minX; x <= maxX; x++)
            {
                for (int y = minY; y <= maxY; y++)
                {
                    if (GetValue(x, y) >= 0)
                    {
                        pixels.Add(new P(x,y));                
                    }
                }
            }

            return pixels;
        }
        
        public bool IsBorder(int x, int y, int dx = 1, int dy = 1)
        {
            bool isB = false;
            For(x - dx, y - dy, x + dx, y + dy, (xx, yy) =>
            {
                isB |= GetValue(xx, yy) < 0;
            });
            return isB;
        }
    }
}
