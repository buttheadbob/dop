﻿using System;
using BlockEditor.PlanetData;

namespace BlockEditor.Images
{
    public class Distortion
    {
        public Pic Distort(int times, Pic pic, byte layer, Random random = null, bool remove = true, bool add = true)
        {
            for (int i = 0; i < times; i++)
            {
                pic = Distort(pic, layer, random, remove, add);
            }

            return pic;
        }

        public Pic Distort(Pic pic, byte layer, Random random = null, bool remove = true, bool add = true)
        {
            random = random ?? new Random();
            
            
            Pic counts = new Pic(pic.w, pic.h, 1);

            pic.For(0,0,pic.w, pic.h, (idx, x, y) =>
            {
                var d = pic.Get(x,y,layer);

                if (d != 0)
                {
                    counts.For(x - 1, y - 1, x + 1, y + 1, (idx2, x2, y2) =>
                    {
                        counts.Sum(x2, y2, 0, 1);
                        return true;
                    });
                }
                
                return true;
            });
            
            
            Pic output = new Pic(pic);
            output.For(0,0,counts.w, counts.h, (idx, x, y) =>
            {
                var count = counts.Get(x,y,0);
                if (count == 0) return true; //Too far
                
                byte resultColor = pic.Get(x,y,layer);
                if (resultColor != 0)
                {
                    if (remove)
                    {
                        var change = ChanceToRemove(count);
                        resultColor = random.NextDouble() < change ? byte.MinValue : resultColor;
                    }
                }
                else
                {
                    if (add)
                    {
                        var change = ChanceToAdd(count);
                        resultColor = random.NextDouble() < change ? byte.MaxValue : resultColor;
                    }
                }

                output.Set(x,y,layer, resultColor);
                return true;
            });

            return output;
        }
        
        private float ChanceToRemove(int count)
        {
            return 0;
            switch (count)
            {
                case 1: return 0.35f;
                case 2: return 0.30f;
                case 3: return 0.25f;
                case 4: return 0.20f;
                case 5: return 0.15f;
                case 6: return 0.10f;
                case 7: return 0.05f;
                case 8: return 0.05f;
                case 9: return 0.0f;
                default: return 0f;
            }
        }
        
        private float ChanceToAdd(int count)
        {
            switch (count)
            {
                case 1: return 0.05f;
                case 2: return 0.10f;
                case 3: return 0.15f;
                case 4: return 0.20f;
                case 5: return 0.25f;
                case 6: return 0.35f;
                case 7: return 0.45f;
                case 8: return 0.65f;
                case 9: return 0.0f;
                default: return 0f;
            }
        }
    }
}