﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Reflection;
using GemCraft2.Mine;
using PlanetGenerator2.MapData;
using Slime.Geom;

namespace BlockEditor.PlanetData
{
    public class Pic : ByteLayerArray
    {
        public const int VOXEL = 2; 
        public const int FOLIAGE = 1; 
        public const int ORE = 0;
        
        public const int A = 3; 
        public const int R = 2; 
        public const int G = 1; 
        public const int B = 0;
        

        public Pic(string s)
        {
            Init((Bitmap) Bitmap.FromFile(s));
        }
        public Pic(Bitmap bmp)
        {
            Init(bmp);
        }
        
        public Pic(Pic pic)
        {
            w = pic.w;
            h = pic.h;
            data = (byte[])pic.data.Clone();
            layers = pic.layers;
        }

        public static Pic FromHeightMap(ushort[,] data)
        {
            var pic = new Pic(data.GetLength(0),data.GetLength(1), 3);

            for (int x = 0; x < pic.w; x++)
            {
                for (int y = 0; y < pic.h; y++)
                {
                    var v = data[x, y];
                    var vv = (byte) (v / 256);
                    pic.SetColor(x,y, vv,vv,vv);
                }
            }

            return pic;
        }

        public Pic(int w, int h, int layers)
        {
            this.w = w;
            this.h = h;
            this.layers = layers;
            this.data = new byte[w*h*layers];
        }

        private void Init(Bitmap bmp)
        {
            w = bmp.Width;
            h = bmp.Height;
            data = bmp.ToByteArray(PixelFormat.Format24bppRgb);
            layers = 3;
        }

        public void CopyFrom(Bitmap bmp)
        {
            data = bmp.ToByteArray(PixelFormat.Format24bppRgb);
        }

        private static List<Color> AllColors = new List<Color>();

        public static List<Color> GetAllColors()
        {
            if (AllColors.Count == 0)
            {
                var colorFields = typeof(Color).GetProperties(BindingFlags.Static | BindingFlags.Public);
                foreach (var f in colorFields)
                {
                    var c = f.GetValue(null);
                    if (c is Color cc)
                    {
                        AllColors.Add(cc);
                    }
                }
                AllColors.Remove(Color.Transparent);
            }

            return AllColors;
        }
        
        public Pic RemapColor(byte layer, Dictionary<byte, ColorMapping> colorMapping = null, bool alphaChannel = false)
        {
            var h1 = new Dictionary<byte, int>();
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    var a1 = Get(x, y, layer);
                    h1[a1] = h1.GetValueOrDefault(a1, 0) + 1;
                }
            }

            //colorMapping
            var dict = new Dictionary<byte, Color>();
            foreach (var c in colorMapping)
            {
                dict.Add(c.Key, c.Value.Color);
            }

            var colors = GetAllColors();
            colors.Shuffle(new Random());

            int i = 0;
            foreach (var h in h1)
            {
                if (dict.ContainsKey(h.Key)) continue;
                dict[h.Key] = colors[i++];
            }


            var pic = new Pic(this);
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    var color = dict[Get(x, y, layer)];
                    pic.Set(x,y,Pic.R, color.R);
                    pic.Set(x,y,Pic.G, color.G);
                    pic.Set(x,y,Pic.B, color.B);
                }
            }

            return pic;
        }
        
        public byte GetR(int x, int y)
        {
            return data[Idx(x, y) + R];
        }
        public byte GetG(int x, int y)
        {
            return data[Idx(x, y) + G];
        }
        public byte GetB(int x, int y)
        {
            return data[Idx(x, y) + B];
        }
        
        public byte GetVoxel(int x, int y)
        {
            return data[Idx(x,y)+VOXEL];
        }
        
        public byte GetVoxel(int idx)
        {
            return data[idx+VOXEL];
        }

        public void Calculate()
        {
            var h1 = new Dictionary<byte, int>();
            var h2 = new Dictionary<byte, int>();
            var h3 = new Dictionary<byte, int>();

            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    var a1 = Get(x, y, 0);
                    var a2 = Get(x, y, 1);
                    var a3 = Get(x, y, 2);

                    h1[a1] = h1.GetValueOrDefault(a1, 0) + 1;
                    h2[a2] = h2.GetValueOrDefault(a2, 0) + 1;
                    h3[a3] = h3.GetValueOrDefault(a3, 0) + 1;
                }
            }

            var wtf = 0;
        }
        
        
        
        public byte GetFoliage(int x, int y)
        {
            return data[Idx(x,y)+FOLIAGE];
        }
        
        public byte GetFoliage(int idx)
        {
            return data[idx+FOLIAGE];
        }
        
        public byte GetOre(int idx)
        {
            return data[idx+ORE];
        }
        
        public byte GetOre(int x, int y)
        {
            return data[Idx(x,y)+ORE];
        }
        
        
        
        
        
        //====================

        public void SetVoxel(int x, int y, byte v)
        {
            data[Idx(x,y)+VOXEL] = v;
        }
        
        public void SetFoliage(int x, int y, byte v)
        {
            data[Idx(x,y)+FOLIAGE] = v;
        }
        
        public void SetOre(int x, int y, byte v)
        {
            data[Idx(x,y)+ORE] = v;
        }
        
        
        //======================


        public bool Search(int at, byte[] searchFor, byte type, int radius, string Mode, double percentage, bool more)
        {
            
            var hasOne = Mode == "Any";
            var hasAll = Mode == "Multiple";
            var hasntAll = Mode == "None";
            
            //
            //
            //
            //
            //

            HashSet<byte> toFind = hasAll ? new HashSet<byte>() : null;
            
            
            
            Idx(at, out var cx,out var cy, out _);


            var minX = Math.Max(0, cx - radius);
            var maxX = Math.Min(w-1, cx + radius);
            
            var minY = Math.Max(0, cy - radius);
            var maxY = Math.Min(h-1, cy + radius);


            int found = 0;
            int total = (maxX-minX+1) * (maxY-minY+1);
            
            for (var x = minX; x<=maxX; x++)
            {
                for (var y = minY; y<=maxY; y++)
                {
                    var d = Get(x, y, type);
                    var c = searchFor.Contains(d);

                    if (hasntAll)
                    {
                        if (c)
                        {
                            return false;
                        }
                        continue;
                    }

                    if (c)
                    {
                        found++;

                    
                        if (hasAll)
                        {
                            toFind.Remove(d);
                        }

                        if (hasAll && toFind.Count == 0 || !hasAll)
                        {
                            var v = ((double)found / total);
                            if (v >= percentage && found > 0)
                            {
                                return more;
                            }
                        }
                    }
                }
            }

            if (hasAll && toFind.Count == 0 || !hasAll)
            {
                var v2 = ((double)found / total);
                if (v2 >= percentage && found > 0)
                {
                    return more;
                }
                return !more;
            }
            return false;
        }

        public Bitmap ToBitmap(bool alphaChannel = false, Bitmap bitmap = null)
        {
            var bytes = data; 
            if (alphaChannel)
            {
                bytes = new byte[4 * w * h];
                for (int x = 0; x < w; x++)
                {
                    for (int y = 0; y < h; y++)
                    {
                        var idx1 = 3 * (x + y * w);
                        var idx2 = 4 * (x + y * w);
                        if (data[idx1] == 0 && data[idx1 + 1] == 0 && data[idx1 + 2] == 0)
                        {
                            bytes[idx2] = 0;
                        }
                        else
                        {
                            bytes[idx2+0] = data[idx1+0];
                            bytes[idx2+1] = data[idx1+1];
                            bytes[idx2+2] = data[idx1+2];
                            bytes[idx2+3] = 255;
                        }
                    }   
                }
            }

            if (bitmap == null)
            {
                bitmap = new Bitmap(w, h, alphaChannel ? PixelFormat.Format32bppArgb : PixelFormat.Format24bppRgb);
            }
            bitmap.SetPixels(data, alphaChannel);
            return bitmap;
        }

        public static Pic RotateFlip(Pic bmp, RotateFlipType type)
        {
            var bitmap = bmp.ToBitmap();
            bitmap.RotateFlip(type);
            return new Pic(bitmap);
        }


        public void SetColor(int i, byte r, byte g, byte b)
        {
            data[i + R] = r;
            data[i + G] = g;
            data[i + B] = b;
        }
        
        public void SetSomeColor(int i, int r, int g, int b)
        {
            if (r >= 0) data[i + R] = (byte)r;
            if (g >= 0) data[i + G] = (byte)g;
            if (g >= 0) data[i + B] = (byte)b;
        }
        
        public void SetSomeColor(int x, int y, int r, int g, int b)
        {
            SetSomeColor(Idx(x,y), r,g,b);
        }
        
        public void SetColor(P xy, byte r, byte g, byte b)
        {
            SetColor(xy.x, xy.y, r, g, b);
        }
        
        public void SetColor(int x, int y, byte r, byte g, byte b)
        {
            var i = Idx(x, y);
            data[i + R] = r;
            data[i + G] = g;
            data[i + B] = b;
        }
        
        public void SetColor(int x, int y, Color c)
        {
            SetColor(x,y, c.R, c.G, c.B);
        }

        public bool IsColor(int x, int y, int r, int g, int b)
        {
            var i = Idx(x, y);
            return IsColor(i, r,g,b);
        }
        
        public bool IsColor(int idx, int r, int g, int b)
        {
            return (r < 0 || data[idx + R] == r) 
                   && (g < 0 || data[idx + G] == g) 
                   && (b < 0 || data[idx + B] == b);
        }
        
        public List<int> FindColors(int r, int g, int b)
        {
            List<int> ids = new List<int>();
            ForAll((i, x, y) =>
            {
                if (IsColor(i, r, g, b))
                {
                    ids.Add(i);
                }

                return true;
            });

            return ids;
        }

        public int GetColor(int x, int y)
        {
            var idx = Idx(x, y);
            return data[idx + R] + data[idx + G] * 256 + data[idx + B] * 256 * 256;
        }
        
        public void SetColor(int x, int y, int v)
        {
            SetColor(x, y, (byte)(v % 256), (byte)(v / 256 % 256), (byte)(v / 256 / 256));
        }
    }
}