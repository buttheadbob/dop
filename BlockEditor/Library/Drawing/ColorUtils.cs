﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BlockEditor.PlanetData;

namespace GemCraft2.Mine
{
    public static class ColorUtils
    {
        public static Bitmap SetPixels(this Bitmap b, byte[] data, bool alphaChannel = false)
        {
            var BoundsRect = new Rectangle(0, 0, b.Width, b.Height);
            BitmapData bmpData = b.LockBits(BoundsRect,
                ImageLockMode.WriteOnly,
                b.PixelFormat);

            IntPtr ptr = bmpData.Scan0;

            if (bmpData.Stride != b.Width*3)
            {
                if (!alphaChannel)
                {
                    for (int y = 0; y < b.Height; y++)
                    {
                        var ptr2 = IntPtr.Add(ptr, y * bmpData.Stride);
                        Marshal.Copy(data, y*b.Width*3, ptr2, b.Width*3);
                    }
                }
                else
                {
                    Marshal.Copy(data, 0, ptr, data.Length);
                }
            }
            else
            {
                Marshal.Copy(data, 0, ptr, data.Length);
            }
            
            b.UnlockBits(bmpData);
            return b;
        }
        
        public static bool AreSame(int ptr1, int ptr2, byte[] ar1, byte[] ar2, int channel_delta)
        {
            byte r1 = ar1[ptr1 + 2];
            byte r2 = ar2[ptr2 + 2];
            byte g1 = ar1[ptr1 + 1];
            byte g2 = ar2[ptr2 + 1];
            byte b1 = ar1[ptr1 + 0];
            byte b2 = ar2[ptr2 + 0];

            if (Math.Abs(r1 - r2) < channel_delta && Math.Abs(g1 - g2) < channel_delta && Math.Abs(b1 - b2) < channel_delta)
            {
                return true;
            }

            return false;
        }

        public static bool AreSame(int x, int y, byte[] ar1, byte[] ar2, int w, int h, int channel_delta) //
        {
            var offset = Ptr(x, y, w);
            AreSame (offset, offset, ar1, ar2, channel_delta);
            return false;
        }

        public static int Ptr (int x, int y, int w)
        {
            return (x + w * y) * 4;
        }

        public static byte[] GetByteArray(this Bitmap source, byte[] buffer)
        {
            BitmapData bitmapData = null;
            try
            {
                // Get width and height of bitmap
                var Width = source.Width;
                var Height = source.Height;

                // Create rectangle to lock
                Rectangle rect = new Rectangle(0, 0, Width, Height);

                // get source bitmap pixel format size
                var Depth = System.Drawing.Bitmap.GetPixelFormatSize(source.PixelFormat);

                // Check if bpp (Bits Per Pixel) is 8, 24, or 32
                if (Depth != 24 && Depth != 8 && Depth != 32)
                {
                    throw new ArgumentException("Only 8, 24 and 32 bpp images are supported.");
                }

                // Lock bitmap and return bitmap data
                bitmapData = source.LockBits(rect, ImageLockMode.ReadWrite, source.PixelFormat);

                // create byte array to copy pixel values
                var RowSize = bitmapData.Stride < 0 ? -bitmapData.Stride : bitmapData.Stride;
                
                var Pixels = (buffer ==null || buffer.Length < Height * RowSize) ? new byte[Height * RowSize] : buffer;
                var Iptr = bitmapData.Scan0;

                // Copy data from pointer to array
                // Not working for negative Stride see. http://stackoverflow.com/a/10360753/1498252
                //Marshal.Copy(Iptr, Pixels, 0, Pixels.Length);
                // Solution for positive and negative Stride:
                for (int y = 0; y < Height; y++)
                {
                    Marshal.Copy(IntPtr.Add(Iptr, y * bitmapData.Stride), Pixels, y * RowSize, RowSize);
                }
                return Pixels;
            }
            finally
            {
                if (bitmapData != null)
                {
                    source.UnlockBits(bitmapData);
                }
            }
        }
    }
}
