﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Advanced;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.Memory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using VRageMath;

namespace PlanetGenerator2.MapData
{
    public interface IMyImage
    {
        Vector2I Size { get; }
        int Stride { get; }
        int BitsPerPixel { get; }

        object Data { get; }
    }

    public interface IMyImage<TData> : IMyImage where TData : unmanaged
    {
        new TData[] Data { get; }
    }

    public static class MyImage
    {
        public enum FileFormat
        {
            Png,
            Jpg,
            Bmp
        }

        static MyImage()
        {
            Configuration.Default.MemoryAllocator = new SimpleGcMemoryAllocator();
        }

        public static IMyImage Load(Stream stream, bool oneChannel, bool headerOnly = false, string debugName = null)
        {
            var format = SixLabors.ImageSharp.Image.Identify(stream);
            stream.Position = 0;

            if (oneChannel == false)
            {
                var data = format.MetaData.GetFormatMetaData(PngFormat.Instance);
                oneChannel = data.ColorType == PngColorType.Grayscale;
            }

            if (headerOnly)
            {
                if (oneChannel)
                {
                    switch ((PngBitDepth)format.PixelType.BitsPerPixel)
                    {
                        case PngBitDepth.Bit8:
                            return MyImage<byte>.Create<Gray8>(format);

                        case PngBitDepth.Bit16:
                            return MyImage<ushort>.Create<Gray16>(format);
                    }
                }
                else
                {
                    return MyImage<uint>.Create<Rgba32>(format);
                }
            }
            else
            {
                if (oneChannel)
                {
                    try
                    {
                        switch ((PngBitDepth)format.PixelType.BitsPerPixel)
                        {
                            case PngBitDepth.Bit8:
                                return MyImage<byte>.Create<Gray8>(stream);

                            case PngBitDepth.Bit16:
                                return MyImage<ushort>.Create<Gray16>(stream);
                        }
                    }
                    catch (Exception e)
                    {
                        var wtf = 0;
                        throw e;
                    }
                    
                }
                else
                {
                    var data = format.MetaData.GetFormatMetaData(PngFormat.Instance);
                    if (data.ColorType == PngColorType.Grayscale)
                    {
                        switch (data.BitDepth)
                        {
                            case PngBitDepth.Bit8:
                                return MyImage<byte>.Create<Gray8>(stream);

                            case PngBitDepth.Bit16:
                                return MyImage<ushort>.Create<Gray16>(stream);
                        }
                    }
                    else
                    {
                        return MyImage<uint>.Create<Rgba32>(stream);
                    }
                }
            }

            return null;
        }

        public static unsafe IMyImage Load(IntPtr pSource, int size, string debugName)
        {
            using (var stream = new UnmanagedMemoryStream((byte*)pSource.ToPointer(), size))
            {
                var image = Load(stream, false, debugName: debugName);
                return image;
            }
        }

        public static IMyImage Load(string path, bool oneChannel)
        {
            using (Stream stream = File.OpenRead(path))
            {
                var image = Load(stream, oneChannel, debugName: path);
                return image;
            }
        }

        public static unsafe void Save (string path, short[] heightMap, int w, int h)
        {
            byte[] result = new byte[heightMap.Length * sizeof(short)];
            Buffer.BlockCopy(heightMap, 0, result, 0, result.Length);

            PngEncoder encoder = new PngEncoder();
            encoder.ColorType = PngColorType.Grayscale;

            fixed (byte* p = result)
            {
                var stream = File.OpenWrite (path);
                Save<Gray16>(stream, FileFormat.Png, (IntPtr)p, w*sizeof(ushort),new Vector2I(w,h),2,encoder);
            }
            
        }

        public static unsafe void Save<TPixel>(Stream stream, FileFormat format, IntPtr dataPointer, int srcPitch, Vector2I size, uint bytesPerPixel, PngEncoder encoder) where TPixel : struct, IPixel<TPixel>
        {
            var dest = new TPixel[size.X * size.Y];
            var memory = new Memory<TPixel>(dest);
            using (var pin = memory.Pin())
            {
                var destPitch = (uint)size.X * bytesPerPixel;
                var destPtr = (byte*)pin.Pointer;
                var srcPtr = (byte*)dataPointer.ToPointer();
                for (int y = 0, destIndex = 0; y < size.Y; y++)
                {
                    Unsafe.CopyBlockUnaligned(destPtr, srcPtr, destPitch);
                    destPtr += destPitch;
                    srcPtr += srcPitch;
                }
            }

            using (var image = Image.WrapMemory(memory, size.X, size.Y))
            {
                switch (format)
                {
                    case FileFormat.Png:
                        image.SaveAsPng(stream, encoder);
                        break;
                    case FileFormat.Bmp:
                        image.SaveAsBmp(stream);
                        break;
                    case FileFormat.Jpg:
                        image.SaveAsJpeg(stream);
                        break;
                    default:
                        throw new NotImplementedException("Unknown image format.");
                }
            }
        }
    }

    public unsafe class MyImage<TData> : IMyImage<TData> where TData : unmanaged
    {
        public static MyImage<TData> Create<TImage>(string path) where TImage : struct, IPixel<TImage>
        {
            using (var stream = File.OpenRead(path))
            {
                return Create<TImage>(stream);
            }
        }

        public static MyImage<TData> Create<TImage>(Stream stream) where TImage : struct, IPixel<TImage>
        {
            using (var image = SixLabors.ImageSharp.Image.Load<TImage>(stream))
            {
                var span = image.GetPixelSpan();
                var castSpan = MemoryMarshal.Cast<TImage, TData>(span);
                var array = castSpan.ToArray();
                return new MyImage<TData>
                {
                    Size = new Vector2I(image.Width, image.Height),
                    Stride = image.Width,
                    Data = array,
                };
            }
        }

        public static MyImage<TData> Create<TImage>(IImageInfo image) where TImage : struct, IPixel<TImage>
        {
            return new MyImage<TData>
            {
                Size = new Vector2I(image.Width, image.Height),
                Stride = image.Width,
                Data = null,
            };
        }

        private MyImage() { }

        public Vector2I Size { get; private set; }
        public int Stride { get; private set; }
        public int BitsPerPixel => sizeof(TData) * 8;

        public TData[] Data { get; private set; }
        object IMyImage.Data => this.Data;
    }
}
