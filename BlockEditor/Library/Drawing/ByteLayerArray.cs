﻿using System;
using System.Collections.Generic;

namespace BlockEditor.PlanetData
{
    public class ByteLayerArray
    {
        public int w;
        public int h;
        public int wh => w * h;
        public byte[] data;
        public int layers;

        public ByteLayerArray()
        {
            
        }

        public ByteLayerArray(int w, int h, int layers)
        {
            data = new byte[w * h * layers];
            this.w = w;
            this.h = h;
            this.layers = layers;
        }

        public int Idx(int x, int y)
        {
            if (x < 0 || x >= w)
            {
                throw new Exception($"Wrong X index: {x} / {w}");
            }
            
            if (y < 0 || y >= h)
            {
                throw new Exception($"Wrong Y index: {y} / {h}");
            }
            return (y * w + x)*layers;
        }

        public void Idx(int idx, out int x, out int y, out int off)
        {
            var i = idx / layers;
            off = idx % layers;
            x = i % w;
            y = i / h;
        }
        
        public bool CanMergeCentered(ByteLayerArray pic, int idx)
        {
            Idx(idx, out var sx, out var sy, out _);
            if (sx - pic.w/2 < 0) return false;
            if (sx + pic.w/2 >= w - 1 - pic.w%2) return false;
            if (sy - pic.h/2 < 0) return false;
            if (sy + pic.h/2 >= h - 1 - pic.h%2) return false;

            return true;
        }
        
        public bool CanMerge(ByteLayerArray pic, int idx)
        {
            Idx(idx, out var sx, out var sy, out _);
            if (sx + pic.w >= w -1) return false;
            if (sy + pic.h >= h -1) return false;

            return true;
        }

        public void FillAll(byte channel, byte color)
        {
            Fill(0,0,w-1,h-1, channel, color);
        }
        
        public void Fill(int sx, int sy, int ex, int ey, int channel, byte v)
        {
            for (int x = sx; x <= ex; x++)
            {
                for (int y = sy; y <= ey; y++)
                {
                    data[Idx(x,y)+channel] = v;
                } 
            }
        }
        
        public void FillAll(byte r, byte g, byte b)
        {
            FillAll(0,0,w-1,h-1, r, g, b);
        }
        
        public void FillAll(int sx, int sy, int ex, int ey, byte r, byte g, byte b)
        {
            for (int x = sx; x <= ex; x++)
            {
                for (int y = sy; y <= ey; y++)
                {
                    data[Idx(x,y)+Pic.R] = r;
                    data[Idx(x,y)+Pic.G] = g;
                    data[Idx(x,y)+Pic.B] = b;
                } 
            }
        }

        public bool Merge(ByteLayerArray pic, int idx, int channel, int channel2)
        {
            if (!CanMerge(pic, idx))
            {
                return false;
            }
            
            Idx(idx, out var sx, out var sy, out _);

            for (int x = 0; x < pic.w; x++)
            {
                for (int y = 0; y < pic.h; y++)
                {
                    data[Idx(sx + x,sy + y)+channel] = pic.data[(x + y * pic.w)*layers+channel2];
                } 
            }

            return true;
        }
        
        public bool MergeCentered(Pic pic, int idx, int channel, int channel2, Func<byte, byte, byte> colorMapper = null)
        {
            if (!CanMergeCentered(pic, idx))
            {
                return false;
            }
            
            Idx(idx, out var sx, out var sy, out _);

            for (int x = 0; x < pic.w; x++)
            {
                for (int y = 0; y < pic.h; y++)
                {
                    var v = pic.data[(x + y * pic.w)*layers+channel2];

                    if (colorMapper != null)
                    {
                        v = colorMapper(v, data[Idx(sx + x - pic.w / 2, sy + y - pic.h / 2) + channel]);
                    }
                    data[Idx(sx + x - pic.w / 2, sy + y - pic.h / 2) + channel] = v;
                } 
            }

            return true;
        }
        
        public byte Get(int idx, byte layer)
        {
            return data[idx+layer];
        }
        
        public byte Get(int x, int y, byte layer, byte v)
        {
            if (x < 0 || x >= w)
            {
                throw new Exception($"Wrong X index: {x} / {w}");
            }
            
            if (y < 0 || y >= h)
            {
                throw new Exception($"Wrong Y index: {y} / {h}");
            }

            var id = Idx(x, y) + layer;
            if (id < 0 || id >= data.Length)
            {
                throw new Exception($"Wrong Y index: {id} / {data.Length}");
            }
            
            return data[Idx(x,y)+layer];
        }
        
        public void Set(int idx, byte layer, byte v)
        {
            data[idx+layer] = v;
        }

        public void Set(int x, int y, byte layer, byte v)
        {
            if (x < 0 || x >= w)
            {
                throw new Exception($"Wrong X index: {x} / {w}");
            }
            
            if (y < 0 || y >= h)
            {
                throw new Exception($"Wrong Y index: {y} / {h}");
            }

            var id = Idx(x, y) + layer;
            if (id < 0 || id >= data.Length)
            {
                throw new Exception($"Wrong Y index: {id} / {data.Length}");
            }
            
            data[Idx(x,y)+layer] = v;
        }
        
        public void Sum(int x, int y, byte layer, byte v)
        {
            data[Idx(x, y) + layer] += v;
        }

        public byte Get(int x, int y, byte type)
        {
            return data[Idx(x,y)+type];
        }
        
        public Dictionary<byte, int> Calculate(byte type)
        {
            var h1 = new Dictionary<byte, int>();
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    var a1 = Get(x, y, type);
                    h1[a1] = h1.GetValueOrDefault(a1, 0) + 1;
                }
            }

            return h1;
        }

        
        public void ForAll(Func<int, int, int, bool> action)
        {
            For(0,0,w,h, action);
        }
        public void For(int sx, int sy, int ex, int ey, Func<int, int, int, bool> action)
        {
            sx = Math.Max(sx, 0);
            sy = Math.Max(sy, 0);
            ex = Math.Min(ex, w-1);
            ey = Math.Min(ey, h-1);
            
            for (int x = sx; x <= ex; x++)
            {
                for (int y = sy; y <= ey; y++)
                {
                    var idx = Idx(x, y);
                    if (!action(idx, x, y))
                    {
                        return;
                    }
                } 
            }
        }
        
        
    }
}