﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using PixelFormat = System.Drawing.Imaging.PixelFormat;

namespace PlanetGenerator2.MapData
{
    public static class ImageUtils
    {
        public static byte[] ToByteArray(this Bitmap bitmap, PixelFormat? format = null)
        {

            BitmapData bmpdata = null;

            try
            {
                bmpdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, format.HasValue ? format.Value : bitmap.PixelFormat);//
                int numbytes = bmpdata.Stride * bitmap.Height;
                byte[] bytedata = new byte[bitmap.Width * bitmap.Height * 3];
                IntPtr ptr = bmpdata.Scan0;

                
                if (bmpdata.Stride != bitmap.Width * 3)
                {
                    for (var h=0; h<bitmap.Height; h++)
                    {
                        Marshal.Copy(ptr+h*bmpdata.Stride, bytedata, bitmap.Width * 3*h, bitmap.Width * 3);
                    }

                    var t = 0;
                }
                else
                {
                    Marshal.Copy(ptr, bytedata, 0, numbytes);
                }

                return bytedata;
            }
            finally
            {
                if (bmpdata != null)
                    bitmap.UnlockBits(bmpdata);
            }

            
        }
        
        public static BitmapImage ToImageSource(this Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();
        
                return bitmapimage;
            }
        }

        public static Bitmap ToBitmap(this byte[] bytes)
        {
            var wh = (int)Math.Sqrt(bytes.Length/3);
            byte[] newbytes = bytes.ToArray();

            return new Bitmap(wh, wh, 3*wh, PixelFormat.Format24bppRgb,  Marshal.UnsafeAddrOfPinnedArrayElement(newbytes, 0));
        }
    }
}