﻿using System;
using VRageMath;

namespace Slime.Geom
{
    public class Line
    {
        private P l1;
        private P l2;
        private double d;

        public Line(P l1, P l2)
        {
            this.l1 = l1;
            this.l2 = l2;
            d = Math.Sqrt(Math.Pow(l2.x - l1.x, 2) + Math.Pow(l2.y - l1.y, 2));
        }

        public double Distance(P point)
        {
            return Math.Abs((l2.x - l1.x) * (l1.y - point.y) - (l1.x - point.x) * (l2.y - l1.y)) / d;
        }
        
        public static Vector2 GetClosestPoint(P a, P b, P p, out bool belongsToSegment)
        {
            Vector2 vectorAP = new Vector2(p.x - a.x, p.y - a.y);     //Vector from A to P
            Vector2 vectorAB = new Vector2(b.x - a.x, b.y - a.y);     //Vector from A to B
        
            float magnitudeAB = vectorAB.LengthSquared();     //Magnitude of AB vector (it's length squared)     
            float ABAPproduct = Vector2.Dot(vectorAP, vectorAB);    //The DOT product of a_to_p and a_to_b     
            float distance = ABAPproduct / magnitudeAB; //The normalized "distance" from a to your closest point  

            belongsToSegment = false;
            //Check if P projection is over vectorAB     
            if (distance < 0) return new Vector2(a.x,a.y);
            if (distance > 1) return new Vector2(b.x,b.y);

            belongsToSegment = true;
            return new Vector2(a.x,a.y) + vectorAB * distance;
        }
                
        public bool BelongsToSegment(P point)
        {
            GetClosestPoint(l1, l2, point, out var belongsToSegment);
            return belongsToSegment;
        }
    }
}