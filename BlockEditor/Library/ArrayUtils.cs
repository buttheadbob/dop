﻿using System;

namespace BlockEditor.Library
{
    public static class ArrayUtils
    {
        public static T[,] ConvertTo2Dim<T>(this T[] array, int w, int h)
        {
            var newArray = new T[w,h];
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    var idx = x + (y * w);
                    newArray[x, y] = array[idx];
                }
            }

            return newArray;
        }
        
        public static T[] ConvertTo1Dim<T>(this T[,] array, int w, int h)
        {
            var newArray = new T[w*h];
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    var idx = x + (y * w);
                    newArray[idx] = array[x, y];
                }
            }

            return newArray;
        }

        public static T[,] Cut<T>(this T[,] array, int x, int y, int w, int h)
        {
            x = Math.Max(x, 0);
            y = Math.Max(y, 0);
            w = Math.Min(x+w, array.GetLength(0));
            h = Math.Min(y+h, array.GetLength(1));

            var newArray = new T[w,h];
            for (int xx = x; xx < x+w; xx++)
            {
                for (int yy = y; yy < y+h; yy++)
                {
                    newArray[xx-y, yy-y] = array[xx,yy];
                }
            }

            return newArray;
        }
        
        public static void Merge<T>(this T[] original, T[,] toCopy, int w, int h, int startX, int startY, int endX, int endY, Func<T, T, T> filter = null)
        {
            startX = Math.Max(0, startX);
            startY = Math.Max(0, startY);
            endX = Math.Min(w, endX);
            endY = Math.Min(h, endY);
            
            for (int x = startX; x < endX; x++)
            {
                for (int y = startY; y < endY; y++)
                {
                    var v1 = original[x + y * h];
                    var v2 = toCopy[x - startX, y - startY];

                    if (filter != null)
                    {
                        original[x + y * h] = filter.Invoke(v1,v2);    
                    }
                    else
                    {
                        original[x + y * h] = v2;
                    }
                    
                }   
            }
        }
        
        /*
         public ushort[,] GetHeightMap(int scale = 4)
        {
            var heightmap = planet.Sides[Side].heightmap;
            
            int w = (int)Math.Sqrt(heightmap.Length);
            int h = w;

            w /= scale;
            h /= scale;

            var hmap2 = new ushort[w, h];
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    var idx = x * scale + (y * w * scale * scale);
                    var v = heightmap[idx];
                    hmap2[x, y] = v;
                }
            }

            return hmap2;
        }
         */
    }
}