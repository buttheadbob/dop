﻿//CUSTOM

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using System.Xml.Serialization;
using BlockEditor;
using BlockEditor.GuiData;
using BlockEditor.SE;
using BlockEditor.Serialization;
using BlockEditor.Wpf;
using VRage.Game;
using VRage.ObjectBuilders;
using VRageRender;
using ContextMenu = System.Windows.Controls.ContextMenu;
using MenuItem = System.Windows.Controls.MenuItem;
using MessageBox = System.Windows.MessageBox;

public class Script
{
    void Apply()
    {
        MenuGenerator();
    }

    void MenuGenerator()
    {
        var i = GlobalEditor.MenuInterceptor[0];
        GlobalEditor.MenuInterceptor.RemoveAll((x) => true);
        GlobalEditor.MenuInterceptor.Add(i);
        GlobalEditor.MenuInterceptor.Add(MenuInterceptor);
    }

    private void MenuInterceptor(List<object> objects, ContextMenu menu)
    {
        if (objects.Count == 0) return;
        AddMenu(menu, objects, "Fix DisplayName/Description/BlockPairName", GenerateDisplayNameAndDescriptions);
        AddMenu(menu, objects, "Generate copies", GenerateMany);
        AddMenu(menu, objects, "Edit crafts", EditCrafts);
        AddMenu(menu, objects, "Apply crafts & formulas", ApplyCraftsAndFormulas);
        AddMenu(menu, objects, "Split by type", SplitByType);
        AddMenu(menu, objects, "Create File", CreateFile);
    }

    public void CreateFile(List<object> objects)
    {
        var name = new InputBox("Name", "", "").ShowDialog();
        name = name + ".sbc";

        Output.Info(""+objects.Count);
        foreach (var obj in objects)
        {
            
            if (obj is Mod mod)
            {
                name = Path.Combine(mod.RootFolder, name);
                DefinitionFile ddf = null;
                foreach (var definition in mod.Definitions)
                {
                    if (definition.File == name)
                    {
                        ddf = definition;
                        
                    }
                }

                if (ddf == null)
                {
                    ddf = new DefinitionFile(mod);
                    ddf.AlwaysVisible = true;
                    ddf.File = name;
                    mod.Definitions.Add(ddf);
                }
                else
                {
                    Output.Info("Found DDF:" + name);
                }
            }
        }
        
        
        GlobalEditor.RefreshTree();
        
    }
    
    public void SplitByType(List<object> objects)
    {
        var path = GlobalEditor.OpenFileDialog()[0];
        if (path == null) return;
        
        //var def = Sugar.Flatten(objects);
        //var settings = new XReader().Read<BlocksSettings>(path);
        
        foreach (var o in objects)
        {
            if (o is DefinitionFile df) {
                var mod = df.Mod;
                var dict = new Dictionary<string, List<object>>();
                foreach (var oo in df.AllItems)
                {
                    if (oo is MyObjectBuilder_CubeBlockDefinition block)
                    {
                        var type = block.Id.TypeId.ToString().Replace("MyObjectBuilder_", "");
                        dict.GetOrNew(type).Add(block);
                    }
                }
                df.AllItems.Clear();
                
                var modFolder = Directory.GetParent(df.File).FullName;
                foreach (var pair in dict)
                {
                    var file = Path.Combine(modFolder, pair.Key);
                    DefinitionFile ddf = null;
                    foreach (var definition in mod.Definitions)
                    {
                        if (definition.File == file)
                        {
                            ddf = definition;
                        }
                    }

                    if (ddf == null)
                    {
                        ddf = new DefinitionFile(mod);
                        ddf.File = file+".sbc";
                        mod.Definitions.Add(ddf);
                    }
                    
                    ddf.AllItems.AddRange(pair.Value);
                }
            }
        }
        
        GlobalEditor.RefreshTree();
    }
    
    public void ApplyCraftsAndFormulas(List<object> objects)
    {
        var dialog = GlobalEditor.OpenFileDialog();
        var path =  dialog[0];
        if (path == null) return;
        
        var def = Sugar.Flatten(objects);
        
        var settings = new XReader().Read<BlocksSettings>(path);
        
        
        foreach (var f in def)
        {
            if (f is MyObjectBuilder_CubeBlockDefinition block)
            {
                var cc = ClassCache.Get(block.GetType());
                var special = block.GetSpecial();
                var tier = block.GetTier();
                var type = block.Id.TypeId.ToString().Replace("MyObjectBuilder_", "");
                foreach (var setting in settings.Settings)
                {
                    if (special != setting.SpecialName && setting.Type != type) continue;
                    
                    foreach (var formula in setting.Formulas)
                    {
                        
                        if (!cc.Fields.TryGetValue(formula.Name, out var mi))
                        {
                            if (!cc.AttributeFields.TryGetValue(formula.Name, out var tmi))
                            {
                                continue;
                            }

                            mi = tmi.Item1;
                        }
                            
                        var deserializer = X.GetDeserializer(Sugar.GetMemberType(mi));
                        if (deserializer != null)
                        {
                            var value = Math.Pow(formula.Pow, tier) * formula.Value + formula.Sum;
                            var o = deserializer.Invoke(""+value);
                            mi.SetValue(block, o);
                        }
                        else
                        {
                            Output.Error(null,$"Not found deserializer {setting.Type}/{setting.SpecialName} -> {formula.Name}");
                        }
                    }
                }
                GenerateNames(block);
            }
        }
       
        
        
    }

    public void GenerateDisplayNameAndDescriptions(List<object> objects)
    {
        var def = Sugar.Flatten(objects);
        foreach (var f in def)
        {
            if (f is MyObjectBuilder_CubeBlockDefinition block)
            {
                GenerateNames(block);
            }
        }
    }

    private void AddMenu(ContextMenu menu, List<object> objects, String name, Action<List<object>> handler)
    {
        var menuItem = new MenuItem();
        menuItem.Header = name;
        menuItem.Click += (o, e) =>
        {
            try
            {
                handler(objects);
            }
            catch (Exception ee)
            {
                Output.Error(ee, "AddMenu");            
            }
        };
        menu.Items.Add(menuItem);
    }

    public void EditCrafts(List<object> objects)
    {
        var t = new BaseItems();
        var v = new List<object>();
        v.Add(t);
        AbstractEditor.ShowDialog("Test", v);

        foreach (var item in objects)
        {
            if (item is MyObjectBuilder_CubeBlockDefinition block)
            {
                GenerateCrafts(block, t.Components);
            }
        }

    }

    public void GenerateNames(MyObjectBuilder_CubeBlockDefinition block)
    {
        var tier = block.GetTier();
        var tierS = Helper.GenerateTierString(tier);

        var special = block.GetSpecial();
        var type = block.Id.TypeId.ToString().Replace("MyObjectBuilder_", "");
        special = special != null ? "_" + special : "";

        //var gsize = block.CubeSize == MyCubeSize.Large ? "L" : "S";
        var size = block.Size.X + "x" + block.Size.Y + "x" + block.Size.Z;

        block.DisplayName = $"DisplayName_{type}{special}_{tierS}";
        block.Description = $"Description_{type}{special}";
        block.BlockPairName = $"{type}{special}_{size}_{tierS}";
    }

    public void GenerateCrafts(MyObjectBuilder_CubeBlockDefinition block, MyObjectBuilder_CubeBlockDefinition.CubeBlockComponent[] Components)
    {
        var tier = block.GetTier();
        var volume = block.Size.x * block.Size.y * block.Size.z;

        var array = new List<MyObjectBuilder_CubeBlockDefinition.CubeBlockComponent>();
        for (var x = 0; x < Components.Length; x++)
        {
            var c = new MyObjectBuilder_CubeBlockDefinition.CubeBlockComponent();
            array.Add(c);

            var left = Components[x].Count * volume;
            while (left > ushort.MaxValue)
            {
                var count = Math.Min(left, ushort.MaxValue);
                left -= count;

                c.Subtype = Components[x].Subtype + Helper.GenerateTierString(tier);
                c.Count = (ushort) count;
                c.DeconstructId = Components[x].DeconstructId;
                c.Type = Components[x].Type;
            }
        }

        block.Components = array.ToArray();
    }
    
    public void GenerateMany(List<object> items)
    {
        var am = new InputBox("How many tiers").ShowDialog();
        int tiers = int.Parse(am);
        
        if (GlobalEditor.Mods.Count >= 2)
        {
            var def = new DefinitionFile(GlobalEditor.Mods[1]);
            def.File = "Tmp";
            GlobalEditor.Mods[1].Definitions.Add(def);
            var extraname = new InputBox("ExtraName", "", "").ShowDialog();
            foreach (var item in items)
            {
                if (item is MyObjectBuilder_CubeBlockDefinition block)
                {
                    for (var tier = 1; tier <= tiers; tier++)
                    {
                        var newBlock = (MyObjectBuilder_CubeBlockDefinition) GlobalEditor.Clone(block);
                        var newExtraName = extraname + block.GetDLC();
                        if (tier == 1)
                        {
                            newBlock.BlockVariants = new SerializableDefinitionId[tiers-1];
                            for (int i = 2; i <= tiers; i++)
                            {
                                newBlock.BlockVariants[i - 2] = new SerializableDefinitionId(newBlock.TypeId, GenerateId(newBlock, newExtraName, i));
                            }
                        }
                        else
                        {
                            newBlock.BlockVariants = null;
                        }
                        
                        def.AllItems.Add(newBlock);
                        def.AlwaysVisible = true;
                        var name = GenerateId(newBlock, newExtraName, tier);
                        newBlock.Id.SubtypeName = name;
                    }
                }
            }
        }

        GlobalEditor.RefreshTree();
    }

    public void CalculateMass()
    {
        
    }

    public string GenerateId(MyObjectBuilder_CubeBlockDefinition block, string extraname, int tier)
    {
        var gsize = block.CubeSize == MyCubeSize.Large ? "L" : "S";
        var size = block.Size.X + "x" + block.Size.Y + "x" + block.Size.Z;
        var tierS = Helper.GenerateTierString(tier);
        var extra = "";
        if (extraname == null)
        {
            extra = "";
        }
        else
        {
            extra = "_" + extraname;
        }

        return $"{gsize}{size}{extra}_{tierS}";
    }
}


public class BaseItems
{
    //[System.Xml.Serialization.XmlElement]
    public MyObjectBuilder_CubeBlockDefinition.CubeBlockComponent[] Components = new MyObjectBuilder_CubeBlockDefinition.CubeBlockComponent[0];
    //[System.Xml.Serialization.XmlElement]
    //public int CriticalItemIndex =0;
    //public string CriticalItem ="SteelPlate";
}

static class Helper
{
    public static string GenerateTierString(int tier)
    {
        if (tier < 10)
        {
            return "T0" + tier;
        }

        return "T" + tier;
    }

    public static int GetTier(this MyObjectBuilder_CubeBlockDefinition block)
    {
        try
        {
            var parts = block.Id.SubtypeName.Split(new string[] {"_"}, StringSplitOptions.None);
            var n = parts[parts.Length-1].Substring(1);
            if (n.StartsWith("0")) n = n.Substring(1);
            return int.Parse(n);
        }
        catch (Exception e)
        {
            MessageBox.Show(""+e);
            return 0;
        }
    }

    public static double GenerateValue(double baseValue, double mlt, int tier)
    {
        return baseValue * Math.Pow(mlt, tier - 1);
    }

    public static string GetDLC(this MyObjectBuilder_CubeBlockDefinition block)
    {
        if (block.DLCs != null && block.DLCs.Length != 0)
        {
            return "-"+block.DLCs[0];
        }

        return "";
    }
    public static string GetSpecial(this MyObjectBuilder_CubeBlockDefinition block)
    {
        string dlc = null;
        if (block.DLCs != null && block.DLCs.Length != 0)
        {
            dlc = block.DLCs[0];
        }
        var split = block.Id.SubtypeName.Split(new string[] {"_"}, StringSplitOptions.None);
        if (split.Length == 3)
        {
            return split[1]+"-"+dlc;
        }

        return dlc;
    }
}