﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using BlockEditor.Editors;

namespace BlockEditor
{
    public class AbstractEditor
    {
        public static void ShowDialog(string Name, List<object> list, Action onEdited)
        {
            var w = new EditorWindow(list, onEdited);
            w.Header.Text = Name;
            w.ShowDialog();
        }
    }
}