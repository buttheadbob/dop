﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml;
using BlockEditor.Editors;
using BlockEditor.GuiData;
using BlockEditor.Serialization;
using BlockEditor.Wpf;
using NLog.Fluent;
using RestSharp.Serializers;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.ObjectBuilders;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.MessageBox;
using MessageBoxResult = System.Windows.MessageBoxResult;
using UserControl = System.Windows.Controls.UserControl;

namespace BlockEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class EditorWindow
    {
        private List<object> objects;
        private Action changed;
        public EditorWindow(List<object> objects, Action changed)
        {
            InitializeComponent();
            this.objects = objects;
            this.changed = changed;
            Refresh();
        }

        public void Refresh ()
        {
            var editor = new PropertyEditor();
            editor.Edit(objects[0].GetType(), objects, null, false);
            EditorArea.Content = editor;
        }

        private void Refresh(object sender, RoutedEventArgs e)
        {
            changed();
            Refresh();
        }

        private void Close(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}