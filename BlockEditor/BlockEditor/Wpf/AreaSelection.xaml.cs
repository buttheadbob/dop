﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Clicker.POE;

namespace Clicker
{
    public partial class AreaSelection : UserControl
    {
        public AreaSelection()
        {
            InitializeComponent();
        }

        public void Apply(BitmapArea cropped, int zoom, bool selected)
        {
            var r = cropped.Rectangle;
            var w = Math.Max(r.Width * zoom+2,100);
            Width = w+2;
            Height = r.Height * zoom+2 + 20;
            Tag = cropped;
            Margin = new Thickness(r.Left * zoom-1, r.Top * zoom-1, 0 ,0);
            HorizontalAlignment = HorizontalAlignment.Left;
            VerticalAlignment = VerticalAlignment.Top;
            
            Rect.Width = r.Width * zoom+2;
            Rect.Height = r.Height * zoom+2;

            var c = selected ? "#00ff00" : "#ffff00";
            Rect.Stroke = new SolidColorBrush((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(c));
            Rect.StrokeThickness = 1d;
            
            AreaName.Width = w;
            AreaName.Text = "Unnamed";//cropped.Name == "" ? "Unnamed" : cropped.Name;
            AreaName.Margin = new Thickness(0, r.Height * zoom+1, 0,0);
        }

        //private void AreaName_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    var name = InputDialog.Show();
        //    ((BitmapArea) Tag).Name = name;
        //    AreaName.Text = name;
        //}
    }
}