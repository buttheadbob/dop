﻿using BlockEditor.SE;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using BlockEditor.Editors;
using BlockEditor.Serialization;
using RestSharp.Serializers;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.ObjectBuilders;
using BlockEditor;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using GasTank = Sandbox.Common.ObjectBuilders.Definitions.MyObjectBuilder_GasTankDefinition;
using Battery = Sandbox.Common.ObjectBuilders.Definitions.MyObjectBuilder_BatteryBlockDefinition;

namespace BlockEditor.Wpf
{
    public class Extern
    {
        public static Func<string, MyObjectBuilder_DefinitionBase> GetBlock;
        public static Func<string, double> GetMass;
        
        public static double GetTankMass(string b, double weight, double perc = 1d)
        {
            return GetMass("GasTankDefinition/"+b) + (GetBlock("GasTankDefinition/"+b) as GasTank).Capacity * perc * weight;
        }
    
        public static double Invert (double d) {
            if (d < 0) {
                d = -1d / d;
            }
        
            return d;
        }



        public static double KeroThrust(object o, FormulaArgument arg)
        {
            var def = o as MyObjectBuilder_DefinitionBase;
            var cube = o as MyObjectBuilder_CubeBlockDefinition;
            var cmp = o as MyObjectBuilder_ComponentDefinition;
            var thrust = o as MyObjectBuilder_ThrustDefinition;
            var engine = o as MyObjectBuilder_HydrogenEngineDefinition;
            var reactor = o as MyObjectBuilder_ReactorDefinition;
            GetMass = arg.GetMass;
            GetBlock = arg.GetBlock;
            double.TryParse(arg.Arg1, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg1);
            double.TryParse(arg.Arg2, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg2);
            double.TryParse(arg.Arg3, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg3);
            var Arg1 = arg.Arg1;
            var Arg2 = arg.Arg2;
            var Arg3 = arg.Arg3;
            var Mass = arg.Mass;
            var Tier = arg.Tier;
            var SizeMlt = arg.SizeMlt;
            var BlockSizeMlt = cube.CubeSize == MyCubeSize.Large ? SizeMlt : 1f;
            var Volume = cube.Size.X*cube.Size.Y*cube.Size.Z;    
        
            //var mass = cube?.GetMass() ?? 0;
            var G = 9.8;

            var v = Mass / GetMass("ThrustDefinition/L3x3x3_Kero_T01");
            var m1 = GetTankMass("L3x3x3_Kero_T01", 0.85f) * 2;
            var m2 = GetTankMass("L3x3x3_Oxy_T01", 1.1f);
            var m3 = GetMass("ThrustDefinition/L3x3x3_Kero_T01");
            
            return Math.Pow(Invert(dArg1), Tier-1) * G * dArg2 * v * (m1 + m2 + m3 + dArg3);
        }
        
        public static double HydroThrust(object o, FormulaArgument arg)
        {
            var def = o as MyObjectBuilder_DefinitionBase;
            var cube = o as MyObjectBuilder_CubeBlockDefinition;
            var cmp = o as MyObjectBuilder_ComponentDefinition;
            var thrust = o as MyObjectBuilder_ThrustDefinition;
            var engine = o as MyObjectBuilder_HydrogenEngineDefinition;
            var reactor = o as MyObjectBuilder_ReactorDefinition;
            GetMass = arg.GetMass;
            GetBlock = arg.GetBlock;
            double.TryParse(arg.Arg1, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg1);
            double.TryParse(arg.Arg2, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg2);
            double.TryParse(arg.Arg3, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg3);
            var Arg1 = arg.Arg1;
            var Arg2 = arg.Arg2;
            var Arg3 = arg.Arg3;
            var Mass = arg.Mass;
            var Tier = arg.Tier;
            var SizeMlt = arg.SizeMlt;
            var BlockSizeMlt = cube.CubeSize == MyCubeSize.Large ? SizeMlt : 1f;
            var Volume = cube.Size.X*cube.Size.Y*cube.Size.Z;    
        
            //var mass = cube?.GetMass() ?? 0;
            var G = 9.8;

            var v = Mass / GetMass("ThrustDefinition/L3x3x3_Hydro_T01");
            var m1 = GetTankMass("L3x3x3_Hydro_T01", 0.85f) * 2;
            var m2 = GetTankMass("L3x3x3_Oxy_T01", 1.1f);
            var m3 = GetMass("ThrustDefinition/L3x3x3_Hydro_T01");
            
            return Math.Pow(Invert(dArg1), Tier-1) * G * dArg2 * v * (m1 + m2 + m3 + dArg3);
        }
        
        public static double OilEngineConsumption(object o, FormulaArgument arg)
        {
            var def = o as MyObjectBuilder_DefinitionBase;
            var cube = o as MyObjectBuilder_CubeBlockDefinition;
            var cmp = o as MyObjectBuilder_ComponentDefinition;
            var thrust = o as MyObjectBuilder_ThrustDefinition;
            var engine = o as MyObjectBuilder_HydrogenEngineDefinition;
            var reactor = o as MyObjectBuilder_ReactorDefinition;
            GetMass = arg.GetMass;
            GetBlock = arg.GetBlock;
            double.TryParse(arg.Arg1, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg1);
            double.TryParse(arg.Arg2, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg2);
            double.TryParse(arg.Arg3, NumberStyles.Any, CultureInfo.InvariantCulture, out var dArg3);
            var Arg1 = arg.Arg1;
            var Arg2 = arg.Arg2;
            var Arg3 = arg.Arg3;
            var Mass = arg.Mass;
            var Tier = arg.Tier;
            var SizeMlt = arg.SizeMlt;
            var BlockSizeMlt = cube.CubeSize == MyCubeSize.Large ? SizeMlt : 1f;
            var Volume = cube.Size.X*cube.Size.Y*cube.Size.Z;    
        
            //var mass = cube?.GetMass() ?? 0;
            var G = 9.8;
            
            
            var r = (GetMass("BatteryBlockDefinition/L1x1x1_Common_T01") / (GetBlock("BatteryBlockDefinition/L1x1x1_Common_T01") as Battery).MaxStoredPower);
            var v = 3600 / dArg1 / (r * dArg2);
            return v;
        }
    }
}