﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace BlockEditor
{
    public class ObjectsPool<T>
    {
        private List<T> data = new List<T>();
        private Func<T> creator;
        private Action<T> cleaner;
        
        public ObjectsPool(int max, Func<T> creator, Action<T> cleaner)
        {
            this.cleaner = cleaner;
            this.creator = creator;
        }

        public T Allocate()
        {
            lock (data)
            {
                if (data.Count != 0)
                {
                    var t = data[data.Count-1];
                    data.RemoveAt(data.Count-1);
                    return t;
                }
            }

            return creator.Invoke();
        }
        public void Deallocate(T t)
        {
            if (t == null) return;
            
            cleaner?.Invoke(t);
            lock (data)
            {
                data.Add(t);
            }
        }
    }
}