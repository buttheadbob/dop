﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using RestSharp.Extensions;
using VRage;
using VRage.Utils;

namespace BlockEditor.Serialization
{
    public class XWriter
        {
            private XmlDocument doc;

            public string Write(object o)
            {
                doc = new XmlDocument();
                
                
                
                var name = "Root";
                var root = o.GetType().GetCustomAttribute<XmlRootAttribute>();
                if (root != null)
                {
                    name = root.ElementName;
                }
                
                var elem = doc.CreateElement(name);
                
                
                //var attribute = doc.CreateAttribute("xmlns","xsi", "http://www.w3.org/2001/XMLSchema");
                //attribute.Value = "http://www.w3.org/2001/XMLSchema-instance";
                //elem.Attributes.Append(attribute);
                
                Write(o, elem);
                doc.AppendChild(elem);
                
                XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                doc.InsertBefore(xmlDeclaration, doc.DocumentElement);
                
                    
                StringWriter sw = new StringWriter();
                doc.Save(sw);
                return sw.ToString().Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            }
            
            public string Write(string file, object o)
            {
                var txt = Write(o);
  
                File.WriteAllText(file, txt, Encoding.UTF8);
                return txt;
            }
            
            private void Write(object o, XmlElement element)
            {
                var t = o.GetType();
                var cache = ClassCache.Get(t);

                if (X.CustomSerializers(o, element, doc))
                {
                    return;
                }
                
                foreach (var a in cache.AttributeFields)
                {
                    var name = a.Key;
                    var value = a.Value.Item1.GetValue(o);
                    if (value != null)
                    {
                        var d = X.GetSerializer(a.Value.Item1.GetMemberType());
                        element.SetAttribute(name, d.Invoke(value));
                    }
                }

                var fields = new List<MemberInfo>(cache.Fields.Values);
                fields.Sort(Sugar.Sort);
                
                

                foreach (var member in fields)
                {
                    //Type tt = null;
                    //if (member is FieldInfo fi) tt = fi.FieldType;
                    //if (member is PropertyInfo pi) tt = pi.PropertyType;
                    
                    if (!X.ShouldSerialize(member)) continue;
                    
                    var v = member.GetValue(o);
                    if (v == null) continue;
                    if (v == "") continue;
                    if (!cache.ShouldSerialize(o, member)) continue;


                    if (!X.ShouldSerialize(member, v))
                    {
                        continue;
                    }

                    var name = member.Name;
                    var index = name.LastIndexOf('.');
                    if (index != -1)
                    {
                        name = name.Substring(index);
                    }
                    
                    var et = member.GetAttribute<XmlElementAttribute>();
                    if (et != null && et.ElementName != null && et.ElementName != "")
                    {
                        name = et.ElementName;
                    }

                    var defaultValue = member.GetAttribute<DefaultValueAttribute>();
                    if (defaultValue != null)
                    {
                        if (defaultValue.Value != null && defaultValue.Value.Equals(v)) continue;
                    }

                    var s = X.GetSerializer(v.GetType());
                    if (s != null)
                    {
                        var elem = doc.CreateElement(name);
                        elem.InnerText = s.Invoke(v);
                        element.AppendChild(elem);
                        continue;
                    } else if (v.GetType().IsArray) {
                        
                        WriteArray (member, name, v, element);
                        continue;
                    } else if (v.GetType().ImplementsGenericInterface(typeof(IEnumerable<>)))
                    {
                        WriteList(member, name, v, element);
                        continue;
                    } else {
                        var elem = doc.CreateElement(name);
                        Write(v, elem);
                        element.AppendChild(elem);
                        continue;
                    }
                }
            }

            public void WriteList(MemberInfo member, string name, object value, XmlElement element)
            {
                GetArrayNames(member, ref name, out var elementName);
                            
                var enumerator = ((IEnumerable) value).GetEnumerator();
                var elem = doc.CreateElement(name);
                

                int count = 0;
                var type = value.GetType().GenericTypeArguments[0];
                while (enumerator.MoveNext())
                {
                    var child = WriteArrayElement(enumerator.Current, elementName, type);
                    elem.AppendChild(child);
                    count++;
                }

                if (count > 0)
                {
                    element.AppendChild(elem);
                }
            }

            private XmlElement WriteArrayElement(object vo, String elementName, Type parentType)
            {
                
                var arrElem = doc.CreateElement(elementName);

                if (vo.GetType() != parentType)
                {
                    var attribute = doc.CreateAttribute("xsi","type", "http://www.w3.org/2001/XMLSchema-instance");
                    attribute.Value = vo.GetType().Name;
                    arrElem.Attributes.Append(attribute);
                }
                var serializer = X.GetSerializer(vo.GetType());
                if (serializer != null)
                {
                    arrElem.InnerText = serializer(vo);
                }
                else
                {
                    Write(vo, arrElem);
                }

                return arrElem;
            }
            
            public void WriteArray(MemberInfo member, string arrayName, object value, XmlElement element)
            {
                var single = member.GetCustomAttribute<XmlElementAttribute>();
                var type = value.GetType().GetElementType();
                if (single != null)
                {
                    
                    if (single.ElementName != null && single.ElementName != "")
                    {
                        arrayName = single.ElementName;
                    }
                    
                    var vo = ((Array) value).GetValue(0);
                    var v = X.GetSerializer(type).Invoke(vo);
                    var inner = doc.CreateElement(arrayName);
                    inner.InnerText = v;
                    element.AppendChild(inner);
                    return;
                }
                
                
                
                GetArrayNames(member, ref arrayName, out var elementName);

                var arr = (Array) value;
                var elem = doc.CreateElement(arrayName);
                for (var x=0; x<arr.Length; x++)
                {
                    var vo = arr.GetValue(x);
                    var child = WriteArrayElement(vo, elementName, type);
                    elem.AppendChild(child);
                }
                        
                if (arr.Length > 0)
                {
                    element.AppendChild(elem);
                }
            }


            public string ReplaceArrayName(string s)
            {
                switch (s)
                {
                    case "UInt32": return "unsignedInt";
                    case "String": return "string";
                    default: return s;
                }
            }
            
            public void GetArrayNames(MemberInfo member, ref string arrayName, out string elementName)
            {
                var type = member.GetMemberType();
                if (type.IsArray)
                {
                    elementName = type.GetElementType().Name;
                }
                else
                {
                    elementName = type.GenericTypeArguments[0].Name;
                }

                elementName = ReplaceArrayName(elementName);
                
                var attrRoot = member.GetCustomAttribute<XmlArrayAttribute>();
                if (attrRoot != null && attrRoot.ElementName != "")
                {
                    arrayName = attrRoot.ElementName;
                }
                var attr = member.GetCustomAttribute<XmlArrayItemAttribute>();
                if (attr != null && attr.ElementName != "")
                {
                    elementName = attr.ElementName;
                }
            } 
            
            
            
        }
}