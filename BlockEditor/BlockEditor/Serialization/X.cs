﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Xml;
using NLog.Fluent;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using VRage;
using VRage.Game;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game.ObjectBuilders.Definitions.SessionComponents;
using VRage.ObjectBuilders;
using VRageMath;

namespace BlockEditor.Serialization
{
    public class X
        {
            private static Dictionary<string, Type> nameToClass = new Dictionary<string, Type>();

            public static Dictionary<Type, Func<string, object>> Deserializers = new Dictionary<Type, Func<string, object>>();
            public static Dictionary<Type, Func<object, string>> Serializers = new Dictionary<Type, Func<object, string>>();
            
            
            public static void Init()
            {
                
                Deserializers[typeof(string)] = delegate(string s) { return s; };
                Deserializers[typeof(bool)] = delegate(string s) { return bool.Parse(s); };
                
                Deserializers[typeof(float)] = delegate(string s) { return float.Parse(s, CultureInfo.InvariantCulture); };
                Deserializers[typeof(double)] = delegate(string s) { return double.Parse(s, CultureInfo.InvariantCulture); };
                Deserializers[typeof(int)] = delegate(string s) { return int.Parse(s, CultureInfo.InvariantCulture); };
                Deserializers[typeof(uint)] = delegate(string s) { return uint.Parse(s, CultureInfo.InvariantCulture); };
                Deserializers[typeof(long)] = delegate(string s) { return long.Parse(s, CultureInfo.InvariantCulture); };
                Deserializers[typeof(ulong)] = delegate(string s) { return ulong.Parse(s, CultureInfo.InvariantCulture); };
                Deserializers[typeof(short)] = delegate(string s) { return short.Parse(s, CultureInfo.InvariantCulture); };
                Deserializers[typeof(ushort)] = delegate(string s) { return ushort.Parse(s, CultureInfo.InvariantCulture); };
                Deserializers[typeof(byte)] = delegate(string s) { return byte.Parse(s, CultureInfo.InvariantCulture); };
                
                
                
                Deserializers[typeof(float?)] = delegate(string s)
                {
                    if (string.IsNullOrEmpty(s)) return null;
                    return float.Parse(s, CultureInfo.InvariantCulture);
                };
                Deserializers[typeof(double?)] = delegate(string s)
                {
                    if (string.IsNullOrEmpty(s)) return null;
                    return double.Parse(s, CultureInfo.InvariantCulture);
                };
                Deserializers[typeof(int?)] = delegate(string s)
                {
                    if (string.IsNullOrEmpty(s)) return null;
                    return int.Parse(s, CultureInfo.InvariantCulture);
                };
                Deserializers[typeof(uint?)] = delegate(string s)
                {
                    if (string.IsNullOrEmpty(s)) return null;
                    return uint.Parse(s, CultureInfo.InvariantCulture);
                };
                Deserializers[typeof(long?)] = delegate(string s)
                {
                    if (string.IsNullOrEmpty(s)) return null;
                    return long.Parse(s, CultureInfo.InvariantCulture);
                };
                Deserializers[typeof(ulong?)] = delegate(string s)
                {
                    if (string.IsNullOrEmpty(s)) return null;
                    return ulong.Parse(s, CultureInfo.InvariantCulture);
                };
                Deserializers[typeof(short?)] = delegate(string s)
                {
                    if (string.IsNullOrEmpty(s)) return null;
                    return short.Parse(s, CultureInfo.InvariantCulture);
                };
                Deserializers[typeof(ushort?)] = delegate(string s)
                {
                    if (string.IsNullOrEmpty(s)) return null;
                    return ushort.Parse(s, CultureInfo.InvariantCulture);
                };
                Deserializers[typeof(byte?)] = delegate(string s)
                {
                    if (string.IsNullOrEmpty(s)) return null;
                    return byte.Parse(s, CultureInfo.InvariantCulture);
                };
                Deserializers[typeof(bool?)] = delegate(string s)
                {
                    if (string.IsNullOrEmpty(s)) return null;
                    return bool.Parse(s);
                };
                
                Deserializers[typeof(MyFixedPoint)] = delegate(string s) { return (MyFixedPoint)double.Parse(s, CultureInfo.InvariantCulture); };
                Deserializers[typeof(MyObjectBuilderType)] = delegate(string s) { return MyObjectBuilderType.Parse("MyObjectBuilder_"+s); };
                
                Func<object, string> defaultTOS = delegate(object s)
                {
                    if (s == null) return "null";
                    switch (s)
                    {
                        case float f: return TrimZero(f.ToString(CultureInfo.InvariantCulture));
                        case double d: return TrimZero(d.ToString(CultureInfo.InvariantCulture));
                        case int i: return i.ToString(CultureInfo.InvariantCulture);
                        case uint ui: return ui.ToString(CultureInfo.InvariantCulture);
                        case long l: return l.ToString(CultureInfo.InvariantCulture);
                        case ulong ul: return ul.ToString(CultureInfo.InvariantCulture);
                        case short sh: return sh.ToString(CultureInfo.InvariantCulture);
                        case ushort ush: return ush.ToString(CultureInfo.InvariantCulture);
                        case byte b: return b.ToString(CultureInfo.InvariantCulture);
                        
                        case string ss: return ss.ToString(CultureInfo.InvariantCulture);
                        case MyFixedPoint mf: return TrimZero(((double)mf).ToString(CultureInfo.InvariantCulture));
                        case bool boo: return boo ? "true" : "false";
                    }

                    string TrimZero(string ss)
                    {
                        if (ss.EndsWith(".0"))
                        {
                            ss = ss.Replace(".0", "");
                        }
                        return ss;
                    }
                    
                    return s.ToString();
                };

                Serializers[typeof(byte)] = defaultTOS;
                Serializers[typeof(string)] = defaultTOS;
                Serializers[typeof(float)] = defaultTOS;
                Serializers[typeof(double)]= defaultTOS;
                Serializers[typeof(int)] = defaultTOS;
                Serializers[typeof(uint)] = defaultTOS;
                Serializers[typeof(long)] = defaultTOS;
                Serializers[typeof(ulong)] = defaultTOS;
                Serializers[typeof(short)] = defaultTOS;
                Serializers[typeof(ushort)] = defaultTOS;
                
                Serializers[typeof(byte?)] = defaultTOS;
                Serializers[typeof(float?)] = defaultTOS;
                Serializers[typeof(double?)]= defaultTOS;
                Serializers[typeof(int?)] = defaultTOS;
                Serializers[typeof(uint?)] = defaultTOS;
                Serializers[typeof(long?)] = defaultTOS;
                Serializers[typeof(ulong?)] = defaultTOS;
                Serializers[typeof(short?)] = defaultTOS;
                Serializers[typeof(ushort?)] = defaultTOS;
                
                
                Serializers[typeof(MyFixedPoint)] = defaultTOS;
                Serializers[typeof(bool)] = defaultTOS;
                Serializers[typeof(bool?)] = defaultTOS;
                
                
                Serializers[typeof(MyObjectBuilderType)] = delegate(object o) { return ((MyObjectBuilderType)o).ToString().Replace("MyObjectBuilder_", ""); };

                HashSet<Assembly> assemblies = new HashSet<Assembly>();
                assemblies.Add(typeof(MyObjectBuilder_CockpitDefinition).Assembly);
                assemblies.Add(typeof(MyObjectBuilder_MedicalRoomDefinition).Assembly);
                assemblies.Add(typeof(VoxelPlacementSettings).Assembly);
                assemblies.Add(typeof(MyPhysicalItemDefinition).Assembly);
                assemblies.Add(typeof(MyComponentDefinition).Assembly);
                assemblies.Add(typeof(SerializableVector3UByte).Assembly);
                assemblies.Add(typeof(MyPowerProducerDefinition).Assembly);
                assemblies.Add(typeof(ItemTypes).Assembly);
                assemblies.Add(typeof(MyObjectBuilder_SearchlightDefinition).Assembly);
                assemblies.Add(typeof(MyObjectBuilder_BlueprintDefinition).Assembly);
                assemblies.Add(typeof(MyTurretTargetingOptions).Assembly);
                assemblies.Add(typeof(MyEnvironmentDefinition).Assembly);

                foreach (var ass in AppDomain.CurrentDomain.GetAssemblies())
                {
                    assemblies.Add(ass);
                }
                //assemblies.Add(typeof(MyObjectBuilder_CockpitDefinition).Assembly);
                
                foreach (var a in assemblies)
                {
                    try
                    {
                        MyObjectBuilderType.RegisterFromAssembly(a);
                        
                        foreach (var t in a.GetTypes())
                        {
                            nameToClass[t.Name] = t;
                        }
                    }
                    catch (Exception e)
                    {
                        Output.Error(e, a.FullName+ " wasn't loaded!");
                    }
                }

                var wtf = 0;
            }

            public static Func<string, object> GetDeserializer(Type t)
            {
                if (t.IsEnum)
                {
                    return (s) =>
                    {
                        var split = s.Split(' ');
                        if (split.Length == 1) return Enum.Parse(t, s);

                        int sum = 0;
                        foreach (var ss in split)
                        {
                            var en = Enum.Parse(t, ss);
                            sum += Convert.ToInt32((Enum) en);
                        }

                        return Enum.ToObject(t, sum);
                        
                    };
                }

                if (Deserializers.ContainsKey(t))
                {
                    return Deserializers[t];
                }
                
                if (t.IsValueType && t.Name.StartsWith("Nullable"))
                {
                    t = t.GenericTypeArguments[0];
                }
                
                if (!Deserializers.ContainsKey(t))
                {
                    return null;
                }
                return Deserializers[t];
            }
            
            public static Func<object, string> GetSerializer(Type t)
            {
                if (t.IsEnum)
                {
                    return (o) => { return o.ToString().Replace(",", ""); };
                }

                if (t.IsValueType && t.Name.StartsWith("Nullable"))
                {
                    t = t.GenericTypeArguments[0];
                }
                
                if (!Serializers.ContainsKey(t))
                {
                    return null;
                }
                return Serializers[t];
            }
            
            public static Type GetTypeForName(string name)
            {
                if (!nameToClass.ContainsKey(name))
                {
                    throw new Exception($"There is no type for name {name}");
                }
                return nameToClass[name];
            }
            
            
            public static bool IsInnerText(XmlElement element, out string value)
            {
                value = null;
                if (!element.HasChildNodes)
                {
                    value = element.InnerText;
                    return true;
                }

                foreach (var c in element.ChildNodes)
                {
                    if (c is XmlText txt)
                    {
                        value = txt.Value;
                        return true;
                    }
                }

                return false;
            }
            
            public static int CountElements(XmlElement element)
            {
                if (!element.HasChildNodes)
                {
                    return 0;
                }

                int x = 0;

                foreach (var c in element.ChildNodes)
                {
                    if (c is XmlElement txt)
                    {
                        x++;
                    }
                }

                return x;
            }

            
            public static bool CustomDeserializers(Type t, XmlElement element, out object value)
            {
                if (t == typeof(SerializableDefinitionId))
                {
                    return CustomIdDeserializer(t, element, out value);
                }

                value = null;
                return false;
            }
            
            private static bool CustomIdDeserializer(Type t, XmlElement element, out object value)
            {
                string Type = null;
                    string TypeId = null;
                    string Subtype = null;
                    string SubtypeId = null;
                    
                    foreach (var att in element.Attributes)
                    {
                        var attr = (XmlAttribute) att;
                        if (attr.Name == "Type")
                        {
                            Type = (string)XReader.Cast(attr.Value, typeof(string));
                        } else if (attr.Name == "Subtype")
                        {
                            Subtype = (string)XReader.Cast(attr.Value, typeof(string));
                        }
                    }
                    
                    foreach (var att in element.ChildNodes)
                    {
                        if (att is XmlElement innerElement)
                        {
                            var v = innerElement.InnerText;
                            var n = innerElement.Name;
                            
                            if (n == "TypeId")
                            {
                                TypeId = (string)XReader.Cast(v, typeof(string));
                            } else if (n == "SubtypeId")
                            {
                                SubtypeId = (string)XReader.Cast(v, typeof(string));
                            }
                        }
                    }

                    var serializableDefinitionId = new SerializableDefinitionId();
                    if (Type != null)
                    {
                        serializableDefinitionId.TypeIdStringAttribute = Type;
                    }
                    if (Subtype != null)
                    {
                        serializableDefinitionId.SubtypeIdAttribute = Subtype;
                    }
                    
                    if (TypeId != null)
                    {
                        serializableDefinitionId.TypeIdString = "MyObjectBuilder_"+TypeId;
                    }
                    if (SubtypeId != null)
                    {
                        serializableDefinitionId.SubtypeId = SubtypeId;
                    }

                    value = serializableDefinitionId;
                    return true;
            }
            
            public static bool CustomSerializers(object data, XmlElement element, XmlDocument doc)
            {
                if (data.GetType() == typeof(SerializableDefinitionId))
                {
                    if (element.Name != "DeconstructId")
                    {
                        var type = doc.CreateElement("TypeId");
                        var sub = doc.CreateElement("SubtypeId");
                        var sdi = (SerializableDefinitionId) data;
                        type.InnerText = sdi.TypeId.ToString()?.Replace("MyObjectBuilder_", "");
                        if (String.IsNullOrEmpty(sdi.SubtypeId))
                        {
                            sub.InnerXml = "";
                        }
                        else
                        {
                            sub.InnerText = sdi.SubtypeId;
                        }
                        
                        element.AppendChild(type);
                        element.AppendChild(sub);
                    }
                    else
                    {
                        var sdi = (SerializableDefinitionId) data;
                        var type = doc.CreateElement("TypeId");
                        var sub = doc.CreateElement("SubtypeId");

                        type.InnerText = sdi.TypeId.ToString()?.Replace("MyObjectBuilder_", "");;
                        if (String.IsNullOrEmpty(sdi.SubtypeId))
                        {
                            sub.InnerXml = "";
                        }
                        else
                        {
                            sub.InnerText = sdi.SubtypeId;
                        }
                        
                        
                        element.AppendChild(type);
                        element.AppendChild(sub);
                    }
                    return true;
                }
                
                return false;
            }

            public static bool ShouldSerialize(MemberInfo member)
            {
                if (member.DeclaringType == typeof(SerializableVector3) && member.Name == "IsZero")
                {
                    return false;
                }
                
                if (member.Name == "SubpartPairing")//FUCK THIS
                {
                    return false;
                }
                
                if (member.DeclaringType == typeof(Vector4) && member.Name == "Item")
                {
                    return false;
                }
                if (member.DeclaringType == typeof(MyObjectBuilder_Base) && member.Name == "SubtypeId")
                {
                    return false;
                }
                if (member.DeclaringType == typeof(MyObjectBuilder_Base) && member.Name == "TypeId")
                {
                    return false;
                }
                return true;
            }

            
            
            public static bool ShouldSerialize(MemberInfo member, object data)
            {
                if (member.Name == "DeconstructId")
                {
                    var d = (SerializableDefinitionId) data;
                    if (string.IsNullOrEmpty(d.SubtypeName) && d.TypeId == MyObjectBuilderType.Invalid)
                    {
                        return false;
                    }
                }

                
                return true;
            }

            public static object CreateInstance(Type innerType)
            {
                if (innerType == typeof(string))
                {
                    return "";
                }
                return Activator.CreateInstance(innerType);
            }
        }
}