using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;
using VRage;

namespace BlockEditor.Serialization
{
    public class XReader
        {
            
            public T Read<T>(string file)
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(file);

                var root = doc.DocumentElement;

                var t = Activator.CreateInstance<T>();
                
                if (X.CustomDeserializers(typeof(T), root, out var custom))
                {
                    return (T)custom;
                }
                
                Read(t, root);
                
                return t;
            }


            
            
            public static object Cast(string value, Type t)
            {
                if (value == null) throw new Exception();
                var d = X.GetDeserializer(t);
                if (d == null)
                {
                    throw new Exception($"No deserializer for type {t} value={value}");
                }

                try
                {
                    var v = d.Invoke(value);
                    return v;
                }
                catch (Exception e)
                {
                    throw new Exception($"No deserializer for type {t} value={value}");
                }
                
            }

            private void ReadWrapper(object o, XmlElement element)
            {
                if (element.Name == "Center")
                {
                    var wtf = 0;
                }
                //var type = element.Attributes?["xsi:type"] ?? null;

                var t = o.GetType();
                var cache = ClassCache.Get(t);
                foreach (var att in element.Attributes)
                {
                    var attr = (XmlAttribute) att;
                    var member = cache.GetAttributeMember(attr.Name);
                    
                    Type tt = member.GetMemberType();
                    if (member != null)
                    {
                        member.SetValue(o, Cast(attr.Value, tt));
                    }
                }

                foreach(XmlNode node in element.ChildNodes)
                {
                    
                    if (node is XmlElement innerElement)
                    {
                        var member = cache.GetMember(innerElement.Name);
                        if (member == null)
                        {
                            continue;
                        }

                        Type tt = member.GetMemberType();
                        var xsitypeS = node.Attributes?["xsi:type"]?.Value ?? null;

                        Type xsitype = tt;
                        if (xsitypeS != null)
                        {
                            xsitype = X.GetTypeForName(xsitypeS);
                        }
                        
                        if (tt.IsArray)
                        {
                            if (!innerElement.HasChildNodes)
                            {
                                member.SetValue(o, null);
                                continue;
                            } else if (X.IsInnerText(innerElement, out var innerText)) {
                                var v = Cast(innerText, tt.GetElementType());
                                var arr = Array.CreateInstance(tt.GetElementType(), 1);
                                arr.SetValue(v, 0);
                                member.SetValue(o, arr);
                                continue;
                            } else {
                                var elementType = tt.GetElementType();
                                var arr = Array.CreateInstance(elementType, X.CountElements(innerElement));
                                int x = 0;
                                foreach (var innerElement2 in innerElement.ChildNodes)
                                {
                                    if (innerElement2 is XmlElement innerElement22)
                                    {
                                        var arrayElement = ReadArrayElement(elementType, innerElement22);
                                        arr.SetValue(arrayElement, x);
                                        x++;
                                    }
                                }
                                member.SetValue(o, arr);
                                continue;
                            }
                        } else if (tt.ImplementsGenericInterface(typeof(IList<>))) {
                            if (!innerElement.HasChildNodes)
                            {
                                member.SetValue(o, null);
                                continue;
                            } 
                            
                            var arr = (IList)Activator.CreateInstance(tt);
                            member.SetValue(o, arr);
                            var elementType = tt.GenericTypeArguments[0];
                            
                            
                            if (X.IsInnerText(innerElement, out var innerText)) {
                                var v = Cast(innerText, tt.GetElementType());
                                arr.Add(v);
                                continue;
                            } else {
                                foreach (var innerElement2 in innerElement.ChildNodes)
                                {
                                    if (innerElement2 is XmlElement innerElement22)
                                    {
                                        var arrayElement = ReadArrayElement(elementType, innerElement22);
                                        arr.Add(arrayElement);
                                    }
                                }
                                
                                member.SetValue(o, arr);
                                continue;
                            }
                        } else {
                            
                            if (X.CustomDeserializers(tt, innerElement, out var custom))
                            {
                                member.SetValue(o, custom);
                                continue;
                            }
                            
                            if (X.GetDeserializer(tt) != null)
                            {
                                if (X.IsInnerText(innerElement, out var innerText))
                                {
                                    var v = Cast(innerText, tt);
                                    member.SetValue(o, v);
                                    continue;
                                }
                            }


                            var oo = Activator.CreateInstance(xsitype);
                            var isNullable = xsitype.IsValueType && xsitype.Name.StartsWith("Nullable");
                            if (isNullable)
                            {
                                xsitype = xsitype.GenericTypeArguments[0];
                                oo = Activator.CreateInstance(xsitype);
                            }

                            if (oo == null) throw new Exception("Cant create: " + xsitype.Name);
                            Read(oo, innerElement);
                            member.SetValue(o, oo);
                        }
                    }
                }
            }

            private void Read(object o, XmlElement element)
            {
                try
                {
                    ReadWrapper(o, element);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            private object ReadArrayElement(Type elementType, XmlElement innerElement22)
            {
                var xsiType = innerElement22.Attributes["xsi:type"];
                if (xsiType != null && xsiType.Value != null && xsiType.Value != "")
                {
                    elementType = X.GetTypeForName(xsiType.Value);
                }

                var deserializer = X.GetDeserializer(elementType);
                if (deserializer != null)
                {
                    if (X.IsInnerText(innerElement22, out var txt))
                    {
                        return deserializer.Invoke(txt);
                    }
                    else
                    {
                        throw new Exception("WTF?");
                    }
                }
                
                if (X.CustomDeserializers(elementType, innerElement22, out var custom))
                {
                    return custom;
                }

                if (elementType == null)
                {
                    var wtf = 0;
                } 
                var ooo = Activator.CreateInstance(elementType);
                Read(ooo, innerElement22);
                return ooo;
            }
   
        }
}