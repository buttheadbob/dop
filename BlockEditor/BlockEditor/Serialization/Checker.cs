﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Documents;
using System.Xml;
using VRage.Game;

namespace BlockEditor.Serialization
{
    public class Checker
    {
        public static void Check(string file)
        {
            var r = new XReader().Read<MyObjectBuilder_Definitions>(file);
            var xml2 = new XWriter().Write(r);
            var xml1 = File.ReadAllText(file);
            
            File.WriteAllText(file+".xml",xml2);
            
            var doc1 = new XmlDocument();
            doc1.LoadXml(xml1);
            var doc2 = new XmlDocument();
            doc2.LoadXml(xml2);

            Clean(doc1);
            Clean(doc2);
            

            CheckLevel("/", doc1, doc2);
        }

        public static void Clean(XmlNode node)
        {
            var toDelete = new List<XmlNode>(); 
            for (var x =0; x<node.ChildNodes.Count; x++)
            {
                var n = node.ChildNodes[x];
                if (n is XmlComment)
                {
                    toDelete.Add(n);
                    continue;
                }

                if (n is XmlDeclaration)
                {
                    continue;
                }
                
                if (n is XmlText)
                {
                    continue;
                }
                
                
                if (!(n is XmlElement))
                {
                    toDelete.Add(n);
                }
            }

            foreach (var x in toDelete)
            {
                node.RemoveChild(x);
            }
            
            for (var x =0; x<node.ChildNodes.Count; x++)
            {
                Clean(node.ChildNodes[x]);
            }
            
        }

        public static void CheckLevel(string path, XmlNode l1, XmlNode l2)
        {
            if (l1.Name != l2.Name)
            {
                Output.Info($"Different Names: {path}: {(l1.Name)} {(l2.Name)}");
            }

            
            if (l1.Value != l2.Value)
            {
                if (double.TryParse(l1.Value, out var d1) && double.TryParse(l1.Value, out var d2) && d1 == d2)
                {
                    
                }
                else
                {
                    Output.Info($"Attributes missmatch: {path} [{l1.Value}]: [{l1.Value}]");
                }
                
            }
            
            if (l1.Attributes != null && l2.Attributes == null || l1.Attributes == null && l2.Attributes != null)
            {
                Output.Info($"Attributes missmatch: {path}: {(l1.Attributes != null)} {(l2.Attributes != null)}");
            }
            else
            {
                if (l1.Attributes != null && l2.Attributes != null)
                {
                    for (var x=0; x<l1.Attributes.Count; x++)
                    {
                        var n1 = l1.Attributes[x];
                        var n2 = l2.Attributes[n1.Name];

                        if (n2 == null)
                        {
                            Output.Info($"Attributes missmatch: {path} {n1.Name}: l2 doesn't exist");
                            continue;
                        }
                
                        if (n1.Value != n2.Value)
                        {
                            if (double.TryParse(n1.Value, out var d1) && double.TryParse(n2.Value, out var d2) && d1 == d2)
                            {
                                continue;
                            }
                            Output.Info($"Attributes missmatch: {path} [{n1.Value}]: [{n2.Value}]");
                        }
                    }
                }
            }

            Dictionary<string, List<XmlNode>> ll2 = new Dictionary<string, List<XmlNode>>();
            for (var x =0; x<l2.ChildNodes.Count; x++)
            {
                ll2.GetOrNew(l2.ChildNodes[x].Name).Add(l2.ChildNodes[x]);
            }

            //if (l1.ChildNodes.Count != l2.ChildNodes.Count)
            //{
            //    Output.Info($"Elements different childs amount: {path}: {(l1.ChildNodes.Count)} {(l2.ChildNodes.Count)}");
            //}
            
            for (var x =0; x<l1.ChildNodes.Count; x++)
            {
                if (!ll2.ContainsKey(l1.ChildNodes[x].Name))
                {
                    Output.Info($"L2 dont have {l1.ChildNodes[x].Name} at all {path}");
                    continue;
                }

                var elms = ll2[l1.ChildNodes[x].Name];
                if (elms.Count == 0)
                {
                    Output.Info($"L2 dont have LEFT {l1.ChildNodes[x].Name} at {path}");
                    continue;
                }

                var node2 = elms[0];
                elms.RemoveAt(0);
                CheckLevel(path + $"{l1.Name}/{x}/", l1.ChildNodes[x], node2);
            }
        }
    }
}