﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using BlockEditor.Editors;
using VRage.Generics;

namespace BlockEditor
{
    public class GUI
    {

        public static ObjectsPool<TextEditor> TextEditors = new ObjectsPool<TextEditor>(0, () =>
        {
            var ed = new TextEditor();
            ed.VerticalAlignment = VerticalAlignment.Top;
            ed.HorizontalAlignment = HorizontalAlignment.Stretch;
            ed.Margin = new Thickness(0, 0, 0, 0);
            return ed;
        }, editor => editor.Clear());
        
        public static ObjectsPool<ArrayEditor> ArrayEditors = new ObjectsPool<ArrayEditor>(0, () =>
        {
            var ed = new ArrayEditor();
            ed.VerticalAlignment = VerticalAlignment.Top;
            ed.HorizontalAlignment = HorizontalAlignment.Stretch;
            ed.Margin = new Thickness(0, 0, 0, 0);
            return ed;
        }, editor => editor.Clear());
        
        public static ObjectsPool<PropertyEditor> PropertyEditors = new ObjectsPool<PropertyEditor>(0, () =>
        {
            var ed = new PropertyEditor();
            ed.VerticalAlignment = VerticalAlignment.Top;
            ed.HorizontalAlignment = HorizontalAlignment.Stretch;
            ed.Margin = new Thickness(0, 0, 0, 0);
            return ed;
        }, editor => editor.Clear());
        
        public static ObjectsPool<EnumEditor> EnumEditors = new ObjectsPool<EnumEditor>(0, () =>
        {
            var ed = new EnumEditor();
            ed.VerticalAlignment = VerticalAlignment.Top;
            ed.HorizontalAlignment = HorizontalAlignment.Stretch;
            ed.Margin = new Thickness(0, 0, 0, 0);
            return ed;
        }, editor => editor.Clear());
        
        public static ObjectsPool<VectorEditor> VectorEditors = new ObjectsPool<VectorEditor>(0, () =>
        {
            var ed = new VectorEditor();
            ed.VerticalAlignment = VerticalAlignment.Top;
            ed.HorizontalAlignment = HorizontalAlignment.Stretch;
            ed.Margin = new Thickness(0, 0, 0, 0);
            return ed;
        }, editor => editor.Clear());
        
            /*
             * VectorEditor GenerateVectorEditor(MemberInfo member, int rowIndex, int colIndex = 0)
        {
            var ve = new VectorEditor();
            
            ve.SetValue(Grid.RowProperty, rowIndex);
            ve.SetValue(Grid.ColumnProperty, colIndex);
            
            ve.Header.Text = member.Name;
            
            Grid.Children.Add(ve);
            
            return ve;
        }
             */

        private static long RadioButtonId;
        public static ObjectsPool<RadioEditor> RadioEditors = new ObjectsPool<RadioEditor>(0, () =>
        {
            var ed = new RadioEditor();
            ed.VerticalAlignment = VerticalAlignment.Top;
            ed.HorizontalAlignment = HorizontalAlignment.Stretch;
            ed.Margin = new Thickness(0, 0, 0, 0);

            var n = "RadioGroup_" +Interlocked.Increment(ref RadioButtonId);
            ed.RadioButton1.GroupName = n;
            ed.RadioButton2.GroupName = n;
            ed.RadioButton3.GroupName = n;
            
            return ed;
        }, editor => editor.Clear());

        public static void Dealloc(object a)
        {
            if (a is TextEditor te)
            {
                GUI.TextEditors.Deallocate(te);
            }
            else if (a is PropertyEditor pe)
            {
                GUI.PropertyEditors.Deallocate(pe);
            } else if (a is EnumEditor ee)
            {
                GUI.EnumEditors.Deallocate(ee);
            } else if (a is ArrayEditor ae)
            {
                GUI.ArrayEditors.Deallocate(ae);
            } else if (a is RadioEditor re)
            {
                GUI.RadioEditors.Deallocate(re);
            }
            else
            {
                return;
            }
        }
        
        
        
        
        public static RadioEditor GenerateRadioEditor(String name, int rowIndex, int colIndex=0)
        {
            var rd = new RadioEditor();
            
            rd.SetValue(Grid.RowProperty, rowIndex);
            rd.SetValue(Grid.ColumnProperty, colIndex);

            

            
            return rd;
        }

        public static EnumEditor GenerateEnumEditor(MemberInfo member, int rowIndex, int colIndex = 0)
        {
            var en = new EnumEditor();
            
            en.SetValue(Grid.RowProperty, rowIndex);
            en.SetValue(Grid.ColumnProperty, colIndex);

            en.Header.Text = member.Name;
            return en;
        }

        private static List<Action> toInvoke = new List<Action>();

        public static void Execute()
        {
            Action[] att;
            lock (toInvoke)
            {
                att = toInvoke.ToArray();
                toInvoke.Clear();
            }

            var s = new Stopwatch();
            s.Start();
            
            
            Parallel.ForEach(att, (x) =>
            {
                x.Invoke();
            });

            var elapsed = s.Elapsed;
            //Output.Info("Executed: " + elapsed + " " + att.Length);
            s.Stop();
        }

        public static void InvokeParallelSilent(Action a)
        {
            Action b = () =>
            {
                try
                {
                    a();
                }
                catch (Exception e)
                {
                    Output.Verbose(e);
                }
            };

            lock (b)
            {
                toInvoke.Add(b);
            }
            
            Parallel.Invoke();
            //Parallel.Invoke();
        }
        public static void InvokeParallel(Action a)
        {
            Action b = () =>
            {
                try
                {
                    a();
                }
                catch (Exception e)
                {
                    Output.Error(e);
                }
            };

            lock (b)
            {
                toInvoke.Add(b);
            }
            
            Parallel.Invoke();
            //Parallel.Invoke();
        }

        public static void InvokeGui(Action a)
        {
            App.Dispatcher.BeginInvoke(a);
        }
        
        public static void InvokeGui(UserControl uc, Action a)
        {
            uc.Dispatcher.BeginInvoke(a);
        }

        public static void RemoveFromParent(UserControl uc)
        {
            if (uc.Parent == null) return;
            
            if (uc.Parent is ScrollViewer sv)
            {
                sv.Content = null;
                return;
            }

            if (uc.Parent is Panel panel)
            {
                panel.Children.Remove(uc);
                return;
            }

            var wtf = 0;
            return;
        }
    }
}