﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace BlockEditor
{
    public static class IconMerger
    {
        
        public static void MergeFolders(string path1, string path2, string output)
        {
            if (!Directory.Exists(path1)) { return; }
            if (!Directory.Exists(path2)) { return; }
            if (!Directory.Exists(output)) { return; }

            Dictionary<string, Bitmap> fileToBitmap = new Dictionary<string, Bitmap>();
            foreach (var load in Directory.EnumerateFiles(path2))
            {
                try
                {
                    var bitmap = (Bitmap)Bitmap.FromFile(load);
                    fileToBitmap.Add(Path.GetFileNameWithoutExtension(load), bitmap);
                } catch (Exception e) {
                    Output.Error(e);
                    continue;
                }
            }

            Directory.EnumerateFiles(path1, "*.png", SearchOption.AllDirectories).ForEach((load) =>
            {
                try
                {
                    var p = Path.GetFullPath(load)
                        .Replace(Path.GetFullPath(path1) + Path.DirectorySeparatorChar, "")
                        .Replace(Path.GetFileName(load), "");

                    var bitmap = (Bitmap) Bitmap.FromFile(load);

                    foreach (var pair in fileToBitmap)
                    {
                        var name = Path.GetFileNameWithoutExtension(load) + "_" + pair.Key + Path.GetExtension(load);
                        var folder = Path.Combine(output, p);
                        var file = Path.Combine(folder, name);
                        if (!Directory.Exists(folder))
                        {
                            Directory.CreateDirectory(folder);
                        }
                        Merge(bitmap, file, pair.Value);
                    }

                }
                catch (Exception e)
                {
                    Output.Error(e);
                }
            });
            
        }

        public static void Merge(string source1, string output, string source2)
        {
            Merge((Bitmap)Bitmap.FromFile(source1), output, (Bitmap)Bitmap.FromFile(source2));
        }
        
        public static void Merge(this Bitmap source1, string output, params Bitmap[] source2)
        {
             // your source images - assuming they're the same size
            
            var target = new Bitmap(source1.Width, source1.Height, PixelFormat.Format32bppArgb);
            var graphics = Graphics.FromImage(target);
            graphics.CompositingMode = CompositingMode.SourceOver; // this is the default, but just to be clear

            graphics.DrawImage(source1, new RectangleF(0,0, source1.Width, source1.Height), new RectangleF(0,0, source1.Width, source1.Height), GraphicsUnit.Pixel);

            foreach (var b in source2)
            {
                graphics.DrawImage(b, new RectangleF(0,0, source1.Width, source1.Height), new RectangleF(0,0, b.Width, b.Height), GraphicsUnit.Pixel);
                b.Dispose();
            }

            
            //return graphics;

            target.Save(output, ImageFormat.Png);
            source1.Dispose();
            
        }
    }
}