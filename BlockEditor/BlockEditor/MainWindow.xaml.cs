﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml;
using BlockEditor.Editors;
using BlockEditor.GuiData;
using BlockEditor.Serialization;
using BlockEditor.Wpf;
using NLog.Fluent;
using RestSharp.Serializers;
using Sandbox.Definitions;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.ObjectBuilders;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.MessageBox;
using MessageBoxResult = System.Windows.MessageBoxResult;
using Timer = System.Threading.Timer;
using UserControl = System.Windows.Controls.UserControl;

namespace BlockEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {

        public static MainWindow INSTANCE;
        
        public MainWindow()
        {
            INSTANCE = this;
            App.Dispatcher = Dispatcher;
            InitializeComponent();
            X.Init();

            Settings.Init();


            GlobalEditor.ModsChanged += () => ShowTree(TextFilter);
            ScriptManager.Init();

            
            
            Script.Text = ScriptManager.SCRIPT_EXAMPLE;
            if (Settings.Data.TryGetValue(Settings.DefaultScript, out var scriptpath))
            {
                var tt = File.ReadAllText(scriptpath);
                try
                {
                    Script.Text = tt;
                    OnClickCompile(null, null);
                }
                catch (Exception e)
                {
                    Output.Error(e, "Error compiling script");
                }
                
                
                DispatcherTimer t = new DispatcherTimer();
                t.Interval = new TimeSpan(0,0, 0, 0, 500);
                t.Tick += ((sender, args) =>
                {
                    if (File.Exists(scriptpath))
                    {
                        var txt = File.ReadAllText(scriptpath);
                        GUI.InvokeGui(() =>
                        {
                            Script.Text = txt;
                        });
                    }
                });
                t.Start();
            }

            var strings = Settings.Data[Settings.DefaultLoadedPaths].Split(",", StringSplitOptions.RemoveEmptyEntries);
            foreach (var s in strings)
            {
                if (Directory.Exists(s) || File.Exists(s))
                {
                    GlobalEditor.Load(s, "Base", AfterLoad:(m) =>
                    {
                        if (s.Contains(Settings.Data[Settings.GameContentPath]))
                        {
                            foreach (var df in m.Definitions)
                            {
                                df.ReadOnly = true;
                            }
                        }
                    });
                }
                
            }
        }

        private void DefinitionsTree_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var list = new List<object>();
            foreach (var selected in DefinitionsTree.SelectedItems)
            {
                var treeItem = (TreeViewItem) (((TextBlock) selected).Parent);
                var d = treeItem.Tag;
                if (d is Mod) continue;
                if (d is DefinitionFile) continue;
                list.Add(d);
            }

            if (list.Count > 0)
            {
                if (EditorArea.Content != null)
                {
                    GUI.Dealloc(EditorArea.Content);
                }
                var editor = new PropertyEditor();
                editor.Edit(list[0].GetType(), list, null, favoriteFields:Settings.Data[Settings.FavoriteFields].Split(" ", ","), favoritesOnly:ShowFavoritesOnly);
                editor.OnChanged += EditorOnOnChanged;
                editor.OnShowFull += () =>
                {
                    ShowFavoritesOnly = !ShowFavoritesOnly;
                    var editor2 = new PropertyEditor();
                    editor2.Edit(list[0].GetType(), list, null, favoriteFields:Settings.Data[Settings.FavoriteFields].Split(" ", ","), favoritesOnly:ShowFavoritesOnly);
                    editor2.OnChanged += EditorOnOnChanged;
                    EditorArea.Content = editor2;
                    editor2.Copy(editor);
                };
                EditorArea.Content = editor;
            }

            
        }

        public static bool ShowFavoritesOnly = true;

        private void EditorOnOnChanged(UserControl obj)
        {
            var xml = ((PropertyEditor) obj).toEditObjects[0];

            GUI.InvokeParallel(() =>
            {
                var txt = new XWriter().Write(xml);
                GUI.InvokeGui(() =>
                {
                    XMLPreview.Text = txt;
                });
            });
        }


        public bool TextFilter(object d)
        {
            if (Search.Text.Length == 0) return true;

            if (d is MyObjectBuilder_DefinitionBase db)
            {
                return db.Id.ToString().Replace("MyObjectBuilder_", "").Contains(Search.Text);
            }
            
            return d.ToString().Contains(Search.Text);
        }

        private void OnClickCompile(object sender, RoutedEventArgs e)
        {
            var assembly = ScriptManager.Compile(Script.Text, out var errors);
            if (errors != null)
            {
                var sb = new StringBuilder();
                foreach (var ee in errors)
                {
                    sb.Append($"{ee.Id}: {ee.GetMessage()}\n\n");
                }
                
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Warning;

                MessageBox.Show(sb.ToString(), "Errors", button, icon, MessageBoxResult.Yes);
                return;
            }

            try
            {
                var type = assembly.GetType("Script");
                var obj = Activator.CreateInstance(type);
                var method = type.GetMethod("Apply", BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic);
                method.Invoke(obj, new object[0]);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString(), "Errors",  MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.Yes);
            }
        }

        public void ShowTree(Func<object, bool> filter)
        {
            var items = ExtendedTreeView.GetTreeViewItems(DefinitionsTree, false);

            var expanded = new List<object>();
            foreach (var item in items)
            {
                if (item.IsExpanded)
                {
                    expanded.Add(item.Tag);
                }
            }
            
            DefinitionsTree.Items.Clear();
            TreeViewItem modTree, defTree;
            
            DefinitionsTree.Visibility = Visibility.Collapsed;
            var s = DateTime.Now;
            foreach (var m in GlobalEditor.Mods)
            {
                modTree = Generate(m, expanded);
                DefinitionsTree.Items.Add(modTree);
                
                defTree = null;
                
                foreach (var d in m.Definitions)
                {
                    if (d.AlwaysVisible || d.AllItems.Count == 0)
                    {
                        defTree = Generate(d, expanded);
                        modTree.Items.Add(defTree);
                    }
                    
                    foreach (var kv in d.AllItems)
                    {
                        if (filter(kv))
                        {
                            var defItem = Generate(kv, expanded);
                            if (defTree == null)
                            {
                                defTree = Generate(d, expanded);
                                modTree.Items.Add(defTree);
                            }
                            defTree.Items.Add(defItem);
                        }
                    }
                    defTree = null;
                }
            }

            DefinitionsTree.Visibility = Visibility.Visible;
        }

        private string GetElementName(object tag)
        {
            if (tag == null) return "null";

            if (tag is DefinitionFile df)
            {
                return df.File.Replace(df.Mod.RootFolder, "") + (df.ReadOnly ? " (ReadOnly)" : "");
            }
            
            if (tag is Mod mod)
            {
                return mod.RootFolder;
            }

            if (tag is MyObjectBuilder_DefinitionBase db)
            {
                return db.Id.ToString().Replace("MyObjectBuilder_", "");
            }

            return tag.ToString();
        }

        private TreeViewItem Generate(object tag, List<object> expanded)
        {
            var txt = new TextBlock();
            txt.Text = GetElementName(tag);
            txt.Tag = tag;
            var modTree = new TreeViewItem();
            modTree.Tag = tag;
            modTree.Header = txt;

            if (tag is DefinitionFile)
            {
                modTree.IsExpanded = expanded.Contains(tag);
            }
            else
            {
                modTree.IsExpanded = true;
            }
            
            return modTree;
        }

        private void CheckXML(object sender, RoutedEventArgs e)
        {
            var files = Directory.GetFiles(@"D:\SE\Sources\SpaceEngineers\Content\Data\CubeBlocks", "*.sbc", SearchOption.TopDirectoryOnly);
            //foreach (var x in files)
            Parallel.ForEach(files, (x) =>
            {
                try
                {
                    Checker.Check(x);
                }
                catch (Exception ee)
                {
                    Output.Error(ee, $"Couldn't load:");
                }
            });
        }

        public void SaveLogic(object sender, RoutedEventArgs e)
        {
            var items = DefinitionsTree.SelectedItems;
            var files = new List<DefinitionFile>();
            foreach (var tb in items)
            {
                var o = (tb as TextBlock).Tag;
                if (o is DefinitionFile df)
                {
                    files.Add(df);
                }
                if (o is Mod md)
                {
                    foreach (var ddf in md.Definitions)
                    {
                        files.Add(ddf);
                    }
                }
            }
            
            Parallel.ForEach(files, (o) =>
            {
                try
                {
                    o.Save();
                }
                catch (Exception ee)
                {
                    Output.Error(ee, "Was unable to save: "+ o.File);   
                }
            });
            
        }
        
        public void LoadModLogic(object sender, RoutedEventArgs e)
        {
            var dlg = new FolderPicker();
            if (dlg.ShowDialog() == true)
            {
                if (dlg.ResultPath != null && Directory.Exists(dlg.ResultPath))
                {
                    GlobalEditor.Load(dlg.ResultPath, dlg.ResultPath, AfterLoad: (m) =>
                    {
                        foreach (var d in m.Definitions)
                        {
                            d.AlwaysVisible = true;
                        }
                    });
                }
            }
        }

        private List<object> Copy = new List<object>();
        
        
        private void MainWindow_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                if (e.Key == Key.C)
                {
                    CopyLogic();
                    return;
                }

                if (e.Key == Key.X)
                {
                    CopyLogic();
                    RemoveLogic();
                    return;
                }
                
                if (e.Key == Key.D)
                {
                    RemoveLogic();
                    return;
                }
                
                if (e.Key == Key.V)
                {
                    PasteCopiedLogic();
                    return;
                }
                
                if (e.Key == Key.S)
                {
                    SaveLogic(null, null);
                    return;
                }
            }
        }

        public void RemoveLogic()
        {
            foreach (var item in DefinitionsTree.SelectedItems)
            {
                var tag = (item as TextBlock).Tag;

                if (tag is DefinitionFile df)
                {
                    df.Mod.Definitions.Remove(df);
                } else if (tag is Mod mod)
                {
                    GlobalEditor.Mods.Remove(mod);
                } else {
                    foreach (var m in GlobalEditor.Mods)
                    {
                        foreach (var dff in m.Definitions)
                        {
                            dff.AllItems.Remove(tag);
                        }
                    }
                }
            }
            
            ShowTree(TextFilter);
        }

        public void CopyLogic()
        {
            Copy.Clear();

            var originals = Sugar.Flatten(DefinitionsTree.SelectedItems);

            foreach (var o in originals)
            {
                var clone = new FastDeepCloner.FastDeepCloner(o).Clone();
                Copy.Add(clone);
            }
        }
        
        public void PasteCopiedLogic()
        {
            if (DefinitionsTree.SelectedItems.Count != 1)
            {
                MessageBox.Show("Must select one item only");
                return;
            }

            var tag = (DefinitionsTree.SelectedItems[0] as TextBlock).Tag;

            var file = tag as DefinitionFile;
            if (file == null)
            {
                MessageBox.Show("Can paste only at 2nd level");
                return;
            }


            if (Copy == null)
            {
                return;
            } 
                    
            file.AllItems.AddRange(Copy);
            ShowTree(TextFilter);
        }

        public void RefreshSearch(object sender, RoutedEventArgs e)
        {
            if (ExtendedSearch.IsChecked == true)
            {
                ShowTree(ScriptManager.CreateFilter(Search.Text));
            }
            else
            {
                ShowTree(TextFilter);
            }
        }

        private void DefinitionsTree_OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            GlobalEditor.ShowContextMenu(sender, e);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            LogOutput.Text = "";
            ErrorLogOutput.Text = "";
            VerboseLogOutput.Text = "";
        }
    }
}