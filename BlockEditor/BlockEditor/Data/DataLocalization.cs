﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BlockEditor.SE
{
    public class Localization
    {
        [XmlArrayItem("Item")]
        [XmlArray("Items")]
        public List<LocalizationLanguage> Items = new List<LocalizationLanguage>();
        
        [XmlArrayItem("Give")]
        [XmlArray("Gives")]
        public List<FormulaTranslation> Gives = new List<FormulaTranslation>();
    }

    public class LocalizationLanguage
    {
        [XmlAttribute("Key")] 
        public string Key;

        [XmlArrayItem("Translate")]
        [XmlArray("Translate")]
        public List<LocalizationItem> Translate = new List<LocalizationItem>();

        [XmlArrayItem("Give")]
        [XmlArray("Gives")]
        public List<Gives> Gives = new List<Gives>();
    }

    public class LocalizationSize
    {
        [XmlAttribute("Size")] 
        public string Size;

        [XmlAttribute("Translation")] 
        public string Translation;
    }
    

    public class Gives
    {
        [XmlAttribute("Key")] 
        public string Key;
        
        [XmlAttribute("Arg1")] 
        public string Arg1;
        
        [XmlAttribute("Arg2")] 
        public string Arg2;
        
        [XmlAttribute("Arg3")] 
        public string Arg3;
    }
    
    public class FormulaLang
    {
        [XmlAttribute("Key")] 
        public string Key;
        
        [XmlText] 
        public string Value = "[+{0}% Speed]";
    }

    public class FormulaTranslation
    {
        [XmlAttribute("Key")] 
        public string Key;

        [XmlArray("Translations")] 
        [XmlArrayItem("Translation")] 
        public List<FormulaLang> Translations = new List<FormulaLang>();
    }
    
    public class LocalizationItem
    {
        [XmlAttribute("Key")] 
        public string Key;
        
        [XmlElement("Name")]
        public string Name;
        
        [XmlElement("Description")]
        public string Description;
        
        [XmlElement("DescriptionExtra")]
        public string DescriptionExtra;
        
        [XmlArrayItem("Size")]
        [XmlArray("Sizes")]
        public List<LocalizationSize> Sizes = new List<LocalizationSize>();
    }
}