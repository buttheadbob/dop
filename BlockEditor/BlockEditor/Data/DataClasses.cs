﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using BlockEditor.PlanetData;
using VRage.Game;

namespace BlockEditor.SE
{
    public class FormulaArgument
    {
        public string Arg1;
        public string Arg2;
        public string Arg3;

        public double Mass;
        public double Tier;
        public double SizeMlt;
        
        public Func<string, MyObjectBuilder_DefinitionBase> GetBlock;
        public Func<string, double> GetMass;

        public override string ToString()
        {
            return $"FA [{Arg1} {Arg2} {Arg3} | {Mass} {Tier} {SizeMlt}]";
        }
    }
    
    public class BlocksSettings
    {
        [XmlArrayItem("Formula")]
        [XmlElement("Formulas")]
        public RawFormula[] Formulas;
        
        [XmlArrayItem("Setting")]
        public BlockSettings[] Settings;
    }

    public class RawFormula
    {
        [XmlAttribute("Name")]
        public String Name;
        
        [XmlText]
        public String Value;
    }
    
    public class BlockSettings
    {
        [XmlAttribute("Type")]
        public String Type;
        [XmlAttribute("SpecialName")]
        public String SpecialName;
        
        [XmlArrayItem("Item")]
        public CubeBlockComponent[] Craft;
        [XmlElement("CriticalElement")]
        public MyObjectBuilder_CubeBlockDefinition.CriticalPart CriticalElement;
        [XmlArrayItem("Formula")]
        public Formula[] Formulas;
        [XmlElement]
        public double LargeBlockMlt;
    }
    
    public class CubeBlockComponent
    {
        [XmlAttribute("Subtype")]
        public string Subtype;
        
        [XmlAttribute("DeconstructId")]
        public string DeconstructId;

        [XmlAttribute("Count")]
        public float Amount;
    }

    public class Formula
    {
        [XmlAttribute]
        public String Name;
        
        [XmlAttribute]
        public String FxName;
        
        [XmlAttribute]
        public String Type = null;
        
        [XmlAttribute]
        public String Arg1;
        
        [XmlAttribute]
        public String Arg2;
        
        [XmlAttribute]
        public String Arg3;
        
        [XmlAttribute]
        public String Arg4;
        
        [XmlAttribute]
        public String Arg5;
        
        [XmlAttribute]
        public String Arg6;
        
        [XmlAttribute]
        public String Arg7;

        [XmlIgnore]
        public Args[] Arguments
        {
            get
            {
                if (m_Arguments == null)
                {
                    var arg1 = new Args(Arg1);
                    var arg2 = new Args(Arg2);
                    var arg3 = new Args(Arg3);
                    var arg4 = new Args(Arg4);
                    var arg5 = new Args(Arg5);
                    var arg6 = new Args(Arg6);
                    var arg7 = new Args(Arg7);
                    m_Arguments = new[]
                    {
                        arg1, arg2, arg3, arg4, arg5, arg6, arg7
                    };
                }

                return m_Arguments;
            }
            set
            {
                m_Arguments = value;
            }
        }
        
        [XmlIgnore] 
        public Args[] m_Arguments;
    }
}