﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using VRage.Game;

namespace BlockEditor.SE
{
    public class BlueprintCrafts
    {
        [XmlArrayItem("Item")]
        [XmlArray("DefaultIn")] 
        public List<BlueprintCraftItem> DefaultIn = new List<BlueprintCraftItem>();
        [XmlArrayItem("Item")]
        [XmlArray("DefaultOut")] 
        public List<BlueprintCraftItem> DefaultOut = new List<BlueprintCraftItem>();
        
        [XmlArrayItem("Craft")]
        [XmlArray("Crafts")] 
        public List<BlueprintCraft> Crafts = new List<BlueprintCraft>();
    }
    
    public class BlueprintCraft
    {
        [XmlArrayItem("Item")]
        [XmlArray("In")]
        public List<BlueprintCraftItem> In = new List<BlueprintCraftItem>();
        
        [XmlArrayItem("Item")]
        [XmlArray("Out")]
        public List<BlueprintCraftItem> Out = new List<BlueprintCraftItem>();
        
        
        
        [XmlAttribute("Name")]
        public string Name;
        
        [XmlAttribute("Time")]
        public float Time;
        
        [XmlAttribute("Tiers")]
        public int Tiers;
    }

    
    public class BlueprintCraftItem
    {
        [DefaultValue(0)]
        [XmlAttribute("IdTierStart")]
        public float IdTierStart = 0;
        
        [DefaultValue(1)]
        [XmlAttribute("IdTierGrow")]
        public float IdTierGrow = 1;
        
        [DefaultValue(0)]
        [XmlAttribute("MinTier")]
        public int MinTier = 0;
        
        [DefaultValue(999)]
        [XmlAttribute("MaxTier")]
        public int MaxTier = 999;
        
        [DefaultValue(1)]
        [XmlAttribute("Amount")]
        public float Amount = 1;
        
        [DefaultValue(1)]
        [XmlAttribute("AmountMlt")]
        public float AmountTierMlt = 1;
        
        [XmlAttribute("Enabled")]
        public bool Enabled = true;
        
        [XmlAttribute("Id")]
        public string Id;

        public override string ToString()
        {
            return $"{Id} G={IdTierGrow} S={IdTierStart}";
        }
    }
}