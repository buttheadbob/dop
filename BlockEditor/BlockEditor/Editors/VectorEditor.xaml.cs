﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Media;
using BlockEditor.Serialization;
using Microsoft.Xml.Serialization.GeneratedAssembly;
using VRage;
using VRage.Game;
using VRageMath;

namespace BlockEditor.Editors
{
    public partial class VectorEditor : UserControl
    {
        private List<object> toEdit;
        private List<object> inner;
        List<MemberInfo> vectors;
        private MemberInfo mi;
        public event Action<UserControl> OnChanged;
        private bool ReactOnTextChange = false;
        
        public VectorEditor()
        {
            InitializeComponent();
        }
        
        public void SetEditorObjects(MemberInfo mi, List<object> toEdit)
        {
            this.toEdit = toEdit;
            this.mi = mi;
            
            inner = new List<object>();
            
            foreach (var o in toEdit)
            {
                var oo = mi.GetValue(o);
                if (oo == null)
                {
                    var t = mi.GetMemberType();
                    oo = Activator.CreateInstance(t);
                }
                inner.Add(oo);
            }

            ReactOnTextChange = true;
        }

        public void RefreshVector()
        {
            var er = "ERROR";
            var vX = er;
            var vY = er;
            var vZ = er;
            try
            {
                var t = mi.GetMemberType();
                if (t.GenericTypeArguments != null && t.GenericTypeArguments.Length > 0)
                {
                    t = t.GenericTypeArguments[0];
                }
                var cc = ClassCache.Get(t);

                vectors = new List<MemberInfo>();
                vectors.AddRange(cc.Fields.Values);
                
                for (var i = 0; i < vectors.Count; i++)
                {
                    switch (i)
                    {
                        case 0:vX = RefreshText(vectors[i], inner); break;
                        case 1:vY = RefreshText(vectors[i], inner); break;
                        case 2:vZ = RefreshText(vectors[i], inner); break;
                    }
                }
            }
            catch (Exception e)
            {

            }

            Dispatcher.BeginInvoke(new Action(()=>SetVector(vX, vY,vZ)));
        }
        public string RefreshText(MemberInfo _mi, List<object> _toEdit)
        {
            string txt = "";
            try
            {
                var serializer = X.GetSerializer(_mi.GetMemberType());
                if (_toEdit[0] != null)
                {
                    
                }

                var v = GetValue(_toEdit[0], _mi);
                txt = v == null ? "NULL" : serializer.Invoke(v);
                for (var i=1; i<_toEdit.Count; i++)
                {
                    v = GetValue(_toEdit[i], _mi);
                    var txt2 = v == null ? "NULL" : serializer.Invoke(v);
                    if (txt != txt2)
                    {
                        txt = "";
                    }
                }
            }
            catch (Exception e)
            {
                Output.Error(e);
                txt = "ERROR";
            }
            return txt;
        }

        object GetValue(object o, MemberInfo memberInfo)
        {
            if (o == null)
            {
                return null;
            }
            return memberInfo.GetValue(o);
        }

        void SetVector(string vX, string vY, string vZ)
        {
            ReactOnTextChange = false;
            nvX.Text = vX;
            nvY.Text = vY;
            nvZ.Text = vZ;
            ReactOnTextChange = true;
        }
        
        public void Setter(string[] texts) // todo FIX
        {
            try
            {
                object[] o = new object[3];
                for (var i = 0; i < texts.Length; i++)
                {
                    var text = texts[i];
                    if (text == "NULL")
                    {
                        o[i] = null;
                    }
                    else
                    {
                        o[i] = X.GetDeserializer(vectors[i].GetMemberType()).Invoke(text);
                    }
                }
                
                var vec = new SerializableVector3((float)o[0],(float)o[1],(float)o[2]); //TODO
                foreach (var e in toEdit)
                { 
                    mi.SetValue(e, vec);
                }
                View.Background = Brushes.White;
            }
            catch (Exception e)
            {
                View.Background = Brushes.Maroon;
            }
            
            
            
            OnChanged?.Invoke(this);
        }

        
        private void Value_OnVectorChanged(object sender, TextChangedEventArgs e)
        {
            if (!ReactOnTextChange) return;
            if (mi == null) return;
            if (toEdit == null || toEdit.Count == 0) return;
            
            Setter(new string[]{nvX.Text,nvY.Text,nvZ.Text});
        }

        public void Clear()
        {
            OnChanged = null;
            toEdit = null;
            inner = null;
            mi = null;
            GUI.RemoveFromParent(this);
        }
    }
}