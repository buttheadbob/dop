﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using BlockEditor.Serialization;
using Epic.OnlineServices.Presence;
using VRage;
using VRageMath;
using SelectionChangedEventArgs = System.Windows.Controls.SelectionChangedEventArgs;

namespace BlockEditor.Editors
{
    public partial class ArrayEditor : UserControl
    {
        private List<object> toEdit = new List<object>();
        private List<object> parents;
        private MemberInfo mi;
        public event Action<UserControl> OnChanged;
        private Type innerType;
        public ArrayEditor()
        {
            InitializeComponent();
        }

        public void Clear()
        {
            toEdit.Clear();
            parents = null;
            mi = null;
            innerType = null;
            ItemsView.ItemsSource = null;
            
            GUI.RemoveFromParent(this);
            
            var ct = ScrollViewer.Content;

            if (ct is TextEditor te)
            {
                GUI.TextEditors.Deallocate(te);
            }
            else if (ct is PropertyEditor pe)
            {
                GUI.PropertyEditors.Deallocate(pe);
            }
        }
        
        public void SetEditorObjects(List<object> EditValues, MemberInfo mi, List<object> parents)
        {
            this.parents = parents;
            this.mi = mi;
            Type type = mi.GetMemberType();
            
            if (type.IsArray)
            {
                innerType = type.GetElementType();
            }
            else if (type.ImplementsGenericInterface(typeof(IList<>)))
            {
                innerType = type.GetGenericArguments()[0];
            }
            else
            {
                throw new Exception("Is not array");
            }

            var d = EditValues[0];
            if (d != null)
            {
                if (type.IsArray)
                {
                    var arr = (Array) d;
                    innerType = type.GetElementType();
                    var length = arr.GetLength(0);
                    for (var x=0; x<length; x++)
                    {
                        try
                        {
                            var dd = arr.GetValue(x);
                            var ddd = new FastDeepCloner.FastDeepCloner(dd).Clone();
                            toEdit.Add(ddd);
                        }
                        catch (Exception e)
                        {
                            var dd = arr.GetValue(x);
                            var ddd = new FastDeepCloner.FastDeepCloner(dd).Clone();
                            Output.Error(e);
                        }
                    }
                }
                else if (type.ImplementsGenericInterface(typeof(IList<>)))
                {
                    innerType = type.GetGenericArguments()[0];
                    var arr = (IList) d;
                    foreach (var dd in arr)
                    {
                        try
                        {
                            var ddd = new FastDeepCloner.FastDeepCloner(dd).Clone();
                            toEdit.Add(ddd);
                        }
                        catch (Exception e)
                        {
                            var ddd = new FastDeepCloner.FastDeepCloner(dd).Clone();
                            Output.Error(e);
                        }
                    }
                }
                else
                {
                    Output.Error(null, "Is not array");
                }
            }

            GUI.InvokeGui(() =>
            {
                ItemsView.ItemsSource = toEdit;
                ItemsView.SelectedIndex = 0;
            });
        }

        private void TriggerChangedEvent()
        {
            Save();
            OnChanged?.Invoke(this);
        }

        private void Save()
        {
            try
            {
                foreach (var p in parents)
                {
                    Type type = mi.GetMemberType();
                    if (type.IsArray)
                    {
                        var arr = Array.CreateInstance(innerType, toEdit.Count);
                        for (var x=0; x<toEdit.Count; x++)
                        {
                            var value = new FastDeepCloner.FastDeepCloner(toEdit[x]).Clone();
                            arr.SetValue(value, x);
                        }
                    
                        mi.SetValue(p, arr);
                    }
                    else if (type.ImplementsGenericInterface(typeof(IList<>)))
                    {
                        var arr = (IList)X.CreateInstance(type);
                        for (var x = 0; x < toEdit.Count; x++)
                        {
                            var value = new FastDeepCloner.FastDeepCloner(toEdit[x]).Clone();
                            arr.Add(value);
                        }
                    
                        mi.SetValue(p, arr);
                    }
                }
            }
            catch (Exception e)
            {
                Output.Error(e);
                //Save();
            }
        }

        public void EditElement(int index)
        {
            if (toEdit.Count == 0)
            {
                return;   
            }
            
            if (X.GetDeserializer(innerType) != null && X.GetSerializer(innerType) != null)
            {
                var te = new TextEditor();
                te.SetEditorObjects(new List<object>() { toEdit[index] }, innerType);
                ScrollViewer.Content = te;
                te.OnChanged += (x) =>
                {
                    var vv = te.Values[0];
                    toEdit[index] = vv;
                    TriggerChangedEvent();
                };
            }
            else
            {
                var te = new PropertyEditor();
                te.Edit(innerType, new List<object>() {toEdit[index]}, null);
                ScrollViewer.Content = te;
                te.OnChanged += (x) =>
                {
                    var vv = te.toEditObjects[0];
                    toEdit[index] = vv;
                    TriggerChangedEvent();
                };
            }
        }

        private void AddClick(object sender, RoutedEventArgs e)
        {
            var inner = X.CreateInstance(innerType) ;
            toEdit.Add(inner);
            ItemsView.ItemsSource = null;
            ItemsView.ItemsSource = toEdit;
            
            TriggerChangedEvent();
        }
        
        private void RemoveClick(object sender, RoutedEventArgs e)
        {
            if (ItemsView.SelectedIndex != -1)
            {
                toEdit.RemoveAt(ItemsView.SelectedIndex);
                ItemsView.ItemsSource = null;
                ItemsView.ItemsSource = toEdit;

                TriggerChangedEvent();
            }
        }
        
        private void UpClick(object sender, RoutedEventArgs e)
        {
            if (ItemsView.SelectedIndex != 0)
            {
                var o = toEdit[ItemsView.SelectedIndex];
                toEdit.RemoveAt(ItemsView.SelectedIndex);
                toEdit.Insert(ItemsView.SelectedIndex, o);
                ItemsView.ItemsSource = null;
                ItemsView.ItemsSource = toEdit;
                
                TriggerChangedEvent();
            }
        }
        
        private void DownClick(object sender, RoutedEventArgs e)
        {
            var o = toEdit[ItemsView.SelectedIndex];
            toEdit.RemoveAt(ItemsView.SelectedIndex);
            toEdit.Insert(ItemsView.SelectedIndex, o);
            ItemsView.ItemsSource = null;
            ItemsView.ItemsSource = toEdit;
            
            TriggerChangedEvent();
        }

        private void ItemsView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            EditElement(ItemsView.SelectedIndex == -1 ? 0 : ItemsView.SelectedIndex);
        }

        
    }
}