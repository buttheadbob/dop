﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using BlockEditor.Serialization;

namespace BlockEditor.Editors
{
    public partial class RadioEditor : UserControl
    {
        private List<object> toEdit;
        private MemberInfo mi;
        public event Action<UserControl> OnChanged;
        
        private bool ReactOnRadioChange = true;
        
        public RadioEditor()
        {
            InitializeComponent();
        }
        
        public void SetEditorObjects(MemberInfo mi, List<object> toEdit)
        {
            this.toEdit = toEdit;
            this.mi = mi;
        }
        
        
        public void Clear()
        {
            toEdit = null;
            mi = null;
            OnChanged = null;
            GUI.RemoveFromParent(this);
        }
        
        public void RefreshRadio()
        {
            string txt = "";
            try
            {
                var serializer = X.GetSerializer(mi.GetMemberType());
                var v = mi.GetValue(toEdit[0]);
                txt = v == null ? "NULL" : serializer.Invoke(v);
                for (var i=1; i<toEdit.Count; i++)
                {
                    v = mi.GetValue(toEdit[i]);
                    var txt2 = v == null ? "NULL" : serializer.Invoke(v);
                    if (txt != txt2)
                    {
                        txt = "";
                    }
                }
            }
            catch (Exception e)
            {
                txt = "ERROR";
            }
            Dispatcher.BeginInvoke(new Action(()=>SetRadio(txt)));
        }

        void SetRadio(string text)
        {
            ReactOnRadioChange = false;
            switch (text)
            {
                case "true": {RadioButton1.IsChecked = true; break;}
                case "false": {RadioButton2.IsChecked = true; break;}
                case "NULL": {RadioButton3.IsChecked = true; break;}
            }
            
            ReactOnRadioChange = true;
        }
        
        
        private void Value_OnRadioChanged(object sender, RoutedEventArgs e)
        {
            if (!ReactOnRadioChange) return;
            if (mi == null) return;
            if (toEdit == null || toEdit.Count == 0) return;
            Setter((sender as RadioButton)?.Content.ToString());
        }
        public void Setter(String text)
        {
            try
            {
                object o;
                if (text == "NULL")
                {
                    o = null;
                }
                else
                {
                
                    o =  X.GetDeserializer(mi.GetMemberType()).Invoke(text);
                }
         
            
                foreach (var e in toEdit)
                {
                    mi.SetValue(e, o);
                }
                View.Background = Brushes.White;
            }
            catch (Exception e)
            {
                View.Background = Brushes.Maroon;
            }
            
            OnChanged?.Invoke(this);
        }

    }
}