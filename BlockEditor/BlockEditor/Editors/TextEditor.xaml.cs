﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Media;
using BlockEditor.Serialization;
using VRage.Library.Collections;
using VRageMath;

namespace BlockEditor.Editors
{
    public partial class TextEditor : UserControl
    {
        private List<object> ParentObjects;
        
        private MemberInfo Mi;
        public event Action<UserControl> OnChanged;
        
        public List<object> Values;
        
        public TextEditor()
        {
            InitializeComponent();
        }
        
        public void Clear()
        {
            Values = null;
            Serializer = null;
            Deserializer = null;
            Mi = null;
            OnChanged = null;
            ParentObjects = null;
            GUI.RemoveFromParent(this);
        }

        private int Count = 0;
        private Func<object, string> Serializer;
        private Func<string, object> Deserializer;
        
        public void SetEditorObjects(List<object> values, Type type, MemberInfo mi = null, List<object> parentObjects = null)
        {
            Serializer = X.GetSerializer(type);
            Deserializer = X.GetDeserializer(type);
            
            Values = values;
            Count = Values.Count;
            
            ParentObjects = parentObjects;
            Mi = mi;

            RefreshText();
        }

        
        
        public void RefreshText()
        {
            string txt = "";
            try
            {
                var v = Values[0];//.GetValue(parentObjects[0]);
                txt = v == null ? "NULL" : Serializer.Invoke(v);
                for (var i=1; i<Values.Count; i++)
                {
                    v = Values[i];
                    var txt2 = v == null ? "NULL" : Serializer.Invoke(v);
                    if (txt != txt2)
                    {
                        txt = "";
                    }
                }
            }
            catch (Exception e)
            {
                txt = "ERROR";
            } 
            

            Dispatcher.BeginInvoke(new Action(()=>SetText(txt)));
        }

        void SetText(string text)
        {
            ReactOnTextChange = false;
            Value.Text = text;
            ReactOnTextChange = true;
        }
        
        public void Setter(String text)
        {
            try
            {
                Values.Clear();
                for (int i = 0; i < Count; i++)
                {
                    object o;
                    if (text == "NULL")
                    {
                        o = null;
                    }
                    else
                    {
                        o =  Deserializer.Invoke(text);
                    }
                    Values.Add(o);
                }

                if (Mi != null)
                {
                    for (var x=0; x<Values.Count; x ++)
                    {
                        Mi.SetValue(ParentObjects[x], Values[x]);
                    }
                }
                
                View.Background = Brushes.White;
            }
            catch (Exception e)
            {
                View.Background = Brushes.Maroon;
            }
            
            OnChanged?.Invoke(this);
        }

        private bool ReactOnTextChange = false;
        private void Value_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!ReactOnTextChange) return;
            Setter(Value.Text);
        }

        
    }
}