﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Media;
using BlockEditor.Serialization;


namespace BlockEditor.Editors
{
    public partial class EnumEditor : UserControl
    {
        private List<object> toEdit;
        private MemberInfo mi;
        public event Action<UserControl> OnChanged;
        private bool ReactOnRadioChange;
        
        public void Clear()
        {
            OnChanged = null;
            toEdit = null;
            mi = null;
            ValueList.Items.Clear();
            GUI.RemoveFromParent(this);
        }
        
        public void SetEditorObjects(MemberInfo mi, List<object> toEdit)
        {
            if (mi.Name == "CubeSize")
            {
                var wtf = 0;
            }
            this.toEdit = toEdit;
            this.mi = mi;
            var t = mi.GetMemberType();
            var eList = Enum.GetNames(t);

            var enumValue = mi.GetValue(toEdit[0]);
            var elA = Enum.GetValues(t);
            int selectedIndex = 0;
            foreach (var o in elA)
            {
                if (enumValue.Equals(o))
                {
                    break;
                }

                selectedIndex++;
            }
            
            
            GUI.InvokeGui(() =>
            {
                foreach (var e in eList)
                {
                    ValueList.Items.Add(e);
                }

                ValueList.SelectedIndex = selectedIndex;
            });

        }
        
        public EnumEditor()
        {
            InitializeComponent();
        }
        public void RefreshEnum()
        {
            string txt = "";
            try
            {
                var serializer = X.GetSerializer(mi.GetMemberType());
                var v = mi.GetValue(toEdit[0]);
                txt = v == null ? "NULL" : serializer.Invoke(v);
                for (var i=1; i<toEdit.Count; i++)
                {
                    v = mi.GetValue(toEdit[i]);
                    var txt2 = v == null ? "NULL" : serializer.Invoke(v);
                    if (txt != txt2)
                    {
                        txt = "";
                    }
                }
            }
            catch (Exception e)
            {
                txt = "ERROR";
            }
            Dispatcher.BeginInvoke(new Action(()=>SetEnum(txt)));
        }
        void SetEnum(string text)
        {
            ReactOnRadioChange = false;
            ValueList.SelectedIndex = ValueList.Items.IndexOf(text);
            ReactOnRadioChange = true;
        }
        
        
        
        private void Value_OnEnumChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!ReactOnRadioChange) return;
            if (mi == null) return;
            if (toEdit == null || toEdit.Count == 0) return;
            Setter((sender as ComboBox)?.SelectedItem.ToString());
        }
        public void Setter(String text)
        {
            try
            {
                object o;
                if (text == "NULL")
                {
                    o = null;
                }
                else
                {
                
                    o =  X.GetDeserializer(mi.GetMemberType()).Invoke(text);
                }
         
            
                foreach (var e in toEdit)
                {
                    mi.SetValue(e, o);
                }
                View.Background = Brushes.White;
            }
            catch (Exception e)
            {
                View.Background = Brushes.Maroon;
            }
            
            OnChanged?.Invoke(this);
        }

       
    }
}