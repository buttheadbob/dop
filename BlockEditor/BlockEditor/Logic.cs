//CUSTOM

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Documents;
using System.Xml.Serialization;
using BlockEditor;
using BlockEditor.GuiData;
using BlockEditor.SE;
using BlockEditor.Serialization;
using BlockEditor.Wpf;
using NLog.Fluent;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using VRage;
using VRage.Game;
using VRage.Game.ObjectBuilders.ComponentSystem;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.ObjectBuilders;
using VRageMath;
using VRageRender;
using ContextMenu = System.Windows.Controls.ContextMenu;
using Localization = BlockEditor.SE.Localization;
using MenuItem = System.Windows.Controls.MenuItem;
using MessageBox = System.Windows.MessageBox;
using GasTank = Sandbox.Common.ObjectBuilders.Definitions.MyObjectBuilder_GasTankDefinition;
using Size = System.Drawing.Size;

public class Script
{
    Dictionary<string, int> OldTierPattern = new Dictionary<string, int>();
    public static Dictionary<string, MyObjectBuilder_ComponentDefinition> Components = new Dictionary<string, MyObjectBuilder_ComponentDefinition>();
    public static Dictionary<string, MyObjectBuilder_PhysicalItemDefinition> Ingots = new Dictionary<string, MyObjectBuilder_PhysicalItemDefinition>();
    public static Dictionary<string, MyObjectBuilder_PhysicalItemDefinition> Ores = new Dictionary<string, MyObjectBuilder_PhysicalItemDefinition>();
    
    void Apply()
    {
        MenuGenerator();

        for (var x=1; x<=12; x++)
        {
            OldTierPattern.Add($"T{x}", x);
            OldTierPattern.Add($"T0{x}", x);
            OldTierPattern.Add($"{x}", x);
            if (x <= 9) OldTierPattern.Add($"0{x}", x);
        }
    }

    public int ExtractOldTier(string s)
    {
        foreach (var v in OldTierPattern)
        {
            if (s.EndsWith(v.Key))
            {
                return v.Value;
            }
        }

        return -1;
    }

    void MenuGenerator()
    {
        var i = GlobalEditor.MenuInterceptor[0];
        GlobalEditor.MenuInterceptor.RemoveAll((x) => true);
        GlobalEditor.MenuInterceptor.Add(i);
        GlobalEditor.MenuInterceptor.Add(MenuInterceptor);
    }

    private void MenuInterceptor(List<object> objects, ContextMenu menu)
    {
        if (objects.Count == 0) return;

        GlobalEditor.AddMenu(menu, objects, "Clear", Clear);
        
        var utils = new MenuItem();
        utils.Header = "Utils";
        menu.Items.Add(utils);
        
        var translations = new MenuItem();
        translations.Header = "Translations";
        menu.Items.Add(translations);
        
        var checks = new MenuItem();
        checks.Header = "Checks";
        menu.Items.Add(checks);

        var generators = new MenuItem();
        generators.Header = "Generators";
        menu.Items.Add(generators);
        
        var other = new MenuItem();
        other.Header = "Other";
        menu.Items.Add(other);
        
        var balance = new MenuItem();
        balance.Header = "Balance";
        menu.Items.Add(balance);
        
        

        GlobalEditor.AddMenu(translations, objects, "1) Generate BlockPairName for blocks (Default)", GenerateBlockPairNames);
        GlobalEditor.AddMenu(translations, objects, "1*) Generate BlockPairNames for blocks with different sizes", GenerateBlockPairNameWithDifferentSizes);
        GlobalEditor.AddMenu(translations, objects, "2) Fix DisplayName+Description", GenerateDisplayNameAndDescriptions);
        GlobalEditor.AddMenu(translations, objects, "3) Generate Translations", GenerateTranslations);

        GlobalEditor.AddMenu(utils, objects, "Rename special", RenameSpecial);
        GlobalEditor.AddMenu(utils, objects, "Split by type", SplitByType);
        
        GlobalEditor.AddMenu(checks, objects, "Find Duplicates", CheckIds);
        GlobalEditor.AddMenu(checks, objects, "Check BlockPairNames", CheckBlockPairName);
        
        GlobalEditor.AddMenu(balance, objects, "Generate Cargo Volume", GenerateCargoContainerVolume);
        GlobalEditor.AddMenu(balance, objects, "Generate Crafts from scheme", GenerateCraftsFromScheme);
        GlobalEditor.AddMenu(balance, objects, "Apply crafts & formulas", ApplyCraftsAndFormulas);
        
        
        GlobalEditor.AddMenu(generators, objects, "Generate copies", GenerateMany);
        GlobalEditor.AddMenu(generators, objects, "Generate Block variants: grouped by block size", GenerateBlockVariants);
        GlobalEditor.AddMenu(generators, objects, "Generate Block variants: in one group", GenerateBlockVariants2);
        GlobalEditor.AddMenu(generators, objects, "Generate Block Categories", GenerateBlockCategories); 
        
        
        GlobalEditor.AddMenu(other, objects, "Generate Ids", GenerateIds);
        GlobalEditor.AddMenu(other, objects, "Icon generator", IconGenerator);
        GlobalEditor.AddMenu(other, objects, "Generate & Fix Tier Icons", GenerateIcons);
        GlobalEditor.AddMenu(other, objects, "Generate BlockPositions", GenerateBlockPositions);
        GlobalEditor.AddMenu(other, objects, "Export BlockPositions", ExportBlockPositions);
    }

    public void CheckBlockPairName(List<object> objects)
    {
        Dictionary<string, List<MyObjectBuilder_CubeBlockDefinition>> blocks = new Dictionary<string, List<MyObjectBuilder_CubeBlockDefinition>>();

        foreach (var m in GlobalEditor.Mods)
        {
            if (!m.ModRoot.ToLower().Contains("nebula")) continue;
            foreach (var d in m.Definitions)
            {
                foreach (var o in d.AllItems)
                {
                    if (o is MyObjectBuilder_CubeBlockDefinition block)
                    {
                        blocks.GetOrNew(block.BlockPairName).Add(block);
                    }
                }
            }
        }

        var sb = new StringBuilder();
        foreach (var kv in blocks)
        {
            if (kv.Value.Count > 2)
            {
                sb.Append(kv.Key + " found same blockPairName: " + kv.Value.Count);
            }
        }
        
        Output.Error(sb.ToString());
    }

    public void GenerateBlockPairNameWithDifferentSizes(List<object> objects)
    {
        var defs = Sugar.Flatten(objects);

        SerializableVector3I zero = new SerializableVector3I();
        SerializableVector3I vector3S = new SerializableVector3I();
        SerializableVector3I vector3L = new SerializableVector3I();
        foreach (var def in defs)
        {
            if (def is MyObjectBuilder_CubeBlockDefinition block)
            {
                if (block.CubeSize == MyCubeSize.Large)
                {
                    if (vector3L != zero && vector3L != block.Size)
                    {
                        MessageBox.Show("Multiple largeGrid sizes are not allowed");
                        return;
                    }
                    vector3L = block.Size;
                }
                else
                {
                    if (vector3S != zero && vector3S != block.Size)
                    {
                        MessageBox.Show("Multiple smallGrid sizes are not allowed");
                        return;
                    }
                    vector3S = block.Size;
                }
            }
        }
        
        foreach (var def in defs)
        {
            if (def is MyObjectBuilder_CubeBlockDefinition block)
            {
                GenerateBlockPairName(block, vector3L, vector3S);
            }
        }
    }

    public void GenerateBlockPairName(MyObjectBuilder_CubeBlockDefinition block, SerializableVector3I vector3L, SerializableVector3I vector3S)
    {
        var tier = block.GetTier();
        var tierS = Helper.GenerateTierString(tier);

        var special = block.GetSpecial(withSubSpecial:true);
        var type = block.Id.TypeId.ToString().Replace("MyObjectBuilder_", "");
        special = special != null ? "_" + special : "";

        //special = special.Replace("DLC-DLC", "DLC");
                
        var size = $"{vector3L.x}x{vector3L.y}x{vector3L.z}";
        if (vector3S.X != vector3L.X || vector3S.Y != vector3L.Y || vector3S.Z != vector3L.Z)
        {
            size += $"-{vector3S.x}x{vector3S.y}x{vector3S.z}";
        }     
        block.BlockPairName = $"{type}{special}_{size}_{tierS}";
        
        
    }

    

    private static bool log = true;
    
    public void GenerateCraftsFromScheme(List<object> objects)
    {
        var dialog = GlobalEditor.OpenFileDialog();
        var path = dialog[0];
        if (path == null) return;
        log = true;
        RefreshMass();

        var scheme = new XReader().Read<BlueprintCrafts>(path);
        var list = new List<MyObjectBuilder_BlueprintDefinition>();

        var list2 = new List<BlueprintClassEntry>();
        var list3 = new Dictionary<string, MyObjectBuilder_BlueprintClassDefinition>();

        if (!(objects[0] is DefinitionFile))
        {
            MessageBox.Show("Click on file");
            return;
        }

        foreach (var c in scheme.Crafts)
        {
            var sb = new StringBuilder();
            for (int t = 1; t <= c.Tiers; t++)
            {
                
                var inn = new List<BlueprintItem>();
                var outt = new List<BlueprintItem>();
                
                c.In = c.In ?? new List<BlueprintCraftItem>();
                c.Out = c.Out ?? new List<BlueprintCraftItem>();

                foreach (var _ in scheme.DefaultIn)
                {
                    var pick = c.In.FirstOrDefault((x) => x.Id == _.Id) ?? _;
                    var bi = Generate(pick);
                    if (bi == null) continue;
                    inn.Add(bi);
                }
                
                
                foreach (var _ in c.In)
                {
                    var pick = scheme.DefaultIn.FirstOrDefault((x) => x.Id == _.Id);
                    if (pick != null) continue; //Already added
                    
                    var bi = Generate(_);
                    if (bi == null) continue;
                    inn.Add(bi);
                }
                
                foreach (var _ in scheme.DefaultOut)
                {
                    var pick = c.Out.FirstOrDefault((x) => x.Id == _.Id) ?? _;
                    var bi = Generate(pick);
                    if (bi == null) continue;
                    outt.Add(bi);
                }
                
                foreach (var _ in c.Out)
                {
                    var pick = scheme.DefaultOut.FirstOrDefault((x) => x.Id == _.Id);
                    if (pick != null) continue; //Already added
                    
                    var bi = Generate(_);
                    if (bi == null) continue;
                    outt.Add(bi);
                }

                var bd = new MyObjectBuilder_BlueprintDefinition();
                bd.Id = $"BlueprintDefinition/{c.Name}_{Helper.GenerateTierString(t)}".ToId();
                bd.Prerequisites = inn.ToArray();
                bd.DisplayName = $"DisplayName_{c.Name}_{Helper.GenerateTierString(t)}";
                bd.Description = $"";
                bd.Results = outt.ToArray();
                bd.IsPrimary = true;
                bd.BaseProductionTimeInSeconds = c.Time;
                
                
                bd.Icons = new[] { $"Data/Icons/{bd.Results[0].Id.TypeId.ToString().Replace("MyObjectBuilder_", "")}/{bd.Results[0].Id.SubtypeId}.png" };
                
                bd.Priority = 999;
                
                list.Add(bd);

                
                
                BlueprintClassEntry bce = new BlueprintClassEntry();
                bce.BlueprintSubtypeId = bd.Id.SubtypeName;
                bce.Class = $"Assemble_{Helper.GenerateTierString(t)}";
                
                list2.Add(bce);

                if (!list3.ContainsKey(bce.Class))
                {
                    
                    var cc = new MyObjectBuilder_BlueprintClassDefinition();
                    cc.Id = $"BlueprintClassDefinition/{bce.Class}".ToId();
                    cc.Icons = new[] { $"Data/Icons/BlueprintClass/{cc.Id.SubtypeName}.dds" };
                    cc.HighlightIcon = $"Data/Icons/BlueprintClass/{cc.Id.SubtypeName}_highlight.dds";
                    cc.FocusIcon = $"Data/Icons/BlueprintClass/{cc.Id.SubtypeName}_focus.dds";
                    cc.InputConstraintIcon = $"Data/Icons/BlueprintClass/{cc.Id.SubtypeName}_input.dds";
                    cc.OutputConstraintIcon = $"Data/Icons/BlueprintClass/{cc.Id.SubtypeName}_output.dds";
                    cc.ProgressBarSoundCue = null;
                    cc.DisplayName = $"DisplayName_BlueprintClassDefinition_{cc.Id.SubtypeName}";
                    cc.Description = $" ";
                    list3.Add(cc.Id.SubtypeName, cc);
                }
                

                BlueprintItem Generate(BlueprintCraftItem pick)
                {
                    

                    if (!pick.Enabled)
                    {
                        //Output.Info("Not enabled: " + pick.Id);
                        return null;
                    }
                    if (t < pick.MinTier || t > pick.MaxTier) {
                        //Output.Info("Not matching tier: " + pick.Id + " " + t + " " + pick.MinTier + " " + pick.MaxTier);
                        return null;
                    }
                    
                    sb.AppendLine($"? =1 + {pick.IdTierStart} + {pick.IdTierGrow} * {(t - pick.MinTier)} : {pick.ToString()}");

                    int idTier = (int) (1 + pick.IdTierStart + pick.IdTierGrow * (t - pick.MinTier));

                    
                    
                    pick.Id.GetTypeSubtype(out var type, out var subtype);
                    var am = pick.Amount * Math.Pow(pick.AmountTierMlt, t - pick.MinTier); 
                    var ob = Helper.GetTieredSubtype(type, subtype, subtype +"_"+ Helper.GenerateTierString(idTier));
                    
                    
                   
                    BlueprintItem bi = new BlueprintItem();
                    bi.Id = new SerializableDefinitionId(MyObjectBuilderType.Parse($"MyObjectBuilder_{type}"), ob.Id.SubtypeName);
                    bi.Amount = "" + am.ToString(CultureInfo.InvariantCulture);
                    return bi;
                }
            }
            
            if (log)
            {
                Output.Info(sb.ToString());
                sb.Clear();
                log = false;
            }
           
        }


        
        //Mod mod = GetOrCreateOutMod();

        var df = (objects[0] as DefinitionFile);
        df.AllItems.Clear();
        //var df = new DefinitionFile(mod);
        //df.AlwaysVisible = true;
        //df.File = "Out";
        df.AllItems.AddRange(list);
        df.AllItems.AddRange(list2);
        df.AllItems.AddRange(list3.Values);
        //mod.Definitions.Add(df);
        
        GlobalEditor.RefreshTree();
    }

    private static int ExtractInt(string s)
    {
        var s2 = new String(s.Where(Char.IsDigit).ToArray());
        return int.Parse(s2);
    }

    public void GenerateTranslations(List<object> objects)
    {
        var dialog = GlobalEditor.OpenFileDialog();
        var path = dialog[0];
        if (path == null)
        {
            MessageBox.Show("GGX:");
            return;
        }
        
        var settings = new XReader().Read<Localization>(path);
        

        var errors = new StringBuilder();
        Regex regex = new Regex(" +");


        HashSet<string> Descriptions = new HashSet<string>();
        HashSet<string> Translations = new HashSet<string>();
        
        
        Dictionary<string, Dictionary<string, string>> trans = new Dictionary<string, Dictionary<string, string>>();
        foreach (var s in settings.Items)
        {
            foreach (var t in s.Translate)
            {
                var lang = t.Key;
                var id = s.Key;
                if (t.Sizes != null)
                {
                    for (int i = 1; i <= 9; i++)
                    {
                        foreach (var size in t.Sizes)
                        {
                            var key = $"DisplayName_{id}_{size.Size}_{Helper.GenerateTierString(i)}";
                            var name2 = String.Format(t.Name, size.Translation);
                            var value = $"{name2} {Helper.GenerateTierString(i).Replace("T0", "T")}";
                            trans.GetOrNew(lang)[key] = value;
                            Translations.Add(key);
                        }
                    }
                }
                else
                {
                    for (int i = 1; i <= 9; i++)
                    {
                        var key = $"DisplayName_{id}_{Helper.GenerateTierString(i)}";
                        var value = $"{t.Name} {Helper.GenerateTierString(i).Replace("0", "")}";
                        trans.GetOrNew(lang)[key] = value;
                        Translations.Add(key);
                    }
                }
                
                
                
                var sb = new StringBuilder();
                foreach (var g1 in s.Gives)
                {
                    var tr = GetGivesTranslationFormula(g1.Key, lang);
                    if (tr == null)
                    {
                        errors.AppendLine($"Gives `{lang}`/`{g1.Key}` not found");
                        continue;
                    }

                    sb.AppendLine(string.Format(tr, g1.Arg1, g1.Arg2, g1.Arg3));
                }


                if (sb.Length != 0)
                {
                    sb.Insert(0, "\nEach tier gives:\n");
                    sb.Remove(sb.Length - 1, 1);
                }

                
                var desc = regex.Replace(t.Description, " ");

                var kkey = $"Description_{id}";
                trans.GetOrNew(lang)[kkey] = desc+sb.ToString();
                Translations.Add(kkey);
            }

            String GetGivesTranslationFormula(string key, string lang)
            {
                foreach (var g2 in settings.Gives)
                {
                    if (key == g2.Key)
                    {
                        foreach (var t2 in g2.Translations)
                        {
                            if (t2.Key == lang)
                            {
                                return t2.Value;
                            }
                        }   
                    }
                }

                return null;
            }
        }

        var ssb = new StringBuilder();
        if (errors.Length != 0)
        {
            MessageBox.Show(errors.ToString(), "Found errors");
        }
        
        foreach (var m in GlobalEditor.Mods)
        {
            if (!m.ModRoot.ToLower().Contains("nebula")) continue;
            foreach (var d in m.Definitions)
            {
                foreach (var o in d.AllItems)
                {
                    if (o is MyObjectBuilder_CubeBlockDefinition block)
                    {
                        Descriptions.Add(block.DisplayName);
                        Descriptions.Add(block.Description);
                    }
                }
            }
        }



        var ss = new StringBuilder();
        foreach (var t in Descriptions)
        {
            
            if (!Translations.Contains(t))
            {
                ss.AppendLine(t);
            }
        }

        if (ss.Length > 0)
        {
            MessageBox.Show("Translations not found:"+ss.ToString(), "");
        }
        
            
        
        foreach (var x in trans)
        {
            foreach (var y in x.Value)
            {
                ssb.AppendLine($"<data name=\"{y.Key}\" xml:space=\"preserve\"><value>{y.Value}</value></data>");
            }
            Clipboard.SetText(ssb.ToString());
            MessageBox.Show($"Translations {x.Key} were copied to your clipboard");
            ssb.Clear();
        }
    }

    public void GenerateBlockVariants(List<object> objects)
    {
        var blocks = new Dictionary<string, List<MyObjectBuilder_CubeBlockDefinition>>();
        var defs = Sugar.Flatten(objects);
        bool NotZero = false; 
        foreach (var def in defs)
        {
            if (def is MyObjectBuilder_CubeBlockDefinition block)
            {
                NotZero = true; 
                var special = block.BlockPairName.SubstringLast("_");
                blocks.GetOrNew(special).Add(block);
            }
        }
        if (!NotZero) 
        { 
            Output.Info(DateTime.Now + ":  0 CubeBlock selected"); 
            return; 
        } 

        var mod = GetOrCreateOutMod();
        var df = new DefinitionFile(mod);
        df.File = "New BlockVariantGroups"; 
        df.Mod.RootFolder = "Out"; 
        df.AlwaysVisible = true;
        mod.Definitions.Add(df);
        
        //var groups = new List<MyObjectBuilder_BlockVariantGroup>();
        foreach (var o in blocks)
        {
            MyObjectBuilder_BlockVariantGroup variantGroup = new MyObjectBuilder_BlockVariantGroup();
            var n = o.Key;
            variantGroup.Id = new SerializableDefinitionId(MyObjectBuilderType.Parse("MyObjectBuilder_BlockVariantGroup"), n);
            variantGroup.Description = "Description_BVG_" + n;
            variantGroup.DisplayName = "Display_Name_BVG_" + n;
            variantGroup.Blocks = new SerializableDefinitionId[o.Value.Count];

            int i = 0;
            foreach (var block in o.Value)
            {
                variantGroup.Blocks[i++] = block.Id;
            }
            df.AllItems.Add(variantGroup);
        }
        
        Output.Info(DateTime.Now + ":  New BlockVariantGroup Created"); 
        GlobalEditor.RefreshTree();
    }
    
    public void GenerateBlockVariants2(List<object> objects)
    {
        var blocks = new Dictionary<string, List<MyObjectBuilder_CubeBlockDefinition>>();
        var defs = Sugar.Flatten(objects);
        foreach (var def in defs)
        {
            if (def is MyObjectBuilder_CubeBlockDefinition block)
            {
                var special = block.BlockPairName.SubstringLast("_");
                blocks.GetOrNew(special).Add(block);
            }
        }
        
        var total = 0;
        foreach (var o in blocks)
        {
            total += o.Value.Count;
        }

        var mod = GetOrCreateOutMod();
        var df = new DefinitionFile(mod);
        df.File = "New BlockVariantGroups"; 
        df.Mod.RootFolder = "Out"; 
        df.AlwaysVisible = true;
        mod.Definitions.Add(df);
        
        MyObjectBuilder_BlockVariantGroup variantGroup = new MyObjectBuilder_BlockVariantGroup();
        variantGroup.Id = new SerializableDefinitionId(MyObjectBuilderType.Parse("MyObjectBuilder_BlockVariantGroup"), "NEW");
        variantGroup.Description = "Description_BVG_NEW";
        variantGroup.DisplayName = "Display_Name_BVG_NEW";
        variantGroup.Blocks = new SerializableDefinitionId[total];
        
        int i = 0;
        foreach (var o in blocks)
        {
            foreach (var block in o.Value)
            {
                variantGroup.Blocks[i++] = block.Id;
            }
        }

        df.AllItems.Add(variantGroup);
        
        Output.Info(DateTime.Now + ":  New BlockVariantGroup Created"); 
        GlobalEditor.RefreshTree();
    }
    
    public void GenerateBlockCategories(List<object> objects) 
    { 
        var GuiBlockCategoryMain = new MyObjectBuilder_GuiBlockCategoryDefinition(); 
        var MainList = new List<string>(); 
        var GuiBlockCategoryLarge = new MyObjectBuilder_GuiBlockCategoryDefinition(); 
        var LargeList = new List<string>(); 
        var GuiBlockCategorySmall = new MyObjectBuilder_GuiBlockCategoryDefinition(); 
        var SmallList = new List<string>(); 
         
        var DLCLists = new Dictionary<string, List<string>>(); 
         
        var defs = Sugar.Flatten(objects); 
        bool NotZero = false; 
        foreach (var def in defs) 
        { 
            if (def is MyObjectBuilder_CubeBlockDefinition block) 
            { 
                NotZero = true; 
                var type = block.Id.TypeId; 
                var subtype = block.Id.SubtypeId; 
                var id = type + "/" + subtype; 
                 
                MainList.Add(id); 
                 
                if (block.CubeSize == MyCubeSize.Large && (!subtype.Contains("_T0") || subtype.Contains("_T01"))) 
                { 
                    LargeList.Add(id); 
                } 
                else if(!subtype.Contains("_T0") || subtype.Contains("_T01")) 
                { 
                    SmallList.Add(id); 
                } 
 
                if (block.DLCs == null || block.DLCs.Length == 0) continue; 
                foreach (var dlc in block.DLCs) 
                { 
                    var List = DLCLists.GetOrNew(dlc); 
                    List.Add(id); 
                } 
            } 
        } 
 
        if (!NotZero) 
        { 
            Output.Info(DateTime.Now + ":  0 CubeBlock selected"); 
            return; 
        } 
         
        var mod = GetOrCreateOutMod(); 
        var df = new DefinitionFile(mod); 
        df.File = "New GuiBlockCategoryDefinitions"; 
        df.AlwaysVisible = true; 
        df.Mod.RootFolder = "Out"; 
        mod.Definitions.Add(df); 
 
        GuiBlockCategoryMain.Id = new SerializableDefinitionId(MyObjectBuilderType.Parse("MyObjectBuilder_GuiBlockCategoryDefinition"), ""); 
        GuiBlockCategoryMain.DisplayName = "DisplayName_Category_"; 
        GuiBlockCategoryMain.Name = "Name"; 
        GuiBlockCategoryMain.ItemIds = MainList.ToArray(); 
        GuiBlockCategoryLarge.Id = new SerializableDefinitionId(MyObjectBuilderType.Parse("MyObjectBuilder_GuiBlockCategoryDefinition"), ""); 
        GuiBlockCategoryLarge.DisplayName = "DisplayName_Category_LargeBlocks"; 
        GuiBlockCategoryLarge.Name = "LargeBlocks"; 
        GuiBlockCategoryLarge.StrictSearch = true; 
        GuiBlockCategoryLarge.ItemIds = LargeList.ToArray(); 
        GuiBlockCategorySmall.Id = new SerializableDefinitionId(MyObjectBuilderType.Parse("MyObjectBuilder_GuiBlockCategoryDefinition"), ""); 
        GuiBlockCategorySmall.DisplayName = "DisplayName_Category_SmallBlocks"; 
        GuiBlockCategorySmall.Name = "SmallBlocks"; 
        GuiBlockCategorySmall.StrictSearch = true; 
        GuiBlockCategorySmall.ItemIds = SmallList.ToArray(); 
 
        foreach (var dlc in DLCLists.Keys) 
        { 
            var GuiBlockCategoryDlC = new MyObjectBuilder_GuiBlockCategoryDefinition(); 
            GuiBlockCategoryDlC.Id = new SerializableDefinitionId(MyObjectBuilderType.Parse("MyObjectBuilder_GuiBlockCategoryDefinition"), ""); 
            GuiBlockCategoryDlC.DisplayName = "DisplayName_DLC_" + dlc; 
            GuiBlockCategoryDlC.Name = dlc; 
            GuiBlockCategoryDlC.StrictSearch = true; 
            GuiBlockCategoryDlC.ItemIds = DLCLists[dlc].ToArray(); 
             
            df.AllItems.Add(GuiBlockCategoryDlC); 
        } 
         
        df.AllItems.Add(GuiBlockCategoryMain); 
        df.AllItems.Add(GuiBlockCategoryLarge); 
        df.AllItems.Add(GuiBlockCategorySmall); 
         
        Output.Info(DateTime.Now + ":  New GuiBlockCategoryDefinitions Created"); 
        GlobalEditor.RefreshTree(); 
    } 
    
    public void GenerateCargoContainerVolume(List<object> objects)
    {
        var defs = Sugar.Flatten(objects);

        DefinitionFile df1 = null, df2 = null;
        foreach (var m in GlobalEditor.Mods)
        {
            foreach (var d in m.Definitions)
            {
                if (!d.ReadOnly && d.File.Contains("Cargo\\EntityContainer"))
                {
                    df1 = d;
                }
                if (!d.ReadOnly && d.File.Contains("Cargo\\EntityComponents"))
                {
                    df2 = d;
                }
            }
        }
        
        
        Mod mod;
        if (df1 == null || df2 == null)
        {
            mod = GetOrCreateOutMod();
            if (df1 == null)
            {
                df1 = new DefinitionFile(mod);
                df1.File = "Out";
                df1.AlwaysVisible = true;
                mod.Definitions.Add(df1);
            } 
            if (df2 == null)
            {
                df2 = new DefinitionFile(mod);
                df2.File = "Out";
                df2.AlwaysVisible = true;
                mod.Definitions.Add(df2);
            } 
        }

        
        
        
        foreach (var def in defs)
        {
            if (def is MyObjectBuilder_CargoContainerDefinition block)
            {
                var entityContainer = new MyObjectBuilder_ContainerDefinition();
                entityContainer.DefaultComponents = new MyObjectBuilder_ContainerDefinition.DefaultComponentBuilder[1];
                entityContainer.DefaultComponents[0] = new MyObjectBuilder_ContainerDefinition.DefaultComponentBuilder();
                entityContainer.DefaultComponents[0].BuilderType = "MyObjectBuilder_Inventory";
                entityContainer.DefaultComponents[0].InstanceType = "CargoContainer";
                entityContainer.DefaultComponents[0].SubtypeId = block.Id.SubtypeName;

                entityContainer.Id = new SerializableDefinitionId(MyObjectBuilderType.Parse("MyObjectBuilder_CargoContainer"), block.Id.SubtypeName);

                var entityComponent = new MyObjectBuilder_InventoryComponentDefinition();
                entityComponent.Id = new SerializableDefinitionId(MyObjectBuilderType.Parse("MyObjectBuilder_Inventory"), block.Id.SubtypeName);
                double volume = block.Size.X * block.Size.Y * block.Size.Z;
                volume *= Math.Pow(1.25, block.GetTier()-1);
                volume *= (block.CubeSize == MyCubeSize.Small ? 1 : 25);
                volume *= 300;
                entityComponent.Size = new SerializableVector3((float)volume / 1000f, 1, 1);
                entityComponent.Volume = (float)volume;
                df1.AllItems.Add(entityContainer);
                df2.AllItems.Add(entityComponent);
            }
        }
        
        GlobalEditor.RefreshTree();
    }

    
    public void CheckIds(List<object> objects)
    {
        foreach (var obj in objects)
        {
            if (obj is Mod mod)
            {
                var dict = new Dictionary<MyDefinitionId, object>();
                var sb = new StringBuilder();
                foreach (var d in mod.Definitions)
                {
                    foreach (var item in d.AllItems)
                    {
                        if (item is MyObjectBuilder_DefinitionBase db)
                        {
                            if (dict.ContainsKey(db.Id))
                            {
                                sb.AppendLine($"{db.Id} Duplicate");
                            }
                            else
                            {
                                dict[db.Id] = db;
                            }
                        }
                    }
                }

                if (sb.Length > 0)
                {
                    MessageBox.Show($"In mod {mod.RootFolder} found duplicates: "+sb);
                }
                else
                {
                    MessageBox.Show($"No Id duplicates in mod {mod.RootFolder}");
                }
            }
        }
    }

    
    
    public void SplitByType(List<object> objects)
    {
        foreach (var o in objects)
        {
            if (o is DefinitionFile df) {
                var mod = df.Mod;
                var dict = new Dictionary<string, List<object>>();
                foreach (var oo in df.AllItems)
                {
                    if (oo is MyObjectBuilder_CubeBlockDefinition block)
                    {
                        var type = block.Id.TypeId.ToString().Replace("MyObjectBuilder_", "");
                        dict.GetOrNew(type).Add(block);
                    }
                }
                df.AllItems.Clear();
                
                var modFolder = Directory.GetParent(df.File).FullName;
                foreach (var pair in dict)
                {
                    var file = Path.Combine(modFolder, pair.Key);
                    DefinitionFile ddf = null;
                    foreach (var definition in mod.Definitions)
                    {
                        if (definition.File == file)
                        {
                            ddf = definition;
                        }
                    }

                    if (ddf == null)
                    {
                        ddf = new DefinitionFile(mod);
                        ddf.File = file+".sbc";
                        mod.Definitions.Add(ddf);
                    }
                    
                    ddf.AllItems.AddRange(pair.Value);
                }
            }
        }
        
        GlobalEditor.RefreshTree();
    }

    public void Clear(List<object> objects)
    {
        foreach (var o in objects)
        {
            if (o is DefinitionFile df)
            {
                df.AllItems.Clear();
            }
        }
    }
    
    public void IconGenerator(List<object> objects)
    {
        MessageBox.Show("Enter 1) source folder 2) overlap folder 3) output folder");

        var source = FolderPicker.Show();
        var overlap = FolderPicker.Show();
        var output = FolderPicker.Show();
        
        IconMerger.MergeFolders(source, overlap, output);
    }

    public void GenerateIcons(List<object> objects)
    {
        var tierFolder = FolderPicker.Show();
        var content = Settings.Data.GetValueOrDefault(Settings.GameContentPath) ?? "";
        var defs = Sugar.Flatten(objects);
        foreach (var o in defs)
        {
            var definition =o as MyObjectBuilder_DefinitionBase;
            if (definition == null) continue;
            if (definition.Icons == null) continue;
            
            var df = GlobalEditor.GetDefinitionFile(definition);
            var mpath = df.Mod.ModRoot ?? Path.GetDirectoryName(df.File);
            var tier = definition.GetTier();

            
            var tierIcon = Path.Combine(tierFolder, Helper.GenerateTierString(tier)+".png");
            if (!File.Exists(tierIcon))
            {
                Output.Info($"{definition.Id} : Tier icon not found {tierIcon}");
                continue;
            }
            
            
            for (var i=0; i<definition.Icons.Length; i++)
            {
                var icon = definition.Icons[i];
                String path = null; 
                if (!File.Exists(Path.Combine(mpath, icon)))
                {
                    if (!File.Exists(Path.Combine(content, icon)))
                    {
                        Output.Info($"File doesn't exist: {Path.Combine(content, icon)} {Path.Combine(mpath, icon)}");
                        continue;
                    }
                    else
                    {
                        path = Path.Combine(content, icon);
                    }
                }
                else
                {
                    path = Path.Combine(mpath, icon);
                }
                Output.Info("GenerateIcons5:");
                
                var outfolder = $"Data/Icons/{definition.Id.TypeIdString.Replace("MyObjectBuilder_", "")}";
                var outpath = $"{outfolder}/{definition.Id.SubtypeId}.png";
                Directory.CreateDirectory(Path.Combine(mpath,outfolder));
                
                
                var realOutpath = Path.Combine(mpath, outpath);
                
                if (tier <= 1)
                {
                    if (mpath == outpath) continue;
                    Output.Info($"{definition.Id} : Tier <= 1 Copying {path} {realOutpath}");
                    File.Copy(path, realOutpath, true);
                    definition.Icons[i] = outpath;
                }
                else
                {
                    try
                    {
                        Output.Info($"Merge: [{path}] & [{tierIcon}] => [{realOutpath}]");
                        IconMerger.Merge(path,  realOutpath, tierIcon);
                        definition.Icons[i] = outpath;
                    }
                    catch (Exception e)
                    {
                        Output.Error(e, $"{definition.Id} : Error generating icon");
                    }
                }
            }
        }
    }

    public void GenerateIds(List<object> objects)
    {
        var def = Sugar.Flatten(objects);
        var sb = new StringBuilder();
        foreach (var d in def)
        {
            if (d is MyObjectBuilder_DefinitionBase db)
                sb.AppendLine("" + db.Id);
        }

        MessageBox.Show(sb.ToString());
    }

    public void ExportBlockPositions(List<object> objects)
    {
        var elements = Sugar.Flatten(objects);
        Dictionary<int, List<MyBlockPosition>> blockPos = new Dictionary<int, List<MyBlockPosition>>();

         
        foreach (var e in elements)
        {
            if (e is MyBlockPosition bp)
            {
                blockPos.GetOrNew(bp.Position.Y).Add(bp);
                
            }
        }

        var max = blockPos.Keys.Max();
        var min = blockPos.Keys.Min();

        var sb = new StringBuilder();
        
        for (var i=min; i<=max; i++)
        {
            if (blockPos.TryGetValue(i, out var list))
            {
                list.Sort((a, b) =>
                {
                    var dx = a.Position.X - b.Position.X;
                    if (dx == 0)
                    {
                        return a.Name.CompareTo(b.Name);
                    }

                    return dx;
                });


                int prev = 0;
                foreach (var position in list)
                {
                    var skipped = position.Position.X - prev - 1;
                    for (int j = 0; j < skipped; j++)
                    {
                        sb.Append("\t");
                    }

                    prev = position.Position.X;
                    sb.Append(position.Name);
                    sb.Append("\t");
                }
                sb.Append("\r\n");
            }
           
            Clipboard.SetText(sb.ToString());
        }
    }
    public void GenerateBlockPositions(List<object> objects)
    {
        var text = Clipboard.GetText();
        var strings = text.Split(new string[] {"\r\n", "\n"}, StringSplitOptions.None);

        var y = 0;
        var sb = new StringBuilder();
        foreach (var s in strings)
        {
            var par = s.Split("\t");
            var x = 0;
            foreach (var t in par)
            {
                if (t != "")
                {
                    sb.AppendLine($"<BlockPosition><Position><X>{x}</X><Y>{y}</Y></Position><Name>{t}</Name></BlockPosition>");
                }
                x++;
            }
            sb.AppendLine();
            y++;
        }
        
        var ss = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        ss +="<Definitions xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n";
        ss += $"<BlockVariantGroups>\n{sb.ToString()}\n</BlockVariantGroups>\n";
        ss += "</Definitions>";
        
        Clipboard.SetText(ss);
    }

    /*public void GenerateBlockPositions(List<object> objects)
    {
        int fromX = 0; 
        int fromY = 10;


        var list = new Dictionary<string, List<MyObjectBuilder_BlockVariantGroup>>();
        GlobalEditor.ForAll<MyObjectBuilder_BlockVariantGroup>((xx) =>
        {
            list.GetOrNew(xx.Blocks[0].TypeIdString).Add(xx);
        });

        var sb = new StringBuilder();
        
        var y = fromY;
        
        var sb2 = new StringBuilder();
        foreach (var kv in list)
        {
            var x = fromX;
            sb.Append($"<!-- {kv.Key} -->\n");
            
            foreach (var bvg in kv.Value)
            {
                if (bvg.Blocks == null || bvg.Blocks.Length == 0) continue;

                var type = bvg.Blocks[0].TypeId;
                var subtype = bvg.Blocks[0].SubtypeId;
                
                String pair = null;
                GlobalEditor.ForAll<MyObjectBuilder_CubeBlockDefinition>((xx) =>
                {
                    if (xx.Id.TypeId == type && xx.Id.SubtypeName == subtype)
                    {
                        pair = xx.BlockPairName;
                    }
                });
                
                if (pair == null)
                {
                    sb.AppendLine($"NOT FOUND: {type} {subtype}");
                    continue;
                }
                
                sb2.Append($"{pair}\t");
                sb.AppendLine($"<BlockPosition><Position><X>{x}</X><Y>{y}</Y></Position><Name>{pair}</Name></BlockPosition>");
                x++;
            }

            sb2.AppendLine();
            y++;
        }
        
     
        
        Output.Info(sb.ToString());
        Output.Info(sb2.ToString());
    }*/


    public static void Calc()
    {
        var G = 9.81;
        var dArg1 = 1;
        var dArg2 = 1;
        var dArg3 = 10000;
        var Tier = 1;

    }

    
    
    public static MyObjectBuilder_CubeBlockDefinition GetBlock(string b)
    {
        return Output.GetQDefinition<MyObjectBuilder_CubeBlockDefinition>(b);
    }
    
    public static float GetMass(string b)
    {
        return GetBlock(b).GetMass();
    }
    
    public void ApplyCraftsAndFormulas(List<object> objects)
    {
        var dialog = GlobalEditor.OpenFileDialog();
        var path =  dialog[0];
        if (path == null) return;
        
        var def = Sugar.Flatten(objects);
        
        var settings = new XReader().Read<BlocksSettings>(path);
        
        
        var formulas = new Dictionary<string, Func<object, FormulaArgument, object>>();

        foreach (var f in settings.Formulas)
        {
            formulas[f.Name] = ScriptManager.CreateFormula(f.Value);
        }


        var sb = new StringBuilder();
        
        var mod = GetOrCreateOutMod();
        var df1 = new DefinitionFile(mod);
        df1.File = "Out";
        df1.AlwaysVisible = true;
        
        var df2 = new DefinitionFile(mod);
        df2.File = "Out";
        df2.AlwaysVisible = true;
        mod.Definitions.Add(df1);
        mod.Definitions.Add(df2);
        
        RefreshMass();
        
        foreach (var f in def)
        {
            if (f is MyObjectBuilder_CubeBlockDefinition block)
            {
                var cc = ClassCache.Get(block.GetType());
                var special = block.GetSpecial();//.Replace("-DLC", "");
                var tier = block.GetTier();
                
                var type = block.Id.TypeId.ToString().Replace("MyObjectBuilder_", "");
                
                foreach (var setting in settings.Settings)
                {
                    if ((special != setting.SpecialName && setting.SpecialName != "*") || (setting.Type != type && setting.Type != "*"))
                    {
                        continue;
                    }
     
                    if (setting.Craft != null && setting.Craft.Length > 0)
                    {
                        GenerateCrafts(block, setting.Craft, setting.CriticalElement, setting.LargeBlockMlt);
                    }

                    var mass = block.GetMass();

                    if (setting.Formulas != null)
                    {
                        foreach (var formula in setting.Formulas)
                        {
                            

                            if (!formulas.TryGetValue(formula.FxName, out var fx) || fx == null)
                            {
                                Output.Error(null,$"Not formula: {setting.Type}/{setting.SpecialName} -> {formula.FxName}");
                                continue;
                            }
                            
                            var arg = new FormulaArgument();
                            arg.Arg1 = formula.Arg1;
                            arg.Arg2 = formula.Arg2;
                            arg.Arg3 = formula.Arg3;
                            arg.Mass = mass;
                            arg.Tier = tier;
                            arg.SizeMlt = setting.LargeBlockMlt;
                            arg.GetMass = (b)=>
                            {
                                try
                                {
                                    return Output.GetQDefinition<MyObjectBuilder_CubeBlockDefinition>(b).GetMass();
                                }
                                catch (Exception e)
                                {
                                    Output.Error(e, "SEARCH: " + b + " " );
                                    return Output.GetQDefinition<MyObjectBuilder_CubeBlockDefinition>(b).GetMass();
                                    throw e;
                                }
                            };
                            arg.GetBlock = (b)=>Output.GetQDefinition<MyObjectBuilder_CubeBlockDefinition>(b);
                            
                            var result = fx.Invoke(block, arg);
                            if (block is MyObjectBuilder_CargoContainerDefinition cargoBlock && formula.Name == "Volume")
                            {
                                var vol = (double)result;
                                var entityContainer = new MyObjectBuilder_ContainerDefinition();
                                entityContainer.DefaultComponents = new MyObjectBuilder_ContainerDefinition.DefaultComponentBuilder[1];
                                entityContainer.DefaultComponents[0] = new MyObjectBuilder_ContainerDefinition.DefaultComponentBuilder();
                                entityContainer.DefaultComponents[0].BuilderType = "MyObjectBuilder_Inventory";
                                entityContainer.DefaultComponents[0].InstanceType = "CargoContainer";
                                entityContainer.DefaultComponents[0].SubtypeId = block.Id.SubtypeName;

                                entityContainer.Id = new SerializableDefinitionId(MyObjectBuilderType.Parse("MyObjectBuilder_CargoContainer"), block.Id.SubtypeName);

                                var entityComponent = new MyObjectBuilder_InventoryComponentDefinition();
                                entityComponent.Id = new SerializableDefinitionId(MyObjectBuilderType.Parse("MyObjectBuilder_Inventory"), block.Id.SubtypeName);
                                entityComponent.Size = new SerializableVector3((float)(vol/1000), 1, 1);
                                entityComponent.Volume = (float)vol;

                                df1.AllItems.Add(entityContainer);
                                df2.AllItems.Add(entityComponent);
                            }
                            else if (block is MyObjectBuilder_OxygenGeneratorDefinition generator && formula.Name.StartsWith("ProducedGases"))
                            {
                                var s = formula.Name.Split(".");
                                if (int.TryParse(s[1], out var n))
                                {
                                    generator.ProducedGases[n] = new MyObjectBuilder_GasGeneratorResourceInfo()
                                    {
                                        IceToGasRatio = (float) (double) result,
                                        Id = generator.ProducedGases[n].Id
                                    };
                                }
                            }
                            else if (block is MyObjectBuilder_OxygenFarmDefinition farm && formula.Name.StartsWith("ProducedGas"))
                            {
                                farm.ProducedGas = new MyObjectBuilder_OxygenFarmDefinition.MyProducedGasInfo()
                                {
                                    MaxOutputPerSecond = (float) (double) result,
                                    Id = farm.ProducedGas.Id
                                };
                            }
                            else
                            {
                                if (!GetObjectToExecute(block, formula.Name, out var toApply, out var mi))
                                {
                                    sb.AppendLine($"Not found field: {setting.Type}/{setting.SpecialName} : {formula.Name}");
                                    continue;
                                }
                                mi.EasySetValue(toApply, result);
                            }
                        }
                    }
                }
            }
        }
        
        
        GlobalEditor.RefreshTree();
        Output.Info(sb.ToString());
    }

    private bool GetObjectToExecute(object block, string name, out object toApply, out MemberInfo mi)
    {
        toApply = block;
        mi = null;
        var split = name.Split(new string[] {"."}, StringSplitOptions.None);
        for (var x=0; x<split.Length; x++)
        {
            var cc = ClassCache.Get(toApply.GetType());
            if (!cc.FieldsAndAttrs.TryGetValue(split[x], out mi))
            {
                Output.Info($"GetObjectToExecute: not found [{split[x]} {toApply.GetType()}]");
                toApply = null;
                mi = null;
                return false;
            }


            if (x < split.Length - 1)
            {
                toApply = mi.GetValue(toApply);
                Output.Info($"GetObjectToExecute: get inner [{split[x]} {toApply.GetType()}]");
            }
        }

        return true;
    }

    
    public void GenerateBlockPairNames(List<object> objects)
    {
        var def = Sugar.Flatten(objects);
        foreach (var f in def)
        {
            if (f is MyObjectBuilder_CubeBlockDefinition block)
            {
                if (block.BlockPairName.IsSpecialPairName()) return;
                GenerateBlockPairName(block, block.Size, block.Size);
            }
        }
    }
    
    public void GenerateDisplayNameAndDescriptions(List<object> objects)
    {
        var def = Sugar.Flatten(objects);
        foreach (var f in def)
        {
            if (f is MyObjectBuilder_DefinitionBase block)
            {
                GenerateDisplayNameAndDescription(block);
            }
        }
    }


    public void GenerateDisplayNameAndDescription(MyObjectBuilder_DefinitionBase definition)
    {
        if (definition is MyObjectBuilder_CubeBlockDefinition block)
        {
            definition.DisplayName = $"DisplayName_{block.BlockPairName}";
            definition.Description = $"Description_{block.BlockPairName.RemoveTier().RemoveSize()}";
        }
    }

    public void GenerateCrafts(MyObjectBuilder_CubeBlockDefinition block, CubeBlockComponent[] cmps, MyObjectBuilder_CubeBlockDefinition.CriticalPart crcm, double LargeMlt = 125d)
    {
        var tier = block.GetTier();
        double volume = block.Size.x * block.Size.y * block.Size.z;

        if (block.CubeSize == MyCubeSize.Large)
        {
            volume *= LargeMlt;
        }

        var array = new List<MyObjectBuilder_CubeBlockDefinition.CubeBlockComponent>();
        for (var x = 0; x < cmps.Length; x++)
        {
            var c = new MyObjectBuilder_CubeBlockDefinition.CubeBlockComponent();
            array.Add(c);

            var left = (int)(cmps[x].Amount * volume);
            do
            {
                var count = Math.Min(left, ushort.MaxValue);
                left -= count;

                c.Subtype = cmps[x].Subtype;
                var tierId = cmps[x].Subtype + "_" + Helper.GenerateTierString(tier);
                if (Components.ContainsKey(tierId))
                {
                    c.Subtype = tierId;
                }

                c.Count = (ushort) count;
                c.DeconstructId = cmps[x].DeconstructId == null ? new SerializableDefinitionId(null, null) : cmps[x].DeconstructId.ToId();
                c.Type = MyObjectBuilderType.Parse("MyObjectBuilder_Component");
            } while (left > ushort.MaxValue);
        }

        var cr = new MyObjectBuilder_CubeBlockDefinition.CriticalPart();
        cr.Subtype = crcm.Subtype;
        var crtierId = crcm.Subtype + "_" + Helper.GenerateTierString(tier);
        
        if (Components.ContainsKey(crtierId))
        {
            cr.Subtype = crtierId;
        }

        block.CriticalComponent = cr;
        block.Components = array.ToArray();
    }

    public DefinitionFile FindDefinitionFile(Func<DefinitionFile, bool> finder)
    {
        foreach (var m in GlobalEditor.Mods)
        {
            foreach (var d in m.Definitions)
            {
                var fileName = Path.GetFileName(d.File);
                if (finder.Invoke(d))
                {
                    return d;
                }
            }
        }

        return null;
    }
    
    public Mod GetOrCreateOutMod()
    {
        Mod mod = null;
        foreach (var m in GlobalEditor.Mods)
        {
            if (m.RootFolder == "Out")
            {
                mod = m;
            }
        }

        if (mod == null)
        {
            mod = new Mod("Out");
            GlobalEditor.Mods.Add(mod);
        }
        
        return mod;
    }
    
    public void GenerateMany(List<object> items)
    {
        var am = new InputBox("How many tiers").ShowDialog();
        int tiers = int.Parse(am);

        var mod = GetOrCreateOutMod();
       
        
        if (GlobalEditor.Mods.Count >= 2)
        {
            var def = new DefinitionFile(mod); 
            def.File = "New generated copies"; 
            def.Mod.RootFolder = "Out"; 
            def.AlwaysVisible = true; 
            mod.Definitions.Add(def); 
            //GlobalEditor.Mods[1].Definitions.Add(def); 

            var extraname = new InputBox("ExtraName", "", "").ShowDialog();
            foreach (var item in items)
            {
                if (item is MyObjectBuilder_DefinitionBase definitionBase)
                {
                    for (var tier = 1; tier <= tiers; tier++)
                    {
                        var newDefinition = (MyObjectBuilder_DefinitionBase) GlobalEditor.Clone(definitionBase);
                        var newExtraName = extraname + definitionBase.GetDLC();

                        if (newDefinition is MyObjectBuilder_CubeBlockDefinition block)
                        {
                            block.BlockVariants = null;
                        }
                        def.AllItems.Add(newDefinition);
                        def.AlwaysVisible = true;
                        var name = GenerateId(newDefinition, newExtraName, tier);
                        newDefinition.Id.SubtypeName = name;
                    }
                }
            }
        }
        
        Output.Info(DateTime.Now + ":  New generated copies created"); 
        GlobalEditor.RefreshTree();
    }

    public void RenameSpecial(List<object> items)
    {
        var am = new InputBox("How many tiers").ShowDialog();
        int tiers = int.Parse(am);
        
        var extraname = new InputBox("ExtraName", "", "").ShowDialog();
        foreach (var item in items)
        {
            if (item is MyObjectBuilder_DefinitionBase definitionBase)
            {
                var def = GlobalEditor.GetDefinitionFile(definitionBase);
                var newBlock = (MyObjectBuilder_DefinitionBase) GlobalEditor.Clone(definitionBase);
                var newExtraName = extraname + definitionBase.GetDLC();
                var tier = definitionBase.GetTier();
                
                if (newBlock is MyObjectBuilder_CubeBlockDefinition block)
                {
                    if (tier == 1)
                    {
                        block.BlockVariants = new SerializableDefinitionId[tiers-1];
                        for (int i = 2; i <= tiers; i++)
                        {
                            block.BlockVariants[i - 2] = new SerializableDefinitionId(block.TypeId, GenerateId(block, newExtraName, i));
                        }
                    }
                    else
                    {
                        block.BlockVariants = null;
                    }
                }

                def.AllItems.Add(newBlock);
                def.AllItems.Remove(definitionBase);
                def.AlwaysVisible = true;
                var name = GenerateId(newBlock, newExtraName, tier);
                newBlock.Id.SubtypeName = name;
            }
        }
        

        GlobalEditor.RefreshTree();
    }

    
    public void RefreshMass()
    {
        Components.Clear();
        Ores.Clear();
        Ingots.Clear();
        
        GlobalEditor.ForAll<MyObjectBuilder_ComponentDefinition>((cmp) =>
        {
            Components[cmp.Id.SubtypeName] = cmp;
        });
        
        GlobalEditor.ForAll<MyObjectBuilder_PhysicalItemDefinition>((cmp) =>
        {
            var type = cmp.Id.TypeId.ToString();
            if (type.EndsWith("Ore"))
            {
                Ores[cmp.Id.SubtypeName] = cmp;
            }
            if (type.EndsWith("Ingot"))
            {
                Ingots[cmp.Id.SubtypeName] = cmp;
            }
        });
    }

    public string GenerateId(MyObjectBuilder_DefinitionBase definitionBase, string extraname, int tier)
    {
        if (definitionBase is MyObjectBuilder_CubeBlockDefinition block)
        {
            var gsize = block.CubeSize == MyCubeSize.Large ? "L" : "S";
            var size = block.Size.X + "x" + block.Size.Y + "x" + block.Size.Z;
            var tierS = Helper.GenerateTierString(tier);
            var extra = "";
            if (string.IsNullOrEmpty(extraname))
            {
                extra = "";
            }
            else
            {
                extra = "_" + extraname;
            }

            return $"{gsize}{size}{extra}_{tierS}";
        }
        
        if (definitionBase is MyObjectBuilder_PhysicalItemDefinition comp)
        {
            var name = comp.GetSpecial();
            var tierS = Helper.GenerateTierString(tier);
            if (extraname == null || extraname == "")
            {
                extraname = "";
            }
            else
            {
                extraname = "_" + extraname;
            }
            return $"{name}{extraname}_{tierS}";
        }
        
        throw new Exception("Not able to generate Id for " + definitionBase.GetType());
    }
}

static class Helper
    {

        public static MyObjectBuilder_PhysicalItemDefinition GetTieredSubtype(string type, string subtype, string tierSubtype)
        {
            try
            {
                if (type == "Ore")
                {
                    if (Script.Ores.ContainsKey(tierSubtype))
                    {
                        return Script.Ores[tierSubtype];
                    }
                    else
                    {
                        return Script.Ores[subtype];
                    }
                }
                else if (type == "Ingot")
                {
                    if (Script.Ingots.ContainsKey(tierSubtype))
                    {
                        return Script.Ingots[tierSubtype];
                    }
                    else
                    {
                        return Script.Ingots[subtype];
                    }
                } else if (type == "Component")
                {
                    if (Script.Components.ContainsKey(tierSubtype))
                    {
                        return Script.Components[tierSubtype];
                    }
                    else
                    {
                        return Script.Components[subtype];  
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                Output.Error(e, $"Couldn't find {type}:{subtype}/{tierSubtype}");
                throw e;
            }
        }
        public static string GetBlockVariantId(this MyObjectBuilder_CubeBlockDefinition block)
        {
            var n = block.Id.SubtypeName;
            var split = n.LastIndexOf("_");
            
            
            if (split != -1)
            {
                n =n.Substring(1, split - 1);
            }

            return block.Id.TypeId.ToString().Replace("MyObjectBuilder_", "") + "_" + n;
        }
        
        public static string GenerateTierString(int tier)
        {
            if (tier < 10)
            {
                return "T0" + tier;
            }

            return "T" + tier;
        }
        
        public static string RemoveTier(this string s)
        {
            return s
                .Replace("_T01", "")
                .Replace("_T02", "")
                .Replace("_T03", "")
                .Replace("_T04", "")
                .Replace("_T05", "")
                .Replace("_T06", "")
                .Replace("_T07", "")
                .Replace("_T08", "")
                .Replace("_T09", "");
        }

        private static Regex SizeRegex = new Regex("_\\d+x\\d+x\\d+(-\\d+x\\d+x\\d+)?");
        private static Regex DoubleSizeRegex = new Regex("_\\d+x\\d+x\\d+-\\d+x\\d+x\\d+");
        
        public static string RemoveSize(this string s)
        {
            return SizeRegex.Replace(s, "");
        }
        
        public static bool IsSpecialPairName(this string s)
        {
            return DoubleSizeRegex.Matches(s).Count == 1;
        }

        public static float GetMass(this MyObjectBuilder_CubeBlockDefinition block)
        {
            float total = 0;
            foreach (var b in block.Components)
            {
                var tier = block.GetTier();
                if (tier != 0 && Script.Components.TryGetValue(b.Subtype+"_"+Helper.GenerateTierString(tier), out var cmp1))
                {
                    total += b.Count * cmp1.Mass;
                    continue;
                }
                
                if (Script.Components.TryGetValue(b.Subtype, out var cmp2))
                {
                    total += b.Count * cmp2.Mass;
                }
            }

            return total;
        }
        
        public static int GetTier(this MyObjectBuilder_DefinitionBase block)
        {
            try
            {
                var parts = block.Id.SubtypeName.Split(new string[] {"_"}, StringSplitOptions.None);
                var n = parts[parts.Length-1].Substring(1);
                if (n.StartsWith("0")) n = n.Substring(1);
                return int.Parse(n);
            }
            catch (Exception e)
            {
                //MessageBox.Show(""+e);
                return 0;
            }
        }

        public static double GenerateValue(double baseValue, double mlt, int tier)
        {
            return baseValue * Math.Pow(mlt, tier - 1);
        }

        public static string GetDLC(this MyObjectBuilder_DefinitionBase block)
        {
            if (block.DLCs != null && block.DLCs.Length != 0)
            {
                return "-DLC";
            }

            return "";
        }
        
        public static string GetSpecial(this MyObjectBuilder_DefinitionBase def, bool withSubSpecial = false)
        {
            if (def is MyObjectBuilder_CubeBlockDefinition block)
            {
                //string dlc = block.GetDLC();
                var split = block.Id.SubtypeName.Split(new string[] {"_"}, StringSplitOptions.None);
                if (split.Length == 3)
                {
                    if (!withSubSpecial)
                    {
                        var split2 = split[1].Split(new string[] {"-"}, StringSplitOptions.None)[0];
                        return split2;
                    }
                    return split[1];
                }
                return "";
            }
            
            if (def is MyObjectBuilder_ComponentDefinition || def is MyObjectBuilder_PhysicalItemDefinition)
            {
                var split = def.Id.SubtypeName.Split(new string[] {"_"}, StringSplitOptions.None);
                return split[0];
            }

            throw new Exception();
        }
    }

/*public void GenerateTranslations(List<object> objects)
    {
        var loc = new Localization();

        var defs = Sugar.Flatten(objects);

        var dict = new Dictionary<string, MyObjectBuilder_CubeBlockDefinition>();
        foreach (var def in defs)
        {
            if (def is MyObjectBuilder_CubeBlockDefinition block)
            {
                var special = block.Description;
                dict[special] = block;
            }
        }

        foreach (var d in dict)
        {
            var sp = d.Value.GetSpecial();
            var tp = d.Value.Id.TypeId.ToString().Replace("MyObjectBuilder_", "");
            
            var ru = new LocalizationItem();
            ru.Name = "Name";
            ru.Description = "Description";
            ru.DescriptionExtra = "";
            ru.Key = "ru";
                
            var en = new LocalizationItem();
            en.Name = "Name";
            en.Description = "Description";
            en.DescriptionExtra = "";
            en.Key = "en";

            LocalizationLanguage ll = new LocalizationLanguage();
            ll.Translate.Add(ru);
            ll.Translate.Add(en);
            ll.Key = tp + "_" + sp;
                
            var give1 = new Gives();
            give1.Key = "Speed";
            give1.Arg1 = "15";
                
            var give2 = new Gives();
            give2.Key = "Power";
            give2.Arg1 = "25";
                
                
            ll.Gives.Add(give1);
            ll.Gives.Add(give2);
                
            loc.Items.Add(ll);
        }

        var f = new FormulaTranslation();
        f.Key = "Speed";
        var fru = new FormulaLang();
        var fen = new FormulaLang();
        fru.Key = "ru";
        fru.Value = "[+{0}% Скорости]";
        fen.Key = "en";
        fen.Value = "[+{0}% Speed]";
        f.Translations.Add(fru);
        f.Translations.Add(fen);
        
        loc.Gives.Add(f);

        var dialog = GlobalEditor.OpenFileDialog();
        var path =  dialog[0];
        if (path == null) return;
        
        new XWriter().Write(path, loc);
    }
    
    
    public void GenerateCrafts(List<object> objects)
    {
        var crafts = new BlueprintCrafts();
        var defs = Sugar.Flatten(objects);

        var dict = new Dictionary<string, MyObjectBuilder_ComponentDefinition>();
        foreach (var def in defs)
        {
            if (def is MyObjectBuilder_ComponentDefinition cmp)
            {
                dict[cmp.GetSpecial()] = cmp;
            }
        }

        foreach (var d in dict)
        {
            var sp = d.Value.GetSpecial();

            var bp = new BlueprintCraft();
            var cmp = new BlueprintCraftItem();
            cmp.Amount = 1;
            cmp.MinTier = 0;
            cmp.MaxTier = 999;
            cmp.AmountTierMlt = 1;
            cmp.IdTierGrow = 1;
            cmp.IdTierStart = 0;
            cmp.Id = $"Component/{sp}";

            var ingot = new BlueprintCraftItem();
            ingot.Amount = 1;
            ingot.MinTier = 0;
            ingot.MaxTier = 999;
            ingot.AmountTierMlt = 1;
            ingot.IdTierGrow = 0.34f;
            ingot.IdTierStart = 0;
            ingot.Id = "Ingot/Iron";
            
            bp.In.Add(ingot);
            bp.Out.Add(cmp);

            bp.Tiers = 9;
            bp.Time = 10;
            crafts.Crafts.Add(bp);
        }

        var dialog = GlobalEditor.OpenFileDialog();
        var path =  dialog[0];
        if (path == null) return;
        
        new XWriter().Write(path, crafts);
    }
    */