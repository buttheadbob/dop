﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using BlockEditor.GuiData;
using BlockEditor.Wpf;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Sandbox.Common.ObjectBuilders.Definitions;
using VRage.Game;
using ContextMenu = System.Windows.Controls.ContextMenu;
using MenuItem = System.Windows.Controls.MenuItem;
using MessageBox = System.Windows.MessageBox;

namespace BlockEditor
{
    public static class GlobalEditor
    {
        public static List<Mod> Mods = new List<Mod>();
        public static event Action ModsChanged;
        public static List<object> SelectedItems
        {
            get
            {
                var list = new List<object>();
                foreach (var tb in MainWindow.INSTANCE.DefinitionsTree.SelectedItems)
                {
                    var d = (tb as TextBlock).Tag;
                    
                    if (d is Mod mod)
                    {
                        foreach (var ddf in mod.Definitions)
                        {
                            foreach (var o in ddf.AllItems)
                            {
                                if (!list.Contains(o))
                                {
                                    list.Add(o);
                                }
                            }
                        }
                        
                        continue;
                    }
                    
                    if (d is DefinitionFile df)
                    {
                        foreach (var o in df.AllItems)
                        {
                            if (!list.Contains(o))
                            {
                                list.Add(o);
                            }
                        }
                        
                        continue;
                    }
                    
                    if (!list.Contains(d))
                    {
                        list.Add(d);
                    }
                }

                return list;
            }
        }

        public static void Load(string modOrFile, string modName, string search = "*.sbc", SearchOption option = SearchOption.AllDirectories, Action<Mod> AfterLoad = null)
        {
            var mod = Mods.Find((x) => x.Name == modName);
            mod = mod ?? new Mod(modOrFile);

            GUI.InvokeParallel(() =>
            {
                if (Directory.Exists(modOrFile))
                {
                    var files = Directory.GetFiles(modOrFile, search, option);
                    Parallel.ForEach(files, (x) =>
                    {
                        try
                        {
                            if (x.EndsWith("sbcB5")) return;
                            var stopwatch = new Stopwatch();
                            stopwatch.Start();
                            var df = new DefinitionFile(mod, x);
                            lock (mod)
                            {
                                mod.Definitions.Add(df);
                            }
                        
                            stopwatch.Stop();
                        
                            Output.Info($"Loaded: {x} in {stopwatch.Elapsed}");
                        }
                        catch (Exception e)
                        {
                            Output.Error(e, $"Couldn't load: {x}");
                        }
                    });
                
                    mod.Definitions.Sort((a, b) => String.CompareOrdinal(a.File, b.File));
                }
                else
                {
                    try
                    {
                        var df = new DefinitionFile(mod, modOrFile);
                        mod.Definitions.Add(df);
                    }
                    catch (Exception e)
                    {
                        Output.Error(e, $"Couldn't load: {modOrFile}");
                        return;
                    }
                }
                
                AfterLoad?.Invoke(mod);
                
                GUI.InvokeGui(() =>
                {
                    if (!Mods.Contains(mod))
                    {
                        Mods.Add(mod);
                    }
            
                    ModsChanged?.Invoke();
                });
            });
            
            GUI.Execute();
        }

        public static List<Action<List<object>, ContextMenu>> MenuInterceptor = new List<Action<List<object>, ContextMenu>>();

        public static string[] OpenFileDialog()
        {
            var dialog = new OpenFileDialog();
            dialog.ShowDialog();
            return dialog.FileNames;
        }
        
        public static string ExtractRelativePath(string s, string folder)
        {
            var d = Path.GetDirectoryName(s);

            while (true)
            {
                try
                {
                    if (Path.GetFileName(d).ToLower() == folder.ToLower())
                    {
                        return s.Replace(d, "").Substring(1);
                    }
                    else
                    {
                        d = Path.GetDirectoryName(d);
                    }
                }
                catch (Exception e)
                {
                    Output.Error(e, $"{d} | {s}");
                    return null;
                }
            }

            return null;
        } 
        
        public static string[] OpenFilesDialog()
        {
            var dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            dialog.ShowDialog();
            return dialog.FileNames;
        }
        
        public static string[] OpenFolderDialog()
        {
            var dialog = new FolderPicker();
            dialog.ShowDialog();

            if (dialog.ResultPath == null)
            {
                return null;
            }
            return new string[] {dialog.ResultPath};
        }
        
        static GlobalEditor()
        {
            var s = new Extern();
            MenuInterceptor.Add((objects, menu) =>
            {
                if (objects.Count == 0) return;
                
                AddMenu(menu, objects, "Copy (Control + C)", (oo) => MainWindow.INSTANCE.CopyLogic());
                AddMenu(menu, objects, "Paste (Control + V)", (oo) => MainWindow.INSTANCE.PasteCopiedLogic());
                AddMenu(menu, objects, "Cut (Control + X)", (oo) => {
                    MainWindow.INSTANCE.CopyLogic();
                    MainWindow.INSTANCE.RemoveLogic();
                });
                AddMenu(menu, objects, "Delete (Control + D)", (oo) => MainWindow.INSTANCE.RemoveLogic());
                
                AddMenu(menu, objects, "Create File", CreateFile);
                AddMenu(menu, objects, "Load File", LoadFile);
                AddMenu(menu, objects, "Open File Location", OpenFileLocation);
                AddMenu(menu, objects, "Switch Lock", SwitchLock);
                
                
            });
        }
        
        public static void SwitchLock(List<object> items)
        {
            foreach (var m in items)
            {
                if (m is DefinitionFile df)
                {
                    df.ReadOnly = !df.ReadOnly;
                }
            }
            GlobalEditor.RefreshTree();
        }

        private static void ShowInExplorer(string filePath)
        {
            if (!File.Exists(filePath) && !Directory.Exists(filePath)) { return; }

            string argument = "/select, \"" + filePath +"\"";

            Process.Start("explorer.exe", argument);
        }
        
        public static void OpenFileLocation(List<object> items)
        {
            foreach (var o in items)
            {
                if (o is Mod m)
                {
                    ShowInExplorer(m.ModRoot);
                }

                if (o is DefinitionFile df)
                {
                    ShowInExplorer(df.File);
                }
            }

        }
        
        public static void LoadFile(List<object> items)
        {
            var mod = items[0] as Mod;
            if (mod == null)
            {
                MessageBox.Show("Click on MOD");
                return;
            }

            var paths = GlobalEditor.OpenFileDialog();
            foreach (var p in paths)
            {
            
                var df = new DefinitionFile(mod, p);
                mod.Definitions.Add(df);
            }
            GlobalEditor.RefreshTree();
        }
        
        public static void AddMenu(MenuItem menu, List<object> objects, String name, Action<List<object>> handler)
        {
            var menuItem = new MenuItem();
            menuItem.Header = name;
            menuItem.Click += (o, e) =>
            {
                try
                {
                    handler(objects);
                }
                catch (Exception ee)
                {
                    Output.Error(ee, "AddMenu");            
                }
            };
            menu.Items.Add(menuItem);
        }
        
        public static void AddMenu(ContextMenu menu, List<object> objects, String name, Action<List<object>> handler)
        {
            var menuItem = new MenuItem();
            menuItem.Header = name;
            menuItem.Click += (o, e) =>
            {
                try
                {
                    handler(objects);
                }
                catch (Exception ee)
                {
                    Output.Error(ee, "AddMenu");            
                }
            };
            menu.Items.Add(menuItem);
        }
        
        private static void CreateFile(List<object> objects)
        {
            var name = new InputBox("Name", "", "").ShowDialog();
            name = name + ".sbc";

            Output.Info(""+objects.Count);
            foreach (var obj in objects)
            {
            
                if (obj is Mod mod)
                {
                    name = Path.Combine(mod.RootFolder, name);
                    DefinitionFile ddf = null;
                    foreach (var definition in mod.Definitions)
                    {
                        if (definition.File == name)
                        {
                            ddf = definition;
                        
                        }
                    }

                    if (ddf == null)
                    {
                        ddf = new DefinitionFile(mod);
                        ddf.AlwaysVisible = true;
                        ddf.File = name;
                        mod.Definitions.Add(ddf);
                    }
                    else
                    {
                        Output.Info("Found DDF:" + name);
                    }
                }
            }
        
        
            GlobalEditor.RefreshTree();
        
        }
        
        public static void ShowContextMenu(object sender, MouseButtonEventArgs e)
        {
            var menu = new ContextMenu();
            menu.PlacementTarget = (UIElement)sender;
            menu.IsOpen = true;
            
            
            var selected = MainWindow.INSTANCE.DefinitionsTree.SelectedItems;
            var list = new List<object>();
            foreach (var o in selected)
            {
                var tag = ((TextBlock) o).Tag;
                list.Add(tag);
            }
            
            
            foreach (var interceptor in MenuInterceptor)
            {
                interceptor.Invoke(list, menu);
            }

            if (menu.Items.Count > 0)
            {
                
            }
        }

        public static object Clone(object item)
        {
            return new FastDeepCloner.FastDeepCloner(item).Clone();
        }

        public static void RefreshTree()
        {
            MainWindow.INSTANCE.RefreshSearch(null, null);
        }


        public static void Foreach<T> (List<object> where, Action<T> action)
        {
            foreach (var o in where)
            {
                Foreach(o, action);
            }
        }

        public static void ForAll<T>(Action<T> action)
        {
            foreach (var o in GlobalEditor.Mods)
            {
                Foreach<T>(o, action);
            }
        }
        
        public static void Foreach<T> (object where, Action<T> action)
        {
            if (where is Mod mod)
            {
                foreach (var d in mod.Definitions) {
                    foreach (var ob in d.AllItems) {
                        if (ob is T t)
                        {
                            action(t);
                        }
                    }
                }
            } else if (where is DefinitionFile file) {
                foreach (var ob in file.AllItems) {
                    if (ob is T t) action(t);
                }
            } else {
                if (where is T t) action(t);
            }
        }
        
        public static DefinitionFile GetDefinitionFile(object obj)
        {
            foreach (var m in Mods)
            {
                foreach (var d in m.Definitions) {
                    foreach (var ob in d.AllItems) {
                        if (obj == ob)
                        {
                            return d;
                        }
                    }
                }
            }

            return null;
        }
    }
}