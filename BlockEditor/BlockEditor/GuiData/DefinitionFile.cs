﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Documents;
using System.Windows.Media.Animation;
using System.Xml;
using System.Xml.Serialization;
using BlockEditor.GuiData;
using BlockEditor.Serialization;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using ParallelTasks;
using RestSharp.Deserializers;
using RestSharp.Extensions;
using RestSharp.Serializers;
using Sandbox.Definitions;
using Sandbox.Engine.Utils;
using VRage;
using VRage.Game;
using VRage.ObjectBuilders;
using VRage.Utils;


namespace BlockEditor
{

    public class DefinitionFile
    {
        public bool ReadOnly = false;
        public bool AlwaysVisible;
        //public Dictionary<SerializableDefinitionId, MyObjectBuilder_DefinitionBase> Dict = new Dictionary<SerializableDefinitionId, MyObjectBuilder_DefinitionBase>();
        public Mod Mod { get; set; }
        public List<object> AllItems = new List<object>();
        public string File;

        public DefinitionFile(Mod Mod)
        {
            this.Mod = Mod;
        }
        
        public DefinitionFile(Mod Mod, String file)
        {
            this.Mod = Mod;
            this.File = file;

            if (Path.GetFileName(Path.GetDirectoryName(file)).StartsWith("_"))
            {
                ReadOnly = true;
            }
            
            var defs = new XReader().Read<MyObjectBuilder_Definitions>(File);

            var defInfo = ClassCache.Get(typeof(MyObjectBuilder_Definitions));
            foreach (var d in defInfo.Fields)
            {
                var memberInfo = d.Value;
                object value = memberInfo.GetValue(defs);
                if (value == null) continue;
                if (value.GetType().IsArray)
                {
                    var arr = (Array) value;
                    foreach (var v in arr)
                    {
                        AllItems.Add(v);
                    }
                }
            }
            
        }

        

        public void Register(object def)
        {
            AllItems.Add(def);
        }
        
        public void Save()
        {
            if (ReadOnly) return;
            
            var defInfo = ClassCache.Get(typeof(MyObjectBuilder_Definitions));
            Dictionary<Type, KeyValuePair<MemberInfo, List<object>>> storage = new Dictionary<Type, KeyValuePair<MemberInfo, List<object>>>();
            foreach (var d in defInfo.Fields)
            {
                var memberInfo = d.Value;
                var type = memberInfo.GetMemberType();
                if (!type.IsArray) continue;

                var elementType = type.GetElementType();
                if (elementType != typeof(MyObjectBuilder_DefinitionBase))
                {
                    if (!storage.TryGetValue(elementType, out var ex))
                    {
                        storage.Add(elementType, new KeyValuePair<MemberInfo, List<object>>(memberInfo, new List<object>()));
                    }
                }
            }
            
            

            

            foreach (var o in AllItems)
            {
                var e = o.GetType();
                foreach (var s in storage)
                {
                    if (s.Key.IsAssignableFrom(e))
                    {
                        s.Value.Value.Add(o);
                        break;
                    }
                }
            }

            var def = new MyObjectBuilder_Definitions();
            foreach (var s in storage)
            {
                if (s.Value.Value.Count > 0)
                {
                    var type = s.Value.Key.GetMemberType().GetElementType();
                    var arr = Array.CreateInstance(type, s.Value.Value.Count);

                    int i = 0;
                    foreach (var o in s.Value.Value)
                    {
                        arr.SetValue(o, i++);
                    }

                    s.Value.Key.SetValue(def, arr);
                }
            }
            
            /*var cubes = new List<MyObjectBuilder_CubeBlockDefinition>();
            var components = new List<MyObjectBuilder_ComponentDefinition>();
            var bps = new List<MyObjectBuilder_BlueprintDefinition>();
            var ammos = new List<MyObjectBuilder_AmmoDefinition>();
            foreach (var o in AllItems)
            {
                if (o is MyObjectBuilder_CubeBlockDefinition cube)
                {
                    cubes.Add(cube);
                }
                if (o is MyObjectBuilder_ComponentDefinition cmp)
                {
                    components.Add(cmp);
                }
                if (o is MyObjectBuilder_AmmoDefinition ammo)
                {
                    ammos.Add(ammo);
                }
                
                if (o is MyObjectBuilder_BlueprintDefinition bp)
                {
                    bps.Add(bp);
                }
            }

           
            def.CubeBlocks = cubes.ToArray();
            def.Components = components.ToArray();
            def.Blueprints = bps.ToArray();
            def.Ammos = ammos.ToArray();*/

            new XWriter().Write(File, def);
        }
    }

}