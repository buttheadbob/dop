﻿using System.Collections.Generic;
using System.IO;

namespace BlockEditor.GuiData
{
    public class Mod
    {
        public string RootFolder;
        public string Name;
        public string ModRoot;
        

        public Mod(string rootFolder)
        {
            RootFolder = rootFolder;
            ModRoot = GetModRoot();
        }

        public List<DefinitionFile> Definitions = new List<DefinitionFile>();

        public string GetModRoot()
        {
            if (Path.GetDirectoryName(RootFolder) == "Data")
            {
                return Directory.GetParent(RootFolder).FullName;
            }

            DirectoryInfo d = Directory.GetParent(RootFolder);
            while (true)
            {
                if (d == null) return null;
                if (d.Name == "Data")
                {
                    return d.Parent.FullName;
                }
                d = d.Parent;
            }
            
        }
    }
}