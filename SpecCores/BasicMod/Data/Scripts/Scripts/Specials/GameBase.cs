using System;
using System.Collections.Generic;
using Digi;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using Scripts;
using Scripts.Shared;
using Scripts.Specials;
using Scripts.Specials.Blocks;
using Slime;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRageMath;

using VRageMath;
using Slime;

namespace ServerMod
{
    [MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation | MyUpdateOrder.AfterSimulation)]
    public class GameBase : MySessionComponentBase
    {
        public static GameBase instance = null;
        private readonly List<Action> unloadActions = new List<Action>();
        public GameBase()
        {
            instance = this;
        }

        protected override void UnloadData()
        {
            foreach (var x in unloadActions)
            {
                try
                {
                    x.Invoke();
                }
                catch (Exception e)
                {
                    Log.ChatError(e);
                }
            }
        }

        public static void AddUnloadAction(Action a)
        {
            instance.unloadActions.Add(a);
        }
    }
}