﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Digi;
using ProtoBuf;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces;
using Sandbox.ModAPI.Interfaces.Terminal;
using Scripts.Shared;
using Scripts.Specials.ShipClass;
using ServerMod;
using Slime;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage.Utils;
using VRageMath;

namespace Scripts.Specials
{
    public partial class UpgradableSpecBlock
    {
        /// <summary>
        /// Отвечает за выбранные элементы в списке.
        /// </summary>
        private static List<int> SelectedTest = new List<int>();
        
        /// <summary>
        /// Текущие тестовые настройки
        /// </summary>
        private static List<int> AddedTest = new List<int>();
        
        private void GetUpgradedValues(Dictionary<int, int> dict, out Limits copyStatic, out Limits copyDynamic)
        {
            var upgrades = SpecCoreSession.Instance.Upgrades;
            copyStatic = Info.DefaultStaticValues.Copy();
            copyDynamic = Info.DefaultDynamicValues.Copy();
            foreach (var u in dict)
            {
                if (!upgrades.ContainsKey(u.Key))
                {
                    Log.ChatError($"Won't be find upgrade with id {u.Key}");
                    continue;
                }
                upgrades[u.Key].ApplyUpgrade(u.Value, copyStatic, copyDynamic);
            }

            copyDynamic.Sum(Settings.CustomDynamic);
            copyStatic.Sum(Settings.CustomStatic);
        }

        

        
        
        private static void InitControls<T>() where T : IMyCubeBlock
        {
            var UpgradesListBox = MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlListbox, IMyRadioAntenna>("AvailableUpgrades");
            UpgradesListBox.Visible = (x) => x.GetUpgradableSpecBlock()?.CanPickUpgrades() ?? false;
            UpgradesListBox.VisibleRowsCount = 12;
            UpgradesListBox.ItemSelected = (a, b) =>
            {
                SelectedTest.Clear();
                foreach (var item in b)
                {
                    SelectedTest.Add((int) item.UserData);
                }
            };
            UpgradesListBox.ListContent = UpgradeListContent;
            MyAPIGateway.TerminalControls.AddControl<T>(UpgradesListBox);

            MyAPIGateway.TerminalControls.CreateButton<UpgradableSpecBlock, T>("AddUpgrade", ShipClass.T.Translation(ShipClass.T.Button_AddUpgrade), ShipClass.T.Translation(ShipClass.T.Button_AddUpgrade_Tooltip), (x) =>
            {
                AddedTest.AddRange(SelectedTest);
                SelectedTest.Clear();
                x.block.RefreshCustomInfo();
                UpdateControls(x);
            }, Sugar2.GetUpgradableSpecBlock, enabled:(x) =>
            {
                return AddedTest.Count < x.Info.MaxUpgrades;
            }, visible: (x) => x.CanPickUpgrades());
            
            MyAPIGateway.TerminalControls.CreateButton<UpgradableSpecBlock, T>("RemoveUpgrade", ShipClass.T.Translation(ShipClass.T.Button_RemoveUpgrade), ShipClass.T.Translation(ShipClass.T.Button_RemoveUpgrade_Tooltip), (x) =>
            {
                foreach (var a in SelectedTest) AddedTest.Remove(a);
                SelectedTest.Clear();
                x.block.RefreshCustomInfo();
                UpdateControls(x);
            }, Sugar2.GetUpgradableSpecBlock, visible: (x) => x.CanPickUpgrades());
            
            
            MyAPIGateway.TerminalControls.CreateLabel<UpgradableSpecBlock, T>("Separator22", MyStringId.GetOrCompute("———vVv Double click vVv ——"));

            MyAPIGateway.TerminalControls.CreateButton<UpgradableSpecBlock, T>("ApplyUpgrades" , ShipClass.T.Translation(ShipClass.T.Button_ApplyUpgrade), ShipClass.T.Translation(ShipClass.T.Button_ApplyUpgrade_Tooltip), (x) =>
            {
                var Settings = new UpgradableSpecBlockSettings();
                Settings.Upgrades.AddRange(AddedTest);
                AddedTest.Clear();
                SelectedTest.Clear();
                Sync.SendMessageToServer(x.block.EntityId, Settings, type: APPLY_SELECTED);
                FrameExecutor.addDelayedLogic(30, (xx) => UpdateControls(x)); //1/2 sec

                if (!MyAPIGateway.Session.IsServer) {
                    x.Settings = Settings; //Remove gui lag
                }
            }, Sugar2.GetUpgradableSpecBlock, enabled: (x) =>AddedTest.Count == x.Info.MaxUpgrades, visible: (x) => x.CanPickUpgrades()).DoubleClick();
            
            
            MyAPIGateway.TerminalControls.CreateButton<UpgradableSpecBlock, T>("ApplyRandomUpgrades", ShipClass.T.Translation(ShipClass.T.Button_ApplyRandomUpgrade), ShipClass.T.Translation(ShipClass.T.Button_ApplyRandomUpgrade_Tooltip), (x) =>
            {
                AddedTest.Clear();
                SelectedTest.Clear();
                var Settings = new UpgradableSpecBlockSettings();
                Settings.Upgrades = new List<int>();

                for (int i = 0; i < x.Info.MaxUpgrades; i++)
                {
                    Settings.Upgrades.Add(-1);
                }

                Sync.SendMessageToServer(x.block.EntityId, x.Settings, type:APPLY_RANDOM); //1/2 sec
                FrameExecutor.addDelayedLogic(30, (xx)=> UpdateControls(x));
                    
                if (!MyAPIGateway.Session.IsServer) x.Settings = Settings; //Remove gui lag
            }, Sugar2.GetUpgradableSpecBlock, 
                enabled: (x) => AddedTest.Count == 0 && x.CanPickUpgrades(), 
                visible: (x) => x.Info.ExtraRandomUpgrades > 0 && x.CanPickUpgrades()).DoubleClick();
        }

        private static void UpgradeListContent(IMyTerminalBlock a, List<MyTerminalControlListBoxItem> b, List<MyTerminalControlListBoxItem> c)
        {
            b.Clear();
            Limits copyStatic, copyDynamic, originalStatic, originalDynamic;
            
            var upgrades = AddedTest.SumDuplicates();
            var specBlock = a.GetUpgradableSpecBlock();
            specBlock.GetUpgradedValues(upgrades, out originalStatic, out originalDynamic);
            var s = new StringBuilder();

            var possibleUpgrades = SpecCoreSession.Instance.Upgrades;
            foreach (var u in specBlock.Info.PossibleUpgrades)
            {
                if (!possibleUpgrades.ContainsKey(u))
                {
                    Log.ChatError($"Won't be find upgrade with id {u}");
                    continue;
                }
                
                upgrades = AddedTest.SumDuplicates();
                upgrades.Sum(u, 1);
                specBlock.GetUpgradedValues(upgrades, out copyStatic, out copyDynamic);
                upgrades.Sum(u, -1);
                GetInfo(s, copyStatic, copyDynamic, originalStatic, originalDynamic);
                var tooltip = s.ToString();
                s.Clear();

                var up = possibleUpgrades[u];

                var txt = T.Translation(T.UpgradesList_RowText, up.Name, upgrades.GetOr(u, 0), up.MaxUpgrades);
                
                b.Add(new MyTerminalControlListBoxItem(MyStringId.GetOrCompute(txt), MyStringId.GetOrCompute(tooltip), u));
            }
        }
        
        
        private static void GetInfo(StringBuilder sb, Limits valuesStatic, Limits valuesDynamic, Limits originalStatic, Limits originalDynamic)
        {
            var valuesDynamicCopy = valuesDynamic.Copy();
            var valuesStaticCopy = valuesStatic.Copy();
            
            valuesStaticCopy.RemoveDuplicates(originalStatic);
            valuesDynamicCopy.RemoveDuplicates(originalDynamic);
            
            
            var duplicates = valuesDynamicCopy.GetDuplicates<int, float, Limits>(valuesStaticCopy);
            if (duplicates.Count > 0)
            {
                sb.AppendLine(T.Translation(T.BlockInfo_StaticOrDynamic));
                T.GetLimitsInfo(null, sb, duplicates, null);
                sb.AppendLine();
            }
            
            valuesStaticCopy.RemoveDuplicates(duplicates);
            valuesDynamicCopy.RemoveDuplicates(duplicates);

            if (valuesStaticCopy.Count > 0)
            {
                sb.AppendLine(T.Translation(T.BlockInfo_StaticOnly));
                T.GetLimitsInfo(null, sb, valuesStaticCopy, null);
                sb.AppendLine();
            }
            if (valuesDynamicCopy.Count > 0)
            {
                sb.AppendLine(T.Translation(T.BlockInfo_DynamicOnly));
                T.GetLimitsInfo(null, sb, valuesDynamicCopy, null);
                sb.AppendLine();
            }
        }
        
        protected override void BlockOnAppendingCustomInfo(IMyTerminalBlock arg1, StringBuilder arg2)
        {
            var upgrades = (CanPickUpgrades() ? AddedTest : Settings.Upgrades).SumDuplicates();
           
            Limits copyStatic, copyDynamic;
            GetUpgradedValues(upgrades, out copyStatic, out copyDynamic);

            
            arg2.AppendLine().AppendT(T.BlockInfo_StatusRow, T.GetErrorInfo(activationError));
            
            if (CanPickUpgrades())
            {
                arg2.AppendLine(T.Translation(T.BlockInfo_NoSpecializations));
            }
            
            var limits = IsStatic.Value ? copyStatic : copyDynamic;
            T.GetLimitsInfo(FoundLimits, arg2, limits, TotalLimits);
            
            arg2.AppendT(T.BlockInfo_UpgradesHeader).AppendLine();

            foreach (var u in upgrades)
            {
                Upgrade up;
                if (SpecCoreSession.Instance.Upgrades.TryGetValue(u.Key, out up))
                {
                    arg2.Append(T.Translation(T.BlockInfo_UpgradesRow, up.Name, u.Value)).AppendLine();
                }
                else
                {
                    arg2.Append($"Unknown upgrade {u.Key}").Append(": ").Append(u.Value).AppendLine();
                }
            }
        }
    }
}