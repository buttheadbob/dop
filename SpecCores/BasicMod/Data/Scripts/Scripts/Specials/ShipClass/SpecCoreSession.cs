﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Digi;
using ProtoBuf;
using Sandbox.ModAPI;
using Scripts.Shared;
using Scripts.Specials;
using Scripts.Specials.ShipClass;
using VRage;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.ModAPI;

namespace ServerMod
{
    

    [MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation)]
    public class SpecCoreSession : SessionComponentExternalSettings<SpecCoreSettings>
    {
        public const float MaxValue = 99999999;
        public const int TYPE_PRIORITY = -1; 
        public const int TYPE_BASE = -2; 
        
        public SpecCoreSettings GetDefaultSettings() { return new SpecCoreSettings(); }
        protected override SpecCoreSettings GetDefault() { return GetDefaultSettings(); }
        protected override string GetFileName() { return "SpecCoreSettings"; }

        private readonly List<Ship> addlist = new List<Ship>();
        private readonly List<long> removelist = new List<long>();
        public readonly Dictionary<long, Ship> gridToShip = new Dictionary<long, Ship>();
        public readonly Dictionary<int, LimitPoint> LimitPoints = new Dictionary<int, LimitPoint>();
        public readonly Dictionary<int, Upgrade> Upgrades = new Dictionary<int, Upgrade>();

        

        public static SpecCoreSession Instance = null;
        private static Dictionary<MyDefinitionId, object> IdToBlock = new Dictionary<MyDefinitionId, object>();
        
        public override void Init(MyObjectBuilder_SessionComponent sessionComponent)
        {
            base.Init(sessionComponent);
            Instance = this;
            UpgradableSpecBlock.Init();
            OnSettingsChanged();
        }

        
        

        void OnSettingsChanged()
        {
            if (Settings.LimitConsumers != null)
            {
                foreach (var u in Settings.LimitConsumers)
                {
                    foreach (var uId in u.BlockIds)
                    {
                        var id = MyDefinitionId.Parse("MyObjectBuilder_" + uId);
                        IdToBlock[id] = u;
                    }
                }
            }

            if (Settings.LimitProducers != null)
            {
                foreach (var u in Settings.LimitProducers)
                {
                    var id = MyDefinitionId.Parse("MyObjectBuilder_" + u.Id);
                    IdToBlock[id] = u;
                }
            }

            if (Settings.Points != null)
            {
                foreach (var u in Settings.Points)
                {
                    LimitsChecker.RegisterKey(u.Id);
                    LimitPoints[u.Id] = u;
                }
            }
            
            if (Settings.Points != null)
            {
                foreach (var u in Settings.Upgrades)
                {
                    Upgrades[u.Id] = u;
                }
            }
        }

        public static bool GetLimitedBlock(IMyCubeBlock block, out ILimitedBlock limitedBlock, out ISpecBlock specBlock)
        {
            limitedBlock = null;
            specBlock = null;
            var tBlock = block as IMyTerminalBlock;
            if (tBlock == null) return false;
            
            object blockInfo;
            if (!IdToBlock.TryGetValue(block.SlimBlock.BlockDefinition.Id, out blockInfo)) return false;

            var consumer = blockInfo as LimitConsumerInfo;
            if (consumer != null)
            {
                limitedBlock = Instance.GetLimitedBlock(tBlock, consumer);
                if (limitedBlock != null) Hooks.TriggerOnLimitedBlockCreated(limitedBlock);
                return limitedBlock != null;
            }
            
            var specBlockInfo = blockInfo as LimitProducerInfo;
            if (specBlockInfo != null)
            {
                specBlock = Instance.GetSpecBlock(tBlock, specBlockInfo);
                if (specBlock != null) Hooks.TriggerOnSpecBlockCreated(specBlock);
                return specBlock != null;
            }

            return false;
        }

        public ILimitedBlock GetLimitedBlock(IMyTerminalBlock block, LimitConsumerInfo info)
        {
            if (!string.IsNullOrEmpty(info.CustomLogicId))
            {
                HookedLimiterInfo info2;
                if (!Hooks.HookedConsumerInfos.TryGetValue(info.CustomLogicId, out info2))
                {
                    Log.ChatError($"Was unable to get hooked limited block [{info.CustomLogicId}]");
                    return null;
                }
                return new HookedLimitedBlock(block, info, info2);
            }
            return new LimitedBlock(block, info);
        }
        
        public ISpecBlock GetSpecBlock(IMyTerminalBlock block, LimitProducerInfo info)
        {
            return new UpgradableSpecBlock(block, info);
        }

        public override void LoadData()
        {
            Hooks.Init();
            MyAPIGateway.Entities.OnEntityAdd += OnEntityAdded;
        }

        protected override void UnloadData()
        {
            Hooks.Close();
            MyAPIGateway.Entities.OnEntityAdd -= OnEntityAdded;
        }
        
        private void OnEntityAdded(IMyEntity ent)
        {
            try
            {
                var grid = ent as IMyCubeGrid;
                if (grid != null)
                {
                    if (!gridToShip.ContainsKey(grid.EntityId))
                    {
                        var shipGrid = new Ship(grid);
                        addlist.Add(shipGrid);
                        grid.OnMarkForClose += OnMarkForClose;
                    }
                }
            }
            catch (Exception e)
            {
                Log.ChatError(e);
            }
        }
        
        private void OnMarkForClose(IMyEntity ent)
        {
            if (ent is IMyCubeGrid)
            {
                removelist.Add(ent.EntityId);
                ent.OnMarkForClose -= OnMarkForClose;
            }
        }

        public override void UpdateBeforeSimulation()
        {
            base.UpdateBeforeSimulation();
            foreach (var x in addlist)
            {
                if (x == null)
                {
                    Log.Error("GB: Add: NULL SHIP");
                    continue;
                }

                if (x.grid == null)
                {
                    Log.Error("GB: Add: NULL GRID SHIP");
                    continue;
                }

                if (!gridToShip.ContainsKey(x.grid.EntityId))
                    gridToShip.Add(x.grid.EntityId, x);
                else
                    gridToShip[x.grid.EntityId] = x;
            }

            addlist.Clear();
            
            foreach (var x in removelist) gridToShip.Remove(x);
            removelist.Clear();
            
            foreach (var x in gridToShip) x.Value.BeforeSimulation();
            
            FrameExecutor.Update();
        }
    }
}