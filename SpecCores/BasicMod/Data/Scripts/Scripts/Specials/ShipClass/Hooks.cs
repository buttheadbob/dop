﻿using System;
using System.Collections.Generic;
using System.Linq;
using Digi;
using Sandbox.ModAPI;
using Scripts.Shared;
using ServerMod;
using VRage.Game.ModAPI;
using F = System.Func<object, bool>;
using FF = System.Func<Sandbox.ModAPI.IMyTerminalBlock, object, bool>;
using A = System.Action<object>;
using C = System.Func<Sandbox.ModAPI.IMyTerminalBlock, object>;
using U = System.Collections.Generic.List<int>;
using L = System.Collections.Generic.IDictionary<int, float>;

namespace Scripts.Specials.ShipClass
{
    public class Hooks
    {
        public static Dictionary<string, HookedLimiterInfo> HookedConsumerInfos = new Dictionary<string, HookedLimiterInfo>();
        private static Action<string, C, A, FF, F, F, A> registerCustomLimitConsumer = RegisterCustomLimitConsumer;
        private static Func<IMyCubeGrid, object> getMainSpecCore = GetMainSpecCore;
        private static Func<IMyCubeGrid, IMyTerminalBlock> getMainSpecCoreBlock = GetMainSpecCoreBlock;
        private static Action<object, L, int> getSpecCoreLimits = GetSpecCoreLimits;
        private static Action<object, U> getSpecCoreUpgrades = GetSpecCoreUpgrades;
        private static Action<object, L,L> setSpecCoreUpgrades = SetSpecCoreCustomValues;
        
        private static Func<IMyTerminalBlock, object> getSpecCoreBlock = GetSpecCoreBlock;
        private static Func<object, IMyTerminalBlock> getBlockSpecCore = GetBlockSpecCore;
        private static Func<IMyTerminalBlock, object> getLimitedBlock = GetLimitedBlock;
        private static Func<object, IMyTerminalBlock> getLimitedBlockBlock = GetLimitedBlockBlock;
        
        public static Func<object, U, L, string, string> GetAntennaText = (a, b, c, d) => { return d; };

        public static event Action<object> OnSpecBlockCreated;
        public static event Action<object> OnLimitedBlockCreated;

        public static void TriggerOnSpecBlockCreated(ISpecBlock block)
        {
            OnSpecBlockCreated?.Invoke(block);
        }
        
        public static void TriggerOnLimitedBlockCreated(ILimitedBlock block)
        {
            OnLimitedBlockCreated?.Invoke(block);
        }
        
        public static object GetSpecCoreBlock(IMyTerminalBlock block)
        {
            var grid = block.CubeGrid;
            var ship = grid.GetShip();
            if (ship == null) return null;
            ISpecBlock specBlock;
            ship.SpecBlocks.TryGetValue(block, out specBlock);
            return specBlock;
        }

        public static IMyTerminalBlock GetBlockSpecCore(object block)
        {
            return ((ISpecBlock) block).block;
        }
        
        public static object GetLimitedBlock(IMyTerminalBlock block)
        {
            var grid = block.CubeGrid;
            var ship = grid.GetShip();
            if (ship == null) return null;
            ILimitedBlock limitedBlock;
            ship.LimitedBlocks.TryGetValue(block, out limitedBlock);
            return limitedBlock;
        }

        public static IMyTerminalBlock GetLimitedBlockBlock(object block)
        {
            return ((ILimitedBlock) block).GetBlock();
        }



        public static object GetMainSpecCore(IMyCubeGrid grid)
        {
            Ship ship;
            if (SpecCoreSession.Instance.gridToShip.TryGetValue(grid.EntityId, out ship))
            {
                return ship.CachedCore;
            }

            return null;
        }
        
        public static IMyTerminalBlock GetMainSpecCoreBlock(IMyCubeGrid grid)
        {
            Ship ship;
            if (SpecCoreSession.Instance.gridToShip.TryGetValue(grid.EntityId, out ship))
            {
                return ship.CachedCore.block;
            }

            return null;
        }
        
        public static void GetSpecCoreLimits(object specCore, IDictionary<int, float> dictionary, int mode)
        {
            var specBlock = specCore as UpgradableSpecBlock;
            if (specBlock == null) return;

            switch (mode)
            {
                case 1: dictionary.Sum(specBlock.StaticLimits); break;
                case 2: dictionary.Sum(specBlock.DynamicLimits); break;
                case 3: dictionary.Sum(specBlock.FoundLimits); break;
                case 4: dictionary.Sum(specBlock.TotalLimits); break;
                case 5: dictionary.Sum(specBlock.Settings.CustomStatic); break;
                case 6: dictionary.Sum(specBlock.Settings.CustomDynamic); break;
            }
        }
        
        public static void GetSpecCoreUpgrades(object specCore, List<int> copyTo, int mode)
        {
            var specBlock = specCore as UpgradableSpecBlock;
            if (specBlock == null) return;
            copyTo.AddRange(specBlock.Settings.Upgrades);
        }
        
        public static void GetSpecCoreUpgrades(object specCore, List<int> copyTo)
        {
            var specBlock = specCore as UpgradableSpecBlock;
            if (specBlock == null) return;
            copyTo.AddRange(specBlock.Settings.Upgrades);
        }
        
        public static void SetSpecCoreCustomValues(object specCore, IDictionary<int, float> staticValues, IDictionary<int, float> dynamicValues)
        {
            var specBlock = specCore as UpgradableSpecBlock;
            if (specBlock == null) return;
            specBlock.Settings.CustomStatic.Sum(staticValues);
            specBlock.Settings.CustomDynamic.Sum(dynamicValues);
            specBlock.ApplyUpgrades();
            specBlock.SaveSettings();
        }

        

        /// <summary>
        /// Must be inited in LoadData of MySessionComponentBase
        /// </summary>
        public static void Init()
        {
            ModConnection.Init();
            
            ModConnection.SetValue("MIG.SpecCores.RegisterCustomLimitConsumer", registerCustomLimitConsumer);
            ModConnection.SetValue("MIG.SpecCores.GetMainSpecCore", getMainSpecCore);
            ModConnection.SetValue("MIG.SpecCores.GetMainSpecCoreBlock", getMainSpecCoreBlock);
            ModConnection.SetValue("MIG.SpecCores.GetSpecCoreLimits", getSpecCoreLimits);
            ModConnection.SetValue("MIG.SpecCores.GetSpecCoreUpgrades", getSpecCoreUpgrades);
            ModConnection.SetValue("MIG.SpecCores.SetSpecCoreCustomValues", setSpecCoreUpgrades);
            
            ModConnection.SetValue("MIG.SpecCores.GetSpecCoreBlock", getSpecCoreBlock);
            ModConnection.SetValue("MIG.SpecCores.GetBlockSpecCore", getBlockSpecCore);
            ModConnection.SetValue("MIG.SpecCores.GetLimitedBlock", getLimitedBlock);
            ModConnection.SetValue("MIG.SpecCores.GetLimitedBlockBlock", getLimitedBlockBlock);
            
            ModConnection.Subscribe<Func<object, U, L, string, string>>("MIG.SpecCores.GetAntennaText", (a) =>
            {
                GetAntennaText = a; 
            });
            ModConnection.Subscribe<Action<object>>("MIG.SpecCores.OnSpecBlockCreated", (a) => { OnSpecBlockCreated += a; });
            ModConnection.Subscribe<Action<object>>("MIG.SpecCores.OnLimitedBlockCreated", (a) => { OnLimitedBlockCreated += a; });
        }
        
        public static void Close()
        {
            ModConnection.Close();
        }

        public static void RegisterCustomLimitConsumer(string Id, C OnNewConsumerRegistered, A CanWork, FF CheckConditions, F CanBeDisabled, F IsDrainingPoints, A Disable)
        {
            HookedConsumerInfos[Id] = new HookedLimiterInfo()
            {
                OnNewConsumerRegistered = OnNewConsumerRegistered,
                CanWork = CanWork,
                CheckConditions = CheckConditions,
                CanBeDisabled = CanBeDisabled,
                IsDrainingPoints = IsDrainingPoints,
                Disable = Disable,
            };
        }
    }

    public class HookedLimiterInfo
    {
        public C OnNewConsumerRegistered;
        public A CanWork;
        public FF CheckConditions;
        public F CanBeDisabled;
        public F IsDrainingPoints;
        public A Disable;
    }
}