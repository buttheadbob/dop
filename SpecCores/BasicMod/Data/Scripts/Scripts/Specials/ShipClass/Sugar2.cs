﻿using System;
using System.Collections.Generic;
using Digi;
using Sandbox.Definitions;
using Sandbox.ModAPI;
using ServerMod;
using Slime;
using VRage.Game.Entity;
using VRage.Game.ModAPI;

namespace Scripts.Specials.ShipClass
{
    public static class Sugar2
    {
        public static List<IMyCubeGrid> GetConnectedGrids(this IMyCubeGrid grid, GridConnectionType with, List<IMyCubeGrid> list = null, bool clear = false) {
            if (with == GridConnectionType.None)
            {
                if (list == null) list = new List<IMyCubeGrid>();
                list.Add(grid);
                return list;
            }
            else
            {
                return grid.GetConnectedGrids(with.Get(), list, clear);
            }
        }

        public static void DecreaseMountLevelToFunctionalState(this IMySlimBlock block, IMyInventory outputInventory)
        {
            var def = (block.BlockDefinition as MyCubeBlockDefinition);
            block.DecreaseMountLevelToDesiredRatio(def.CriticalIntegrityRatio, outputInventory);
        }
        
        public static void DecreaseMountLevelToDesiredRatio(this IMySlimBlock block, float desiredIntegrityRatio, IMyInventory outputInventory)
        {
            var def = (block.BlockDefinition as MyCubeBlockDefinition);
            float desiredIntegrity = desiredIntegrityRatio * block.MaxIntegrity;
            float grinderAmount = block.Integrity - desiredIntegrity;
            VRage.MyDebug.Assert(grinderAmount >= 0f);
            if (grinderAmount <= 0f)
                return;

            if (block.FatBlock != null)
                grinderAmount *= block.FatBlock.DisassembleRatio;
            else
                grinderAmount *= def.DisassembleRatio;

            block.DecreaseMountLevel(grinderAmount / def.IntegrityPointsPerSec, outputInventory, useDefaultDeconstructEfficiency: true);
        }

        public static GridLinkTypeEnum Get(this GridConnectionType type)
        {
            switch (type)
            {
                case GridConnectionType.Electrical: return GridLinkTypeEnum.Electrical;
                case GridConnectionType.Logical: return GridLinkTypeEnum.Logical;
                case GridConnectionType.Mechanical: return GridLinkTypeEnum.Mechanical;
                case GridConnectionType.Physical: return GridLinkTypeEnum.Physical;
                case GridConnectionType.NoContactDamage: return GridLinkTypeEnum.NoContactDamage;
                default: return GridLinkTypeEnum.Electrical;
            }
        }
        
        public static ILimitedBlock GetLimitedBlock(this IMyTerminalBlock t)
        {
            var ship = t.CubeGrid.GetShip();
            if (ship == null) return null;
            ILimitedBlock shipSpecBlock;
            if (ship.LimitedBlocks.TryGetValue(t, out shipSpecBlock))
            {
                return shipSpecBlock;
            }
            return null;
        }

        public static Limits GetLimits(this UsedPoints[] usedPoints, bool isLarge, float blockVolume)
        {
            Limits result = new Limits();
            if (usedPoints == null) return result;

            foreach (var u in usedPoints)
            {
                var volume = u.SizeMlt == 0 ? 1 : blockVolume * u.SizeMlt;
                var finalMlt = volume * (isLarge ? u.SmallToLargeMlt : 1f);
                
                var value = u.Amount * finalMlt;
                if (!float.IsNaN(u.MinValue)) value = Math.Max(u.MinValue, value);
                if (u.RoundLimits) value = (float)Math.Round(value);
                result[u.Id] = value;
            }

            return result;
        }
        
        public static Limits GetLimits(this UsedPoints[] usedPoints)
        {
            Limits result = new Limits();
            if (usedPoints == null) return result;

            foreach (var u in usedPoints)
            {
                result[u.Id] = u.Amount;
            }

            return result;
        }
        
        public static ISpecBlock GetSpecBlock(this IMyTerminalBlock t)
        {
            var ship = t.CubeGrid.GetShip();
            if (ship == null) return null;
            ISpecBlock shipSpecBlock;
            if (ship.SpecBlocks.TryGetValue(t, out shipSpecBlock))
            {
                return shipSpecBlock;
            }
            return null;
        }

        public static UpgradableSpecBlock GetUpgradableSpecBlock(this IMyTerminalBlock t)
        {
            return (UpgradableSpecBlock)GetSpecBlock (t);
        }
    }
}