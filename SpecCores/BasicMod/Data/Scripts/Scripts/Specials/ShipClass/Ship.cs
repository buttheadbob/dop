﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using Scripts;
using Scripts.Specials;
using Scripts.Specials.ShipClass;
using Slime;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.ModAPI;

namespace ServerMod
{
    public class Ship
    {
        public IMyCubeGrid grid;
        public HashSet<MyShipController> Cockpits = new HashSet<MyShipController>();
        public Dictionary<IMyCubeBlock, ISpecBlock> SpecBlocks = new Dictionary<IMyCubeBlock, ISpecBlock>();
        public Dictionary<IMyCubeBlock, ILimitedBlock> LimitedBlocks = new Dictionary<IMyCubeBlock, ILimitedBlock>();
        public ISpecBlock CachedCore = null;
        
        public Ship(IMyCubeGrid grid)
        {
            this.grid = grid;
            grid.FindBlocks((x)=>Grid_OnBlockAdded(x));
            grid.OnBlockAdded += Grid_OnBlockAdded;
            grid.OnBlockRemoved += Grid_OnBlockRemoved;
            grid.OnMarkForClose += Grid_OnMarkForClose;
            grid.OnGridSplit += Grid_OnGridSplit;
        }
        
        private void Grid_OnMarkForClose(VRage.ModAPI.IMyEntity obj) {
            grid.OnBlockAdded -= Grid_OnBlockAdded;
            grid.OnBlockRemoved -= Grid_OnBlockRemoved;
            grid.OnMarkForClose -= Grid_OnMarkForClose;
            grid.OnGridSplit -= Grid_OnGridSplit;
        }
        
        private void Grid_OnBlockRemoved(IMySlimBlock obj) {
            onAddedRemoved (obj, false);
        }
        private void Grid_OnBlockAdded(IMySlimBlock obj) {
            onAddedRemoved (obj, true);
        }

        private void Grid_OnGridSplit(IMyCubeGrid arg1, IMyCubeGrid arg2) {
            //LimitsChecker.OnGridSplit(arg1, arg2);
        }

        public void BeforeSimulation()
        {
            RefreshConnections(this);
            if (limitsLastChecked.tick()) {
                LimitsChecker.CheckLimitsInGrid(grid);
            }
        }

        internal void onAddedRemoved(IMySlimBlock obj, bool added) {
            var fat = obj.FatBlock;
            if (fat != null) {
                
                //RegisterUnregisterGameLogic<LimitedBlock, ILimitedBlock>(fat, added, limitedBlocks);
                //RegisterUnregisterGameLogic(fat, added, limitsProducer);
                RegisterUnregisterType(fat, added, Cockpits);

                if (added)
                {
                    ISpecBlock specBlock;
                    ILimitedBlock limitedBlock;
                    if (SpecCoreSession.GetLimitedBlock(fat, out limitedBlock, out specBlock))
                    {
                        if (limitedBlock != null)
                        {
                            LimitedBlocks[fat] = limitedBlock;
                        }
                        if (specBlock != null)
                        {
                            SpecBlocks[fat] = specBlock;
                        }
                    }
                }
                else
                {
                    LimitedBlocks.Remove(fat);
                    SpecBlocks.Remove(fat);
                }
            }
        }
        
        private bool RegisterUnregisterType<T>(IMyCubeBlock fat, bool added, ICollection<T> collection) where T : IMyCubeBlock
        {
            if (fat is T)
            {
                if (added) collection.Add((T)fat);
                else collection.Remove((T)fat);
                return true;
            }

            return false;
        }
        
        public AutoTimer limitsLastChecked = new AutoTimer(SpecCoreSession.Instance.Settings.Timers.CheckLimitsInterval, new Random().Next(SpecCoreSession.Instance.Settings.Timers.CheckLimitsInterval));
        public List<IMyCubeGrid> connectedGrids = new List<IMyCubeGrid>();
        public bool isApplied = false;
        public static void RefreshConnections (Ship from)
        {
            if (!from.isApplied)
            {
                from.isApplied = true;
                from.grid.GetConnectedGrids(SpecCoreSession.Instance.Settings.ConnectionType, from.connectedGrids, true);
                foreach (var x in from.connectedGrids)
                {
                    if (x == from.grid) continue;
                    var sh = x.GetShip();
                    if (sh != null)
                    {
                        sh.connectedGrids.Clear();
                        sh.connectedGrids.AddList(from.connectedGrids);
                        sh.isApplied = true;
                    }
                }
            }
        } 
        
        public void ResetLimitsTimer()
        {
            limitsLastChecked.reset();
        }
    }
}