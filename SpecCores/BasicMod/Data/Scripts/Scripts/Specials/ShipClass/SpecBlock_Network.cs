using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Digi;
using ProtoBuf;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces;
using Sandbox.ModAPI.Interfaces.Terminal;
using Scripts.Shared;
using Scripts.Specials.ShipClass;
using ServerMod;
using Slime;
using SpaceEngineers.Game.ModAPI;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage.Utils;
using VRageMath;

namespace Scripts.Specials {
    public partial class UpgradableSpecBlock : ISpecBlock
    {
        private static HashSet<Type> InitedControls = new HashSet<Type>();
        private static Guid Guid = new Guid("82f6e121-550f-4042-a2b8-185ed2a52abc");

        public static Sync<UpgradableSpecBlockSettings, UpgradableSpecBlock> Sync;
        private const byte APPLY_RANDOM = 125;
        private const byte APPLY_SELECTED = 126;

        public static void Init()
        {
            Sync = new Sync<UpgradableSpecBlockSettings, UpgradableSpecBlock>(17273, (x) => x.Settings, Handler, entityLogicGetter: (id) =>
            {
                var block = id.As<IMyCubeBlock>();
                if (block == null) return null;
                var ship = block.CubeGrid.GetShip();
                if (ship == null) return null;
                ISpecBlock shipSpecBlock;
                if (ship.SpecBlocks.TryGetValue(block, out shipSpecBlock))
                {
                    return (UpgradableSpecBlock)shipSpecBlock;
                }
                return null;
            });
        }
        
        public List<int> TestUpgrades = new List<int>();

        public UpgradableSpecBlockSettings Settings;
        private LimitProducerInfo Info;
        
        public UpgradableSpecBlock(IMyTerminalBlock Entity, LimitProducerInfo info) : base(Entity)
        {
            this.Info = info;
            Settings = new UpgradableSpecBlockSettings();

            if (MyAPIGateway.Session.IsServer)
            {
                if (!Entity.TryGetStorageData(Guid, out Settings, true))
                {
                    Settings = new UpgradableSpecBlockSettings();
                    Settings.Upgrades = new List<int>();
                }
                
                ApplyUpgrades();
            }
            else
            {
                ApplyUpgrades();
                Sync.RequestData(Entity.EntityId);
            }
            
            
            lock (typeof(UpgradableSpecBlock))
            {
                var t = Entity.GetType();
                if (!InitedControls.Contains(t))
                {
                    InitedControls.Add(t);
                    if (Entity is IMyCockpit) InitControls<IMyCockpit>();
                    else if (Entity is IMyRadioAntenna) InitControls<IMyRadioAntenna>();
                    else if (Entity is IMyBeacon) InitControls<IMyBeacon>();
                    else if (Entity is IMyUpgradableBlock) InitControls<IMyUpgradableBlock>();
                    else if (Entity is IMyControlPanel) InitControls<IMyControlPanel>();
                }
            }
        }

        private void ApplyDataFromClient(UpgradableSpecBlockSettings blockSettings,  ulong arg4, byte arg3)
        {
            //Log.ChatError($"[HandleMessage settings: {blockSettings}");
            if (!CanPickUpgrades())
            {
                Log.ChatError($"[{arg4.Identity()?.DisplayName}/{arg4.IdentityId()}] Core already has upgrades: {Settings}: {Settings.Upgrades.Count}/{Info.MaxUpgrades}");
                return;
            }

            if (!block.HasPlayerAccess(arg4.Identity().IdentityId))
            {
                Log.ChatError($"[{arg4.Identity()?.DisplayName}/{arg4.IdentityId()}] You dont have access");
                return;
            }

            if (arg3 == APPLY_RANDOM)
            {
                Settings = new UpgradableSpecBlockSettings();
                
                var r = new Random();
                for (int i = 0; i < Info.MaxUpgrades+Info.ExtraRandomUpgrades; i++)
                {
                    Settings.Upgrades.Add(r.Next(Info.PossibleUpgrades));
                }
            }
            else if (arg3 == APPLY_SELECTED)
            {
                if (blockSettings.Upgrades == null || blockSettings.Upgrades.Count != Info.MaxUpgrades)
                {
                    Log.ChatError($"[{arg4.Identity()?.DisplayName}/{arg4.IdentityId()}] Wrong number of upgrades {Settings.Upgrades?.Count ?? -1 }");
                    return;
                }
                Settings = blockSettings;
                
                //Log.ChatError($"[Applied settings: {Settings}");
            }
        }

        public bool CanPickUpgrades()
        {
            return Settings.Upgrades.Count == 0 && Info.MaxUpgrades > 0;
        }

        private static void UpdateControls(UpgradableSpecBlock x)
        {
            try
            {
                bool orig = x.block.ShowInToolbarConfig;
                x.block.ShowInToolbarConfig = !orig;
                x.block.ShowInToolbarConfig = orig;
            }
            catch (Exception e)
            {
                Log.ChatError("UpdateControls", e);
            }
            
        }

        public void ApplyUpgrades()
        {
            Limits copyStatic, copyDynamic;
            GetUpgradedValues(Settings.Upgrades.SumDuplicates(), out copyStatic, out copyDynamic);
            SetOptions(copyStatic, copyDynamic);
        }
        
        // ================================================================================
        
        public static void Handler (UpgradableSpecBlock block, UpgradableSpecBlockSettings settings, byte type, ulong userSteamId, bool isFromServer)
        {
            if (isFromServer && !MyAPIGateway.Session.IsServer)
            {
                block.Settings = settings;
                block.OnSettingsChanged();
            }
            else
            {
                block.ApplyDataFromClient(settings, userSteamId, type);
                block.NotifyAndSave();
                block.OnSettingsChanged();
            }
        }

        public void OnSettingsChanged()
        {
            ApplyUpgrades();
        }

        protected void NotifyAndSave(byte type=255, bool forceSave = false)
        {
            try
            {
                if (MyAPIGateway.Session.IsServer)
                {
                    Sync.SendMessageToOthers(block.EntityId, Settings, type: type);
                    SaveSettings(forceSave);
                }
                else
                {
                    if (Sync != null)
                    {
                        Sync.SendMessageToServer(block.EntityId, Settings, type: type);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ChatError($"NotifyAndSave {type} Exception {ex} {ex.StackTrace}");
            }
        }
        
        public void SaveSettings(bool forceSave = false)
        {
            if (MyAPIGateway.Session.IsServer)
            {
                block.SetStorageData(Guid, Settings, true);
            }
        }
        
    }
}