﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Digi;
using ProtoBuf;
using Scripts.Base;
using Scripts.Specials;
using Scripts.Specials.ShipClass;
using VRage;
using VRage.Game.ModAPI;

namespace ServerMod
{
    [Flags]
    public enum TypeOfGridGroup
    {
        None = 0, 
        Large = 1, 
        Small = 2, 
        Static = 4
    }
    
    public enum GridConnectionType
    {
        None,
        Logical,
        Physical,
        NoContactDamage,
        Mechanical,
        Electrical
    }

    [ProtoContract]
    public class SpecCoreSettings
    {
        public Timers Timers = new Timers();
        
        [XmlArrayItem("Point")]
        public LimitPoint[] Points;
        
        public GridConnectionType ConnectionType = GridConnectionType.Physical;
        
        public LimitProducerInfo[] LimitProducers;
        
        public LimitConsumerInfo[] LimitConsumers;

        public NoSpecCoreSettings NoSpecCoreSettings;

        [XmlArrayItem("Upgrade")]
        public Upgrade[] Upgrades;

    }

    public class Timers
    {
        [XmlAttribute("CheckLimitsInterval")]
        public int CheckLimitsInterval = 30;
        
        [XmlAttribute("CheckBlocksOnSubGridsInterval")]
        public int CheckBlocksOnSubGridsInterval = 60;
    }
    
    public class NoSpecCoreSettings
    {
        [XmlArrayItem("Point")]
        public UsedPoints[] LargeStatic;
        
        [XmlArrayItem("Point")]
        public UsedPoints[] LargeDynamic;
        
        [XmlArrayItem("Point")]
        public UsedPoints[] SmallStatic;
        
        [XmlArrayItem("Point")]
        public UsedPoints[] SmallDynamic;
        
        [XmlArrayItem("Point")]
        public UsedPoints[] LargeAndSmallStatic;
        
        [XmlArrayItem("Point")]
        public UsedPoints[] LargeAndSmallDynamic;
        
        [XmlIgnore]
        private Dictionary<TypeOfGridGroup, Limits> m_limits = new Dictionary<TypeOfGridGroup, Limits>();
        public Limits GetLimits(TypeOfGridGroup mlt)
        {
            Limits result;
            if (!m_limits.TryGetValue(mlt, out result))
            {
                UsedPoints[] pointsArray = null;
                switch (mlt)
                {
                    case TypeOfGridGroup.Large: 
                        pointsArray = LargeDynamic;
                        break;
                    case TypeOfGridGroup.Static | TypeOfGridGroup.Large: 
                        pointsArray = LargeStatic;
                        break;
                    case TypeOfGridGroup.Small: 
                        pointsArray = SmallDynamic;
                        break;
                    case TypeOfGridGroup.Static | TypeOfGridGroup.Small: 
                        pointsArray = SmallStatic;
                        break;
                    case TypeOfGridGroup.Small | TypeOfGridGroup.Large: 
                        pointsArray = LargeAndSmallDynamic;
                        break;
                    case TypeOfGridGroup.Static | TypeOfGridGroup.Small | TypeOfGridGroup.Large: 
                        pointsArray = LargeAndSmallStatic;
                        break;
                }

                result = pointsArray.GetLimits();
                m_limits[mlt] = result;
            }

            return result;
        }
    }

    [ProtoContract]
    public class LimitConsumerInfo
    {
        [XmlArrayItem("Id")]
        public string[] BlockIds;
        
        [XmlAttribute("CustomLogicId")]
        public string CustomLogicId = null;
        
        [XmlAttribute("DisableBehavior")]
        public DisableBehaviorType DisableBehavior;

        [XmlArrayItem("Point")]
        public UsedPoints[] UsedPoints;
        
        [XmlAttribute("ConsumesLimitIf")]
        public ConsumeBehaviorType ConsumeBehavior;

        [XmlAttribute("CanWorkOnSubGrids")]
        public bool CanWorkOnSubGrids = true;
        
        [XmlAttribute("CanWorkWithoutSpecCore")]
        public bool CanWorkWithoutSpecCore = true;

        [XmlIgnore]
        private Dictionary<Pair<bool, float>, Limits> m_limits = new Dictionary<Pair<bool, float>, Limits>();
        public Limits GetLimits(bool isLarge, float blockVolume)
        {
            Limits result;
            var pair = new Pair<bool, float>(isLarge, blockVolume);
            if (!m_limits.TryGetValue(pair, out result))
            {
                result = UsedPoints.GetLimits(isLarge, blockVolume);
                m_limits[pair] = result;
            }

            return result;
        }
    }
    
    public class LimitProducerInfo
    {
        [XmlAttribute("Id")]
        public string Id;
        
        [XmlAttribute("Priority")]
        public int Priority;
        
        [XmlArrayItem("Point")]
        public UsedPoints[] DefaultStatic;
        
        [XmlArrayItem("Point")]
        public UsedPoints[] DefaultDynamic;
        
        [XmlAttribute("MaxUpgrades")]
        public int MaxUpgrades;
        
        public int[] PossibleUpgrades;
        
        [XmlAttribute("ExtraRandomUpgrades")]
        public int ExtraRandomUpgrades;

        [XmlIgnore]
        private Limits m_defaultStaticValues;
        
        [XmlIgnore]
        private Limits m_defaultDynamicValues;

        [XmlIgnore]
        public Limits DefaultStaticValues
        {
            get
            {
                if (m_defaultStaticValues == null)
                {
                    m_defaultStaticValues = new Limits();
                    foreach (var up in DefaultStatic)
                    {
                        m_defaultStaticValues.Add(up.Id, up.Amount);
                    }
                }

                return m_defaultStaticValues;
            }
        }
        
        [XmlIgnore]
        public Limits DefaultDynamicValues
        {
            get
            {
                if (m_defaultDynamicValues == null)
                {
                    m_defaultDynamicValues = new Limits();
                    foreach (var up in DefaultDynamic)
                    {
                        m_defaultDynamicValues.Add(up.Id, up.Amount);
                    }
                }

                return m_defaultDynamicValues;
            }
        }
    }
    
    [ProtoContract]
    public class LimitPoint
    {
        [XmlAttribute("Id")]
        public int Id;
        
        [XmlAttribute("Name")]
        public string Name;
        
        [XmlAttribute("DisplayPriority")]
        public float DisplayPriority = 1;

        [XmlAttribute("UnitName")]
        public string UnitName;

        [XmlAttribute("Visible")]
        public bool Visible = true;
        
        [XmlAttribute("VisibleIfInfinity")]
        public bool VisibleIfInfinity = true;
        
        
        [XmlAttribute("OverlimitWarning")]
        public string OverlimitWarning;
    }
    
    [ProtoContract]
    public class UsedPoints
    {
        [XmlAttribute("Id")]
        public int Id;
        
        [XmlAttribute("Amount")]
        public float Amount;
        
        [XmlAttribute("MinValue")]
        public float MinValue = float.NaN;
        
        [XmlAttribute("RoundLimits")]
        public bool RoundLimits = true;
        
        [XmlAttribute("SizeMlt")]
        public float SizeMlt = 0;
        
        [XmlAttribute("SmallToLargeMlt")]
        public float SmallToLargeMlt = 1;
    }

    public enum DisableBehaviorType
    {
        OnOff,
        DamageToFunctional,
        Destroy,
        MinedOre
    }
    
    public enum ConsumeBehaviorType
    {
        Placed,
        Functional,
        Enabled,
        Working
    }

    public class AttributeModificator
    {
        [XmlAttribute("PointId")]
        public int PointId;
        
        [XmlArrayItem("Value")]
        public float[] SumStaticValues;
        
        [XmlArrayItem("Value")]
        public float[] MltStaticValues;

        [XmlArrayItem("Value")]
        public float[] SumDynamicValues;
        
        [XmlArrayItem("Value")]
        public float[] MltDynamicValues;
    }
    
    [ProtoContract]
    public class Upgrade
    {
        [XmlAttribute("Id")]
        public int Id;
        
        [XmlAttribute("Name")]
        public string Name;

        [XmlAttribute("MaxUpgrades")]
        public int MaxUpgrades = 999;

        [XmlArrayItem("Modificator")]
        public AttributeModificator[] Modificators;

        public void ApplyUpgrade(int times, Limits copyStatic, Limits copyDynamic)
        {
            if (Modificators != null)
            {
                foreach (var m in Modificators)
                {
                    if (m.SumStaticValues != null && m.SumStaticValues.Length > 0) copyStatic.Sum(m.PointId, m.SumStaticValues.ElementAtOrLast(times-1));
                    if (m.SumDynamicValues != null && m.SumDynamicValues.Length > 0) copyDynamic.Sum(m.PointId, m.SumDynamicValues.ElementAtOrLast(times-1));
                    if (m.MltStaticValues != null && m.MltStaticValues.Length > 0) copyStatic.Mlt(m.PointId, m.MltStaticValues.ElementAtOrLast(times-1));
                    if (m.MltDynamicValues != null && m.MltDynamicValues.Length > 0) copyDynamic.Mlt(m.PointId, m.MltDynamicValues.ElementAtOrLast(times-1));
                }
            }
        }
    }
}