﻿using System;
using System.Collections.Generic;
using System.Text;
using Digi;
using ServerMod;
using VRage;

namespace Scripts.Specials.ShipClass
{
    public static class T
    {
        public static string BlockActivationError_NONE = "SpecCores_BlockActivationError_NONE";
        public static string BlockActivationError_NOT_ENABLED = "SpecCores_BlockActivationError_NOT_ENABLED";
        public static string BlockActivationError_HAS_LARGE_BLOCKS = "SpecCores_BlockActivationError_HAS_LARGE_BLOCKS";
        public static string BlockActivationError_HAS_SMALL_BLOCKS = "SpecCores_BlockActivationError_HAS_SMALL_BLOCKS";
        public static string BlockActivationError_HAS_PCU_LIMIT = "SpecCores_BlockActivationError_HAS_PCU_LIMIT";
        public static string BlockActivationError_HAS_BLOCKS_LIMIT = "SpecCores_BlockActivationError_HAS_BLOCKS_LIMIT";
        public static string BlockActivationError_HAS_OVERLIMITED_GRIDS = "SpecCores_BlockActivationError_HAS_OVERLIMITED_GRIDS";
        public static string BlockActivationError_ANOTHER_CORE = "SpecCores_BlockActivationError_ANOTHER_CORE";
        public static string BlockActivationError_POI = "SpecCores_BlockActivationError_POI";
        
        public static string Infinity = "SpecCores_Infinity";
        
        public static string BlockInfo_StatusRow = "SpecCores_BlockInfo_StatusRow";//STATUS: {0}\r\n==============\r\n
        public static string BlockInfo_WarningSign = "SpecCores_BlockInfo_WarningSign";
        public static string BlockInfo_FullInfoFormat = "SpecCores_BlockInfo_FullInfoFormat";//
        public static string BlockInfo_NoSpecializations = "SpecCores_BlockInfo_NoSpecializations";//"Block hasn't picked specializations"
        public static string BlockInfo_UpgradesHeader = "SpecCores_BlockInfo_UpgradesHeader";//"\r\n==============\r\nUpgrades:"
        public static string BlockInfo_UpgradesRow = "SpecCores_BlockInfo_UpgradesRow";//"{0}: {1}"
        public static string BlockInfo_StaticOnly = "SpecCores_BlockInfo_StaticOnly";//"Static:"
        public static string BlockInfo_DynamicOnly = "SpecCores_BlockInfo_DynamicOnly";//"Dynamic:"
        public static string BlockInfo_StaticOrDynamic = "SpecCores_BlockInfo_StaticOrDynamic";//"Static/Dynamic:"
        public static string BlockInfo_CantWorkOnSubGrids = "SpecCores_BlockInfo_CantWorkOnSubGrids"; //Cant work on sub grids
        public static string BlockInfo_Header = "SpecCores_BlockInfo_Header"; //>>> Limits: <<<
        
        
        public static string UpgradesList_RowText = "SpecCores_UpgradesList_RowText";//$"{0} {1}/{2}"
        
        public static string Button_ApplyRandomUpgrade = "SpecCores_Button_ApplyRandomUpgrade";
        public static string Button_ApplyRandomUpgrade_Tooltip = "SpecCores_Button_ApplyRandomUpgrade_Tooltip";
        public static string Button_ApplyUpgrade = "SpecCores_Button_ApplyUpgrade";
        public static string Button_ApplyUpgrade_Tooltip = "SpecCores_Button_ApplyUpgrade_Tooltip";
        public static string Button_AddUpgrade = "SpecCores_Button_AddUpgrade";
        public static string Button_AddUpgrade_Tooltip = "SpecCores_Button_AddUpgrade_Tooltip";
        public static string Button_RemoveUpgrade = "SpecCores_Button_RemoveUpgrade";
        public static string Button_RemoveUpgrade_Tooltip = "SpecCores_Button_RemoveUpgrade_Tooltip";
/**        
<data name="SpecCores_BlockActivationError_NONE" xml:space="preserve"><value>NONE</value></data>        
<data name="SpecCores_BlockActivationError_NOT_ENABLED" xml:space="preserve"><value>NOT_ENABLED</value></data>        
<data name="SpecCores_BlockActivationError_HAS_LARGE_BLOCKS" xml:space="preserve"><value>HAS_LARGE_BLOCKS</value></data>        
<data name="SpecCores_BlockActivationError_HAS_SMALL_BLOCKS" xml:space="preserve"><value>HAS_SMALL_BLOCKS</value></data>        
<data name="SpecCores_BlockActivationError_HAS_PCU_LIMIT" xml:space="preserve"><value>HAS_PCU_LIMIT</value></data>        
<data name="SpecCores_BlockActivationError_HAS_BLOCKS_LIMIT" xml:space="preserve"><value>HAS_BLOCKS_LIMIT</value></data>        
<data name="SpecCores_BlockActivationError_HAS_OVERLIMITED_GRIDS" xml:space="preserve"><value>HAS_OVERLIMITED_GRIDS</value></data>        
<data name="SpecCores_BlockActivationError_ANOTHER_CORE" xml:space="preserve"><value>ANOTHER_CORE</value></data>     

<data name="SpecCores_BlockInfo_StatusRow" xml:space="preserve"><value>STATUS: {0}
==============
</value></data>        
<data name="SpecCores_BlockInfo_WarningSign" xml:space="preserve"><value>!!!</value></data>        
<data name="SpecCores_BlockInfo_FullInfoFormat" xml:space="preserve"><value>Admin Blocks</value></data>        
<data name="SpecCores_BlockInfo_NoSpecializations" xml:space="preserve"><value>Block hasn't picked specializations</value></data>        
<data name="SpecCores_BlockInfo_UpgradesHeader" xml:space="preserve"><value>
==============
Upgrades:</value></data>        
<data name="SpecCores_BlockInfo_UpgradesRow" xml:space="preserve"><value>{0}: {1}</value></data>        
<data name="SpecCores_BlockInfo_StaticOnly" xml:space="preserve"><value>Static:</value></data>        
<data name="SpecCores_BlockInfo_DynamicOnly" xml:space="preserve"><value>Dynamic:</value></data>        
<data name="SpecCores_BlockInfo_StaticOrDynamic" xml:space="preserve"><value>Static/Dynamic:</value></data>        
<data name="SpecCores_BlockInfo_CantWorkOnSubGrids" xml:space="preserve"><value>   Can't work on sub grids!</value></data>        
<data name="SpecCores_BlockInfo_Header" xml:space="preserve"><value>&gt;&gt;&gt; Limits: &lt;&lt;&lt;</value></data>   
     
<data name="SpecCores_UpgradesList_RowText" xml:space="preserve"><value>{0} {1}/{2}</value></data>  

      
<data name="SpecCores_Button_ApplyRandomUpgrade" xml:space="preserve"><value>Apply Random (DoubleClick)</value></data>        
<data name="SpecCores_Button_ApplyRandomUpgrade_Tooltip" xml:space="preserve"><value>Applies random upgrades to block. Can't be undone</value></data>        
<data name="SpecCores_Button_ApplyUpgrade" xml:space="preserve"><value>Apply (DoubleClick)</value></data>        
<data name="SpecCores_Button_ApplyUpgrade_Tooltip" xml:space="preserve"><value>Applies upgrades to block. Can't be undone</value></data>        
<data name="SpecCores_Button_AddUpgrade" xml:space="preserve"><value>Add Upgrade</value></data>        
<data name="SpecCores_Button_AddUpgrade_Tooltip" xml:space="preserve"><value>Add upgrade to block. It is not applied till you double click `Apply (DoubleClick)`</value></data>        
<data name="SpecCores_Button_RemoveUpgrade" xml:space="preserve"><value>Remove upgrade</value></data>        
<data name="SpecCores_Button_RemoveUpgrade_Tooltip" xml:space="preserve"><value>Remove upgrade from block</value></data>

<data name="SpecCores_Points_SMALLGRID" xml:space="preserve"><value>SMALLGRID</value></data>
<data name="SpecCores_Points_LARGEGRID" xml:space="preserve"><value>LARGEGRID</value></data>
<data name="SpecCores_Points_PCU" xml:space="preserve"><value>PCU</value></data>
<data name="SpecCores_Points_BLOCKS" xml:space="preserve"><value>BLOCKS</value></data>
<data name="SpecCores_Points_PRIORITY" xml:space="preserve"><value>PRIORITY</value></data>
<data name="SpecCores_Points_MAX_GRIDS" xml:space="preserve"><value>MAX_GRIDS</value></data>
<data name="SpecCores_Points_BROADCASTRADIUS" xml:space="preserve"><value>BROADCASTRADIUS</value></data>
*/

        public static StringBuilder AppendT(this StringBuilder sb, string key, params object[] data)
        {
            sb.Append(Translation(key, data));
            return sb;
        }

        public static StringBuilder AppendT(this StringBuilder sb, string key)
        {
            sb.Append(Translation(key));
            return sb;
        }
        
        public static string Translation(string key)
        {
            return MyTexts.GetString(key);
        }

        public static string Translation(string key, params object[] data)
        {
            var s = MyTexts.GetString(key);
            return string.Format(s, data);
        }
        
        public static void GetLimitsInfo(Limits foundLimits, StringBuilder sb, Limits maxAvailable, Limits total)
        {
            var allKeys = new Limits(maxAvailable);
            if (foundLimits != null)
            {
                allKeys.Sum(foundLimits);
            }

            var keys = new List<int>(allKeys.Keys);
            foreach (var kv in SpecCoreSession.Instance.LimitPoints)
            {
                if (!kv.Value.Visible) keys.Remove(kv.Key);
            }
            
            keys.Sort((a, b) =>
            {
                var points = SpecCoreSession.Instance.LimitPoints;

                var a1 = points[a].DisplayPriority;
                var a2 = points[b].DisplayPriority;
                return a2 > a1 ? 1 : a2 < a1 ? -1 : 0;
            });
            
            if (foundLimits != null)
            {
                foreach (var x in keys) {
                    var am1 = foundLimits.GetValueOrDefault(x, 0);
                    var am2 = maxAvailable.GetValueOrDefault(x, 0);
                    var am3 = total.GetValueOrDefault(x, 0);
                    if (am1 == 0 && am2 == 0 && am3 == 0) continue;

                    if (am2 > SpecCoreSession.MaxValue && !SpecCoreSession.Instance.LimitPoints[x].VisibleIfInfinity)
                    {
                        continue;
                    }

                    if (x >= 0)
                    {
                        var warningSign = am3 > am2 ? Translation(T.BlockInfo_WarningSign) : "";
                        
                        sb.Append($"{warningSign}{GetTypeDescription(x)}: {FormatNumber(am1)}/{FormatNumber(am2)} ({FormatNumber(am3)}) {TypeEnd(x)}\r\n");
                        if (am3 > am2)
                        {
                            sb.Append(GetOverlimitDescription(x)+"\r\n");
                        }
                    }
                    else
                    {
                        sb.Append($"{GetTypeDescription(x)}: {FormatNumber(am2)} {TypeEnd(x)}\r\n");
                    }
                }
            }
            else
            {
                foreach (var x in keys) {
                    var am2 = maxAvailable.GetValueOrDefault(x, 0);
                    if (am2 == 0) continue;

                    if (am2 > SpecCoreSession.MaxValue && !SpecCoreSession.Instance.LimitPoints[x].VisibleIfInfinity)
                    {
                        continue;
                    }
                    sb.Append($"{GetTypeDescription(x)}: {FormatNumber(am2)}{TypeEnd(x)}\r\n");
                }
            }
        }


        
        
        public static String GetErrorInfo(SpecBlockActivationError activationError) {
            switch (activationError) {
                case SpecBlockActivationError.NONE: return "Activated";
                case SpecBlockActivationError.NOT_ENABLED: return "Disabled, Block is not powered";
                case SpecBlockActivationError.ANOTHER_CORE: return "Disabled, Another spec-block used";
                case SpecBlockActivationError.HAS_PCU_LIMIT: return "Disabled, PCU limit";
                case SpecBlockActivationError.HAS_LARGE_BLOCKS: return "Disabled, Ship has large subparts";
                case SpecBlockActivationError.HAS_SMALL_BLOCKS: return "Disabled, Ship has small subparts";
                case SpecBlockActivationError.HAS_BLOCKS_LIMIT: return "Disabled, Blocks limit";
                case SpecBlockActivationError.POI: return "Disabled, POI Near";
                case SpecBlockActivationError.HAS_OVERLIMITED_GRIDS: return "Disabled, grids are over limit";
                default: return "Unknown status";
            }
        }
        
        public static string GetTypeDescription(int type)
        {
            var points = SpecCoreSession.Instance.LimitPoints;
            LimitPoint lp;
            if (points.TryGetValue(type, out lp))
            {
                return Translation(lp.Name);
            }
            
            return "UNKNOWN ("+type+")";
        }
        
        public static string GetOverlimitDescription(int type)
        {
            var points = SpecCoreSession.Instance.LimitPoints;
            LimitPoint lp;
            if (points.TryGetValue(type, out lp))
            {
                return Translation(lp.OverlimitWarning);
            }
            
            return "";
        }
        
        private static string TypeEnd(int key)
        {
            var points = SpecCoreSession.Instance.LimitPoints;
            LimitPoint lp;
            if (points.TryGetValue(key, out lp))
            {
                return Translation(lp.UnitName);
            }
            return "";
        }
        
        private static string FormatNumber(float value)
        {
            if (value >= SpecCoreSession.MaxValue)
            {
                return Translation(T.Infinity);
            }
            return $"{value:G29}";
        }
    }
}