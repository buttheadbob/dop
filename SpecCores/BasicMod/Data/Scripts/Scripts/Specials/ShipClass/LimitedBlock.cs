﻿using System;
using System.Collections.Generic;
using System.Text;
using Digi;
using ProtoBuf;
using Sandbox.Definitions;
using Sandbox.ModAPI;
using Scripts;
using Scripts.Shared;
using Scripts.Specials;
using Scripts.Specials.ShipClass;
using Slime;
using VRage.Game;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;

namespace ServerMod
{
    public enum SpecBlockActivationError {
        NONE,
        NOT_ENABLED,
        HAS_LARGE_BLOCKS,
        HAS_SMALL_BLOCKS,
        HAS_PCU_LIMIT,
        HAS_BLOCKS_LIMIT,
        HAS_OVERLIMITED_GRIDS,
        ANOTHER_CORE,
        POI
    }

    public interface ILimitedBlock
    {
        bool IsDrainingPoints();
        void Disable();
        void CanWork();
        bool CanBeDisabled();
        long EntityId();
        Limits GetLimits();
        bool CheckConditions(ISpecBlock specblock);
        IMyTerminalBlock GetBlock();
    }

    
    /*[ProtoContract]
    public class LimitedBlockSettings
    {
        [ProtoMember(1)] 
        public bool AutoEnable;
    }

    public class LimitedBlockNetworking
    {
        private static Guid Guid = new Guid("82f6e121-550f-4042-a2b8-185ed2a52abd");
        public static Sync<LimitedBlockSettings, LimitedBlock> Sync;
        public LimitedBlockSettings Settings;
        
        public static void Init()
        {
            Sync = new Sync<LimitedBlockSettings, LimitedBlock>(17273, (x) => x.Component.Settings, Handler, entityLogicGetter: (id) =>
            {
                var block = id.As<IMyCubeBlock>();
                if (block == null) return null;
                var ship = block.CubeGrid.GetShip();
                if (ship == null) return null;
                ILimitedBlock shipSpecBlock;
                if (ship.LimitedBlocks.TryGetValue(block, out shipSpecBlock))
                {
                    return (LimitedBlock)shipSpecBlock;
                }
                return null;
            });
        }
        
        public static void Handler (LimitedBlock block, LimitedBlockSettings settings, byte type, ulong userSteamId, bool isFromServer)
        {
            if (isFromServer && !MyAPIGateway.Session.IsServer)
            {
                block.Component.Settings = settings;
                block.Component.OnSettingsChanged();
            }
            else
            {
                block.Component.ApplyDataFromClient(settings, userSteamId, type);
                block.Component.NotifyAndSave();
                block.Component.OnSettingsChanged();
            }
        }
        
        public void OnSettingsChanged()
        {
            ApplyUpgrades();
        }

        protected void NotifyAndSave(byte type=255, bool forceSave = false)
        {
            try
            {
                if (MyAPIGateway.Session.IsServer)
                {
                    Sync.SendMessageToOthers(block.EntityId, Settings, type: type);
                    SaveSettings(forceSave);
                }
                else
                {
                    if (Sync != null)
                    {
                        Sync.SendMessageToServer(block.EntityId, Settings, type: type);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ChatError($"NotifyAndSave {type} Exception {ex} {ex.StackTrace}");
            }
        }
        
        protected void SaveSettings(bool forceSave = false)
        {
            if (MyAPIGateway.Session.IsServer)
            {
                block.SetStorageData(Guid, Settings);
            }
        }
        
        private void ApplyDataFromClient(UpgradableSpecBlockSettings blockSettings,  ulong arg4, byte arg3)
        {
            arg4.
        }
    }*/
    
    public class LimitedBlock : ILimitedBlock {
        public IMyTerminalBlock block;
        public IMyFunctionalBlock fblock;
        public IMyShipDrill drill;
        
        protected Limits limits;
        bool IsCurrentlyOnMainGrid = false;
        private LimitConsumerInfo info;


        //public LimitedBlockNetworking Component;

        public LimitedBlock(IMyTerminalBlock Entity, LimitConsumerInfo info)
        {
            block = Entity;
            fblock = Entity as IMyFunctionalBlock;
            var def = Entity.SlimBlock.BlockDefinition as MyCubeBlockDefinition;

            this.info = info;
            this.limits = info.GetLimits(Entity.SlimBlock.CubeGrid.GridSizeEnum == MyCubeSize.Large, def.Size.Volume());

            if (!MyAPIGateway.Session.isTorchServer()) {
                block.AppendingCustomInfo += BlockOnAppendingCustomInfo;
                block.OnMarkForClose += BlockOnOnMarkForClose;
                FrameExecutor.addFrameLogic(new AutoTimer(60, 60), block, (x) => block.RefreshCustomInfo());
            }

            if (!info.CanWorkOnSubGrids)
            {
                FrameExecutor.addFrameLogic(new AutoTimer(SpecCoreSession.Instance.Settings.Timers.CheckBlocksOnSubGridsInterval, SpecCoreSession.Instance.Settings.Timers.CheckBlocksOnSubGridsInterval), block, Tick);
            }
        }
        
        
        public void Tick(long frame)
        {
            IsCurrentlyOnMainGrid = IsOnMainGrid();
            if (!IsCurrentlyOnMainGrid)
            {
                //Log.ChatError("Tick disable");
                Disable();
            }
        }
        private bool IsOnMainGrid()
        {
            var ship = block.CubeGrid.GetShip();
            if (ship != null)
            {
                foreach (var x in ship.Cockpits)
                {
                    if (x.IsMainControlledCockpit()) return true;
                }
            }

            return false;
        }
        
        private void BlockOnOnMarkForClose(IMyEntity obj) {
            block.OnMarkForClose -= BlockOnOnMarkForClose;
            if (!MyAPIGateway.Session.isTorchServer()) {
                block.AppendingCustomInfo -= BlockOnAppendingCustomInfo;
            }
        }
        
        protected virtual void BlockOnAppendingCustomInfo(IMyTerminalBlock arg1, StringBuilder arg2) { 
            arg2.AppendLine().AppendT(T.BlockInfo_StaticOnly).AppendLine();
            T.GetLimitsInfo(null, arg2, limits, null);
            if (!info.CanWorkOnSubGrids)
            {
                arg2.AppendLine().Append(T.Translation(T.BlockInfo_CantWorkOnSubGrids)).AppendLine();
            }
        }

        public void CanWork() { }

        public bool CheckConditions(ISpecBlock specblock)
        {
            return true;//info.CanWorkWithoutSpecCore || specblock != null;
        }

        public IMyTerminalBlock GetBlock()
        {
            return block;
        }

        public long EntityId() { return block.EntityId; }
        public Limits GetLimits() { return limits; }
        

        public virtual bool CanBeDisabled() {
            switch (info.ConsumeBehavior)
            {
                case ConsumeBehaviorType.Functional:
                case ConsumeBehaviorType.Placed:
                    return false;
                default:
                    return true;
            }
        }
        
        public virtual bool CanBeActivated() {  return (!info.CanWorkOnSubGrids || IsCurrentlyOnMainGrid) && IsDrainingPoints(); }
        

        public bool IsDrainingPoints()
        {
            switch (info.ConsumeBehavior)
            {
                case ConsumeBehaviorType.Placed:
                    return true;
                case ConsumeBehaviorType.Enabled:
                    return fblock.Enabled;
                case ConsumeBehaviorType.Functional:
                    return block.IsFunctional;
                case ConsumeBehaviorType.Working:
                    return fblock.IsWorking && fblock.Enabled;
                default:
                    return true;
            }
        }

        public void Disable()
        {
            switch (info.DisableBehavior)
            {
                case DisableBehaviorType.OnOff:
                    fblock.Enabled = false;
                    break;
                case DisableBehaviorType.Destroy:
                    block.CubeGrid.RazeBlock(block.Position);
                    break;
                case DisableBehaviorType.DamageToFunctional:
                    block.SlimBlock.DecreaseMountLevelToFunctionalState(null);
                    break;
            }
        }
    }
    
     public class HookedLimitedBlock : ILimitedBlock {
        public IMyTerminalBlock block;

        protected Limits limits;
        bool IsCurrentlyOnMainGrid = false;
        private LimitConsumerInfo info;


        //public LimitedBlockNetworking Component;
        private HookedLimiterInfo hooks;
        private object logic;
        public HookedLimitedBlock(IMyTerminalBlock Entity, LimitConsumerInfo info, HookedLimiterInfo hooks)
        {
            block = Entity;
            this.hooks = hooks;
            var def = Entity.SlimBlock.BlockDefinition as MyCubeBlockDefinition;
            this.info = info;
            this.limits = info.GetLimits(Entity.SlimBlock.CubeGrid.GridSizeEnum == MyCubeSize.Large, def.Size.Volume());

            if (!MyAPIGateway.Session.isTorchServer()) {
                block.AppendingCustomInfo += BlockOnAppendingCustomInfo;
                block.OnMarkForClose += BlockOnOnMarkForClose;
                FrameExecutor.addFrameLogic(new AutoTimer(60, 60), block, (x) => block.RefreshCustomInfo());
            }

            if (!info.CanWorkOnSubGrids)
            {
                FrameExecutor.addFrameLogic(new AutoTimer(SpecCoreSession.Instance.Settings.Timers.CheckBlocksOnSubGridsInterval, SpecCoreSession.Instance.Settings.Timers.CheckBlocksOnSubGridsInterval), block, Tick);
            }
            
            logic = hooks.OnNewConsumerRegistered(block);
        }
        
        
        public void Tick(long frame)
        {
            IsCurrentlyOnMainGrid = IsOnMainGrid();
            if (!IsCurrentlyOnMainGrid)
            {
                Disable();
            }
        }
        private bool IsOnMainGrid()
        {
            var ship = block.CubeGrid.GetShip();
            if (ship != null)
            {
                foreach (var x in ship.Cockpits)
                {
                    if (x.IsMainControlledCockpit()) return true;
                }
            }

            return false;
        }
        
        private void BlockOnOnMarkForClose(IMyEntity obj) {
            block.OnMarkForClose -= BlockOnOnMarkForClose;
            if (!MyAPIGateway.Session.isTorchServer()) {
                block.AppendingCustomInfo -= BlockOnAppendingCustomInfo;
            }
        }
        
        protected virtual void BlockOnAppendingCustomInfo(IMyTerminalBlock arg1, StringBuilder arg2) { 
            arg2.AppendLine().AppendT(T.BlockInfo_Header).AppendLine();
            T.GetLimitsInfo(null, arg2, limits, null);
            if (!info.CanWorkOnSubGrids)
            {
                arg2.AppendLine().Append(T.Translation(T.BlockInfo_CantWorkOnSubGrids)).AppendLine();
            }
        }

        public void CanWork()
        {
            hooks.CanWork(logic);
        }

        public bool CheckConditions(ISpecBlock specblock)
        {
            return (info.CanWorkWithoutSpecCore || specblock != null) && hooks.CheckConditions(specblock?.block, logic);
        }

        public IMyTerminalBlock GetBlock()
        {
            return block;
        }

        public long EntityId() { return block.EntityId; }
        public Limits GetLimits() { return limits; }
        

        public virtual bool CanBeDisabled() { return hooks.CanBeDisabled(logic); }
        

        public bool IsDrainingPoints()
        {
            return hooks.IsDrainingPoints(logic);
        }

        public void Disable()
        {
            hooks.Disable(logic);
        }
    }
}