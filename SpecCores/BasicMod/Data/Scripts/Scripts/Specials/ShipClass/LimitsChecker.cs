using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerMod;
using Slime;
using VRage.Game.ModAPI;
using Digi;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using Scripts.Specials.ShipClass;

namespace Scripts.Specials
{
    public class LimitsChecker {
        public const int TYPE_PRIORITY = -1;
        
        public const int TYPE_MAX_SMALLGRIDS = -5;
        public const int TYPE_MAX_LARGEGRIDS = -6;
        public const int TYPE_MAX_GRIDS = -7;
        public const int TYPE_PCU = -8;
        public const int TYPE_BLOCKS = -9;
        
        public const int TYPE_BROADCASTRADIUS = -50;
        
        
        public const float MaxValue = 999999999;
        
        private static HashSet<int> allPositiveKeys = new HashSet<int>();
        private static Limits buffer = From();
        private static Limits totalPoints = From();
        private static Limits bufferTotal = From();
        private static Limits EmptyLimits = From();
        
        private static List<ISpecBlock> bufferProducers = new List<ISpecBlock>(10);
        private static List<ILimitedBlock> bufferConsumers = new List<ILimitedBlock>(100);
        private static List<IMyCubeGrid> gridBuffer = new List<IMyCubeGrid>();
        
        public static void RegisterKey(int key)
        {
            buffer[key] = 0;
            totalPoints[key] = 0;
            bufferTotal[key] = 0;
            EmptyLimits[key] = 0;
            allPositiveKeys.Add(key);
        }

        private static void ClearClientLimitInfo(List<IMyCubeGrid> grids, ISpecBlock producer)
        {
            foreach (var g in grids)
            {
                var ship = g.GetShip();
                if (ship == null) continue;
                foreach (var x in ship.SpecBlocks.Values)
                {
                    x.FoundLimits.Clear();
                    x.FoundLimits.Plus(bufferTotal);
                    x.TotalLimits.Clear();
                    x.TotalLimits.Plus(totalPoints);
                }
            }

            if (producer != null)
            {
                foreach (var x in bufferProducers)
                {
                    x.activationError = SpecBlockActivationError.ANOTHER_CORE;
                }

                producer.activationError = SpecBlockActivationError.NONE;
            }
        }

        private static GridGroupInfo GridGroupInfo = new GridGroupInfo();
        
        public static void CheckLimitsInGrid(IMyCubeGrid grid) {
            try
            {
                if (grid.isFrozen()) return;
                var grids = grid.GetConnectedGrids(SpecCoreSession.Instance.Settings.ConnectionType, gridBuffer, true);
                if (grids == null) return;
                
                

                foreach (var x in allPositiveKeys)
                {
                    buffer[x] = 0;
                    bufferTotal[x] = 0;
                }
                bufferProducers.Clear();
                bufferConsumers.Clear();
                totalPoints.Clear();

                GridGroupInfo.Calculate(grids);

                foreach (var g in grids)
                {
                    if ((grid as MyCubeGrid).Projector != null)
                    {
                        continue;
                    }

                    var ship = g.GetShip();
                    if (ship == null) continue;
                    ship.ResetLimitsTimer(); //Resetting checks    

                    //Log.ChatError($"CheckLimitsInGrid {g} {ship.LimitedBlocks.Count} | {ship.SpecBlocks.Count}");
                    
                    foreach (var x in ship.LimitedBlocks.Values)
                    {
                        
                        if (!x.IsDrainingPoints()) continue;
                        bufferTotal.Plus(x.GetLimits());
                        bufferConsumers.Add(x);
                        buffer.Plus(x.GetLimits());
                    }

                    if (!MyAPIGateway.Session.isTorchServer())
                    {
                        foreach (var x in ship.LimitedBlocks.Values)
                        {
                            totalPoints.Plus(x.GetLimits());
                        }
                    }

                    foreach (var x in ship.SpecBlocks.Values)
                    {
                        if (x.CanBeApplied(grids, GridGroupInfo))
                        {
                            bufferProducers.Add(x);
                        }
                    }
                }
                
                

                Dictionary<int,float> maxLimits = EmptyLimits;
                ISpecBlock producer = null;
                if (bufferProducers.Count > 0)
                {
                    bufferProducers.Sort((a, b) => (b.Priority.CompareTo(a.Priority)));
                    producer = bufferProducers[0];
                    maxLimits = producer.GetLimits();
                }
                else
                {
                    maxLimits = SpecCoreSession.Instance.Settings.NoSpecCoreSettings.GetLimits(GridGroupInfo.GetTypeOfGridGroup(grids));
                }
                
                //Log.ChatError($"CheckLimitsInGrid {bufferProducers.Count} | {grids.Count} Limits: {producer?.GetLimits().Print((k,v)=>$"{k}:{v}", separator:" ") ?? "null"}");

                if (!MyAPIGateway.Session.isTorchServer())
                {
                    ClearClientLimitInfo(grids, producer);
                }

                if (!MyAPIGateway.Session.IsServer) return;

                
                foreach (var x in bufferConsumers)
                {
                    if (!x.CheckConditions(producer))
                    {
                        Log.ChatError("Conditions disable");
                        x.Disable();
                    }
                }

                if (!buffer.IsOneKeyMoreThan(maxLimits)) //Exceeding limits
                {
                    foreach (var consumer in bufferConsumers)
                    {
                        consumer.CanWork();
                    }

                    UpdateCachedCore(grids, producer);
                    return;
                }


                //Log.ChatError("Exceeding limits:" + buffer.Print(" | ", (k,v)=> v != 0));
                foreach (var x in bufferConsumers)
                {
                    if (!x.CanBeDisabled())
                    {
                        Log.ChatError("Can't be disabled");
                        continue;
                    }

                    if (buffer.IsOneKeyMoreThan(maxLimits))
                    {
                        //Log.ChatError("Exceeding disable");
                        x.Disable();
                        buffer.Minus(x.GetLimits());
                        //Log.ChatError("Limits:" + buffer.Print(" | ", (k,v)=> v != 0) + " -> " + maxLimits.Print(" | ", (k,v)=> v != 0));
                    }
                }
                //Log.ChatError("After limits:" + buffer.Print(" | ", (k,v)=> v != 0));

                //Still violating => disable all blocks;
                if (buffer.IsOneKeyMoreThan(maxLimits))
                {
                    foreach (var x in bufferConsumers)
                    {
                        Log.ChatError("Punish all disable");
                        x.Disable();
                    }
                    
                    UpdateCachedCore(grids, null);
                }
                else
                {
                    foreach (var x in bufferConsumers)
                    {
                        if (x.IsDrainingPoints())
                        {
                            x.CanWork();
                        }
                    }

                    UpdateCachedCore(grids, producer);
                }
            }
            catch (Exception e)
            {
                Log.ChatError("Checker:" + e.ToString());
            }
        }

        private static void UpdateCachedCore(List<IMyCubeGrid> grids, ISpecBlock specBlock)
        {
            foreach (var g in grids)
            {
                var ship = g.GetShip();
                if (ship != null)
                {
                    ship.CachedCore = specBlock;
                }
            }
        }

        public static void OnGridSplit(IMyCubeGrid arg1, IMyCubeGrid arg2) {
            if (MyAPIGateway.Session.IsServer) { return; }
            if (!arg1.InScene) { return; }
            CheckLimitsInGrid(arg1);
            if (!arg2.InScene) { return; }
            CheckLimitsInGrid(arg2);
        }
        
        public static ISpecBlock GetMainCore (Ship sh)
        {
            ISpecBlock best = null;
            GridGroupInfo.Calculate(sh.connectedGrids);
            foreach (var g in sh.connectedGrids)
            {
                var ship = g.GetShip();
                if (ship == null) continue;

                foreach (var x in ship.SpecBlocks.Values)
                {
                    if ((x.Priority > (best?.Priority ?? -9999)) && x.CanBeApplied(sh.connectedGrids, GridGroupInfo))
                    {
                        best = x;
                    }
                }
            }

            return best;
        }
        
        
        private static int FloatToInt(float v)
        {
            return (int)v;
        }
        
        public static Limits From(params float[] data) {
            var map = new Limits();
            foreach (var x in allPositiveKeys) {
                map.Add(x, 0);
            }

            for (int x = 0; x < data.Length - 1; x += 2)
            {
                var key = (int)data[x];
                map[key] = data[x + 1];
            }

            return map;
        }
        
        public static Limits From(Limits baseValues, params float[] data) {
            var map = new Limits();
            foreach (var x in allPositiveKeys) {
                map.Add(x, 0);
            }
            
            foreach (var x in baseValues)
            {
                map[x.Key] = x.Value;
            }

            for (int x = 0; x < data.Length - 1; x += 2)
            {
                map[FloatToInt(data[x])] = data[x + 1];
            }

            return map;
        }
    }
}