﻿using System;
using System.Collections.Generic;
using System.Text;
using Digi;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces.Terminal;
using Scripts.Shared;
using ServerMod;
using Slime;
using VRage.Game;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;

namespace Scripts.Specials.ShipClass
{
    public class GridGroupInfo
    {
        public int totalPCU = 0;
        public int totalBlocks = 0;
        public int LargeGrids = 0;
        public int SmallGrids = 0;
        public void Calculate(List<IMyCubeGrid> grids)
        {
            totalBlocks = 0;
            totalPCU = 0;
            foreach (var x in grids)
            {
                LargeGrids |= x.GridSizeEnum == MyCubeSize.Small ? 1 : 0;
                SmallGrids |= x.GridSizeEnum == MyCubeSize.Large ? 1 : 0;
                totalPCU += (x as MyCubeGrid).BlocksPCU;
                totalBlocks += (x as MyCubeGrid).BlocksCount;
            }
        }

        public TypeOfGridGroup GetTypeOfGridGroup(List<IMyCubeGrid> grids)
        {
            TypeOfGridGroup result = TypeOfGridGroup.None;
            foreach (var g in grids)
            {
                if (g.IsStatic)
                {
                    result |= TypeOfGridGroup.Static;
                    break;
                }
            }
            result |= LargeGrids > 0 ? TypeOfGridGroup.Large : TypeOfGridGroup.None;
            result |= SmallGrids > 0 ? TypeOfGridGroup.Small : TypeOfGridGroup.None;
            return result;
        }
    }
    
    public abstract class ISpecBlock {
        public IMyFunctionalBlock block;
        
        
        public Limits StaticLimits;
        public Limits DynamicLimits;
        public Limits FoundLimits = new Limits();
        public Limits TotalLimits = new Limits();
        public SpecBlockActivationError activationError = SpecBlockActivationError.NONE;
        private FrameExecutor.ActionWrapper RefreshCustomInfo;
        public float Priority => GetLimits()[SpecCoreSession.TYPE_PRIORITY];
        public bool IsBase => GetLimits()[SpecCoreSession.TYPE_BASE] > 0;
        public Limits GetLimits() { return IsStatic.Value ? StaticLimits : DynamicLimits; }

        private RadioAntennaExtension m_radioAntennaExtension;

        protected DecayingByFramesLazy<bool> IsStatic = new DecayingByFramesLazy<bool>(6);
        public ISpecBlock(IMyTerminalBlock Entity)
        {
            IsStatic.SetGetter(GetIsStatic);

            block = (Entity as IMyFunctionalBlock);

            if (block is IMyRadioAntenna)
            {
                m_radioAntennaExtension = new RadioAntennaExtension((IMyRadioAntenna)block, this);
            }
            
            if (!MyAPIGateway.Session.isTorchServer()) {
                FrameExecutor.addFrameLogic(new AutoTimer(100, 100), block, (x) => block.RefreshCustomInfo());
                block.AppendingCustomInfo += BlockOnAppendingCustomInfo;
                block.OnMarkForClose += BlockOnOnMarkForClose;
            }
        }
        
        
        private bool GetIsStatic(bool oldValue)
        {
            var sh = block.CubeGrid.GetShip();
            if (sh == null) return block.CubeGrid.IsStatic;
            
            foreach (var g in sh.connectedGrids)
            {
                if (g.IsStatic) return true;
            }
            return false;
        }
        
        protected void SetOptions(Limits staticLimits, Limits dynamicLimits = null) {
            this.StaticLimits = staticLimits;
            this.DynamicLimits = dynamicLimits;
        }

        

        public virtual bool CanBeApplied(List<IMyCubeGrid> grids, GridGroupInfo info) {
            if (!block.IsWorking) {
                activationError = SpecBlockActivationError.NOT_ENABLED;
                //Log.ChatError("Not enabled");
                return false;
            }
            
            var limits = GetLimits();
            if (info.LargeGrids > limits[LimitsChecker.TYPE_MAX_LARGEGRIDS]) { activationError = SpecBlockActivationError.HAS_LARGE_BLOCKS; return false; }
            if (info.SmallGrids > limits[LimitsChecker.TYPE_MAX_SMALLGRIDS]) { activationError = SpecBlockActivationError.HAS_SMALL_BLOCKS; return false; }
            if (grids.Count > limits[LimitsChecker.TYPE_MAX_GRIDS]) { activationError = SpecBlockActivationError.HAS_BLOCKS_LIMIT; return false; }
            
            if (info.totalPCU > limits[LimitsChecker.TYPE_PCU]) { activationError = SpecBlockActivationError.HAS_PCU_LIMIT; return false; }
            if (info.totalBlocks > limits[LimitsChecker.TYPE_BLOCKS]) { activationError = SpecBlockActivationError.HAS_BLOCKS_LIMIT; return false; }
            
            
            //if (IsBase)
            //{
            //    var aabb = grids.GetAABB();
            //    if (!POICore.CanCoreWork(aabb.Center, (float)aabb.HalfExtents.Max()))
            //    {
            //        activationError = SpecBlockActivationError.POI;
            //        return false;
            //    }
            //}

            activationError = SpecBlockActivationError.NONE;
            return true;
        }

        private void BlockOnOnMarkForClose(IMyEntity obj) {
            block.AppendingCustomInfo -= BlockOnAppendingCustomInfo;
            block.OnMarkForClose -= BlockOnOnMarkForClose;
        }

        protected virtual void BlockOnAppendingCustomInfo(IMyTerminalBlock arg1, StringBuilder arg2) {
            arg2.Append("\r\nSTATUS: ").Append(T.GetErrorInfo(activationError)).Append("\r\n");

            arg2.Append("\r\n>>> ").Append(block.SlimBlock.BlockDefinition.Id.SubtypeName.Replace("SpecBlock", "")).Append(" Gives: <<<\r\n");
            
            var limits = GetLimits();
            if (limits == null) return;

            arg2.Append("==============\r\n");

            
            foreach (var x in limits.Keys) {
                var am1 = FoundLimits.GetValueOrDefault(x, 0);
                var am2 = limits.GetValueOrDefault(x, 0);
                if (am1 == 0 && am2 == 0) continue;

                arg2.Append(T.GetTypeDescription(x)).Append(" : ").Append(am1).Append("/").Append(am2).Append("\r\n");
            }
        }
    }

    public class RadioAntennaExtension
    {
        public IMyRadioAntenna antenna;
        private static IMyTerminalControlSlider radiusSlider;
        

        private ISpecBlock specBlock;
        public RadioAntennaExtension(IMyRadioAntenna antenna, ISpecBlock specBlock)
        {
            this.specBlock = specBlock;
            this.antenna = antenna;
            this.antenna.Radius = 1;
            if (MyAPIGateway.Session.IsServer)
            {
                FrameExecutor.addFrameLogic(new AutoTimer(100, 100), antenna, l => CheckAntenna());
            }
        }

        protected virtual void CheckAntenna()
        {
            if (!antenna.Enabled) { antenna.Enabled = true; }
            if (!antenna.EnableBroadcasting) { antenna.EnableBroadcasting = true; }
            if (!antenna.ShowShipName) { antenna.ShowShipName = true; }
            var limits = specBlock.GetLimits();

            antenna.HudText = Hooks.GetAntennaText(specBlock, ((UpgradableSpecBlock) specBlock).Settings.Upgrades, specBlock.GetLimits(), antenna.HudText);
            
            if (antenna.Radius < limits[LimitsChecker.TYPE_BROADCASTRADIUS])
            {
                antenna.Radius = limits[LimitsChecker.TYPE_BROADCASTRADIUS];
            }
        }
        
                
        //private void SetRadius(float rad)
        //{
        //    try
        //    {
        //        InitSlider();
        //        antenna.Radius = 1;
        //        radiusSlider.Setter.Invoke(antenna, rad);
        //    }
        //    catch (Exception e)
        //    {
        //        Log.ChatError(e);
        //    }
        //}
        
        //private static void InitSlider()
        //{
        //    if (radiusSlider == null)
        //    {
        //        var list = new List<IMyTerminalControl>();
        //        MyAPIGateway.TerminalControls.GetControls<IMyRadioAntenna>(out list);
        //        foreach (var x in list)
        //        {
        //            if (x.Id == "Radius")
        //            {
        //                radiusSlider = x as IMyTerminalControlSlider;
        //                return;
        //            }
        //        }
        //    }
        //}
    }
}