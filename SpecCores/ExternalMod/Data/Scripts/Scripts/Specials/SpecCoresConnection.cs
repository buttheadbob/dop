using System;
using System.Collections.Generic;
using System.Text;
using Digi;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI;
using Scripts;
using Scripts.Specials;
using Scripts.Specials.ShipClass;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRageMath;

using VRageMath;

namespace ServerMod
{
    [MySessionComponentDescriptor(MyUpdateOrder.NoUpdate)]
    public class SpecCoresConnection : MySessionComponentBase
    {
        public override void LoadData()
        {
            base.LoadData();
            SpecBlockHooks.Init();
            SpecBlockHooks.OnReady += OnHooksReady;
        }

        private void OnHooksReady()
        {
            Log.ChatError("OnHooksReady");
        }
    }
}