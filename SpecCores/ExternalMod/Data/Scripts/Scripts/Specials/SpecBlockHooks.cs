﻿using System;
using System.Collections.Generic;
using Digi;
using Sandbox.ModAPI;
using Scripts.Shared;
using ServerMod;
using VRage.Game.ModAPI;
using F = System.Func<object, bool>;
using FF = System.Func<Sandbox.ModAPI.IMyTerminalBlock, object, bool>;
using A = System.Action<object>;
using C = System.Func<Sandbox.ModAPI.IMyTerminalBlock, object>;
using U = System.Collections.Generic.List<int>;
using L = System.Collections.Generic.IDictionary<int, float>;

namespace Scripts.Specials.ShipClass
{
    public class SpecBlockHooks
    {
        public interface ILimitedBlock
        {
            bool IsDrainingPoints();
            void Disable();
            void CanWork();
            bool CanBeDisabled();
            bool CheckConditions(IMyTerminalBlock specBlock);
        }
        
        public enum GetSpecCoreLimits
        {
            StaticLimits = 1,
            DynamicLimits = 2,
            FoundLimits = 3,
            TotalLimits = 4,
            CustomStatic = 5,
            CustomDynamic = 6
        }
        

        private static Action<string, C, A, FF, F, F, A> RegisterCustomLimitConsumerImpl;
        private static Func<IMyCubeGrid, object> getMainSpecCore;
        private static Func<IMyCubeGrid, IMyTerminalBlock> getMainSpecCoreBlock;
        private static Action<object, L, int> getSpecCoreLimits;
        private static Action<object, U> getSpecCoreUpgrades;
        private static Action<object, L,L> setSpecCoreCustomValues;
        
        private static Func<IMyTerminalBlock, object> getSpecCoreBlock;
        private static Func<object, IMyTerminalBlock> getBlockSpecCore;
        private static Func<IMyTerminalBlock, object> getLimitedBlock;
        private static Func<object, IMyTerminalBlock> getLimitedBlockBlock;
        
        public static event Action OnReady;
        
        public static event Action<object> OnSpecBlockCreated;
        public static event Action<object> OnLimitedBlockCreated;

        public static bool IsReady()
        {
            return 
                RegisterCustomLimitConsumerImpl != null
                ;
        }

        private static void TriggerIsReady()
        {
            if (IsReady()) OnReady?.Invoke();
        }
        
        /// <summary>
        /// Must be inited in LoadData of MySessionComponentBase
        /// </summary>
        public static void Init()
        {
            ModConnection.Init();
            ModConnection.Subscribe<Action<string, C, A, FF, F, F, A>>("MIG.SpecCores.RegisterCustomLimitConsumer", (x) => { RegisterCustomLimitConsumerImpl = x; TriggerIsReady(); });
            ModConnection.Subscribe<Func<IMyCubeGrid, object>>("MIG.SpecCores.GetMainSpecCore", (x) => { getMainSpecCore = x; TriggerIsReady(); });
            ModConnection.Subscribe<Func<IMyCubeGrid, IMyTerminalBlock>>("MIG.SpecCores.GetMainSpecCoreBlock", (x) => { getMainSpecCoreBlock = x; TriggerIsReady(); });
            ModConnection.Subscribe<Action<object, L, int>>("MIG.SpecCores.GetSpecCoreLimits", (x) => { getSpecCoreLimits = x; TriggerIsReady(); });
            ModConnection.Subscribe<Action<object, U>>("MIG.SpecCores.GetSpecCoreUpgrades", (x) => { getSpecCoreUpgrades = x; TriggerIsReady(); });
            ModConnection.Subscribe<Action<object, L,L>>("MIG.SpecCores.SetSpecCoreCustomValues", (x) => { setSpecCoreCustomValues = x; TriggerIsReady(); });

            Action<object> onSpecBlockCreated = (x) => OnSpecBlockCreated?.Invoke(x);
            Action<object> onLimitedBlockCreated = (x) => OnLimitedBlockCreated?.Invoke(x);
            
            ModConnection.SetValue("MIG.SpecCores.OnSpecBlockCreated", onSpecBlockCreated);
            ModConnection.SetValue("MIG.SpecCores.OnLimitedBlockCreated", onLimitedBlockCreated);
        }

        public static void Close()
        {
            ModConnection.Close();
        }
        
        public static void RegisterCustomLimitConsumer(string Id, C OnNewConsumerRegistered, A CanWork, FF CheckConditions, F CanBeDisabled, F IsDrainingPoints, A Disable)
        {
            RegisterCustomLimitConsumerImpl(Id, OnNewConsumerRegistered, CanWork, CheckConditions, CanBeDisabled, IsDrainingPoints, Disable);
        }
        
        /// <summary>
        /// SpecCore
        /// Upgrades
        /// Previous text
        /// Returns: Result string
        /// </summary>
        /// <param name="func"></param>
        public static void SetGetAntennaText(Func<object, U, L, string, string> func)
        {
            ModConnection.SetValue("MIG.SpecCores.GetAntennaText", func);
        }
        
        public static object GetMainSpecCore(IMyCubeGrid grid)
        {
            return getMainSpecCore.Invoke(grid);
        }
        
        public static IMyTerminalBlock GetMainSpecCoreBlock(IMyCubeGrid grid)
        {
            return getMainSpecCoreBlock.Invoke(grid);
        }
        
        public static void GetMainSpecCoreBlock(object specCore, IDictionary<int, float> buffer, GetSpecCoreLimits limits)
        {
            getSpecCoreLimits.Invoke(specCore, buffer, (int) limits);
        }
        
        public static void GetSpecCoreUpgrades(object specCore, List<int> buffer, GetSpecCoreLimits limits)
        {
            getSpecCoreUpgrades.Invoke(specCore, buffer);
        }
        
        public static void SetSpecCoreCustomValues(object specCore, IDictionary<int, float> staticValues, IDictionary<int, float> dynamicValues)
        {
            setSpecCoreCustomValues.Invoke(specCore, staticValues, dynamicValues);
        }
        
        public static object GetSpecCoreBlock(IMyTerminalBlock block)
        {
            return getSpecCoreBlock.Invoke(block);
        }

        public static IMyTerminalBlock GetBlockSpecCore(object block)
        {
            return getBlockSpecCore.Invoke(block);
        }
        
        public static object GetLimitedBlock(IMyTerminalBlock block)
        {
            return getLimitedBlock.Invoke(block);
        }

        public static IMyTerminalBlock GetLimitedBlockBlock(object block)
        {
            return getLimitedBlockBlock.Invoke(block);
        }

        public static void RegisterCustomLimitConsumer(string Id, Func<IMyTerminalBlock, ILimitedBlock> creator)
        {
            SpecBlockHooks.RegisterCustomLimitConsumer(Id, 
                creator,
                (logic)=>
                {
                    ((ILimitedBlock) logic).CanWork();
                },
                (block, logic) =>
                {
                    return ((ILimitedBlock) logic).CheckConditions(block);
                },
                (logic)=>
                {
                    return ((ILimitedBlock) logic).CanBeDisabled();
                },
                (logic)=>
                {
                    return ((ILimitedBlock) logic).IsDrainingPoints();
                },
                (logic)=>
                {
                    ((ILimitedBlock) logic).Disable();
                });
        }

    }
}