﻿using System;
using System.Collections.Generic;
using System.Text;
using Digi;
using Scripts.Specials.ShipClass;
using VRage.Game.Components;

namespace ServerMod
{
    [MySessionComponentDescriptor(MyUpdateOrder.NoUpdate)]
    public class CustomAntennaText : MySessionComponentBase
    {
        static CustomAntennaText()
        {
            SpecBlockHooks.OnReady += HooksOnOnReady;
        }

        private static void HooksOnOnReady()
        {
            SpecBlockHooks.SetGetAntennaText(GetAntennaText);
        }
        
        private static string GetAntennaText(object specCore, List<int> upgrades, IDictionary<int, float> limits, string prevText)
        {
            
            var s = new StringBuilder("Custom Antenna text");
            foreach (var u in upgrades)
            {
                s.Append(" " + u);
            }
            return s.ToString();
        }
    }
}