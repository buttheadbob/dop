﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows.Controls;
using NAPI;
using Torch.Commands;

namespace VStorageNs.Forms
{
    public partial class VStorageGui : UserControl
    {
        public VStorageGui()
        {
            InitializeComponent();
            StringBuilder sb = new StringBuilder();
            PrintCommands(sb, typeof(Code.Commands.VsCommand));
            PrintCommands(sb, typeof(Code.Commands.PlayerCommands));
            PrintCommands(sb, typeof(Code.Commands.AdminCommands));
            PrintCommands(sb, typeof(Code.Commands.AreaCommands));
            TextBlockCommands.Text = sb.ToString();
        }

        private void PrintCommands(StringBuilder sb, Type type)
        {
            var a = type.GetCustomAttribute<CategoryAttribute>();
            var prefix = "!";
            if (a != null)
            {
                var p = a.Path;
                prefix += String.Join(" ", p) + " ";
            }

            var l = new List<Torch.Commands.CommandAttribute>();
            Ext2.GetCommandsInfo(type, l);

            sb.Append(type.Name + ":\n");
            foreach (var x in l)
            {
                sb.Append("\t").Append(prefix).Append(x.Name).Append(": ").AppendLine(x.Description);
            }
        }
    }
}