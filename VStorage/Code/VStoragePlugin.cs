﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Controls;
using NLog;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Scripts.Shared;
using Torch;
using Torch.API;
using Torch.Managers.PatchManager;
using TorchPlugin;
using VStorageNs.Code.Structs;
using VStorageNs.Forms;

namespace VStorageNs.Code
{
    public class VStoragePlugin : CommonPlugin
    {
        private static Persistent<PluginConfig> m_config;
        private static readonly string VARIABLE_NAME = "VStorage_SessionConfig";

        public static readonly Logger Log = LogManager.GetLogger("VStoragePlugin");
        public static PluginConfig CurrentPluginConfig => m_config?.Data;

        public override void Init(ITorchBase torch)
        {
            base.Init(torch);
            
            
            m_config = Persistent<PluginConfig>.Load(Path.Combine(StoragePath, "MIG-VStorage.cfg"));
        }

        public override void Patch(PatchContext context)
        {
            base.Patch(context);
            if (!CurrentPluginConfig.Enabled)
            {
                Log.Error("VStorage disabled in config!");
            }
            LoadData();
            var SaveMethod = typeof(MySession).GetMethod("Save", BindingFlags.Public | BindingFlags.Instance, null, new Type[] { typeof(MySessionSnapshot).MakeByRefType(), typeof(string) }, null);
            if (SaveMethod == null)
            {
                throw new InvalidOperationException("Couldn't find Save() method");
            }
            var beforeSave = typeof(VStoragePlugin).GetMethod(nameof(BeforeSave), BindingFlags.Static | BindingFlags.Public);
            context.GetPattern(SaveMethod).Prefixes.Add(beforeSave);
            
            ModAPI.Init();
        }

        public static bool BeforeSave(out MySessionSnapshot snapshot, string customSaveName = null)
        {
            if (!CurrentPluginConfig.Enabled || VStorage.IsInUninstallProcess)
            {
                snapshot = null;
                return true;
            }

            var sw = System.Diagnostics.Stopwatch.StartNew();
            snapshot = null;
            try
            {
                SaveData();
                ToLog($"Plugin settings saved to the game save.");
            }
            catch (Exception ex)
            {
                VStoragePlugin.ToLog($"Exception in BeforeSave(), ex: {ex}", 2);
            }
            finally
            {
                sw?.Stop();
                VStoragePlugin.ToLog($"BeforeSave elapsed ms: " + sw?.ElapsedMilliseconds + Environment.NewLine);
            }
            return true;
        }

        public static void LoadData()
        {
            if (MyAPIGateway.Utilities.GetVariable(VARIABLE_NAME, out string value))
            {
                VStorage.CurrentSessionConfig = MyAPIGateway.Utilities.SerializeFromBinary<SessionConfig>(Convert.FromBase64String(value));
                ToLog($"Config found. Withdraw places: [{VStorage.CurrentSessionConfig.CollectingAreas.Count}] Players with cells: [{VStorage.CurrentSessionConfig.PlayersCells.Count}]");
            }
            else
            {
                VStorage.CreateSessionConfig();
                ToLog($"Config not found! Using default config!");
                SaveData();
            }
        }

        public static void SaveData()
        {
            if (!CurrentPluginConfig.Enabled)
            {
                Log.Error("VStorage disabled in config, settings not saved!");
                return;
            }
            MyAPIGateway.Utilities.SetVariable(VARIABLE_NAME, Convert.ToBase64String(MyAPIGateway.Utilities.SerializeToBinary(VStorage.CurrentSessionConfig)));
        }

        public static void DeleteData()
        {
            MyAPIGateway.Utilities.RemoveVariable(VARIABLE_NAME);
        }
        public override UserControl CreateControl()
        {
            return new VStorageGui() { DataContext = CurrentPluginConfig };
        }

        public static void ToLog(string msg, int lvl = 0)
        {
            if (lvl == 0)
            {
                Log.Info($"{msg}");
            }
            else if (lvl == 1)
            {
                Log.Warn($"{msg}");
            }
            else
            {
                Log.Error($"{msg}");
            }
        }
    }
}