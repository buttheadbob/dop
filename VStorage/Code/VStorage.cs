﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using SharedLibTorch;
using Torch.Commands;
using VRage;
using VRage.Game;
using VRage.Game.ModAPI;
using VRageMath;
using VStorageNs.Code.Structs;
using System.Text;

namespace VStorageNs.Code
{
    public static class VStorage
    {
        public static SessionConfig CurrentSessionConfig;
        public static bool IsInUninstallProcess = false;
        public static void CleanupInternal()
        {
            foreach (var group in GetGridGroups(x => x is IMyBeacon))
            {
                DisasmGroup(group);
            }
        }

        public static void DisasmGroup(List<MyCubeGrid> group)
        {
            List<MyObjectBuilder_CubeGrid> gridsOb = new List<MyObjectBuilder_CubeGrid>();
            if (!TryGetArea(group.FirstOrDefault().PositionComp.GetPosition(), out CollectingArea area))
            {
                return;
            }

            MyAPIGateway.Parallel.ForEach(group, (x) =>
            {
                if (x.Closed || x.MarkedForClose) { return; }
                MyObjectBuilder_CubeGrid ob = (MyObjectBuilder_CubeGrid)x.GetObjectBuilder();
                lock (gridsOb) { gridsOb.Add(ob); }
                x.Close();
                return;
            });
            /*Dvar sw = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i < 1000; i++)
            {
                temp = DeconstructUtils.GetAllComponentsAndInventoriesForPlayers(gridsOb);
            }
            sw?.Stop();
            Log.Error("GetAllComponentsAndInventoriesForPlayers Elapsed ms: " + sw?.ElapsedMilliseconds / 1000f + Environment.NewLine);
            */

            Dictionary<long, Dictionary<MyDefinitionId, MyFixedPoint>> ComponentsAndInventoriesForPlayers = DeconstructUtils.GetAllComponentsAndInventoriesForPlayers(VStoragePlugin.CurrentPluginConfig.GasList, gridsOb);
            Dictionary<long, Dictionary<string, double>> tmp = new Dictionary<long, Dictionary<string, double>>(ComponentsAndInventoriesForPlayers.Count);
            foreach (var playerBucket in ComponentsAndInventoriesForPlayers)
            {
                if (!tmp.ContainsKey(playerBucket.Key))
                {
                    tmp.Add(playerBucket.Key, new Dictionary<string, double>(10));
                }

                foreach (KeyValuePair<MyDefinitionId, MyFixedPoint> item in playerBucket.Value)
                {
                    if (!tmp[playerBucket.Key].ContainsKey(item.Key.ToString()))
                    {
                        tmp[playerBucket.Key].Add(item.Key.ToString(), (double)item.Value);
                    }
                    else
                    {
                        tmp[playerBucket.Key][item.Key.ToString()] += (double)item.Value;
                    }
                }
            }

            string gridNames = "";
            foreach (var grid in gridsOb)
            {
                gridNames += $", [ {grid.DisplayName} / {grid.EntityId} ] ";
            }

            string cellsis = "";
            foreach (var playerBucket in tmp)
            {
                var cellid = AddCell(playerBucket.Key, area, playerBucket.Value);
                cellsis += $", [{playerBucket.Key}]/[{cellid}] ";
            }

            VStoragePlugin.ToLog($"Grids: [{gridNames.Substring(1)}] was deleted from the server and added to cells: {Environment.NewLine}[{cellsis.Substring(1)}]");
        }

        public static void PrintCellItems(PlayerCell cell, CommandContext context)
        {
            var sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine($"Cell [{cell.ID}] contains:");
            foreach (var item in cell.InvItems)
            {
                sb.AppendLine($"[{item.Key.Replace("MyObjectBuilder_", "") + ']',-28} - [{item.Value}]");
            }
            context.Respond(sb.ToString());
        }

        public static void WithdrawInternal(int id, IMyCharacter player, CommandContext context)
        {
            if (!VStorage.CurrentSessionConfig.PlayersCells.ContainsKey(context.Player.IdentityId))
            {
                context.Respond($"Your don't have cells.");
                return;
            }

            var playerCells = VStorage.CurrentSessionConfig.PlayersCells[context.Player.IdentityId];
            if (playerCells == null || !playerCells.Any())
            {
                context.Respond($"Your don't have any cells.");
                return;
            }

            var tmpplayerCell = playerCells.Where(x => x.ID == id);
            PlayerCell playerCell;
            if (tmpplayerCell.Any())
            {
                playerCell = tmpplayerCell.First();
            }
            else
            {
                context.Respond($"Cell with id: [{id}] not found.");
                return;
            }

            var currTime = DateTime.UtcNow.ToUnixTimestamp();
            if (playerCell.ExpiresAt <= currTime)
            {
                context.Respond($"Cell: [{playerCell.ID}] expired and removed!");
                VStorage.CurrentSessionConfig.PlayersCells[context.Player.IdentityId].RemoveAll(x => x.ID == id);
                if (VStorage.CurrentSessionConfig.PlayersCells[context.Player.IdentityId] == null)
                {
                    VStorage.CurrentSessionConfig.PlayersCells.Remove(context.Player.IdentityId);
                }
                return;
            }
            context.Respond($"Withdraw form cell: {playerCell.ID} invCount: {playerCell.InvItems.Count()}");
            var pos = player.PositionComp.GetPosition();
            var area = playerCell.SavedCollectingArea;

            if (!TryGetClosestWithdrawPlace(pos, area, out WithdrawPlace withdrawPlace))
            {
                context.Respond($"This collecting area don't have any withdraw place! Ask admin. AreaID: [{area.Name}] Cell: [{playerCell.ID}] inventory count: {playerCell.InvItems.Count()}");
                return;
            }

            if (((withdrawPlace.Position - pos).LengthSquared()) > (withdrawPlace.Radius * withdrawPlace.Radius))
            {
                context.Respond($"Place: [{area.Name}]  too far from you!");
                return;
            }

            foreach (var item in playerCell.InvItems.ToList())
            {
                context.Respond($"item: [{item.Key}]  amount [{item.Value}]");
                var temp = playerCell.InvItems[item.Key] = Math.Round(item.Value * ((1 - (currTime - playerCell.AddedAt) / (playerCell.ExpiresAt - playerCell.AddedAt)) * (1 - withdrawPlace.ConstantTax) * (1 - withdrawPlace.TimeTax)));
                context.Respond($"after tax item: [{item.Key}]  amount [{temp}]");
            }

            WithdrawResult result = WithdrawUtils.AddToInventories(context.Player, playerCell.InvItems);
            if (result == WithdrawResult.Ok)
            {
                VStorage.CurrentSessionConfig.PlayersCells[context.Player.IdentityId].RemoveAll(x => x.ID == id);
                if (VStorage.CurrentSessionConfig.PlayersCells[context.Player.IdentityId] == null)
                {
                    VStorage.CurrentSessionConfig.PlayersCells.Remove(context.Player.IdentityId);
                }
                context.Respond($"Done!");
                return;
            }
            else if (result == WithdrawResult.NoPlayerInventory)
            {
                context.Respond($"Character inventory not found, u need to be alive to receive items.");
                return;
            }
            else
            {
                context.Respond($"Cell with id [{id}] not found.");
                return;
            }
        }

        public static void RecreatePlacesGPSForArea(long playerId, CollectingArea area)
        {
            List<IMyGps> list = new List<IMyGps>();
            foreach (var place in area.WithdrawPlaces)
            {
                var gname = place.Position + place.Name;
                var gps = GetWithDescription(gname, playerId);
                if (gps != null)
                {
                    gps.Name = area.Name + ": " + place.Name;
                    gps.Coords = place.Position;
                    MyAPIGateway.Session.GPS.ModifyGps(playerId, gps);
                }
                else
                {
                    var newgps = MyAPIGateway.Session.GPS.Create(area.Name + ": " + place.Name, gname, place.Position, true);
                    MyAPIGateway.Session.GPS.AddGps(playerId, newgps);
                }
            }
        }

        public static IMyGps GetWithDescription(string startsWith, long player)
        {
            List<IMyGps> list = MyAPIGateway.Session.GPS.GetGpsList(player);
            foreach (var item in list)
            {
                if (item.Description != null && item.Description.StartsWith(startsWith))
                {
                    return item;
                }
            }
            return null;
        }
        private static bool TryGetClosestWithdrawPlace(Vector3D pos, CollectingArea area, out WithdrawPlace place)
        {
            if (area.WithdrawPlaces == null || area.WithdrawPlaces.Count == 0)
            {
                place = new WithdrawPlace();
                return false;
            }

            var tempPlaces = area.WithdrawPlaces.ToList();
            tempPlaces.SortNoAlloc((x, y) => Vector3D.DistanceSquared(x.Position, pos).CompareTo(Vector3D.DistanceSquared(x.Position, pos)));
            place = tempPlaces.FirstOrDefault();
            return true;
        }

        private static bool TryGetArea(Vector3D pos, out CollectingArea area)
        {
            //TODO sort
            /* foreach (var area2 in CurrentSessionConfig.CollectingAreas)
             {
                 VStoragePlugin.ToLog($"Vector3D.DistanceSquared(area2.Position, pos) [{Vector3D.DistanceSquared(area2.Position, pos)}] <=" +
                     $" (area2.Radius * area2.Radius): [{(area2.Radius * area2.Radius)}]" +
                     $"res: {(Vector3D.DistanceSquared(area2.Position, pos)) <= (area2.Radius * area2.Radius)}!");
             }*/

            var areas = CurrentSessionConfig.CollectingAreas.Where(x => (Vector3D.DistanceSquared(x.Position, pos)) <= (x.Radius * x.Radius)).ToList();
            if (areas.Count() != 0)
            {
                areas.SortNoAlloc((x, y) => Vector3D.DistanceSquared(x.Position, pos).CompareTo(Vector3D.DistanceSquared(x.Position, pos)));
                area = areas.First();
                return true;
            }
            else
            {
                area = new CollectingArea();
                VStoragePlugin.ToLog($"No collecting areas configured on the server!");
                return false;
            }
        }

        public static int AddCell(long identityId, CollectingArea area, Dictionary<string, double> items, uint expiresAt = 0)
        {
            var currTime = DateTime.UtcNow;
            var addedAt = currTime.ToUnixTimestamp();
            expiresAt = expiresAt == 0 ? (currTime + TimeSpan.FromMinutes(area.ExpireInMinutes)).ToUnixTimestamp() : expiresAt;

            if (!CurrentSessionConfig.PlayersCells.ContainsKey(identityId))
            {
                CurrentSessionConfig.PlayersCells.Add(identityId, new List<PlayerCell>());
            }

            if (CurrentSessionConfig.PlayersCells[identityId] == null)
            {
                CurrentSessionConfig.PlayersCells[identityId] = new List<PlayerCell>();
            }

            PlayerCell newcell = new PlayerCell(GenUniqId(identityId), addedAt, expiresAt, area, items);

            VStoragePlugin.ToLog($"Cell with id: [{newcell.ID}] added to [{identityId}].");
            CurrentSessionConfig.PlayersCells[identityId].Add(newcell);
            return newcell.ID;
        }

        public static List<List<MyCubeGrid>> GetGridGroups(Func<MyCubeBlock, bool> filter)
        {
            var arrayList = new List<List<MyCubeGrid>>();

            MyAPIGateway.Parallel.ForEach(MyCubeGridGroups.Static.Physical.Groups, (group) =>
            {
                bool found = false;
                List<MyCubeGrid> grids = new List<MyCubeGrid>();
                foreach (var x in group.Nodes)
                {
                    if (x.NodeData.Closed || x.NodeData.MarkedForClose) { return; }
                    grids.Add(x.NodeData);
                    foreach (var y in x.NodeData.GetFatBlocks())
                    {
                        if (y == null) { continue; }

                        if (filter.Invoke(y))
                        {
                            found = true;
                        }
                    }
                }

                if (!found)
                {
                    lock (arrayList) { arrayList.Add(grids); }
                }
            });

            return arrayList;
        }

        private static int GenUniqId(long playerID)
        {
            if (!CurrentSessionConfig.PlayersCells.ContainsKey(playerID))
            {
                return 1;
            }
            if (CurrentSessionConfig.PlayersCells[playerID] == null)
            {
                return 1;
            }
            
            var pc = CurrentSessionConfig.PlayersCells[playerID];
            var id = pc.Count == 0 ? 1 : pc[pc.Count - 1].ID + 1;
            return Math.Min(id, 1);
        }

        public static void CreateSessionConfig()
        {
            var withdrawPlace = new WithdrawPlace()
            {
                Position = Vector3D.Zero,
                Name = "Default",
                TimeTax = 0f,
                Radius = double.MaxValue,
                ConstantTax = 0f
            };

            var defArea = new CollectingArea()
            {
                Name = "DefaultArea",
                Position = Vector3D.Zero,
                Radius = double.MaxValue,
                ExpireInMinutes = (int)(DateTime.MaxValue - DateTime.Now).TotalMinutes,
                WithdrawPlaces = new List<WithdrawPlace>() { withdrawPlace }
            };

            var defCell = new PlayerCell() { AddedAt = DateTime.UtcNow.ToUnixTimestamp(), ExpiresAt = (DateTime.UtcNow.AddDays(2)).ToUnixTimestamp(), ID = 1, SavedCollectingArea = defArea };
            var defAreas = new List<CollectingArea>() { defArea };
            var defCells = new Dictionary<long, List<PlayerCell>>();
            defCells.Add(99999999, new List<PlayerCell>() { defCell });
            CurrentSessionConfig = new SessionConfig() { PlayersCells = defCells, CollectingAreas = defAreas };
        }
    }
}