﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using NAPI;
using Sandbox.Definitions;
using Scripts.Shared;
using VRage;
using VStorageNs.Code.Structs;

namespace VStorageNs.Code
{
    public class ModAPI
    {
        public static void Init()
        {
            ModConnection.InitOnce();

            Func<long, int, string, Dictionary<string, double>, bool> action = AddUniqCellItems;
            ModConnection.SetValue("MIG.VStorage.Add", action);
        }
        
        
        public static bool AddUniqCellItems(long player, int cellId, string withdrawName, Dictionary<string, double> items)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                return false;
            }
            
            var playerCells = VStorage.CurrentSessionConfig.PlayersCells;
            if (!playerCells.TryGetValue(player, out var cells))
            {
                playerCells[player] = new List<PlayerCell>();
                cells = new List<PlayerCell>();
            }

            var areas = VStorage.CurrentSessionConfig.CollectingAreas.Where((x) => x.Name == withdrawName);
            if (areas.Count() == 0)
            {
                return false;
            }

            var area = areas.First();
            
            //var collection = 
            var currTime = DateTime.UtcNow.ToUnixTimestamp();
            var cellWhere = cells.Where((x) => x.ID == cellId);
            
            if (cellWhere.Count() == 0)
            {
                var cell = new PlayerCell();
                cell.AddedAt = currTime;
                cell.ExpiresAt = (DateTime.UtcNow.AddYears(9)).ToUnixTimestamp();
                cell.SavedCollectingArea = area;
                cell.InvItems = items;
                cells.Insert(0, new PlayerCell());
            }
            else
            {
                PlayerCell cell = cellWhere.First();
                SharpUtils.Sum(cell.InvItems, items);
            }

            return true;
        }
    }
}