﻿using System.Collections.Generic;
using ProtoBuf;

namespace VStorageNs.Code.Structs
{
    [ProtoContract]
    public struct PlayerCell
    {
        [ProtoMember(1)]
        public int ID;
        [ProtoMember(2)]
        public uint AddedAt; //time stamp when added
        [ProtoMember(3)]
        public uint ExpiresAt; //time stamp when should decoy
        [ProtoMember(4)]
        public CollectingArea SavedCollectingArea; //Save current area
        [ProtoMember(5)]
        public Dictionary<string, double> InvItems; // list with all grid components + inventories

        public PlayerCell(int id, uint addedAt, uint expiresAt, CollectingArea savedWithdrawPlace, Dictionary<string, double> invItems)
        {
            ID = id;
            AddedAt = addedAt;
            ExpiresAt = expiresAt;
            SavedCollectingArea = savedWithdrawPlace;
            InvItems = invItems;
        }
    }
}
