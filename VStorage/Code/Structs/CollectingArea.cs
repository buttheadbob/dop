﻿using ProtoBuf;
using System.Collections.Generic;
using VRageMath;

namespace VStorageNs.Code.Structs
{
    [ProtoContract]
    public struct CollectingArea
    {
        [ProtoMember(1)]
        public string Name;
        [ProtoMember(2)]
        public Vector3D Position;
        [ProtoMember(3)]
        public double Radius;
        [ProtoMember(4)]
        public int ExpireInMinutes;
        [ProtoMember(5)]
        public List<WithdrawPlace> WithdrawPlaces;
    }
}
