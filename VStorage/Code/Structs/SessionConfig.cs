﻿using System.Collections.Generic;
using ProtoBuf;

namespace VStorageNs.Code.Structs
{
    [ProtoContract]
    public class SessionConfig
    {
        [ProtoMember(1)]
        public Dictionary<long, List<PlayerCell>> PlayersCells;
        [ProtoMember(2)]
        public List<CollectingArea> CollectingAreas;
    }
}
