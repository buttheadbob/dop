﻿namespace VStorageNs.Code.Structs
{
    public enum WithdrawResult
    {
        Ok,
        NoPlayerInventory,
        Exception,
        NotFound
    }
}
