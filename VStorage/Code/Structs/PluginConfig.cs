using System;
using System.Collections.Generic;
using Torch;
using SharedLibTorch;

namespace VStorageNs.Code.Structs
{
    public class PluginConfig : ViewModel
    {
        private bool _enabled;
        public bool Enabled { get => _enabled; set => SetValue(ref _enabled, value); }

        private List<DeconstructUtils.GasToOreRatio> _gasList;
        public List<DeconstructUtils.GasToOreRatio> GasList { get => _gasList; set => SetValue(ref _gasList, value); }

    }
}