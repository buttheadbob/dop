﻿using ProtoBuf;
using VRageMath;

namespace VStorageNs.Code.Structs
{
    [ProtoContract]
    public struct WithdrawPlace
    {
        [ProtoMember(1)]
        public string Name;
        [ProtoMember(2)]
        public Vector3D Position;
        [ProtoMember(3)]
        public double Radius;
        [ProtoMember(4)]
        public double ConstantTax; // % items tax
        [ProtoMember(5)]
        public double TimeTax; // % based on time items tax
    }
}
