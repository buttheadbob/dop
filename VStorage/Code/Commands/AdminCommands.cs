using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using NAPI;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage;
using VRage.Game;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRageMath;
using SharedLibTorch;

namespace VStorageNs.Code.Commands
{
    [Category("vs")]
    public class AdminCommands : CommandModule
    {
        [Command("cleanup", "Do cleanup.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void Cleanup()
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            var sw = System.Diagnostics.Stopwatch.StartNew();
            Context.Respond("Cleanup started.");
            VStorage.CleanupInternal();
            sw?.Stop();
            Context.Respond("Elapsed ms: " + sw?.ElapsedMilliseconds + Environment.NewLine);
        }

        [Command("alllist", "List of all cells on server.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void AllList()
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            var sw = System.Diagnostics.Stopwatch.StartNew();
            try
            {
                if (VStorage.CurrentSessionConfig.PlayersCells == null || VStorage.CurrentSessionConfig.PlayersCells.Count == 0)
                {
                    Context.Respond($"No any cell on server!");
                }

                foreach (var player in VStorage.CurrentSessionConfig.PlayersCells)
                {
                    if (player.Value == null)
                    {
                        continue;
                    }
                    foreach (var cell in player.Value)
                    {
                        string invcount = cell.InvItems == null ? "NULL!" : cell.InvItems.Count().ToString();
                        Context.Respond($"Player: [{player.Key}/{player.Key.PlayerName()}] Cell: {cell.ID} invCount: {invcount}");
                    }
                }
            }
            catch (Exception ex)
            {
                Context.Respond($"Exception while processing your command, contact admin.");
                string name = "Unknown";
                if (Context.Player != null)
                {
                    name = Context.Player.DisplayName;
                }
                VStoragePlugin.ToLog($"Exception in List(), player: [{name}] ex: {ex}");
            }
            finally
            {
                sw?.Stop();
                Context.Respond("Elapsed ms: " + sw?.ElapsedMilliseconds + Environment.NewLine);
            }
        }

        [Command("areaslist", "Prints all collecting areas.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void AreasList()
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            var sw = System.Diagnostics.Stopwatch.StartNew();
            foreach (var area in VStorage.CurrentSessionConfig.CollectingAreas)
            {
                string tmpareaWithdrawPlaces = area.WithdrawPlaces == null ? " " : area.WithdrawPlaces.Count().ToString();
                Context.Respond($"Name: [{area.Name}] Radius: [{area.Radius}] {Environment.NewLine}" +
                    $"ExpireInMinutes: [{area.ExpireInMinutes}] WithdrawPlaces: [{tmpareaWithdrawPlaces}] {Environment.NewLine}" +
                    $"Center: [{area.Position}]");
            }
            sw?.Stop();
            Context.Respond("Elapsed ms: " + sw?.ElapsedMilliseconds + Environment.NewLine);
        }

        [Command("admlist", "Prints cells list of any player.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void AdminList(string nameOrSteamId)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (!CommandsHelpers.TryParseIdentity(Context, nameOrSteamId, out MyIdentity identity))
            {
                Context.Respond($"Player with name/steamid [{nameOrSteamId}] not found!");
                return;
            }

            if (!VStorage.CurrentSessionConfig.PlayersCells.ContainsKey(identity.IdentityId) || VStorage.CurrentSessionConfig.PlayersCells[identity.IdentityId] == null)
            {
                Context.Respond($"Player {identity.DisplayName} don't have cells.");
                return;
            }

            var playerCells = VStorage.CurrentSessionConfig.PlayersCells[identity.IdentityId];

            Context.Respond($"Player [{identity.DisplayName}] have: [{playerCells.Count}] cells.");
            foreach (var cell in playerCells)
            {
                Context.Respond($"{identity.DisplayName} Cell: {cell.ID} invCount: {cell.InvItems.Count()}");
            }
        }

        [Command("disasm", "Disassemble ship with target name.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void DisassembleShip(string shipName = "")
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (string.IsNullOrWhiteSpace(shipName))
            {
                Context.Respond("Enter valid ship name!");
                return;
            }
            else
            {
                HashSet<IMyEntity> currentShipList = new HashSet<IMyEntity>();
                MyAPIGateway.Entities.GetEntities(currentShipList, e => e is IMyCubeGrid && e.DisplayName.Equals(shipName, StringComparison.InvariantCultureIgnoreCase));
                if (currentShipList != null && currentShipList.Any())
                {
                    var gridgroup = MyCubeGridGroups.Static.GetGroups(GridLinkTypeEnum.Logical).GetGroupNodes((MyCubeGrid)currentShipList.First());
                    VStorage.DisasmGroup(gridgroup);
                    Context.Respond($"Ship with name [{shipName}] disassembled!");
                    return;
                }
            }

            Context.Respond($"Failed. Ship with name [{shipName}] not found!");
            return;
        }

        [Command("addcell", "Add empty cell to player account.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void AddCell(string nameOrSteamId, double expiresInMunutes, string CollectingAreaName)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (!CommandsHelpers.TryParseIdentity(Context, nameOrSteamId, out MyIdentity identity))
            {
                Context.Respond($"Player with name/steamid [{nameOrSteamId}] not found!");
                return;
            }

            if (expiresInMunutes == 0)
            {
                expiresInMunutes = 9999999; // ~9 years
            }

            var areatmp = VStorage.CurrentSessionConfig.CollectingAreas.Where(x => x.Name == CollectingAreaName);
            if (!areatmp.Any())
            {
                Context.Respond($"Failed. Area with name: [{CollectingAreaName}] not found.");
                return;
            }

            var currTime = DateTime.UtcNow;
            var expiresAt = (currTime + TimeSpan.FromMinutes(expiresInMunutes)).ToUnixTimestamp();
            var cellId = VStorage.AddCell(identity.IdentityId, areatmp.First(), new Dictionary<string, double>(), expiresAt);
            Context.Respond($"Cell with id: [{cellId}] created in {identity.DisplayName} account.");
        }

        [Command("addcellhere", "Add empty cell to player account.Uses your position for get collection area.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void AddCellHere(string nameOrSteamId, double expiresInMunutes)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (!CommandsHelpers.TryParseIdentity(Context, nameOrSteamId, out MyIdentity identity))
            {
                Context.Respond($"Player with name/steamid [{nameOrSteamId}] not found!");
                return;
            }

            if (Context.Player == null || Context.Player.Character == null || Context.Player.Character.IsDead)
            {
                Context.Respond("Failed. For use this command u need a body!");
                return;
            }

            if (expiresInMunutes == 0)
            {
                expiresInMunutes = 9999999; // ~9 years
            }

            var playerPos = Context.Player.Character.PositionComp.GetPosition();
            var areatmp = VStorage.CurrentSessionConfig.CollectingAreas.Where(x => Vector3D.DistanceSquared(x.Position, playerPos) < x.Radius * x.Radius);
            if (areatmp == null || !areatmp.Any())
            {
                Context.Respond($"Failed. There's no places around.");
                return;
            }

            var currTime = DateTime.UtcNow;
            var expiresAt = (currTime + TimeSpan.FromMinutes(expiresInMunutes)).ToUnixTimestamp();
            var cellId = VStorage.AddCell(identity.IdentityId, areatmp.First(), new Dictionary<string, double>(), expiresAt);
            Context.Respond($"Cell with id: [{cellId}] created in {identity.DisplayName} account.");
        }

        [Command("remcell", "Delete cell from player account.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void RemCell(string nameOrSteamId, int sellID)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (!CommandsHelpers.TryParseIdentity(Context, nameOrSteamId, out MyIdentity identity))
            {
                Context.Respond($"Player with name/steamid [{nameOrSteamId}] not found!");
                return;
            }

            if (!VStorage.CurrentSessionConfig.PlayersCells.ContainsKey(identity.IdentityId) || VStorage.CurrentSessionConfig.PlayersCells[identity.IdentityId] == null)
            {
                Context.Respond($"Player {identity.DisplayName} don't have cells.");
                return;
            }

            var playerCells = VStorage.CurrentSessionConfig.PlayersCells[identity.IdentityId];
            if (playerCells.RemoveAll(x => x.ID == sellID) > 0)
            {
                Context.Respond($"Cell with id [{sellID}] was removed from [{identity.DisplayName}] account.!");
            }
            else
            {
                Context.Respond($"Cell with id [{sellID}] not found in [{identity.DisplayName}] account.!");
            }
        }

        [Command("additem", "Add item amount to a player cell.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void AddItem(string nameOrSteamId, int cellId, string itemMatcher, double amount)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (!CommandsHelpers.TryParseIdentity(Context, nameOrSteamId, out MyIdentity identity))
            {
                Context.Respond($"Player with name/steamid [{nameOrSteamId}] not found!");
                return;
            }

            if (!VStorage.CurrentSessionConfig.PlayersCells.ContainsKey(identity.IdentityId) || VStorage.CurrentSessionConfig.PlayersCells[identity.IdentityId] == null)
            {
                Context.Respond($"Player {identity.DisplayName} don't have cells.");
                return;
            }

            var playerCells = VStorage.CurrentSessionConfig.PlayersCells[identity.IdentityId];
            var tmpcell = playerCells.Where(x => x.ID == cellId);
            if (!tmpcell.Any())
            {
                Context.Respond($"CellId [{cellId}] not found in player {identity.DisplayName}.");
                Context.Respond($"Player cells:");
                foreach (var acell in playerCells)
                {
                    Context.Respond($"{identity.DisplayName} Cell: {acell.ID} invCount: {acell.InvItems.Count()}");
                }
            }

            var cell = tmpcell.First();
            var matcher = new SharedLib.BlockIdMatcher(itemMatcher);
            var foundDefs = MyDefinitionManager.Static.GetAllDefinitions().Where(x => matcher.Matches(x.Id.TypeId.ToString(), x.Id.SubtypeId.String));
            Context.Respond($"Found definitions: " + foundDefs.Count());
            if (!foundDefs.Any())
            {
                Context.Respond($"ItemMatcher: Item [{itemMatcher}] not found!");
                return;
            }

            var defId = foundDefs.First().Id.ToString();
            if (cell.InvItems.ContainsKey(defId))
            {
                cell.InvItems[defId] += amount;
            }
            else
            {
                cell.InvItems.Add(defId, amount);
            }

            Context.Respond($"Success!: " + foundDefs.Count());
            VStorage.PrintCellItems(cell, Context);
        }

        [Command("remitem", "Remove item amount from player cell.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void RemoveItem(string nameOrSteamId, int cellId, string itemMatcher, double amount)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (!CommandsHelpers.TryParseIdentity(Context, nameOrSteamId, out MyIdentity identity))
            {
                Context.Respond($"Player with name/steamid [{nameOrSteamId}] not found!");
                return;
            }

            if (!VStorage.CurrentSessionConfig.PlayersCells.ContainsKey(identity.IdentityId) || VStorage.CurrentSessionConfig.PlayersCells[identity.IdentityId] == null)
            {
                Context.Respond($"Player {identity.DisplayName} don't have cells.");
                return;
            }

            var playerCells = VStorage.CurrentSessionConfig.PlayersCells[identity.IdentityId];

            var tmpcell = playerCells.Where(x => x.ID == cellId);
            if (!tmpcell.Any())
            {
                Context.Respond($"CellId [{cellId}] not found in player {identity.DisplayName}.");
                Context.Respond($"Player cells:");
                foreach (var acell in playerCells)
                {
                    Context.Respond($"{identity.DisplayName} Cell: {acell.ID} invCount: {acell.InvItems.Count()}");
                }
            }

            var cell = tmpcell.First();
            double famount = (double)amount;
            var matcher = new SharedLib.BlockIdMatcher(itemMatcher);

            Dictionary<string, double> invItemsToRemove = new Dictionary<string, double>();
            foreach (var invItem in cell.InvItems)
            {
                if (MyDefinitionId.TryParse(invItem.Key, out MyDefinitionId definitionId))
                {
                    if (matcher.Matches(definitionId.TypeId.ToString(), definitionId.SubtypeId.String))
                    {
                        invItemsToRemove.Add(invItem.Key, invItem.Value);
                    }
                }
            }

            // var foundItems = cell.InvItems.Where(x => matcher.Matches(x.Key.TypeId.ToString(), x.Key.SubtypeId.String));
            var item = invItemsToRemove.First();
            Context.Respond($"Found definitions: " + invItemsToRemove.Count());
            Context.Respond($"Old amount: " + item.Value);
            if (item.Value - famount <= 0)
            {
                cell.InvItems.Remove(item.Key);
            }
            else
            {
                cell.InvItems[item.Key] = item.Value - famount;
            }

            Context.Respond($"Success! new amount: " + cell.InvItems[item.Key]);
            VStorage.PrintCellItems(cell, Context);
        }

        [Command("delete-settings-all-data-will-be-lost", "Delete areas and places only!", "")]
        [Permission(MyPromoteLevel.Admin)]
        public void DeleteAllCollectingAreas(string confirm = "")
        {
            if (confirm != "ok")
            {
                Context.Respond("Aborted! Danger! For pass this check add 'ok' after command.");
                return;
            }

            VStorage.CurrentSessionConfig.CollectingAreas.Clear();
            Context.Respond("Areas and places have been deleted.");
        }

        [Command("delete-cells-all-data-will-be-lost", "Delete player cells only!", "")]
        [Permission(MyPromoteLevel.Admin)]
        public void DeleteAllPlayersCells(string confirm = "")
        {
            if (confirm != "ok")
            {
                Context.Respond("Aborted! Danger! For pass this check add 'ok' after command.");
                return;
            }

            VStorage.CurrentSessionConfig.PlayersCells.Clear();
            Context.Respond("Players cells have been deleted.");
        }

        [Command("reset-all-settings-all-data-will-be-lost", "Reset all data to default, all data will be removed!", "")]
        [Permission(MyPromoteLevel.Admin)]
        public void ResetAllSettings(string confirm = "")
        {
            if (confirm != "ok")
            {
                Context.Respond("Aborted! Danger! For pass this check add 'ok' after command.");
                return;
            }

            VStorage.CreateSessionConfig();
            Context.Respond("Settings have been reset to default.");
        }

        [Command("uninstall-all-data-will-be-lost", "Remove all data from game save and disable plugin, all data will be removed!!!", "")]
        [Permission(MyPromoteLevel.Admin)]
        public void UninstallPlugin(string confirm = "")
        {
            if (confirm != "ok")
            {
                Context.Respond("Aborted! Danger! For pass this check add 'ok' after command.");
                return;
            }

            VStoragePlugin.CurrentPluginConfig.Enabled = false;
            VStorage.IsInUninstallProcess = true;
            VStorage.CurrentSessionConfig = null;
            VStoragePlugin.DeleteData();
            Context.Respond("Settings have been deleted from save, now u can save game and restart.");
        }
    }
}