﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game.ModAPI;
using VRageMath;
using VStorageNs.Code.Structs;

namespace VStorageNs.Code.Commands
{
    [Category("vs")]
    public class AreaCommands : CommandModule
    {
        [Command("setarea", "Create new or modify existent collecting area.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void SetCollectingArea(string name, double radius, int expireInMinutes, double posX, double posY, double posZ)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (string.IsNullOrEmpty(name))
            {
                Context.Respond("This command need area name!");
            }
            var tmparea = VStorage.CurrentSessionConfig.CollectingAreas.Where(x => x.Name == name);
            List<WithdrawPlace> withdraw = null;
            if (tmparea.Any())
            {
                withdraw = tmparea.First().WithdrawPlaces;
            }

            VStorage.CurrentSessionConfig.CollectingAreas.RemoveAll(x => x.Name == name);
            var area = new CollectingArea()
            {
                Name = name,
                Radius = radius,
                ExpireInMinutes = expireInMinutes,
                Position = new Vector3D(posX, posY, posZ),
                WithdrawPlaces = withdraw == null ? new List<WithdrawPlace>() : withdraw
            };
            VStorage.CurrentSessionConfig.CollectingAreas.Add(area);
            Context.Respond($"Area [{area.Name}] configured!");
        }

        [Command("remarea", "Remove collecting area.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void RemoveCollectingArea(string name)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (string.IsNullOrEmpty(name))
            {
                Context.Respond("This command need area name!");
            }

            if (VStorage.CurrentSessionConfig.CollectingAreas.RemoveAll(x => x.Name == name) > 0)
            {
                Context.Respond($"Area [{name}] removed!");
            }
            else
            {
                Context.Respond($"Area [{name}] not found!");
            }
        }

        [Command("placeslist", "Prints all withdraw places in area.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void PlacesList(string areaName)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            var sw = System.Diagnostics.Stopwatch.StartNew();
            if (string.IsNullOrWhiteSpace(areaName))
            {
                Context.Respond("This command need area name!");
                return;
            }

            var tmparea = VStorage.CurrentSessionConfig.CollectingAreas.Where(x => x.Name == areaName);
            if (!tmparea.Any())
            {
                Context.Respond($"Area with name [{areaName}] not found!");
                return;
            }

            CollectingArea area = tmparea.First();
            if (!area.WithdrawPlaces.Any())
            {
                Context.Respond($"Area with name [{areaName}] don't have withdraw places!");
                return;
            }
            var sb = new StringBuilder();
            sb.Append(Environment.NewLine);
            sb.AppendLine($"Area [{areaName}]:");
            sb.AppendLine($"-Radius [{string.Format("{0:0.00}", area.Radius)}]");
            sb.AppendLine($"-ExpireInMinutes [{string.Format("{0:0.00}", area.ExpireInMinutes)}]");
            sb.AppendLine($"-Center [{area.Position}]");
            sb.AppendLine($"-WithdrawPlaces:");
            foreach (var place in area.WithdrawPlaces)
            {
                sb.AppendLine($"--Place: [{place.Name}] {Environment.NewLine}" +
                    $"--Radius: [{string.Format("{0:0.00}", place.Radius)}] {Environment.NewLine}" +
                    $"--TimeTax: [{string.Format("{0:0.00}", place.TimeTax)}] ConstantTax: [{string.Format("{0:0.00}", place.ConstantTax)}] {Environment.NewLine}" +
                    $"--Position: [{place.Position}]{Environment.NewLine} ");
            }
            Context.Respond(sb.ToString());
            if (Context.Player != null)
            {
                VStorage.RecreatePlacesGPSForArea(Context.Player.IdentityId, area);
            }

            sw?.Stop();
            Context.Respond("Elapsed ms: " + sw?.ElapsedMilliseconds + Environment.NewLine);
        }

        [Command("setplace", "Create new or modify existent withdraw place in collecting area.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void SetWithdrawPlace(string areaName, string witdrawPlaceName, int withdrawRadius, double timeTax, double constantTax, double posX, double posY, double posZ)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (string.IsNullOrWhiteSpace(areaName))
            {
                Context.Respond("This command need area name!");
                return;
            }

            if (string.IsNullOrWhiteSpace(witdrawPlaceName))
            {
                Context.Respond("This command need place name!");
                return;
            }

            var tmparea = VStorage.CurrentSessionConfig.CollectingAreas.Where(x => x.Name == areaName);
            if (!tmparea.Any())
            {
                Context.Respond($"Area with name [{areaName}] not found!");
                return;
            }

            CollectingArea area = tmparea.First();
            if (area.WithdrawPlaces.RemoveAll(x => x.Name == witdrawPlaceName) > 0)
            {
                Context.Respond($"Withdraw place with name [{witdrawPlaceName}] in [{areaName}] collecting area reconfigured!");
            }
            else
            {
                Context.Respond($"New withdraw place with name [{witdrawPlaceName}] added to [{areaName}] collecting area!");
            }

            var place = new WithdrawPlace()
            {
                Name = witdrawPlaceName,
                Radius = withdrawRadius,
                TimeTax = timeTax,
                ConstantTax = constantTax,
                Position = new Vector3D(posX, posY, posZ)
            };
            area.WithdrawPlaces.Add(place);
        }

        [Command("remplace", "Remove withdraw place from collecting area.", "")]
        [Permission(MyPromoteLevel.Moderator)]
        public void RemoveWithdrawPlace(string areaName, string witdrawPlaceName)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (string.IsNullOrWhiteSpace(areaName))
            {
                Context.Respond("This command need area name!");
                return;
            }

            if (string.IsNullOrWhiteSpace(witdrawPlaceName))
            {
                Context.Respond("This command need place name!");
                return;
            }

            var tmparea = VStorage.CurrentSessionConfig.CollectingAreas.Where(x => x.Name == areaName);
            if (!tmparea.Any())
            {
                Context.Respond($"Area with name [{areaName}] not found!");
                return;
            }

            CollectingArea area = tmparea.First();
            if (area.WithdrawPlaces.RemoveAll(x => x.Name == witdrawPlaceName) > 0)
            {
                Context.Respond($"Withdraw place with name [{witdrawPlaceName}] in [{areaName}] collecting area removed!");
            }
            else
            {
                Context.Respond($"Withdraw place with name [{witdrawPlaceName}] not found in [{areaName}] collecting area!");
            }
        }
    }
}
