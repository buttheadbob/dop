﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Torch.API.Managers;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game.ModAPI;

namespace VStorageNs.Code.Commands
{
    public class VsCommand : CommandModule
    {
        [Command("vs", "Virtual storage entry point. Gives a list with all allowed commands.")]
        [Permission(MyPromoteLevel.None)]
        public void vshelp()
        {
            var commandManager = Context.Torch.CurrentSession?.Managers.GetManager<CommandManager>();
            if (commandManager == null)
            {
                Context.Respond("Must have an attached session to list commands");
                return;
            }

            commandManager.Commands.GetNode(new List<string>() { "vs" }, out CommandTree.CommandNode node);
            if (node != null)
            {
                var command = node.Command;
                var children = node.Subcommands.Where(e => Context.Player == null || e.Value.Command?.MinimumPromoteLevel <= Context.Player.PromoteLevel).Select(x => x.Key);
                var sb = new StringBuilder();
                if (command != null)
                {
                    sb.AppendLine($"Right syntax: {command.SyntaxHelp}");
                    sb.Append(command.HelpText);
                }

                if (node.Subcommands.Count() != 0)
                    sb.Append($"\nSubcommands: {string.Join(", ", children)}");

                Context.Respond(sb.ToString());
            }
        }
    }
}
