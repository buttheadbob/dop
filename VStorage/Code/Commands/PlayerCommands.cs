﻿using System;
using System.Linq;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game.ModAPI;

namespace VStorageNs.Code.Commands
{
    [Category("vs")]
    public class PlayerCommands : CommandModule
    {
        [Command("list", "List of your pending cells.", "")]
        [Permission(MyPromoteLevel.None)]
        public void List(int cell_id = 0)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            if (Context.Player == null)
            {
                Context.Respond($"Use from console unsupported!");
                return;
            }

            try
            {
                if (!VStorage.CurrentSessionConfig.PlayersCells.ContainsKey(Context.Player.IdentityId) || VStorage.CurrentSessionConfig.PlayersCells[Context.Player.IdentityId] == null)
                {
                    Context.Respond($"Your don't have cells.");
                    return;
                }

                if (cell_id <= 0 || VStorage.CurrentSessionConfig.PlayersCells[Context.Player.IdentityId].Count < cell_id)
                {
                    var playerCells = VStorage.CurrentSessionConfig.PlayersCells[Context.Player.IdentityId];
                    if (playerCells.Any())
                    {
                        foreach (var cell in playerCells)
                        {
                            string invcount = cell.InvItems == null ? "NULL!" : cell.InvItems.Count().ToString();
                            Context.Respond($"Cell: {cell.ID} invCount: {invcount}");
                        }
                    }
                }
                else
                {
                    VStorage.PrintCellItems(VStorage.CurrentSessionConfig.PlayersCells[Context.Player.IdentityId][--cell_id], Context);
                }
            }
            catch (Exception ex)
            {
                Context.Respond($"Exception while processing your command, contact admin.");
                string name = "Unknown";
                if (Context.Player != null)
                {
                    name = Context.Player.DisplayName;
                }
                VStoragePlugin.ToLog($"Exception in List(), player: [{name}] ex: {ex}");
            }
        }

        [Command("withdraw", "Withdraw a cell.", "")]
        [Permission(MyPromoteLevel.None)]
        public void Withdraw(int id)
        {
            if (!VStoragePlugin.CurrentPluginConfig.Enabled)
            {
                Context.Respond($"Plugin disabled!");
                return;
            }

            var sw = System.Diagnostics.Stopwatch.StartNew();
            if (Context.Player == null)
            {
                Context.Respond($"Use from console unsupported!");
                return;
            }

            try
            {
                var player = Context.Player.Character;
                if (player == null && !player.IsDead)
                {
                    Context.Respond($"Character not found, u need to be alive to recive items.");
                    return;
                }

                VStorage.WithdrawInternal(id, player, Context);
            }
            catch (Exception ex)
            {
                Context.Respond($"Exception while processing your command, contact admin.");
                string name = "Unknown";
                if (Context.Player != null)
                {
                    name = Context.Player.DisplayName;
                }
                VStoragePlugin.ToLog($"Exception in Withdraw(), player: [{name}] ex: {ex}");
            }
            finally
            {
                sw?.Stop();
                Context.Respond("Elapsed ms: " + sw?.ElapsedMilliseconds + Environment.NewLine);
            }
        }
    }
}
