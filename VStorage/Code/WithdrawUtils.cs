﻿using System;
using System.Collections.Generic;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using SharedLibTorch;
using VRage;
using VRage.Game;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VStorageNs.Code.Structs;

namespace VStorageNs.Code
{
    class WithdrawUtils
    {
        /// <summary>
        /// Add to all inventories with access, then overfill player inventory
        /// </summary>
        public static WithdrawResult AddToInventories(IMyPlayer collectingPlayer, Dictionary<string, double> items)
        {
            try
            {
                var cargoBlocks = new List<MyEntity>();
                var invcargoBlocks = new List<MyInventory>();
                var controllingCube = collectingPlayer.Controller.ControlledEntity as IMyCubeBlock;
                if (controllingCube != null)
                {
                    var terminalsys = MyAPIGateway.TerminalActionsHelper.GetTerminalSystemForGrid(controllingCube.CubeGrid);
                    var blocks = new List<IMyTerminalBlock>();
                    terminalsys.GetBlocksOfType<IMyCargoContainer>(blocks);

                    // Only select cargo blocks that the player has access to (Not enemy, or unshared faction).
                    foreach (IMyTerminalBlock block in blocks)
                    {
                        MyCubeBlock cubeblock = block as MyCubeBlock;
                        if (cubeblock != null)
                        {
                            MyRelationsBetweenPlayerAndBlock relation = cubeblock.GetUserRelationToOwner(collectingPlayer.Identity.IdentityId);

                            if (relation == MyRelationsBetweenPlayerAndBlock.Owner ||
                                relation == MyRelationsBetweenPlayerAndBlock.NoOwnership ||
                                relation == MyRelationsBetweenPlayerAndBlock.FactionShare)
                            {
                                var temp = cubeblock.GetInventory();
                                if (temp != null)
                                {
                                    invcargoBlocks.Add(temp);
                                    cargoBlocks.Add(cubeblock);
                                }
                            }
                        }
                    }
                }

                var playerInventory = collectingPlayer.Character.GetInventory();
                if (playerInventory == null)
                {
                    return WithdrawResult.NoPlayerInventory;
                }

                foreach (var item in items)
                {
                    MyDefinitionId.TryParse(item.Key, out MyDefinitionId definitionId);
                    var amount = (double)item.Value;
                    double space;

                    foreach (var cubeInventory in invcargoBlocks)
                    {
                        if (amount <= 0) break;

                        space = (double)cubeInventory.ComputeAmountThatFits(definitionId);
                        if (amount <= space)
                        {
                            if (InventoryUtils.InventoryAdd(cubeInventory, (MyFixedPoint)amount, definitionId, false))
                            {
                                amount = 0;
                            }
                        }
                        else
                        {
                            if (InventoryUtils.InventoryAdd(cubeInventory, (MyFixedPoint)space, definitionId, false))
                            {
                                amount -= space;
                            }
                        }
                    }

                    if (amount <= 0) continue;

                    InventoryUtils.InventoryAdd(playerInventory, (MyFixedPoint) amount, definitionId, true);
                }

                foreach (var cubeInventory in invcargoBlocks)
                {
                    InventoryUtils.SortInventory(cubeInventory);
                    cubeInventory.Refresh();
                }

                InventoryUtils.SortInventory((MyInventory)playerInventory);
                ((MyInventory)playerInventory).Refresh();

                return WithdrawResult.Ok;
            }
            catch (Exception ex)
            {
                VStoragePlugin.ToLog($"Exception in AddToInventories() {ex}", 2);
                return WithdrawResult.Exception;
            }
        }
    }
}
