﻿using Torch.API;
using Torch.Managers.PatchManager;
using NAPI;
using Slime.Features;
using Slime;
using System.Windows.Controls;
using Slime.GUI;
using System.Collections.Generic;
using System;
using Sandbox.Engine.Physics;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.ModAPI;
using Torch.Commands;
using VRage.Game.ModAPI;
using VRage.Utils;
using Freezer.Freezer.Hooks;
using Sandbox.Engine.Utils;

namespace TorchPlugin
{
    public class FreezerPlugin : CommonPlugin
    {
        public static Slime.Freezer freezer;
        public static FreezerPlugin instance;
        public static SimTracker simTracker = new SimTracker();

        public override void Init(ITorchBase torch)
        {
            base.Init(torch);
            instance = this;
            freezer = new Slime.Freezer();
            freezer.Init(torch, StoragePath);
            SlowLogic.Init();
            FrameExecutor.addFrameLogic(freezer);
            FrameExecutor.addFrameLogic(simTracker);
            FrameExecutor.addFrameLogic(new SlowLogic());
            FrameExecutor.addDelayedLogic(0, (x)=> {
                FreezerDamagePrevention.Init(freezer);
            });

            torch.GameStateChanged += Torch_GameStateChanged;
        }

        private void Torch_GameStateChanged(Sandbox.MySandboxGame game, TorchGameState newState)
        {
            if (newState == TorchGameState.Creating)
            {
                freezer.BuildMatchers();
            }
        }

        public override void Dispose()
        {
            freezer.Dispose(StoragePath);
            base.Dispose();
        }

        public override void Patch(PatchContext patchContext)
        {
            FreezerEnergyFix.Init();
            FreezerPhysics.Init(patchContext);
            FixFreezer.InitHack(patchContext);
            GameSaveHack.InitHack(patchContext);
            PhysicsTracker.Init(patchContext);
            MemoryTracker.Init();
            MyDebugDrawSettings.DEBUG_DRAW_DISABLE_TRACKTRAILS = true;
            //Hook_MyCubeGrid_RaisePhysicsChanged.Patch(patchContext);
            //Hook_MyEntity_RaisePhysicsChanged.Patch(patchContext);
            //Hook_MyShipConnector_CubeGrid_OnBodyPhysicsChanged.Patch(patchContext);
            //Hook_MyShipConnector_CubeGrid_OnPhysicsChanged.Patch(patchContext);
            //Hook_MyShipConnector_MyShipConnector_OnPhysicsChanged.Patch(patchContext);
            Hook_MyShipConnector_OnPhysicsEnabledChanged.Patch(patchContext);

            var manager = Torch.CurrentSession?.Managers.GetManager(typeof(CommandManager)) as CommandManager;
            manager.RegisterCommandModule (typeof(FreezerCommands));
            manager.RegisterCommandModule (typeof(SearchCommands));
        }

        public override UserControl CreateControl()
        {
            return new FreezerGUI() { DataContext = freezer };
        }

        public static void OnGameSave()
        {
            freezer.OnGameSave();
        }
    }
}