using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Slime.Features;
using SpaceEngineers.Game.Entities.Blocks;
using System;
using System.Collections.Generic;
using System.Reflection;
using Freezer.Freezer.Data;
using TorchPlugin;
using VRage.Network;

namespace Slime
{
    class FreezerEnergyFix
    {
        public static MethodInfo StorePower;

        public static void Init()
        {
            if (FreezerPlugin.freezer.Config.OfflinePowerProduction)
            {               
                StorePower = typeof(MyBatteryBlock).easyMethod("StorePower");
            }
        }

        public static void OnFreeze(List<MyCubeGrid> grids, FrozenGridGroup fgg)
        {

        }

        public static void OnUnFreeze(List<MyCubeGrid> grids, FrozenGridGroup fgg)
        {
            try
            {
                if (!FreezerPlugin.freezer.Config.OfflinePowerProduction) return;
                if (fgg.Timestamp <= 0) return;

                var solarMlt = FreezerPlugin.freezer.Settings.Data.SolarPanelMlt;
                var windMlt = FreezerPlugin.freezer.Settings.Data.WindTurbineMlt;
                var generated = 0f;
                var timepassed = SharpUtils.timeStamp() - fgg.Timestamp;

                
                var batts = new List<MyBatteryBlock>();
                foreach (var x in grids)
                {
                    var gi = x.GridInfo();
                    if (gi == null) continue;

                    if (gi.blocks.ContainsKey(ShipSubpart.WIND_TURBINE))
                    {
                        foreach (var y in gi.blocks[ShipSubpart.WIND_TURBINE])
                        {
                            var w = y as MyWindTurbine;
                            generated += w.SourceComp.CurrentOutput * windMlt * timepassed;
                        }
                    }

                    if (gi.blocks.ContainsKey(ShipSubpart.SOLAR_PANEL))
                    {
                        foreach (var y in gi.blocks[ShipSubpart.SOLAR_PANEL])
                        {
                            var w = y as MySolarPanel;
                            generated += w.SourceComp.CurrentOutput * solarMlt * timepassed;
                        }
                    }

                    if (gi.blocks.ContainsKey(ShipSubpart.BATTERY))
                    {
                        foreach (var y in gi.blocks[ShipSubpart.BATTERY])
                        {
                            batts.Add(y as MyBatteryBlock);
                        }
                    }
                }

                batts.Sort((a, b) =>
                {
                    var d = (b.MaxStoredPower - b.CurrentStoredPower) - (a.MaxStoredPower - a.CurrentStoredPower);
                    return d > 0 ? 1 : d < 0 ? -1 : 0;
                });

                foreach (var x in batts)
                {
                    if (generated > 0)
                    {
                        var canStore = (x.MaxStoredPower - x.CurrentStoredPower) * 3600 * 1.25f;
                        var charge = Math.Min(canStore, generated);
                        generated -= charge;

                        StorePower.Invoke(x, new object[] { 1000, charge });
                    }
                }
            }
            catch (Exception e)
            {
                Log.Fatal(e.ToString());
            }

        }
    }
}
