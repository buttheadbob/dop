using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Entities.Blocks;
using Freezer.Freezer.Data;
using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Slime.Features;
using TorchPlugin;
using VRage.Game;
using VRage.Game.Entity;
using VRage.Game.Entity.EntityComponents.Interfaces;
using VRage.Game.ModAPI;
using VRage.Groups;
using VRageMath;

namespace Slime
{
    public class FrozenInfo
    {
        private static FieldInfo cryoOccupied = typeof(MyCryoChamber).GetField("m_currentPlayerId", BindingFlags.NonPublic | BindingFlags.Instance);
        public static double CalculateRating(List<MyCubeGrid> Grids)
        {
            var r = 0.0d;
            foreach (var x in Grids)
            {
                r += x.BlocksPCU + (x.IsStatic ? 1000 : 0);
            }
            //Rating = Rating * (0.8+(Grids.Count * 0.2f));
            return r;
        }

        public long Id { get; }
        public bool IsConcealed { get; private set; }
        public BoundingBoxD WorldAABB { get; private set; }
        public List<MyCubeGrid> Grids { get; }


        public readonly int Rating = 0;
        public event Action<FrozenInfo> Closing;
        internal volatile int ProxyId = -1;
        public HashSet<long> disabledProjectors = new HashSet<long>();
        public FrozenGridGroup Info;

        public FrozenInfo(long id, List<MyCubeGrid> grids, BoundingBoxD worldAABB, FrozenGridGroup info)
        {
            Id = id;
            Grids = grids;
            WorldAABB = worldAABB;
            Rating = (int)CalculateRating(Grids);
            Info = info;
            if (Info == null)
            {
                Info = new FrozenGridGroup();
                Info.Timestamp = SharpUtils.timeStamp();
                Info.Grids = new List<FrozenGridInfo>();
                foreach (var g in grids)
                {
                    var fi = new FrozenGridInfo();
                    fi.EntityId = g.EntityId;
                    fi.Speed = g.Physics?.LinearVelocity ?? Vector3.Zero;
                    fi.Aspeed = g.Physics?.AngularVelocity ?? Vector3.Zero;
                    Info.Grids.Add(fi);
                    bool profiling = FreezerPlugin.freezer.Config.Profiling;
                    if (profiling)
                    {
                        Log.Warn($"---SaveSpeed grid:[{g.DisplayName}] speed: [{fi.Speed.ToString()}], [{fi.Aspeed}]");
                    }
                }
            }
        }

        public static void CalculateIdAndAABB(List<MyCubeGrid> grids, out long maxId, out BoundingBoxD worldAABB)
        {
            bool first = true;
            maxId = 0;
            worldAABB = new BoundingBoxD();
            foreach (var grid in grids)
            {
                maxId = Math.Max(grid.EntityId, maxId);
                if (grid.PositionComp != null)
                {
                    if (first)
                    {
                        worldAABB = grid.PositionComp.WorldAABB;
                        first = false;
                    }
                    else
                    {
                        worldAABB.Include(grid.PositionComp.WorldAABB);
                    }
                }
            }
        }


        public void StopGrids()
        {
            foreach (var x in Grids)
            {
                if (!x.IsStatic)
                {
                    x.Physics?.SetSpeeds(Vector3.Zero, Vector3.Zero);
                }
            }
        }



        public bool ShouldSkipFreeze(bool ConcealPirates, bool ConcealProduction, float dontFreezeGravity, int dontFreezeProductionMaximumPCU, int DelayLastOnline, bool FreezeNPC, bool FreezeStaticNPC, bool FreezeSignals)
        {

            if (dontFreezeGravity > 0 && Grids[0].Physics != null)
            {
                var g = Grids[0].Physics.Gravity.Length();
                if (g > 0 && g < dontFreezeGravity)
                {
                    return true;
                }
            }

            if (!ConcealPirates)
            {
                var pirateId = MyPirateAntennas.GetPiratesId();
                foreach (var grid in Grids)
                {
                    if (grid.BigOwners.Contains(pirateId)) { return true; }
                }
            }

            if (ShouldSkipFreeze_NpcGrid(FreezeNPC, FreezeStaticNPC))
            {
                return true;
            }

            if (FreezeSignals && ShouldSkipFreeze_SignalsInGravity())
            {
                return true;
            }

            bool hasProduction = false;
            bool hasBlockingPlayer = false;
            bool hasStaticGrid = false;

            foreach (var grid in Grids)
            {
                hasStaticGrid |= grid.IsStatic;
                var tt = grid.GridInfo();
                if (tt == null)
                {
                    Log.LogSpamming((27345723), (builder, i) => builder.Append("Freezer: GridInfo is null:").Append(grid));
                    return true; // WRONG STATE?
                }

                if (tt.IsExcluded())
                {
                    return true;
                }
                //if (tt.HasWorkingProduction())
                if (!hasProduction)
                {
                    hasProduction = tt.HasWorkingProduction();
                    if (hasProduction && !ConcealProduction) { return true; }
                }

                if (DelayLastOnline >= 0)
                {
                    if (!hasStaticGrid && !hasBlockingPlayer)
                    {
                        if (HasBlockingPilot(tt, DelayLastOnline))
                        {
                            hasBlockingPlayer = true;
                        }
                    }
                }
            }

            if (hasBlockingPlayer && !hasStaticGrid)
            {
                return true;
            }

            if (hasProduction && Rating <= dontFreezeProductionMaximumPCU)
            {
                return true;
            }
            return false;
        }


        private bool ShouldSkipFreeze_SignalsInGravity()
        {
            if (Grids.Count != 1) return false;

            var grid = Grids[0];
            if (grid.GridSizeEnum == MyCubeSize.Small && grid.Physics != null && grid.Physics.Gravity.LengthSquared() > 0 && grid.DisplayName.Contains("Container MK"))
            {
                return true;
            }

            return false;
        }

        private bool HasBlockingPilot(ShipSubpart tt, int DelayLastOnline)
        {
            if (!tt.blocks.TryGetValue(ShipSubpart.COCKPIT, out var blocks))
            {
                return false;
            }

            foreach (var x in blocks)
            {
                var cock = (x as MyCockpit);
                if (cock.Pilot == null) continue;

                var ident = cock.Pilot.GetIdentity();
                if (Sync.Players.IsPlayerOnline(ident.IdentityId)) return true;

                if (DelayLastOnline == 0) return true;
                else
                {
                    if (ident.LastLoginTime > ident.LastLogoutTime)
                    {
                        return true; //Player is online
                    }
                    if ((DateTime.Now - ident.LastLogoutTime).TotalSeconds < DelayLastOnline)
                    {
                        return true; //Player is offline, but left, not so long time ago
                    }
                }
            }

            return false;
        }

        public bool ShouldSkipFreeze_NpcGrid(bool FreezeNPCgrids, bool FreezeStaticNPCgrids)
        {
            if (FreezeNPCgrids && FreezeStaticNPCgrids)
            {
                return false;
            }

            foreach (var grid in Grids)
            {
                if (grid.BigOwners == null || grid.BigOwners.Count == 0 || grid.BigOwners.Count > 1)
                {
                    return false;
                }

                if (grid.IsStatic)
                {
                    if (FreezeStaticNPCgrids) return false;
                }
                else
                {
                    if (FreezeNPCgrids) return false;
                }

                if (!MySession.Static.Players.IdentityIsNpc(grid.BigOwners[0]))
                {
                    return false;
                }
            }

            return true;
        }

        public void UpdatePostConceal()
        {

        }

        public void UpdatePostReveal()
        {
            IsConcealed = false;
            foreach (var grid in Grids)
            {
                grid.OnMarkForClose -= Grid_OnMarkForClose;
            }
        }

        private void Grid_OnMarkForClose(MyEntity obj)
        {
            EnableProjectors();
            foreach (var grid in Grids)
            {
                grid.OnMarkForClose -= Grid_OnMarkForClose;
            }
            Closing?.Invoke(this);
        }

        public void UpdateAABB()
        {
            var startPos = Grids[0].PositionComp.GetPosition();
            var box = new BoundingBoxD(startPos, startPos);

            foreach (var aabb in Grids.Select(g => g.PositionComp.WorldAABB)) box.Include(aabb);

            WorldAABB = box;
        }

        public bool IsCryoOccupied(ulong steamId)
        {
            try
            {
                foreach (var g in Grids)
                {
                    var gi = g.GridInfo();

                    if (gi == null || !gi.blocks.ContainsKey(ShipSubpart.CRYO)) continue;

                    foreach (var c in gi.blocks[ShipSubpart.CRYO])
                    {
                        if (c == null) continue;
                        var value = (MyPlayer.PlayerId?)cryoOccupied.GetValue(c);
                        if (value?.SteamId == steamId) return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                Log.LogSpamming(472766168, (x, a) => x.Append("IsCryoOccupiedError:" + e));
                return false;
            }
        }

        private void DisableProjectors(MyCubeGrid grid)
        {
            var gi = grid.GridInfo();
            var projectors = gi.blocks.GetOr(ShipSubpart.PROJECTOR, null);
            if (projectors != null)
            {
                foreach (var projector in projectors)
                {
                    if (!(projector is MyProjectorBase proj)) continue;

                    if (proj.Enabled)
                    {
                        proj.Enabled = false;
                        if (!disabledProjectors.Contains(proj.EntityId))
                            disabledProjectors.Add(proj.EntityId);
                    }
                }
            }
        }

        public void EnableProjectors()
        {
            if (disabledProjectors == null) return;
            foreach (var projector in disabledProjectors)
            {
                var x = projector.As<MySpaceProjector>();
                if (x == null) continue;
                x.Enabled = true;
            }

            disabledProjectors.Clear();
        }

        public void Conceal()
        {
            IsConcealed = true;
            foreach (var grid in Grids)
            {
                grid.setFrozen();
            }

            foreach (var grid in Grids)
            {
                DisableProjectors(grid);
                if (grid.Parent == null) UnregisterRecursive(grid);
            }
            FreezerPhysics.FreezeGroup(Grids, Info);

            FreezerEnergyFix.OnFreeze(Grids, Info);

            if (FreezerPlugin.freezer.Config.StopGrids)
            {
                StopGrids();
            }

            UpdateAABB();
            foreach (var grid in Grids)
            {
                grid.OnMarkForClose += Grid_OnMarkForClose;
            }
        }

        public void Reveal()
        {
            FreezerPhysics.UnFreezeGroup(Grids, Info);

            foreach (var grid in Grids)
            {
                if (grid.Parent == null) RegisterRecursive(grid);
            }
            FreezerEnergyFix.OnUnFreeze(Grids, Info);
            EnableProjectors();
            foreach (var grid in Grids)
            {
                grid.setUnFrozen();
            }
            }

        public void RevealOnServerStart()
        {
            FreezerPhysics.UnFreezeGroup(Grids, Info);
            FreezerEnergyFix.OnUnFreeze(Grids, Info);
        }


        private void UnregisterRecursive(MyEntity e)
        {
            if (e.IsPreview) return;

            MyEntities.UnregisterForUpdate(e);
            (e.GameLogic as IMyGameLogicComponent)?.UnregisterForUpdate();
            e.setFrozen();
            if (e.Hierarchy == null) return;

            foreach (var child in e.Hierarchy.Children) UnregisterRecursive((MyEntity)child.Container.Entity);
        }

        private void RegisterRecursive(MyEntity e)
        {
            if (e.IsPreview) return;

            MyEntities.RegisterForUpdate(e);
            (e.GameLogic as IMyGameLogicComponent)?.RegisterForUpdate();
          //  e.setUnFrozen();
            if (e.Hierarchy == null) return;

            foreach (var child in e.Hierarchy.Children) RegisterRecursive((MyEntity)child.Container.Entity);
        }
    }
}