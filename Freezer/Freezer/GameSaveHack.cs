﻿using System;
using System.Reflection;
using Sandbox.Game.Entities;
using Sandbox.Game.World.Generator;
using Torch.Managers.PatchManager;
using NAPI;
using Sandbox.Game.World;
using TorchPlugin;

namespace Slime
{
    internal class GameSaveHack
    {
        private static MethodInfo TrackEntity;
        public static void InitHack(PatchContext patchContext)
        {
            patchContext.Prefix(typeof(MySession), typeof(GameSaveHack), "Save", new []{ "snapshot", "customSaveName" });
        }

        private static bool Save(MySession __instance, ref MySessionSnapshot snapshot, ref String customSaveName)
        {
            FreezerPlugin.OnGameSave();
            return true;
        }
    }
}