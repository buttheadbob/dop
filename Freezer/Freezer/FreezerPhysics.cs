using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Game.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using Torch.Commands;
using Sandbox.Game.World;
using VRage.Game.ModAPI;
using Sandbox.ModAPI;
using Torch.Commands.Permissions;
using NAPI;
using Sandbox.Engine.Physics;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Scripts.Shared;
using VRage.ModAPI;
using VRageMath;
using VRageMath.Spatial;
using Sandbox.ModAPI;
using Torch;
using System.Linq;
using Sandbox.Engine.Multiplayer;
using VRage.Network;
using Sandbox;
using Sandbox.Game;
using System.Linq.Expressions;
using Freezer.Freezer.Data;
using Sandbox.Game.Entities.Blocks;
using TorchPlugin;
using Sandbox.Definitions;
using Torch.Managers.PatchManager;
using VRage.Game.Components;
using SpaceEngineers.Game.Entities.Blocks;

namespace Slime
{
    public class FreezerPhysics
    {
        public static Guid PhysFreezeGuid = new Guid("2ace9a8c-25d0-4205-80b6-cdf5435f2846");
        private static readonly FieldInfo m_MyLandingGear_m_needsToRetryLock = typeof(MyLandingGear).GetField("m_needsToRetryLock", BindingFlags.NonPublic | BindingFlags.Instance);
        private static readonly MethodInfo m_MyLandingGear_RetryLock = typeof(MyLandingGear).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x1 => x1.Name == "RetryLock").First();
       // private static readonly MethodInfo m_MyMechanicalConnectionBlockBase_Reattach = typeof(MyMechanicalConnectionBlockBase).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x1 => x1.Name == "Reattach").First();
       // private static readonly MethodInfo m_MyMechanicalConnectionBlockBase_UpdateAttachState = typeof(MyMechanicalConnectionBlockBase).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x1 => x1.Name == "UpdateAttachState").First();
        private static readonly MethodInfo m_MyMechanicalConnectionBlockBase_RefreshConstraint = typeof(MyMechanicalConnectionBlockBase).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x1 => x1.Name == "RefreshConstraint").First();
       
        


        private static readonly Type typeLanding = typeof(MyLandingGear);
        private static readonly Type typeMyMotorSuspension = typeof(MyMotorSuspension);

        public static void Init(PatchContext patchContext)
        {
            if (!FreezerPlugin.freezer.Config.FreezePhysicsEnabled) return;

            patchContext.Prefix(typeof(MyGridPhysics), typeof(FreezerPhysics), "AddForce");
        }

        public static bool AddForce(MyGridPhysics __instance, MyPhysicsForceType type,
            Vector3? force,
            Vector3D? position,
            Vector3? torque,
            float? maxSpeed = null,
            bool applyImmediately = true,
            bool activeOnly = false)
        {
            if (activeOnly)
            {
                return true;
            }
            else
            {
                if (__instance.Entity.isFrozen())
                {
                    if (FreezerPlugin.freezer.Config.Profiling)
                    {
                        Log.LogSpamming(37747, (builder, i) => { builder.Append($"AddForce from: {new StackTrace()}"); });
                    }
                    return false;
                }
                return true;
            }
        }



        internal static void FreezeGroup(List<MyCubeGrid> grids, FrozenGridGroup info)
        {
            if (!FreezerPlugin.freezer.Config.FreezePhysicsEnabled) return;

            if (info.Grids == null)
            {
                info.Grids = new List<FrozenGridInfo>();
                bool profiling = FreezerPlugin.freezer.Config.Profiling;
                if (profiling)
                {
                    Log.Warn("(dbg)FreezeGroup: info.Grids == null!");
                }
            }
            foreach (var g in grids)
            {
                if (!g.IsStatic && g.Physics != null)
                {
                    g.Physics.Gravity = Vector3.Zero;
                    g.Physics.ClearSpeed();
                }
            }

            foreach (var g in grids)
            {
                if (!g.IsStatic && g.Physics != null)
                {
                    // g.Physics.Deactivate(g.Physics.HavokWorld);
                    g.Physics.Enabled = false;
                }
            }
        }

        internal static void UnFreezeGroup(List<MyCubeGrid> grids, FrozenGridGroup info)
        {
            if (!FreezerPlugin.freezer.Config.FreezePhysicsEnabled) return;
            foreach (var x in grids)
            {
                if (x.Physics == null)
                {
                    Log.Warn("Physics == null! [" + x.EntityId);
                    continue;
                }
                //if (x.Physics.RigidBody != null)
                // x.Physics.RigidBody.IsActive = true;
                x.Physics.Enabled = true;
            }
            foreach (var x in grids)
            {
                var gi = x.GridInfo();
                if (gi == null) continue;

                if (gi.blocks.ContainsKey(typeLanding))
                {
                    foreach (var block in gi.blocks[typeLanding])
                    {
                        var obj = block as MyLandingGear;
                        // Log.Warn("m_MyLandingGear_RetryLock Invoke ib game thread:");
                        obj.RequestLock(false);
                        //TODO test it more/refactor
                        MySandboxGame.Static.Invoke(() =>
                    {
                        try
                        {
                            // m_MyLandingGear_m_needsToRetryLock.SetValue(obj, true);
                            // m_MyLandingGear_RetryLock.Invoke(obj, null); //не помогло
                            obj.RequestLock(true); //возможно можно и не инвокать, не тестил
                        }
                        catch
                        {
                            MySandboxGame.Log.Log(VRage.Utils.MyLogSeverity.Error, "FreezerPlugin: Fail to invoke Connector Weld");
                        }
                    }
                    , "FreezerPlugin");

                    }
                }

                if (gi.blocks.ContainsKey(typeMyMotorSuspension))
                {//x.GridSizeEnum == VRage.Game.MyCubeSize.Large 
                    //Whell code is spagetti, cant find whal wrong
                    //MarkForReattach
                    foreach (var block in gi.blocks[typeMyMotorSuspension])
                    {
                        var obj = block as MyMotorSuspension;
                        // Log.Warn("m_MyLandingGear_RetryLock Invoke ib game thread:");
                        // obj.MarkForReattach();
                         m_MyMechanicalConnectionBlockBase_RefreshConstraint.Invoke(obj, null);
                        MySandboxGame.Static.Invoke(() =>
                        {
                            try
                            {
                                // m_MyLandingGear_m_needsToRetryLock.SetValue(obj, true);
                                // m_MyLandingGear_RetryLock.Invoke(obj, null); //не помогло
                                //   obj.RequestLock(true); //возможно можно и не инвокать, не тестил

                               
                            }
                            catch
                            {
                                MySandboxGame.Log.Log(VRage.Utils.MyLogSeverity.Error, "FreezerPlugin: Fail to invoke Connector Weld");
                            }
                        }
                    , "FreezerPlugin");

                    }
                }
            }
            foreach (var x in grids)
            {
                if (info.Grids == null) continue;
                /*foreach (var block in x.GetBlocks())
                {
                    var conn = block?.FatBlock as MyShipConnector;
                    if (conn != null)
                    {
                        //conn.UpdateOnceBeforeFrame();
                        // var other = conn.Other.EntityId;
                        // conn.Detach(false);
                        // conn.TryConnect();
                       // MethodInfo dynMethod = conn.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x1 => x1.Name == "Weld").First() ;
                       //  dynMethod.Invoke(conn, null);
                    }
                }*/
                var gridInfo = info.Grids.Find(g => g.EntityId == x.EntityId);
                if (gridInfo != null)
                {
                    bool profiling = FreezerPlugin.freezer.Config.Profiling;
                    if (profiling)
                    {
                        Log.Warn($"+++SetSpeed grid:[{x.DisplayName}] speed: [{gridInfo.Speed}], [{gridInfo.Aspeed}] ");
                    }
                    x.Physics.SetSpeeds(gridInfo.Speed, gridInfo.Aspeed);
                }
            }
        }
    }
}
