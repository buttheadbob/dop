﻿using System.Collections.Generic;
using ProtoBuf;
using VRageMath;

namespace Freezer.Freezer.Data
{
    [ProtoContract]
    public class FrozenGrids
    {
        [ProtoMember(1)]
        public Dictionary<long, FrozenGridGroup> GridGroups;
    }
    
    [ProtoContract]
    public class FrozenGridGroup
    {
        [ProtoMember(1)]
        public List<FrozenGridInfo> Grids;
        
        [ProtoMember(2)]
        public long Timestamp;
    }
    
    [ProtoContract]
    public class FrozenGridInfo
    {
        [ProtoMember(1)]
        public long EntityId;
        [ProtoMember(2)]
        public Vector3 Speed;
        [ProtoMember(3)]
        public Vector3 Aspeed;
    }
}