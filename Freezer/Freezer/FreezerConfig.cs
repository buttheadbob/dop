using Torch;

namespace Slime.Z
{

    public class FreezerConfig : ViewModel
    {
        private bool _enabled = true;
        private bool _enabledLogs = true;

        private bool _enabledSlowdownTurretTurnOff = false;
        private bool _enabledSlowdownTurretMod = false;
        private bool _enabledSlowdownTurretActivation = false;
        private int _enabledSlowdownTurretTurnOffRadius = 3000;
        private int _enabledSlowdownTurretTurnOffInterval = 500;

        private bool _loadbalancer = true;
        private bool _freezeProduction;
        
        private bool _freezePirates;
        private bool _freezeNpc;
        private bool _freezeStaticNpc;
        private bool _freezeSignals;
        
        private bool m_frozenGridsDontReceiveDamage = false;
        private bool _convertToStations;

        private bool _freezePhysicsEnabled;
        private bool _freezePhysicsHooksEnabled;

        private bool _stopGrids;
        private bool _useUnfreezePriority;
        private bool _useFreezePriority;
        private bool _profiling;

        private int _dontFreezeProductionMaximumPCU = -1;

        private double _freezeDistance = 10000;
        private double _unfreezeDistance = 7000;

        private float _dontFreezeGravity = -1f;


        private float _windTurbineMlt = 1f;
        private float _solarPanelMlt = 0.5f;

        private int _freezeInterval = 20;
        private bool _freezeAllOnStart = false;

        private bool _offlinePowerProduction = false;
        private int _unfreezeInterval = 20;
        private int _batchAmount = 3;
        private int _maxBatteryDeltaMs = 30000;
        private int _workDelayMs = 5000;
        
        private string _excludedSubtypes = "";
        private string _excludedWorkingSubtypes = "";

        private int _delayAfterLogout = -1;
        
        float _optimalMinLoad = 70;
        float _optimalMaxLoad = 85;
        int _downwardSteps = 1;
        int _upwardSteps = 30;
        int _downwardInterval = 10;

        public string ExcludedSubtypes { get => _excludedSubtypes; set { SetValue(ref _excludedSubtypes, value); } }
        public string ExcludedWorkingSubtypes { get => _excludedWorkingSubtypes; set { SetValue(ref _excludedWorkingSubtypes, value); } }

        public bool Enabled { get => _enabled; set { SetValue(ref _enabled, value); } }
        public bool FreezeAllOnStart { get => _freezeAllOnStart; set { SetValue(ref _freezeAllOnStart, value); } }
        
        public bool LoadBalancer { get => _loadbalancer; set { SetValue(ref _loadbalancer, value); } }
        public float OptimalMinLoad { get => _optimalMinLoad; set { SetValue(ref _optimalMinLoad, value); } }
        public float OptimalMaxLoad { get => _optimalMaxLoad; set { SetValue(ref _optimalMaxLoad, value); } }
        public int DownwardSteps { get => _downwardSteps; set { SetValue(ref _downwardSteps, value); } }
        public int UpwardSteps { get => _upwardSteps; set { SetValue(ref _upwardSteps, value); } }

        public int DownwardInterval { get => _downwardInterval; set => SetValue(ref _downwardInterval, value); }

        public bool UseUnfreezePriority { get => _useUnfreezePriority; set { SetValue(ref _useUnfreezePriority, value); } }
        public bool UseFreezePriority { get => _useFreezePriority; set { SetValue(ref _useFreezePriority, value); } }
        public bool Profiling { get => _profiling; set { SetValue(ref _profiling, value); } }



        public int BatchAmount { get => _batchAmount; set { SetValue(ref _batchAmount, value); } }
        public int WorkDelayMs { get => _workDelayMs; set { SetValue(ref _workDelayMs, value); } }

        public int FreezeInterval { get => _freezeInterval; set { SetValue(ref _freezeInterval, value); } }
        public int UnFreezeInterval { get => _unfreezeInterval; set { SetValue(ref _unfreezeInterval, value); } }
        public double FreezeDistance { get => _freezeDistance; set { SetValue(ref _freezeDistance, value); } }
        public double UnFreezeDistance { get => _unfreezeDistance; set { SetValue(ref _unfreezeDistance, value); } }
        public bool FreezeProduction { get => _freezeProduction; set { SetValue(ref _freezeProduction, value); } }
        public bool FreezePirates { get => _freezePirates; set { SetValue(ref _freezePirates, value); } }

        public bool FreezePhysicsEnabled { get => _freezePhysicsEnabled; set { SetValue(ref _freezePhysicsEnabled, value); } }
        public bool FreezePhysicsHooksEnabled { get => _freezePhysicsHooksEnabled; set { SetValue(ref _freezePhysicsHooksEnabled, value); } }
        
        public bool FreezeNPC { get => _freezeNpc; set { SetValue(ref _freezeNpc, value); } }
        public bool FreezeStaticNPC { get => _freezeStaticNpc; set { SetValue(ref _freezeStaticNpc, value); } }
        
        public bool FreezeSignals { get => _freezeSignals; set { SetValue(ref _freezeSignals, value); } }

        public bool FrozenGridsDontReceiveDamage { get => m_frozenGridsDontReceiveDamage; set { SetValue(ref m_frozenGridsDontReceiveDamage, value); } }

        public int DontFreezeProductionMaximumPCU { get => _dontFreezeProductionMaximumPCU; set { SetValue(ref _dontFreezeProductionMaximumPCU, value); } }

        public float DontFreezeInGravity
        {
            get => _dontFreezeGravity;
            set
            {
                var v = value <= 0f ? -1f : value;
                SetValue(ref _dontFreezeGravity, v);
            }
        }

        public bool ConvertToStations { get => _convertToStations; set { SetValue(ref _convertToStations, value); } }
        public bool StopGrids { get => _stopGrids; set { SetValue(ref _stopGrids, value); } }

        public int MaxBatteryDeltaMs { get => _maxBatteryDeltaMs; set { SetValue(ref _maxBatteryDeltaMs, value); } }

        public bool OfflinePowerProduction
        {
            get => _offlinePowerProduction;
            set
            {
                SetValue(ref _offlinePowerProduction, value);
            }
        }

        public float WindTurbineMlt
        {
            get => _windTurbineMlt;
            set
            {
                var v = value <= 0f ? -1f : value;
                SetValue(ref _windTurbineMlt, v);
            }
        }

        public float SolarPanelMlt
        {
            get => _solarPanelMlt;
            set
            {
                var v = value <= 0f ? -1f : value;
                SetValue(ref _solarPanelMlt, v);
            }
        }
        
        public bool EnabledSlowdownTurretTurnOff { get => _enabledSlowdownTurretTurnOff; set => SetValue(ref _enabledSlowdownTurretTurnOff, value); }
        public bool EnabledSlowdownTurretSaveState { get => _enabledSlowdownTurretMod; set => SetValue(ref _enabledSlowdownTurretMod, value); }
        public bool EnabledSlowdownTurretSendSignal { get => _enabledSlowdownTurretActivation; set => SetValue(ref _enabledSlowdownTurretActivation, value); }
        public int EnabledSlowdownTurretTurnOffRadius { get => _enabledSlowdownTurretTurnOffRadius; set => SetValue(ref _enabledSlowdownTurretTurnOffRadius, value); }
        public int EnabledSlowdownTurretTurnOffInterval{ get => _enabledSlowdownTurretTurnOffInterval; set => SetValue(ref _enabledSlowdownTurretTurnOffInterval, value); }

        public int DelayAfterLogout { get => _delayAfterLogout; set { SetValue(ref _delayAfterLogout, value); } }
    }
}