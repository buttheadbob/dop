﻿using Sandbox.Game.Entities;
using Slime.Features;
using System.Collections.Generic;

namespace NAPI
{
    public static class Ext3
    {
        public static ShipSubpart GridInfo(this MyCubeGrid grid) { 
            return SlowLogic.refsShips.GetValueOrDefault(grid, null) as ShipSubpart; 
        }
    }
}
