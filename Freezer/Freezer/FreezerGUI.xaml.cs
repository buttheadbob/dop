#region

using System;
using System.Windows;
using System.Windows.Controls;
using TorchPlugin;

#endregion

namespace Slime.GUI
{
	public partial class FreezerGUI : UserControl {
        public FreezerGUI() { InitializeComponent(); }

        private Freezer Plugin => (Freezer) DataContext;
        private void Run(Action a) { FreezerPlugin.InvokeTorch(a); }

        private void UnfreezeSome_OnClick(object sender, RoutedEventArgs e) {
            var p = Plugin;
            Run(delegate { p.RevealAll(p.Settings.Data.BatchAmount); });
        }

        private void FreezeSome_OnClick(object sender, RoutedEventArgs e) {
            var p = Plugin;
            Run(delegate { p.ConcealGrids(p.Settings.Data.FreezeDistance, p.Settings.Data.BatchAmount); });
        }

        private void UnfreezeAll_OnClick(object sender, RoutedEventArgs e) {
            var p = Plugin;
            Run(delegate { p.RevealAll(); });
        }

        private void FreezeAll_OnClick(object sender, RoutedEventArgs e) {
            var p = Plugin;
            Run(delegate { p.ConcealGrids(p.Settings.Data.FreezeDistance, 0); });
        }

        private void RefreshGui_OnIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //RefreshGui.IsChecked
        }
    }
}