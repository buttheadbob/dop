using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;
using Freezer.Freezer.Data;
using NAPI;
using NLog;
using Sandbox.Engine.Multiplayer;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using SharedLib;
using SharedLibTorch;
using Slime.GUI;
using Slime.Z;
using Torch;
using Torch.API;
using Torch.Managers;
using Torch.Managers.PatchManager;
using TorchPlugin;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Groups;
using VRage.ModAPI;
using VRage.Utils;
using VRageMath;
using ILogger = NLog.ILogger;

namespace Slime
{
    public sealed class Freezer : Action1<long>
    {
        static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        public Persistent<FreezerConfig> Settings { get; private set; }
        public FreezerConfig Config { get => Settings.Data; }
        public readonly Dictionary<long, FrozenInfo> Concealed = new Dictionary<long, FrozenInfo>();

        private ulong _counter;
        private bool _init;
        private readonly List<FrozenInfo> buffer = new List<FrozenInfo>();
        private MyDynamicAABBTreeD _concealedAabbTree;

        private static List<FrozenInfo> _freezableGridGroups = new List<FrozenInfo>();
        private static List<BoundingSphereD> m_cacheList2 = new List<BoundingSphereD>();
        private static ThreadLocal<ObjectPool<List<MyCubeGrid>>> m_grids_pool = new ThreadLocal<ObjectPool<List<MyCubeGrid>>>(() =>
        {
            return new ObjectPool<List<MyCubeGrid>>(() => new List<MyCubeGrid>(50), (x) => x.Clear());
        });


        private static VariableStorage<FrozenGrids> FrozenGrids = new VariableStorage<FrozenGrids>("MIG.Freezer.FrozenGrids");

        private bool _settingsChanged;
        private bool _ready;
        private bool _wasready = false;

        public BlockIdMatcher DontFreezeWithWorking;
        public BlockIdMatcher DontFreezeWithBlocks;

        private ITorchBase Torch;

        internal void BuildMatchers()
        {
            DontFreezeWithBlocks = new BlockIdMatcher(Settings.Data.ExcludedSubtypes ?? "");
            DontFreezeWithWorking = new BlockIdMatcher(Settings.Data.ExcludedWorkingSubtypes ?? "");
        }

        public void Init(ITorchBase torch, string StoragePath)
        {
            this.Torch = torch;
            try
            {
                Settings = Persistent<FreezerConfig>.Load(Path.Combine(StoragePath, "MIG-Freezer.cfg"));
            }
            catch (Exception e)
            {
                Log.Error(e);
            }

            if (Settings?.Data == null) Settings = new Persistent<FreezerConfig>(Path.Combine(StoragePath, "MIG-Freezer.cfg"), new FreezerConfig());
            Settings.Data.PropertyChanged += Data_PropertyChanged;
            _concealedAabbTree = new MyDynamicAABBTreeD(MyConstants.GAME_PRUNING_STRUCTURE_AABB_EXTENSION);
        }

        public void Dispose(string StoragePath)
        {
            Settings.Save(Path.Combine(StoragePath, "MIG-Freezer.cfg"));
        }

        private void Data_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            _settingsChanged = true;
        }



        //TODO: divide conceal/reveal runs over several ticks to avoid stuttering.

        public void run(long frame)
        {
            if (MyAPIGateway.Session == null || !Config.Enabled) return;

            Init();

            if (_ready && !_wasready)
            {
                _wasready = true;
                if (Config.FreezeAllOnStart)
                    ConcealGrids();
            }

            if (_ready)
            {
                if (Config.LoadBalancer)
                {
                    if (_counter % (ulong)Config.UnFreezeInterval == 0)
                    {
                        RevealGrids(Config.UnFreezeDistance);
                    }

                    var load = FreezerPlugin.simTracker.avrg; // 0 -> very good, 100 -> very bad
                    if (load < Config.OptimalMinLoad) // good sim
                    {
                        if (Concealed.Count > 0)
                        {
                            if (_counter % (ulong)Config.DownwardInterval == 0)
                            {
                                RevealAll(Config.DownwardSteps);
                            }
                        }
                    }
                    else if (load < Config.OptimalMaxLoad) // optimal sim
                    {
                        // pass
                    }
                    else if (load < 200) // bad sim
                    {
                        ConcealGrids(Config.FreezeDistance, Config.UpwardSteps);
                    }
                    else // very bad sim
                    {
                        ConcealGrids(Config.FreezeDistance, 999999);
                    }
                }
                else
                {
                    if (_counter % (ulong)Config.FreezeInterval == 0)
                    {
                        ConcealGrids(Config.FreezeDistance, Config.BatchAmount);
                    }

                    if (_counter % (ulong)Config.UnFreezeInterval == 1)
                    {
                        RevealGrids(Config.UnFreezeDistance);
                    }
                }

                _counter += 1;

                UpdateGui();
            }
        }


        public void UpdateGui()
        {
            if (_counter % 300 == 0)
            {
                try
                {
                    var frozen = Concealed.Count;
                    var total = MyCubeGridGroups.Static.Physical.Groups.Count;
                    FreezerPlugin.instance.UpdateUI((x) =>
                    {
                        var gui = x as FreezerGUI;
                        gui.FreezerInfo.Text = $"Frozen: {frozen} / {total} | {FreezerPlugin.simTracker.avrg:N2}% | Phys: {FreezerPlugin.simTracker.avrgPhysics:N2} | NET Memory: {MemoryTracker.lastGC / 1024 / 1024} / {MemoryTracker.now / 1024 / 1024} mb, GCCounter: {MemoryTracker.GCCounter}, Mb/S = {60 * MemoryTracker.memoryPerTick / 1024} kb";
                    });
                }
                catch (Exception e) { Log.Error(e, "WTF?"); }
            }
        }

        public void Init()
        {
            if (_init || MyAPIGateway.TerminalControls == null) return;

            var gridGroups = FrozenGrids.Data?.GridGroups;
            if (gridGroups != null)
            {
                foreach (var g in gridGroups)
                {
                    var list = new List<MyCubeGrid>();
                    foreach (var grid in g.Value.Grids)
                    {
                        var gg = grid.EntityId.As<MyCubeGrid>();
                        if (gg != null)
                        {
                            list.Add(gg);
                        }
                    }

                    FrozenInfo.CalculateIdAndAABB(list, out var Id, out var aabb);
                    var fi = new FrozenInfo(Id, list, aabb, g.Value);
                    fi.RevealOnServerStart();
                }
            }

            Log.Warn($"Freezer [{Environment.Version}] will work in {Settings.Data.WorkDelayMs} ms");
            var delayTimer = new System.Timers.Timer { AutoReset = false, Interval = Settings.Data.WorkDelayMs };
            delayTimer.Elapsed += (sender, args) =>
            {
                Log.Warn("Freezer started work");
                _ready = true;
            };
            delayTimer.Start();

            //MySession.Static.Players.
            //MyMultiplayer.Static.ClientJoined += RevealCryoPod;

            Sync.Clients.ClientAdded += RevealCryoSteamId;
            _init = true;
        }

        public void RevealCryoSteamId(ulong player)
        {
            Log.Info($"RevealCryoSteamId: {player}");
            Torch.Invoke(() =>
            {
                try
                {
                    var list = new List<FrozenInfo>(Concealed.Values);
                    for (var i = 0; i < list.Count; i++)
                    {
                        var g = list[i];
                        if (g.IsCryoOccupied(player))
                        {
                            Log.Info($"RevealCryoSteamId: found : {player}");
                            RevealGroup(g);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.Error("Freezer" + e);
                }
            });
        }


        private int Freeze(FrozenInfo group, int index)
        {
            if (Concealed.ContainsKey(group.Id))
                return 0;

            group.Conceal();
            //group.UpdateAABB();
            var aabb = group.WorldAABB;

            group.ProxyId = _concealedAabbTree.AddProxy(ref aabb, group, 0);
            group.Closing += Group_Closing;
            group.UpdatePostConceal();

            Concealed.Add(group.Id, group);
            return group.Grids.Count;
        }

        private void Group_Closing(FrozenInfo group) { RevealGroup(group, true); }

        public int RevealGroup(FrozenInfo group, bool closing = false)
        {
            if (!group.IsConcealed) { return 0; }

            if (FreezerPlugin.freezer.Config.Profiling)
            {
                string str = $"UnFreezing group: {Environment.NewLine}[{group.Grids.Select(xs => xs.EntityId.ToString()).Aggregate((string j, string k) => j + "," + k)}]{Environment.NewLine} ({group.Id})";
                Log.Warn(str);
            }
            var count = group.Grids.Count;
            group.Reveal();

            Concealed.Remove(group.Id);

            _concealedAabbTree.RemoveProxy(group.ProxyId);
            group.UpdatePostReveal();
            return count;
        }

        public int RevealGrids(double distanceFromPlayers)
        {
            var revealed = 0;
            var playerSpheres = GetPlayerViewSpheres(distanceFromPlayers);
            foreach (var sphere in playerSpheres)
            {
                revealed += RevealGridsInSphere(sphere);
            }

            if (_settingsChanged)
            {
                List<FrozenInfo> list;
                lock (Concealed)
                {
                    list = new List<FrozenInfo>(Concealed.Values);
                }

                var ss = Settings.Data;
                for (var i = list.Count - 1; i >= 0; i--)
                {
                    if (list[i].ShouldSkipFreeze(ss.FreezePirates, ss.FreezeProduction, ss.DontFreezeInGravity, ss.DontFreezeProductionMaximumPCU, ss.DelayAfterLogout, ss.FreezeNPC, ss.FreezeStaticNPC, ss.FreezeSignals)) revealed += RevealGroup(list[i]);
                }
                _settingsChanged = false;
            }

            //if (revealed != 0) Log.Info($"Revealed {revealed} grids near players.");
            return revealed;
        }

        public int RevealGridsInSphere(BoundingSphereD sphere)
        {
            var revealed = 0;
            _concealedAabbTree.OverlapAllBoundingSphere(ref sphere, buffer);
            foreach (var group in buffer)
            {
                revealed += RevealGroup(group);
            }
            buffer.Clear();
            return revealed;
        }

        public int ConcealGrids(double distanceFromPlayers = 0, int maxAmount = 0)
        {
            var x = new Stopwatch();
            x.Start();
            var gc = MemoryTracker.now;
            var groups = TargetConcealGrids(distanceFromPlayers, maxAmount);
            //for (int j = 0; j < 300; j++)
            //{
            //    TargetConcealGrids(distanceFromPlayers, maxAmount);
            //}
            int concealed = 0;
            var d = x.Elapsed.TotalMilliseconds;
            if (d > 1 && !MemoryTracker.Changed(gc))
            {
                Log.Error($"Find grids {groups.Count()} / {Settings.Data.UseFreezePriority} to conceal took:{d} ms");
            }

            x.Reset();

            lock (groups)
            {
                var i = 0;
                foreach (var group in groups)
                {
                    string str = $"Freezing group: {Environment.NewLine}[{group.Grids.Select(xs => xs.EntityId.ToString()).Aggregate((string j, string k) => j + "," + k)}]{Environment.NewLine} ({group.Id})";
                    Log.Warn(str);
                    concealed += Freeze(group, i);
                    if (++i > maxAmount && maxAmount != 0)
                        break;
                }
            }


            d = x.Elapsed.TotalMilliseconds;
            //if (d > 1) Log.Error($"Conceal {groups.Count()} grids took:{x.Elapsed.TotalMilliseconds} ms");
            x.Stop();

            return concealed;
        }

        public int RevealAll(int maxAmount = 9999)
        {
            var revealed = 0;

            List<FrozenInfo> list;
            lock (Concealed)
            {
                list = new List<FrozenInfo>(Concealed.Values);
            }

            var max = Math.Min(list.Count, maxAmount);

            if (Settings.Data.UseUnfreezePriority)
                list.Sort((a, b) => b.Rating.CompareTo(a.Rating));
            else
                list.ShuffleList(); //Randomize

            for (var i = 0; i < max; i++)
            {
                revealed += RevealGroup(list[i]);
            }

            return revealed;
        }

        public IEnumerable<FrozenInfo> TargetConcealGrids(double distanceFromPlayers = 0, int maxAmount = 0)
        {
            _freezableGridGroups.Clear();
            var playerSpheres = GetPlayerViewSpheres(distanceFromPlayers);

            long found = 0;

            Parallel.ForEach(MyCubeGridGroups.Static.Physical.Groups, @group =>
            {
                var pool = m_grids_pool.Value;
                var grids = pool.get();
                if (!TryFindFreezableGridGroups(@group, maxAmount, playerSpheres, ref found, grids))
                {
                    pool.put(grids);
                }
            });

            if (Settings.Data.UseFreezePriority)
            {
                _freezableGridGroups.Sort((a, b) => b.Rating.CompareTo(a.Rating));
            }

            return _freezableGridGroups;
        }

        private bool TryFindFreezableGridGroups(MyGroups<MyCubeGrid, MyGridPhysicalGroupData>.Group group, int maxAmount, List<BoundingSphereD> playerSpheres, ref long found, List<MyCubeGrid> grids)
        {

            if (maxAmount > 0)
            {
                var f = Interlocked.Read(ref found);
                if (f > maxAmount)
                    return false;
            }

            foreach (var g in group.Nodes)
            {
                grids.Add(g.NodeData);
            }

            FrozenInfo.CalculateIdAndAABB(grids, out var Id, out var aabb);

            if (Id == 0)
                return false;

            foreach (var sphere in playerSpheres)
            {
                if (sphere.Contains(aabb) != ContainmentType.Disjoint)
                    return false;
            }

            lock (Concealed)
            {
                if (Concealed.ContainsKey(Id))
                    return false;
            }

            if (maxAmount > 0)
                Interlocked.Increment(ref found);

            var concealGroup = new FrozenInfo(Id, grids, aabb, null);
            if (concealGroup.ShouldSkipFreeze(Settings.Data.FreezePirates,
                Settings.Data.FreezeProduction,
                Config.DontFreezeInGravity,
                Config.DontFreezeProductionMaximumPCU,
                Config.DelayAfterLogout,
                Config.FreezeNPC,
                Config.FreezeStaticNPC,
                Config.FreezeSignals))
            {
                return false;
            }

            lock (_freezableGridGroups)
            {
                _freezableGridGroups.Add(concealGroup);
            }
            return true;
        }

        private List<BoundingSphereD> GetPlayerViewSpheres(double distance)
        {
            m_cacheList2.Clear();
            var all = ((MyPlayerCollection)MyAPIGateway.Multiplayer.Players).GetOnlinePlayers();
            foreach (var p in all)
            {
                m_cacheList2.Add(new BoundingSphereD(p.GetPosition(), distance));
            }

            return m_cacheList2;
        }

        public void OnGameSave()
        {
            lock (Concealed)
            {
                bool profiling = FreezerPlugin.freezer.Config.Profiling;
                if (profiling)
                {
                    Log.Error("OnGameSave start.");
                }

                var fg = new FrozenGrids
                {
                    GridGroups = new Dictionary<long, FrozenGridGroup>()
                };

                foreach (var c in Concealed)
                {
                    if (c.Value == null || c.Value.Info == null)
                        continue;

                    fg.GridGroups[c.Key] = c.Value.Info;
                }

                FrozenGrids.Save(fg);
                if (profiling)
                {
                    Log.Error("OnGameSave end.");
                }
            }
        }
    }
}