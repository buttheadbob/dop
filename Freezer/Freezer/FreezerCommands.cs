﻿using NAPI;
using Sandbox.Game;
using Sandbox.Game.Entities;
using System.Collections.Generic;
using System.Linq;
using Torch.Commands;
using Torch.Commands.Permissions;
using TorchPlugin;
using VRage.Game.ModAPI;
using VRageMath;

namespace Slime
{
    class FreezerCommands : CommandModule
    {
        public Freezer Plugin => FreezerPlugin.freezer;

        [Command("freeze", "Freeze grids x distance from players. Use Distance=0 for default distance. Use howMany=0 to freeze all in one moment. When howMany > 0, it wont be concealed more than that value"), Permission(MyPromoteLevel.SpaceMaster)]
        public void Conceal(double distance = 0, int howMany = 0)
        {
            if (distance == 0) { distance = Plugin.Settings.Data.FreezeDistance; }

            var num = Plugin.ConcealGrids(distance, howMany);
            Context.Respond($"{num} grids concealed.");
        }

        [Command("unfreeze", "Unfreeze grids. If you don't want make a huge freeze, use small amount (howMany=10) in cycle"), Permission(MyPromoteLevel.SpaceMaster)]
        public void RevealAll(int howMany = 9999)
        {
            int num = Plugin.RevealAll(howMany);
            Context.Respond($"{num} grids revealed.");
        }

        [Command("freeze on", "Enable freezer.")]
        public void Enable()
        {
            Plugin.Settings.Data.Enabled = true;
            Plugin.ConcealGrids(Plugin.Settings.Data.FreezeDistance);
        }

        [Command("freeze production", "Enable production freeze or not")]
        public void ProductionFreeze(bool enable)
        {
            if (Plugin.Settings.Data.FreezeProduction != enable)
            {
                Plugin.Settings.Data.FreezeProduction = enable;
                if (enable == true)
                {
                    Plugin.Settings.Data.FreezeProduction = enable;
                }
                else
                {
                    Plugin.RevealAll();
                }
            }
        }

        [Command("freeze off", "Disable freezer.")]
        public void Disable()
        {
            Plugin.Settings.Data.Enabled = false;
            Plugin.RevealAll();
        }

        [Command("freeze auto on", "Enable freezer.")]
        public void EnableAuto()
        {
            Plugin.Settings.Data.LoadBalancer = true;
            Plugin.ConcealGrids(Plugin.Settings.Data.FreezeDistance);
        }

        [Command("freeze auto off", "Disable freezer.")]
        public void DisableAuto() { Plugin.Settings.Data.LoadBalancer = false; }




    }

    [Category("show")]
    class SearchCommands : CommandModule
    {
        [Command("can-be-frozen", "Shows all players")]
        public void ShowCanBefrozen()
        {
            var f = FreezerPlugin.freezer;
            var positions = f.TargetConcealGrids(f.Config.FreezeDistance, 0);

            foreach (var p in positions)
            {
                MyVisualScriptLogicProvider.AddGPS("w", "Warm but can be cold", p.WorldAABB.Center, Color.LawnGreen, 120, Context.Player.IdentityId);
            }
            Context.Respond("Total can-be-frozen:" + positions.Count());
        }

        [Command("frozen", "Shows all players")]
        public void ShowFrozen()
        {
            var f = FreezerPlugin.freezer;
            var list = new List<FrozenInfo>(f.Concealed.Values);
            foreach (var p in list)
            {
                MyVisualScriptLogicProvider.AddGPS("f", "Frozen", p.WorldAABB.Center, Color.Gold, 120, Context.Player.IdentityId);
            }
            Context.Respond("Total frozen:" + list.Count);
        }

        [Command("not-frozen", "Shows all players")]
        public void ShowNotFrozen()
        {
            var f = FreezerPlugin.freezer;
            var am = 0;
            foreach (var g in MyCubeGridGroups.Static.Physical.Groups)
            {
                var Grids = g.Nodes.Select(n => n.NodeData).ToList();
                if (Grids.First().isFrozen()) continue;
                am++;
                MyVisualScriptLogicProvider.AddGPS("a", "Not-Frozen", Grids.First().WorldMatrix.Translation, Color.Aqua, 120, Context.Player.IdentityId);
            }
            Context.Respond("Total not frozen:" + am);
        }
    }
}