﻿using NAPI;
using Sandbox.ModAPI;
using VRage.Game.ModAPI;
using VRage.Utils;

namespace Slime
{
    public static class FreezerDamagePrevention
    {
        
        public static void Init(Freezer freezer)
        {
            if (freezer.Settings.Data.FrozenGridsDontReceiveDamage)
            {
                MyAPIGateway.Session.DamageSystem.RegisterBeforeDamageHandler(0, HandleDamage);
            }
        }

        private static void HandleDamage(object target, ref MyDamageInformation damage)
        {
            var slimBlock = target as IMySlimBlock;
            if (slimBlock != null)
            {
                var g = slimBlock.CubeGrid;
                if (g.isFrozen()) { 
                    damage.Amount = 0; 
                    damage.IsDeformation = false; 
                }
            }
        }

    }
}