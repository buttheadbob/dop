﻿using System.Linq;
using System.Reflection;
using NAPI;
using Sandbox;
using Sandbox.Game.Entities.Cube;
using Slime;
using Torch.Managers.PatchManager;
using TorchPlugin;

namespace Freezer.Freezer.Hooks
{
    public class Hook_MyShipConnector_OnPhysicsEnabledChanged
    {
        public static MethodInfo methodWeld;
        public static void Patch(PatchContext patchContext)
        {
            if (!FreezerPlugin.freezer.Config.FreezePhysicsEnabled) return;
            patchContext.Suffix(typeof(MyShipConnector), typeof(Hook_MyShipConnector_OnPhysicsEnabledChanged), "OnPhysicsEnabledChanged");
            Log.Info($"MyShipConnector.OnPhysicsEnabledChanged Suffix patched!");
            methodWeld = typeof(MyShipConnector).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x1 => x1.Name == "Weld").First();
        }

        public static void OnPhysicsEnabledChanged(MyShipConnector __instance)
        {
            Log.Warn($"After MyShipConnector.OnPhysicsEnabledChanged hit suffix patch.");

            if (__instance.CubeGrid.isFrozen())
            {
                if (__instance != null && !__instance.CubeGrid.MarkedForClose && !__instance.MarkedForClose && !__instance.Closed && __instance.CubeGrid.Physics != null && __instance.CubeGrid.Physics.Enabled)
                {
                    MySandboxGame.Static.Invoke(() =>
                    {
                        try
                        {
                            methodWeld.Invoke(__instance, null);
                        }
                        catch
                        {
                            MySandboxGame.Log.Log(VRage.Utils.MyLogSeverity.Error, "FreezerPlugin: Fail to invoke Connector Weld");
                        }
                    }
                    , "FreezerPlugin");

                    if (FreezerPlugin.freezer.Config.Profiling)
                    {
                        Log.Warn($"OnPhysicsEnabledChanged invoke Weld. Because Frozen.[{__instance.CubeGrid.DisplayName}]");
                    }
                }
            }
            else
            {
                if (FreezerPlugin.freezer.Config.Profiling)
                {
                    Log.Warn($"OnPhysicsEnabledChanged not invoked Weld. Because NOT Frozen.[{__instance.CubeGrid.DisplayName}]");
                }
            }
            return;
        }
    }
}