﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Game.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using Torch.Commands;
using Sandbox.Game.World;
using VRage.Game.ModAPI;
using Sandbox.ModAPI;
using Torch.Commands.Permissions;
using NAPI;
using Sandbox.Engine.Physics;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Scripts.Shared;
using VRage.ModAPI;
using VRageMath;
using VRageMath.Spatial;
using Sandbox.ModAPI;
using Torch;
using System.Linq;
using Sandbox.Engine.Multiplayer;
using VRage.Network;
using Sandbox;
using Sandbox.Game;
using System.Linq.Expressions;
using Freezer.Freezer.Data;
using Sandbox.Game.Entities.Blocks;
using TorchPlugin;
using Sandbox.Definitions;
using Torch.Managers.PatchManager;
using VRage.Game.Components;
using Slime;
using VRage.Game.Entity;

namespace Freezer.Freezer.Hooks
{
    public class Hook_MyShipConnector_CubeGrid_OnBodyPhysicsChanged
    {

        public static void Patch(PatchContext patchContext)
        {
            if (!FreezerPlugin.freezer.Config.FreezePhysicsEnabled) return;

            patchContext.Prefix(typeof(MyShipConnector), typeof(Hook_MyShipConnector_CubeGrid_OnBodyPhysicsChanged), "CubeGrid_OnBodyPhysicsChanged");
            Log.Info($"MyShipConnector.CubeGrid_OnBodyPhysicsChanged patched!");
        }

        public static bool CubeGrid_OnBodyPhysicsChanged(MyShipConnector __instance,MyEntity obj)
        {
            if (!FreezerPlugin.freezer.Config.FreezePhysicsHooksEnabled)
            {
                Log.Warn($"MyShipConnector.CubeGrid_OnBodyPhysicsChanged hit. Hook skiped.");
                return true;
            }

            if (__instance.CubeGrid.isFrozen() || obj.isFrozen())
            {
                if (FreezerPlugin.freezer.Config.Profiling)
                {
                    // Log.LogSpamming(37747, (builder, i) => { builder.Append($"OnPhysicsEnabledChanged from: {new StackTrace()}"); });
                    Log.Warn($"CubeGrid_OnBodyPhysicsChanged eaten. Because Frozen.[{__instance.CubeGrid.DisplayName}]");
                }
                return false;
            }
            else
            {
                if (FreezerPlugin.freezer.Config.Profiling)
                {
                    Log.Warn($"CubeGrid_OnBodyPhysicsChanged NOT eaten. Because NOT Frozen.[{__instance.CubeGrid.DisplayName}]");
                }
                return true;
            }
        }
    }
}
