﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Game.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using Torch.Commands;
using Sandbox.Game.World;
using VRage.Game.ModAPI;
using Sandbox.ModAPI;
using Torch.Commands.Permissions;
using NAPI;
using Sandbox.Engine.Physics;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Scripts.Shared;
using VRage.ModAPI;
using VRageMath;
using VRageMath.Spatial;
using Sandbox.ModAPI;
using Torch;
using System.Linq;
using Sandbox.Engine.Multiplayer;
using VRage.Network;
using Sandbox;
using Sandbox.Game;
using System.Linq.Expressions;
using Freezer.Freezer.Data;
using Sandbox.Game.Entities.Blocks;
using TorchPlugin;
using Sandbox.Definitions;
using Torch.Managers.PatchManager;
using VRage.Game.Components;
using Slime;
using VRage.Game.Entity;

namespace Freezer.Freezer.Hooks
{
    public class Hook_MyEntity_RaisePhysicsChanged
    {

        public static void Patch(PatchContext patchContext)
        {
            if (!FreezerPlugin.freezer.Config.FreezePhysicsEnabled) return;

            patchContext.Prefix(typeof(MyEntity), typeof(Hook_MyEntity_RaisePhysicsChanged), "RaisePhysicsChanged");
            Log.Info($"MyEntity.RaisePhysicsChanged patched!");
        }

        public static bool RaisePhysicsChanged(MyEntity __instance)
        {
           
            if (!FreezerPlugin.freezer.Config.FreezePhysicsHooksEnabled)
            {
                Log.Warn($"MyEntity.RaisePhysicsChanged hit.");
               // return true;
            }

            if (__instance.isFrozen())
            {
                try
                {
                    if (__instance is MyShipConnector)
                    {
                        var methodWeld = __instance.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x1 => x1.Name == "Weld").First();
                        methodWeld.Invoke(__instance, null);
                        Log.Warn($"MyEntity.RaisePhysicsChanged methodWeld");
                    }
                }
                catch(Exception ex) { Log.Warn($"MyEntity.RaisePhysicsChanged catch" + ex);}
                if (FreezerPlugin.freezer.Config.Profiling)
                {
                    // Log.LogSpamming(37747, (builder, i) => { builder.Append($"OnPhysicsEnabledChanged from: {new StackTrace()}"); });
                    Log.Warn($"MyEntity RaisePhysicsChanged  . Because Frozen.[{__instance.EntityId}({(__instance as MyCubeGrid)?.DisplayName})]");
                }
                return true;
            }
            else
            {
                if (FreezerPlugin.freezer.Config.Profiling)
                {
                    Log.Warn($"MyEntity RaisePhysicsChanged NOT  . Because NOT Frozen.[{__instance.EntityId}({(__instance as MyCubeGrid)?.DisplayName})]");
                }
                return true;
            }
        }
    }
}
