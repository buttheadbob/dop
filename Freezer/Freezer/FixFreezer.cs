﻿using System;
using System.Reflection;
using Sandbox.Game.Entities;
using Sandbox.Game.World.Generator;
using Torch.Managers.PatchManager;
using NAPI;
using TorchPlugin;

namespace Slime
{
    internal class FixFreezer
    {
        private static MethodInfo TrackEntity;
        public static void InitHack(PatchContext patchContext)
        {
            TrackEntity = typeof(MyProceduralWorldGenerator).easyPrivateMethod("TrackEntity");
            patchContext.Prefix(typeof(MyProceduralWorldGenerator), typeof(FixFreezer), "OnGridStaticChanged");

            if (FreezerPlugin.freezer.Settings.Data.MaxBatteryDeltaMs > 0)
            {
                Log.Error("Patching battaries. For fix of discharging");
                patchContext.Prefix(typeof(MyBatteryBlock), typeof(FixFreezer), "ConsumePower");
                patchContext.Prefix(typeof(MyBatteryBlock), typeof(FixFreezer), "StorePower");
            }
        }

        private static bool OnGridStaticChanged(MyCubeGrid grid, Boolean newIsStatic)
        {
            try
            {
                if (grid == null)
                {
                    Log.LogSpamming(1727274, (sb, x) => sb.Append("DOP : OnGridStaticChanged : Grid == null!"));
                    return false;
                }

                if (!newIsStatic)
                {
                    if (MyProceduralWorldGenerator.Static != null)
                    {
                        TrackEntity.Invoke(MyProceduralWorldGenerator.Static, new object[] { grid, grid.PositionComp.WorldAABB.HalfExtents.Length() });
                    }
                    else
                    {
                        Log.LogSpamming(1727275, (sb, t) => sb.Append("DOP : OnGridStaticChanged MyProceduralWorldGenerator.Static == null"));
                    }
                }
                MyProceduralWorldGenerator.Static.RemoveTrackedEntity(grid);
                return false;
            }
            catch (Exception e)
            {
                Log.LogSpamming(1727276, (sb, t) => sb.Append("DOP : OnGridStaticChanged HANDLED Exception: " + grid.EntityId + "/" + grid.DisplayName));
            }

            return false;
        }

        private static bool ConsumePower(MyBatteryBlock __instance, float timeDeltaMs, float output)
        {
            if (timeDeltaMs >= FreezerPlugin.freezer.Settings.Data.MaxBatteryDeltaMs)
            {
                return false;
            }
            return true;
        }

        private static bool StorePower(MyBatteryBlock __instance, float timeDeltaMs, float input)
        {
            if (timeDeltaMs >= FreezerPlugin.freezer.Settings.Data.MaxBatteryDeltaMs)
            {
                return false;
            }
            return true;
        }

    }
}