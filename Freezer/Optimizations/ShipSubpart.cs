using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Entities.Blocks;
using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.Weapons;
using Sandbox.ModAPI;
using SpaceEngineers.Game.Entities.Blocks;
using SpaceEngineers.Game.Weapons.Guns;
using TorchPlugin;

namespace Slime.Features
{
    //9e333c09-e4c3-4aef-bd7b-36cfdf983ce2
    public class ShipSubpart : SlowdownShip
    {
        public static Type ASSEMBLER = typeof(MyAssembler);
        public static Type REFINERY = typeof(MyRefinery);

        public static Type WIND_TURBINE = typeof(MyWindTurbine);
        public static Type SOLAR_PANEL = typeof(MySolarPanel);
        public static Type BATTERY = typeof(MyBatteryBlock);

        public static Type GATLING_TURRET = typeof(MyLargeGatlingTurret);
        public static Type MISSLE_TURRET = typeof(MyLargeMissileTurret);

        public static Type COCKPIT = typeof(MyCockpit);
        public static Type PROJECTOR = typeof(MySpaceProjector);

        public static Type CRYO = typeof(MyCryoChamber);


        public HashSet<MyCubeBlock> excluded = new HashSet<MyCubeBlock>();
        public HashSet<IMyFunctionalBlock> excludedWorking = new HashSet<IMyFunctionalBlock>();


        public Dictionary<Type, HashSet<MyCubeBlock>> blocks = new Dictionary<Type, HashSet<MyCubeBlock>>();

        public ShipSubpart(MyCubeGrid grid) : base(grid)
        {
            
            Dictionary<Type, HashSet<MyCubeBlock>> _dict = new Dictionary<Type, HashSet<MyCubeBlock>>();

            Parallel.ForEach(grid.GetBlocks(), xx =>
            {
                try
                {
                    var fat = xx.FatBlock;
                    var sn = xx.BlockDefinition.Id.SubtypeName;
                    if (fat != null)
                    {
                        lock (_dict)
                        {
                            var t = fat.GetType();
                            HashSet<MyCubeBlock> set;
                            if (!_dict.ContainsKey(t))
                            {
                                set = new HashSet<MyCubeBlock>();
                                _dict.Add(t, set);
                            }
                            else
                            {
                                set = _dict[t];
                            }
                            set.Add(fat);
                        }

                        if (FreezerPlugin.freezer.DontFreezeWithBlocks.Matches(xx.BlockDefinition.Id))
                        {
                            lock (excluded)
                            {
                                excluded.Add(fat);
                            }
                        }

                        var fu = fat as IMyFunctionalBlock;
                        if (fu != null && FreezerPlugin.freezer.DontFreezeWithWorking.Matches(xx.BlockDefinition.Id))
                        {
                            lock (excludedWorking)
                            {
                                excludedWorking.Add(fu);
                            }
                        }
                    }
                }
                catch (Exception e) { 
                    Log.Fatal(e, "SlowGrid" + xx + " " + xx.BlockDefinition.Id.SubtypeName + " WTF? "); 
                }
            });

            this.blocks = _dict;
        }

        public void BlockAddedOrRemoved(MyCubeBlock block, bool added)
        {
            var t = block.GetType();
            var set = blocks.GetOrCreate(t);
            set.AddOrRemove(block, added);
            
            if (FreezerPlugin.freezer.DontFreezeWithBlocks.Matches(block.BlockDefinition.Id))
            {
                lock (excluded)
                {
                    excluded.AddOrRemove(block, added);
                }
            }

            var fu = block as IMyFunctionalBlock;
            if (fu != null && FreezerPlugin.freezer.DontFreezeWithWorking.Matches(block.BlockDefinition.Id))
            {
                lock (excludedWorking)
                {
                    excludedWorking.AddOrRemove(fu, added);
                }
            }
        }

        public bool HasWorkingProduction()
        {
            if (blocks.ContainsKey(ASSEMBLER))
            {
                foreach (var pp in blocks[ASSEMBLER])
                {
                    if ((pp as MyAssembler).IsProducing) { return true; }
                }
            }

            if (blocks.ContainsKey(REFINERY))
            {
                foreach (var pp in blocks[REFINERY])
                {
                    if ((pp as MyRefinery).IsProducing) { return true; }
                }
            }

            return false;
        }

        public bool IsExcluded()
        {
            if (excluded.Count > 0) return true;
            foreach (var x in excludedWorking)
            {
                if (x.IsWorking) { return true; }
            }
            return false;
        }
    }
}
