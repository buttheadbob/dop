﻿using System;
using System.Collections.Generic;
using NAPI;
using ParallelTasks;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.Weapons;
using Sandbox.ModAPI;
using VRage.Game.Entity;

namespace Slime.Features
{
    public class SlowLogic : Action1<long>
    {
        public static readonly Dictionary<MyCubeGrid, SlowdownShip> refsShips = new Dictionary<MyCubeGrid, SlowdownShip>();
        public static readonly Dictionary<MyCubeGrid, SlowdownShip> refsShipsToDelete = new Dictionary<MyCubeGrid, SlowdownShip>();

        public static void Init()
        {
            var ent = MyEntities.GetEntities();
            foreach (var e in ent) EntityAdded(e);
            MyEntities.OnEntityAdd += EntityAdded;
        }

        public void run(long frame)
        {
            if (refsShipsToDelete.Count > 0)
            {
                foreach (var x in refsShipsToDelete) { refsShips.Remove(x.Key); }

                refsShipsToDelete.Clear();
            }

            foreach (var x in refsShips) { x.Value.Tick(); }
        }

        public static void Clear()
        {
            refsShips.Clear();
            refsShipsToDelete.Clear();
            MyEntities.OnEntityAdd -= EntityAdded;
        }

        private static void EntityAdded(MyEntity myEntity)
        {
            if (myEntity is MyCubeGrid x) { slowGrid(x); }
        }

        private static void slowGrid(MyCubeGrid x)
        {
            x.OnFatBlockAdded -= X_OnFatBlockAdded;
            x.OnFatBlockAdded += X_OnFatBlockAdded;
            x.OnFatBlockRemoved -= X_OnFatBlockRemoved;
            x.OnFatBlockRemoved += X_OnFatBlockRemoved;
            refsShips.Add(x, new ShipSubpart(x));
        }


        private static void X_OnFatBlockAdded(MyCubeBlock obj)
        {
            if (!refsShips.ContainsKey(obj.CubeGrid)) return;
            var grid = refsShips[obj.CubeGrid];
            ((ShipSubpart)grid).BlockAddedOrRemoved(obj, true);
        }

        private static void X_OnFatBlockRemoved(MyCubeBlock obj)
        {
            if (!refsShips.ContainsKey(obj.CubeGrid)) return;
            var grid = refsShips[obj.CubeGrid];
            ((ShipSubpart)grid).BlockAddedOrRemoved(obj, false);
        }
    }
}