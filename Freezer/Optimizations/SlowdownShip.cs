﻿using System;
using Sandbox.Game.Entities;
using VRage.Game.Entity;

namespace Slime.Features
{
    public abstract class SlowdownShip
    {
        public static readonly Random rand = new Random();
        internal MyCubeGrid grid;

        protected SlowdownShip(MyCubeGrid grid)
        {
            this.grid = grid;
            grid.OnMarkForClose += Destroy;
        }

        protected void Destroy(MyEntity obj)
        {
            if (!SlowLogic.refsShipsToDelete.ContainsKey(grid)) { SlowLogic.refsShipsToDelete.Add(grid, this); }
        }

        public virtual void Tick(){}
        public virtual void Hack(){}
    }
}