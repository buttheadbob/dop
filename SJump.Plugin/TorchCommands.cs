﻿using System.IO;
using Sandbox.ModAPI;
using SJump;
using SJump.API;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game.ModAPI;

namespace SJump.Commands
{
    public class InfoModule : CommandModule
    {
        [Command("GroupImport", "Import a grid.")]
        [Permission(MyPromoteLevel.Admin)]
        public void GroupImport(string gridName, string targetName = null)
        {
            Context.Respond($"GroupImport work");

            if (targetName == null)
            {
                if (Context.Player == null)
                {
                    Context.Respond("Target entity must be specified.");
                    return;
                }

                targetName = Context.Player.Controller.ControlledEntity.Entity.DisplayName;
            }

            if (!Util.TryGetEntityByNameOrId(targetName, out var ent))
            {
                Context.Respond("Target entity not found.");
                return;
            }

            var path = MainLogic.Instanse.Storagepath + "\\OutgoingShipsBackup\\" + gridName + ".sbc";

            if (!File.Exists(path))
            {
                Context.Respond("File does not exist.");
                return;
            }
           // MyAPIGateway.Parallel.StartBackground(() => Util.ImportGridsFromFile(ent, path));

        }

        /*
        [Command("ForceJump", "[In dev] Force launch jump")]
        [Permission(MyPromoteLevel.Owner)]
        public void ForceJump()
        {
            Context.Respond($"ForceJump , console? = " + Context.SentBySelf);
            
        }
        */


        [Command("restartsoon", "Check for restart.")]
        [Permission(MyPromoteLevel.Admin)]
        public void restartsoon()
        {
         //   Patch.IsRestartSoon(30);
        }

        [Command("jump", "Jump")]
        [Permission(MyPromoteLevel.None)]
        public void jump()
        {
            
            MainLogic.Instanse.StartJump(this.Context.Player.SteamUserId);
        }

    }
}

