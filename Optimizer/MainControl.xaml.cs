﻿using Slime.Slow;
using System.Text;
using System.Windows.Controls;

namespace Slime.GUI
{
    public partial class MainControl : UserControl
    {
        public MainControl()
        {
            InitializeComponent();
            POptimizations.DataContext = OptimizerPlugin.Config;

            var sb = new StringBuilder();
            foreach (var x in Optimizations.DISABLES)
            {
                foreach (var y in x.Value)
                {
                    sb.Append($"{x.Key.Name}-{y} ");
                }
            }

            Disabled.Text = sb.ToString();
            sb.Clear();

            foreach (var x in Optimizations.IMPLEMENTATIONS)
            {
                foreach (var y in x.Value)
                {
                    sb.Append($"{x.Key.Name}-{y} ");
                }
            }

            Implentations.Text = sb.ToString();
            sb.Clear();

            foreach (var x in Optimizations.EXPEREMENTAL)
            {
                foreach (var y in x.Value)
                {
                    sb.Append($"{x.Key.Name}-{y} ");
                }
            }

            Expermental.Text = sb.ToString();
            sb.Clear();

        }
    }
}