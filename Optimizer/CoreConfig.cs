using Slime.GUI;
using System;
using System.Runtime.CompilerServices;
using Torch;

namespace Slime.Config
{
    public class CoreConfig : ViewModel
    {
        private bool _enabledSlowdownSafezoneEnabled = false;
        private int _enabledSlowdownSafezoneEnabledTimes = 3;

        private bool _slowPBUpdateEnable = false;
        private int _slowPBUpdate1 = 10;
        private int _slowPBUpdate10 = 4;
        private int _slowPBUpdate100 = 2;
        private string _slowPBIgnored = "";

        private string _disabledMethods = "";
        private string _implementations = "";

        private string _experimental = "";
        private string _customOptimizations = "";

        private bool _welderTweaksEnabled = false;
        private bool _welderNoLimitsCheck = false;
        private bool _welderWeldProjectionsNextFrame = true;
        private bool _welderWeldNextFrames = true;
        private bool _welderCanWeldProjectionsIfWeldedOtherBlocks = false;
        private bool _welderSelfWelding = false;
        private bool _welderExcludeNanobot = true;
        private bool _welderFasterSearch = true;
        private bool _welderSkipCreativeWelding = true;
        private bool _enabledDisableModProfiler = true;
        private bool _enabledDisableGameProfiler = true;

        [DisplayTab(Name = "DisabledMethods", GroupName = "Optimizations", Tab = "Optimizations", Order = 0)]
        public string DisabledMethods { get => _disabledMethods; set => SetValue(ref _disabledMethods, value); }

        [DisplayTab(Name = "Implementations", GroupName = "Optimizations", Tab = "Optimizations", Order = 0)]
        public string Implementations { get => _implementations; set => SetValue(ref _implementations, value); }


        //=================================================================================================

        [DisplayTab(Name = "Enabled", GroupName = "Welder Tweaks (WIP)", Tab = "Optimizations", Order = 0, Description = "If disabled all off theese features not working. Optimization MyCubeGrid-GetBlocksInsideSphere - also highly recommended")]
        public bool WelderTweaksEnabled { get => _welderTweaksEnabled; set => SetValue(ref _welderTweaksEnabled, value); }

        [DisplayTab(Name = "No Limits Check", GroupName = "Welder Tweaks (WIP)", Tab = "Optimizations", Order = 1, Description = "Welder doesn't check limits, which making welding faster (not recommended)")]
        public bool WelderTweaksNoLimitsCheck { get => _welderNoLimitsCheck; set => SetValue(ref _welderNoLimitsCheck, value); }

        [DisplayTab(Name = "Weld Next Frames", GroupName = "Welder Tweaks (WIP)", Tab = "Optimizations", Order = 2, Description = "Welder block search is in 1 frame, but welding in random next 5 frames (will remove high simulation drops because 100 welders were enabled in 1 frame)")]
        public bool WelderTweaksWeldNextFrames { get => _welderWeldNextFrames; set => SetValue(ref _welderWeldNextFrames, value); }

        [DisplayTab(Name = "Weld Projections Next Frame", GroupName = "Welder Tweaks (WIP)", Tab = "Optimizations", Order = 3, Description = "Welder can weld it self")]
        public bool WelderTweaksWeldProjectionsNextFrame { get => _welderWeldProjectionsNextFrame; set => SetValue(ref _welderWeldProjectionsNextFrame, value); }

        [DisplayTab(Name = "Weld Projections if welded other blocks", GroupName = "Welder Tweaks (WIP)", Tab = "Optimizations", Order = 4, Description = "Welder can weld projections and non projected blocks on same frame (faster welding, less optimization)")]
        public bool WelderTweaksCanWeldProjectionsIfWeldedOtherBlocks { get => _welderCanWeldProjectionsIfWeldedOtherBlocks; set => SetValue(ref _welderCanWeldProjectionsIfWeldedOtherBlocks, value); }

        [DisplayTab(Name = "Self Welding", GroupName = "Welder Tweaks (WIP)", Tab = "Optimizations", Order = 5, Description = "Welder can weld it self")]
        public bool WelderTweaksSelfWelding { get => _welderSelfWelding; set => SetValue(ref _welderSelfWelding, value); }

        [DisplayTab(Name = "Exclude Nanobot", GroupName = "Welder Tweaks (WIP)", Tab = "Optimizations", Order = 6, Description = "Nanobot can't weld as regular welder (and shouldn't)")]
        public bool WelderTweaksExcludeNanobot { get => _welderExcludeNanobot; set => SetValue(ref _welderExcludeNanobot, value); }

        [DisplayTab(Name = "Faster block search", GroupName = "Welder Tweaks (WIP)", Tab = "Optimizations", Order = 7, Description = "Inaccurate block search")]
        public bool WelderTweaksFasterSearch { get => _welderFasterSearch; set => SetValue(ref _welderFasterSearch, value); }

        [DisplayTab(Name = "Faster block search", GroupName = "Welder Tweaks (WIP)", Tab = "Optimizations", Order = 8, Description = "Inaccurate block search")]
        public bool WelderSkipCreativeWelding { get => _welderFasterSearch; set => SetValue(ref _welderFasterSearch, value); }

        //=================================================================================================

        [DisplayTab(Name = "Enabled", GroupName = "ProgramBlock", Tab = "Optimizations", Order = 1)]
        public bool SlowPBEnabled { get => _slowPBUpdateEnable; set => SetValue(ref _slowPBUpdateEnable, value); }

        [DisplayTab(Name = "Update 1 Slow", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "Description 1-> vanilla, 5-> 4 frames idle, 1 update")]
        public int SlowPBUpdate1 { get => _slowPBUpdate1; set => SetValue(ref _slowPBUpdate1, Math.Max(1, value)); }

        [DisplayTab(Name = "Update 10 Slow", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "Description 1-> vanilla, 5-> 49 frames idle, 1 update")]
        public int SlowPBUpdate10 { get => _slowPBUpdate10; set => SetValue(ref _slowPBUpdate10, Math.Max(1, value)); }

        [DisplayTab(Name = "Update 100 Slow", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "Description 1-> vanilla, 5-> 499 frames idle, 1 update")]
        public int SlowPBUpdate100 { get => _slowPBUpdate100; set => SetValue(ref _slowPBUpdate100, Math.Max(1, value)); }

        [DisplayTab(Name = "Ignored subtypes", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "This subtypeIds will be ignored. use ' ' or ',' as a separator")]
        public string SlowPBIgnored { get => _slowPBIgnored; set => SetValue(ref _slowPBIgnored, value); }

        //=================================================================================================

        [DisplayTab(Name = "On/Off", GroupName = "Safezones", Tab = "Optimizations", Order = 1)]
        public bool EnabledSlowdownSafezoneEnabled { get => _enabledSlowdownSafezoneEnabled; set => SetValue(ref _enabledSlowdownSafezoneEnabled, value); }

        [DisplayTab(Name = "Slowdown times", GroupName = "Safezones", Tab = "Optimizations", Order = 2, Description = "-1 Disables at all pulling out from safezones")]
        public int EnabledSlowdownSafezoneEnabledTimes { get => _enabledSlowdownSafezoneEnabledTimes; set => SetValue(ref _enabledSlowdownSafezoneEnabledTimes, Math.Max(-1, value)); }
        
        [DisplayTab(Name = "On/Off", GroupName = "ModProfiler", Tab = "Optimizations", Order = 1)]
        public bool EnabledDisableModProfiler { get => _enabledDisableModProfiler; set => SetValue(ref _enabledDisableModProfiler, value); }
        
        [DisplayTab(Name = "On/Off", GroupName = "GameProfiler", Tab = "Optimizations", Order = 1)]
        public bool EnabledDisableGameProfiler { get => _enabledDisableGameProfiler; set => SetValue(ref _enabledDisableGameProfiler, value); }
    }
}