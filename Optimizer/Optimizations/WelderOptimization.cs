﻿using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.World;
using SpaceEngineers.Game.Entities.Blocks;
using System;
using System.Collections.Generic;
using VRage;
using NAPI;
using Sandbox.Game.Multiplayer;
using VRage.Game;
using VRage.Game.Entity;
using VRageMath;
using System.Linq;
using Sandbox.Definitions;
using Sandbox;
using Sandbox.Game.Weapons;
using Sandbox.ModAPI;
using Torch.Managers.PatchManager;
using Slime;

namespace Optimizer.Optimizations
{
    public class WelderOptimization
    {
        const string ActionNAME ="ShipWelder BuildProjection";
        private static Dictionary<string, int> m_missingComponents = new Dictionary<string, int>();
        private static HashSet<MySlimBlock> m_projectedBlock = new HashSet<MySlimBlock>();

        public static Random random = new Random();

        public static void Patch (PatchContext context)
        {
            if (OptimizerPlugin.Config.WelderTweaksEnabled)
            {
                context.Redirect(typeof(MyShipWelder), typeof(WelderOptimization), "Activate");
            }
        }

        public static bool Activate(MyShipWelder __instance, ref bool __result, HashSet<MySlimBlock> targets)
        {
            __result = false; //it affects only sound;
            if (!OptimizerPlugin.Config.WelderTweaksEnabled)
            {
                return true;
            }

            if (OptimizerPlugin.Config.WelderTweaksExcludeNanobot)
            {
                var def = (MyShipWelderDefinition)__instance.BlockDefinition;
                if (def.SensorRadius < 0.01f) //NanobotOptimiztion
                {
                    return false;
                }
            }

            if (!OptimizerPlugin.Config.WelderTweaksSelfWelding)
            {
                targets.Remove(__instance.SlimBlock);
            }

            if (OptimizerPlugin.Config.WelderTweaksWeldNextFrames)
            {
                var ntargets = new HashSet<MySlimBlock>(targets.Count);
                foreach(var x in targets)
                {
                    ntargets.Add(x);
                }

                FrameExecutor.addDelayedLogic(random.Next(4)+1, (x) =>
                {
                    ActivateInternal(__instance, ntargets);
                });
            } 
            else
            {
                ActivateInternal(__instance, targets);
            }

            //we dont need any checks now;
            return false;
        }
        

        public static void ActivateInternal (MyShipWelder welder, HashSet<MySlimBlock> targets)
        {
            if (welder.MarkedForClose || welder.Closed)
            {
                return;
            }

            int num = targets.Count;
            m_missingComponents.Clear();
            foreach (MySlimBlock mySlimBlock in targets)
            {
                if (mySlimBlock.IsFullIntegrity)
                {
                    num--;
                }
                else
                {
                    MyCubeBlockDefinition.PreloadConstructionModels(mySlimBlock.BlockDefinition);
                    mySlimBlock.GetMissingComponents(m_missingComponents);
                }
            }
            MyInventory inventory = welder.GetInventory(0);
            foreach (KeyValuePair<string, int> keyValuePair in m_missingComponents)
            {
                MyDefinitionId myDefinitionId = new MyDefinitionId(typeof(MyObjectBuilder_Component), keyValuePair.Key);
                if (Math.Max(keyValuePair.Value - (int)inventory.GetItemAmount(myDefinitionId, MyItemFlags.None, false), 0) != 0 && welder.UseConveyorSystem)
                {
                    welder.CubeGrid.GridSystems.ConveyorSystem.PullItem(myDefinitionId, new MyFixedPoint?(keyValuePair.Value), welder, inventory, false, false);
                }
            }

            m_missingComponents.Clear();

            var welded = Weld (welder, targets, inventory, num);

            if (!welded || OptimizerPlugin.Config.WelderTweaksCanWeldProjectionsIfWeldedOtherBlocks)
            {
                if (OptimizerPlugin.Config.WelderTweaksWeldProjectionsNextFrame)
                {
                    FrameExecutor.addDelayedLogic(1, (x) =>
                    {
                        WeldProjections(welder);
                    });
                }
                else
                {
                    WeldProjections(welder);
                }
            }
        }

        

        public static bool Weld (MyShipWelder welder, HashSet<MySlimBlock> targets, MyInventory inventory, int foundBlocks)
        {
            float blocksToWeld = Math.Min(4, (foundBlocks > 0) ? foundBlocks : 1);
            float weldAmount = MySession.Static.WelderSpeedMultiplier * MyShipWelder.WELDER_AMOUNT_PER_SECOND * (MyShipGrinderConstants.GRINDER_COOLDOWN_IN_MILISECONDS * 0.001f) / blocksToWeld;
            float maxAllowedBoneMovement = MyShipWelder.WELDER_MAX_REPAIR_BONE_MOVEMENT_SPEED * (MyShipGrinderConstants.GRINDER_COOLDOWN_IN_MILISECONDS * 0.001f);


            bool weldedAnyThing = false;
            foreach (var block in targets)
            {
                if (block.CubeGrid.Physics != null && block.CubeGrid.Physics.Enabled)
                {
                    bool canWeld = false;
                    if (!OptimizerPlugin.Config.WelderTweaksNoLimitsCheck)
                    {
                        canWeld = true;
                    } else
                    {
                        bool? flag2 = block.ComponentStack.WillFunctionalityRise(weldAmount);
                        if (flag2 == null || !flag2.Value || MySession.Static.CheckLimitsAndNotify(MySession.Static.LocalPlayerId, block.BlockDefinition.BlockPairName, block.BlockDefinition.PCU - MyCubeBlockDefinition.PCU_CONSTRUCTION_STAGE_COST, 0, 0, null))
                        {
                            canWeld = true;
                        }
                    }

                    if (canWeld)
                    {
                        block.MoveItemsToConstructionStockpile(inventory);
                        block.MoveUnneededItemsFromConstructionStockpile(inventory);
                        if (block.HasDeformation || block.MaxDeformation > 0.0001f || !block.IsFullIntegrity)
                        {
                            block.IncreaseMountLevel(weldAmount, welder.OwnerId, inventory, maxAllowedBoneMovement, welder.HelpOthers, welder.IDModule.ShareMode, false, false);
                            weldedAnyThing = true;
                        }
                    }
                }
            }

            return weldedAnyThing;
        }

        public static void WeldProjections (MyShipWelder welder)
        {
            if (welder.MarkedForClose || welder.Closed)
            {
                return;
            }

            MyInventory inventory = welder.GetInventory(0);
            var array = FindProjectedBlocks(welder);
            if (welder.UseConveyorSystem)
            {
                Dictionary<MyDefinitionId, int> componentsToPull = new Dictionary<MyDefinitionId, int>(); 
                for (int i = 0; i < array.Count; i++)
                {
                    MyCubeBlockDefinition.Component[] components = array[i].hitCube.BlockDefinition.Components;
                    if (components != null && components.Length != 0)
                    {
                        MyDefinitionId id = components[0].Definition.Id;
                        componentsToPull.Sum(id, 1);
                    }
                }

                foreach (var x in componentsToPull)
                {
                    welder.CubeGrid.GridSystems.ConveyorSystem.PullItem(x.Key, new MyFixedPoint?(x.Value), welder, inventory, false, false);
                }
            }

            bool flag3 = false; 
            if (!OptimizerPlugin.Config.WelderSkipCreativeWelding)
            {
                flag3 = MySession.Static.CreativeMode;
                MyPlayer.PlayerId id2;
                MyPlayer myPlayer;
                if (MySession.Static.Players.TryGetPlayerId(welder.BuiltBy, out id2) && MySession.Static.Players.TryGetPlayerById(id2, out myPlayer))
                {
                    flag3 |= MySession.Static.CreativeToolsEnabled(Sync.MyId);
                }
            }
            
            foreach (MyWelder.ProjectionRaycastData projectionRaycastData in array)
            {
                if (welder.IsWithinWorldLimits(projectionRaycastData.cubeProjector, projectionRaycastData.hitCube.BlockDefinition.BlockPairName, flag3 ? projectionRaycastData.hitCube.BlockDefinition.PCU : MyCubeBlockDefinition.PCU_CONSTRUCTION_STAGE_COST) && (MySession.Static.CreativeMode || inventory.ContainItems(new MyFixedPoint?(1), projectionRaycastData.hitCube.BlockDefinition.Components[0].Definition.Id, MyItemFlags.None)))
                {
                    MyWelder.ProjectionRaycastData invokedBlock = projectionRaycastData;
                    MySandboxGame.Static.Invoke(delegate ()
                    {
                        if (!invokedBlock.cubeProjector.Closed && !invokedBlock.cubeProjector.CubeGrid.Closed && (invokedBlock.hitCube.FatBlock == null || !invokedBlock.hitCube.FatBlock.Closed))
                        {
                            invokedBlock.cubeProjector.Build(invokedBlock.hitCube, welder.OwnerId, welder.EntityId, true, welder.BuiltBy);
                        }
                    }, ActionNAME);
                }
            }
        }

        // Token: 0x060019BA RID: 6586 RVA: 0x0007BD0C File Offset: 0x00079F0C
        private static List<MyWelder.ProjectionRaycastData> FindProjectedBlocks(MyShipWelder welder)
        {
            var w = welder.WorldMatrix;
            var d = (MyShipWelderDefinition)(welder.BlockDefinition);
            BoundingSphereD boundingSphereD = new BoundingSphereD(w.Translation + w.Forward * (welder.CubeGrid.GridSize * 1.5f + d.SensorOffset), (double)d.SensorRadius);
            List<MyWelder.ProjectionRaycastData> list = new List<MyWelder.ProjectionRaycastData>();
            List<MyEntity> entitiesInSphere = MyEntities.GetEntitiesInSphere(ref boundingSphereD);

            foreach (MyEntity myEntity in entitiesInSphere)
            {
                MyCubeGrid myCubeGrid = myEntity as MyCubeGrid;
                if (myCubeGrid != null && myCubeGrid.Projector != null)
                {
                    myCubeGrid.GetBlocksInsideSphere(ref boundingSphereD, m_projectedBlock, false);
                    foreach (MySlimBlock mySlimBlock in m_projectedBlock)
                    {
                        if (myCubeGrid.Projector.CanBuild(mySlimBlock, true) == BuildCheckResult.OK)
                        {
                            MySlimBlock cubeBlock = myCubeGrid.GetCubeBlock(mySlimBlock.Position);
                            if (cubeBlock != null)
                            {
                                list.Add(new MyWelder.ProjectionRaycastData(BuildCheckResult.OK, cubeBlock, myCubeGrid.Projector));
                            }
                        }
                    }
                    m_projectedBlock.Clear();
                }
            }

            m_projectedBlock.Clear();
            entitiesInSphere.Clear();
            return list;
        }
    }
}
