﻿using NAPI;
using Slime;
using Torch.Managers.PatchManager;
using VRage;

namespace Optimizer.Optimizations
{
    public static class NoGameProfiler
    {
        public static void PrePatch(PatchContext context)
        {
            if (OptimizerPlugin.Config.EnabledDisableGameProfiler)
            {
                MySimpleProfiler.ENABLE_SIMPLE_PROFILER = false;
            }
        }
    }
}
