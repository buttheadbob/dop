﻿using Sandbox.Game;
using Sandbox.Game.Components;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.GameSystems;
using Sandbox.Game.World;
using SpaceEngineers.Game.Entities.Blocks;
using System;
using System.Collections.Generic;
using System.Reflection;
using Torch.Managers.PatchManager;
using VRage;
using VRage.Game.ModAPI;
using VRage.Scripting;
using VRageRender.Animations;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using NAPI;
using Sandbox.Game.Multiplayer;
using VRage.Game;
using Sandbox.Engine.Utils;
using VRage.Game.Entity;
using VRageMath;
using System.Collections.Concurrent;
using Sandbox.Game.Entities.Character;
using VRage.Voxels.DualContouring;
using System.Linq;
using ParallelTasks;
using VRage.Utils;
using System.Threading;
using Sandbox.Definitions;
using Sandbox;
using Sandbox.Game.Weapons;
using Sandbox.ModAPI;

namespace Slime.Slow
{
	class Optimizations {

        public static Func<MyCubeGrid, ConcurrentDictionary<Vector3I, MyCube>> MCubes;

        public static bool Slow(long id, Dictionary<long, int> timers, int howSlow) {
            int timer;

            if (timers.TryGetValue(id, out timer)) {
                if (timer > 0) {
                    timers[id] = timer - 1;
                    return false;
                } else {
                    timers[id] = howSlow - 1;
                    return true;
                }
            } else {
                timers.Add(id, howSlow - 1);
                return true;
            }
        }

		public static Dictionary<Type, List<string>> DISABLES = new Dictionary<Type, List<string>>()
		{
			{ typeof(MyShipSoundComponent), new List<string> () { "UpdateSounds" } },
			{ typeof(MyDecals), new List<string> () { "HandleAddDecal" } },

			{ typeof(MyGasGenerator), new List<string> () { "SetEmissiveStateWorking", "UpdateSounds" } },
			{ typeof(MyProjectorBase), new List<string> () { "UpdateSounds" } },
			{ typeof(MyFunctionalBlock), new List<string> () { "UpdateSoundEmitters" } },
			{ typeof(MyThrust), new List<string>() {"ThrustParticles" } },
			{ typeof(MyEntity3DSoundEmitter), new List<string> () { "PlaySoundWithDistance", "Update", "PlaySoundInternal" }  },
			{ typeof(MyBatteryBlock), new List<string>() { "UpdateDetailedInfo","UpdateEmissivity" ,"SetEmissive","CalculateOutputTimeRemaining", "CalculateInputTimeRemaining" }  },
			{ typeof(MyGasTank), new List<string> ()  { "UpdateEmissivity" } },
			{ typeof(MyWheel), new List<string> ()  { "CheckTrail" } },
            
		};

		public static Dictionary<Type, List<string>> EXPEREMENTAL = new Dictionary<Type, List<string>>()
		{
			{ typeof(MySlimBlock), new List<string> () { "ReleaseUnneededStockpileItems" } },
			{ typeof(MyCubeBlock), new List<string> () { "ReleaseInventory" }  },
			{ typeof(MySimpleProfiler), new List<string> () { "BeginBlock", "Begin", "End", "EndNoMemberPairingCheck", "EndMemberPairingCheck", "BeginGPUBlock", "EndGPUBlock", "GetOrMakeBlock", "Reset", "Commit" } },
			{ Ext2.getTypeByName("Sandbox.Game.Weapons.MyProjectile"), new List<string> () { "ApplyProjectileForce", "ApllyDeformationCubeGrid" } },
			{ typeof(MyCharacterBone), new List<string>() { "ComputeAbsoluteTransforms" } },
            { typeof(MyDualContouringMesher), new List<string> ()  { "PreparePostprocessing" } },
			{ typeof(MyCharacter), new List<string>() { "UpdateHeadAndWeapon" } }
		};

		public static Dictionary<Type, List<string>> IMPLEMENTATIONS = new Dictionary<Type, List<string>>()
		{
			{ typeof(MyInventory), new List<string> () { "get_IsConstrained" } },
			{ typeof(MyGridTerminalSystem), new List<string> () { "UpdateGridBlocksOwnership"} },
			{ typeof(MyFactionCollection), new List<string> () { "IsNpcFaction"} },
			{ typeof(MyGasTank), new List<string> () { "ChangeFillRatioAmount" } },
			{ typeof(MyCubeGrid), new List<string> () { "GetBlocksInsideSphere" } },
			{ typeof(MySpinWait), new List<string> () { "SpinOnce" } },
		};



		public static void InitHack(PatchContext patchContext) {
            var impls = OptimizerPlugin.Config.Implementations.Split(' ');
            MCubes = typeof(MyCubeGrid).easyField ("m_cubes").CreateGetter<MyCubeGrid, ConcurrentDictionary<Vector3I, MyCube>>();

            var blocksField = typeof(MyGridTerminalSystem).easyField("m_blocks");
			m_blocks = (x) => { return (HashSet<MyTerminalBlock>)blocksField.GetValue(x); };
            
			foreach (var x in IMPLEMENTATIONS)
			{
				foreach (var z in x.Value)
				{
                    if (impls.Contains(x.Key.Name + "-" + z))
                    {
                        Log.Warn($"Patching implementation: {x.Key} -> {z}");
                        patchContext.Prefix(x.Key, typeof(Optimizations), z);
                    }
                    else
                    {
                        Log.Warn($"Ingoring: {x.Key} -> {z}");
                    }
				}
			}


            // ==== 
            var co = OptimizerPlugin.Config.DisabledMethods;
            if (co.Length > 0)
            {
                var disables = OptimizerPlugin.Config.DisabledMethods.Split(' ');
                var types = Ext2.GetAllTypes();

                foreach (var x in disables)
                {
                    var s = x.Split('-');
                    if (s.Length != 2)
                    {
                        continue;
                    }
                    var type = s[0];
                    var method = s[1];


                    var m = types.Where(y=>y.Name == type).Select(y => y.easyMethod(method, false)).Where(y=>y!=null).ToList();
                    switch (m.Count)
                    {
                        case 1:
                            {
                                try
                                {
                                    Log.Warn($"Disabling method: {x}");
                                    patchContext.Disable (m[0]);
                                } catch (Exception e)
                                {
                                    Log.Fatal(e, $"Patching: {s} Failed");
                                }
                                break;
                            }
                        case 0:
                            Log.Warn($"Haven't found type/method to disable: {x}");
                            break;
                        default:
                            Log.Warn($"Found more than 1 type/method to disable: {x}");
                            break;

                    }
                }
            }
        }

        public static bool get_IsConstrained(MyInventory __instance, ref bool __result) {
            __result = true;
            return false;
        }
        
		public static bool IsNpcFaction(MyFactionCollection __instance, ref bool __result, string tag)
		{
			__result = tag.Length > 3;
			return false;
		}

        private static bool SpinOnce(ref MySpinWait __instance)
        {
            return MyUtils.MainThread != Thread.CurrentThread;
        }


        public static bool GetBlocksInsideSphere(MyCubeGrid __instance, BoundingSphereD sphere, HashSet<MySlimBlock> blocks, bool checkTriangles = false)
        {
            __instance.GetBlocksInsideSphereInternal(ref sphere, blocks, false);
            return false;
        }

        public static bool CanAddCubesImpl(ConcurrentDictionary<Vector3I, MyCube> m_cubes, Vector3I min, Vector3I max)
        {
            int minX = min.X;
            int maxX = max.X;
            int minY = min.Y;
            int maxY = max.Y;
            int minZ = min.Z;
            int maxZ = max.Z;

            if (minX > maxX)
            {
                maxX = min.X;
                minX = max.X;
            }

            if (minY > maxY)
            {
                maxY = min.Y;
                minY = max.Y;
            }

            if (minZ > maxZ)
            {
                maxZ = min.Z;
                minZ = max.Z;
            }

            var v = new Vector3I();
            for (var x = minX; x <= maxX; x++)
            {
                for (var y = minY; y <= maxY; y++)
                {
                    for (var z = minZ; z <= maxZ; z++)
                    {
                        v.X = x;
                        v.Y = y;
                        v.Z = z;
                        if (m_cubes.ContainsKey(v))
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }




        //[ReflectedGetter(Name = "m_blocks", Type = typeof(MyGridTerminalSystem))]
        private static Func<MyGridTerminalSystem, HashSet<MyTerminalBlock>> m_blocks;

		public static bool UpdateGridBlocksOwnership(MyGridTerminalSystem __instance, long ownerID) {
            IMyFaction myFaction = MySession.Static.Factions.TryGetPlayerFaction(ownerID);
            var isNPCFaction = myFaction != null && myFaction.Tag.Length > 3;
            IMyFaction myFaction2;

            foreach (MyTerminalBlock myTerminalBlock in m_blocks.Invoke(__instance)) {
                var id = myTerminalBlock.IDModule;
                if (id == null) {
                    myTerminalBlock.IsAccessibleForProgrammableBlock = true;
                    continue;
                } else {
                    if (id.Owner == ownerID) {
                        myTerminalBlock.IsAccessibleForProgrammableBlock = true;
                        continue;
                    } else {
                        if (id.ShareMode == VRage.Game.MyOwnershipShareModeEnum.All) {
                            myTerminalBlock.IsAccessibleForProgrammableBlock = true;
                            continue;
                        }

                        if (id.ShareMode == VRage.Game.MyOwnershipShareModeEnum.None) {
                            myTerminalBlock.IsAccessibleForProgrammableBlock = false;
                            continue;
                        }

                        if (myFaction == null || isNPCFaction) {
                            myTerminalBlock.IsAccessibleForProgrammableBlock = false;
                            continue;
                        } //faction share
                        else {
                            myFaction2 = MySession.Static.Factions.TryGetPlayerFaction(ownerID);
                            myTerminalBlock.IsAccessibleForProgrammableBlock = myFaction2 == myFaction;
                            continue;
                        }
                    }
                }
            }

            return false;
        }



        private static Dictionary<long, double> filledRatio = new Dictionary<long, double>();
        private static MethodInfo ChangeFilledRatio = typeof(MyGasTank).easyMethod("ChangeFilledRatio");
        private static bool ChangeFillRatioAmount(MyGasTank __instance, float newFilledRatio) {
            double value;
            if (newFilledRatio < 0.01) {
                return true;
            }
            
            if (filledRatio.TryGetValue(__instance.EntityId, out value)) {
                if (Math.Abs(newFilledRatio - value) < 0.005) {
                    ChangeFilledRatio.Invoke(__instance, new object[] {(double)newFilledRatio, false});
                    return false;
                }
                
                filledRatio[__instance.EntityId] = newFilledRatio;
                return true;

            } else {
                filledRatio.Add(__instance.EntityId, newFilledRatio);
                return true;
            }
        }



		internal static class FixRelations
		{

			public static bool GetRelationPlayerBlock(ref MyRelationsBetweenPlayerAndBlock __result, long owner, long user, MyOwnershipShareModeEnum share = MyOwnershipShareModeEnum.None, MyRelationsBetweenPlayerAndBlock noFactionResult = MyRelationsBetweenPlayerAndBlock.Enemies, MyRelationsBetweenFactions defaultFactionRelations = MyRelationsBetweenFactions.Enemies, MyRelationsBetweenPlayerAndBlock defaultShareWithAllRelations = MyRelationsBetweenPlayerAndBlock.FactionShare)
			{
				if (!MyFakes.SHOW_FACTIONS_GUI)
				{
					__result = MyRelationsBetweenPlayerAndBlock.NoOwnership;
					return false;
				}
				if (owner == user)
				{
					__result = MyRelationsBetweenPlayerAndBlock.Owner;
					return false;
				}
				if (owner == 0L || user == 0L)
				{
					__result = MyRelationsBetweenPlayerAndBlock.NoOwnership;
					return false;
				}
				IMyFaction myFaction = MySession.Static.Factions.TryGetPlayerFaction(user);
				IMyFaction myFaction2 = MySession.Static.Factions.TryGetPlayerFaction(owner);
				if (myFaction != null && myFaction == myFaction2 && share == MyOwnershipShareModeEnum.Faction)
				{
					__result = MyRelationsBetweenPlayerAndBlock.FactionShare;
					return false;
				}
				if (share == MyOwnershipShareModeEnum.All)
				{
					__result = defaultShareWithAllRelations;
					return false;
				}
				if (myFaction == null && myFaction2 == null)
				{
					__result = noFactionResult;
					return false;
				}

				var pp = GetRelationPlayerPlayer(user, myFaction, owner, myFaction2, defaultFactionRelations, ConvertToPlayerRelation(noFactionResult));
				__result = ConvertToPlayerBlockRelation(pp);
				return false;
			}

			public static MyRelationsBetweenPlayers GetRelationPlayerPlayer(long owner, IMyFaction myFaction2, long user, IMyFaction myFaction, MyRelationsBetweenFactions defaultFactionRelations = MyRelationsBetweenFactions.Enemies, MyRelationsBetweenPlayers defaultNoFactionRelation = MyRelationsBetweenPlayers.Enemies)
			{
				if (owner == user)
				{
					return MyRelationsBetweenPlayers.Self;
				}
				if (myFaction == null && myFaction2 == null)
				{
					return defaultNoFactionRelation;
				}
				if (myFaction == null)
				{
					return ConvertToPlayerRelation(MySession.Static.Factions.GetRelationBetweenPlayerAndFaction(user, myFaction2.FactionId).Item1);
				}
				if (myFaction2 == null)
				{
					return ConvertToPlayerRelation(MySession.Static.Factions.GetRelationBetweenPlayerAndFaction(owner, myFaction.FactionId).Item1);
				}

				var flag = myFaction.Tag.Length > 3;
				var flag2 = myFaction2.Tag.Length > 3;
				if (flag != flag2)
				{
					if (flag)
					{
						return ConvertToPlayerRelation(MySession.Static.Factions.GetRelationBetweenPlayerAndFaction(owner, myFaction.FactionId).Item1);
					}
					if (flag2)
					{
						return ConvertToPlayerRelation(MySession.Static.Factions.GetRelationBetweenPlayerAndFaction(user, myFaction2.FactionId).Item1);
					}
				}

				var reputation = TranslateRelationToReputation(defaultFactionRelations);
				return ConvertToPlayerRelation(MySession.Static.Factions.GetRelationBetweenFactions(myFaction2.FactionId, myFaction.FactionId, new Tuple<MyRelationsBetweenFactions, int>(defaultFactionRelations, reputation)).Item1);
			}

			private static MyRelationsBetweenPlayers ConvertToPlayerRelation(MyRelationsBetweenFactions factionRelation)
			{
				switch (factionRelation)
				{
					case MyRelationsBetweenFactions.Neutral: return MyRelationsBetweenPlayers.Neutral;
					case MyRelationsBetweenFactions.Enemies: return MyRelationsBetweenPlayers.Enemies;
					case MyRelationsBetweenFactions.Allies:
					case MyRelationsBetweenFactions.Friends: return MyRelationsBetweenPlayers.Allies;
					default: return MyRelationsBetweenPlayers.Enemies;
				}
			}

			internal static int TranslateRelationToReputation(MyRelationsBetweenFactions relation)
			{
				switch (relation)
				{
					case MyRelationsBetweenFactions.Neutral:
					case MyRelationsBetweenFactions.Allies: return 0;
					case MyRelationsBetweenFactions.Enemies: return -1000;
					case MyRelationsBetweenFactions.Friends: return 0;
					default: return 0;
				}
			}

			private static MyRelationsBetweenPlayerAndBlock ConvertToPlayerBlockRelation(MyRelationsBetweenPlayers playerRelation)
			{
				switch (playerRelation)
				{
					case MyRelationsBetweenPlayers.Self:
					case MyRelationsBetweenPlayers.Allies: return MyRelationsBetweenPlayerAndBlock.Friends;
					case MyRelationsBetweenPlayers.Neutral: return MyRelationsBetweenPlayerAndBlock.Neutral;
					case MyRelationsBetweenPlayers.Enemies: return MyRelationsBetweenPlayerAndBlock.Enemies;
					default: return MyRelationsBetweenPlayerAndBlock.Enemies;
				}
			}

			private static MyRelationsBetweenPlayers ConvertToPlayerRelation(MyRelationsBetweenPlayerAndBlock blockRelation)
			{
				switch (blockRelation)
				{
					case MyRelationsBetweenPlayerAndBlock.NoOwnership:
					case MyRelationsBetweenPlayerAndBlock.Neutral: return MyRelationsBetweenPlayers.Neutral;
					case MyRelationsBetweenPlayerAndBlock.Owner:
					case MyRelationsBetweenPlayerAndBlock.FactionShare:
					case MyRelationsBetweenPlayerAndBlock.Friends: return MyRelationsBetweenPlayers.Allies;
					case MyRelationsBetweenPlayerAndBlock.Enemies: return MyRelationsBetweenPlayers.Enemies;
					default: return MyRelationsBetweenPlayers.Enemies;
				}
			}
		}

	}
}