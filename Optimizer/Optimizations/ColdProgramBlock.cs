﻿using NAPI;
using Sandbox.Game.Entities.Blocks;
using Sandbox.ModAPI.Ingame;
using Slime.Slow;
using System;
using System.Collections.Generic;
using Torch.Managers.PatchManager;
using VRage.Utils;

namespace Slime.Features
{
    class ColdProgramBlock
    {
        static Dictionary<long, int> timers1 = new Dictionary<long, int>();
        static Dictionary<long, int> timers10 = new Dictionary<long, int>();
        static Dictionary<long, int> timers100 = new Dictionary<long, int>();

        static HashSet<MyStringHash> ignoredTimers = new HashSet<MyStringHash>();

        public static void InitHack(PatchContext patchContext)
        {
            var subs = OptimizerPlugin.Config.SlowPBIgnored.Split(new[] { ",", " " }, StringSplitOptions.None);
            foreach (var x in subs)
            {
                if (x.Length > 0)
                {
                    Log.Warn("Added " + x + " to ignored PB's");
                    ignoredTimers.Add(MyStringHash.GetOrCompute(x));
                }
            }

            patchContext.Prefix(typeof(MyProgrammableBlock), typeof(ColdProgramBlock), "ExecuteCode");
        }

        public static bool ExecuteCode(MyProgrammableBlock __instance, ref MyProgrammableBlock.ScriptTerminationReason __result, string argument, UpdateType updateSource, out string response)
        {
            __result = MyProgrammableBlock.ScriptTerminationReason.None;
            response = string.Empty;

            if (!OptimizerPlugin.Config.SlowPBEnabled) { return true; }

            if (ignoredTimers.Contains(__instance.BlockDefinition.Id.SubtypeId)) { return true; }

            if (updateSource == UpdateType.Update1)
            {
                var sl = OptimizerPlugin.Config.SlowPBUpdate1;
                if (sl == 1) { return true; } else { return Optimizations.Slow(__instance.EntityId, timers1, sl); }
            }
            else if (updateSource == UpdateType.Update10)
            {
                var sl = OptimizerPlugin.Config.SlowPBUpdate10;
                if (sl == 1) { return true; } else { return Optimizations.Slow(__instance.EntityId, timers10, sl); }
            }
            else if (updateSource == UpdateType.Update100)
            {
                var sl = OptimizerPlugin.Config.SlowPBUpdate100;
                if (sl == 1) { return true; } else { return Optimizations.Slow(__instance.EntityId, timers100, sl); }
            }

            return true;
        }
    }
}