﻿using System.Collections.Generic;
using Sandbox.Game.Entities;
using Slime.Slow;
using Torch.Managers.PatchManager;
using NAPI;

namespace Slime
{
    internal class OptimizationSafezone
    {

        private static readonly Dictionary<long, int> dictionary = new Dictionary<long, int>();
        public static void InitHack(PatchContext patchContext)
        {
            if (OptimizerPlugin.Config.EnabledSlowdownSafezoneEnabled) {
                patchContext.Prefix(typeof(MySafeZone), "UpdateBeforeSimulation", typeof(OptimizationSafezone), new[] { "__instance" });
            }
        }

        private static bool UpdateBeforeSimulation(ref MySafeZone __instance)
        {
            if (OptimizerPlugin.Config.EnabledSlowdownSafezoneEnabledTimes < 0) return true;
            if (OptimizerPlugin.Config.EnabledSlowdownSafezoneEnabledTimes == 0) return false;
            return Optimizations.Slow(__instance.EntityId, dictionary, OptimizerPlugin.Config.EnabledSlowdownSafezoneEnabledTimes);
        }
    }
}