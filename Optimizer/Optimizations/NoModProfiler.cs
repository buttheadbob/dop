﻿using System;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using NAPI;
using Torch.Managers.PatchManager;
using VRage.Scripting;
using VRage.Utils;

namespace Slime.Features
{
    public static class NoModProfiler
    {
        public static void PrePatch(PatchContext patchContext)
        {
            patchContext.Prefix(typeof(MyScriptCompiler), typeof(NoModProfiler), "InjectMod");
        }

        private static bool InjectMod(CSharpCompilation compilation, SyntaxTree syntaxTree, int modId, ref SyntaxTree __result)
        {
            __result = syntaxTree;
            return false;
        }
    }
}
