﻿using System;
using System.Collections.Generic;
using Torch.Managers.PatchManager;
using System.Windows.Controls;
using Torch.Commands;
using Slime.Features;
using Slime.Features.Commands;
using NAPI;
using Slime.GUI;

namespace TorchPlugin
{
    public class CommandsPlugin : CommonPlugin
    {
        public static List<Type> commands = new List<Type>()
        {
            typeof(FixGPS),
            typeof(FixSafezone),
            typeof(BlocksDamage),
           // typeof(DebugComands),
            typeof(EconomyCalculate),
            typeof(SearchCommands),
            typeof(EssentialsExtension),
            typeof(ConvertToStatic),
            typeof(FixClippingGrids),
        };


        public override void Patch(PatchContext patchContext)
        {
            var manager = Torch.CurrentSession?.Managers.GetManager(typeof(CommandManager)) as CommandManager;
            foreach (var x in commands)
            {
                manager.RegisterCommands(x, this);
            }
        }

        public override UserControl CreateControl()
        {
            return new CommandsGui();
        }
    }
}