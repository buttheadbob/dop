﻿using System.Collections.Generic;
using Torch.Commands;
using Sandbox.Game.World;
using VRage.Game.ModAPI;
using Sandbox.Game.Screens.Helpers;
using Slime.Utils;
using Torch.Commands.Permissions;
using NAPI;

namespace Slime.Features.Commands {
    [Category("fix")]
    class FixGPS : CommandModule {
        static Dictionary<ulong, long> lastActivated = new Dictionary<ulong, long>();

        [Command("gps", "Helps you, if your gpses, are not shown")]
        [Permission(MyPromoteLevel.None)]
        public void FixCharacterCommand() {
            if (!this.DelayAction(lastActivated, 300)) { return; }

            var list = new List<IMyGps>();
            MySession.Static.Gpss.GetGpsList(Context.Player.IdentityId, list);

            foreach (var x in list) { MySession.Static.Gpss.SendDeleteGpsRequest(Context.Player.IdentityId, x.Hash); }

            foreach (var x in list) {
                var t = x as MyGps;
                MySession.Static.Gpss.SendAddGpsRequest(Context.Player.IdentityId, ref t);
                //TODO check it
            }
        }

        [Command("static")]
        [Permission(MyPromoteLevel.None)]
        public void FixSmallGridsCommand() {
            var grids = UtilitesEntity.GetAllGrids();
            foreach (var x in grids) {
                if (x.IsStatic && x.GridSizeEnum == VRage.Game.MyCubeSize.Small) { x.RequestConversionToShip(null); }
            }
        }
    }
}