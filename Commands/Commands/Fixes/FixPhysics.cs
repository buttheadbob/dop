using System;
using System.Collections.Generic;
using System.Text;
using Torch.Commands;
using Sandbox.Game.World;
using VRage.Game.ModAPI;
using Sandbox.ModAPI;
using Torch.Commands.Permissions;
using NAPI;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Scripts.Shared;
using VRageMath;
using Torch;
using System.Linq;
using Sandbox.Engine.Multiplayer;
using VRage.Network;
using Sandbox.Game.Entities.Blocks;

namespace Slime.Features.Commands
{
    [Category("fix")]
    class FixClippingGrids : CommandModule
    {
        /*private static Func<MyClusterTree, Dictionary<ulong, object>> hkWorld = typeof(MyClusterTree).easyField("m_objectsData").CreateGetter<MyClusterTree, Dictionary<ulong, object>>();*/
        private ParallelTasks.Task m_fixClangTask;
        [Command("physics", "fixmode 1- only gps, 2 - gps + fix, 3 - gps+fix+dbg log, 4 - gps + fix + large slow moving grid.\ntostatic 0 - not convert, 1- convert only large grids.2 convert all.\nUPDATES_TO_TRACE - how many updates we will scan grids", "help text")]
        [Permission(MyPromoteLevel.Moderator)]
        public void FixClang(int fixmode, int tostatic = 0, int UPDATES_TO_TRACE = 60)
        {
            if (m_fixClangTask.IsComplete)
            {
                m_fixClangTask = MyAPIGateway.Parallel.StartBackground(() => FixClangTask(fixmode, tostatic, UPDATES_TO_TRACE, IsClangingOriginal));
            }
        }

        [Command("physics2 find", "Searching for clanging grids. Not fixing them")]
        [Permission(MyPromoteLevel.Moderator)]
        public void FixClangFind()
        {
            if (m_fixClangTask.IsComplete)
            {
                m_fixClangTask = MyAPIGateway.Parallel.StartBackground(() => FixClangTask(3, 0, 60, IsClangingNew));
            }
        }

        [Command("physics2 fix toStationAndSmall", "Searching for clanging grids. Converting to station even small ships")]
        [Permission(MyPromoteLevel.Moderator)]
        public void FixClangToStationAndSmall()
        {
            if (m_fixClangTask.IsComplete)
            {
                m_fixClangTask = MyAPIGateway.Parallel.StartBackground(() => FixClangTask(2, 3, 60, IsClangingNew));
            }
        }

        [Command("physics2 fix toStationLargeOnly", "Searching for clanging grids. Converting to station large only")]
        [Permission(MyPromoteLevel.Moderator)]
        public void FixClangToStationLargeOnly()
        {
            if (m_fixClangTask.IsComplete)
            {
                m_fixClangTask = MyAPIGateway.Parallel.StartBackground(() => FixClangTask(2, 2, 60, IsClangingNew));
            }
        }



        public void CountContacts()
        {
            //TODO OnContact event counter
        }

        public void FixClangTask(int mode, int tostatic, int UPDATES_TO_TRACE, Func<KeyValuePair<MyCubeGrid, ScanData>, bool> IsClangingCheck)
        {
            try
            {
                var ScanHive = new Dictionary<MyCubeGrid, ScanData>();
                ///-------------------------------------------------------------
                for (int i = 0; i < UPDATES_TO_TRACE; i++)
                {
                    TorchBase.Instance.InvokeBlocking(() => CollectData(i, ScanHive));
                }
                ///-----------------------------------------------------------------------
                var sb = new StringBuilder("FixClangGrids report start:");
                if (mode == 3)
                {
                    foreach (var susgrid in ScanHive)
                    {
                        sb.AppendLine("Grid: " + susgrid.Key);
                        sb.AppendLine("Position:");

                        foreach (var item in susgrid.Value.Position)
                        {
                            sb.AppendLine(item.ToString());
                        }
                        ///------
                        sb.AppendLine("AngularVelocity:");
                        foreach (var item in susgrid.Value.AngularVelocity)
                        {
                            sb.AppendLine(item.ToString());
                        }
                        ///------
                        sb.AppendLine("LinearVelocity:");
                        foreach (var item in susgrid.Value.LinearVelocity)
                        {
                            sb.AppendLine(item.ToString());
                        }
                    }
                }
                //TODO (f100.Position-f0.Positon).Length() < 10 - привязан к одному месту
                // SumLinearVelocity_f0_f100.Length() > frames - перемещается
                //GetAverageAngleBetweenVectors () > 15 (градусов)
                //var degree = Math.Acos (v1.Dot(v2) / (v1.Length() * v2.Length())).toDegree();
                if (!ScanHive.Any())
                {
                    Context.Respond("Nothing found.");
                    return;
                }
                else
                {
                    sb.AppendLine($"Traced grids: [{ScanHive.Count}].");
                }
                var tofix = new List<MyCubeGrid>();
                foreach (var grid in ScanHive)
                {
                    if (!IsClangingCheck(grid))
                    {
                        if (mode == 3) sb.AppendLine($"Grid {grid.Key.DisplayName} changing pos so its not clanging");
                    }
                    else
                    {
                        if (mode == 3)
                        {
                            TraceData (grid, sb);
                        }
                        if (mode != 4 && Voxels.VoxelDetectorForGrids(grid.Key))
                        {
                            //TODO Vector check?
                            tofix.Add(grid.Key);
                        }
                        else // show all slow moving ships near voxels, to find grids on voxels vibrating.
                            tofix.Add(grid.Key);
                    }
                }
                sb.AppendLine($"Possible clang'in grids:" + tofix.Count());

                //TODO convert to stations?
                //TODO subgrids for more safety?
                //TODO del voxels around?
                //TODO var OnlinePlayers = MySession.Static.Players.GetOnlinePlayers();
                foreach (var g in tofix)
                {
                    sb.AppendLine($"Will fix grid: " + g.DisplayName);
                    if (Context.Player != null)
                    {
                        MyVisualScriptLogicProvider.AddGPS("ClangingGrid: " + g.DisplayName, "ADMIN TOOLS", g.PositionComp.GetPosition(), Color.Lime, 120, Context.Player.IdentityId);
                    }
                }

                if (mode != 1)
                {
                    TorchBase.Instance.InvokeBlocking(() => FixGrids(tofix, tostatic));
                }
                sb.AppendLine("Command end.");
                Context.Respond(sb.ToString());
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[FixClangTask Exception]");
            }
        }

        

        private static void CollectData(int i, Dictionary<MyCubeGrid, ScanData> ScanHive)
        {
            try
            {
                List<MyCubeGrid> grids;
                if (i == 0)
                {
                    grids = GetGridsForCheck();
                }
                else
                {
                    grids = ScanHive.Keys.ToList();
                }

                foreach (var grid in grids)
                {
                    if (grid == null || grid.MarkedForClose || grid.Closed || grid.Physics == null) continue;
                    if (i == 0)
                    {
                        ScanHive[grid] = new ScanData();
                    }
                    ScanHive[grid].AngularVelocity.Add(grid.Physics.AngularVelocity);
                    ScanHive[grid].Position.Add(grid.PositionComp.GetPosition());
                    ScanHive[grid].LinearVelocity.Add(grid.Physics.LinearVelocity);
                }
            }
            catch (Exception ex) { Log.Error(ex, "FixClangTask InvokeBlocking catch!"); }
        }

        private static bool IsClangingOriginal (KeyValuePair<MyCubeGrid, ScanData> grid)
        {
            var temp = (grid.Value.Position.First() - grid.Value.Position.Last()).LengthSquared();
            return temp <= 20;
        }

        private static bool IsClangingNew(KeyValuePair<MyCubeGrid, ScanData> grid)
        {
            var temp = (grid.Value.Position.First() - grid.Value.Position.Last()).LengthSquared();
            return temp <= 20;
        }

        private static void TraceData (KeyValuePair<MyCubeGrid, ScanData> grid, StringBuilder sb)
        {
            sb.AppendLine($"LinearVelocity degrees:");
            for (int j1 = 0; j1 < grid.Value.LinearVelocity.Count() - 1; j1++)
            {
                var degree = Math.Acos(grid.Value.LinearVelocity[j1].Dot(grid.Value.LinearVelocity[j1 + 1]) / (grid.Value.LinearVelocity[j1].Length() * grid.Value.LinearVelocity[j1 + 1].Length()));
                sb.AppendLine(degree.ToString());
            }
            //------------------------
            sb.AppendLine($"Angular velocity delta:");
            Vector3D AngularAverage = Vector3D.Zero;
            for (int j2 = 0; j2 < grid.Value.AngularVelocity.Count(); j2++)
            {
                AngularAverage += grid.Value.AngularVelocity[j2];
            }
            AngularAverage /= grid.Value.AngularVelocity.Count();
            sb.AppendLine(AngularAverage.ToString());
            //----------------------------
            sb.AppendLine("Linear velocity delta:");
            Vector3D LinearAverage = Vector3D.Zero;
            for (int j3 = 0; j3 < grid.Value.LinearVelocity.Count(); j3++)
            {
                LinearAverage += grid.Value.LinearVelocity[j3];
            }
            LinearAverage /= grid.Value.LinearVelocity.Count();
            sb.AppendLine(LinearAverage.ToString());
        }

        private void FixGrids(List<MyCubeGrid> tofix, int tostatic)
        {
            try
            {
                foreach (var g in tofix)
                {
                    g.Physics.ClearSpeed();
                    g.Physics.SetSpeeds(Vector3D.Zero, Vector3D.Zero);
                    g.Physics.RigidBody.AngularDamping = 0f;
                    g.Physics.RigidBody.LinearDamping = 0f;
                    //g.Physics.RigidBody.AllowedPenetrationDepth = 0f;
                    g.Physics.RigidBody.IsActive = false;

                    if (tostatic != 0)
                    {
                        if ((tostatic == 1 && g.GridSizeEnum == VRage.Game.MyCubeSize.Large) || (tostatic == 2))
                        {
                            g.ConvertToStatic();
                            try
                            {
                                MyMultiplayer.RaiseEvent<MyCubeGrid>(g, (MyCubeGrid x) => new Action(x.ConvertToStatic), default(EndpointId));
                                foreach (var player in MySession.Static.Players.GetOnlinePlayers())
                                {
                                    MyMultiplayer.RaiseEvent<MyCubeGrid>(g, (MyCubeGrid x) => new Action(x.ConvertToStatic), new EndpointId(player.Id.SteamId));
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex, "()Exception in RaiseEvent.");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Exception in last invoke fix call.");
            }
        }


        private static List<MyCubeGrid> GetGridsForCheck()
        {
            return UtilitesEntity.GetAllGrids((x) =>
            {
                var p = x.Physics;
                if (p == null) return false;
                if (p.IsStatic) return false;
                if (!p.IsActive) return false;
                if (p.Gravity.LengthSquared() == 0) return false;
                if (p.LinearVelocity.LengthSquared() == 0 || p.LinearVelocity.LengthSquared() > 900f) return false; //is not moving at all, or moving too fast

                if (x.BlocksCount == 1 && IsOnlyParts(x)) return false;

                TryGetPlanetElevation(x, Sandbox.ModAPI.Ingame.MyPlanetElevation.Surface, out var elevation);
                if (elevation > 300) return false;
                return true;
            });
        }

        private static bool IsOnlyParts(MyCubeGrid Grid)
        {
            var FatBlocks = Grid.CubeBlocks.Where
                (x => (x.FatBlock != null));
            var blocks = FatBlocks.Where
                (x => x.FatBlock is IMyMotorAdvancedRotor || x.FatBlock is IMyMotorAdvancedStator || x.FatBlock is IMyMotorBase || x.FatBlock is IMyMotorRotor || x.FatBlock is IMyMotorStator || x.FatBlock is IMyMotorSuspension || x.FatBlock is IMyWheel || x.FatBlock is IMyPistonBase || x.FatBlock is IMyPistonTop || x.FatBlock is IMyExtendedPistonBase || x.FatBlock is MyWheel);

            return FatBlocks.Count() == blocks.Count();
        }

        private static bool TryGetPlanetElevation(MyCubeGrid grid, Sandbox.ModAPI.Ingame.MyPlanetElevation detail, out double elevation)
        {
            var boundingBox = grid.PositionComp.WorldAABB;
            var nearestPlanet = MyGamePruningStructure.GetClosestPlanet(ref boundingBox);
            if (nearestPlanet == null)
            {
                elevation = double.PositiveInfinity;
                return false;
            }

            switch (detail)
            {
                case Sandbox.ModAPI.Ingame.MyPlanetElevation.Sealevel:
                    elevation = ((boundingBox.Center - nearestPlanet.PositionComp.GetPosition()).Length() - nearestPlanet.AverageRadius);
                    return true;

                case Sandbox.ModAPI.Ingame.MyPlanetElevation.Surface:
                    var controlledEntityPosition = grid.Physics.CenterOfMassWorld;
                    Vector3D closestPoint = nearestPlanet.GetClosestSurfacePointGlobal(ref controlledEntityPosition);
                    elevation = Vector3D.Distance(closestPoint, controlledEntityPosition);
                    return true;

                default:
                    throw new ArgumentOutOfRangeException("detail", detail, null);
            }
        }

        public class ScanData
        {
            public List<Vector3D> AngularVelocity = new List<Vector3D>();
            public List<Vector3D> LinearVelocity = new List<Vector3D>();
            public List<Vector3D> Position = new List<Vector3D>();
        }
    }
}