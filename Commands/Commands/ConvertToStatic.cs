﻿using Torch.Commands;
using VRage.Game.ModAPI;
using Torch.Commands.Permissions;
using VRage.ModAPI;
using NAPI;

namespace Slime.Features.Commands
{
	[Category("!convert")]
    class ConvertToStatic : CommandModule {
        public const EntityFlags CONVERT_TO_DYNAMIC = (EntityFlags) (1 << 29); //top of MyEntityFlags. Hopefully Keen doesn't add another dozen flags

        [Command("static small", "Convert to static all small ships")]
        [Permission(MyPromoteLevel.Admin)]
        public void MakeStatic () {
            var grids = UtilitesEntity.GetAllGrids();
            foreach (var x in grids) {
                if (x.GridSizeEnum != VRage.Game.MyCubeSize.Small) continue;

                if (!x.IsStatic) {
                    x.ConvertToStatic();
                    x.Flags |= CONVERT_TO_DYNAMIC;
                }
            }
            
            Context.Respond("Please use !convert dynamic small, before game save");
        }
        
        [Command("dynamic small", "Convert all small ships to dynamic")]
        [Permission(MyPromoteLevel.Admin)]
        public void MakeDynamic() {
            var grids = UtilitesEntity.GetAllGrids();
            foreach (var x in grids) {
                if (x.GridSizeEnum != VRage.Game.MyCubeSize.Small) continue;
                
                if ((x.Flags & CONVERT_TO_DYNAMIC) == CONVERT_TO_DYNAMIC) {
                    x.RequestConversionToShip(null);
                    x.Flags = (EntityFlags) (x.Flags - CONVERT_TO_DYNAMIC);
                }
            }
        }
    }
}