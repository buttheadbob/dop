using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Sandbox.Game.Entities;
using Sandbox.Game.World;
using Torch.Commands;
using Sandbox.Game.EntityComponents;
using Vector3D = VRageMath.Vector3D;
using Sandbox.Game;
using Sandbox.ModAPI;
using VRage.Game.ModAPI;
using Slime.Utils;
using NAPI;
using Torch.Commands.Permissions;

namespace Slime.Features {
    [Category("cleanup")]
    class EssentialsExtension : CommandModule {

        [Command("nobuiltby", "Destroy every block, that dont have built by")]
        [Permission(MyPromoteLevel.Admin)]
        public void NoBuiltBy()
        {
            Context.Respond("Cleaning up started");
            var blocks = UtilitesEntity.GetBlocksFromAllGridsParalell((x) => x.BuiltBy == 0, UtilitesEntity.IsAdminGrid);
            Context.Respond("Cleaning up " + blocks.Count + " blocks");

            foreach (var x in blocks) { x.CubeGrid.RazeBlock(x.Position); }

            Context.Respond("Cleaning up - Done");
        }

        [Command("nobuiltbyfix", "Add buildby as grid owner on every block, that dont have builtby,\nbut skip all Admin Owned Grids, and Wormhole Gates")]
        [Permission(MyPromoteLevel.Admin)]
        public void NoBuiltByFix()
        {
            var list = new List<IMyIdentity>();
            MyAPIGateway.Players.GetAllIdentites(list, identity => true);
            var set = list.Select((x) => x.IdentityId).ToHashSet();

            Context.Respond("Starting BuildBy fix!");
            var blocks = UtilitesEntity.GetBlocksFromAllGridsParalell((x) => x.BuiltBy == 0, UtilitesEntity.IsAdminGrid);

            if (blocks.Count == 0)
            {
                Context.Respond($"Found {blocks.Count} No BuildBy blocks! All good! no need to fix.");
                return;
            }

            Context.Respond($"Found {blocks.Count} No BuildBy blocks, BuildBy 0, fixing");

            try
            {
                var Count0Blocks = 0;
                foreach (var x in blocks)
                {
                    if (x.CubeGrid != null && x.CubeGrid.BigOwners != null && x.CubeGrid.BigOwners.FirstOrDefault() != 0)
                    {
                        x.CubeGrid.TransferBlocksBuiltByID(0, x.CubeGrid.BigOwners.FirstOrDefault());
                        Count0Blocks++;
                    }
                }
                Context.Respond($"Managed to fix BuildBy 0: {Count0Blocks} out of total : {blocks.Count}");
            }
            catch
            {
                Context.Respond("There was CRASH during fixing buildby 0, try again.");
            }

            var blocksLostBuildBy = UtilitesEntity.GetBlocksFromAllGridsParalell((x) => !set.Contains(x.BuiltBy), UtilitesEntity.IsAdminGrid);

            if (blocksLostBuildBy.Count == 0)
            {
                Context.Respond($"Found {blocksLostBuildBy.Count} BuildBy Blocks with Not Existing Players in Save! All good! no need to fix.");
                return;
            }

            Context.Respond($"Found {blocksLostBuildBy.Count} BuildBy Blocks with Not Existing Players in Save, fixing");

            try
            {
                var CountMissingBuildByBlocks = 0;
                foreach (var x in blocksLostBuildBy)
                {
                    if (x.CubeGrid != null && x.CubeGrid.BigOwners != null && x.CubeGrid.BigOwners.FirstOrDefault() != 0)
                    {
                        x.CubeGrid.TransferBlocksBuiltByID(x.BuiltBy, x.CubeGrid.BigOwners.FirstOrDefault());
                        CountMissingBuildByBlocks++;
                    }
                }

                Context.Respond($"Managed to fix BuildBy Blocks with Not existing Player : {CountMissingBuildByBlocks} out of total : {blocksLostBuildBy.Count}");
            }
            catch
            {
                Context.Respond("There was CRASH during fixing not existing players, try again.");
            }
        }

        [Command("nobuiltby-info", "Count every block, that dont have built by")]
        [Permission(MyPromoteLevel.Admin)]
        public void NoBuiltByInfo()
        {
            Context.Respond("Counting no BuildBy started");
            var blocks = UtilitesEntity.GetBlocksFromAllGridsParalell((x) => x.BuiltBy == 0, UtilitesEntity.IsAdminGrid);
            Context.Respond("NoBuiltBy Blocks: " + blocks.Count);
        }

        [Command("nobuiltby2", "Destroy every block, that dont have built by")]
        [Permission(MyPromoteLevel.Admin)]
        public void NoBuiltBy2()
        {
            Context.Respond("Cleaning up started");
            var blocks = UtilitesEntity.GetBlocksFromAllGridsParalell((x) => x.BuiltBy == 0, UtilitesEntity.IsAdminGrid);
            Context.Respond("Cleaning up " + blocks.Count + " blocks");
            UtilitiesDamageIntegrity.DamageBlocks2(blocks, 99999999f, -1f);
            Context.Respond("Cleaning up - Done");
        }
        
        [Command("nobuiltby3", "Destroy every block, that dont have built by")]
        [Permission(MyPromoteLevel.Admin)]
        public void NoBuiltBy3()
        {
            var list = new List<IMyIdentity>();
            MyAPIGateway.Players.GetAllIdentites(list, identity => true);
            var set = list.Select((x)=>x.IdentityId).ToHashSet();
            
            Context.Respond("Cleaning up started");
            var blocks = UtilitesEntity.GetBlocksFromAllGridsParalell((x) => !set.Contains(x.BuiltBy), UtilitesEntity.IsAdminGrid);
            Context.Respond("Cleaning up " + blocks.Count + " blocks");
            UtilitiesDamageIntegrity.DamageBlocks2(blocks, 99999999f, -1f);
            Context.Respond("Cleaning up - Done");
        }
        
        [Command("nobuiltby3-info", "Count every block, that dont have built by")]
        [Permission(MyPromoteLevel.Admin)]
        public void NoBuiltByInfo3()
        {
            var list = new List<IMyIdentity>();
            MyAPIGateway.Players.GetAllIdentites(list, identity => true);
            var set = list.Select((x) => x.IdentityId).ToHashSet();

            Context.Respond("Counting no BuildBy started");
            var blocks = UtilitesEntity.GetBlocksFromAllGridsParalell((x) => !set.Contains(x.BuiltBy), UtilitesEntity.IsAdminGrid);
            Context.Respond("NoBuiltBy Blocks: " + blocks.Count);

            SearchCommands.ClusterShow(Context, blocks, 6000);
        }

        [Command("nobuiltby4-info", "Count every block, that dont have builtBy\nIncluding Admin Owned Grids")]
        [Permission(MyPromoteLevel.Admin)]
        public void NoBuiltByInfo4()
        {
            var list = new List<IMyIdentity>();
            MyAPIGateway.Players.GetAllIdentites(list, identity => true);
            var set = list.Select((x) => x.IdentityId).ToHashSet();

            Context.Respond("Counting no BuildBy Including Admin Owned Grids! started");
            var blocks = UtilitesEntity.GetBlocksFromAllGridsParalell((x) => !set.Contains(x.BuiltBy), UtilitesEntity.IsAdminGrid);
            Context.Respond("NoBuiltBy Blocks: " + blocks.Count);
            
            SearchCommands.ClusterShow(Context, blocks, 6000);
        }

        [Command("notify", "Notifies players that grid will be deleted")]
        [Permission(MyPromoteLevel.Admin)]
        public void NotifyPlayers(string format = "Grid {0} will be soon removed. Please place gridcore")
        {
            var data = Context.Args;
            if (data.Count > 0) data.RemoveAt(0);

            var grids = ScanConditions(Context.Args).OrderBy(g => g.DisplayName).ToList();

            var list = new List<IMyPlayer>();
            MyAPIGateway.Multiplayer.Players.GetPlayers(list);

            HashSet<long> onlinePlayers = new HashSet<long>();
            foreach (var x in list) { onlinePlayers.Add(x.IdentityId); }

            HashSet<IMyFaction> factions = new HashSet<IMyFaction>();
            foreach (var x in grids) {
                if (x.BigOwners.Count > 0) {
                    factions.Clear();
                    foreach (var o in x.BigOwners) {
                        var f = MyAPIGateway.Session.Factions.TryGetPlayerFaction(o);
                        if (f == null) { MyVisualScriptLogicProvider.SendChatMessage(String.Format(format, x.DisplayName), "Server", o, "Red"); } else { factions.Add(f); }
                    }

                    foreach (var f in factions) {
                        foreach (var m in f.Members) {
                            if (onlinePlayers.Contains(m.Key)) { MyVisualScriptLogicProvider.SendChatMessage(String.Format(format, x.DisplayName), "Server", m.Key, "Red"); }
                        }
                    }
                }
            }
        }

        [Command("item-stacks", "Find big amount of item stacks in inventories.\nRemoves `PhysicalGunObject` by default")]
        [Permission(MyPromoteLevel.Admin)]
        public void SearchItemStacks(int maxOkItems = 100, string typeToDelete = "PhysicalGunObject")
        { //
            typeToDelete = "MyObjectBuilder_" + typeToDelete;

            var blocks = UtilitesEntity.GetBlocksFromAllGrids((b) => {
                var fat = b.FatBlock;
                if (fat == null || !fat.HasInventory) return false;
                for (var x = 0; x < fat.InventoryCount; x++) {
                    if (fat.GetInventory(x).ItemCount > maxOkItems) {
                        var inv = fat.GetInventory(x);
                        var l = inv.ItemCount;

                        Context.Respond("Found block with " + fat.GetInventory(x).ItemCount + " items : " + fat.WorldMatrix.Translation + " entity=" + fat.EntityId + " owner=" + fat.OwnerId);
                        try {
                            for (var y = 0; y < l; y++) {
                                var ii = inv.GetItemAt(y);
                                if (ii.HasValue && ii.Value.Type.TypeId == typeToDelete) {
                                    inv.RemoveItemsAt(y, null);
                                    y--;
                                    l--;
                                }
                            }

                            /*if (fat.GetInventory(x).ItemCount > maxOkItems) {
                                Context.Respond ("Still alot of items ("+fat.GetInventory(x).ItemCount+"). Making full clean");
                                fat.GetInventory(x).ClearItems();
                            }*/
                        } catch (Exception e) { Log.Error(e); }
                    }
                }

                return false;
            });
        }

        public EssentialsExtension() {
            var methods = typeof(EssentialsExtension).GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);
            _conditionLookup = new List<Condition>(methods.Length);
            foreach (var m in methods) {
                var a = m.GetCustomAttribute<ConditionAttribute>();
                if (a == null) continue;

                var c = new Condition(m, a);

                _conditionLookup.Add(c);
            }
        }

        private List<Condition> _conditionLookup;

        private IEnumerable<MyCubeGrid> ScanConditions(IReadOnlyList<string> args) {
            var conditions = new List<Func<MyCubeGrid, bool?>>();

            for (var i = 0; i < args.Count; i++) {
                string parameter;
                if (i + 1 >= args.Count) { parameter = null; } else { parameter = args[i + 1]; }

                var arg = args[i];


                if (parameter != null) {
                    //parameter is the name of a command. Assume this command requires no parameters
                    if (_conditionLookup.Any(c => parameter.Equals(c.Command, StringComparison.CurrentCultureIgnoreCase) || parameter.Equals(c.InvertCommand, StringComparison.CurrentCultureIgnoreCase))) { parameter = null; }
                    //next string is a parameter, so pass it to the condition and skip it next loop
                    else i++;
                }

                bool found = false;

                foreach (var condition in _conditionLookup) {
                    if (arg.Equals(condition.Command, StringComparison.CurrentCultureIgnoreCase)) {
                        conditions.Add(g => condition.Evaluate(g, parameter, false, this));
                        found = true;
                        break;
                    } else if (arg.Equals(condition.InvertCommand, StringComparison.CurrentCultureIgnoreCase)) {
                        conditions.Add(g => condition.Evaluate(g, parameter, true, this));
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    Context.Respond($"Unknown argument '{arg}'");
                    yield break;
                }
            }

            //default scan to find grids without pilots
            if (!args.Contains("haspilot", StringComparer.CurrentCultureIgnoreCase)) conditions.Add(g => !Piloted(g));

            foreach (var group in MyCubeGridGroups.Static.Logical.Groups) {
                //if (group.Nodes.All(grid => conditions.TrueForAll(func => func(grid.NodeData))))
                bool res = true;
                foreach (var node in group.Nodes) {
                    if (node.NodeData.Projector != null) continue;

                    foreach (var c in conditions) {
                        bool? r = c.Invoke(node.NodeData);
                        if (r == null) yield break;
                        if (r == true) continue;

                        res = false;
                        break;
                    }

                    if (!res) break;
                }

                if (res)
                    foreach (var grid in group.Nodes.Where(x => x.NodeData.Projector == null))
                        yield return grid.NodeData;
            }
        }

        [Condition("name", helpText: "Finds grids with a matching name. Accepts regex format.")]
        public bool NameMatches(MyCubeGrid grid, string str) {
            if (string.IsNullOrEmpty(grid.DisplayName)) return false;

            var regex = new Regex(str);
            return regex.IsMatch(grid.DisplayName);
        }

        [Condition("blockslessthan", helpText: "Finds grids with less than the given number of blocks.")]
        public bool BlocksLessThan(MyCubeGrid grid, int count) { return grid.BlocksCount < count; }

        [Condition("blocksgreaterthan", helpText: "Finds grids with more than the given number of blocks.")]
        public bool BlocksGreaterThan(MyCubeGrid grid, int count) { return grid.BlocksCount > count; }

        [Condition("haspower", "nopower", "Finds grids with, or without power.")]
        public bool HasPower(MyCubeGrid grid) {
            foreach (var b in grid.GetFatBlocks()) {
                var c = b.Components?.Get<MyResourceSourceComponent>();
                if (c == null) continue;

                //some sources don't have electricity and Keen apparently doesn't know what TryGetValue is
                if (!c.ResourceTypes.Contains(MyResourceDistributorComponent.ElectricityId)) continue;

                if (c.HasCapacityRemainingByType(MyResourceDistributorComponent.ElectricityId) && c.ProductionEnabledByType(MyResourceDistributorComponent.ElectricityId)) return true;
            }

            return false;
        }

        [Condition("insideplanet", helpText: "Finds grids that are trapped inside planets.")]
        public bool InsidePlanet(MyCubeGrid grid) {
            var s = grid.PositionComp.WorldVolume;
            var voxels = new List<MyVoxelBase>();
            MyGamePruningStructure.GetAllVoxelMapsInSphere(ref s, voxels);

            if (!voxels.Any()) return false;

            foreach (var v in voxels) {
                var planet = v as MyPlanet;
                if (planet == null) continue;

                var dist2center = Vector3D.DistanceSquared(s.Center, planet.PositionComp.WorldVolume.Center);
                if (dist2center <= (planet.MaximumRadius * planet.MaximumRadius) / 2) return true;
            }

            return false;
        }

        [Condition("playerdistancelessthan", "playerdistancegreaterthan", "Finds grids that are further than the given distance from players.")]
        public bool PlayerDistanceLessThan(MyCubeGrid grid, double dist) {
            dist *= dist;
            foreach (var player in MySession.Static.Players.GetOnlinePlayers()) {
                if (Vector3D.DistanceSquared(player.GetPosition(), grid.PositionComp.GetPosition()) < dist) return true;
            }

            return false;
        }

        [Condition("ownedby", helpText: "Finds grids owned by the given player. Can specify player name, IdentityId, 'nobody', or 'pirates'.")]
        public bool OwnedBy(MyCubeGrid grid, string str) {
            long identityId;

            if (string.Compare(str, "nobody", StringComparison.InvariantCultureIgnoreCase) == 0) { return grid.BigOwners.Count == 0; }

            if (string.Compare(str, "pirates", StringComparison.InvariantCultureIgnoreCase) == 0) { identityId = MyPirateAntennas.GetPiratesId(); } else {
                var player = Ext2.GetPlayerByNameOrId(str);
                if (player == null) {
                    if (long.TryParse(str, out long NPCId)) {
                        if (MySession.Static.Players.IdentityIsNpc(NPCId)) { return grid.BigOwners.Contains(NPCId); }
                    }

                    return false;
                }

                identityId = player.IdentityId;
            }

            return grid.BigOwners.Contains(identityId);
        }

        [Condition("hastype", "notype", "Finds grids containing blocks of the given type.")]
        public bool BlockType(MyCubeGrid grid, string str) { return grid.HasBlockType(str); }

        [Condition("hassubtype", "nosubtype", "Finds grids containing blocks of the given subtype.")]
        public bool BlockSubType(MyCubeGrid grid, string str) { return grid.HasBlockSubtype(str); }

        [Condition("haspilot", "Finds grids with pilots")]
        public bool Piloted(MyCubeGrid grid) { return grid.GetFatBlocks().OfType<MyCockpit>().Any(b => b.Pilot != null); }

        /// <summary>
        /// Removes pilots from grid before deleting, 
        /// so the character doesn't also get deleted and break everything
        /// </summary>
        /// <param name="grid"></param>
        public void EjectPilots(MyCubeGrid grid) {
            var b = grid.GetFatBlocks<MyCockpit>();
            foreach (var c in b) { c.RemovePilot(); }
        }

        private class Condition {
            public string Command;
            public string InvertCommand;
            public string HelpText;
            private MethodInfo _method;
            public readonly ParameterInfo Parameter;

            public Condition(MethodInfo evalMethod, ConditionAttribute attribute) {
                Command = attribute.Command;
                InvertCommand = attribute.InvertCommand;
                HelpText = attribute.HelpText;
                _method = evalMethod;
                if (_method.ReturnType != typeof(bool)) throw new TypeLoadException("Condition does not return a bool!");
                var p = _method.GetParameters();
                if (p.Length < 1 || p[0].ParameterType != typeof(MyCubeGrid)) throw new TypeLoadException("Condition does not accept MyCubeGrid as first parameter");
                if (p.Length > 2) throw new TypeLoadException("Condition can only have two parameters");
                if (p.Length == 1) Parameter = null;
                else Parameter = p[1];
            }

            public bool? Evaluate(MyCubeGrid grid, string arg, bool invert, EssentialsExtension module) {
                var context = module.Context;
                bool result;
                if (!string.IsNullOrEmpty(arg) && Parameter == null) {
                    context.Respond($"Condition does not accept an argument. Cannot continue!");
                    return null;
                }

                if (string.IsNullOrEmpty(arg) && Parameter != null && !Parameter.HasDefaultValue) {
                    context.Respond($"Condition requires an argument! {Parameter.ParameterType.Name}: {Parameter.Name} Not supplied, cannot continue!");
                    return null;
                }

                if (Parameter != null && !string.IsNullOrEmpty(arg)) {
                    if (!arg.TryConvert(Parameter.ParameterType, out object val)) {
                        context.Respond($"Could not parse argument!");
                        return null;
                    }

                    result = (bool) _method.Invoke(module, new[] {grid, val});
                } else { result = (bool) _method.Invoke(module, new object[] {grid}); }

                return result != invert;
            }
        }

        [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
        private sealed class ConditionAttribute : Attribute {
            public string Command;
            public string InvertCommand;
            public string HelpText;

            public ConditionAttribute(string command, string invertCommand = null, string helpText = null) {
                Command = command;
                InvertCommand = invertCommand;
                HelpText = helpText;
            }
        }
    }
}