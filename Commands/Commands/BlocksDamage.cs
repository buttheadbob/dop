using Torch.Commands;
using System;
using VRage.ObjectBuilders;
using Slime.Utils;
using NAPI;
using Sandbox.Game.Entities;

using System.Collections.Generic;

using SharedLib;

using Sandbox.Game.Entities.Cube;

using VRage.Game.ModAPI;

using Sandbox.ModAPI;

using Torch.Commands.Permissions;



namespace Slime.Features
{

    class BlocksDamage : CommandModule
    {

        [Command("damage", "(OBSOLETE) Damage block by integrity, if FatOnly - only functional blocks but it is MUCH FASTER")]
        [Permission(MyPromoteLevel.Admin)]
        public void DamageBlocks2(string like, float integrityDamage, float minIntegrity = -1, bool fatOnly = false)

        {
            Damage2(like, integrityDamage, minIntegrity, fatOnly);
        }

        public void Damage2(string like, float integrityDamage, float minimum, bool fatOnly)
        {
            try
            {
                var blocksToDamage = fatOnly ? UtilitesEntity.GetFatBlocksFromAllGrids((b) => b.idIsLike(like)) : UtilitesEntity.GetBlocksFromAllGrids((b) => b.idIsLike(like));
                UtilitiesDamageIntegrity.DamageBlocks2(blocksToDamage, integrityDamage, minimum);
                Context.Respond($"Damaged {blocksToDamage.Count} blocks by {integrityDamage} integrity.");
            }
            catch (Exception e)
            {
                Log.Error($"ERROR: {e.Message}");
            }
        }


        [Command("damage-blocks", "See: (!damage). like & excepet - newFormat: `*/*Armor*` `*Battery*/*Tier2*` percentDamage in [0-1]")]
        [Permission(MyPromoteLevel.Admin)]
        public void DamageBlocks2(string like, float absoluteDamage, float minIntegrity = -1, bool fatOnly = false, string except = "", float percentDamage = 0f, bool enabledOnly = false)

        {
            var likeMatcher = new BlockIdMatcher(like);
            var exceptMatcher = new BlockIdMatcher(except);

            try
            {
                var blocksToDamage = fatOnly ? UtilitesEntity.GetFatBlocksFromAllGrids((b) => (enabledOnly ? b.isEnabledAndFunctional() : true) && likeMatcher.Matches(b.BlockDefinition.Id) && !exceptMatcher.Matches(b.BlockDefinition.Id)) : UtilitesEntity.GetBlocksFromAllGrids((b) => (enabledOnly ? b.isEnabledAndFunctional() : true) && likeMatcher.Matches(b.BlockDefinition.Id) && !exceptMatcher.Matches(b.BlockDefinition.Id));

                UtilitiesDamageIntegrity.DamageBlocks2(blocksToDamage, absoluteDamage, minIntegrity);
                Context.Respond($"Damaged {blocksToDamage.Count} blocks.");
            }
            catch (Exception e)
            {
                Log.Error($"ERROR: {e.Message}");
            }

            Damage2(like, absoluteDamage, minIntegrity, fatOnly);
        }

        // - вфьф
        [Command("damage-blocks-if-no-other", "Damage block found by `likeSecond` ONLY if there is no blocks found by `likeFirst`, if FatOnly - only functional blocks but it is MUCH FASTER : Example `!damage-blocks-if-no-other \"*/*Beacon*\" \"*/*Battery*\" 300000 -1 true` - destroyes ")]

        [Permission(MyPromoteLevel.Admin)]

        public void Damage3(string likeFirst, string likeSecond, float integrityDamage, float minIntegrity = -1, bool fatOnly = false, bool enabledOnly = false)

        {

            var matcher1 = new BlockIdMatcher(likeFirst);

            var matcher2 = new BlockIdMatcher(likeSecond);



            var gg = UtilitesEntity.GetAllGridGroups();



            var all = new List<MySlimBlock>();



            var now = SharpUtils.msTimeStamp();

            var list1 = new List<MySlimBlock>();

            var list2 = new List<MySlimBlock>();

            foreach (var x in gg)

            {

                list1.Clear();

                list2.Clear();

                foreach (var y in x)

                {

                    if (fatOnly)

                    {

                        foreach (var z in (y as MyCubeGrid).GetFatBlocks())

                        {

                            if (enabledOnly)

                            {

                                var ff = z as IMyFunctionalBlock;

                                if (ff == null || !ff.Enabled || !ff.IsFunctional) continue;

                            }

                            if (matcher2.Matches(z.BlockDefinition.Id))

                            {

                                list2.Add(z.SlimBlock);

                            }

                            else if (matcher1.Matches(z.BlockDefinition.Id))

                            {

                                list1.Add(z.SlimBlock);

                            }

                        }

                    }

                    else

                    {

                        var blocks = new List<IMySlimBlock>();

                        y.GetBlocks(blocks);

                        foreach (var z in blocks)

                        {

                            if (enabledOnly)

                            {

                                var fat = z.FatBlock;

                                if (fat == null) continue;

                                var func = fat as IMyFunctionalBlock;

                                if (func == null) continue;

                                if (!func.Enabled) continue;

                                if (!func.IsFunctional) continue;

                            }



                            if (matcher2.Matches(z.BlockDefinition.Id))

                            {

                                list2.Add((MySlimBlock) z);

                            }

                            else if (matcher1.Matches(z.BlockDefinition.Id))

                            {

                                list1.Add((MySlimBlock) z);

                            }

                        }

                    }

                }



                if (list1.Count > 0)

                {

                    all.AddList(list1);

                }

                else

                {

                    if (list2.Count > 0)

                    {

                        all.AddList(list2);

                    }

                }

            }



            var now2 = SharpUtils.msTimeStamp();

            UtilitiesDamageIntegrity.DamageBlocks2(all, integrityDamage, minIntegrity);

            var now3 = SharpUtils.msTimeStamp();



            Context.Respond($"Damaged {all.Count} blocks. Time seacrh : {now2 - now} : {now3 - now2} ms");

        }
    }
}