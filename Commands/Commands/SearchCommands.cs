using System.Collections.Generic;
using Torch.Commands;
using VRage.Game.ModAPI;
using Sandbox.Game;
using VRageMath;
using System;
using System.IO;
using Sandbox.Game.Entities;
using System.Linq;
using System.Text;
using NLog;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Torch.Utils;
using Slime;
using Torch.Commands.Permissions;

namespace NAPI
{
    [Category("show")]
    class SearchCommands : CommandModule
    {
        [Command("active-players", "Show players, that visited server more than X hours")]
        public void ShowActivePlayers(int maxHours = 72) {
            var identity = new List<IMyIdentity>(); 
            MyAPIGateway.Multiplayer.Players.GetAllIdentites(identity, (x)=>true);

            var count = 0;

            var identity2 = new List<MyIdentity>();
            
            foreach (var x in identity) {
                var ii = x as MyIdentity;
                if (ii != null) {
                    identity2.Add(ii);
                }
            }
            
            identity2.Sort((a,b)=>DateTime.Compare(a.LastLogoutTime,b.LastLogoutTime));
            
            foreach (var ii in identity2) {
                var time = (ii.LastLogoutTime - DateTime.Now).TotalHours;// ;
                Context.Respond(ii.DisplayName +" / " + ii.IdentityId + " LastLogin " + time);
                if (time < maxHours) { count++; }
            }
            
            Context.Respond("Total:" + count);
        }
        
        [Command("item-stacks", "Find big amount of item stacks in inventories.")]
        [Permission(MyPromoteLevel.Admin)]
        public void SearchItemStacks(int maxOkItems = 60)
        {
            var blocks = UtilitesEntity.GetBlocksFromAllGrids((b) =>
            {
                var fat = b.FatBlock;
                if (fat == null || !fat.HasInventory) return false;
                for (var x = 0; x < fat.InventoryCount; x++) {
                    if (fat.GetInventory(x).ItemCount > maxOkItems) { Context.Respond("Found block with " + fat.GetInventory(x).ItemCount + " items : " + fat.WorldMatrix.Translation + " entity=" + fat.EntityId + " owner=" + fat.OwnerId); }
                }

                return false;
            });
        }
        
        [ReflectedGetter(Name = "m_programData")] private static Func<MyProgrammableBlock, string> programGetter;
        
        [Command("scripts", "Exports all scripts into files")]
        [Permission(MyPromoteLevel.Admin)]
        public void ExportScripts()
        {
            try
            {
                var blocks = UtilitesEntity.GetBlocksFromAllGrids((b) =>
                {
                    return b.FatBlock != null && b.FatBlock is IMyProgrammableBlock;
                });

                if (blocks.Count == 0) {
                    Context.Respond("No scripts found!");
                    return;
                }
            
                var dir = "ScriptExports/"+DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
                Directory.CreateDirectory(dir);
            
                StringBuilder sb = new StringBuilder();
                int inc = 0;
                foreach (var x in blocks) {
                    var prog = (x.FatBlock as MyProgrammableBlock);
                    var program = programGetter(prog);
                    if (string.IsNullOrEmpty(program)) continue;

                    sb.Append("BuiltBy:").Append(x.BuiltBy).Append(" ").Append(x.BuiltBy.PlayerName() ?? "UnknownPlayerName").AppendLine();
                    sb.Append("OwnerId:").Append(x.OwnerId).Append(" ").Append(x.OwnerId.PlayerName() ?? "UnknownPlayerName").AppendLine();
                    sb.Append("EntityId:").Append(x.FatBlock.EntityId).Append(" ").Append(prog.CustomName).AppendLine();
                    sb.Append("Position:").Append(x.FatBlock.WorldMatrix.Translation).AppendLine();
                    sb.Append("Enabled:").Append(prog.Enabled ? "y" : "n").AppendLine();
                    sb.AppendLine("================");
                    sb.Append(program);
                    File.WriteAllText(dir+"/"+x.BuiltBy+"_"+x.FatBlock.EntityId+".txt", sb.ToString());
                    sb.Clear();
                    inc++;
                }
                Context.Respond("Exported scripts:" + inc);
            } catch (Exception e) {
                Context.Respond("Error on exporting scripts:" + e);
            }
        }
        
        
        [Command("active-physics", "Find active working grids")]
        [Permission(MyPromoteLevel.Admin)]
        public void ActivePhysics()
        {
            var grids = UtilitesEntity.GetAllGrids();
            var g = new Dictionary<MyCubeGrid,Vector3D>();
            foreach (var x in grids) {
                if (x.Physics != null && x.Physics.Enabled && x.Physics.IsActive) {
                    g.Add(x, x.WorldMatrix.Translation);
                }
            }

            if (Context.Player != null)
            {
                var cluster = UtilitesEntity.Cluster(g, 100);
                foreach (var x in cluster) {
                    MyVisualScriptLogicProvider.AddGPS(x.Value.Count >= 0 ? "p:" + x.Value.Count : "p", "ADMIN TOOLS", x.Key, Color.Lime, 120, Context.Player.IdentityId); 
                }
            }

            Context.Respond("Total active grid-physics:" + g.Count + " / " + grids.Count);
        }

        [Command("stats", "Find big amount of grids in one place.")]
        [Permission(MyPromoteLevel.Admin)]
        public void SearchStatic()
        {
            var grids = UtilitesEntity.GetAllGrids();

            int stations = 0;
            int ships = 0;
            int small = 0;
            int large = 0;

            Dictionary<long, Pair<int, int>> gridsInfo = new Dictionary<long, Pair<int, int>>();
            Dictionary<long, int> player = new Dictionary<long, int>();

            foreach (var x in grids) {
                var top = x.GetTopMostParent();
                var gid = top.EntityId;
                var owner = x.BigOwners.FirstOrDefault();

                var dynamic = !x.IsStatic;

                if (!player.ContainsKey(owner)) { player.Add(owner, 0); }

                player[owner]++;

                if (!gridsInfo.ContainsKey(gid)) { gridsInfo.Add(gid, new Pair<int, int>(0, 0)); }

                if (dynamic) gridsInfo[gid].v++;
                else gridsInfo[gid].v++;

                if (dynamic) ships++;
                else stations++;

                if (x.GridSizeEnum == VRage.Game.MyCubeSize.Large) large++;
                else small++;
            }

            Context.Respond("Static:" + stations + " Dynamic:" + ships + " Large:" + large + " Small:" + small);


            foreach (var x in gridsInfo) {
                if (x.Value.k + x.Value.v > 10) { Context.Respond("GridGroup: Entity:" + x.Key + " Static:" + x.Value.k + " Dynamic:" + x.Value.v); }
            }

            foreach (var x in player) {
                if (x.Value > 10) { Context.Respond("Player: Entity:" + x.Key + " Grids owned:" + x.Value); }
            }
        }

        //[Command("wturrets", "Finds working turrets")]
        //public void SearchWrokingTurrets(int radius = 7000) {
        //    var blocks = UtilitesEntity.GetBlocksFromAllGrids((b) => {
        //        var gun = b.FatBlock as MyLargeTurretBase;
        //        return gun != null && gun.NeedsUpdate != VRage.ModAPI.MyEntityUpdateEnum.NONE;
        //    });
//
        //    var blocks2 = UtilitesEntity.GetBlocksFromAllGrids((b) => {
        //        var gun = b.FatBlock as MyLargeTurretBase;
        //        return gun != null && gun.NeedsUpdate == VRage.ModAPI.MyEntityUpdateEnum.NONE;
        //    });
//
        //    Context.Respond("Working:" + blocks.Count + " / Not working " + blocks2.Count);
//
        //    ClusterShow(Context,blocks, radius);
        //}

        [Command("blocks", "Shows blocks by criterias")]
        [Permission(MyPromoteLevel.Admin)]
        public void SearchAll(string like, int radius = 7000)
        {
            var blocks = UtilitesEntity.GetBlocksFromAllGrids((b) => b.idIsLike(like));
            ClusterShow(Context,blocks, radius);
            Context.Respond("Total blocks:" + blocks.Count);
        }

        [Command("oblocks", "Shows blocks by criterias")]
        [Permission(MyPromoteLevel.Admin)]
        public void SearchOnline(string like, int radius = 7000)
        {
            var blocks = UtilitesEntity.GetBlocksFromAllGrids((b) => b.idIsLike(like) && b.isNearPlayer(radius));
            ClusterShow(Context,blocks, radius);
            Context.Respond("Total blocks near players:" + blocks.Count);
        }

        [Command("wblocks", "Shows blocks by criterias")]
        [Permission(MyPromoteLevel.Admin)]
        public void SearchWorking(string like, int radius = 7000)
        {
            var blocks = UtilitesEntity.GetBlocksFromAllGrids((b) => b.idIsLike(like) && b.isEnabled() && b.isNearPlayer(radius));
            ClusterShow(Context,blocks, radius);
            Context.Respond("Total working blocks:" + blocks.Count);
        }
        
        [Command("players", "Shows all players")]
        [Permission(MyPromoteLevel.Admin)]
        public void ShowPlayers()
        {
            var positions = ((MyPlayerCollection)MyAPIGateway.Multiplayer.Players).GetOnlinePlayers();
            foreach (var p in positions)
            {
                MyVisualScriptLogicProvider.AddGPS(p.DisplayName, "ADMIN TOOLS", p.GetPosition(), Color.Khaki, 120, Context.Player.IdentityId);
            }
        }
        
        

        public static void ClusterShow(CommandContext Context, List<MySlimBlock> blocks, int radius)
        {
            var d = new Dictionary<IMySlimBlock, Vector3D>();
            foreach (var x in blocks) { d.Add(x, x.WorldPosition); }

            var cluster = UtilitesEntity.Cluster(d, radius);
            if (Context.Player != null) {
                foreach (var x in cluster) {
                    Context.Respond("Found : " + x.Value.Count + " at " + x.Key);
                    if (Context.Player != null) { MyVisualScriptLogicProvider.AddGPS(x.Value.Count >= 0 ? "x:" + x.Value.Count : "x", "ADMIN TOOLS", x.Key, Color.Purple, 120, Context.Player.IdentityId); }
                }
            }
        }


        [Command("nests", "Find big amount of grids in one place.")]
        [Permission(MyPromoteLevel.Admin)]
        public void SearchNests(int minimumSubparts = 30, int radius = 10000)
        {
            var grids = UtilitesEntity.GetAllGrids((b) => true);
            ClusterShowGrids(grids, minimumSubparts, radius);
        }

        [Command("nests-for-players", "Grids")]
        [Permission(MyPromoteLevel.Admin)]
        public void SearchNests(int minimumSubparts = 30, int radius = 10000, String gpsName = "Come and raid me", String gpsDescription = "", int GpstimeOut = 180)
        {
            var grids = UtilitesEntity.GetAllGrids((b) => true);
            ClusterShowGrids2(grids, minimumSubparts, radius, gpsName, gpsDescription, GpstimeOut);
        }

        public void ClusterShowGrids(List<MyCubeGrid> grids, int minimum, int radius) {
            if (grids.Count == 0) return;

            var d = new Dictionary<MyCubeGrid, Vector3D>();
            foreach (var x in grids) { d.Add(x, x.WorldMatrix.Translation); }

            var cluster = UtilitesEntity.Cluster(d, radius);
            var data = new List<KeyValuePair<Vector3D, int>>();

            foreach (var x in cluster) {
                if (x.Value.Count >= minimum) { MyVisualScriptLogicProvider.AddGPS(x.Value.Count >= 0 ? "x:" + x.Value.Count : "x", "ADMIN TOOLS", x.Key, Color.Purple, 120, Context.Player.IdentityId); }
            }
        }

        public void ClusterShowGrids2(List<MyCubeGrid> grids, int minimum, int radius, String gpsName = "Come and raid me", String gpsDescription = "", int timeOut = 180) {
            if (grids.Count == 0) return;

            var d = new Dictionary<MyCubeGrid, Vector3D>();
            foreach (var x in grids) { d.Add(x, x.WorldMatrix.Translation); }

            var cluster = UtilitesEntity.Cluster(d, radius);
            var data = new List<KeyValuePair<Vector3D, int>>();
            //var Vector3D pos;


            foreach (var x in cluster) {
                if (x.Value.Count > minimum) { data.Add(new KeyValuePair<Vector3D, int>(x.Key, x.Value.Count)); }
            }

            if (data.Count > 0) {
                data.Sort((a, b) => b.Value - a.Value);
                var at = data[0];
                MyVisualScriptLogicProvider.AddGPSForAll(gpsName, gpsDescription, at.Key, Color.Purple, timeOut);
            }
        }
    }
}