﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAPI;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game.ModAPI;
using VRageMath;

namespace Commands.Commands.Utils
{
    [Category("mig")]
    public class AdminUtils : CommandModule
    {
        [Command("ShowVoxels", "Shows all voxels position in GPS", "help text")]
        [Permission(MyPromoteLevel.Moderator)]
        public void ShowVoxels(int disappeartime = 240)
        {
            if (Context.Player == null)
                return;

            try
            {
                List<MyVoxelBase> Maps = MyEntities.GetEntities().OfType<MyVoxelBase>().ToList();
                if (Maps.Count == 0)
                {
                    Context.Respond($"Voxel maps not found.");
                    return;
                }

                foreach (var voxelMap in Maps)
                {

                    if (voxelMap.MarkedForClose)
                    {
                        continue;
                    }

                    MyVisualScriptLogicProvider.AddGPS($"Voxel Map: {voxelMap.StorageName}", "", voxelMap.PositionComp.GetPosition(), Color.Lime, disappeartime, Context.Player.IdentityId);
                }

                Context.Respond($"Found [{Maps.Count}] voxel maps.");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Voxel find ex!");
            }
        }
    }
}
