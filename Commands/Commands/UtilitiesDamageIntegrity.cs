using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using NAPI;
using Sandbox.Engine.Utils;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.GameSystems;
using Sandbox.Game.Multiplayer;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRageMath;

namespace Slime.Utils {
    internal static class UtilitiesDamageIntegrity {
        static MethodInfo DeconstructStockpile = typeof(MySlimBlock).easyMethod("DeconstructStockpile");
        static MethodInfo OnIntegrityChanged = typeof(MyCubeBlock).easyMethod("OnIntegrityChanged");
        static MethodInfo UpdateHackingIndicator = typeof(MySlimBlock).easyMethod("UpdateHackingIndicator");
        static MethodInfo CreateConstructionSmokes = typeof(MySlimBlock).easyMethod("CreateConstructionSmokes");
        static MethodInfo OnSlimBlockBuildRatioLowered = typeof(MyGridGasSystem).easyMethod("OnSlimBlockBuildRatioLowered");
        static MethodInfo UpdateProgressGeneratedBlocks = typeof(MySlimBlock).easyMethod("UpdateProgressGeneratedBlocks");
        static FieldInfo m_componentStack = typeof(MySlimBlock).easyField("m_componentStack");

        private static readonly MyInventory VirtualInventory = new MyInventory(long.MaxValue, Vector3D.One, MyInventoryFlags.CanReceive | MyInventoryFlags.CanSend);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DamageBlocks(IEnumerable blocksToDamage, float integrityDamage, float minIntegrity) {
            var mySlimBlocks = (List<MySlimBlock>) blocksToDamage;
            var blocksToRemove = new List<MySlimBlock>();
            foreach (var mySlimBlock in mySlimBlocks) {
                //MyVisualScriptLogicProvider.SendChatMessage ("Damage: " + mySlimBlock.Integrity +" " + mySlimBlock.MaxIntegrity);
                var targetIntegrity = Math.Max(mySlimBlock.Integrity - integrityDamage, minIntegrity);
                var damage = mySlimBlock.Integrity - targetIntegrity;

                //MyVisualScriptLogicProvider.SendChatMessage ("Damage: " + mySlimBlock.Integrity +"->" + targetIntegrity +" (" + mySlimBlock.MaxIntegrity + ") " + damage + " " + integrityDamage +" | " + minIntegrity);
                if (damage > 0) {
                    DecreaseMountLevel(mySlimBlock, integrityDamage, VirtualInventory);
                    mySlimBlock.MoveItemsFromConstructionStockpile(VirtualInventory);
                    if (mySlimBlock.IsFullyDismounted) { blocksToRemove.Add(mySlimBlock); }
                }
            }

            foreach (var mySlimBlock in blocksToRemove) { mySlimBlock.CubeGrid.RemoveBlock(mySlimBlock); }

            VirtualInventory.ClearItems();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DamageBlocks2(IEnumerable<MySlimBlock> blocksToDamage, float integrityDamage, float minIntegrity, float percentDamage = 0f) {
            var blocksToRemove = new List<MySlimBlock>();
            foreach (var mySlimBlock in blocksToDamage) {
                var targetIntegrity = Math.Max(mySlimBlock.Integrity - integrityDamage + mySlimBlock.Integrity * percentDamage, minIntegrity);
                var damage = mySlimBlock.Integrity - targetIntegrity;

                //MyVisualScriptLogicProvider.SendChatMessage ("Damage: " + mySlimBlock.Integrity +"->" + targetIntegrity +" (" + mySlimBlock.MaxIntegrity + ") " + damage + " " + integrityDamage +" | " + minIntegrity);
                if (damage > 0) {
                    DecreaseMountLevel2(mySlimBlock, integrityDamage, VirtualInventory);
                    mySlimBlock.MoveItemsFromConstructionStockpile(VirtualInventory);
                    if (mySlimBlock.IsFullyDismounted) { blocksToRemove.Add(mySlimBlock); }
                }
            }

            foreach (var mySlimBlock in blocksToRemove) { mySlimBlock.CubeGrid.RemoveBlock(mySlimBlock); }

            VirtualInventory.ClearItems();
        }


        //public void Do() {
        //    foreach (var x in MySessionComponentSafeZones.SafeZones) { Log("" + x.EntityId); }
        //}

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DecreaseMountLevel(MySlimBlock _this, float grinderAmount, MyInventoryBase outputInventory, bool useDefaultDeconstructEfficiency = false, long identityId = 0L) {
            var compStack = m_componentStack.GetValue(_this) as MyComponentStack;

            if (!Sync.IsServer || compStack.IsFullyDismounted) { return; }
            //ulong user = 0UL;
            //if (identityId != 0L)
            //{
            //	MyPlayer.PlayerId playerId;
            //	MySession.Static.Players.TryGetPlayerId(identityId, out playerId);
            //	user = playerId.SteamId;
            //}
            //if (!MySessionComponentSafeZones.IsActionAllowed(_this.CubeGrid, MySafeZoneAction.Building, 0L, user))
            //{
            //	return;
            //}

            float dissassemble;

            if (_this.FatBlock != null) { dissassemble = _this.FatBlock.DisassembleRatio; } else { dissassemble = _this.BlockDefinition.DisassembleRatio; }


            grinderAmount /= dissassemble;

            grinderAmount *= _this.BlockDefinition.IntegrityPointsPerSec;

            float buildRatio = compStack.BuildRatio;
            DeconstructStockpile.Invoke(_this, new object[] {grinderAmount, outputInventory, useDefaultDeconstructEfficiency});
            float newRatio = (_this.BuildIntegrity - grinderAmount) / _this.BlockDefinition.MaxIntegrity;
            if (_this.BlockDefinition.RatioEnoughForDamageEffect(_this.BuildLevelRatio) && _this.FatBlock != null && _this.FatBlock.OwnerId != 0L) { OnIntegrityChanged.Invoke(_this.FatBlock, new object[] {_this.BuildIntegrity, _this.Integrity, false, 0L, MyOwnershipShareModeEnum.Faction}); }

            long num = 0L;
            if (outputInventory != null && outputInventory.Entity != null) {
                IMyComponentOwner<MyIDModule> myComponentOwner = outputInventory.Entity as IMyComponentOwner<MyIDModule>;
                MyIDModule myIDModule;
                if (myComponentOwner != null && myComponentOwner.GetComponent(out myIDModule)) { num = myIDModule.Owner; }
            }

            UpdateHackingIndicator.Invoke(_this, new object[] {newRatio, buildRatio, num});
            bool flag = _this.BlockDefinition.ModelChangeIsNeeded(compStack.BuildRatio, buildRatio);
            MyIntegrityChangeEnum integrityChangeType = MyIntegrityChangeEnum.Damage;
            if (flag) {
                _this.UpdateVisual(true);
                if (_this.FatBlock != null) {
                    int num2 = _this.CalculateCurrentModelID();
                    if (num2 == -1 || _this.BuildLevelRatio == 0f) { integrityChangeType = MyIntegrityChangeEnum.DeconstructionEnd; } else if (num2 == _this.BlockDefinition.BuildProgressModels.Length - 1) { integrityChangeType = MyIntegrityChangeEnum.DeconstructionBegin; } else { integrityChangeType = MyIntegrityChangeEnum.DeconstructionProcess; }

                    _this.FatBlock.SetDamageEffect(false);
                }

                _this.PlayConstructionSound(integrityChangeType, true);
                CreateConstructionSmokes.Invoke(_this, null);
                if (_this.CubeGrid.GridSystems.GasSystem != null) { OnSlimBlockBuildRatioLowered.Invoke(_this.CubeGrid.GridSystems.GasSystem, new object[] {_this}); }
            }

            if (MyFakes.ENABLE_GENERATED_BLOCKS && !_this.BlockDefinition.IsGeneratedBlock && _this.BlockDefinition.GeneratedBlockDefinitions != null && _this.BlockDefinition.GeneratedBlockDefinitions.Length != 0) { UpdateProgressGeneratedBlocks.Invoke(_this, new object[] {buildRatio}); }

            _this.CubeGrid.SendIntegrityChanged(_this, integrityChangeType, num);
            _this.CubeGrid.OnIntegrityChanged(_this, false);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DecreaseMountLevel2(MySlimBlock _this, float grinderAmountIntegrity, MyInventoryBase outputInventory, bool useDefaultDeconstructEfficiency = false, long identityId = 0L) {
            var compStack = m_componentStack.GetValue(_this) as MyComponentStack;
            if (!Sync.IsServer || compStack.IsFullyDismounted) { return; }

            var grinderAmount = grinderAmountIntegrity;

            float buildRatio = compStack.BuildRatio;
            DeconstructStockpile.Invoke(_this, new object[] {grinderAmount, outputInventory, useDefaultDeconstructEfficiency});
            float newRatio = (_this.BuildIntegrity - grinderAmount) / _this.BlockDefinition.MaxIntegrity;
            if (_this.BlockDefinition.RatioEnoughForDamageEffect(_this.BuildLevelRatio) && _this.FatBlock != null && _this.FatBlock.OwnerId != 0L) { OnIntegrityChanged.Invoke(_this.FatBlock, new object[] {_this.BuildIntegrity, _this.Integrity, false, 0L, MyOwnershipShareModeEnum.Faction}); }

            long num = 0L;
            if (outputInventory != null && outputInventory.Entity != null) {
                IMyComponentOwner<MyIDModule> myComponentOwner = outputInventory.Entity as IMyComponentOwner<MyIDModule>;
                MyIDModule myIDModule;
                if (myComponentOwner != null && myComponentOwner.GetComponent(out myIDModule)) { num = myIDModule.Owner; }
            }

            UpdateHackingIndicator.Invoke(_this, new object[] {newRatio, buildRatio, num});
            bool flag = _this.BlockDefinition.ModelChangeIsNeeded(compStack.BuildRatio, buildRatio);
            MyIntegrityChangeEnum integrityChangeType = MyIntegrityChangeEnum.Damage;
            if (flag) {
                _this.UpdateVisual(true);
                if (_this.FatBlock != null) {
                    int num2 = _this.CalculateCurrentModelID();
                    if (num2 == -1 || _this.BuildLevelRatio == 0f) { integrityChangeType = MyIntegrityChangeEnum.DeconstructionEnd; } else if (num2 == _this.BlockDefinition.BuildProgressModels.Length - 1) { integrityChangeType = MyIntegrityChangeEnum.DeconstructionBegin; } else { integrityChangeType = MyIntegrityChangeEnum.DeconstructionProcess; }

                    _this.FatBlock.SetDamageEffect(false);
                }

                _this.PlayConstructionSound(integrityChangeType, true);
                CreateConstructionSmokes.Invoke(_this, null);
                if (_this.CubeGrid.GridSystems.GasSystem != null) { OnSlimBlockBuildRatioLowered.Invoke(_this.CubeGrid.GridSystems.GasSystem, new object[] {_this}); }
            }

            if (MyFakes.ENABLE_GENERATED_BLOCKS && !_this.BlockDefinition.IsGeneratedBlock && _this.BlockDefinition.GeneratedBlockDefinitions != null && _this.BlockDefinition.GeneratedBlockDefinitions.Length != 0) { UpdateProgressGeneratedBlocks.Invoke(_this, new object[] {buildRatio}); }

            _this.CubeGrid.SendIntegrityChanged(_this, integrityChangeType, num);
            _this.CubeGrid.OnIntegrityChanged(_this, false);
        }
    }
}