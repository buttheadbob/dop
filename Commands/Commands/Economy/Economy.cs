﻿using Torch.Commands;
using System;
using VRage.ObjectBuilders;
using Slime.Utils;
using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.ModAPI;
using System.Collections.Generic;
using VRage.Game;
using VRage;
using VRage.Game.ModAPI;
using Sandbox.Definitions;
using System.Linq;
using VRage.Game.ModAPI.Ingame;
using System.Text;
using Commands.Commands.Economy;

namespace Slime.Features {
    class EconomyCalculate : CommandModule {
        public static MyObjectBuilderType COMPONENT = MyObjectBuilderType.Parse("MyObjectBuilder_Component");
        public static MyObjectBuilderType ORE = MyObjectBuilderType.Parse("MyObjectBuilder_Ore");
        public static MyDefinitionId ASSEMBLER_TIME = new MyDefinitionId(ORE, "AssemblerTime");
        public static MyDefinitionId REFINERY_TIME = new MyDefinitionId(ORE , "RefineryTime");
        public static MyDefinitionId WORTH = new MyDefinitionId(ORE, "Money");
        public static MyDefinitionId WORTH_OK = new MyDefinitionId(ORE, "MoneyOk");

        public static Dictionary<MyDefinitionId, MyFixedPoint> PRICES = new Dictionary<MyDefinitionId, MyFixedPoint>();
        public static Dictionary<MyDefinitionId, double> MAPPER = new Dictionary<MyDefinitionId, double>();
        public static Dictionary<MyDefinitionId, Dictionary<MyDefinitionId, double>> WORTH_MLT = new Dictionary<MyDefinitionId, Dictionary<MyDefinitionId, double>>();

        public static void Init()
        {
            
        }

        public static void CalculateAutoCost (int refineryTimeCost, int assemblerTimeCost)
        {
            //MyDefinitionManager.Static.getBloc(MyDefinitionId.Parse(x), out blockdef)
            //var bp = GetCostInIngotsAndTime(new string[] { }, MAPPER);
            var blueprints = new HashSet<MyBlueprintDefinitionBase>();
        }

        public static Dictionary<MyDefinitionId, Dictionary<MyDefinitionId, double>> GetCostInIngotsAndTime (Dictionary<MyDefinitionId, double> mapper)//, double refineryMultiplier = 0.2f, double assemblerMutliplier = 1f)
        {
            var blueprints = new HashSet<MyBlueprintDefinitionBase>();
            Dictionary<MyDefinitionId, Dictionary <MyDefinitionId, double>> dict = new Dictionary<MyDefinitionId, Dictionary<MyDefinitionId, double>>();

            foreach (var b in MyDefinitionManager.Static.GetBlueprintDefinitions())
            {
                

                var t = b.GetBlueprintType();
                var canProcess = t == BlueprintType.IngotsToComponent && (b.Results.Length == 1);
                canProcess |= (t == BlueprintType.OresToIngots && t == BlueprintType.OreToIngot) && (b.Prerequisites.Length == 1);
                canProcess |= t == BlueprintType.IngotsToTools && (b.Results.Length == 1);

                Log.Error("BP: " + canProcess + " " + t + " " + b.Prerequisites.Length + " " + b.Results.Length + " " + b.DisplayNameText + " " + b.Id);

                if (!canProcess)
                {
                    continue;
                }

                if (t == BlueprintType.IngotsToComponent || t == BlueprintType.IngotsToTools)
                {
                    FormWorth(dict, b, mapper, false);
                }
                else
                {
                    FormWorth(dict, b, mapper, true);
                }
            }

            return dict;
        }

        private static void FormWorth(Dictionary<MyDefinitionId, Dictionary<MyDefinitionId, double>> dict, MyBlueprintDefinitionBase b, Dictionary<MyDefinitionId, double> mapper, bool isRef)
        {
            var from = isRef ? b.Prerequisites : b.Results;
            var to = isRef ? b.Results : b.Prerequisites;
            var time = isRef ? REFINERY_TIME : ASSEMBLER_TIME;

            var key = from[0].Id;
            var am = from[0].Amount;
            var d = new Dictionary<MyDefinitionId, double>();

            double w;
            var money = 0.0;
            var hasAllMappings = true;
            foreach (var y in to)
            {
                w = (double)y.Amount / (double)am;
                d[y.Id] = w;// * assemblerMutliplier;

                if (hasAllMappings && mapper.ContainsKey(y.Id))
                {
                    money += mapper[y.Id] * w;
                }
                else
                {
                    hasAllMappings = false;
                }
            }



            w = b.BaseProductionTimeInSeconds / (double)am;
            d[time] = w;

            if (hasAllMappings && mapper.ContainsKey(time))
            {
                money += w * mapper[time];
            }
            else
            {
                hasAllMappings = false;
            }


            d[WORTH] = money;
            if (hasAllMappings)
            {
                d[WORTH_OK] = 1;
            }

            foreach (var x in d)
            {
                Log.Error("W : " + from + " " + x.Key+ " " + x.Value);
            }

            dict[key] = d;
        }

        [Command("economy-form", "Damage block by integrity, if FatOnly - only functional blocks but it is MUCH FASTER")]
        public void CalculateWorth()
        {
            WORTH_MLT = GetCostInIngotsAndTime (new Dictionary<MyDefinitionId, double>());
        }

        [Command("economy-calculate", "Damage block by integrity, if FatOnly - only functional blocks but it is MUCH FASTER")]
        public void Calculate() {
            var grids = MyEntities.GetEntities().OfType<MyCubeGrid>().ToList();
            var arrayList = new List<MySlimBlock>();

            Dictionary <long, Dictionary <MyDefinitionId, MyFixedPoint>> worthByFactionOrPlayer = new Dictionary<long, Dictionary<MyDefinitionId, MyFixedPoint>>();

            MyAPIGateway.Parallel.ForEach(grids, (x) =>
            {
                if (x.Closed || x.MarkedForClose) { return; }

                var worth = Calculate (x);
                long owner = 0;

                if (x.BigOwners != null && x.BigOwners.Count > 0)
                {
                    owner = x.BigOwners[0];
                    var faction = owner.PlayerFaction();
                    if (faction != null)
                    {
                        owner = faction.FactionId;
                    }
                }

                lock (worthByFactionOrPlayer)
                {
                    if (!worthByFactionOrPlayer.ContainsKey(owner)) {
                        worthByFactionOrPlayer[owner] = worth;
                    } else {
                        worthByFactionOrPlayer[owner].Sum(worth);
                    }
                }
            });


            foreach (var x in worthByFactionOrPlayer)
            {
  
                var ent = x.Key.GetIdentity();
                var name = ent?.DisplayName ?? "<"+x.Key+">";

                var sb = new StringBuilder();
                sb.Append(name).Append(": ");

                

                var worth2 = new Dictionary<MyDefinitionId, double>();
                foreach (var y in x.Value)
                {
                    if (WORTH_MLT.ContainsKey(y.Key))
                    {
                        foreach (var z in WORTH_MLT[y.Key])
                        {
                            worth2.TryGetValue(z.Key, out var am);
                            worth2[z.Key] = am + ((double)y.Value * z.Value);
                        }
                    } else
                    {
                        worth2[y.Key] = (double)y.Value;
                    }
                }

                foreach (var y in worth2)
                {
                    sb.Append(y.Key).Append(": ").AppendLine("" + y.Value);
                }

                Context.Respond (sb.ToString());
            }
        }

        

        public static Dictionary<MyDefinitionId, MyFixedPoint> Calculate (MyCubeGrid grid)
        {
            Dictionary<MyDefinitionId, MyFixedPoint> totalInventory = new Dictionary<MyDefinitionId, MyFixedPoint>();

            Dictionary<string, int> totalBlocksCost = new Dictionary<string, int>();
            Dictionary<string, int> temp = new Dictionary<string, int>();

            foreach (var y in grid.GetFatBlocks())
            {
                CalculateInventory (y, totalInventory);
                y.SlimBlock.GetRealComponentsCost(totalBlocksCost, temp);
            }

            foreach (var x in totalBlocksCost)
            {
                var item = new MyDefinitionId(COMPONENT, x.Key);
                totalInventory.TryGetValue (item, out var amount);
                totalInventory[item] = amount + x.Value;
            }

            return totalInventory;
        }

        public static void CalculateInventory (MyCubeBlock block, Dictionary<MyDefinitionId, MyFixedPoint> total)
        {
            for (var x = 0; x < block.InventoryCount; x++)
            {
                block.GetInventory(x).CountItems(total);
            }
        }
    }
}