﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SeEval.Forms
{
    public partial class SeEvalControlForm : UserControl
    {
        public SeEvalControlForm()
        {
            InitializeComponent();
        }

        private void btn_execute_Click(object sender, RoutedEventArgs e)
        {
            var assembly = SeEval.Code.ScriptManager.Compile(textbox_script.Text, out var errors);
            if (errors != null)
            {
                var sb = new StringBuilder();
                foreach (var ee in errors)
                {
                    sb.Append($"{ee.Id}: {ee.GetMessage()}\n\n");
                }

                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Warning;

                MessageBox.Show(sb.ToString(), "Errors", button, icon, MessageBoxResult.Yes);
                return;
            }

            try
            {
                var type = assembly.GetType("Script");
                var obj = Activator.CreateInstance(type);
                var method = type.GetMethod("Apply", BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic);
                method.Invoke(obj, new object[0]);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString(), "Errors", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.Yes);
            }
        }
    }
}
