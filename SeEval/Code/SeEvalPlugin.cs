﻿using System.IO;
using System.Windows.Controls;
using NLog;
using SeEval.Forms;
using Torch;
using Torch.API;
using TorchPlugin;

namespace SeEval.Code
{
    public class SeEvalPlugin : CommonPlugin
    {
        private static Persistent<SeEvalConfig> m_config; 

        public static readonly Logger Log = LogManager.GetLogger("VStoragePlugin");
        public static SeEvalConfig CurrentPluginConfig => m_config?.Data;

        public override void Init(ITorchBase torch)
        {
            base.Init(torch);
            m_config = Persistent<SeEvalConfig>.Load(Path.Combine(StoragePath, "MIG-SeEvalConfig.cfg"));
            ScriptManager.Init();
        }

        public override UserControl CreateControl()
        {
            return new SeEvalControlForm() { DataContext = CurrentPluginConfig };
        }

        public static void ToLog(string msg, int lvl = 0)
        {
            if (lvl == 0)
            {
                Log.Info($"{msg}");
            }
            else if (lvl == 1)
            {
                Log.Warn($"{msg}");
            }
            else
            {
                Log.Error($"{msg}");
            }
        }
    }
}