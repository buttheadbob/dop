﻿using Torch;

namespace SeEval.Code
{
    public class SeEvalConfig : ViewModel
    {
        private bool _enable;

        public bool Enabled { get => _enable; set => SetValue(ref _enable, value); }

        private string _script;

        public string Script { get => _script; set => SetValue(ref _script, value); }

    }
}