﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using VRage.Game;

namespace SeEval.Code
{
    public class ScriptManager {
        public static string SCRIPT_EXAMPLE = "";
        public static string SCRIPT_TEMPLATE = "<YOUR CODE HERE>";

        public static void Init()
        {
            ReadFile("Templates/ScriptTemplate.cs", ref SCRIPT_TEMPLATE);
            ReadFile("Templates/ScriptExample.cs", ref SCRIPT_EXAMPLE);
        }

        private static void ReadFile(string path, ref string where)
        {
            if (File.Exists(path))
            {
                where = File.ReadAllText(path);
            }
            else
            {
                throw new Exception($"File {path} not found");
            }
        }
        
        public static Assembly Compile(string txt, out IEnumerable<Diagnostic> errors)
        {
            if (!txt.StartsWith("//CUSTOM"))
            {
                txt = SCRIPT_TEMPLATE.Replace("<YOUR CODE HERE>", txt);
            }

            return InnerCompile(txt, out errors);
        }
        
        private static MetadataReference[] m_References;
        private static MetadataReference[] References {
            get {
                
                if (m_References == null)
                {
                    var list = new HashSet<string>();
                    var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                    for (int i = 0; i < assemblies.Length; i++)
                    {
                        list.Add(assemblies[i].Location);//, 
                    }
                    
                    
                    
                    var files = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.dll", SearchOption.AllDirectories);
                    foreach (var file in files)
                    {
                        list.Add(file);
                    }

                    var arr = new List<MetadataReference>();
                    Parallel.ForEach(list, (s) =>
                    {
                        if (s.ToLower().Contains("magick")) return;
                        var m = MetadataReference.CreateFromFile(s);
                        lock (arr)
                        {
                            arr.Add(m);
                        }
                    });
                   
                    m_References = arr.ToArray();
                }
                return m_References;
            }
        }
        
        private static Assembly InnerCompile(string txt, out IEnumerable<Diagnostic> errors)
        {
            // define source code, then parse it (to the type used for compilation)
            SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(txt);

            // define other necessary objects for compilation
            string assemblyName = Path.GetRandomFileName();
            // analyse and generate IL code from syntax tree
            CSharpCompilation compilation = CSharpCompilation.Create(
                assemblyName,
                syntaxTrees: new[] {syntaxTree},
                references: References,
                options: new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));

            using (var ms = new MemoryStream()) {
            using (var ms2 = new MemoryStream()) {
                // write IL code into memory
                EmitResult result = compilation.Emit(ms, ms2);

                if (!result.Success)
                {
                    // handle exceptions
                    IEnumerable<Diagnostic> failures = result.Diagnostics.Where(diagnostic =>
                        diagnostic.IsWarningAsError ||
                        diagnostic.Severity == DiagnosticSeverity.Error);

                    errors = failures;
                    return null;
                    
                }

                errors = null;
                ms.Seek(0, SeekOrigin.Begin);
                ms2.Seek(0, SeekOrigin.Begin);
                return Assembly.Load(ms.ToArray(), ms2.ToArray());
            }
            }
        }
    }
}