﻿using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Torch.Commands;

namespace SharedLibTorch
{
    public static class CommandsHelpers
    {
        public static bool TryParseIdentity(CommandContext context, string nameOrSteamId, out MyIdentity identity)
        {
            long identityId = 0;
            identity = null;
            if (string.IsNullOrWhiteSpace(nameOrSteamId))
            {
                context.Respond($"Enter valid steamid or player name!");
                return false;
            }

            if (ulong.TryParse(nameOrSteamId, out ulong steamid))
            {
                identityId = Sync.Players.TryGetIdentityId(steamid);
                identity = Sync.Players.TryGetIdentity(identityId);
            }
            else
            {
                var player = Sync.Players.GetPlayerByName(nameOrSteamId);
                if (player != null)
                {
                    identity = player.Identity;
                }
            }

            return identity != null;
        }
    }
}
