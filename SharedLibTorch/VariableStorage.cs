﻿using System;
using NAPI;
using Sandbox.ModAPI;
using Slime;

namespace SharedLibTorch
{
    
    public class VariableStorage<T>
    {
        public T Data
        {
            get
            {
                if (!loaded)
                {
                    Load();
                }
                return m_data;
            }
            private set
            {
                m_data = value;
            }
        }

        private T m_data;
        private bool loaded = false;
        private bool createNewOnError;
        private string key;
        private Func<T> creator = null;

        public VariableStorage(string key, Func<T> creator = null, bool createNewOnError = true)
        {
            this.key = key;
            this.creator = creator;
            this.createNewOnError = createNewOnError;
        }
        
        public void Load ()
        {
            loaded = true;
            try
            {
                if (MyAPIGateway.Utilities.GetVariable<string>(key, out var data))
                {
                    m_data = data.FromBase64Binary<T>();
                }
                else
                {
                    if (creator != null)
                    {
                        m_data = creator.Invoke();
                    }
                }
                
                Log.Error("VariableStorage:Load");
            }
            catch (Exception e)
            {
                Log.Fatal($"{e}");
                if (createNewOnError)
                {
                    if (creator != null)
                    {
                        m_data = creator.Invoke();
                    }
                }
                else
                {
                    throw;
                }
            }
        }

        public void Save(T data)
        {
            loaded = true;
            m_data = data;
            
            if (Data != null)
            {
                var s = m_data.ToBase64Binary();
                MyAPIGateway.Utilities.SetVariable(key, s);
            }
            else
            {
                MyAPIGateway.Utilities.RemoveVariable(key);
            }
        }
    }
}