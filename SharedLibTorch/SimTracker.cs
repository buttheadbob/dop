﻿using Sandbox;
using Sandbox.Game.World;
using System;

namespace NAPI
{
    public class SimTracker : Action1<long>
    {
        public float load_max = 0;
        public float load_min = 999999;

        float _load_max = 0;
        float _load_min = 999999;
        float load_total;
        private double loadPhysicsTotal = 0;
        public double avrgPhysics = 0;
        int load_took;

        public int wasSaving = 120;
        public float avrg = 1;

        static int SAVE_INTERVAL = 60 * 3;
        static int REFRESH_INTERVAL = 60 * 10;

        public bool refreshed = false;

        public void run(long frame)
        {
            refreshed = false;
            if (MySession.Static.ServerSaving)
            {
                wasSaving = SAVE_INTERVAL;
                return;
            }
            else
            {
                if (wasSaving > 0)
                {
                    wasSaving--;
                    return;
                }
            }

            var load = MySandboxGame.Static.CPULoad;

            load_total += load;
            load_took++;
            _load_max = Math.Max(_load_max, load);
            _load_min = Math.Min(_load_min, load);
            loadPhysicsTotal += PhysicsTracker.elapsed.TotalMilliseconds;

            if (load_took == REFRESH_INTERVAL)
            { //5 sec
                refreshed = true;
                avrg = load_total / load_took;
                avrgPhysics = loadPhysicsTotal / load_took;
                load_took = 0;
                load_total = 0;

                load_max = _load_max;
                load_min = _load_min;

                _load_max = 0;
                loadPhysicsTotal = 0;
                _load_min = 999999;
            }
        }
    }
}