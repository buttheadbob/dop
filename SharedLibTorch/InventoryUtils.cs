﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game;
using VRage;
using VRage.Game;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.ObjectBuilders;

namespace SharedLibTorch
{
    public static class InventoryUtils
    {
        private static readonly MyObjectBuilderType COMPONENT_TYPE = MyObjectBuilderType.Parse("MyObjectBuilder_Component");
        private static readonly MyObjectBuilderType INGOT_TYPE = MyObjectBuilderType.Parse("MyObjectBuilder_Ingot");
        private static readonly MyObjectBuilderType ORE_TYPE = MyObjectBuilderType.Parse("MyObjectBuilder_Ore");
        private static MethodInfo ADD_ITEMS_METHOD = typeof(MyInventory).GetMethod("AddItemsInternal", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public, null, new Type[] { typeof(MyFixedPoint), typeof(MyObjectBuilder_PhysicalObject), typeof(uint?), typeof(int) }, null);

        public static bool InventoryAdd(IMyInventory inventory, MyFixedPoint amount, MyDefinitionId definitionId, bool forcepush)
        {
            var someObject = MyObjectBuilderSerializer.CreateNewObject(definitionId);
            MyObjectBuilder_PhysicalObject physObject = null;

            if (someObject is MyObjectBuilder_PhysicalObject)
            {
                physObject = (MyObjectBuilder_PhysicalObject)someObject;
            }

            var gasContainer = someObject as MyObjectBuilder_GasContainerObject;
            if (gasContainer != null)
            {
                gasContainer.GasLevel = 1f;
            }

            MyObjectBuilder_InventoryItem inventoryItem = new MyObjectBuilder_InventoryItem { Amount = amount, PhysicalContent = physObject };

            if (forcepush)
            {
                InventoryUtils.ForceAddItems(inventory as MyInventory, amount, inventoryItem.PhysicalContent, null);
            }
            else
            {
                if (inventory.CanItemsBeAdded(inventoryItem.Amount, definitionId))
                {
                    inventory.AddItems(inventoryItem.Amount, inventoryItem.PhysicalContent, -1);
                }
                else
                {
                    return false;
                }
            }

            return true;
        }
        ///Copy of Keen MyInventory.AddItemsInternal(amount, myObjectBuilder_PhysicalObject, itemId, index);
        /// without volume/mass check
        public static bool ForceAddItems(MyInventory invtoadd, MyFixedPoint amount, MyObjectBuilder_Base objectBuilder, uint? itemId, int index = -1)
        {
            if (amount == 0)
            {
                return false;
            }

            if (invtoadd == null)
            {
                return false;
            }

            MyObjectBuilder_PhysicalObject myObjectBuilder_PhysicalObject = objectBuilder as MyObjectBuilder_PhysicalObject;
            MyDefinitionId myDefinitionId = objectBuilder.GetId();

            if (myObjectBuilder_PhysicalObject == null)
            {
                myObjectBuilder_PhysicalObject = new MyObjectBuilder_BlockItem();
                (myObjectBuilder_PhysicalObject as MyObjectBuilder_BlockItem).BlockDefId = myDefinitionId;
            }
            else
            {
                MyCubeBlockDefinition myCubeBlockDefinition = MyDefinitionManager.Static.TryGetComponentBlockDefinition(myDefinitionId);
                if (myCubeBlockDefinition != null)
                {
                    myObjectBuilder_PhysicalObject = new MyObjectBuilder_BlockItem();
                    (myObjectBuilder_PhysicalObject as MyObjectBuilder_BlockItem).BlockDefId = myCubeBlockDefinition.Id;
                }
            }

            if (myObjectBuilder_PhysicalObject == null)
            {
                return false;
            }

            amount = MyFixedPoint.Floor((MyFixedPoint)(Math.Round((double)amount * 1000.0) / 1000.0));
            ADD_ITEMS_METHOD.Invoke(invtoadd, new object[] { amount, myObjectBuilder_PhysicalObject, itemId, index });
            invtoadd.Refresh();
            return true;
        }

        public static void SortInventory(MyInventory cargoinv)
        {
            var items = cargoinv.GetItems();
            items.Sort((a, b) =>
            {
                var v = GetTypeSortValue(a) - GetTypeSortValue(b);
                if (v != 0) return v;
                return (b.Amount > a.Amount) ? 1 : ((b.Amount == a.Amount) ? 0 : -1);
            });
            int GetTypeSortValue(MyPhysicalInventoryItem item)
            {
                if (item.Content.TypeId == COMPONENT_TYPE) return 1;
                if (item.Content.TypeId == INGOT_TYPE) return 2;
                if (item.Content.TypeId == ORE_TYPE) return 3;
                return 4;
            }
        }
    }
}
