﻿using NAPI;
using NLog;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Documents;
using VRage;
using VRage.Game;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.ObjectBuilders;

namespace SharedLibTorch
{
    public static class DeconstructUtils
    {
        public static Dictionary<MyDefinitionId, MyFixedPoint> GetAllComponentsAndInventories(List<GasToOreRatio> gasList, ref MyObjectBuilder_CubeGrid[] gridsOB)
        {
            object lockobject = new object();
            var gridComp = new Dictionary<MyDefinitionId, MyFixedPoint>(10);
            if (gridsOB.Count() >= 3)
            {
                MyAPIGateway.Parallel.ForEach(gridsOB, (grid) =>
                {
                    var gridCompThread = new Dictionary<MyDefinitionId, MyFixedPoint>(10);
                    DeconstructBlocks(grid, gridComp);
                    lock (lockobject)
                    {
                        gridComp.Sum(gridCompThread);
                    }
                });
            }
            else
            {
                foreach (var grid in gridsOB)
                {
                    DeconstructBlocks(grid, gridComp);
                }
            }
            return gridComp;

            void DeconstructBlocks(MyObjectBuilder_CubeGrid grid, Dictionary<MyDefinitionId, MyFixedPoint> _gridComp)
            {
                foreach (var block in grid.CubeBlocks)
                {
                    DeconstructBlock(gasList, block, _gridComp);
                }
            }
        }

        /// <summary>
        /// Returns all components in inventories and blocks from grid group, grouped via block builder
        /// MyDefinitionId, MyFixedPoint is faster than string, double
        /// </summary>
        /// <param name="gridsOB"></param>
        /// <returns></returns>
        public static Dictionary<long, Dictionary<MyDefinitionId, MyFixedPoint>> GetAllComponentsAndInventoriesForPlayers(List<GasToOreRatio> gasList, List<MyObjectBuilder_CubeGrid> grids)
        {
            Dictionary<long, Dictionary<MyDefinitionId, MyFixedPoint>> AllComponentsAndInventoryItems = new Dictionary<long, Dictionary<MyDefinitionId, MyFixedPoint>>();
            object lockobject = new object();

            MyAPIGateway.Parallel.ForEach(grids, (grid) =>
            {
                if (grid == null || grid.CubeBlocks == null || grid.CubeBlocks.Count == 0) { return; }
                Dictionary<long, Dictionary<MyDefinitionId, MyFixedPoint>> GridComponentsAndInventoryItems = new Dictionary<long, Dictionary<MyDefinitionId, MyFixedPoint>>(3);
                var blocks = grid.CubeBlocks;
                var blockComponentsAndInv = new Dictionary<MyDefinitionId, MyFixedPoint>(10);
                foreach (var block in blocks)
                {
                    blockComponentsAndInv.Clear();
                    DeconstructBlock(gasList, block, blockComponentsAndInv);
                    var playerId = block.BuiltBy;
                    if (!GridComponentsAndInventoryItems.ContainsKey(playerId))
                    {
                        GridComponentsAndInventoryItems.Add(playerId, new Dictionary<MyDefinitionId, MyFixedPoint>());
                    }
                    //Not else bcs dictionary is reference type we dont want clear it after add
                    foreach (var blockComp in blockComponentsAndInv)
                    {
                        GridComponentsAndInventoryItems[playerId].Sum(blockComp.Key, blockComp.Value);
                    }
                }
                lock (lockobject)
                {
                    foreach (var playerBucket in GridComponentsAndInventoryItems)
                    {
                        var playerId = playerBucket.Key;
                        if (!AllComponentsAndInventoryItems.ContainsKey(playerId))
                        {
                            AllComponentsAndInventoryItems.Add(playerId, playerBucket.Value);
                        }
                        else // bcs we dont clear player bucket
                        {
                            foreach (var gridcomp in playerBucket.Value)
                            {
                                AllComponentsAndInventoryItems[playerId].Sum(gridcomp.Key, gridcomp.Value);
                            }
                        }
                    }
                }
            });
            return AllComponentsAndInventoryItems;
        }

        private static void DeconstructBlock(List<GasToOreRatio> gasList, MyObjectBuilder_CubeBlock block, Dictionary<MyDefinitionId, MyFixedPoint> blockComponentsAndInv)
        {
            MyCubeBlockDefinition blockDef = MyDefinitionManager.Static.GetCubeBlockDefinition(block);
            foreach (var component in blockDef.Components)
            {
                blockComponentsAndInv.Sum(component.Definition.Id, component.Count);
            }

            var missingComponents = GetMissingComponents(block, blockDef);
            foreach (var misscompSubtype in missingComponents)
            {
                blockComponentsAndInv[MyDefinitionId.Parse("MyObjectBuilder_Component/" + misscompSubtype.Key)] -= misscompSubtype.Value;
            }
            if (block is MyObjectBuilder_TerminalBlock)
            {
                var tank = block as Sandbox.Common.ObjectBuilders.MyObjectBuilder_OxygenTank;
                var gasTankDef = blockDef as MyGasTankDefinition;
                if (gasTankDef != null && tank != null)
                {
                    float volumeinlitres = gasTankDef.Capacity * tank.FilledRatio;
                    // Log.Error("gasTankDef.StoredGasId: " + gasTankDef.StoredGasId.ToString()); //MyObjectBuilder_GasProperties/Hydrogen
                    // Log.Error("gasTankDef.StoredGasId.SubtypeName: " + gasTankDef.StoredGasId.SubtypeName.ToString()); //Hydrogen
                    GasToOre(gasList, gasTankDef.StoredGasId.SubtypeName, volumeinlitres, out List<Tuple<string, double>> gasOreItems);
                    foreach (var ore in gasOreItems)
                    {
                        blockComponentsAndInv.Sum((MyDefinitionId.Parse(ore.Item1)), (MyFixedPoint)ore.Item2);
                    }
                }

                // Inventory check
                if (block.ComponentContainer != null)
                {
                    var compData = block.ComponentContainer.Components.Find(s => s.Component.TypeId == typeof(MyObjectBuilder_Inventory));
                    if (compData != null)
                    {
                        var inventory = (compData.Component as MyObjectBuilder_Inventory).Items;
                        if (inventory != null || inventory.Count != 0)
                        {
                            foreach (var item in inventory)
                            {
                                var itemdefinitionid = item.PhysicalContent.GetId();
                                blockComponentsAndInv.Sum(itemdefinitionid, item.Amount);
                                // Go through Gas bottles.
                                var gasContainer = item.PhysicalContent as MyObjectBuilder_GasContainerObject;
                                if (gasContainer != null)
                                {
                                    var gascontainerdefinition = (MyOxygenContainerDefinition)MyDefinitionManager.Static.GetPhysicalItemDefinition(itemdefinitionid);
                                    float volumeinlitres = gascontainerdefinition.Capacity * gasContainer.GasLevel;
                                    GasToOre(gasList, gascontainerdefinition.StoredGasId.SubtypeName, volumeinlitres, out List<Tuple<string, double>> gasOreItems);
                                    foreach (var ore in gasOreItems)
                                    {
                                        blockComponentsAndInv.Sum((MyDefinitionId.Parse(ore.Item1)), (MyFixedPoint)ore.Item2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void GasToOre(List<GasToOreRatio> gasList, string gasSubtypeName, float litres, out List<Tuple<string, double>> ListDefinitionidStr)
        {
            ListDefinitionidStr = new List<Tuple<string, double>>();
            bool found = false;
            foreach (var item in gasList)
            {
                if (gasSubtypeName.Contains(item.GasName))
                {
                    foreach (var ore in item.Ores)
                    {
                        ListDefinitionidStr.Add(new Tuple<string, double>(ore.Ore, litres * ore.KgFrom1Liter));
                    }
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                ListDefinitionidStr = new List<Tuple<string, double>>() { new Tuple<string, double>("MyObjectBuilder_Ore/Ice", litres) };
            }
        }

        private static Dictionary<string, int> GetMissingComponents(MyObjectBuilder_CubeBlock block, MyCubeBlockDefinition blockDefintion)
        {
            var result_Dictionary = new Dictionary<string, int>();
            var stack = new MyComponentStack(blockDefintion, block.IntegrityPercent, block.BuildPercent);
            stack.GetMissingComponents(result_Dictionary);
            return result_Dictionary;
        }

        public class OreRatio
        {
            public string Ore;
            public float KgFrom1Liter;
        }

        public class GasToOreRatio
        {
            public string GasName;
            public List<OreRatio> Ores;

            public GasToOreRatio()
            {
                GasName = "";
                Ores = new List<OreRatio>();
            }

            public GasToOreRatio(string _GasName, List<OreRatio> _OreName)
            {
                GasName = _GasName;
                Ores = _OreName;
            }
        }
    }
}