﻿using Sandbox;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;

namespace NAPI
{
	public class MemoryTracker : Action1<long>
	{
		private static bool inited = false;
		private static MemoryTracker instance = new MemoryTracker();

        public static int GCCounter;
		public static long lastGC;
		public static long now;
		public static ulong gcTick;
		public static long memoryPerTick;
		

		public static void Init ()
		{
            if (inited) return;
            inited = true;
            FrameExecutor.addFrameLogic(instance);
        }

		public MemoryTracker()
		{
			now = GC.GetTotalMemory(false);
            lastGC = now;
			GCCounter = 0;
            memoryPerTick = 0;
            gcTick = 0;//MySandboxGame.Static?.FrameTimeTicks ?? 0;
		}

		public static bool Changed(long prevMemory)
		{
			if (GC.GetTotalMemory(false) < prevMemory)
			{
				return true;
			}
			return false;
		}

		public void Track ()
		{
            if (MySandboxGame.Static == null) return;
			var prevValue = now;
			now = GC.GetTotalMemory(false);
            if (now < prevValue)
			{
                var nowTick = MySandboxGame.Static.SimulationFrameCounter+1;
                if ((nowTick - gcTick) != 0)
                {
                    memoryPerTick = (prevValue - lastGC)/((long)(nowTick - gcTick));
                }
                gcTick = nowTick;
                GCCounter++;
				lastGC = now;
			}
		}

		public void run(long t)
		{
            try
            {
                Track();
            } catch (Exception e)
            {
                Log.Error (e);
            }
			
		}
	}
}
