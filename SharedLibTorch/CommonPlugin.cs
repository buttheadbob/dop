﻿using System;
using System.Collections.Generic;
using NLog;
using Torch;
using Torch.API;
using Torch.API.Session;
using Torch.API.Managers;
using Torch.Managers.PatchManager;
using Torch.Session;
using NAPI;
using SharedLibTorch;
using System.Windows.Controls;
using Torch.API.Plugins;
using Sandbox.ModAPI;

namespace TorchPlugin
{
	public abstract class CommonPlugin : TorchPluginBase, IWpfPlugin
	{
		public static ITorchBase Torch = null;
		public UserControl _control = null;
        bool updaterEnabled = false;

		public override void Init(ITorchBase torch)
		{
			base.Init(torch);
            
            if (Torch == null)
            {
                updaterEnabled = true;
            }
			Torch = torch;
			Log.Init(new TorchLogger (LogManager.GetCurrentClassLogger()));
			var sessionManager = Torch.Managers.GetManager<TorchSessionManager>();
			if (sessionManager != null) sessionManager.SessionStateChanged += SessionChanged;
			else Log.Error("No session manager loaded!");

            if (EarlyPatches.Count > 0)
            {
                try
                {
                    var pm = Torch.Managers.GetManager<PatchManager>();
                    var context = pm.AcquireContext();
                    EarlyPatch(context);
                    pm.Commit();
                }
                catch (Exception e)
                {
                    Log.Fatal(this.GetType().Name+":: EarlyPatch: " + e);
                }
            }
        }

        private List<Action<PatchContext>> EarlyPatches = new List<Action<PatchContext>>();

        public void AddEarlyPatch(Action<PatchContext> patch)
        {
            EarlyPatches.Add(patch);
        }

        protected void Patch (Action<PatchContext> action)
        {
            var pm2 = Torch.Managers.GetManager<PatchManager>();
            var context = pm2.AcquireContext();
            action (context);
            pm2.Commit();
        }

        public virtual void SessionChanged(ITorchSession session, TorchSessionState state)
        {
            switch (state)
            {
                case TorchSessionState.Loading:
                    try
                    {
                        var pm2 = Torch.Managers.GetManager<PatchManager>();
                        var context = pm2.AcquireContext();
                        PrePatch(context);
                        pm2.Commit();
                    }
                    catch (Exception e)
                    {
                        Log.Fatal(this.GetType().Name+"::PrePatch: " + e);
                    }
                    break;
                case TorchSessionState.Loaded:  
                    try
                    {
                        var pm = Torch.Managers.GetManager<PatchManager>();
                        var context = pm.AcquireContext();
                        Patch(context);
                        pm.Commit();
                        
                        Log.Error($"MIG {this.GetType()}: PATCH COMPLETED");
                    }
                    catch (Exception e)
                    {
                        Log.Fatal(this.GetType().Name+"::Patch: " + e);
                    }
                    break;
            }
        }

        public virtual void Patch(PatchContext context)
        {

        }

        public virtual void PrePatch(PatchContext context)
        {

        }

        public virtual void EarlyPatch(PatchContext context)
        {
        }

        public override void Update()
		{
			base.Update();
			if (updaterEnabled) {
                FrameExecutor.Update();
            }
		}

        public UserControl GetControl()
        {
            if (_control == null)
            {
                _control = CreateControl();
            }
            return _control;
        }

        public abstract UserControl CreateControl();



        public static void InvokeTorch(Action a)
        {
            Torch.Invoke(a, "Invoke torch");
        }

        public void UpdateUI(Action<UserControl> action)
        {
            try
            {
                if (_control != null)
                {
                    _control.Dispatcher.BeginInvoke(() =>
                    {
                        try
                        {
                            action.Invoke(_control);
                        }
                        catch (Exception e)
                        {
                            Log.Error(e, "Something wrong in executing function:" + action);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "Cant UpdateUI");
            }
        }


    }
}