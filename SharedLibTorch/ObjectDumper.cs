﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

public class ObjectDumper
{
    private int _level;
    private readonly int _indentSize;
    private readonly StringBuilder _stringBuilder;
    private HashSet<object> _ignoredElements = new HashSet<object>();
    private HashSet<Type> _ignoredTypes = new HashSet<Type>();

    private ObjectDumper(int indentSize)
    {
        _indentSize = indentSize;
        _stringBuilder = new StringBuilder();
        _ignoredElements = new HashSet<object>();
    }

    public static string Dump(object element, int indentSize = 2, HashSet<object> ignoredElements = null, HashSet<Type> ignoredTypes = null)
    {
        var instance = new ObjectDumper(indentSize);
        if (ignoredElements == null) ignoredElements = new HashSet<object>();
        if (ignoredTypes == null) ignoredTypes = new HashSet<Type>();


        instance._ignoredTypes = ignoredTypes;
        instance._ignoredElements = ignoredElements;

        instance.DumpElement(element);

        return instance._stringBuilder.ToString();
    }

    private void DumpElement(object element)
    {
        var s = GetSimpleValue(element);
        if (s != null)
        {
            Write(s);
            return;
        }

        var objectType = element.GetType();
        foreach (var x in _ignoredTypes)
        {
            if (x.IsAssignableFrom(objectType))
            {
                Write(objectType.Name + " [ignored]" + objectType.FullName);
                return;
            }
        }

        var enumerableElement = element as IEnumerable;
        if (enumerableElement != null)
        {
            foreach (object item in enumerableElement)
            {
                if (item is IEnumerable && !(item is string))
                {
                    _level++;
                    DumpElement(item);
                    _level--;
                }
                else
                {
                    if (!AlreadyTouched(item))
                        DumpElement(item);
                    else
                        Write("{{{0}}} <-- bidirectional reference found", item.GetType().FullName);
                }
            }
        }
        else
        {
            Write("{{{0}}}", objectType.FullName);
            _ignoredElements.Add(element);
            _level++;
            PrintClassMembers(element);
            _level--;
        }
    }

    private void PrintClassMembers(object element)
    {
        MemberInfo[] members = element.GetType().GetMembers(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
        foreach (var memberInfo in members)
        {
            var fieldInfo = memberInfo as FieldInfo;
            var propertyInfo = memberInfo as PropertyInfo;

            if (fieldInfo == null && propertyInfo == null)
                continue;

            if (memberInfo.Name.Contains("k__BackingField"))
                continue;

            if (_stringBuilder.Length > 150000)
            {
                Write("!Break!");
                break;
            }

            object value = null;
            Type type = null;
            try
            {
                if (fieldInfo != null)
                {
                    value = fieldInfo.GetValue(element);
                    type = fieldInfo.FieldType;
                }
                else
                {
                    value = propertyInfo.GetValue(element, null);
                    type = propertyInfo.PropertyType;
                }
            }
            catch (Exception e)
            {
                value = "Error at getting info : " + memberInfo.DeclaringType + "." + memberInfo.Name;
                type = typeof(string);
            }


            var v = GetSimpleValue(value);
            if (v != null)
            {
                Write("{0}: {1}", memberInfo.Name, v);
                continue;
            }

            var isEnumerable = typeof(IEnumerable).IsAssignableFrom(type);
            Write("{0}: {1}", memberInfo.Name, isEnumerable ? "..." : "{ }");

            var alreadyTouched = !isEnumerable && AlreadyTouched(value);
            _level++;
            if (!alreadyTouched)
                DumpElement(value);
            else
                Write("{{{0}}} <-- bidirectional reference found", value.GetType().FullName);
            _level--;
        }
    }

    private bool AlreadyTouched(object value)
    {
        return _ignoredElements.Contains(value);
    }

    private void Write(string value, params object[] args)
    {
        var space = new string(' ', _level * _indentSize);

        if (args != null && args.Length > 0)
            value = string.Format(value, args);

        _stringBuilder.AppendLine(space + value);
    }

    private string GetSimpleValue(object o)
    {
        if (o == null)
            return ("null");

        if (o is string)
            return string.Format("\"{0}\"", o);

        if (o is char && (char)o == '\0')
            return string.Empty;

        if (o is ValueType)
            return (o.ToString());

        if (o is DateTime dt)
            return dt.ToShortDateString();

        if (o is StringBuilder sb)
            return sb.ToString();

        if (o is TimeSpan ts)
            return ts.ToString();

        if (o.GetType().Namespace.StartsWith ("System") || o.GetType().Namespace.StartsWith("Microsoft"))
        {
            return o.ToString();
        }

        return null;
    }
}