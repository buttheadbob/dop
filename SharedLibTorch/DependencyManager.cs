﻿using Torch.API;
using Torch.Managers.PatchManager;
using Torch.Session;
using Torch.Managers;

namespace NAPI
{
    public class DependencyManager : Manager
    {
        public static DependencyManager instance;

        [Dependency] public readonly PatchManager _patchManager;
        [Dependency] public readonly TorchSessionManager _sessionManager;

        public PatchContext _ctx;

        public DependencyManager(ITorchBase torchInstance) : base(torchInstance) { instance = this; }

        public override void Attach()
        {
            base.Attach();
            if (_ctx == null) _ctx = _patchManager.AcquireContext();
        }

        public override void Detach()
        {
            base.Detach();
            _patchManager.FreeContext(_ctx);
            _ctx = null;
        }
    }
}