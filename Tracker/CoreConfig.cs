using Slime.GUI;
using Torch;
using VRageMath;

namespace Slime.Config
{
    public partial class CoreConfig : ViewModel
    {
        private string _authKey;
        private bool _pluginenabled;

        private int _trackInterval = 60 * 5;
        private bool _trackPhysics = false;
        private bool _trackCrashes = false;
        private bool _trackEnabled = false;
        private bool _trackPlayersCount = true;




        public string AuthKey { get { return _authKey; } set { SetValue<string>(ref _authKey, value); } }
        public bool PluginEnabled { get { return _pluginenabled; } set { SetValue(ref _pluginenabled, value); } }

        //==========================================================================================================================
        //MyLargeGatlingTurret.OnModelChange()

        [DisplayTab(GroupName = "Tracking", Tab = "Features")]
        public bool TrackEnabled { get { return _trackEnabled; } set { SetValue(ref _trackEnabled, value); } }

        [DisplayTab(GroupName = "Tracking", Tab = "Features", Name = "TrackInterval (sec)")]
        public int TrackInterval
        {
            get { return _trackInterval; }
            set
            {
                var vv = MathHelper.Clamp(value, 10, 15 * 60);
                SetValue(ref _trackInterval, vv);
            }
        }

        [DisplayTab(GroupName = "Tracking", Tab = "Features", Description = "How much time Havok takes time")]
        public bool TrackPhysics
        {
            get { return _trackPhysics; }
            set
            {
                SetValue(ref _trackPhysics, value);
            }
        }

        [DisplayTab(GroupName = "Tracking", Tab = "Features", Description = "Send crash-info to server")]
        public bool TrackCrashes
        {
            get { return _trackCrashes; }
            set
            {
                SetValue(ref _trackCrashes, value);
            }
        }

        [DisplayTab(GroupName = "Tracking", Tab = "Features")]
        public bool TrackPlayersCount { get { return _trackPlayersCount; } set { SetValue(ref _trackPlayersCount, value); } }
    }
}