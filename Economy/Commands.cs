﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Windows.Documents;
using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Torch.API.Managers;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game;
using VRage.Game.ModAPI;

namespace Economy
{
    [Category("mig-economy")]
    public class Commands : CommandModule
    {
        [Command("share-limits", "Transfer old limits/rights to faction members/other builders on all grids.")]
        [Permission(MyPromoteLevel.Admin)]
        public void LimClean(double days, bool AddNobody = false, bool FixOwners = false, bool LogOnly = false, bool IgnoreNPC = false)
        {
            var CleanTask = new CleaningTask(days, AddNobody, FixOwners, LogOnly, IgnoreNPC);
            CleanTask.CollectOldIdentities();
            CleanTask.CleanLimits();
            Log.Warn(CleanTask.PrintAllLogs());
            Context.Respond(CleanTask.PrintShortInfo());
        }

        [Command("share-limits", "Transfer old limits/rights to faction members/other builders on all grids. But only affects input nicks.")]
        [Permission(MyPromoteLevel.Admin)]
        public void LimCleanGroup(double days, bool AddNobody = false, bool FixOwners = false, bool LogOnly = false, bool IgnoreNPC = false, string Nick = "", string Nick2 = "", string Nick3 = "", string Nick4 = "", string Nick5 = "")
        {
            if (!TryFindPlayers(out Dictionary<long, MyIdentity> _playersDict, Nick, Nick2, Nick3, Nick4, Nick5))
            {
                Context.Respond($"Cant start cleaning, needs at least one player specified.");
                return;
            }

            var CleanTask = new CleaningTask(days, AddNobody, FixOwners, LogOnly, IgnoreNPC);
            CleanTask.AddPlayers(_playersDict);
            CleanTask.CleanLimits();
            Log.Warn(CleanTask.PrintAllLogs());
            Context.Respond(CleanTask.PrintShortInfo());

            bool TryFindPlayers(out Dictionary<long, MyIdentity> _identities, params string[] nicks)
            {
                var allIdentities = MySession.Static.Players.GetAllIdentities();
                _identities = new Dictionary<long, MyIdentity>();
                foreach (var n in nicks)
                {
                    if (!string.IsNullOrWhiteSpace(n))
                    {
                        var t = allIdentities.Where(x => x.DisplayName == (n));
                        if (!t.Any())
                        {
                            Context.Respond($"Cant find player with [{n}] name.");
                        }
                        else
                        {
                            var idt = t.FirstOrDefault();
                            _identities.Add(idt.IdentityId, idt);
                            Context.Respond($"Added ( {n} )[{idt.IdentityId}] to process list.");
                        }
                    }
                }
                return _identities.Any();
            }
        }
    }
}