﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Windows.Documents;
using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Slime;
using Torch.API.Managers;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game;
using VRage.Game.ModAPI;

namespace Economy
{
    public class CleaningTask
    {
        private List<StringBuilder> m_logs = new List<StringBuilder>();
        private Dictionary<long, MyIdentity> m_oldIdentities = new Dictionary<long, MyIdentity>();
        private Dictionary<long, List<long>> m_factionsData;
        private Dictionary<long, MyIdentity> m_allIdentities;
        private int m_transferedOldBuildersBlocks = 0;
        private int m_fixedOwnershipBlocks = 0;
        private int m_transferedOldBuildersGrids = 0;
        private int m_deletedGrids = 0;
        private double m_days;
        private bool m_addNobody;
        private bool m_fixOwners;
        private bool m_logOnly;
        private bool m_cleanupIngnoreNPC;

        public CleaningTask(double days, bool addNobody, bool fixOwners, bool logOnly, bool cleanupIngnoreNPC)
        {
            m_cleanupIngnoreNPC = cleanupIngnoreNPC;
            m_addNobody = addNobody;
            m_fixOwners = fixOwners;
            m_logOnly = logOnly;
            m_days = days;
            m_allIdentities = CollectAllIdentities();
        }

        public void CleanLimits()
        {
            var sw = System.Diagnostics.Stopwatch.StartNew();
            try
            {
                CleanLimitsInternal();
            }
            catch (Exception ex)
            {
                if (m_logs == null)
                {
                    m_logs = new List<StringBuilder>();
                }
                m_logs.Add(new StringBuilder($"CleanLimits throw Exception: " + ex.ToString() + Environment.NewLine));
            }
            finally
            {
                sw?.Stop();
                var report = new StringBuilder("Elapsed ms: " + sw?.ElapsedMilliseconds + Environment.NewLine);
                report.AppendLine($"{m_oldIdentities?.Count} old identities found, transfered {m_transferedOldBuildersBlocks} blocks in {m_transferedOldBuildersGrids} grids, deleted {m_deletedGrids} grids. Fixed [{m_fixedOwnershipBlocks}] blocks with wrong owners." + Environment.NewLine);
                m_logs.Add(report);
            }
        }

        void CleanLimitsInternal()
        {
            m_logs.Add(new StringBuilder($"Transfer old players limits players start" + Environment.NewLine));
            var allGrids = MyEntities.GetEntities().OfType<MyCubeGrid>().ToList();
            if (m_addNobody)
            {
                m_oldIdentities.Add(0, null);
            }
            else
            {
                m_allIdentities.Add(0, null);
            }

            m_factionsData = GetFactionsData();
            m_logs.Add(new StringBuilder($"All identities: [{m_allIdentities.Count}] Old identities: [{m_oldIdentities.Count}]" + Environment.NewLine));
            if (!m_oldIdentities.Any())
            {
                m_logs.Add(new StringBuilder($"No old identities found." + Environment.NewLine));
                return;
            }
            MyAPIGateway.Parallel.ForEach(MyCubeGridGroups.Static.Physical.Groups, (x) =>
            {
                StringBuilder MyLog = new StringBuilder();
                try
                {
                    GetBlocksAndBuildersFromGrids(ref MyLog, x.Nodes, out List<long> gridBuildersSorted, out HashSet<MySlimBlock> blocksToTransfer, out HashSet<MyCubeBlock> blocktofixowner);
                    var gridBuildersOld = gridBuildersSorted.Where(b => m_oldIdentities.ContainsKey(b) || !m_allIdentities.ContainsKey(b)).ToList();

                    MyLog.AppendLine($"Grid group: [{x.Nodes.FirstOrDefault().NodeData.DisplayName}] gridBuildersOld: [{gridBuildersOld.Count()}] blocksToTransfer: [{blocksToTransfer.Count()}] gridBuildersSorted: [{gridBuildersSorted.Count}]");

                    if (!gridBuildersSorted.Where(b => b != 0).Any())
                    { //no builders.
                        MyLog.AppendLine($"No OK builders for transfer, so close it.");
                        DeleteGroup(ref MyLog, x.Nodes);
                        return;
                    }

                    if (gridBuildersOld.Any())
                    {
                        MyLog.AppendLine($"Start process this grid group(OldBuilders):");
                        if (ProcessGridGroup(ref MyLog, ref blocksToTransfer, ref gridBuildersSorted, gridBuildersOld))
                        {
                            m_transferedOldBuildersGrids += x.Nodes.Count();
                            MyLog.AppendLine("Full transfer ok.");
                        }
                        else
                        {
                            MyLog.AppendLine($"Full transfer fail!? , so close it.");
                            DeleteGroup(ref MyLog, x.Nodes);
                            return;
                        }
                    }

                    if (m_fixOwners && blocktofixowner.Any())
                    {
                        var block = blocktofixowner.FirstOrDefault();
                        MyLog.AppendLine($"Fix owner at [{block.CubeGrid}] grid. First block: [{block.Name}]OwnerId:[{block.OwnerId}]");
                        ProcessGridGroupOwnership(ref blocktofixowner);
                    }
                }
                catch (Exception ex)
                {
                    MyLog.AppendLine($"Exception in parallel loop ex: {ex}");
                }
                finally
                {
                    if (MyLog.Length != 0)
                        m_logs.Add(MyLog);
                }
            });
        }
        #region helpers
        public void AddPlayers(Dictionary<long, MyIdentity> playersDict)
        {
            var cutoff = DateTime.Now - TimeSpan.FromDays(m_days);
            m_oldIdentities = playersDict.Where((x) => x.Value.LastLoginTime < cutoff).ToDictionary(x => x.Value.IdentityId, y => y.Value);
        }

        public void CollectOldIdentities()
        {
            var cutoff = DateTime.Now - TimeSpan.FromDays(m_days);
            m_oldIdentities = m_allIdentities.Where((x) => x.Value.LastLoginTime < cutoff).ToDictionary(x => x.Value.IdentityId, y => y.Value);            
        }

        private Dictionary<long, MyIdentity> CollectAllIdentities()
        {
            return MySession.Static.Players.GetAllIdentities().ToDictionary(x => x.IdentityId, y => y); 
        }

        public string PrintShortInfo()
        {
            return m_logs.Last().ToString();
        }

        public string PrintAllLogs()
        {
            var b = new StringBuilder("");
            foreach (var bstr in m_logs)
            {
                b.Append(bstr);
            }
            return b.ToString();
        }

        void GetBlocksAndBuildersFromGrids(ref StringBuilder log, VRage.Collections.HashSetReader<VRage.Groups.MyGroups<MyCubeGrid, MyGridPhysicalGroupData>.Node> grids, out List<long> buildersImpact, out HashSet<MySlimBlock> blocksToTransfer, out HashSet<MyCubeBlock> blockToFixOwner)
        {
            blocksToTransfer = new HashSet<MySlimBlock>();
            blockToFixOwner = new HashSet<MyCubeBlock>();
            var gridbuildersDict = new Dictionary<long, int>();
            foreach (var g in grids)
            {
                if (g.NodeData.Closed || g.NodeData.MarkedForClose) { continue; }

                foreach (var block in g.NodeData.CubeBlocks)
                {
                    if (m_oldIdentities.ContainsKey(block.BuiltBy) || !m_allIdentities.ContainsKey(block.BuiltBy))
                    {
                        blocksToTransfer.Add(block);
                    }
                    else
                    {
                        if (block.FatBlock != null && block.BlockDefinition.ContainsComputer() && (m_oldIdentities.ContainsKey(block.FatBlock.OwnerId) || !m_allIdentities.ContainsKey(block.FatBlock.OwnerId)))
                        {                            
                            blockToFixOwner.Add(block.FatBlock);
                        }
                    }

                    if (gridbuildersDict.ContainsKey(block.BuiltBy))
                        gridbuildersDict[block.BuiltBy] += 1;
                    else
                        gridbuildersDict.Add(block.BuiltBy, 1);
                }
            }

            foreach (var fac in gridbuildersDict)
            {
                log.AppendLine($"RAW Builder: id=[{fac.Key}] / blocks=[{fac.Value}]");
            }

            //взяли билдеров gridbuildersDict
            var facBlocks = new Dictionary<long, int>();
            foreach (var b in gridbuildersDict) //посчитали их фракционыые блоки если есть
            {
                var fac = b.Key.PlayerFaction();
                if (fac != null)
                {
                    if (facBlocks.ContainsKey(fac.FactionId))
                    {
                        facBlocks[fac.FactionId] = facBlocks[fac.FactionId] + b.Value;
                    }
                    else
                    {
                        facBlocks.Add(fac.FactionId, b.Value);
                    }
                }
            }

            foreach (var b in gridbuildersDict.Keys.ToList())
            {
                var fac = b.PlayerFaction();
                if (fac != null && facBlocks.ContainsKey(fac.FactionId))
                {
                    gridbuildersDict[b] = facBlocks[fac.FactionId];//заменили их блоки фракционными
                                                                   //хорошо бы их тоже сортировать с учетом *вклада*
                }
            }

            buildersImpact = gridbuildersDict.OrderBy((x) => -x.Value).Select(x => x.Key).ToList();

            foreach (var b in buildersImpact)
            {
                log.AppendLine($"Sorted BuilderId: [{b}] / blocks or faction blocks: [{gridbuildersDict[b]}]");
            }
        }

        void ProcessGridGroupOwnership(ref HashSet<MyCubeBlock> blocktofixowner)
        {
            if (!m_logOnly)
            {
                foreach (var b in blocktofixowner)
                {
                    b.ChangeOwner(b.BuiltBy, MyOwnershipShareModeEnum.Faction);
                }
            }

            m_fixedOwnershipBlocks += blocktofixowner.Count();
        }

        bool ProcessGridGroup(ref StringBuilder log, ref HashSet<MySlimBlock> blocksToTransfer, ref List<long> gridbuilders, List<long> oldbuilders)
        {
            var otherOkBuilders = gridbuilders.Where(b => !m_oldIdentities.ContainsKey(b) && m_allIdentities.ContainsKey(b));
            log.AppendLine($"otherOkBuilders = [{otherOkBuilders.Count()}] gridbuilders = [{gridbuilders.Count}]");
            foreach (var block in blocksToTransfer)
            {
                long targetbuilder = 0;
                var buildby = block.BuiltBy;
                if (oldbuilders.Contains(buildby))
                {
                    //TryGetFactionMembers(m_oldIdentities[buildby], out List<KeyValuePair<long, int>> members);
                    if (!TryTransferToFaction(ref log, block, buildby, out targetbuilder))
                    {
                        //cant transfer to our faction members so:
                        if (otherOkBuilders.Any())
                        {
                            if (!TryTransferToFaction(ref log, block, otherOkBuilders.FirstOrDefault(), out targetbuilder))
                            {
                                //cant transfer to other builder's faction members
                                //so overlimit this builder
                                targetbuilder = otherOkBuilders.FirstOrDefault();
                            }
                        }
                        else
                        {
                            //no other possible builders so scan all factions on all builders:
                            if (!TryTransferToAnyone(ref log, block, ref gridbuilders, out targetbuilder))
                            {
                                //cant transfer to anyone
                                //so delete this grid
                                return false;
                            }
                        }
                    }

                    if (!m_logOnly)
                    {
                        block.RemoveAuthorship();
                        block.TransferAuthorshipClient(targetbuilder);
                        block.AddAuthorship();
                        if (block.FatBlock != null)
                        {
                            block.FatBlock.ChangeOwner(targetbuilder, MyOwnershipShareModeEnum.Faction);
                        }
                    }
                    m_transferedOldBuildersBlocks++;
                }
                else
                {
                    log.AppendLine($"Normal block?Wrong owner?, grid: [{block.CubeGrid.DisplayName}] buildby: [{buildby}] IsOld?:{m_oldIdentities.ContainsKey(buildby)} IsOK?: {m_allIdentities.ContainsKey(buildby)} Owner: [{block.OwnerId}] OwnerIsOld:{m_oldIdentities.ContainsKey(block.OwnerId)} OwnerIsOK?: {m_allIdentities.ContainsKey(block.OwnerId)} targetbuilder = {targetbuilder} oldbuilders.Contains(buildby): [{oldbuilders.Contains(buildby)}]");
                    foreach (var t in oldbuilders)
                    {
                        log.AppendLine($"oldbuilder: {t}");
                    }
                }
            }
            return true;
        }

        bool TryTransferToAnyone(ref StringBuilder log, MySlimBlock block, ref List<long> gridBuilders, out long targetBuilder)
        {
            foreach (var player in gridBuilders)
            {
                if (m_factionsData.TryGetValue(player, out List<long> facdata2))
                {
                    foreach (var member in facdata2)
                    {
                        if (CanRecieve(ref log, member, block))
                        {
                            targetBuilder = member;
                            return true;
                        }
                    }
                }
            }

            foreach (var player in gridBuilders)
            {
                if (m_factionsData.TryGetValue(player, out List<long> facdata2))
                {
                    foreach (var member in facdata2)
                    {
                        if (IsRealPlayer(log, member, out MyIdentity indt))
                        {
                            targetBuilder = member;
                            return true;
                        }
                    }
                }
            }
            targetBuilder = 0;
            return false;
        }

        bool TryTransferToFaction(ref StringBuilder log, MySlimBlock block, long facmem, out long targetBuilder)
        {
            if (m_factionsData.TryGetValue(facmem, out List<long> members))
            {
                foreach (var player in members)
                {
                    if (CanRecieve(ref log, player, block))
                    {
                        targetBuilder = player;
                        return true;
                    }
                }
            }
            targetBuilder = 0;
            return false;
        }

        private Dictionary<long, List<long>> GetFactionsData()
        {
            var result = new Dictionary<long, List<long>>();
            foreach (var identity in m_allIdentities)
            {
                var members = new Dictionary<long, int>();
                MyFaction f = (MyFaction)MyAPIGateway.Session.Factions.TryGetPlayerFaction(identity.Key);
                if (f != null)
                {
                    var m = f.Members;
                    if (m.Count != 1)
                    {
                        foreach (var mem in f.Members)
                        {
                            if (!m_oldIdentities.ContainsKey(mem.Key))
                            {
                                if (members == null) members = new Dictionary<long, int>();
                                if (IsRealPlayer(m_logs[0], mem.Key, out MyIdentity ident))
                                {
                                    members.Add(mem.Key, ident.BlockLimits.PCUBuilt);
                                }
                            }
                        }
                    }

                    if (members != null)
                    {
                        result.Add(identity.Key, members.OrderBy(x => x.Value).Select(x => x.Key).ToList());
                    }
                }
            }
            return result;
        }

        bool IsRealPlayer(StringBuilder log, long id, out MyIdentity _myIdentity)
        {
            MyIdentity myIdentity = MySession.Static.Players.TryGetIdentity(id);
            _myIdentity = myIdentity;
            if (myIdentity == null)
            {
                log.AppendLine($"(IsRealPlayer) Cant find player identity, IdentityId:[{id}]");
                return false;
            }

            if (!MySession.Static.Players.TryGetPlayerId(myIdentity.IdentityId, out MyPlayer.PlayerId playerid))
            {
                log.AppendLine($"(IsRealPlayer) Cant find player with IdentityId: [{myIdentity.IdentityId}] Name:[{myIdentity.DisplayName}]");
                return false;
            }

            if ((MySession.Static.Players.IdentityIsNpc(myIdentity.IdentityId) || playerid.SteamId == 0) && !m_cleanupIngnoreNPC)
            {
                log.AppendLine($"(IsRealPlayer) skip this NPC IdentityId: [{myIdentity.IdentityId}] Name:[{myIdentity.DisplayName}]");
                return false;
            }
            return true;
        }

        void DeleteGroup(ref StringBuilder log, VRage.Collections.HashSetReader<VRage.Groups.MyGroups<MyCubeGrid, MyGridPhysicalGroupData>.Node> nodes)
        {
            foreach (var g in nodes)
            {
                if (!m_logOnly)
                {
                    g.NodeData.Close();
                }
                m_deletedGrids++;
            }
        }

        bool CanRecieve(ref StringBuilder log, long ownerID, MySlimBlock b)
        {
            MyIdentity myIdentity = MySession.Static.Players.TryGetIdentity(ownerID);
            if (myIdentity == null || !MySession.Static.Players.TryGetPlayerId(myIdentity.IdentityId, out MyPlayer.PlayerId playerid))
            {
                log.AppendLine("(TryTransferTo.CanRecieve) Cant find player with EntId: " + myIdentity.IdentityId + "NAme:" + myIdentity.DisplayName);
                return false;
            }

            if ((MySession.Static.Players.IdentityIsNpc(myIdentity.IdentityId) || playerid.SteamId == 0) && !m_cleanupIngnoreNPC)
            {
                log.AppendLine("skip this NPC EntId: " + myIdentity.IdentityId + "Name: " + myIdentity.DisplayName);
                return false;
            }

            MyBlockLimits blockLimits = myIdentity.BlockLimits;
            if (MySession.Static.MaxBlocksPerPlayer != 0 && blockLimits.BlocksBuilt + 1 > blockLimits.MaxBlocks)
            {
                return false;
            }

            if (MySession.Static.TotalPCU != 0 && b.BlockDefinition.PCU > blockLimits.PCU)
            {
                if (b.BlockDefinition.PCU < 0)
                {
                    return false;
                }
            }

            string blockPairName = b.BlockDefinition.BlockPairName;
            if (blockLimits.BlockTypeBuilt.TryGetValue(blockPairName, out MyBlockLimits.MyTypeLimitData blockBuilded))
            {
                var futureBlockBuild = 1 + blockBuilded.BlocksBuilt;
                if (futureBlockBuild > MySession.Static.GetBlockTypeLimit(blockPairName))
                {
                    return false;
                }
            }

            return true;
        }
        #endregion
    }
}
