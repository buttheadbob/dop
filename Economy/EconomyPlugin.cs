﻿using Torch.Managers.PatchManager;
using System.Windows.Controls;
using TorchPlugin;
using Torch.API;
using Torch;
using System.IO;
using Slime.GUI;
using Slime.Config;
using Economy;

namespace Slime
{
    public class EconomyPlugin : CommonPlugin
    {
        public static CoreConfig Config => _config.Data;
        private static Persistent<CoreConfig> _config;

        public override void Init(ITorchBase torch)
        {
            base.Init(torch);
            _config = Persistent<CoreConfig>.Load(Path.Combine(StoragePath, "MIG-Economy.cfg"));
        }

        public override void Patch(PatchContext patchContext)
        {
            DisableBlocks.Patch(patchContext);
        }

        public override void Dispose()
        {
            _config.Save(Path.Combine(StoragePath, "MIG-Economy.cfg"));
            base.Dispose();
        }

        public override UserControl CreateControl()
        {
            return new MainControl();
        }
    }
}