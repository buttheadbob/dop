using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Slime;
using System;
using Torch.Managers.PatchManager;
using VRage.Game;
using VRage.Game.Entity;

namespace Economy
{
    class DisableBlocks
    {
        public static void Patch (PatchContext ctx)
        {

            if (EconomyPlugin.Config.DisableBlockNoBuiltBy || EconomyPlugin.Config.OfflineDisabledBlocksMaxTime >= 0 || EconomyPlugin.Config.DisableBlocksOnOverLimit)
            {
                var ent = MyEntities.GetEntities();
                foreach (var e in ent) EntityAdded(e);
                MyEntities.OnEntityAdd += EntityAdded;
            } 
        }

        private static void EntityAdded(MyEntity e)
        {
            var grid = e as MyCubeGrid;
            if (grid != null)
            {
                foreach (var f in grid.GetFatBlocks())
                {
                    var fun = f as MyFunctionalBlock;
                    if (fun != null)
                    {
                        EnabledChanged(fun);
                        fun.EnabledChanged += EnabledChanged;
                    }
                }
            }
        }

        public static void DelayedDisable(MyFunctionalBlock block)
        {
            FrameExecutor.addDelayedLogic (3, (x)=> block.Enabled = false);
        }

        private static void EnabledChanged(MyTerminalBlock obj)
        {
            try
            {
                var fu = obj as MyFunctionalBlock;
                if (!fu.Enabled) {
                    return;
                }

                var c = EconomyPlugin.Config;
                if (obj.BuiltBy == 0)
                {
                    if (c.DisableBlockNoBuiltBy)
                    {
                        DelayedDisable (fu);
                    }
                    //Log.Error("BuiltBy == 0");
                    return;
                }

                var identity = (MyAPIGateway.Multiplayer.Players as MyPlayerCollection).TryGetIdentity(obj.BuiltBy);
                if (identity == null)
                {
                    if (c.DisableBlockNoBuiltBy)
                    {
                        DelayedDisable(fu);
                    }
                    //Log.Error("Identity == 0");
                    return;
                }

                if (identity.BlockLimits.IsOverLimits)
                {
                    if (c.DisableBlocksOnOverLimit)
                    {
                        if (c.IgnoreNPCOverLimits && !identity.IsRealPlayer() || c.IgnoreAdminOverLimits && identity.IsAdmin())
                        {
                            
                        } else
                        {
                            if (c.DisableBlocksOnOverLimitDisabledBlocksMatcher.Matches(fu.BlockDefinition.Id) && !c.DisableBlocksOnOverLimitDisabledBlocksExceptMatcher.Matches(fu.BlockDefinition.Id))
                            {
                                DelayedDisable(fu);
                                //Log.Error("IsOverLimits");
                                return;
                            }
                        }
                    }
                }

                if (c.OfflineDisabledBlocksEnabled && c.OfflineDisabledBlocksMaxTime >= 0 && (DateTime.Now - identity.LastLogoutTime).TotalSeconds > c.OfflineDisabledBlocksMaxTime)
                {
                    if (c.IgnoreNPCOffline && !identity.IsRealPlayer() || c.IgnoreAdminOverLimits && identity.IsAdmin())
                    {

                    } else
                    {
                        if (c.OfflineEnabledBlocksMatcher.Matches(fu.BlockDefinition.Id) && !c.OfflineEnabledBlocksExceptMatcher.Matches(fu.BlockDefinition.Id)) //MATCHES FILTER
                        {
                            DelayedDisable(fu);
                            //Log.Error("Offline");
                            return;
                        }
                    }
                }
            } catch (Exception e)
            {
                Log.LogSpamming (47672734, (sb,t)=>sb.Append("Error:"+e));
            }
            
        }
    }
}
