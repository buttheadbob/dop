using SharedLib;
using Slime.GUI;
using System;
using Torch;

namespace Slime.Config
{
    public class CoreConfig : ViewModel
    {
        private bool _disableBlockNoBuiltBy = false;
        private bool _offlineEnabledBlocksEnabled = false;
        private int _offlineEnabledBlocksMaxTime = 60 * 60 * 24; // 1 day
        private string _offlineEnabledBlocksFilter = "*/*";
        private string _offlineEnabledBlocksFilterExcept = "*Medical*/* *Battery*/*";
        private bool _disableBlocksOnOverLimit = false;
        private bool _ingoreNPCOverLimits = false;
        private bool _ingoreAdminOverLimits = false;
        private bool _ingoreNPCOffline = false;
        private bool _ingoreAdminOffline = false;
        private string _disableBlocksOnOverLimitFilter = "*/*";
        private string _disableBlocksOnOverLimitFilterExcept = "*Medical*/* *Battery*/*";
        private BlockIdMatcher _OfflineEnabledBlocksCache = null;
        private BlockIdMatcher _OfflineEnabledBlocksExceptCache = null;
        private BlockIdMatcher _DisableBlocksOnOverLimitDisabledBlocksCache = null;
        private BlockIdMatcher _DisableBlocksOnOverLimitDisabledBlocksExceptCache = null;

        public BlockIdMatcher OfflineEnabledBlocksMatcher
        {
            get
            {
                if (_OfflineEnabledBlocksCache == null) _OfflineEnabledBlocksCache = new BlockIdMatcher(OfflineDisabledBlocksFilter);
                return _OfflineEnabledBlocksCache;
            }
        }

        public BlockIdMatcher OfflineEnabledBlocksExceptMatcher
        {
            get
            {
                if (_OfflineEnabledBlocksExceptCache == null) _OfflineEnabledBlocksExceptCache = new BlockIdMatcher(OfflineDisabledBlocksFilterExcept);
                return _OfflineEnabledBlocksExceptCache;
            }
        }

        public BlockIdMatcher DisableBlocksOnOverLimitDisabledBlocksMatcher
        {
            get
            {
                if (_DisableBlocksOnOverLimitDisabledBlocksCache == null) _DisableBlocksOnOverLimitDisabledBlocksCache = new BlockIdMatcher(DisableBlocksOnOverLimitDisabledBlocks);
                return _DisableBlocksOnOverLimitDisabledBlocksCache;
            }
        }

        public BlockIdMatcher DisableBlocksOnOverLimitDisabledBlocksExceptMatcher
        {
            get
            {
                if (_DisableBlocksOnOverLimitDisabledBlocksExceptCache == null) _DisableBlocksOnOverLimitDisabledBlocksExceptCache = new BlockIdMatcher(DisableBlocksOnOverLimitDisabledBlocksExcept);
                return _DisableBlocksOnOverLimitDisabledBlocksExceptCache;
            }
        }

        private void DropCache()
        {
            try
            {
                EconomyPlugin.Torch.Invoke(() =>
                {
                    _OfflineEnabledBlocksCache = null;
                    _OfflineEnabledBlocksExceptCache = null;
                    _DisableBlocksOnOverLimitDisabledBlocksCache = null;
                    _DisableBlocksOnOverLimitDisabledBlocksExceptCache = null;
                });
            }
            catch (Exception ee) { }
        }

        [DisplayTab(Name = "Enable", GroupName = "DisableBlockWithNoBuiltBy", Order = 0, Description = "Disables all blocks without BuiltBy", LiveUpdate = true)]
        public bool DisableBlockNoBuiltBy { get => _disableBlockNoBuiltBy; set => SetValue(ref _disableBlockNoBuiltBy, value); }

        [DisplayTab(Name = "IngoreNPC", GroupName = "OfflineDisabledBlocks", Order = 0, Description = "Don't turn off NPC blocks", LiveUpdate = true)]
        public bool IgnoreNPCOffline { get => _ingoreNPCOffline; set => SetValue(ref _ingoreNPCOffline, value); }

        [DisplayTab(Name = "IngoreAdmin", GroupName = "OfflineDisabledBlocks", Order = 0, Description = "Don't turn off Admin blocks", LiveUpdate = true)]
        public bool IgnoreAdminOffline { get => _ingoreAdminOffline; set => SetValue(ref _ingoreAdminOffline, value); }

        [DisplayTab(Name = "Enable", GroupName = "OfflineDisabledBlocks", Order = 0, Description = "When player doesn't login for a long time, his blocks are turned off", LiveUpdate = true)]
        public bool OfflineDisabledBlocksEnabled { get => _offlineEnabledBlocksEnabled; set => SetValue(ref _offlineEnabledBlocksEnabled, value); }

        [DisplayTab(Name = "MaxTime (sec)", GroupName = "OfflineDisabledBlocks", Order = 1, LiveUpdate = true)]
        public int OfflineDisabledBlocksMaxTime { get => _offlineEnabledBlocksMaxTime; set => SetValue(ref _offlineEnabledBlocksMaxTime, value); }

        [DisplayTab(Name = "Disabled Blocks", GroupName = "OfflineDisabledBlocks", Order = 1, LiveUpdate = true)]
        public string OfflineDisabledBlocksFilter
        {
            get => _offlineEnabledBlocksFilter;
            set
            {
                SetValue(ref _offlineEnabledBlocksFilter, value);
                DropCache();
            }
        }

        [DisplayTab(Name = "Disabled Blocks Except", GroupName = "OfflineDisabledBlocks", Order = 1, LiveUpdate = true)]
        public string OfflineDisabledBlocksFilterExcept
        {
            get => _offlineEnabledBlocksFilterExcept;
            set
            {
                SetValue(ref _offlineEnabledBlocksFilterExcept, value);
                DropCache();
            }
        }

        [DisplayTab(Name = "Enable", GroupName = "DisableBlocksOnOverLimit", Order = 0, Description = "If player has overlimits, his blocks won't turn on", LiveUpdate = true)]
        public bool DisableBlocksOnOverLimit { get => _disableBlocksOnOverLimit; set => SetValue(ref _disableBlocksOnOverLimit, value); }

        [DisplayTab(Name = "IngoreNPC", GroupName = "DisableBlocksOnOverLimit", Order = 0, Description = "Don't turn off NPC blocks", LiveUpdate = true)]
        public bool IgnoreNPCOverLimits { get => _ingoreNPCOverLimits; set => SetValue(ref _ingoreNPCOverLimits, value); }

        [DisplayTab(Name = "IngoreAdmin", GroupName = "DisableBlocksOnOverLimit", Order = 0, Description = "Don't turn off Admin blocks", LiveUpdate = true)]
        public bool IgnoreAdminOverLimits { get => _ingoreAdminOverLimits; set => SetValue(ref _ingoreAdminOverLimits, value); }

        [DisplayTab(Name = "DisabledBlocks", GroupName = "DisableBlocksOnOverLimit", Order = 0)]
        public string DisableBlocksOnOverLimitDisabledBlocks
        {
            get => _disableBlocksOnOverLimitFilter;
            set
            {
                SetValue(ref _disableBlocksOnOverLimitFilter, value);
                DropCache();
            }
        }

        [DisplayTab(Name = "DisabledBlocksExcept", GroupName = "DisableBlocksOnOverLimit", Order = 0)]
        public string DisableBlocksOnOverLimitDisabledBlocksExcept
        {
            get => _disableBlocksOnOverLimitFilterExcept;
            set
            {
                SetValue(ref _disableBlocksOnOverLimitFilterExcept, value);
                DropCache();
            }
        }
    }
}