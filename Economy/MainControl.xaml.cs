﻿using System.Windows.Controls;

namespace Slime.GUI
{
    public partial class MainControl : UserControl
    {
        public MainControl()
        {
            InitializeComponent();
            POptimizations.DataContext = EconomyPlugin.Config;
        }
    }
}